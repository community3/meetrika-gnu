/*
               File: EstadoGeral_UnidadeOrganizacionalWC
        Description: Estado Geral_Unidade Organizacional WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:21:58.84
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class estadogeral_unidadeorganizacionalwc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public estadogeral_unidadeorganizacionalwc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public estadogeral_unidadeorganizacionalwc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Estado_UF )
      {
         this.AV7Estado_UF = aP0_Estado_UF;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkUnidadeOrganizacional_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Estado_UF = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Estado_UF", AV7Estado_UF);
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(String)AV7Estado_UF});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_87 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_87_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_87_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
                  AV18UnidadeOrganizacional_Nome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18UnidadeOrganizacional_Nome1", AV18UnidadeOrganizacional_Nome1);
                  AV19TpUo_Nome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TpUo_Nome1", AV19TpUo_Nome1);
                  AV20UnidadeOrganizacional_VinculadaNom1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UnidadeOrganizacional_VinculadaNom1", AV20UnidadeOrganizacional_VinculadaNom1);
                  AV22DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
                  AV23DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)));
                  AV24UnidadeOrganizacional_Nome2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24UnidadeOrganizacional_Nome2", AV24UnidadeOrganizacional_Nome2);
                  AV25TpUo_Nome2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TpUo_Nome2", AV25TpUo_Nome2);
                  AV26UnidadeOrganizacional_VinculadaNom2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26UnidadeOrganizacional_VinculadaNom2", AV26UnidadeOrganizacional_VinculadaNom2);
                  AV28DynamicFiltersSelector3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
                  AV29DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)));
                  AV30UnidadeOrganizacional_Nome3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30UnidadeOrganizacional_Nome3", AV30UnidadeOrganizacional_Nome3);
                  AV31TpUo_Nome3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TpUo_Nome3", AV31TpUo_Nome3);
                  AV32UnidadeOrganizacional_VinculadaNom3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32UnidadeOrganizacional_VinculadaNom3", AV32UnidadeOrganizacional_VinculadaNom3);
                  AV21DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
                  AV27DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersEnabled3", AV27DynamicFiltersEnabled3);
                  AV7Estado_UF = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Estado_UF", AV7Estado_UF);
                  AV47Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV34DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34DynamicFiltersIgnoreFirst", AV34DynamicFiltersIgnoreFirst);
                  AV33DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  A611UnidadeOrganizacional_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A609TpUo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A613UnidadeOrganizacional_Vinculada = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n613UnidadeOrganizacional_Vinculada = false;
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18UnidadeOrganizacional_Nome1, AV19TpUo_Nome1, AV20UnidadeOrganizacional_VinculadaNom1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24UnidadeOrganizacional_Nome2, AV25TpUo_Nome2, AV26UnidadeOrganizacional_VinculadaNom2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30UnidadeOrganizacional_Nome3, AV31TpUo_Nome3, AV32UnidadeOrganizacional_VinculadaNom3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7Estado_UF, AV47Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, AV6WWPContext, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo, A613UnidadeOrganizacional_Vinculada, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAP42( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV47Pgmname = "EstadoGeral_UnidadeOrganizacionalWC";
               context.Gx_err = 0;
               WSP42( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Estado Geral_Unidade Organizacional WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117215931");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("estadogeral_unidadeorganizacionalwc.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV7Estado_UF))+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME1", StringUtil.RTrim( AV18UnidadeOrganizacional_Nome1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTPUO_NOME1", StringUtil.RTrim( AV19TpUo_Nome1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vUNIDADEORGANIZACIONAL_VINCULADANOM1", StringUtil.RTrim( AV20UnidadeOrganizacional_VinculadaNom1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV22DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME2", StringUtil.RTrim( AV24UnidadeOrganizacional_Nome2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTPUO_NOME2", StringUtil.RTrim( AV25TpUo_Nome2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vUNIDADEORGANIZACIONAL_VINCULADANOM2", StringUtil.RTrim( AV26UnidadeOrganizacional_VinculadaNom2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3", AV28DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME3", StringUtil.RTrim( AV30UnidadeOrganizacional_Nome3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTPUO_NOME3", StringUtil.RTrim( AV31TpUo_Nome3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vUNIDADEORGANIZACIONAL_VINCULADANOM3", StringUtil.RTrim( AV32UnidadeOrganizacional_VinculadaNom3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV21DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV27DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_87", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_87), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Estado_UF", StringUtil.RTrim( wcpOAV7Estado_UF));
         GxWebStd.gx_hidden_field( context, sPrefix+"vESTADO_UF", StringUtil.RTrim( AV7Estado_UF));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV47Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV34DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV33DynamicFiltersRemoving);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormP42( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("estadogeral_unidadeorganizacionalwc.js", "?20203117215982");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "EstadoGeral_UnidadeOrganizacionalWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Estado Geral_Unidade Organizacional WC" ;
      }

      protected void WBP40( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "estadogeral_unidadeorganizacionalwc.aspx");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
            }
            wb_table1_2_P42( true) ;
         }
         else
         {
            wb_table1_2_P42( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_P42e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtEstado_UF_Internalname, StringUtil.RTrim( A23Estado_UF), StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEstado_UF_Jsonclick, 0, "Attribute", "", "", "", edtEstado_UF_Visible, 0, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "UF", "left", true, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV21DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(104, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV27DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(105, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"");
         }
         wbLoad = true;
      }

      protected void STARTP42( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Estado Geral_Unidade Organizacional WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPP40( ) ;
            }
         }
      }

      protected void WSP42( )
      {
         STARTP42( ) ;
         EVTP42( ) ;
      }

      protected void EVTP42( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11P42 */
                                    E11P42 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12P42 */
                                    E12P42 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13P42 */
                                    E13P42 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14P42 */
                                    E14P42 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15P42 */
                                    E15P42 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16P42 */
                                    E16P42 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17P42 */
                                    E17P42 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18P42 */
                                    E18P42 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19P42 */
                                    E19P42 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20P42 */
                                    E20P42 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21P42 */
                                    E21P42 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP40( ) ;
                              }
                              nGXsfl_87_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_87_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_87_idx), 4, 0)), 4, "0");
                              SubsflControlProps_872( ) ;
                              AV35Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV35Update)) ? AV44Update_GXI : context.convertURL( context.PathToRelativeUrl( AV35Update))));
                              AV36Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV36Delete)) ? AV45Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV36Delete))));
                              AV37Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV37Display)) ? AV46Display_GXI : context.convertURL( context.PathToRelativeUrl( AV37Display))));
                              A611UnidadeOrganizacional_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUnidadeOrganizacional_Codigo_Internalname), ",", "."));
                              A612UnidadeOrganizacional_Nome = StringUtil.Upper( cgiGet( edtUnidadeOrganizacional_Nome_Internalname));
                              A609TpUo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTpUo_Codigo_Internalname), ",", "."));
                              A610TpUo_Nome = StringUtil.Upper( cgiGet( edtTpUo_Nome_Internalname));
                              A613UnidadeOrganizacional_Vinculada = (int)(context.localUtil.CToN( cgiGet( edtUnidadeOrganizacional_Vinculada_Internalname), ",", "."));
                              n613UnidadeOrganizacional_Vinculada = false;
                              A1082UnidadeOrganizacional_VinculadaNom = StringUtil.Upper( cgiGet( edtUnidadeOrganizacional_VinculadaNom_Internalname));
                              n1082UnidadeOrganizacional_VinculadaNom = false;
                              A1174UnidadeOrganizacinal_Arvore = cgiGet( edtUnidadeOrganizacinal_Arvore_Internalname);
                              A629UnidadeOrganizacional_Ativo = StringUtil.StrToBool( cgiGet( chkUnidadeOrganizacional_Ativo_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E22P42 */
                                          E22P42 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E23P42 */
                                          E23P42 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E24P42 */
                                          E24P42 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Unidadeorganizacional_nome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME1"), AV18UnidadeOrganizacional_Nome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tpuo_nome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTPUO_NOME1"), AV19TpUo_Nome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Unidadeorganizacional_vinculadanom1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_VINCULADANOM1"), AV20UnidadeOrganizacional_VinculadaNom1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV22DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV23DynamicFiltersOperator2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Unidadeorganizacional_nome2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME2"), AV24UnidadeOrganizacional_Nome2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tpuo_nome2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTPUO_NOME2"), AV25TpUo_Nome2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Unidadeorganizacional_vinculadanom2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_VINCULADANOM2"), AV26UnidadeOrganizacional_VinculadaNom2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV28DynamicFiltersSelector3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV29DynamicFiltersOperator3 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Unidadeorganizacional_nome3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME3"), AV30UnidadeOrganizacional_Nome3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tpuo_nome3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTPUO_NOME3"), AV31TpUo_Nome3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Unidadeorganizacional_vinculadanom3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_VINCULADANOM3"), AV32UnidadeOrganizacional_VinculadaNom3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV21DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV27DynamicFiltersEnabled3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPP40( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEP42( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormP42( ) ;
            }
         }
      }

      protected void PAP42( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("UNIDADEORGANIZACIONAL_NOME", "Organizacional", 0);
            cmbavDynamicfiltersselector1.addItem("TPUO_NOME", "Tp Uo_Nome", 0);
            cmbavDynamicfiltersselector1.addItem("UNIDADEORGANIZACIONAL_VINCULADANOM", "Depend�ncia", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("UNIDADEORGANIZACIONAL_NOME", "Organizacional", 0);
            cmbavDynamicfiltersselector2.addItem("TPUO_NOME", "Tp Uo_Nome", 0);
            cmbavDynamicfiltersselector2.addItem("UNIDADEORGANIZACIONAL_VINCULADANOM", "Depend�ncia", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV22DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV22DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV23DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("UNIDADEORGANIZACIONAL_NOME", "Organizacional", 0);
            cmbavDynamicfiltersselector3.addItem("TPUO_NOME", "Tp Uo_Nome", 0);
            cmbavDynamicfiltersselector3.addItem("UNIDADEORGANIZACIONAL_VINCULADANOM", "Depend�ncia", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV28DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV28DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV29DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "UNIDADEORGANIZACIONAL_ATIVO_" + sGXsfl_87_idx;
            chkUnidadeOrganizacional_Ativo.Name = GXCCtl;
            chkUnidadeOrganizacional_Ativo.WebTags = "";
            chkUnidadeOrganizacional_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkUnidadeOrganizacional_Ativo_Internalname, "TitleCaption", chkUnidadeOrganizacional_Ativo.Caption);
            chkUnidadeOrganizacional_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_872( ) ;
         while ( nGXsfl_87_idx <= nRC_GXsfl_87 )
         {
            sendrow_872( ) ;
            nGXsfl_87_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_87_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_87_idx+1));
            sGXsfl_87_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_87_idx), 4, 0)), 4, "0");
            SubsflControlProps_872( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       String AV18UnidadeOrganizacional_Nome1 ,
                                       String AV19TpUo_Nome1 ,
                                       String AV20UnidadeOrganizacional_VinculadaNom1 ,
                                       String AV22DynamicFiltersSelector2 ,
                                       short AV23DynamicFiltersOperator2 ,
                                       String AV24UnidadeOrganizacional_Nome2 ,
                                       String AV25TpUo_Nome2 ,
                                       String AV26UnidadeOrganizacional_VinculadaNom2 ,
                                       String AV28DynamicFiltersSelector3 ,
                                       short AV29DynamicFiltersOperator3 ,
                                       String AV30UnidadeOrganizacional_Nome3 ,
                                       String AV31TpUo_Nome3 ,
                                       String AV32UnidadeOrganizacional_VinculadaNom3 ,
                                       bool AV21DynamicFiltersEnabled2 ,
                                       bool AV27DynamicFiltersEnabled3 ,
                                       String AV7Estado_UF ,
                                       String AV47Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV34DynamicFiltersIgnoreFirst ,
                                       bool AV33DynamicFiltersRemoving ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int A611UnidadeOrganizacional_Codigo ,
                                       int A609TpUo_Codigo ,
                                       int A613UnidadeOrganizacional_Vinculada ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFP42( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACIONAL_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A611UnidadeOrganizacional_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"UNIDADEORGANIZACIONAL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACIONAL_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A612UnidadeOrganizacional_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"UNIDADEORGANIZACIONAL_NOME", StringUtil.RTrim( A612UnidadeOrganizacional_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TPUO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A609TpUo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"TPUO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A609TpUo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACIONAL_VINCULADA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A613UnidadeOrganizacional_Vinculada), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"UNIDADEORGANIZACIONAL_VINCULADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACINAL_ARVORE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1174UnidadeOrganizacinal_Arvore, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"UNIDADEORGANIZACINAL_ARVORE", StringUtil.RTrim( A1174UnidadeOrganizacinal_Arvore));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACIONAL_ATIVO", GetSecureSignedToken( sPrefix, A629UnidadeOrganizacional_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"UNIDADEORGANIZACIONAL_ATIVO", StringUtil.BoolToStr( A629UnidadeOrganizacional_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV22DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV22DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV23DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV28DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV28DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV29DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFP42( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV47Pgmname = "EstadoGeral_UnidadeOrganizacionalWC";
         context.Gx_err = 0;
      }

      protected void RFP42( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 87;
         /* Execute user event: E23P42 */
         E23P42 ();
         nGXsfl_87_idx = 1;
         sGXsfl_87_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_87_idx), 4, 0)), 4, "0");
         SubsflControlProps_872( ) ;
         nGXsfl_87_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_872( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17DynamicFiltersOperator1 ,
                                                 AV18UnidadeOrganizacional_Nome1 ,
                                                 AV19TpUo_Nome1 ,
                                                 AV20UnidadeOrganizacional_VinculadaNom1 ,
                                                 AV21DynamicFiltersEnabled2 ,
                                                 AV22DynamicFiltersSelector2 ,
                                                 AV23DynamicFiltersOperator2 ,
                                                 AV24UnidadeOrganizacional_Nome2 ,
                                                 AV25TpUo_Nome2 ,
                                                 AV26UnidadeOrganizacional_VinculadaNom2 ,
                                                 AV27DynamicFiltersEnabled3 ,
                                                 AV28DynamicFiltersSelector3 ,
                                                 AV29DynamicFiltersOperator3 ,
                                                 AV30UnidadeOrganizacional_Nome3 ,
                                                 AV31TpUo_Nome3 ,
                                                 AV32UnidadeOrganizacional_VinculadaNom3 ,
                                                 A612UnidadeOrganizacional_Nome ,
                                                 A610TpUo_Nome ,
                                                 A1082UnidadeOrganizacional_VinculadaNom ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A23Estado_UF ,
                                                 AV7Estado_UF },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING
                                                 }
            });
            lV18UnidadeOrganizacional_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18UnidadeOrganizacional_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18UnidadeOrganizacional_Nome1", AV18UnidadeOrganizacional_Nome1);
            lV18UnidadeOrganizacional_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18UnidadeOrganizacional_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18UnidadeOrganizacional_Nome1", AV18UnidadeOrganizacional_Nome1);
            lV19TpUo_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19TpUo_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TpUo_Nome1", AV19TpUo_Nome1);
            lV19TpUo_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19TpUo_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TpUo_Nome1", AV19TpUo_Nome1);
            lV20UnidadeOrganizacional_VinculadaNom1 = StringUtil.PadR( StringUtil.RTrim( AV20UnidadeOrganizacional_VinculadaNom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UnidadeOrganizacional_VinculadaNom1", AV20UnidadeOrganizacional_VinculadaNom1);
            lV20UnidadeOrganizacional_VinculadaNom1 = StringUtil.PadR( StringUtil.RTrim( AV20UnidadeOrganizacional_VinculadaNom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UnidadeOrganizacional_VinculadaNom1", AV20UnidadeOrganizacional_VinculadaNom1);
            lV24UnidadeOrganizacional_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV24UnidadeOrganizacional_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24UnidadeOrganizacional_Nome2", AV24UnidadeOrganizacional_Nome2);
            lV24UnidadeOrganizacional_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV24UnidadeOrganizacional_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24UnidadeOrganizacional_Nome2", AV24UnidadeOrganizacional_Nome2);
            lV25TpUo_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV25TpUo_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TpUo_Nome2", AV25TpUo_Nome2);
            lV25TpUo_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV25TpUo_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TpUo_Nome2", AV25TpUo_Nome2);
            lV26UnidadeOrganizacional_VinculadaNom2 = StringUtil.PadR( StringUtil.RTrim( AV26UnidadeOrganizacional_VinculadaNom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26UnidadeOrganizacional_VinculadaNom2", AV26UnidadeOrganizacional_VinculadaNom2);
            lV26UnidadeOrganizacional_VinculadaNom2 = StringUtil.PadR( StringUtil.RTrim( AV26UnidadeOrganizacional_VinculadaNom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26UnidadeOrganizacional_VinculadaNom2", AV26UnidadeOrganizacional_VinculadaNom2);
            lV30UnidadeOrganizacional_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV30UnidadeOrganizacional_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30UnidadeOrganizacional_Nome3", AV30UnidadeOrganizacional_Nome3);
            lV30UnidadeOrganizacional_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV30UnidadeOrganizacional_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30UnidadeOrganizacional_Nome3", AV30UnidadeOrganizacional_Nome3);
            lV31TpUo_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV31TpUo_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TpUo_Nome3", AV31TpUo_Nome3);
            lV31TpUo_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV31TpUo_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TpUo_Nome3", AV31TpUo_Nome3);
            lV32UnidadeOrganizacional_VinculadaNom3 = StringUtil.PadR( StringUtil.RTrim( AV32UnidadeOrganizacional_VinculadaNom3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32UnidadeOrganizacional_VinculadaNom3", AV32UnidadeOrganizacional_VinculadaNom3);
            lV32UnidadeOrganizacional_VinculadaNom3 = StringUtil.PadR( StringUtil.RTrim( AV32UnidadeOrganizacional_VinculadaNom3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32UnidadeOrganizacional_VinculadaNom3", AV32UnidadeOrganizacional_VinculadaNom3);
            /* Using cursor H00P42 */
            pr_default.execute(0, new Object[] {AV7Estado_UF, lV18UnidadeOrganizacional_Nome1, lV18UnidadeOrganizacional_Nome1, lV19TpUo_Nome1, lV19TpUo_Nome1, lV20UnidadeOrganizacional_VinculadaNom1, lV20UnidadeOrganizacional_VinculadaNom1, lV24UnidadeOrganizacional_Nome2, lV24UnidadeOrganizacional_Nome2, lV25TpUo_Nome2, lV25TpUo_Nome2, lV26UnidadeOrganizacional_VinculadaNom2, lV26UnidadeOrganizacional_VinculadaNom2, lV30UnidadeOrganizacional_Nome3, lV30UnidadeOrganizacional_Nome3, lV31TpUo_Nome3, lV31TpUo_Nome3, lV32UnidadeOrganizacional_VinculadaNom3, lV32UnidadeOrganizacional_VinculadaNom3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_87_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A23Estado_UF = H00P42_A23Estado_UF[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A23Estado_UF", A23Estado_UF);
               A629UnidadeOrganizacional_Ativo = H00P42_A629UnidadeOrganizacional_Ativo[0];
               A1082UnidadeOrganizacional_VinculadaNom = H00P42_A1082UnidadeOrganizacional_VinculadaNom[0];
               n1082UnidadeOrganizacional_VinculadaNom = H00P42_n1082UnidadeOrganizacional_VinculadaNom[0];
               A610TpUo_Nome = H00P42_A610TpUo_Nome[0];
               A609TpUo_Codigo = H00P42_A609TpUo_Codigo[0];
               A612UnidadeOrganizacional_Nome = H00P42_A612UnidadeOrganizacional_Nome[0];
               A611UnidadeOrganizacional_Codigo = H00P42_A611UnidadeOrganizacional_Codigo[0];
               A613UnidadeOrganizacional_Vinculada = H00P42_A613UnidadeOrganizacional_Vinculada[0];
               n613UnidadeOrganizacional_Vinculada = H00P42_n613UnidadeOrganizacional_Vinculada[0];
               A610TpUo_Nome = H00P42_A610TpUo_Nome[0];
               A1082UnidadeOrganizacional_VinculadaNom = H00P42_A1082UnidadeOrganizacional_VinculadaNom[0];
               n1082UnidadeOrganizacional_VinculadaNom = H00P42_n1082UnidadeOrganizacional_VinculadaNom[0];
               if ( ! H00P42_n613UnidadeOrganizacional_Vinculada[0] )
               {
                  GXt_char1 = A1174UnidadeOrganizacinal_Arvore;
                  new prc_uoarvoredependencias(context ).execute(  A613UnidadeOrganizacional_Vinculada, out  GXt_char1) ;
                  A1174UnidadeOrganizacinal_Arvore = GXt_char1;
               }
               else
               {
                  A1174UnidadeOrganizacinal_Arvore = "";
               }
               /* Execute user event: E24P42 */
               E24P42 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 87;
            WBP40( ) ;
         }
         nGXsfl_87_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV17DynamicFiltersOperator1 ,
                                              AV18UnidadeOrganizacional_Nome1 ,
                                              AV19TpUo_Nome1 ,
                                              AV20UnidadeOrganizacional_VinculadaNom1 ,
                                              AV21DynamicFiltersEnabled2 ,
                                              AV22DynamicFiltersSelector2 ,
                                              AV23DynamicFiltersOperator2 ,
                                              AV24UnidadeOrganizacional_Nome2 ,
                                              AV25TpUo_Nome2 ,
                                              AV26UnidadeOrganizacional_VinculadaNom2 ,
                                              AV27DynamicFiltersEnabled3 ,
                                              AV28DynamicFiltersSelector3 ,
                                              AV29DynamicFiltersOperator3 ,
                                              AV30UnidadeOrganizacional_Nome3 ,
                                              AV31TpUo_Nome3 ,
                                              AV32UnidadeOrganizacional_VinculadaNom3 ,
                                              A612UnidadeOrganizacional_Nome ,
                                              A610TpUo_Nome ,
                                              A1082UnidadeOrganizacional_VinculadaNom ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A23Estado_UF ,
                                              AV7Estado_UF },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV18UnidadeOrganizacional_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18UnidadeOrganizacional_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18UnidadeOrganizacional_Nome1", AV18UnidadeOrganizacional_Nome1);
         lV18UnidadeOrganizacional_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18UnidadeOrganizacional_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18UnidadeOrganizacional_Nome1", AV18UnidadeOrganizacional_Nome1);
         lV19TpUo_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19TpUo_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TpUo_Nome1", AV19TpUo_Nome1);
         lV19TpUo_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19TpUo_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TpUo_Nome1", AV19TpUo_Nome1);
         lV20UnidadeOrganizacional_VinculadaNom1 = StringUtil.PadR( StringUtil.RTrim( AV20UnidadeOrganizacional_VinculadaNom1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UnidadeOrganizacional_VinculadaNom1", AV20UnidadeOrganizacional_VinculadaNom1);
         lV20UnidadeOrganizacional_VinculadaNom1 = StringUtil.PadR( StringUtil.RTrim( AV20UnidadeOrganizacional_VinculadaNom1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UnidadeOrganizacional_VinculadaNom1", AV20UnidadeOrganizacional_VinculadaNom1);
         lV24UnidadeOrganizacional_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV24UnidadeOrganizacional_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24UnidadeOrganizacional_Nome2", AV24UnidadeOrganizacional_Nome2);
         lV24UnidadeOrganizacional_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV24UnidadeOrganizacional_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24UnidadeOrganizacional_Nome2", AV24UnidadeOrganizacional_Nome2);
         lV25TpUo_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV25TpUo_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TpUo_Nome2", AV25TpUo_Nome2);
         lV25TpUo_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV25TpUo_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TpUo_Nome2", AV25TpUo_Nome2);
         lV26UnidadeOrganizacional_VinculadaNom2 = StringUtil.PadR( StringUtil.RTrim( AV26UnidadeOrganizacional_VinculadaNom2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26UnidadeOrganizacional_VinculadaNom2", AV26UnidadeOrganizacional_VinculadaNom2);
         lV26UnidadeOrganizacional_VinculadaNom2 = StringUtil.PadR( StringUtil.RTrim( AV26UnidadeOrganizacional_VinculadaNom2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26UnidadeOrganizacional_VinculadaNom2", AV26UnidadeOrganizacional_VinculadaNom2);
         lV30UnidadeOrganizacional_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV30UnidadeOrganizacional_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30UnidadeOrganizacional_Nome3", AV30UnidadeOrganizacional_Nome3);
         lV30UnidadeOrganizacional_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV30UnidadeOrganizacional_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30UnidadeOrganizacional_Nome3", AV30UnidadeOrganizacional_Nome3);
         lV31TpUo_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV31TpUo_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TpUo_Nome3", AV31TpUo_Nome3);
         lV31TpUo_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV31TpUo_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TpUo_Nome3", AV31TpUo_Nome3);
         lV32UnidadeOrganizacional_VinculadaNom3 = StringUtil.PadR( StringUtil.RTrim( AV32UnidadeOrganizacional_VinculadaNom3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32UnidadeOrganizacional_VinculadaNom3", AV32UnidadeOrganizacional_VinculadaNom3);
         lV32UnidadeOrganizacional_VinculadaNom3 = StringUtil.PadR( StringUtil.RTrim( AV32UnidadeOrganizacional_VinculadaNom3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32UnidadeOrganizacional_VinculadaNom3", AV32UnidadeOrganizacional_VinculadaNom3);
         /* Using cursor H00P43 */
         pr_default.execute(1, new Object[] {AV7Estado_UF, lV18UnidadeOrganizacional_Nome1, lV18UnidadeOrganizacional_Nome1, lV19TpUo_Nome1, lV19TpUo_Nome1, lV20UnidadeOrganizacional_VinculadaNom1, lV20UnidadeOrganizacional_VinculadaNom1, lV24UnidadeOrganizacional_Nome2, lV24UnidadeOrganizacional_Nome2, lV25TpUo_Nome2, lV25TpUo_Nome2, lV26UnidadeOrganizacional_VinculadaNom2, lV26UnidadeOrganizacional_VinculadaNom2, lV30UnidadeOrganizacional_Nome3, lV30UnidadeOrganizacional_Nome3, lV31TpUo_Nome3, lV31TpUo_Nome3, lV32UnidadeOrganizacional_VinculadaNom3, lV32UnidadeOrganizacional_VinculadaNom3});
         GRID_nRecordCount = H00P43_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18UnidadeOrganizacional_Nome1, AV19TpUo_Nome1, AV20UnidadeOrganizacional_VinculadaNom1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24UnidadeOrganizacional_Nome2, AV25TpUo_Nome2, AV26UnidadeOrganizacional_VinculadaNom2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30UnidadeOrganizacional_Nome3, AV31TpUo_Nome3, AV32UnidadeOrganizacional_VinculadaNom3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7Estado_UF, AV47Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, AV6WWPContext, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo, A613UnidadeOrganizacional_Vinculada, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18UnidadeOrganizacional_Nome1, AV19TpUo_Nome1, AV20UnidadeOrganizacional_VinculadaNom1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24UnidadeOrganizacional_Nome2, AV25TpUo_Nome2, AV26UnidadeOrganizacional_VinculadaNom2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30UnidadeOrganizacional_Nome3, AV31TpUo_Nome3, AV32UnidadeOrganizacional_VinculadaNom3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7Estado_UF, AV47Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, AV6WWPContext, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo, A613UnidadeOrganizacional_Vinculada, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18UnidadeOrganizacional_Nome1, AV19TpUo_Nome1, AV20UnidadeOrganizacional_VinculadaNom1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24UnidadeOrganizacional_Nome2, AV25TpUo_Nome2, AV26UnidadeOrganizacional_VinculadaNom2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30UnidadeOrganizacional_Nome3, AV31TpUo_Nome3, AV32UnidadeOrganizacional_VinculadaNom3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7Estado_UF, AV47Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, AV6WWPContext, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo, A613UnidadeOrganizacional_Vinculada, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18UnidadeOrganizacional_Nome1, AV19TpUo_Nome1, AV20UnidadeOrganizacional_VinculadaNom1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24UnidadeOrganizacional_Nome2, AV25TpUo_Nome2, AV26UnidadeOrganizacional_VinculadaNom2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30UnidadeOrganizacional_Nome3, AV31TpUo_Nome3, AV32UnidadeOrganizacional_VinculadaNom3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7Estado_UF, AV47Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, AV6WWPContext, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo, A613UnidadeOrganizacional_Vinculada, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18UnidadeOrganizacional_Nome1, AV19TpUo_Nome1, AV20UnidadeOrganizacional_VinculadaNom1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24UnidadeOrganizacional_Nome2, AV25TpUo_Nome2, AV26UnidadeOrganizacional_VinculadaNom2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30UnidadeOrganizacional_Nome3, AV31TpUo_Nome3, AV32UnidadeOrganizacional_VinculadaNom3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7Estado_UF, AV47Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, AV6WWPContext, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo, A613UnidadeOrganizacional_Vinculada, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPP40( )
      {
         /* Before Start, stand alone formulas. */
         AV47Pgmname = "EstadoGeral_UnidadeOrganizacionalWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E22P42 */
         E22P42 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            AV18UnidadeOrganizacional_Nome1 = StringUtil.Upper( cgiGet( edtavUnidadeorganizacional_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18UnidadeOrganizacional_Nome1", AV18UnidadeOrganizacional_Nome1);
            AV19TpUo_Nome1 = StringUtil.Upper( cgiGet( edtavTpuo_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TpUo_Nome1", AV19TpUo_Nome1);
            AV20UnidadeOrganizacional_VinculadaNom1 = StringUtil.Upper( cgiGet( edtavUnidadeorganizacional_vinculadanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UnidadeOrganizacional_VinculadaNom1", AV20UnidadeOrganizacional_VinculadaNom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV22DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV23DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)));
            AV24UnidadeOrganizacional_Nome2 = StringUtil.Upper( cgiGet( edtavUnidadeorganizacional_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24UnidadeOrganizacional_Nome2", AV24UnidadeOrganizacional_Nome2);
            AV25TpUo_Nome2 = StringUtil.Upper( cgiGet( edtavTpuo_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TpUo_Nome2", AV25TpUo_Nome2);
            AV26UnidadeOrganizacional_VinculadaNom2 = StringUtil.Upper( cgiGet( edtavUnidadeorganizacional_vinculadanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26UnidadeOrganizacional_VinculadaNom2", AV26UnidadeOrganizacional_VinculadaNom2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV28DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV29DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)));
            AV30UnidadeOrganizacional_Nome3 = StringUtil.Upper( cgiGet( edtavUnidadeorganizacional_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30UnidadeOrganizacional_Nome3", AV30UnidadeOrganizacional_Nome3);
            AV31TpUo_Nome3 = StringUtil.Upper( cgiGet( edtavTpuo_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TpUo_Nome3", AV31TpUo_Nome3);
            AV32UnidadeOrganizacional_VinculadaNom3 = StringUtil.Upper( cgiGet( edtavUnidadeorganizacional_vinculadanom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32UnidadeOrganizacional_VinculadaNom3", AV32UnidadeOrganizacional_VinculadaNom3);
            A23Estado_UF = StringUtil.Upper( cgiGet( edtEstado_UF_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A23Estado_UF", A23Estado_UF);
            AV21DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
            AV27DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersEnabled3", AV27DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_87 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_87"), ",", "."));
            AV40GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV41GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Estado_UF = cgiGet( sPrefix+"wcpOAV7Estado_UF");
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME1"), AV18UnidadeOrganizacional_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTPUO_NOME1"), AV19TpUo_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_VINCULADANOM1"), AV20UnidadeOrganizacional_VinculadaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV22DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV23DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME2"), AV24UnidadeOrganizacional_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTPUO_NOME2"), AV25TpUo_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_VINCULADANOM2"), AV26UnidadeOrganizacional_VinculadaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV28DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV29DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME3"), AV30UnidadeOrganizacional_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTPUO_NOME3"), AV31TpUo_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_VINCULADANOM3"), AV32UnidadeOrganizacional_VinculadaNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV21DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV27DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E22P42 */
         E22P42 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22P42( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersSelector2 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV28DynamicFiltersSelector3 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtEstado_UF_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtEstado_UF_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEstado_UF_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Organizacional", 0);
         cmbavOrderedby.addItem("2", "Tp Uo_Nome", 0);
         cmbavOrderedby.addItem("3", "Depend�ncia", 0);
         cmbavOrderedby.addItem("4", "Ativa?", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
      }

      protected void E23P42( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TPUO_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV21DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "TPUO_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV27DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "TPUO_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtUnidadeOrganizacional_Nome_Titleformat = 2;
         edtUnidadeOrganizacional_Nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV14OrderedBy==1) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Organizacional", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtUnidadeOrganizacional_Nome_Internalname, "Title", edtUnidadeOrganizacional_Nome_Title);
         edtTpUo_Nome_Titleformat = 2;
         edtTpUo_Nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV14OrderedBy==2) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Tp Uo_Nome", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTpUo_Nome_Internalname, "Title", edtTpUo_Nome_Title);
         edtUnidadeOrganizacional_VinculadaNom_Titleformat = 2;
         edtUnidadeOrganizacional_VinculadaNom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV14OrderedBy==3) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Depend�ncia", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtUnidadeOrganizacional_VinculadaNom_Internalname, "Title", edtUnidadeOrganizacional_VinculadaNom_Title);
         chkUnidadeOrganizacional_Ativo_Titleformat = 2;
         chkUnidadeOrganizacional_Ativo.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV14OrderedBy==4) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Ativa?", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkUnidadeOrganizacional_Ativo_Internalname, "Title", chkUnidadeOrganizacional_Ativo.Title.Text);
         AV40GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40GridCurrentPage), 10, 0)));
         AV41GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11P42( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV39PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV39PageToGo) ;
         }
      }

      private void E24P42( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("geral_unidadeorganizacional.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A611UnidadeOrganizacional_Codigo);
            AV35Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV35Update);
            AV44Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV35Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV35Update);
            AV44Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("geral_unidadeorganizacional.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A611UnidadeOrganizacional_Codigo);
            AV36Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV36Delete);
            AV45Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV36Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV36Delete);
            AV45Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewgeral_unidadeorganizacional.aspx") + "?" + UrlEncode("" +A611UnidadeOrganizacional_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV37Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV37Display);
            AV46Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV37Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV37Display);
            AV46Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         edtUnidadeOrganizacional_Nome_Link = formatLink("viewgeral_unidadeorganizacional.aspx") + "?" + UrlEncode("" +A611UnidadeOrganizacional_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtTpUo_Nome_Link = formatLink("viewgeral_tp_uo.aspx") + "?" + UrlEncode("" +A609TpUo_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtUnidadeOrganizacional_VinculadaNom_Link = formatLink("viewgeral_unidadeorganizacional.aspx") + "?" + UrlEncode("" +A613UnidadeOrganizacional_Vinculada) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 87;
         }
         sendrow_872( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_87_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(87, GridRow);
         }
      }

      protected void E12P42( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E17P42( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV21DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18UnidadeOrganizacional_Nome1, AV19TpUo_Nome1, AV20UnidadeOrganizacional_VinculadaNom1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24UnidadeOrganizacional_Nome2, AV25TpUo_Nome2, AV26UnidadeOrganizacional_VinculadaNom2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30UnidadeOrganizacional_Nome3, AV31TpUo_Nome3, AV32UnidadeOrganizacional_VinculadaNom3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7Estado_UF, AV47Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, AV6WWPContext, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo, A613UnidadeOrganizacional_Vinculada, sPrefix) ;
      }

      protected void E13P42( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV33DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
         AV34DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34DynamicFiltersIgnoreFirst", AV34DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV33DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
         AV34DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34DynamicFiltersIgnoreFirst", AV34DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18UnidadeOrganizacional_Nome1, AV19TpUo_Nome1, AV20UnidadeOrganizacional_VinculadaNom1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24UnidadeOrganizacional_Nome2, AV25TpUo_Nome2, AV26UnidadeOrganizacional_VinculadaNom2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30UnidadeOrganizacional_Nome3, AV31TpUo_Nome3, AV32UnidadeOrganizacional_VinculadaNom3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7Estado_UF, AV47Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, AV6WWPContext, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo, A613UnidadeOrganizacional_Vinculada, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV28DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E18P42( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E19P42( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV27DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersEnabled3", AV27DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18UnidadeOrganizacional_Nome1, AV19TpUo_Nome1, AV20UnidadeOrganizacional_VinculadaNom1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24UnidadeOrganizacional_Nome2, AV25TpUo_Nome2, AV26UnidadeOrganizacional_VinculadaNom2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30UnidadeOrganizacional_Nome3, AV31TpUo_Nome3, AV32UnidadeOrganizacional_VinculadaNom3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7Estado_UF, AV47Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, AV6WWPContext, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo, A613UnidadeOrganizacional_Vinculada, sPrefix) ;
      }

      protected void E14P42( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV33DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
         AV21DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV33DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18UnidadeOrganizacional_Nome1, AV19TpUo_Nome1, AV20UnidadeOrganizacional_VinculadaNom1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24UnidadeOrganizacional_Nome2, AV25TpUo_Nome2, AV26UnidadeOrganizacional_VinculadaNom2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30UnidadeOrganizacional_Nome3, AV31TpUo_Nome3, AV32UnidadeOrganizacional_VinculadaNom3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7Estado_UF, AV47Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, AV6WWPContext, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo, A613UnidadeOrganizacional_Vinculada, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV28DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E20P42( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV23DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E15P42( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV33DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
         AV27DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersEnabled3", AV27DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV33DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18UnidadeOrganizacional_Nome1, AV19TpUo_Nome1, AV20UnidadeOrganizacional_VinculadaNom1, AV22DynamicFiltersSelector2, AV23DynamicFiltersOperator2, AV24UnidadeOrganizacional_Nome2, AV25TpUo_Nome2, AV26UnidadeOrganizacional_VinculadaNom2, AV28DynamicFiltersSelector3, AV29DynamicFiltersOperator3, AV30UnidadeOrganizacional_Nome3, AV31TpUo_Nome3, AV32UnidadeOrganizacional_VinculadaNom3, AV21DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV7Estado_UF, AV47Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, AV6WWPContext, A611UnidadeOrganizacional_Codigo, A609TpUo_Codigo, A613UnidadeOrganizacional_Vinculada, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV28DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E21P42( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV29DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E16P42( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("geral_unidadeorganizacional.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavUnidadeorganizacional_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome1_Visible), 5, 0)));
         edtavTpuo_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTpuo_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpuo_nome1_Visible), 5, 0)));
         edtavUnidadeorganizacional_vinculadanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_vinculadanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_vinculadanom1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 )
         {
            edtavUnidadeorganizacional_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TPUO_NOME") == 0 )
         {
            edtavTpuo_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTpuo_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpuo_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 )
         {
            edtavUnidadeorganizacional_vinculadanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_vinculadanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_vinculadanom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavUnidadeorganizacional_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome2_Visible), 5, 0)));
         edtavTpuo_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTpuo_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpuo_nome2_Visible), 5, 0)));
         edtavUnidadeorganizacional_vinculadanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_vinculadanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_vinculadanom2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 )
         {
            edtavUnidadeorganizacional_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "TPUO_NOME") == 0 )
         {
            edtavTpuo_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTpuo_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpuo_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 )
         {
            edtavUnidadeorganizacional_vinculadanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_vinculadanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_vinculadanom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavUnidadeorganizacional_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome3_Visible), 5, 0)));
         edtavTpuo_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTpuo_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpuo_nome3_Visible), 5, 0)));
         edtavUnidadeorganizacional_vinculadanom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_vinculadanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_vinculadanom3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 )
         {
            edtavUnidadeorganizacional_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "TPUO_NOME") == 0 )
         {
            edtavTpuo_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTpuo_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpuo_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 )
         {
            edtavUnidadeorganizacional_vinculadanom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_vinculadanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_vinculadanom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV21DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
         AV22DynamicFiltersSelector2 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
         AV23DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)));
         AV24UnidadeOrganizacional_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24UnidadeOrganizacional_Nome2", AV24UnidadeOrganizacional_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersEnabled3", AV27DynamicFiltersEnabled3);
         AV28DynamicFiltersSelector3 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
         AV29DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)));
         AV30UnidadeOrganizacional_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30UnidadeOrganizacional_Nome3", AV30UnidadeOrganizacional_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV38Session.Get(AV47Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV47Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV38Session.Get(AV47Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18UnidadeOrganizacional_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18UnidadeOrganizacional_Nome1", AV18UnidadeOrganizacional_Nome1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TPUO_NOME") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV19TpUo_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TpUo_Nome1", AV19TpUo_Nome1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV20UnidadeOrganizacional_VinculadaNom1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UnidadeOrganizacional_VinculadaNom1", AV20UnidadeOrganizacional_VinculadaNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV21DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV22DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 )
               {
                  AV23DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)));
                  AV24UnidadeOrganizacional_Nome2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24UnidadeOrganizacional_Nome2", AV24UnidadeOrganizacional_Nome2);
               }
               else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "TPUO_NOME") == 0 )
               {
                  AV23DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)));
                  AV25TpUo_Nome2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TpUo_Nome2", AV25TpUo_Nome2);
               }
               else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 )
               {
                  AV23DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)));
                  AV26UnidadeOrganizacional_VinculadaNom2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26UnidadeOrganizacional_VinculadaNom2", AV26UnidadeOrganizacional_VinculadaNom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV27DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersEnabled3", AV27DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV28DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 )
                  {
                     AV29DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)));
                     AV30UnidadeOrganizacional_Nome3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30UnidadeOrganizacional_Nome3", AV30UnidadeOrganizacional_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "TPUO_NOME") == 0 )
                  {
                     AV29DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)));
                     AV31TpUo_Nome3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TpUo_Nome3", AV31TpUo_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 )
                  {
                     AV29DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)));
                     AV32UnidadeOrganizacional_VinculadaNom3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32UnidadeOrganizacional_VinculadaNom3", AV32UnidadeOrganizacional_VinculadaNom3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV33DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV38Session.Get(AV47Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV47Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV34DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18UnidadeOrganizacional_Nome1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV18UnidadeOrganizacional_Nome1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TPUO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TpUo_Nome1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV19TpUo_Nome1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV20UnidadeOrganizacional_VinculadaNom1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV20UnidadeOrganizacional_VinculadaNom1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            if ( AV33DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV21DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV22DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24UnidadeOrganizacional_Nome2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV24UnidadeOrganizacional_Nome2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV23DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "TPUO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TpUo_Nome2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV25TpUo_Nome2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV23DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV26UnidadeOrganizacional_VinculadaNom2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV26UnidadeOrganizacional_VinculadaNom2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV23DynamicFiltersOperator2;
            }
            if ( AV33DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV27DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV28DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV30UnidadeOrganizacional_Nome3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV30UnidadeOrganizacional_Nome3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV29DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "TPUO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TpUo_Nome3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV31TpUo_Nome3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV29DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV32UnidadeOrganizacional_VinculadaNom3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV32UnidadeOrganizacional_VinculadaNom3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV29DynamicFiltersOperator3;
            }
            if ( AV33DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV47Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Geral_UnidadeOrganizacional";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Estado_UF";
         AV10TrnContextAtt.gxTpr_Attributevalue = AV7Estado_UF;
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV38Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_P42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_P42( true) ;
         }
         else
         {
            wb_table2_8_P42( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_P42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_84_P42( true) ;
         }
         else
         {
            wb_table3_84_P42( false) ;
         }
         return  ;
      }

      protected void wb_table3_84_P42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_P42e( true) ;
         }
         else
         {
            wb_table1_2_P42e( false) ;
         }
      }

      protected void wb_table3_84_P42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"87\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Organizacional_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUnidadeOrganizacional_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtUnidadeOrganizacional_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUnidadeOrganizacional_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Tipo UO") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTpUo_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtTpUo_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTpUo_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "UO Vinculada") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUnidadeOrganizacional_VinculadaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtUnidadeOrganizacional_VinculadaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUnidadeOrganizacional_VinculadaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "de depend�ncias") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkUnidadeOrganizacional_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkUnidadeOrganizacional_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkUnidadeOrganizacional_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV35Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV36Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV37Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A612UnidadeOrganizacional_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUnidadeOrganizacional_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUnidadeOrganizacional_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtUnidadeOrganizacional_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A609TpUo_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A610TpUo_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTpUo_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTpUo_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtTpUo_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1082UnidadeOrganizacional_VinculadaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUnidadeOrganizacional_VinculadaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUnidadeOrganizacional_VinculadaNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtUnidadeOrganizacional_VinculadaNom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1174UnidadeOrganizacinal_Arvore));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A629UnidadeOrganizacional_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkUnidadeOrganizacional_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkUnidadeOrganizacional_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 87 )
         {
            wbEnd = 0;
            nRC_GXsfl_87 = (short)(nGXsfl_87_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_84_P42e( true) ;
         }
         else
         {
            wb_table3_84_P42e( false) ;
         }
      }

      protected void wb_table2_8_P42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table4_11_P42( true) ;
         }
         else
         {
            wb_table4_11_P42( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_P42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_21_P42( true) ;
         }
         else
         {
            wb_table5_21_P42( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_P42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_P42e( true) ;
         }
         else
         {
            wb_table2_8_P42e( false) ;
         }
      }

      protected void wb_table5_21_P42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_24_P42( true) ;
         }
         else
         {
            wb_table6_24_P42( false) ;
         }
         return  ;
      }

      protected void wb_table6_24_P42e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_P42e( true) ;
         }
         else
         {
            wb_table5_21_P42e( false) ;
         }
      }

      protected void wb_table6_24_P42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", true, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_33_P42( true) ;
         }
         else
         {
            wb_table7_33_P42( false) ;
         }
         return  ;
      }

      protected void wb_table7_33_P42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV22DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_52_P42( true) ;
         }
         else
         {
            wb_table8_52_P42( false) ;
         }
         return  ;
      }

      protected void wb_table8_52_P42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV28DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV28DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_71_P42( true) ;
         }
         else
         {
            wb_table9_71_P42( false) ;
         }
         return  ;
      }

      protected void wb_table9_71_P42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_24_P42e( true) ;
         }
         else
         {
            wb_table6_24_P42e( false) ;
         }
      }

      protected void wb_table9_71_P42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidadeorganizacional_nome3_Internalname, StringUtil.RTrim( AV30UnidadeOrganizacional_Nome3), StringUtil.RTrim( context.localUtil.Format( AV30UnidadeOrganizacional_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,76);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidadeorganizacional_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUnidadeorganizacional_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTpuo_nome3_Internalname, StringUtil.RTrim( AV31TpUo_Nome3), StringUtil.RTrim( context.localUtil.Format( AV31TpUo_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,77);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTpuo_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTpuo_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidadeorganizacional_vinculadanom3_Internalname, StringUtil.RTrim( AV32UnidadeOrganizacional_VinculadaNom3), StringUtil.RTrim( context.localUtil.Format( AV32UnidadeOrganizacional_VinculadaNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,78);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidadeorganizacional_vinculadanom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUnidadeorganizacional_vinculadanom3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_71_P42e( true) ;
         }
         else
         {
            wb_table9_71_P42e( false) ;
         }
      }

      protected void wb_table8_52_P42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "", true, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidadeorganizacional_nome2_Internalname, StringUtil.RTrim( AV24UnidadeOrganizacional_Nome2), StringUtil.RTrim( context.localUtil.Format( AV24UnidadeOrganizacional_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,57);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidadeorganizacional_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUnidadeorganizacional_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTpuo_nome2_Internalname, StringUtil.RTrim( AV25TpUo_Nome2), StringUtil.RTrim( context.localUtil.Format( AV25TpUo_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,58);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTpuo_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTpuo_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidadeorganizacional_vinculadanom2_Internalname, StringUtil.RTrim( AV26UnidadeOrganizacional_VinculadaNom2), StringUtil.RTrim( context.localUtil.Format( AV26UnidadeOrganizacional_VinculadaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,59);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidadeorganizacional_vinculadanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUnidadeorganizacional_vinculadanom2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_52_P42e( true) ;
         }
         else
         {
            wb_table8_52_P42e( false) ;
         }
      }

      protected void wb_table7_33_P42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidadeorganizacional_nome1_Internalname, StringUtil.RTrim( AV18UnidadeOrganizacional_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18UnidadeOrganizacional_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidadeorganizacional_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUnidadeorganizacional_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTpuo_nome1_Internalname, StringUtil.RTrim( AV19TpUo_Nome1), StringUtil.RTrim( context.localUtil.Format( AV19TpUo_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,39);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTpuo_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTpuo_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidadeorganizacional_vinculadanom1_Internalname, StringUtil.RTrim( AV20UnidadeOrganizacional_VinculadaNom1), StringUtil.RTrim( context.localUtil.Format( AV20UnidadeOrganizacional_VinculadaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,40);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidadeorganizacional_vinculadanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUnidadeorganizacional_vinculadanom1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_33_P42e( true) ;
         }
         else
         {
            wb_table7_33_P42e( false) ;
         }
      }

      protected void wb_table4_11_P42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_EstadoGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_P42e( true) ;
         }
         else
         {
            wb_table4_11_P42e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Estado_UF = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Estado_UF", AV7Estado_UF);
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAP42( ) ;
         WSP42( ) ;
         WEP42( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Estado_UF = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAP42( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "estadogeral_unidadeorganizacionalwc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAP42( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Estado_UF = (String)getParm(obj,2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Estado_UF", AV7Estado_UF);
         }
         wcpOAV7Estado_UF = cgiGet( sPrefix+"wcpOAV7Estado_UF");
         if ( ! GetJustCreated( ) && ( ( StringUtil.StrCmp(AV7Estado_UF, wcpOAV7Estado_UF) != 0 ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Estado_UF = AV7Estado_UF;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Estado_UF = cgiGet( sPrefix+"AV7Estado_UF_CTRL");
         if ( StringUtil.Len( sCtrlAV7Estado_UF) > 0 )
         {
            AV7Estado_UF = cgiGet( sCtrlAV7Estado_UF);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Estado_UF", AV7Estado_UF);
         }
         else
         {
            AV7Estado_UF = cgiGet( sPrefix+"AV7Estado_UF_PARM");
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAP42( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSP42( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSP42( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Estado_UF_PARM", StringUtil.RTrim( AV7Estado_UF));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Estado_UF)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Estado_UF_CTRL", StringUtil.RTrim( sCtrlAV7Estado_UF));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEP42( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311722453");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("estadogeral_unidadeorganizacionalwc.js", "?2020311722453");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_872( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_87_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_87_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_87_idx;
         edtUnidadeOrganizacional_Codigo_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_CODIGO_"+sGXsfl_87_idx;
         edtUnidadeOrganizacional_Nome_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_NOME_"+sGXsfl_87_idx;
         edtTpUo_Codigo_Internalname = sPrefix+"TPUO_CODIGO_"+sGXsfl_87_idx;
         edtTpUo_Nome_Internalname = sPrefix+"TPUO_NOME_"+sGXsfl_87_idx;
         edtUnidadeOrganizacional_Vinculada_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_VINCULADA_"+sGXsfl_87_idx;
         edtUnidadeOrganizacional_VinculadaNom_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_VINCULADANOM_"+sGXsfl_87_idx;
         edtUnidadeOrganizacinal_Arvore_Internalname = sPrefix+"UNIDADEORGANIZACINAL_ARVORE_"+sGXsfl_87_idx;
         chkUnidadeOrganizacional_Ativo_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_ATIVO_"+sGXsfl_87_idx;
      }

      protected void SubsflControlProps_fel_872( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_87_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_87_fel_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_87_fel_idx;
         edtUnidadeOrganizacional_Codigo_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_CODIGO_"+sGXsfl_87_fel_idx;
         edtUnidadeOrganizacional_Nome_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_NOME_"+sGXsfl_87_fel_idx;
         edtTpUo_Codigo_Internalname = sPrefix+"TPUO_CODIGO_"+sGXsfl_87_fel_idx;
         edtTpUo_Nome_Internalname = sPrefix+"TPUO_NOME_"+sGXsfl_87_fel_idx;
         edtUnidadeOrganizacional_Vinculada_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_VINCULADA_"+sGXsfl_87_fel_idx;
         edtUnidadeOrganizacional_VinculadaNom_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_VINCULADANOM_"+sGXsfl_87_fel_idx;
         edtUnidadeOrganizacinal_Arvore_Internalname = sPrefix+"UNIDADEORGANIZACINAL_ARVORE_"+sGXsfl_87_fel_idx;
         chkUnidadeOrganizacional_Ativo_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_ATIVO_"+sGXsfl_87_fel_idx;
      }

      protected void sendrow_872( )
      {
         SubsflControlProps_872( ) ;
         WBP40( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_87_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_87_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_87_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV35Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV35Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV44Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV35Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV35Update)) ? AV44Update_GXI : context.PathToRelativeUrl( AV35Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV35Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV36Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV36Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV45Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV36Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV36Delete)) ? AV45Delete_GXI : context.PathToRelativeUrl( AV36Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV36Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV37Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV37Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV46Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV37Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV37Display)) ? AV46Display_GXI : context.PathToRelativeUrl( AV37Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV37Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUnidadeOrganizacional_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A611UnidadeOrganizacional_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUnidadeOrganizacional_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUnidadeOrganizacional_Nome_Internalname,StringUtil.RTrim( A612UnidadeOrganizacional_Nome),StringUtil.RTrim( context.localUtil.Format( A612UnidadeOrganizacional_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtUnidadeOrganizacional_Nome_Link,(String)"",(String)"",(String)"",(String)edtUnidadeOrganizacional_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTpUo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A609TpUo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A609TpUo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTpUo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTpUo_Nome_Internalname,StringUtil.RTrim( A610TpUo_Nome),StringUtil.RTrim( context.localUtil.Format( A610TpUo_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtTpUo_Nome_Link,(String)"",(String)"",(String)"",(String)edtTpUo_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUnidadeOrganizacional_Vinculada_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A613UnidadeOrganizacional_Vinculada), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUnidadeOrganizacional_Vinculada_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUnidadeOrganizacional_VinculadaNom_Internalname,StringUtil.RTrim( A1082UnidadeOrganizacional_VinculadaNom),StringUtil.RTrim( context.localUtil.Format( A1082UnidadeOrganizacional_VinculadaNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtUnidadeOrganizacional_VinculadaNom_Link,(String)"",(String)"",(String)"",(String)edtUnidadeOrganizacional_VinculadaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUnidadeOrganizacinal_Arvore_Internalname,StringUtil.RTrim( A1174UnidadeOrganizacinal_Arvore),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUnidadeOrganizacinal_Arvore_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)1000,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkUnidadeOrganizacional_Ativo_Internalname,StringUtil.BoolToStr( A629UnidadeOrganizacional_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACIONAL_CODIGO"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sPrefix+sGXsfl_87_idx, context.localUtil.Format( (decimal)(A611UnidadeOrganizacional_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACIONAL_NOME"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sPrefix+sGXsfl_87_idx, StringUtil.RTrim( context.localUtil.Format( A612UnidadeOrganizacional_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TPUO_CODIGO"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sPrefix+sGXsfl_87_idx, context.localUtil.Format( (decimal)(A609TpUo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACIONAL_VINCULADA"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sPrefix+sGXsfl_87_idx, context.localUtil.Format( (decimal)(A613UnidadeOrganizacional_Vinculada), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACINAL_ARVORE"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sPrefix+sGXsfl_87_idx, StringUtil.RTrim( context.localUtil.Format( A1174UnidadeOrganizacinal_Arvore, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACIONAL_ATIVO"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sPrefix+sGXsfl_87_idx, A629UnidadeOrganizacional_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_87_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_87_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_87_idx+1));
            sGXsfl_87_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_87_idx), 4, 0)), 4, "0");
            SubsflControlProps_872( ) ;
         }
         /* End function sendrow_872 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR1";
         edtavUnidadeorganizacional_nome1_Internalname = sPrefix+"vUNIDADEORGANIZACIONAL_NOME1";
         edtavTpuo_nome1_Internalname = sPrefix+"vTPUO_NOME1";
         edtavUnidadeorganizacional_vinculadanom1_Internalname = sPrefix+"vUNIDADEORGANIZACIONAL_VINCULADANOM1";
         tblTablemergeddynamicfilters1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR2";
         edtavUnidadeorganizacional_nome2_Internalname = sPrefix+"vUNIDADEORGANIZACIONAL_NOME2";
         edtavTpuo_nome2_Internalname = sPrefix+"vTPUO_NOME2";
         edtavUnidadeorganizacional_vinculadanom2_Internalname = sPrefix+"vUNIDADEORGANIZACIONAL_VINCULADANOM2";
         tblTablemergeddynamicfilters2_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = sPrefix+"ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = sPrefix+"DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR3";
         edtavUnidadeorganizacional_nome3_Internalname = sPrefix+"vUNIDADEORGANIZACIONAL_NOME3";
         edtavTpuo_nome3_Internalname = sPrefix+"vTPUO_NOME3";
         edtavUnidadeorganizacional_vinculadanom3_Internalname = sPrefix+"vUNIDADEORGANIZACIONAL_VINCULADANOM3";
         tblTablemergeddynamicfilters3_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = sPrefix+"REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtavDisplay_Internalname = sPrefix+"vDISPLAY";
         edtUnidadeOrganizacional_Codigo_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_CODIGO";
         edtUnidadeOrganizacional_Nome_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_NOME";
         edtTpUo_Codigo_Internalname = sPrefix+"TPUO_CODIGO";
         edtTpUo_Nome_Internalname = sPrefix+"TPUO_NOME";
         edtUnidadeOrganizacional_Vinculada_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_VINCULADA";
         edtUnidadeOrganizacional_VinculadaNom_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_VINCULADANOM";
         edtUnidadeOrganizacinal_Arvore_Internalname = sPrefix+"UNIDADEORGANIZACINAL_ARVORE";
         chkUnidadeOrganizacional_Ativo_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_ATIVO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtEstado_UF_Internalname = sPrefix+"ESTADO_UF";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = sPrefix+"vDYNAMICFILTERSENABLED3";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtUnidadeOrganizacinal_Arvore_Jsonclick = "";
         edtUnidadeOrganizacional_VinculadaNom_Jsonclick = "";
         edtUnidadeOrganizacional_Vinculada_Jsonclick = "";
         edtTpUo_Nome_Jsonclick = "";
         edtTpUo_Codigo_Jsonclick = "";
         edtUnidadeOrganizacional_Nome_Jsonclick = "";
         edtUnidadeOrganizacional_Codigo_Jsonclick = "";
         edtavUnidadeorganizacional_vinculadanom1_Jsonclick = "";
         edtavTpuo_nome1_Jsonclick = "";
         edtavUnidadeorganizacional_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavUnidadeorganizacional_vinculadanom2_Jsonclick = "";
         edtavTpuo_nome2_Jsonclick = "";
         edtavUnidadeorganizacional_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavUnidadeorganizacional_vinculadanom3_Jsonclick = "";
         edtavTpuo_nome3_Jsonclick = "";
         edtavUnidadeorganizacional_nome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtUnidadeOrganizacional_VinculadaNom_Link = "";
         edtTpUo_Nome_Link = "";
         edtUnidadeOrganizacional_Nome_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         chkUnidadeOrganizacional_Ativo_Titleformat = 0;
         edtUnidadeOrganizacional_VinculadaNom_Titleformat = 0;
         edtTpUo_Nome_Titleformat = 0;
         edtUnidadeOrganizacional_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavUnidadeorganizacional_vinculadanom3_Visible = 1;
         edtavTpuo_nome3_Visible = 1;
         edtavUnidadeorganizacional_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavUnidadeorganizacional_vinculadanom2_Visible = 1;
         edtavTpuo_nome2_Visible = 1;
         edtavUnidadeorganizacional_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavUnidadeorganizacional_vinculadanom1_Visible = 1;
         edtavTpuo_nome1_Visible = 1;
         edtavUnidadeorganizacional_nome1_Visible = 1;
         chkUnidadeOrganizacional_Ativo.Title.Text = "Ativa?";
         edtUnidadeOrganizacional_VinculadaNom_Title = "Depend�ncia";
         edtTpUo_Nome_Title = "Tp Uo_Nome";
         edtUnidadeOrganizacional_Nome_Title = "Organizacional";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkUnidadeOrganizacional_Ativo.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtEstado_UF_Jsonclick = "";
         edtEstado_UF_Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7Estado_UF',fld:'vESTADO_UF',pic:'@!',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A613UnidadeOrganizacional_Vinculada',fld:'UNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV47Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19TpUo_Nome1',fld:'vTPUO_NOME1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_VinculadaNom1',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM1',pic:'@!',nv:''},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25TpUo_Nome2',fld:'vTPUO_NOME2',pic:'@!',nv:''},{av:'AV26UnidadeOrganizacional_VinculadaNom2',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM2',pic:'@!',nv:''},{av:'AV30UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV31TpUo_Nome3',fld:'vTPUO_NOME3',pic:'@!',nv:''},{av:'AV32UnidadeOrganizacional_VinculadaNom3',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM3',pic:'@!',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtUnidadeOrganizacional_Nome_Titleformat',ctrl:'UNIDADEORGANIZACIONAL_NOME',prop:'Titleformat'},{av:'edtUnidadeOrganizacional_Nome_Title',ctrl:'UNIDADEORGANIZACIONAL_NOME',prop:'Title'},{av:'edtTpUo_Nome_Titleformat',ctrl:'TPUO_NOME',prop:'Titleformat'},{av:'edtTpUo_Nome_Title',ctrl:'TPUO_NOME',prop:'Title'},{av:'edtUnidadeOrganizacional_VinculadaNom_Titleformat',ctrl:'UNIDADEORGANIZACIONAL_VINCULADANOM',prop:'Titleformat'},{av:'edtUnidadeOrganizacional_VinculadaNom_Title',ctrl:'UNIDADEORGANIZACIONAL_VINCULADANOM',prop:'Title'},{av:'chkUnidadeOrganizacional_Ativo_Titleformat',ctrl:'UNIDADEORGANIZACIONAL_ATIVO',prop:'Titleformat'},{av:'chkUnidadeOrganizacional_Ativo.Title.Text',ctrl:'UNIDADEORGANIZACIONAL_ATIVO',prop:'Title'},{av:'AV40GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV41GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11P42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19TpUo_Nome1',fld:'vTPUO_NOME1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_VinculadaNom1',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV25TpUo_Nome2',fld:'vTPUO_NOME2',pic:'@!',nv:''},{av:'AV26UnidadeOrganizacional_VinculadaNom2',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM2',pic:'@!',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV31TpUo_Nome3',fld:'vTPUO_NOME3',pic:'@!',nv:''},{av:'AV32UnidadeOrganizacional_VinculadaNom3',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Estado_UF',fld:'vESTADO_UF',pic:'@!',nv:''},{av:'AV47Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A613UnidadeOrganizacional_Vinculada',fld:'UNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E24P42',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A613UnidadeOrganizacional_Vinculada',fld:'UNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV35Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV36Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV37Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'edtUnidadeOrganizacional_Nome_Link',ctrl:'UNIDADEORGANIZACIONAL_NOME',prop:'Link'},{av:'edtTpUo_Nome_Link',ctrl:'TPUO_NOME',prop:'Link'},{av:'edtUnidadeOrganizacional_VinculadaNom_Link',ctrl:'UNIDADEORGANIZACIONAL_VINCULADANOM',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E12P42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19TpUo_Nome1',fld:'vTPUO_NOME1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_VinculadaNom1',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV25TpUo_Nome2',fld:'vTPUO_NOME2',pic:'@!',nv:''},{av:'AV26UnidadeOrganizacional_VinculadaNom2',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM2',pic:'@!',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV31TpUo_Nome3',fld:'vTPUO_NOME3',pic:'@!',nv:''},{av:'AV32UnidadeOrganizacional_VinculadaNom3',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Estado_UF',fld:'vESTADO_UF',pic:'@!',nv:''},{av:'AV47Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A613UnidadeOrganizacional_Vinculada',fld:'UNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E17P42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19TpUo_Nome1',fld:'vTPUO_NOME1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_VinculadaNom1',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV25TpUo_Nome2',fld:'vTPUO_NOME2',pic:'@!',nv:''},{av:'AV26UnidadeOrganizacional_VinculadaNom2',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM2',pic:'@!',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV31TpUo_Nome3',fld:'vTPUO_NOME3',pic:'@!',nv:''},{av:'AV32UnidadeOrganizacional_VinculadaNom3',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Estado_UF',fld:'vESTADO_UF',pic:'@!',nv:''},{av:'AV47Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A613UnidadeOrganizacional_Vinculada',fld:'UNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E13P42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19TpUo_Nome1',fld:'vTPUO_NOME1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_VinculadaNom1',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV25TpUo_Nome2',fld:'vTPUO_NOME2',pic:'@!',nv:''},{av:'AV26UnidadeOrganizacional_VinculadaNom2',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM2',pic:'@!',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV31TpUo_Nome3',fld:'vTPUO_NOME3',pic:'@!',nv:''},{av:'AV32UnidadeOrganizacional_VinculadaNom3',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Estado_UF',fld:'vESTADO_UF',pic:'@!',nv:''},{av:'AV47Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A613UnidadeOrganizacional_Vinculada',fld:'UNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19TpUo_Nome1',fld:'vTPUO_NOME1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_VinculadaNom1',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV25TpUo_Nome2',fld:'vTPUO_NOME2',pic:'@!',nv:''},{av:'AV26UnidadeOrganizacional_VinculadaNom2',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM2',pic:'@!',nv:''},{av:'AV31TpUo_Nome3',fld:'vTPUO_NOME3',pic:'@!',nv:''},{av:'AV32UnidadeOrganizacional_VinculadaNom3',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM3',pic:'@!',nv:''},{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'},{av:'edtavTpuo_nome2_Visible',ctrl:'vTPUO_NOME2',prop:'Visible'},{av:'edtavUnidadeorganizacional_vinculadanom2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_VINCULADANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'},{av:'edtavTpuo_nome3_Visible',ctrl:'vTPUO_NOME3',prop:'Visible'},{av:'edtavUnidadeorganizacional_vinculadanom3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_VINCULADANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'},{av:'edtavTpuo_nome1_Visible',ctrl:'vTPUO_NOME1',prop:'Visible'},{av:'edtavUnidadeorganizacional_vinculadanom1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_VINCULADANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E18P42',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'},{av:'edtavTpuo_nome1_Visible',ctrl:'vTPUO_NOME1',prop:'Visible'},{av:'edtavUnidadeorganizacional_vinculadanom1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_VINCULADANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E19P42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19TpUo_Nome1',fld:'vTPUO_NOME1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_VinculadaNom1',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV25TpUo_Nome2',fld:'vTPUO_NOME2',pic:'@!',nv:''},{av:'AV26UnidadeOrganizacional_VinculadaNom2',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM2',pic:'@!',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV31TpUo_Nome3',fld:'vTPUO_NOME3',pic:'@!',nv:''},{av:'AV32UnidadeOrganizacional_VinculadaNom3',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Estado_UF',fld:'vESTADO_UF',pic:'@!',nv:''},{av:'AV47Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A613UnidadeOrganizacional_Vinculada',fld:'UNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E14P42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19TpUo_Nome1',fld:'vTPUO_NOME1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_VinculadaNom1',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV25TpUo_Nome2',fld:'vTPUO_NOME2',pic:'@!',nv:''},{av:'AV26UnidadeOrganizacional_VinculadaNom2',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM2',pic:'@!',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV31TpUo_Nome3',fld:'vTPUO_NOME3',pic:'@!',nv:''},{av:'AV32UnidadeOrganizacional_VinculadaNom3',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Estado_UF',fld:'vESTADO_UF',pic:'@!',nv:''},{av:'AV47Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A613UnidadeOrganizacional_Vinculada',fld:'UNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19TpUo_Nome1',fld:'vTPUO_NOME1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_VinculadaNom1',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV25TpUo_Nome2',fld:'vTPUO_NOME2',pic:'@!',nv:''},{av:'AV26UnidadeOrganizacional_VinculadaNom2',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM2',pic:'@!',nv:''},{av:'AV31TpUo_Nome3',fld:'vTPUO_NOME3',pic:'@!',nv:''},{av:'AV32UnidadeOrganizacional_VinculadaNom3',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM3',pic:'@!',nv:''},{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'},{av:'edtavTpuo_nome2_Visible',ctrl:'vTPUO_NOME2',prop:'Visible'},{av:'edtavUnidadeorganizacional_vinculadanom2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_VINCULADANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'},{av:'edtavTpuo_nome3_Visible',ctrl:'vTPUO_NOME3',prop:'Visible'},{av:'edtavUnidadeorganizacional_vinculadanom3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_VINCULADANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'},{av:'edtavTpuo_nome1_Visible',ctrl:'vTPUO_NOME1',prop:'Visible'},{av:'edtavUnidadeorganizacional_vinculadanom1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_VINCULADANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E20P42',iparms:[{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'},{av:'edtavTpuo_nome2_Visible',ctrl:'vTPUO_NOME2',prop:'Visible'},{av:'edtavUnidadeorganizacional_vinculadanom2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_VINCULADANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E15P42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19TpUo_Nome1',fld:'vTPUO_NOME1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_VinculadaNom1',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM1',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV25TpUo_Nome2',fld:'vTPUO_NOME2',pic:'@!',nv:''},{av:'AV26UnidadeOrganizacional_VinculadaNom2',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM2',pic:'@!',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV31TpUo_Nome3',fld:'vTPUO_NOME3',pic:'@!',nv:''},{av:'AV32UnidadeOrganizacional_VinculadaNom3',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM3',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Estado_UF',fld:'vESTADO_UF',pic:'@!',nv:''},{av:'AV47Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A609TpUo_Codigo',fld:'TPUO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A613UnidadeOrganizacional_Vinculada',fld:'UNIDADEORGANIZACIONAL_VINCULADA',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19TpUo_Nome1',fld:'vTPUO_NOME1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_VinculadaNom1',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV25TpUo_Nome2',fld:'vTPUO_NOME2',pic:'@!',nv:''},{av:'AV26UnidadeOrganizacional_VinculadaNom2',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM2',pic:'@!',nv:''},{av:'AV31TpUo_Nome3',fld:'vTPUO_NOME3',pic:'@!',nv:''},{av:'AV32UnidadeOrganizacional_VinculadaNom3',fld:'vUNIDADEORGANIZACIONAL_VINCULADANOM3',pic:'@!',nv:''},{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'},{av:'edtavTpuo_nome2_Visible',ctrl:'vTPUO_NOME2',prop:'Visible'},{av:'edtavUnidadeorganizacional_vinculadanom2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_VINCULADANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'},{av:'edtavTpuo_nome3_Visible',ctrl:'vTPUO_NOME3',prop:'Visible'},{av:'edtavUnidadeorganizacional_vinculadanom3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_VINCULADANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'},{av:'edtavTpuo_nome1_Visible',ctrl:'vTPUO_NOME1',prop:'Visible'},{av:'edtavUnidadeorganizacional_vinculadanom1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_VINCULADANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E21P42',iparms:[{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV29DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'},{av:'edtavTpuo_nome3_Visible',ctrl:'vTPUO_NOME3',prop:'Visible'},{av:'edtavUnidadeorganizacional_vinculadanom3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_VINCULADANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E16P42',iparms:[{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV7Estado_UF = "";
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV18UnidadeOrganizacional_Nome1 = "";
         AV19TpUo_Nome1 = "";
         AV20UnidadeOrganizacional_VinculadaNom1 = "";
         AV22DynamicFiltersSelector2 = "";
         AV24UnidadeOrganizacional_Nome2 = "";
         AV25TpUo_Nome2 = "";
         AV26UnidadeOrganizacional_VinculadaNom2 = "";
         AV28DynamicFiltersSelector3 = "";
         AV30UnidadeOrganizacional_Nome3 = "";
         AV31TpUo_Nome3 = "";
         AV32UnidadeOrganizacional_VinculadaNom3 = "";
         AV47Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         A23Estado_UF = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV35Update = "";
         AV44Update_GXI = "";
         AV36Delete = "";
         AV45Delete_GXI = "";
         AV37Display = "";
         AV46Display_GXI = "";
         A612UnidadeOrganizacional_Nome = "";
         A610TpUo_Nome = "";
         A1082UnidadeOrganizacional_VinculadaNom = "";
         A1174UnidadeOrganizacinal_Arvore = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV18UnidadeOrganizacional_Nome1 = "";
         lV19TpUo_Nome1 = "";
         lV20UnidadeOrganizacional_VinculadaNom1 = "";
         lV24UnidadeOrganizacional_Nome2 = "";
         lV25TpUo_Nome2 = "";
         lV26UnidadeOrganizacional_VinculadaNom2 = "";
         lV30UnidadeOrganizacional_Nome3 = "";
         lV31TpUo_Nome3 = "";
         lV32UnidadeOrganizacional_VinculadaNom3 = "";
         H00P42_A23Estado_UF = new String[] {""} ;
         H00P42_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         H00P42_A1082UnidadeOrganizacional_VinculadaNom = new String[] {""} ;
         H00P42_n1082UnidadeOrganizacional_VinculadaNom = new bool[] {false} ;
         H00P42_A610TpUo_Nome = new String[] {""} ;
         H00P42_A609TpUo_Codigo = new int[1] ;
         H00P42_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         H00P42_A611UnidadeOrganizacional_Codigo = new int[1] ;
         H00P42_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         H00P42_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         GXt_char1 = "";
         H00P43_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GridRow = new GXWebRow();
         AV38Session = context.GetSession();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Estado_UF = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.estadogeral_unidadeorganizacionalwc__default(),
            new Object[][] {
                new Object[] {
               H00P42_A23Estado_UF, H00P42_A629UnidadeOrganizacional_Ativo, H00P42_A1082UnidadeOrganizacional_VinculadaNom, H00P42_n1082UnidadeOrganizacional_VinculadaNom, H00P42_A610TpUo_Nome, H00P42_A609TpUo_Codigo, H00P42_A612UnidadeOrganizacional_Nome, H00P42_A611UnidadeOrganizacional_Codigo, H00P42_A613UnidadeOrganizacional_Vinculada, H00P42_n613UnidadeOrganizacional_Vinculada
               }
               , new Object[] {
               H00P43_AGRID_nRecordCount
               }
            }
         );
         AV47Pgmname = "EstadoGeral_UnidadeOrganizacionalWC";
         /* GeneXus formulas. */
         AV47Pgmname = "EstadoGeral_UnidadeOrganizacionalWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_87 ;
      private short nGXsfl_87_idx=1 ;
      private short AV14OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV23DynamicFiltersOperator2 ;
      private short AV29DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_87_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtUnidadeOrganizacional_Nome_Titleformat ;
      private short edtTpUo_Nome_Titleformat ;
      private short edtUnidadeOrganizacional_VinculadaNom_Titleformat ;
      private short chkUnidadeOrganizacional_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A611UnidadeOrganizacional_Codigo ;
      private int A609TpUo_Codigo ;
      private int A613UnidadeOrganizacional_Vinculada ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtEstado_UF_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV39PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavUnidadeorganizacional_nome1_Visible ;
      private int edtavTpuo_nome1_Visible ;
      private int edtavUnidadeorganizacional_vinculadanom1_Visible ;
      private int edtavUnidadeorganizacional_nome2_Visible ;
      private int edtavTpuo_nome2_Visible ;
      private int edtavUnidadeorganizacional_vinculadanom2_Visible ;
      private int edtavUnidadeorganizacional_nome3_Visible ;
      private int edtavTpuo_nome3_Visible ;
      private int edtavUnidadeorganizacional_vinculadanom3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV40GridCurrentPage ;
      private long AV41GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String AV7Estado_UF ;
      private String wcpOAV7Estado_UF ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_87_idx="0001" ;
      private String AV18UnidadeOrganizacional_Nome1 ;
      private String AV19TpUo_Nome1 ;
      private String AV20UnidadeOrganizacional_VinculadaNom1 ;
      private String AV24UnidadeOrganizacional_Nome2 ;
      private String AV25TpUo_Nome2 ;
      private String AV26UnidadeOrganizacional_VinculadaNom2 ;
      private String AV30UnidadeOrganizacional_Nome3 ;
      private String AV31TpUo_Nome3 ;
      private String AV32UnidadeOrganizacional_VinculadaNom3 ;
      private String AV47Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String edtEstado_UF_Internalname ;
      private String A23Estado_UF ;
      private String edtEstado_UF_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtUnidadeOrganizacional_Codigo_Internalname ;
      private String A612UnidadeOrganizacional_Nome ;
      private String edtUnidadeOrganizacional_Nome_Internalname ;
      private String edtTpUo_Codigo_Internalname ;
      private String A610TpUo_Nome ;
      private String edtTpUo_Nome_Internalname ;
      private String edtUnidadeOrganizacional_Vinculada_Internalname ;
      private String A1082UnidadeOrganizacional_VinculadaNom ;
      private String edtUnidadeOrganizacional_VinculadaNom_Internalname ;
      private String A1174UnidadeOrganizacinal_Arvore ;
      private String edtUnidadeOrganizacinal_Arvore_Internalname ;
      private String chkUnidadeOrganizacional_Ativo_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String lV18UnidadeOrganizacional_Nome1 ;
      private String lV19TpUo_Nome1 ;
      private String lV20UnidadeOrganizacional_VinculadaNom1 ;
      private String lV24UnidadeOrganizacional_Nome2 ;
      private String lV25TpUo_Nome2 ;
      private String lV26UnidadeOrganizacional_VinculadaNom2 ;
      private String lV30UnidadeOrganizacional_Nome3 ;
      private String lV31TpUo_Nome3 ;
      private String lV32UnidadeOrganizacional_VinculadaNom3 ;
      private String GXt_char1 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavUnidadeorganizacional_nome1_Internalname ;
      private String edtavTpuo_nome1_Internalname ;
      private String edtavUnidadeorganizacional_vinculadanom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavUnidadeorganizacional_nome2_Internalname ;
      private String edtavTpuo_nome2_Internalname ;
      private String edtavUnidadeorganizacional_vinculadanom2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavUnidadeorganizacional_nome3_Internalname ;
      private String edtavTpuo_nome3_Internalname ;
      private String edtavUnidadeorganizacional_vinculadanom3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String edtUnidadeOrganizacional_Nome_Title ;
      private String edtTpUo_Nome_Title ;
      private String edtUnidadeOrganizacional_VinculadaNom_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtUnidadeOrganizacional_Nome_Link ;
      private String edtTpUo_Nome_Link ;
      private String edtUnidadeOrganizacional_VinculadaNom_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavUnidadeorganizacional_nome3_Jsonclick ;
      private String edtavTpuo_nome3_Jsonclick ;
      private String edtavUnidadeorganizacional_vinculadanom3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavUnidadeorganizacional_nome2_Jsonclick ;
      private String edtavTpuo_nome2_Jsonclick ;
      private String edtavUnidadeorganizacional_vinculadanom2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavUnidadeorganizacional_nome1_Jsonclick ;
      private String edtavTpuo_nome1_Jsonclick ;
      private String edtavUnidadeorganizacional_vinculadanom1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7Estado_UF ;
      private String sGXsfl_87_fel_idx="0001" ;
      private String ROClassString ;
      private String edtUnidadeOrganizacional_Codigo_Jsonclick ;
      private String edtUnidadeOrganizacional_Nome_Jsonclick ;
      private String edtTpUo_Codigo_Jsonclick ;
      private String edtTpUo_Nome_Jsonclick ;
      private String edtUnidadeOrganizacional_Vinculada_Jsonclick ;
      private String edtUnidadeOrganizacional_VinculadaNom_Jsonclick ;
      private String edtUnidadeOrganizacinal_Arvore_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV21DynamicFiltersEnabled2 ;
      private bool AV27DynamicFiltersEnabled3 ;
      private bool AV34DynamicFiltersIgnoreFirst ;
      private bool AV33DynamicFiltersRemoving ;
      private bool n613UnidadeOrganizacional_Vinculada ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1082UnidadeOrganizacional_VinculadaNom ;
      private bool A629UnidadeOrganizacional_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV35Update_IsBlob ;
      private bool AV36Delete_IsBlob ;
      private bool AV37Display_IsBlob ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV22DynamicFiltersSelector2 ;
      private String AV28DynamicFiltersSelector3 ;
      private String AV44Update_GXI ;
      private String AV45Delete_GXI ;
      private String AV46Display_GXI ;
      private String AV35Update ;
      private String AV36Delete ;
      private String AV37Display ;
      private IGxSession AV38Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkUnidadeOrganizacional_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00P42_A23Estado_UF ;
      private bool[] H00P42_A629UnidadeOrganizacional_Ativo ;
      private String[] H00P42_A1082UnidadeOrganizacional_VinculadaNom ;
      private bool[] H00P42_n1082UnidadeOrganizacional_VinculadaNom ;
      private String[] H00P42_A610TpUo_Nome ;
      private int[] H00P42_A609TpUo_Codigo ;
      private String[] H00P42_A612UnidadeOrganizacional_Nome ;
      private int[] H00P42_A611UnidadeOrganizacional_Codigo ;
      private int[] H00P42_A613UnidadeOrganizacional_Vinculada ;
      private bool[] H00P42_n613UnidadeOrganizacional_Vinculada ;
      private long[] H00P43_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
   }

   public class estadogeral_unidadeorganizacionalwc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00P42( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18UnidadeOrganizacional_Nome1 ,
                                             String AV19TpUo_Nome1 ,
                                             String AV20UnidadeOrganizacional_VinculadaNom1 ,
                                             bool AV21DynamicFiltersEnabled2 ,
                                             String AV22DynamicFiltersSelector2 ,
                                             short AV23DynamicFiltersOperator2 ,
                                             String AV24UnidadeOrganizacional_Nome2 ,
                                             String AV25TpUo_Nome2 ,
                                             String AV26UnidadeOrganizacional_VinculadaNom2 ,
                                             bool AV27DynamicFiltersEnabled3 ,
                                             String AV28DynamicFiltersSelector3 ,
                                             short AV29DynamicFiltersOperator3 ,
                                             String AV30UnidadeOrganizacional_Nome3 ,
                                             String AV31TpUo_Nome3 ,
                                             String AV32UnidadeOrganizacional_VinculadaNom3 ,
                                             String A612UnidadeOrganizacional_Nome ,
                                             String A610TpUo_Nome ,
                                             String A1082UnidadeOrganizacional_VinculadaNom ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             String A23Estado_UF ,
                                             String AV7Estado_UF )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [24] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Estado_UF], T1.[UnidadeOrganizacional_Ativo], T3.[UnidadeOrganizacional_Nome] AS UnidadeOrganizacional_VinculadaNom, T2.[TpUo_Nome], T1.[TpUo_Codigo], T1.[UnidadeOrganizacional_Nome], T1.[UnidadeOrganizacional_Codigo], T1.[UnidadeOrganizacional_Vinculada] AS UnidadeOrganizacional_Vinculada";
         sFromString = " FROM (([Geral_UnidadeOrganizacional] T1 WITH (NOLOCK) INNER JOIN [Geral_tp_uo] T2 WITH (NOLOCK) ON T2.[TpUo_Codigo] = T1.[TpUo_Codigo]) LEFT JOIN [Geral_UnidadeOrganizacional] T3 WITH (NOLOCK) ON T3.[UnidadeOrganizacional_Codigo] = T1.[UnidadeOrganizacional_Vinculada])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Estado_UF] = @AV7Estado_UF)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18UnidadeOrganizacional_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like @lV18UnidadeOrganizacional_Nome1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18UnidadeOrganizacional_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV18UnidadeOrganizacional_Nome1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TPUO_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TpUo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[TpUo_Nome] like @lV19TpUo_Nome1)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TPUO_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TpUo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[TpUo_Nome] like '%' + @lV19TpUo_Nome1)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20UnidadeOrganizacional_VinculadaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeOrganizacional_Nome] like @lV20UnidadeOrganizacional_VinculadaNom1)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20UnidadeOrganizacional_VinculadaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeOrganizacional_Nome] like '%' + @lV20UnidadeOrganizacional_VinculadaNom1)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( AV23DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24UnidadeOrganizacional_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like @lV24UnidadeOrganizacional_Nome2)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( AV23DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24UnidadeOrganizacional_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV24UnidadeOrganizacional_Nome2)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "TPUO_NOME") == 0 ) && ( AV23DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TpUo_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[TpUo_Nome] like @lV25TpUo_Nome2)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "TPUO_NOME") == 0 ) && ( AV23DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TpUo_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[TpUo_Nome] like '%' + @lV25TpUo_Nome2)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 ) && ( AV23DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26UnidadeOrganizacional_VinculadaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeOrganizacional_Nome] like @lV26UnidadeOrganizacional_VinculadaNom2)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 ) && ( AV23DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26UnidadeOrganizacional_VinculadaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeOrganizacional_Nome] like '%' + @lV26UnidadeOrganizacional_VinculadaNom2)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( AV29DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30UnidadeOrganizacional_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like @lV30UnidadeOrganizacional_Nome3)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( AV29DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30UnidadeOrganizacional_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV30UnidadeOrganizacional_Nome3)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "TPUO_NOME") == 0 ) && ( AV29DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TpUo_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[TpUo_Nome] like @lV31TpUo_Nome3)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "TPUO_NOME") == 0 ) && ( AV29DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TpUo_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[TpUo_Nome] like '%' + @lV31TpUo_Nome3)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 ) && ( AV29DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32UnidadeOrganizacional_VinculadaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeOrganizacional_Nome] like @lV32UnidadeOrganizacional_VinculadaNom3)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 ) && ( AV29DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32UnidadeOrganizacional_VinculadaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeOrganizacional_Nome] like '%' + @lV32UnidadeOrganizacional_VinculadaNom3)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Estado_UF], T1.[UnidadeOrganizacional_Nome]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Estado_UF] DESC, T1.[UnidadeOrganizacional_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Estado_UF], T2.[TpUo_Nome]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Estado_UF] DESC, T2.[TpUo_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Estado_UF], T3.[UnidadeOrganizacional_Nome]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Estado_UF] DESC, T3.[UnidadeOrganizacional_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Estado_UF], T1.[UnidadeOrganizacional_Ativo]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Estado_UF] DESC, T1.[UnidadeOrganizacional_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[UnidadeOrganizacional_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00P43( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18UnidadeOrganizacional_Nome1 ,
                                             String AV19TpUo_Nome1 ,
                                             String AV20UnidadeOrganizacional_VinculadaNom1 ,
                                             bool AV21DynamicFiltersEnabled2 ,
                                             String AV22DynamicFiltersSelector2 ,
                                             short AV23DynamicFiltersOperator2 ,
                                             String AV24UnidadeOrganizacional_Nome2 ,
                                             String AV25TpUo_Nome2 ,
                                             String AV26UnidadeOrganizacional_VinculadaNom2 ,
                                             bool AV27DynamicFiltersEnabled3 ,
                                             String AV28DynamicFiltersSelector3 ,
                                             short AV29DynamicFiltersOperator3 ,
                                             String AV30UnidadeOrganizacional_Nome3 ,
                                             String AV31TpUo_Nome3 ,
                                             String AV32UnidadeOrganizacional_VinculadaNom3 ,
                                             String A612UnidadeOrganizacional_Nome ,
                                             String A610TpUo_Nome ,
                                             String A1082UnidadeOrganizacional_VinculadaNom ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             String A23Estado_UF ,
                                             String AV7Estado_UF )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [19] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([Geral_UnidadeOrganizacional] T1 WITH (NOLOCK) INNER JOIN [Geral_tp_uo] T2 WITH (NOLOCK) ON T2.[TpUo_Codigo] = T1.[TpUo_Codigo]) LEFT JOIN [Geral_UnidadeOrganizacional] T3 WITH (NOLOCK) ON T3.[UnidadeOrganizacional_Codigo] = T1.[UnidadeOrganizacional_Vinculada])";
         scmdbuf = scmdbuf + " WHERE (T1.[Estado_UF] = @AV7Estado_UF)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18UnidadeOrganizacional_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like @lV18UnidadeOrganizacional_Nome1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18UnidadeOrganizacional_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV18UnidadeOrganizacional_Nome1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TPUO_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TpUo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[TpUo_Nome] like @lV19TpUo_Nome1)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TPUO_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TpUo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[TpUo_Nome] like '%' + @lV19TpUo_Nome1)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20UnidadeOrganizacional_VinculadaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeOrganizacional_Nome] like @lV20UnidadeOrganizacional_VinculadaNom1)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20UnidadeOrganizacional_VinculadaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeOrganizacional_Nome] like '%' + @lV20UnidadeOrganizacional_VinculadaNom1)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( AV23DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24UnidadeOrganizacional_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like @lV24UnidadeOrganizacional_Nome2)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( AV23DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24UnidadeOrganizacional_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV24UnidadeOrganizacional_Nome2)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "TPUO_NOME") == 0 ) && ( AV23DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TpUo_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[TpUo_Nome] like @lV25TpUo_Nome2)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "TPUO_NOME") == 0 ) && ( AV23DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TpUo_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[TpUo_Nome] like '%' + @lV25TpUo_Nome2)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 ) && ( AV23DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26UnidadeOrganizacional_VinculadaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeOrganizacional_Nome] like @lV26UnidadeOrganizacional_VinculadaNom2)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 ) && ( AV23DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26UnidadeOrganizacional_VinculadaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeOrganizacional_Nome] like '%' + @lV26UnidadeOrganizacional_VinculadaNom2)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( AV29DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30UnidadeOrganizacional_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like @lV30UnidadeOrganizacional_Nome3)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( AV29DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30UnidadeOrganizacional_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV30UnidadeOrganizacional_Nome3)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "TPUO_NOME") == 0 ) && ( AV29DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TpUo_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[TpUo_Nome] like @lV31TpUo_Nome3)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "TPUO_NOME") == 0 ) && ( AV29DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TpUo_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[TpUo_Nome] like '%' + @lV31TpUo_Nome3)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 ) && ( AV29DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32UnidadeOrganizacional_VinculadaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeOrganizacional_Nome] like @lV32UnidadeOrganizacional_VinculadaNom3)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( AV27DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADANOM") == 0 ) && ( AV29DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32UnidadeOrganizacional_VinculadaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[UnidadeOrganizacional_Nome] like '%' + @lV32UnidadeOrganizacional_VinculadaNom3)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00P42(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] );
               case 1 :
                     return conditional_H00P43(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00P42 ;
          prmH00P42 = new Object[] {
          new Object[] {"@AV7Estado_UF",SqlDbType.Char,2,0} ,
          new Object[] {"@lV18UnidadeOrganizacional_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18UnidadeOrganizacional_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19TpUo_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19TpUo_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV20UnidadeOrganizacional_VinculadaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV20UnidadeOrganizacional_VinculadaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV24UnidadeOrganizacional_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV24UnidadeOrganizacional_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV25TpUo_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV25TpUo_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV26UnidadeOrganizacional_VinculadaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV26UnidadeOrganizacional_VinculadaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV30UnidadeOrganizacional_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV30UnidadeOrganizacional_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV31TpUo_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV31TpUo_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV32UnidadeOrganizacional_VinculadaNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV32UnidadeOrganizacional_VinculadaNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00P43 ;
          prmH00P43 = new Object[] {
          new Object[] {"@AV7Estado_UF",SqlDbType.Char,2,0} ,
          new Object[] {"@lV18UnidadeOrganizacional_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18UnidadeOrganizacional_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19TpUo_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19TpUo_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV20UnidadeOrganizacional_VinculadaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV20UnidadeOrganizacional_VinculadaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV24UnidadeOrganizacional_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV24UnidadeOrganizacional_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV25TpUo_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV25TpUo_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV26UnidadeOrganizacional_VinculadaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV26UnidadeOrganizacional_VinculadaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV30UnidadeOrganizacional_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV30UnidadeOrganizacional_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV31TpUo_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV31TpUo_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV32UnidadeOrganizacional_VinculadaNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV32UnidadeOrganizacional_VinculadaNom3",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00P42", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00P42,11,0,true,false )
             ,new CursorDef("H00P43", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00P43,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 50) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                return;
       }
    }

 }

}
