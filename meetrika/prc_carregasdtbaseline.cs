/*
               File: PRC_CarregaSDTBaseline
        Description: Stub for PRC_CarregaSDTBaseline
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:55.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_carregasdtbaseline : GXProcedure
   {
      public prc_carregasdtbaseline( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public prc_carregasdtbaseline( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( short aP0_PraLinha ,
                           String aP1_Arquivo ,
                           String aP2_Aba ,
                           short aP3_ColNomen ,
                           short aP4_ColTipon ,
                           short aP5_ColDERn ,
                           short aP6_ColRLRn ,
                           short aP7_ColObservn ,
                           String aP8_FileName ,
                           int aP9_Sistema_Codigo ,
                           String aP10_Sistema_Sigla )
      {
         this.AV2PraLinha = aP0_PraLinha;
         this.AV3Arquivo = aP1_Arquivo;
         this.AV4Aba = aP2_Aba;
         this.AV5ColNomen = aP3_ColNomen;
         this.AV6ColTipon = aP4_ColTipon;
         this.AV7ColDERn = aP5_ColDERn;
         this.AV8ColRLRn = aP6_ColRLRn;
         this.AV9ColObservn = aP7_ColObservn;
         this.AV10FileName = aP8_FileName;
         this.AV11Sistema_Codigo = aP9_Sistema_Codigo;
         this.AV12Sistema_Sigla = aP10_Sistema_Sigla;
         initialize();
         executePrivate();
      }

      public void executeSubmit( short aP0_PraLinha ,
                                 String aP1_Arquivo ,
                                 String aP2_Aba ,
                                 short aP3_ColNomen ,
                                 short aP4_ColTipon ,
                                 short aP5_ColDERn ,
                                 short aP6_ColRLRn ,
                                 short aP7_ColObservn ,
                                 String aP8_FileName ,
                                 int aP9_Sistema_Codigo ,
                                 String aP10_Sistema_Sigla )
      {
         prc_carregasdtbaseline objprc_carregasdtbaseline;
         objprc_carregasdtbaseline = new prc_carregasdtbaseline();
         objprc_carregasdtbaseline.AV2PraLinha = aP0_PraLinha;
         objprc_carregasdtbaseline.AV3Arquivo = aP1_Arquivo;
         objprc_carregasdtbaseline.AV4Aba = aP2_Aba;
         objprc_carregasdtbaseline.AV5ColNomen = aP3_ColNomen;
         objprc_carregasdtbaseline.AV6ColTipon = aP4_ColTipon;
         objprc_carregasdtbaseline.AV7ColDERn = aP5_ColDERn;
         objprc_carregasdtbaseline.AV8ColRLRn = aP6_ColRLRn;
         objprc_carregasdtbaseline.AV9ColObservn = aP7_ColObservn;
         objprc_carregasdtbaseline.AV10FileName = aP8_FileName;
         objprc_carregasdtbaseline.AV11Sistema_Codigo = aP9_Sistema_Codigo;
         objprc_carregasdtbaseline.AV12Sistema_Sigla = aP10_Sistema_Sigla;
         objprc_carregasdtbaseline.context.SetSubmitInitialConfig(context);
         objprc_carregasdtbaseline.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_carregasdtbaseline);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_carregasdtbaseline)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(short)AV2PraLinha,(String)AV3Arquivo,(String)AV4Aba,(short)AV5ColNomen,(short)AV6ColTipon,(short)AV7ColDERn,(short)AV8ColRLRn,(short)AV9ColObservn,(String)AV10FileName,(int)AV11Sistema_Codigo,(String)AV12Sistema_Sigla} ;
         ClassLoader.Execute("aprc_carregasdtbaseline","GeneXus.Programs.aprc_carregasdtbaseline", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 11 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV2PraLinha ;
      private short AV5ColNomen ;
      private short AV6ColTipon ;
      private short AV7ColDERn ;
      private short AV8ColRLRn ;
      private short AV9ColObservn ;
      private int AV11Sistema_Codigo ;
      private String AV3Arquivo ;
      private String AV4Aba ;
      private String AV10FileName ;
      private String AV12Sistema_Sigla ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
