/*
               File: type_SdtPurchaseReceiptInformation
        Description: PurchaseReceiptInformation
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:57.10
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "PurchaseReceiptInformation" )]
   [XmlType(TypeName =  "PurchaseReceiptInformation" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtPurchaseReceiptInformation : GxUserType
   {
      public SdtPurchaseReceiptInformation( )
      {
         /* Constructor for serialization */
         gxTv_SdtPurchaseReceiptInformation_Productid = "";
         gxTv_SdtPurchaseReceiptInformation_Transactionid = "";
         gxTv_SdtPurchaseReceiptInformation_Purchasedate = (DateTime)(DateTime.MinValue);
         gxTv_SdtPurchaseReceiptInformation_Originaltransactionid = "";
         gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate = (DateTime)(DateTime.MinValue);
         gxTv_SdtPurchaseReceiptInformation_Applicationitemsid = "";
      }

      public SdtPurchaseReceiptInformation( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtPurchaseReceiptInformation deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtPurchaseReceiptInformation)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtPurchaseReceiptInformation obj ;
         obj = this;
         obj.gxTpr_Valid = deserialized.gxTpr_Valid;
         obj.gxTpr_Quantity = deserialized.gxTpr_Quantity;
         obj.gxTpr_Productid = deserialized.gxTpr_Productid;
         obj.gxTpr_Transactionid = deserialized.gxTpr_Transactionid;
         obj.gxTpr_Purchasedate = deserialized.gxTpr_Purchasedate;
         obj.gxTpr_Originaltransactionid = deserialized.gxTpr_Originaltransactionid;
         obj.gxTpr_Originalpurchasedate = deserialized.gxTpr_Originalpurchasedate;
         obj.gxTpr_Applicationitemsid = deserialized.gxTpr_Applicationitemsid;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Valid") )
               {
                  gxTv_SdtPurchaseReceiptInformation_Valid = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Quantity") )
               {
                  gxTv_SdtPurchaseReceiptInformation_Quantity = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ProductId") )
               {
                  gxTv_SdtPurchaseReceiptInformation_Productid = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TransactionId") )
               {
                  gxTv_SdtPurchaseReceiptInformation_Transactionid = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PurchaseDate") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtPurchaseReceiptInformation_Purchasedate = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtPurchaseReceiptInformation_Purchasedate = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OriginalTransactionId") )
               {
                  gxTv_SdtPurchaseReceiptInformation_Originaltransactionid = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OriginalPurchaseDate") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ApplicationItemsId") )
               {
                  gxTv_SdtPurchaseReceiptInformation_Applicationitemsid = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "PurchaseReceiptInformation";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Valid", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtPurchaseReceiptInformation_Valid)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Quantity", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtPurchaseReceiptInformation_Quantity), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ProductId", StringUtil.RTrim( gxTv_SdtPurchaseReceiptInformation_Productid));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("TransactionId", StringUtil.RTrim( gxTv_SdtPurchaseReceiptInformation_Transactionid));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtPurchaseReceiptInformation_Purchasedate) )
         {
            oWriter.WriteStartElement("PurchaseDate");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtPurchaseReceiptInformation_Purchasedate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtPurchaseReceiptInformation_Purchasedate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtPurchaseReceiptInformation_Purchasedate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtPurchaseReceiptInformation_Purchasedate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtPurchaseReceiptInformation_Purchasedate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtPurchaseReceiptInformation_Purchasedate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("PurchaseDate", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("OriginalTransactionId", StringUtil.RTrim( gxTv_SdtPurchaseReceiptInformation_Originaltransactionid));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate) )
         {
            oWriter.WriteStartElement("OriginalPurchaseDate");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("OriginalPurchaseDate", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("ApplicationItemsId", StringUtil.RTrim( gxTv_SdtPurchaseReceiptInformation_Applicationitemsid));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Valid", gxTv_SdtPurchaseReceiptInformation_Valid, false);
         AddObjectProperty("Quantity", gxTv_SdtPurchaseReceiptInformation_Quantity, false);
         AddObjectProperty("ProductId", gxTv_SdtPurchaseReceiptInformation_Productid, false);
         AddObjectProperty("TransactionId", gxTv_SdtPurchaseReceiptInformation_Transactionid, false);
         datetime_STZ = gxTv_SdtPurchaseReceiptInformation_Purchasedate;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("PurchaseDate", sDateCnv, false);
         AddObjectProperty("OriginalTransactionId", gxTv_SdtPurchaseReceiptInformation_Originaltransactionid, false);
         datetime_STZ = gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("OriginalPurchaseDate", sDateCnv, false);
         AddObjectProperty("ApplicationItemsId", gxTv_SdtPurchaseReceiptInformation_Applicationitemsid, false);
         return  ;
      }

      [  SoapElement( ElementName = "Valid" )]
      [  XmlElement( ElementName = "Valid"   )]
      public bool gxTpr_Valid
      {
         get {
            return gxTv_SdtPurchaseReceiptInformation_Valid ;
         }

         set {
            gxTv_SdtPurchaseReceiptInformation_Valid = value;
         }

      }

      [  SoapElement( ElementName = "Quantity" )]
      [  XmlElement( ElementName = "Quantity"   )]
      public int gxTpr_Quantity
      {
         get {
            return gxTv_SdtPurchaseReceiptInformation_Quantity ;
         }

         set {
            gxTv_SdtPurchaseReceiptInformation_Quantity = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ProductId" )]
      [  XmlElement( ElementName = "ProductId"   )]
      public String gxTpr_Productid
      {
         get {
            return gxTv_SdtPurchaseReceiptInformation_Productid ;
         }

         set {
            gxTv_SdtPurchaseReceiptInformation_Productid = (String)(value);
         }

      }

      [  SoapElement( ElementName = "TransactionId" )]
      [  XmlElement( ElementName = "TransactionId"   )]
      public String gxTpr_Transactionid
      {
         get {
            return gxTv_SdtPurchaseReceiptInformation_Transactionid ;
         }

         set {
            gxTv_SdtPurchaseReceiptInformation_Transactionid = (String)(value);
         }

      }

      [  SoapElement( ElementName = "PurchaseDate" )]
      [  XmlElement( ElementName = "PurchaseDate"  , IsNullable=true )]
      public string gxTpr_Purchasedate_Nullable
      {
         get {
            if ( gxTv_SdtPurchaseReceiptInformation_Purchasedate == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtPurchaseReceiptInformation_Purchasedate).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtPurchaseReceiptInformation_Purchasedate = DateTime.MinValue;
            else
               gxTv_SdtPurchaseReceiptInformation_Purchasedate = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Purchasedate
      {
         get {
            return gxTv_SdtPurchaseReceiptInformation_Purchasedate ;
         }

         set {
            gxTv_SdtPurchaseReceiptInformation_Purchasedate = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "OriginalTransactionId" )]
      [  XmlElement( ElementName = "OriginalTransactionId"   )]
      public String gxTpr_Originaltransactionid
      {
         get {
            return gxTv_SdtPurchaseReceiptInformation_Originaltransactionid ;
         }

         set {
            gxTv_SdtPurchaseReceiptInformation_Originaltransactionid = (String)(value);
         }

      }

      [  SoapElement( ElementName = "OriginalPurchaseDate" )]
      [  XmlElement( ElementName = "OriginalPurchaseDate"  , IsNullable=true )]
      public string gxTpr_Originalpurchasedate_Nullable
      {
         get {
            if ( gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate = DateTime.MinValue;
            else
               gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Originalpurchasedate
      {
         get {
            return gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate ;
         }

         set {
            gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ApplicationItemsId" )]
      [  XmlElement( ElementName = "ApplicationItemsId"   )]
      public String gxTpr_Applicationitemsid
      {
         get {
            return gxTv_SdtPurchaseReceiptInformation_Applicationitemsid ;
         }

         set {
            gxTv_SdtPurchaseReceiptInformation_Applicationitemsid = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtPurchaseReceiptInformation_Productid = "";
         gxTv_SdtPurchaseReceiptInformation_Transactionid = "";
         gxTv_SdtPurchaseReceiptInformation_Purchasedate = (DateTime)(DateTime.MinValue);
         gxTv_SdtPurchaseReceiptInformation_Originaltransactionid = "";
         gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate = (DateTime)(DateTime.MinValue);
         gxTv_SdtPurchaseReceiptInformation_Applicationitemsid = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtPurchaseReceiptInformation_Quantity ;
      protected String sTagName ;
      protected String sDateCnv ;
      protected String sNumToPad ;
      protected DateTime gxTv_SdtPurchaseReceiptInformation_Purchasedate ;
      protected DateTime gxTv_SdtPurchaseReceiptInformation_Originalpurchasedate ;
      protected DateTime datetime_STZ ;
      protected bool gxTv_SdtPurchaseReceiptInformation_Valid ;
      protected String gxTv_SdtPurchaseReceiptInformation_Productid ;
      protected String gxTv_SdtPurchaseReceiptInformation_Transactionid ;
      protected String gxTv_SdtPurchaseReceiptInformation_Originaltransactionid ;
      protected String gxTv_SdtPurchaseReceiptInformation_Applicationitemsid ;
   }

   [DataContract(Name = @"PurchaseReceiptInformation", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtPurchaseReceiptInformation_RESTInterface : GxGenericCollectionItem<SdtPurchaseReceiptInformation>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtPurchaseReceiptInformation_RESTInterface( ) : base()
      {
      }

      public SdtPurchaseReceiptInformation_RESTInterface( SdtPurchaseReceiptInformation psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Valid" , Order = 0 )]
      public bool gxTpr_Valid
      {
         get {
            return sdt.gxTpr_Valid ;
         }

         set {
            sdt.gxTpr_Valid = value;
         }

      }

      [DataMember( Name = "Quantity" , Order = 1 )]
      public String gxTpr_Quantity
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Quantity), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Quantity = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ProductId" , Order = 2 )]
      public String gxTpr_Productid
      {
         get {
            return sdt.gxTpr_Productid ;
         }

         set {
            sdt.gxTpr_Productid = (String)(value);
         }

      }

      [DataMember( Name = "TransactionId" , Order = 3 )]
      public String gxTpr_Transactionid
      {
         get {
            return sdt.gxTpr_Transactionid ;
         }

         set {
            sdt.gxTpr_Transactionid = (String)(value);
         }

      }

      [DataMember( Name = "PurchaseDate" , Order = 4 )]
      public String gxTpr_Purchasedate
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Purchasedate) ;
         }

         set {
            sdt.gxTpr_Purchasedate = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "OriginalTransactionId" , Order = 5 )]
      public String gxTpr_Originaltransactionid
      {
         get {
            return sdt.gxTpr_Originaltransactionid ;
         }

         set {
            sdt.gxTpr_Originaltransactionid = (String)(value);
         }

      }

      [DataMember( Name = "OriginalPurchaseDate" , Order = 6 )]
      public String gxTpr_Originalpurchasedate
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Originalpurchasedate) ;
         }

         set {
            sdt.gxTpr_Originalpurchasedate = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ApplicationItemsId" , Order = 7 )]
      public String gxTpr_Applicationitemsid
      {
         get {
            return sdt.gxTpr_Applicationitemsid ;
         }

         set {
            sdt.gxTpr_Applicationitemsid = (String)(value);
         }

      }

      public SdtPurchaseReceiptInformation sdt
      {
         get {
            return (SdtPurchaseReceiptInformation)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtPurchaseReceiptInformation() ;
         }
      }

   }

}
