/*
               File: GetContrato_UnidadesMedicaoWCFilterData
        Description: Get Contrato_Unidades Medicao WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:7:32.58
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontrato_unidadesmedicaowcfilterdata : GXProcedure
   {
      public getcontrato_unidadesmedicaowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontrato_unidadesmedicaowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontrato_unidadesmedicaowcfilterdata objgetcontrato_unidadesmedicaowcfilterdata;
         objgetcontrato_unidadesmedicaowcfilterdata = new getcontrato_unidadesmedicaowcfilterdata();
         objgetcontrato_unidadesmedicaowcfilterdata.AV18DDOName = aP0_DDOName;
         objgetcontrato_unidadesmedicaowcfilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetcontrato_unidadesmedicaowcfilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetcontrato_unidadesmedicaowcfilterdata.AV22OptionsJson = "" ;
         objgetcontrato_unidadesmedicaowcfilterdata.AV25OptionsDescJson = "" ;
         objgetcontrato_unidadesmedicaowcfilterdata.AV27OptionIndexesJson = "" ;
         objgetcontrato_unidadesmedicaowcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontrato_unidadesmedicaowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontrato_unidadesmedicaowcfilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontrato_unidadesmedicaowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATOUNIDADES_UNDMEDNOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOUNIDADES_UNDMEDNOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATOUNIDADES_UNDMEDSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOUNIDADES_UNDMEDSIGLAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("Contrato_UnidadesMedicaoWCGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "Contrato_UnidadesMedicaoWCGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("Contrato_UnidadesMedicaoWCGridState"), "");
         }
         AV37GXV1 = 1;
         while ( AV37GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV37GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDNOM") == 0 )
            {
               AV10TFContratoUnidades_UndMedNom = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDNOM_SEL") == 0 )
            {
               AV11TFContratoUnidades_UndMedNom_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_PRODUTIVIDADE") == 0 )
            {
               AV12TFContratoUnidades_Produtividade = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, ".");
               AV13TFContratoUnidades_Produtividade_To = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDSIGLA") == 0 )
            {
               AV14TFContratoUnidades_UndMedSigla = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOUNIDADES_UNDMEDSIGLA_SEL") == 0 )
            {
               AV15TFContratoUnidades_UndMedSigla_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "PARM_&CONTRATOUNIDADES_CONTRATOCOD") == 0 )
            {
               AV34ContratoUnidades_ContratoCod = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
            }
            AV37GXV1 = (int)(AV37GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATOUNIDADES_UNDMEDNOMOPTIONS' Routine */
         AV10TFContratoUnidades_UndMedNom = AV16SearchTxt;
         AV11TFContratoUnidades_UndMedNom_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV11TFContratoUnidades_UndMedNom_Sel ,
                                              AV10TFContratoUnidades_UndMedNom ,
                                              AV12TFContratoUnidades_Produtividade ,
                                              AV13TFContratoUnidades_Produtividade_To ,
                                              AV15TFContratoUnidades_UndMedSigla_Sel ,
                                              AV14TFContratoUnidades_UndMedSigla ,
                                              A1205ContratoUnidades_UndMedNom ,
                                              A1208ContratoUnidades_Produtividade ,
                                              A1206ContratoUnidades_UndMedSigla ,
                                              AV34ContratoUnidades_ContratoCod ,
                                              A1207ContratoUnidades_ContratoCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFContratoUnidades_UndMedNom = StringUtil.PadR( StringUtil.RTrim( AV10TFContratoUnidades_UndMedNom), 50, "%");
         lV14TFContratoUnidades_UndMedSigla = StringUtil.PadR( StringUtil.RTrim( AV14TFContratoUnidades_UndMedSigla), 15, "%");
         /* Using cursor P00IS2 */
         pr_default.execute(0, new Object[] {AV34ContratoUnidades_ContratoCod, lV10TFContratoUnidades_UndMedNom, AV11TFContratoUnidades_UndMedNom_Sel, AV12TFContratoUnidades_Produtividade, AV13TFContratoUnidades_Produtividade_To, lV14TFContratoUnidades_UndMedSigla, AV15TFContratoUnidades_UndMedSigla_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKIS2 = false;
            A1207ContratoUnidades_ContratoCod = P00IS2_A1207ContratoUnidades_ContratoCod[0];
            A1204ContratoUnidades_UndMedCod = P00IS2_A1204ContratoUnidades_UndMedCod[0];
            A1206ContratoUnidades_UndMedSigla = P00IS2_A1206ContratoUnidades_UndMedSigla[0];
            n1206ContratoUnidades_UndMedSigla = P00IS2_n1206ContratoUnidades_UndMedSigla[0];
            A1208ContratoUnidades_Produtividade = P00IS2_A1208ContratoUnidades_Produtividade[0];
            n1208ContratoUnidades_Produtividade = P00IS2_n1208ContratoUnidades_Produtividade[0];
            A1205ContratoUnidades_UndMedNom = P00IS2_A1205ContratoUnidades_UndMedNom[0];
            n1205ContratoUnidades_UndMedNom = P00IS2_n1205ContratoUnidades_UndMedNom[0];
            A1206ContratoUnidades_UndMedSigla = P00IS2_A1206ContratoUnidades_UndMedSigla[0];
            n1206ContratoUnidades_UndMedSigla = P00IS2_n1206ContratoUnidades_UndMedSigla[0];
            A1205ContratoUnidades_UndMedNom = P00IS2_A1205ContratoUnidades_UndMedNom[0];
            n1205ContratoUnidades_UndMedNom = P00IS2_n1205ContratoUnidades_UndMedNom[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00IS2_A1207ContratoUnidades_ContratoCod[0] == A1207ContratoUnidades_ContratoCod ) && ( P00IS2_A1204ContratoUnidades_UndMedCod[0] == A1204ContratoUnidades_UndMedCod ) )
            {
               BRKIS2 = false;
               AV28count = (long)(AV28count+1);
               BRKIS2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1205ContratoUnidades_UndMedNom)) )
            {
               AV20Option = A1205ContratoUnidades_UndMedNom;
               AV19InsertIndex = 1;
               while ( ( AV19InsertIndex <= AV21Options.Count ) && ( StringUtil.StrCmp(((String)AV21Options.Item(AV19InsertIndex)), AV20Option) < 0 ) )
               {
                  AV19InsertIndex = (int)(AV19InsertIndex+1);
               }
               AV21Options.Add(AV20Option, AV19InsertIndex);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), AV19InsertIndex);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKIS2 )
            {
               BRKIS2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOUNIDADES_UNDMEDSIGLAOPTIONS' Routine */
         AV14TFContratoUnidades_UndMedSigla = AV16SearchTxt;
         AV15TFContratoUnidades_UndMedSigla_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV11TFContratoUnidades_UndMedNom_Sel ,
                                              AV10TFContratoUnidades_UndMedNom ,
                                              AV12TFContratoUnidades_Produtividade ,
                                              AV13TFContratoUnidades_Produtividade_To ,
                                              AV15TFContratoUnidades_UndMedSigla_Sel ,
                                              AV14TFContratoUnidades_UndMedSigla ,
                                              A1205ContratoUnidades_UndMedNom ,
                                              A1208ContratoUnidades_Produtividade ,
                                              A1206ContratoUnidades_UndMedSigla ,
                                              A1207ContratoUnidades_ContratoCod ,
                                              AV34ContratoUnidades_ContratoCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFContratoUnidades_UndMedNom = StringUtil.PadR( StringUtil.RTrim( AV10TFContratoUnidades_UndMedNom), 50, "%");
         lV14TFContratoUnidades_UndMedSigla = StringUtil.PadR( StringUtil.RTrim( AV14TFContratoUnidades_UndMedSigla), 15, "%");
         /* Using cursor P00IS3 */
         pr_default.execute(1, new Object[] {AV34ContratoUnidades_ContratoCod, lV10TFContratoUnidades_UndMedNom, AV11TFContratoUnidades_UndMedNom_Sel, AV12TFContratoUnidades_Produtividade, AV13TFContratoUnidades_Produtividade_To, lV14TFContratoUnidades_UndMedSigla, AV15TFContratoUnidades_UndMedSigla_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKIS4 = false;
            A1204ContratoUnidades_UndMedCod = P00IS3_A1204ContratoUnidades_UndMedCod[0];
            A1207ContratoUnidades_ContratoCod = P00IS3_A1207ContratoUnidades_ContratoCod[0];
            A1206ContratoUnidades_UndMedSigla = P00IS3_A1206ContratoUnidades_UndMedSigla[0];
            n1206ContratoUnidades_UndMedSigla = P00IS3_n1206ContratoUnidades_UndMedSigla[0];
            A1208ContratoUnidades_Produtividade = P00IS3_A1208ContratoUnidades_Produtividade[0];
            n1208ContratoUnidades_Produtividade = P00IS3_n1208ContratoUnidades_Produtividade[0];
            A1205ContratoUnidades_UndMedNom = P00IS3_A1205ContratoUnidades_UndMedNom[0];
            n1205ContratoUnidades_UndMedNom = P00IS3_n1205ContratoUnidades_UndMedNom[0];
            A1206ContratoUnidades_UndMedSigla = P00IS3_A1206ContratoUnidades_UndMedSigla[0];
            n1206ContratoUnidades_UndMedSigla = P00IS3_n1206ContratoUnidades_UndMedSigla[0];
            A1205ContratoUnidades_UndMedNom = P00IS3_A1205ContratoUnidades_UndMedNom[0];
            n1205ContratoUnidades_UndMedNom = P00IS3_n1205ContratoUnidades_UndMedNom[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00IS3_A1206ContratoUnidades_UndMedSigla[0], A1206ContratoUnidades_UndMedSigla) == 0 ) )
            {
               BRKIS4 = false;
               A1204ContratoUnidades_UndMedCod = P00IS3_A1204ContratoUnidades_UndMedCod[0];
               A1207ContratoUnidades_ContratoCod = P00IS3_A1207ContratoUnidades_ContratoCod[0];
               AV28count = (long)(AV28count+1);
               BRKIS4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1206ContratoUnidades_UndMedSigla)) )
            {
               AV20Option = A1206ContratoUnidades_UndMedSigla;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKIS4 )
            {
               BRKIS4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContratoUnidades_UndMedNom = "";
         AV11TFContratoUnidades_UndMedNom_Sel = "";
         AV14TFContratoUnidades_UndMedSigla = "";
         AV15TFContratoUnidades_UndMedSigla_Sel = "";
         scmdbuf = "";
         lV10TFContratoUnidades_UndMedNom = "";
         lV14TFContratoUnidades_UndMedSigla = "";
         A1205ContratoUnidades_UndMedNom = "";
         A1206ContratoUnidades_UndMedSigla = "";
         P00IS2_A1207ContratoUnidades_ContratoCod = new int[1] ;
         P00IS2_A1204ContratoUnidades_UndMedCod = new int[1] ;
         P00IS2_A1206ContratoUnidades_UndMedSigla = new String[] {""} ;
         P00IS2_n1206ContratoUnidades_UndMedSigla = new bool[] {false} ;
         P00IS2_A1208ContratoUnidades_Produtividade = new decimal[1] ;
         P00IS2_n1208ContratoUnidades_Produtividade = new bool[] {false} ;
         P00IS2_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         P00IS2_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         AV20Option = "";
         P00IS3_A1204ContratoUnidades_UndMedCod = new int[1] ;
         P00IS3_A1207ContratoUnidades_ContratoCod = new int[1] ;
         P00IS3_A1206ContratoUnidades_UndMedSigla = new String[] {""} ;
         P00IS3_n1206ContratoUnidades_UndMedSigla = new bool[] {false} ;
         P00IS3_A1208ContratoUnidades_Produtividade = new decimal[1] ;
         P00IS3_n1208ContratoUnidades_Produtividade = new bool[] {false} ;
         P00IS3_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         P00IS3_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontrato_unidadesmedicaowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00IS2_A1207ContratoUnidades_ContratoCod, P00IS2_A1204ContratoUnidades_UndMedCod, P00IS2_A1206ContratoUnidades_UndMedSigla, P00IS2_n1206ContratoUnidades_UndMedSigla, P00IS2_A1208ContratoUnidades_Produtividade, P00IS2_n1208ContratoUnidades_Produtividade, P00IS2_A1205ContratoUnidades_UndMedNom, P00IS2_n1205ContratoUnidades_UndMedNom
               }
               , new Object[] {
               P00IS3_A1204ContratoUnidades_UndMedCod, P00IS3_A1207ContratoUnidades_ContratoCod, P00IS3_A1206ContratoUnidades_UndMedSigla, P00IS3_n1206ContratoUnidades_UndMedSigla, P00IS3_A1208ContratoUnidades_Produtividade, P00IS3_n1208ContratoUnidades_Produtividade, P00IS3_A1205ContratoUnidades_UndMedNom, P00IS3_n1205ContratoUnidades_UndMedNom
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV37GXV1 ;
      private int AV34ContratoUnidades_ContratoCod ;
      private int A1207ContratoUnidades_ContratoCod ;
      private int A1204ContratoUnidades_UndMedCod ;
      private int AV19InsertIndex ;
      private long AV28count ;
      private decimal AV12TFContratoUnidades_Produtividade ;
      private decimal AV13TFContratoUnidades_Produtividade_To ;
      private decimal A1208ContratoUnidades_Produtividade ;
      private String AV10TFContratoUnidades_UndMedNom ;
      private String AV11TFContratoUnidades_UndMedNom_Sel ;
      private String AV14TFContratoUnidades_UndMedSigla ;
      private String AV15TFContratoUnidades_UndMedSigla_Sel ;
      private String scmdbuf ;
      private String lV10TFContratoUnidades_UndMedNom ;
      private String lV14TFContratoUnidades_UndMedSigla ;
      private String A1205ContratoUnidades_UndMedNom ;
      private String A1206ContratoUnidades_UndMedSigla ;
      private bool returnInSub ;
      private bool BRKIS2 ;
      private bool n1206ContratoUnidades_UndMedSigla ;
      private bool n1208ContratoUnidades_Produtividade ;
      private bool n1205ContratoUnidades_UndMedNom ;
      private bool BRKIS4 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00IS2_A1207ContratoUnidades_ContratoCod ;
      private int[] P00IS2_A1204ContratoUnidades_UndMedCod ;
      private String[] P00IS2_A1206ContratoUnidades_UndMedSigla ;
      private bool[] P00IS2_n1206ContratoUnidades_UndMedSigla ;
      private decimal[] P00IS2_A1208ContratoUnidades_Produtividade ;
      private bool[] P00IS2_n1208ContratoUnidades_Produtividade ;
      private String[] P00IS2_A1205ContratoUnidades_UndMedNom ;
      private bool[] P00IS2_n1205ContratoUnidades_UndMedNom ;
      private int[] P00IS3_A1204ContratoUnidades_UndMedCod ;
      private int[] P00IS3_A1207ContratoUnidades_ContratoCod ;
      private String[] P00IS3_A1206ContratoUnidades_UndMedSigla ;
      private bool[] P00IS3_n1206ContratoUnidades_UndMedSigla ;
      private decimal[] P00IS3_A1208ContratoUnidades_Produtividade ;
      private bool[] P00IS3_n1208ContratoUnidades_Produtividade ;
      private String[] P00IS3_A1205ContratoUnidades_UndMedNom ;
      private bool[] P00IS3_n1205ContratoUnidades_UndMedNom ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
   }

   public class getcontrato_unidadesmedicaowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00IS2( IGxContext context ,
                                             String AV11TFContratoUnidades_UndMedNom_Sel ,
                                             String AV10TFContratoUnidades_UndMedNom ,
                                             decimal AV12TFContratoUnidades_Produtividade ,
                                             decimal AV13TFContratoUnidades_Produtividade_To ,
                                             String AV15TFContratoUnidades_UndMedSigla_Sel ,
                                             String AV14TFContratoUnidades_UndMedSigla ,
                                             String A1205ContratoUnidades_UndMedNom ,
                                             decimal A1208ContratoUnidades_Produtividade ,
                                             String A1206ContratoUnidades_UndMedSigla ,
                                             int AV34ContratoUnidades_ContratoCod ,
                                             int A1207ContratoUnidades_ContratoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [7] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoUnidades_ContratoCod], T1.[ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod, T2.[UnidadeMedicao_Sigla] AS ContratoUnidades_UndMedSigla, T1.[ContratoUnidades_Produtividade], T2.[UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom FROM ([ContratoUnidades] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoUnidades_UndMedCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoUnidades_ContratoCod] = @AV34ContratoUnidades_ContratoCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoUnidades_UndMedNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratoUnidades_UndMedNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV10TFContratoUnidades_UndMedNom)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoUnidades_UndMedNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV11TFContratoUnidades_UndMedNom_Sel)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV12TFContratoUnidades_Produtividade) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] >= @AV12TFContratoUnidades_Produtividade)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV13TFContratoUnidades_Produtividade_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] <= @AV13TFContratoUnidades_Produtividade_To)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoUnidades_UndMedSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoUnidades_UndMedSigla)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] like @lV14TFContratoUnidades_UndMedSigla)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoUnidades_UndMedSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] = @AV15TFContratoUnidades_UndMedSigla_Sel)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoUnidades_ContratoCod], T1.[ContratoUnidades_UndMedCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00IS3( IGxContext context ,
                                             String AV11TFContratoUnidades_UndMedNom_Sel ,
                                             String AV10TFContratoUnidades_UndMedNom ,
                                             decimal AV12TFContratoUnidades_Produtividade ,
                                             decimal AV13TFContratoUnidades_Produtividade_To ,
                                             String AV15TFContratoUnidades_UndMedSigla_Sel ,
                                             String AV14TFContratoUnidades_UndMedSigla ,
                                             String A1205ContratoUnidades_UndMedNom ,
                                             decimal A1208ContratoUnidades_Produtividade ,
                                             String A1206ContratoUnidades_UndMedSigla ,
                                             int A1207ContratoUnidades_ContratoCod ,
                                             int AV34ContratoUnidades_ContratoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [7] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod, T1.[ContratoUnidades_ContratoCod], T2.[UnidadeMedicao_Sigla] AS ContratoUnidades_UndMedSigla, T1.[ContratoUnidades_Produtividade], T2.[UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom FROM ([ContratoUnidades] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoUnidades_UndMedCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoUnidades_ContratoCod] = @AV34ContratoUnidades_ContratoCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoUnidades_UndMedNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratoUnidades_UndMedNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV10TFContratoUnidades_UndMedNom)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoUnidades_UndMedNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV11TFContratoUnidades_UndMedNom_Sel)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV12TFContratoUnidades_Produtividade) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] >= @AV12TFContratoUnidades_Produtividade)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV13TFContratoUnidades_Produtividade_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoUnidades_Produtividade] <= @AV13TFContratoUnidades_Produtividade_To)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoUnidades_UndMedSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoUnidades_UndMedSigla)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] like @lV14TFContratoUnidades_UndMedSigla)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoUnidades_UndMedSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Sigla] = @AV15TFContratoUnidades_UndMedSigla_Sel)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[UnidadeMedicao_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00IS2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (decimal)dynConstraints[2] , (decimal)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (decimal)dynConstraints[7] , (String)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] );
               case 1 :
                     return conditional_P00IS3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (decimal)dynConstraints[2] , (decimal)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (decimal)dynConstraints[7] , (String)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00IS2 ;
          prmP00IS2 = new Object[] {
          new Object[] {"@AV34ContratoUnidades_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratoUnidades_UndMedNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFContratoUnidades_UndMedNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV12TFContratoUnidades_Produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV13TFContratoUnidades_Produtividade_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@lV14TFContratoUnidades_UndMedSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV15TFContratoUnidades_UndMedSigla_Sel",SqlDbType.Char,15,0}
          } ;
          Object[] prmP00IS3 ;
          prmP00IS3 = new Object[] {
          new Object[] {"@AV34ContratoUnidades_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratoUnidades_UndMedNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFContratoUnidades_UndMedNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV12TFContratoUnidades_Produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV13TFContratoUnidades_Produtividade_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@lV14TFContratoUnidades_UndMedSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV15TFContratoUnidades_UndMedSigla_Sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00IS2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00IS2,100,0,true,false )
             ,new CursorDef("P00IS3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00IS3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontrato_unidadesmedicaowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontrato_unidadesmedicaowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontrato_unidadesmedicaowcfilterdata") )
          {
             return  ;
          }
          getcontrato_unidadesmedicaowcfilterdata worker = new getcontrato_unidadesmedicaowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
