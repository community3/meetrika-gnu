/*
               File: WWContratante
        Description:  Contratante
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:30:54.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontratante : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontratante( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontratante( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_32 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_32_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_32_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV79TFContratante_RazaoSocial = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFContratante_RazaoSocial", AV79TFContratante_RazaoSocial);
               AV80TFContratante_RazaoSocial_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratante_RazaoSocial_Sel", AV80TFContratante_RazaoSocial_Sel);
               AV83TFContratante_NomeFantasia = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFContratante_NomeFantasia", AV83TFContratante_NomeFantasia);
               AV84TFContratante_NomeFantasia_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContratante_NomeFantasia_Sel", AV84TFContratante_NomeFantasia_Sel);
               AV87TFContratante_CNPJ = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFContratante_CNPJ", AV87TFContratante_CNPJ);
               AV88TFContratante_CNPJ_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFContratante_CNPJ_Sel", AV88TFContratante_CNPJ_Sel);
               AV91TFContratante_Telefone = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFContratante_Telefone", AV91TFContratante_Telefone);
               AV92TFContratante_Telefone_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFContratante_Telefone_Sel", AV92TFContratante_Telefone_Sel);
               AV95TFContratante_Ramal = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95TFContratante_Ramal", AV95TFContratante_Ramal);
               AV96TFContratante_Ramal_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96TFContratante_Ramal_Sel", AV96TFContratante_Ramal_Sel);
               AV99TFMunicipio_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99TFMunicipio_Nome", AV99TFMunicipio_Nome);
               AV100TFMunicipio_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFMunicipio_Nome_Sel", AV100TFMunicipio_Nome_Sel);
               AV103TFEstado_UF = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103TFEstado_UF", AV103TFEstado_UF);
               AV104TFEstado_UF_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFEstado_UF_Sel", AV104TFEstado_UF_Sel);
               AV107TFContratante_InicioDoExpediente = DateTimeUtil.ResetDate(context.localUtil.ParseDTimeParm( GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TFContratante_InicioDoExpediente", context.localUtil.TToC( AV107TFContratante_InicioDoExpediente, 0, 5, 0, 3, "/", ":", " "));
               AV108TFContratante_InicioDoExpediente_To = DateTimeUtil.ResetDate(context.localUtil.ParseDTimeParm( GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContratante_InicioDoExpediente_To", context.localUtil.TToC( AV108TFContratante_InicioDoExpediente_To, 0, 5, 0, 3, "/", ":", " "));
               AV113TFContratante_FimDoExpediente = DateTimeUtil.ResetDate(context.localUtil.ParseDTimeParm( GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113TFContratante_FimDoExpediente", context.localUtil.TToC( AV113TFContratante_FimDoExpediente, 0, 5, 0, 3, "/", ":", " "));
               AV114TFContratante_FimDoExpediente_To = DateTimeUtil.ResetDate(context.localUtil.ParseDTimeParm( GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114TFContratante_FimDoExpediente_To", context.localUtil.TToC( AV114TFContratante_FimDoExpediente_To, 0, 5, 0, 3, "/", ":", " "));
               AV61ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV61ManageFiltersExecutionStep), 1, 0));
               AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace", AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace);
               AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace", AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace);
               AV89ddo_Contratante_CNPJTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89ddo_Contratante_CNPJTitleControlIdToReplace", AV89ddo_Contratante_CNPJTitleControlIdToReplace);
               AV93ddo_Contratante_TelefoneTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93ddo_Contratante_TelefoneTitleControlIdToReplace", AV93ddo_Contratante_TelefoneTitleControlIdToReplace);
               AV97ddo_Contratante_RamalTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97ddo_Contratante_RamalTitleControlIdToReplace", AV97ddo_Contratante_RamalTitleControlIdToReplace);
               AV101ddo_Municipio_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101ddo_Municipio_NomeTitleControlIdToReplace", AV101ddo_Municipio_NomeTitleControlIdToReplace);
               AV105ddo_Estado_UFTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ddo_Estado_UFTitleControlIdToReplace", AV105ddo_Estado_UFTitleControlIdToReplace);
               AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace", AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace);
               AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace", AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace);
               AV39AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39AreaTrabalho_Codigo), 6, 0)));
               AV165Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               A29Contratante_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n29Contratante_Codigo = false;
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV79TFContratante_RazaoSocial, AV80TFContratante_RazaoSocial_Sel, AV83TFContratante_NomeFantasia, AV84TFContratante_NomeFantasia_Sel, AV87TFContratante_CNPJ, AV88TFContratante_CNPJ_Sel, AV91TFContratante_Telefone, AV92TFContratante_Telefone_Sel, AV95TFContratante_Ramal, AV96TFContratante_Ramal_Sel, AV99TFMunicipio_Nome, AV100TFMunicipio_Nome_Sel, AV103TFEstado_UF, AV104TFEstado_UF_Sel, AV107TFContratante_InicioDoExpediente, AV108TFContratante_InicioDoExpediente_To, AV113TFContratante_FimDoExpediente, AV114TFContratante_FimDoExpediente_To, AV61ManageFiltersExecutionStep, AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace, AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace, AV89ddo_Contratante_CNPJTitleControlIdToReplace, AV93ddo_Contratante_TelefoneTitleControlIdToReplace, AV97ddo_Contratante_RamalTitleControlIdToReplace, AV101ddo_Municipio_NomeTitleControlIdToReplace, AV105ddo_Estado_UFTitleControlIdToReplace, AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace, AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace, AV39AreaTrabalho_Codigo, AV165Pgmname, AV6WWPContext, A29Contratante_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA0Y2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START0Y2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311730555");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontratante.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_RAZAOSOCIAL", StringUtil.RTrim( AV79TFContratante_RazaoSocial));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_RAZAOSOCIAL_SEL", StringUtil.RTrim( AV80TFContratante_RazaoSocial_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_NOMEFANTASIA", StringUtil.RTrim( AV83TFContratante_NomeFantasia));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_NOMEFANTASIA_SEL", StringUtil.RTrim( AV84TFContratante_NomeFantasia_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_CNPJ", AV87TFContratante_CNPJ);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_CNPJ_SEL", AV88TFContratante_CNPJ_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_TELEFONE", StringUtil.RTrim( AV91TFContratante_Telefone));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_TELEFONE_SEL", StringUtil.RTrim( AV92TFContratante_Telefone_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_RAMAL", StringUtil.RTrim( AV95TFContratante_Ramal));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_RAMAL_SEL", StringUtil.RTrim( AV96TFContratante_Ramal_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMUNICIPIO_NOME", StringUtil.RTrim( AV99TFMunicipio_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMUNICIPIO_NOME_SEL", StringUtil.RTrim( AV100TFMunicipio_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFESTADO_UF", StringUtil.RTrim( AV103TFEstado_UF));
         GxWebStd.gx_hidden_field( context, "GXH_vTFESTADO_UF_SEL", StringUtil.RTrim( AV104TFEstado_UF_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_INICIODOEXPEDIENTE", context.localUtil.TToC( AV107TFContratante_InicioDoExpediente, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_INICIODOEXPEDIENTE_TO", context.localUtil.TToC( AV108TFContratante_InicioDoExpediente_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_FIMDOEXPEDIENTE", context.localUtil.TToC( AV113TFContratante_FimDoExpediente, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_FIMDOEXPEDIENTE_TO", context.localUtil.TToC( AV114TFContratante_FimDoExpediente_To, 10, 8, 0, 3, "/", ":", " "));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_32", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_32), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV65ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV65ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV120GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV121GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV118DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV118DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATANTE_RAZAOSOCIALTITLEFILTERDATA", AV78Contratante_RazaoSocialTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATANTE_RAZAOSOCIALTITLEFILTERDATA", AV78Contratante_RazaoSocialTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATANTE_NOMEFANTASIATITLEFILTERDATA", AV82Contratante_NomeFantasiaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATANTE_NOMEFANTASIATITLEFILTERDATA", AV82Contratante_NomeFantasiaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATANTE_CNPJTITLEFILTERDATA", AV86Contratante_CNPJTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATANTE_CNPJTITLEFILTERDATA", AV86Contratante_CNPJTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATANTE_TELEFONETITLEFILTERDATA", AV90Contratante_TelefoneTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATANTE_TELEFONETITLEFILTERDATA", AV90Contratante_TelefoneTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATANTE_RAMALTITLEFILTERDATA", AV94Contratante_RamalTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATANTE_RAMALTITLEFILTERDATA", AV94Contratante_RamalTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMUNICIPIO_NOMETITLEFILTERDATA", AV98Municipio_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMUNICIPIO_NOMETITLEFILTERDATA", AV98Municipio_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vESTADO_UFTITLEFILTERDATA", AV102Estado_UFTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vESTADO_UFTITLEFILTERDATA", AV102Estado_UFTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATANTE_INICIODOEXPEDIENTETITLEFILTERDATA", AV106Contratante_InicioDoExpedienteTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATANTE_INICIODOEXPEDIENTETITLEFILTERDATA", AV106Contratante_InicioDoExpedienteTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATANTE_FIMDOEXPEDIENTETITLEFILTERDATA", AV112Contratante_FimDoExpedienteTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATANTE_FIMDOEXPEDIENTETITLEFILTERDATA", AV112Contratante_FimDoExpedienteTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV165Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Caption", StringUtil.RTrim( Ddo_contratante_razaosocial_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Tooltip", StringUtil.RTrim( Ddo_contratante_razaosocial_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Cls", StringUtil.RTrim( Ddo_contratante_razaosocial_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Filteredtext_set", StringUtil.RTrim( Ddo_contratante_razaosocial_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Selectedvalue_set", StringUtil.RTrim( Ddo_contratante_razaosocial_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratante_razaosocial_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratante_razaosocial_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Includesortasc", StringUtil.BoolToStr( Ddo_contratante_razaosocial_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Includesortdsc", StringUtil.BoolToStr( Ddo_contratante_razaosocial_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Sortedstatus", StringUtil.RTrim( Ddo_contratante_razaosocial_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Includefilter", StringUtil.BoolToStr( Ddo_contratante_razaosocial_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Filtertype", StringUtil.RTrim( Ddo_contratante_razaosocial_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Filterisrange", StringUtil.BoolToStr( Ddo_contratante_razaosocial_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Includedatalist", StringUtil.BoolToStr( Ddo_contratante_razaosocial_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Datalisttype", StringUtil.RTrim( Ddo_contratante_razaosocial_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Datalistproc", StringUtil.RTrim( Ddo_contratante_razaosocial_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratante_razaosocial_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Sortasc", StringUtil.RTrim( Ddo_contratante_razaosocial_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Sortdsc", StringUtil.RTrim( Ddo_contratante_razaosocial_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Loadingdata", StringUtil.RTrim( Ddo_contratante_razaosocial_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Cleanfilter", StringUtil.RTrim( Ddo_contratante_razaosocial_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Noresultsfound", StringUtil.RTrim( Ddo_contratante_razaosocial_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Searchbuttontext", StringUtil.RTrim( Ddo_contratante_razaosocial_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Caption", StringUtil.RTrim( Ddo_contratante_nomefantasia_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Tooltip", StringUtil.RTrim( Ddo_contratante_nomefantasia_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Cls", StringUtil.RTrim( Ddo_contratante_nomefantasia_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Filteredtext_set", StringUtil.RTrim( Ddo_contratante_nomefantasia_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Selectedvalue_set", StringUtil.RTrim( Ddo_contratante_nomefantasia_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratante_nomefantasia_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratante_nomefantasia_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Includesortasc", StringUtil.BoolToStr( Ddo_contratante_nomefantasia_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratante_nomefantasia_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Sortedstatus", StringUtil.RTrim( Ddo_contratante_nomefantasia_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Includefilter", StringUtil.BoolToStr( Ddo_contratante_nomefantasia_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Filtertype", StringUtil.RTrim( Ddo_contratante_nomefantasia_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Filterisrange", StringUtil.BoolToStr( Ddo_contratante_nomefantasia_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Includedatalist", StringUtil.BoolToStr( Ddo_contratante_nomefantasia_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Datalisttype", StringUtil.RTrim( Ddo_contratante_nomefantasia_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Datalistproc", StringUtil.RTrim( Ddo_contratante_nomefantasia_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratante_nomefantasia_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Sortasc", StringUtil.RTrim( Ddo_contratante_nomefantasia_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Sortdsc", StringUtil.RTrim( Ddo_contratante_nomefantasia_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Loadingdata", StringUtil.RTrim( Ddo_contratante_nomefantasia_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Cleanfilter", StringUtil.RTrim( Ddo_contratante_nomefantasia_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Noresultsfound", StringUtil.RTrim( Ddo_contratante_nomefantasia_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Searchbuttontext", StringUtil.RTrim( Ddo_contratante_nomefantasia_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Caption", StringUtil.RTrim( Ddo_contratante_cnpj_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Tooltip", StringUtil.RTrim( Ddo_contratante_cnpj_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Cls", StringUtil.RTrim( Ddo_contratante_cnpj_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Filteredtext_set", StringUtil.RTrim( Ddo_contratante_cnpj_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Selectedvalue_set", StringUtil.RTrim( Ddo_contratante_cnpj_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratante_cnpj_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratante_cnpj_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Includesortasc", StringUtil.BoolToStr( Ddo_contratante_cnpj_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Includesortdsc", StringUtil.BoolToStr( Ddo_contratante_cnpj_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Sortedstatus", StringUtil.RTrim( Ddo_contratante_cnpj_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Includefilter", StringUtil.BoolToStr( Ddo_contratante_cnpj_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Filtertype", StringUtil.RTrim( Ddo_contratante_cnpj_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Filterisrange", StringUtil.BoolToStr( Ddo_contratante_cnpj_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Includedatalist", StringUtil.BoolToStr( Ddo_contratante_cnpj_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Datalisttype", StringUtil.RTrim( Ddo_contratante_cnpj_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Datalistproc", StringUtil.RTrim( Ddo_contratante_cnpj_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratante_cnpj_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Sortasc", StringUtil.RTrim( Ddo_contratante_cnpj_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Sortdsc", StringUtil.RTrim( Ddo_contratante_cnpj_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Loadingdata", StringUtil.RTrim( Ddo_contratante_cnpj_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Cleanfilter", StringUtil.RTrim( Ddo_contratante_cnpj_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Noresultsfound", StringUtil.RTrim( Ddo_contratante_cnpj_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Searchbuttontext", StringUtil.RTrim( Ddo_contratante_cnpj_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Caption", StringUtil.RTrim( Ddo_contratante_telefone_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Tooltip", StringUtil.RTrim( Ddo_contratante_telefone_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Cls", StringUtil.RTrim( Ddo_contratante_telefone_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Filteredtext_set", StringUtil.RTrim( Ddo_contratante_telefone_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Selectedvalue_set", StringUtil.RTrim( Ddo_contratante_telefone_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratante_telefone_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratante_telefone_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Includesortasc", StringUtil.BoolToStr( Ddo_contratante_telefone_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Includesortdsc", StringUtil.BoolToStr( Ddo_contratante_telefone_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Sortedstatus", StringUtil.RTrim( Ddo_contratante_telefone_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Includefilter", StringUtil.BoolToStr( Ddo_contratante_telefone_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Filtertype", StringUtil.RTrim( Ddo_contratante_telefone_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Filterisrange", StringUtil.BoolToStr( Ddo_contratante_telefone_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Includedatalist", StringUtil.BoolToStr( Ddo_contratante_telefone_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Datalisttype", StringUtil.RTrim( Ddo_contratante_telefone_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Datalistproc", StringUtil.RTrim( Ddo_contratante_telefone_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratante_telefone_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Sortasc", StringUtil.RTrim( Ddo_contratante_telefone_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Sortdsc", StringUtil.RTrim( Ddo_contratante_telefone_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Loadingdata", StringUtil.RTrim( Ddo_contratante_telefone_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Cleanfilter", StringUtil.RTrim( Ddo_contratante_telefone_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Noresultsfound", StringUtil.RTrim( Ddo_contratante_telefone_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Searchbuttontext", StringUtil.RTrim( Ddo_contratante_telefone_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Caption", StringUtil.RTrim( Ddo_contratante_ramal_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Tooltip", StringUtil.RTrim( Ddo_contratante_ramal_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Cls", StringUtil.RTrim( Ddo_contratante_ramal_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Filteredtext_set", StringUtil.RTrim( Ddo_contratante_ramal_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Selectedvalue_set", StringUtil.RTrim( Ddo_contratante_ramal_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratante_ramal_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratante_ramal_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Includesortasc", StringUtil.BoolToStr( Ddo_contratante_ramal_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Includesortdsc", StringUtil.BoolToStr( Ddo_contratante_ramal_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Sortedstatus", StringUtil.RTrim( Ddo_contratante_ramal_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Includefilter", StringUtil.BoolToStr( Ddo_contratante_ramal_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Filtertype", StringUtil.RTrim( Ddo_contratante_ramal_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Filterisrange", StringUtil.BoolToStr( Ddo_contratante_ramal_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Includedatalist", StringUtil.BoolToStr( Ddo_contratante_ramal_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Datalisttype", StringUtil.RTrim( Ddo_contratante_ramal_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Datalistproc", StringUtil.RTrim( Ddo_contratante_ramal_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratante_ramal_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Sortasc", StringUtil.RTrim( Ddo_contratante_ramal_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Sortdsc", StringUtil.RTrim( Ddo_contratante_ramal_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Loadingdata", StringUtil.RTrim( Ddo_contratante_ramal_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Cleanfilter", StringUtil.RTrim( Ddo_contratante_ramal_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Noresultsfound", StringUtil.RTrim( Ddo_contratante_ramal_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Searchbuttontext", StringUtil.RTrim( Ddo_contratante_ramal_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Caption", StringUtil.RTrim( Ddo_municipio_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Tooltip", StringUtil.RTrim( Ddo_municipio_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Cls", StringUtil.RTrim( Ddo_municipio_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_municipio_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_municipio_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_municipio_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_municipio_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_municipio_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_municipio_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_municipio_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_municipio_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Filtertype", StringUtil.RTrim( Ddo_municipio_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_municipio_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_municipio_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Datalisttype", StringUtil.RTrim( Ddo_municipio_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Datalistproc", StringUtil.RTrim( Ddo_municipio_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_municipio_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Sortasc", StringUtil.RTrim( Ddo_municipio_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Sortdsc", StringUtil.RTrim( Ddo_municipio_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Loadingdata", StringUtil.RTrim( Ddo_municipio_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_municipio_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_municipio_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_municipio_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Caption", StringUtil.RTrim( Ddo_estado_uf_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Tooltip", StringUtil.RTrim( Ddo_estado_uf_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Cls", StringUtil.RTrim( Ddo_estado_uf_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Filteredtext_set", StringUtil.RTrim( Ddo_estado_uf_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Selectedvalue_set", StringUtil.RTrim( Ddo_estado_uf_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Dropdownoptionstype", StringUtil.RTrim( Ddo_estado_uf_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_estado_uf_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Includesortasc", StringUtil.BoolToStr( Ddo_estado_uf_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Includesortdsc", StringUtil.BoolToStr( Ddo_estado_uf_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Sortedstatus", StringUtil.RTrim( Ddo_estado_uf_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Includefilter", StringUtil.BoolToStr( Ddo_estado_uf_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Filtertype", StringUtil.RTrim( Ddo_estado_uf_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Filterisrange", StringUtil.BoolToStr( Ddo_estado_uf_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Includedatalist", StringUtil.BoolToStr( Ddo_estado_uf_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Datalisttype", StringUtil.RTrim( Ddo_estado_uf_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Datalistproc", StringUtil.RTrim( Ddo_estado_uf_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_estado_uf_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Sortasc", StringUtil.RTrim( Ddo_estado_uf_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Sortdsc", StringUtil.RTrim( Ddo_estado_uf_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Loadingdata", StringUtil.RTrim( Ddo_estado_uf_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Cleanfilter", StringUtil.RTrim( Ddo_estado_uf_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Noresultsfound", StringUtil.RTrim( Ddo_estado_uf_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Searchbuttontext", StringUtil.RTrim( Ddo_estado_uf_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Caption", StringUtil.RTrim( Ddo_contratante_iniciodoexpediente_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Tooltip", StringUtil.RTrim( Ddo_contratante_iniciodoexpediente_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Cls", StringUtil.RTrim( Ddo_contratante_iniciodoexpediente_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Filteredtext_set", StringUtil.RTrim( Ddo_contratante_iniciodoexpediente_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Filteredtextto_set", StringUtil.RTrim( Ddo_contratante_iniciodoexpediente_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratante_iniciodoexpediente_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratante_iniciodoexpediente_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Includesortasc", StringUtil.BoolToStr( Ddo_contratante_iniciodoexpediente_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Includesortdsc", StringUtil.BoolToStr( Ddo_contratante_iniciodoexpediente_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Sortedstatus", StringUtil.RTrim( Ddo_contratante_iniciodoexpediente_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Includefilter", StringUtil.BoolToStr( Ddo_contratante_iniciodoexpediente_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Filtertype", StringUtil.RTrim( Ddo_contratante_iniciodoexpediente_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Filterisrange", StringUtil.BoolToStr( Ddo_contratante_iniciodoexpediente_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Includedatalist", StringUtil.BoolToStr( Ddo_contratante_iniciodoexpediente_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Sortasc", StringUtil.RTrim( Ddo_contratante_iniciodoexpediente_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Sortdsc", StringUtil.RTrim( Ddo_contratante_iniciodoexpediente_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Cleanfilter", StringUtil.RTrim( Ddo_contratante_iniciodoexpediente_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Rangefilterfrom", StringUtil.RTrim( Ddo_contratante_iniciodoexpediente_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Rangefilterto", StringUtil.RTrim( Ddo_contratante_iniciodoexpediente_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Searchbuttontext", StringUtil.RTrim( Ddo_contratante_iniciodoexpediente_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Caption", StringUtil.RTrim( Ddo_contratante_fimdoexpediente_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Tooltip", StringUtil.RTrim( Ddo_contratante_fimdoexpediente_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Cls", StringUtil.RTrim( Ddo_contratante_fimdoexpediente_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Filteredtext_set", StringUtil.RTrim( Ddo_contratante_fimdoexpediente_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Filteredtextto_set", StringUtil.RTrim( Ddo_contratante_fimdoexpediente_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratante_fimdoexpediente_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratante_fimdoexpediente_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Includesortasc", StringUtil.BoolToStr( Ddo_contratante_fimdoexpediente_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Includesortdsc", StringUtil.BoolToStr( Ddo_contratante_fimdoexpediente_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Sortedstatus", StringUtil.RTrim( Ddo_contratante_fimdoexpediente_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Includefilter", StringUtil.BoolToStr( Ddo_contratante_fimdoexpediente_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Filtertype", StringUtil.RTrim( Ddo_contratante_fimdoexpediente_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Filterisrange", StringUtil.BoolToStr( Ddo_contratante_fimdoexpediente_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Includedatalist", StringUtil.BoolToStr( Ddo_contratante_fimdoexpediente_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Sortasc", StringUtil.RTrim( Ddo_contratante_fimdoexpediente_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Sortdsc", StringUtil.RTrim( Ddo_contratante_fimdoexpediente_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Cleanfilter", StringUtil.RTrim( Ddo_contratante_fimdoexpediente_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Rangefilterfrom", StringUtil.RTrim( Ddo_contratante_fimdoexpediente_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Rangefilterto", StringUtil.RTrim( Ddo_contratante_fimdoexpediente_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Searchbuttontext", StringUtil.RTrim( Ddo_contratante_fimdoexpediente_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Activeeventkey", StringUtil.RTrim( Ddo_contratante_razaosocial_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Filteredtext_get", StringUtil.RTrim( Ddo_contratante_razaosocial_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Selectedvalue_get", StringUtil.RTrim( Ddo_contratante_razaosocial_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Activeeventkey", StringUtil.RTrim( Ddo_contratante_nomefantasia_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Filteredtext_get", StringUtil.RTrim( Ddo_contratante_nomefantasia_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_NOMEFANTASIA_Selectedvalue_get", StringUtil.RTrim( Ddo_contratante_nomefantasia_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Activeeventkey", StringUtil.RTrim( Ddo_contratante_cnpj_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Filteredtext_get", StringUtil.RTrim( Ddo_contratante_cnpj_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Selectedvalue_get", StringUtil.RTrim( Ddo_contratante_cnpj_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Activeeventkey", StringUtil.RTrim( Ddo_contratante_telefone_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Filteredtext_get", StringUtil.RTrim( Ddo_contratante_telefone_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Selectedvalue_get", StringUtil.RTrim( Ddo_contratante_telefone_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Activeeventkey", StringUtil.RTrim( Ddo_contratante_ramal_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Filteredtext_get", StringUtil.RTrim( Ddo_contratante_ramal_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAMAL_Selectedvalue_get", StringUtil.RTrim( Ddo_contratante_ramal_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_municipio_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_municipio_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_municipio_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Activeeventkey", StringUtil.RTrim( Ddo_estado_uf_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Filteredtext_get", StringUtil.RTrim( Ddo_estado_uf_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Selectedvalue_get", StringUtil.RTrim( Ddo_estado_uf_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Activeeventkey", StringUtil.RTrim( Ddo_contratante_iniciodoexpediente_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Filteredtext_get", StringUtil.RTrim( Ddo_contratante_iniciodoexpediente_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Filteredtextto_get", StringUtil.RTrim( Ddo_contratante_iniciodoexpediente_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Activeeventkey", StringUtil.RTrim( Ddo_contratante_fimdoexpediente_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Filteredtext_get", StringUtil.RTrim( Ddo_contratante_fimdoexpediente_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Filteredtextto_get", StringUtil.RTrim( Ddo_contratante_fimdoexpediente_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE0Y2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT0Y2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontratante.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContratante" ;
      }

      public override String GetPgmdesc( )
      {
         return " Contratante" ;
      }

      protected void WB0Y0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_0Y2( true) ;
         }
         else
         {
            wb_table1_2_0Y2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_0Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratante.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContratante.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV61ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratante.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_razaosocial_Internalname, StringUtil.RTrim( AV79TFContratante_RazaoSocial), StringUtil.RTrim( context.localUtil.Format( AV79TFContratante_RazaoSocial, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_razaosocial_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratante_razaosocial_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratante.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_razaosocial_sel_Internalname, StringUtil.RTrim( AV80TFContratante_RazaoSocial_Sel), StringUtil.RTrim( context.localUtil.Format( AV80TFContratante_RazaoSocial_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_razaosocial_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratante_razaosocial_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratante.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_nomefantasia_Internalname, StringUtil.RTrim( AV83TFContratante_NomeFantasia), StringUtil.RTrim( context.localUtil.Format( AV83TFContratante_NomeFantasia, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_nomefantasia_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratante_nomefantasia_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratante.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_nomefantasia_sel_Internalname, StringUtil.RTrim( AV84TFContratante_NomeFantasia_Sel), StringUtil.RTrim( context.localUtil.Format( AV84TFContratante_NomeFantasia_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_nomefantasia_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratante_nomefantasia_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratante.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_cnpj_Internalname, AV87TFContratante_CNPJ, StringUtil.RTrim( context.localUtil.Format( AV87TFContratante_CNPJ, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_cnpj_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratante_cnpj_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratante.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_cnpj_sel_Internalname, AV88TFContratante_CNPJ_Sel, StringUtil.RTrim( context.localUtil.Format( AV88TFContratante_CNPJ_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_cnpj_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratante_cnpj_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratante.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_telefone_Internalname, StringUtil.RTrim( AV91TFContratante_Telefone), StringUtil.RTrim( context.localUtil.Format( AV91TFContratante_Telefone, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_telefone_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratante_telefone_Visible, 1, 0, "text", "", 100, "px", 1, "row", 20, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WWContratante.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_telefone_sel_Internalname, StringUtil.RTrim( AV92TFContratante_Telefone_Sel), StringUtil.RTrim( context.localUtil.Format( AV92TFContratante_Telefone_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_telefone_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratante_telefone_sel_Visible, 1, 0, "text", "", 100, "px", 1, "row", 20, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WWContratante.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_ramal_Internalname, StringUtil.RTrim( AV95TFContratante_Ramal), StringUtil.RTrim( context.localUtil.Format( AV95TFContratante_Ramal, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_ramal_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratante_ramal_Visible, 1, 0, "text", "", 100, "px", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratante.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_ramal_sel_Internalname, StringUtil.RTrim( AV96TFContratante_Ramal_Sel), StringUtil.RTrim( context.localUtil.Format( AV96TFContratante_Ramal_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_ramal_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratante_ramal_sel_Visible, 1, 0, "text", "", 100, "px", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratante.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmunicipio_nome_Internalname, StringUtil.RTrim( AV99TFMunicipio_Nome), StringUtil.RTrim( context.localUtil.Format( AV99TFMunicipio_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmunicipio_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfmunicipio_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratante.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmunicipio_nome_sel_Internalname, StringUtil.RTrim( AV100TFMunicipio_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV100TFMunicipio_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmunicipio_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfmunicipio_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratante.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfestado_uf_Internalname, StringUtil.RTrim( AV103TFEstado_UF), StringUtil.RTrim( context.localUtil.Format( AV103TFEstado_UF, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfestado_uf_Jsonclick, 0, "Attribute", "", "", "", edtavTfestado_uf_Visible, 1, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratante.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfestado_uf_sel_Internalname, StringUtil.RTrim( AV104TFEstado_UF_Sel), StringUtil.RTrim( context.localUtil.Format( AV104TFEstado_UF_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfestado_uf_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfestado_uf_sel_Visible, 1, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratante.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_32_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratante_iniciodoexpediente_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_iniciodoexpediente_Internalname, context.localUtil.TToC( AV107TFContratante_InicioDoExpediente, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV107TFContratante_InicioDoExpediente, "99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 0,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_iniciodoexpediente_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratante_iniciodoexpediente_Visible, 1, 0, "text", "", 40, "px", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratante.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratante_iniciodoexpediente_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratante_iniciodoexpediente_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratante.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_32_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratante_iniciodoexpediente_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_iniciodoexpediente_to_Internalname, context.localUtil.TToC( AV108TFContratante_InicioDoExpediente_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV108TFContratante_InicioDoExpediente_To, "99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 0,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_iniciodoexpediente_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratante_iniciodoexpediente_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratante.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratante_iniciodoexpediente_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratante_iniciodoexpediente_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratante.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratante_iniciodoexpedienteauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_32_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratante_iniciodoexpedienteauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratante_iniciodoexpedienteauxdate_Internalname, context.localUtil.Format(AV109DDO_Contratante_InicioDoExpedienteAuxDate, "99/99/99"), context.localUtil.Format( AV109DDO_Contratante_InicioDoExpedienteAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratante_iniciodoexpedienteauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratante.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratante_iniciodoexpedienteauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratante.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_32_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratante_iniciodoexpedienteauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratante_iniciodoexpedienteauxdateto_Internalname, context.localUtil.Format(AV110DDO_Contratante_InicioDoExpedienteAuxDateTo, "99/99/99"), context.localUtil.Format( AV110DDO_Contratante_InicioDoExpedienteAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratante_iniciodoexpedienteauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratante.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratante_iniciodoexpedienteauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratante.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_32_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratante_fimdoexpediente_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_fimdoexpediente_Internalname, context.localUtil.TToC( AV113TFContratante_FimDoExpediente, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV113TFContratante_FimDoExpediente, "99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 0,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_fimdoexpediente_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratante_fimdoexpediente_Visible, 1, 0, "text", "", 40, "px", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratante.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratante_fimdoexpediente_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratante_fimdoexpediente_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratante.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_32_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratante_fimdoexpediente_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_fimdoexpediente_to_Internalname, context.localUtil.TToC( AV114TFContratante_FimDoExpediente_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV114TFContratante_FimDoExpediente_To, "99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 0,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_fimdoexpediente_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratante_fimdoexpediente_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratante.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratante_fimdoexpediente_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratante_fimdoexpediente_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratante.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratante_fimdoexpedienteauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_32_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratante_fimdoexpedienteauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratante_fimdoexpedienteauxdate_Internalname, context.localUtil.Format(AV115DDO_Contratante_FimDoExpedienteAuxDate, "99/99/99"), context.localUtil.Format( AV115DDO_Contratante_FimDoExpedienteAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratante_fimdoexpedienteauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratante.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratante_fimdoexpedienteauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratante.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_32_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratante_fimdoexpedienteauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratante_fimdoexpedienteauxdateto_Internalname, context.localUtil.Format(AV116DDO_Contratante_FimDoExpedienteAuxDateTo, "99/99/99"), context.localUtil.Format( AV116DDO_Contratante_FimDoExpedienteAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratante_fimdoexpedienteauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratante.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratante_fimdoexpedienteauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratante.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATANTE_RAZAOSOCIALContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_32_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Internalname, AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"", 0, edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratante.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATANTE_NOMEFANTASIAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_32_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratante_nomefantasiatitlecontrolidtoreplace_Internalname, AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,82);\"", 0, edtavDdo_contratante_nomefantasiatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratante.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATANTE_CNPJContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_32_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Internalname, AV89ddo_Contratante_CNPJTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"", 0, edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratante.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATANTE_TELEFONEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_32_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratante_telefonetitlecontrolidtoreplace_Internalname, AV93ddo_Contratante_TelefoneTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,86);\"", 0, edtavDdo_contratante_telefonetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratante.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATANTE_RAMALContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_32_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratante_ramaltitlecontrolidtoreplace_Internalname, AV97ddo_Contratante_RamalTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,88);\"", 0, edtavDdo_contratante_ramaltitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratante.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MUNICIPIO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_32_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname, AV101ddo_Municipio_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"", 0, edtavDdo_municipio_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratante.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_ESTADO_UFContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_32_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_estado_uftitlecontrolidtoreplace_Internalname, AV105ddo_Estado_UFTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", 0, edtavDdo_estado_uftitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratante.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATANTE_INICIODOEXPEDIENTEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_32_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratante_iniciodoexpedientetitlecontrolidtoreplace_Internalname, AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"", 0, edtavDdo_contratante_iniciodoexpedientetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratante.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATANTE_FIMDOEXPEDIENTEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_32_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratante_fimdoexpedientetitlecontrolidtoreplace_Internalname, AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"", 0, edtavDdo_contratante_fimdoexpedientetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratante.htm");
         }
         wbLoad = true;
      }

      protected void START0Y2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Contratante", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP0Y0( ) ;
      }

      protected void WS0Y2( )
      {
         START0Y2( ) ;
         EVT0Y2( ) ;
      }

      protected void EVT0Y2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E110Y2 */
                              E110Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E120Y2 */
                              E120Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATANTE_RAZAOSOCIAL.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E130Y2 */
                              E130Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATANTE_NOMEFANTASIA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E140Y2 */
                              E140Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATANTE_CNPJ.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E150Y2 */
                              E150Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATANTE_TELEFONE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E160Y2 */
                              E160Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATANTE_RAMAL.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E170Y2 */
                              E170Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MUNICIPIO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E180Y2 */
                              E180Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ESTADO_UF.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E190Y2 */
                              E190Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATANTE_INICIODOEXPEDIENTE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E200Y2 */
                              E200Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATANTE_FIMDOEXPEDIENTE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E210Y2 */
                              E210Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_32_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_32_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_32_idx), 4, 0)), 4, "0");
                              SubsflControlProps_322( ) ;
                              AV31Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV162Update_GXI : context.convertURL( context.PathToRelativeUrl( AV31Update))));
                              AV32Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV163Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV32Delete))));
                              AV68Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV68Display)) ? AV164Display_GXI : context.convertURL( context.PathToRelativeUrl( AV68Display))));
                              A5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_Codigo_Internalname), ",", "."));
                              A29Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratante_Codigo_Internalname), ",", "."));
                              n29Contratante_Codigo = false;
                              A335Contratante_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratante_PessoaCod_Internalname), ",", "."));
                              A9Contratante_RazaoSocial = StringUtil.Upper( cgiGet( edtContratante_RazaoSocial_Internalname));
                              n9Contratante_RazaoSocial = false;
                              A10Contratante_NomeFantasia = StringUtil.Upper( cgiGet( edtContratante_NomeFantasia_Internalname));
                              A12Contratante_CNPJ = cgiGet( edtContratante_CNPJ_Internalname);
                              n12Contratante_CNPJ = false;
                              A31Contratante_Telefone = cgiGet( edtContratante_Telefone_Internalname);
                              A32Contratante_Ramal = cgiGet( edtContratante_Ramal_Internalname);
                              n32Contratante_Ramal = false;
                              A26Municipio_Nome = StringUtil.Upper( cgiGet( edtMunicipio_Nome_Internalname));
                              A23Estado_UF = StringUtil.Upper( cgiGet( edtEstado_UF_Internalname));
                              A1448Contratante_InicioDoExpediente = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( edtContratante_InicioDoExpediente_Internalname), 0));
                              n1448Contratante_InicioDoExpediente = false;
                              A1192Contratante_FimDoExpediente = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( edtContratante_FimDoExpediente_Internalname), 0));
                              n1192Contratante_FimDoExpediente = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E220Y2 */
                                    E220Y2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E230Y2 */
                                    E230Y2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E240Y2 */
                                    E240Y2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_razaosocial Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_RAZAOSOCIAL"), AV79TFContratante_RazaoSocial) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_razaosocial_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_RAZAOSOCIAL_SEL"), AV80TFContratante_RazaoSocial_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_nomefantasia Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_NOMEFANTASIA"), AV83TFContratante_NomeFantasia) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_nomefantasia_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_NOMEFANTASIA_SEL"), AV84TFContratante_NomeFantasia_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_cnpj Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_CNPJ"), AV87TFContratante_CNPJ) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_cnpj_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_CNPJ_SEL"), AV88TFContratante_CNPJ_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_telefone Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_TELEFONE"), AV91TFContratante_Telefone) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_telefone_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_TELEFONE_SEL"), AV92TFContratante_Telefone_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_ramal Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_RAMAL"), AV95TFContratante_Ramal) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_ramal_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_RAMAL_SEL"), AV96TFContratante_Ramal_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmunicipio_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMUNICIPIO_NOME"), AV99TFMunicipio_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmunicipio_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMUNICIPIO_NOME_SEL"), AV100TFMunicipio_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfestado_uf Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_UF"), AV103TFEstado_UF) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfestado_uf_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_UF_SEL"), AV104TFEstado_UF_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_iniciodoexpediente Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATANTE_INICIODOEXPEDIENTE"), 0) != AV107TFContratante_InicioDoExpediente )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_iniciodoexpediente_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATANTE_INICIODOEXPEDIENTE_TO"), 0) != AV108TFContratante_InicioDoExpediente_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_fimdoexpediente Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATANTE_FIMDOEXPEDIENTE"), 0) != AV113TFContratante_FimDoExpediente )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_fimdoexpediente_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATANTE_FIMDOEXPEDIENTE_TO"), 0) != AV114TFContratante_FimDoExpediente_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE0Y2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA0Y2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavAreatrabalho_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_322( ) ;
         while ( nGXsfl_32_idx <= nRC_GXsfl_32 )
         {
            sendrow_322( ) ;
            nGXsfl_32_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_32_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_32_idx+1));
            sGXsfl_32_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_32_idx), 4, 0)), 4, "0");
            SubsflControlProps_322( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV79TFContratante_RazaoSocial ,
                                       String AV80TFContratante_RazaoSocial_Sel ,
                                       String AV83TFContratante_NomeFantasia ,
                                       String AV84TFContratante_NomeFantasia_Sel ,
                                       String AV87TFContratante_CNPJ ,
                                       String AV88TFContratante_CNPJ_Sel ,
                                       String AV91TFContratante_Telefone ,
                                       String AV92TFContratante_Telefone_Sel ,
                                       String AV95TFContratante_Ramal ,
                                       String AV96TFContratante_Ramal_Sel ,
                                       String AV99TFMunicipio_Nome ,
                                       String AV100TFMunicipio_Nome_Sel ,
                                       String AV103TFEstado_UF ,
                                       String AV104TFEstado_UF_Sel ,
                                       DateTime AV107TFContratante_InicioDoExpediente ,
                                       DateTime AV108TFContratante_InicioDoExpediente_To ,
                                       DateTime AV113TFContratante_FimDoExpediente ,
                                       DateTime AV114TFContratante_FimDoExpediente_To ,
                                       short AV61ManageFiltersExecutionStep ,
                                       String AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace ,
                                       String AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace ,
                                       String AV89ddo_Contratante_CNPJTitleControlIdToReplace ,
                                       String AV93ddo_Contratante_TelefoneTitleControlIdToReplace ,
                                       String AV97ddo_Contratante_RamalTitleControlIdToReplace ,
                                       String AV101ddo_Municipio_NomeTitleControlIdToReplace ,
                                       String AV105ddo_Estado_UFTitleControlIdToReplace ,
                                       String AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace ,
                                       String AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace ,
                                       int AV39AreaTrabalho_Codigo ,
                                       String AV165Pgmname ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int A29Contratante_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF0Y2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0Y2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV165Pgmname = "WWContratante";
         context.Gx_err = 0;
      }

      protected void RF0Y2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 32;
         /* Execute user event: E230Y2 */
         E230Y2 ();
         nGXsfl_32_idx = 1;
         sGXsfl_32_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_32_idx), 4, 0)), 4, "0");
         SubsflControlProps_322( ) ;
         nGXsfl_32_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_322( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel ,
                                                 AV144WWContratanteDS_2_Tfcontratante_razaosocial ,
                                                 AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel ,
                                                 AV146WWContratanteDS_4_Tfcontratante_nomefantasia ,
                                                 AV149WWContratanteDS_7_Tfcontratante_cnpj_sel ,
                                                 AV148WWContratanteDS_6_Tfcontratante_cnpj ,
                                                 AV151WWContratanteDS_9_Tfcontratante_telefone_sel ,
                                                 AV150WWContratanteDS_8_Tfcontratante_telefone ,
                                                 AV153WWContratanteDS_11_Tfcontratante_ramal_sel ,
                                                 AV152WWContratanteDS_10_Tfcontratante_ramal ,
                                                 AV155WWContratanteDS_13_Tfmunicipio_nome_sel ,
                                                 AV154WWContratanteDS_12_Tfmunicipio_nome ,
                                                 AV157WWContratanteDS_15_Tfestado_uf_sel ,
                                                 AV156WWContratanteDS_14_Tfestado_uf ,
                                                 AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente ,
                                                 AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ,
                                                 AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente ,
                                                 AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ,
                                                 A9Contratante_RazaoSocial ,
                                                 A10Contratante_NomeFantasia ,
                                                 A12Contratante_CNPJ ,
                                                 A31Contratante_Telefone ,
                                                 A32Contratante_Ramal ,
                                                 A26Municipio_Nome ,
                                                 A23Estado_UF ,
                                                 A1448Contratante_InicioDoExpediente ,
                                                 A1192Contratante_FimDoExpediente ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A5AreaTrabalho_Codigo ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                                 TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV144WWContratanteDS_2_Tfcontratante_razaosocial = StringUtil.PadR( StringUtil.RTrim( AV144WWContratanteDS_2_Tfcontratante_razaosocial), 100, "%");
            lV146WWContratanteDS_4_Tfcontratante_nomefantasia = StringUtil.PadR( StringUtil.RTrim( AV146WWContratanteDS_4_Tfcontratante_nomefantasia), 100, "%");
            lV148WWContratanteDS_6_Tfcontratante_cnpj = StringUtil.Concat( StringUtil.RTrim( AV148WWContratanteDS_6_Tfcontratante_cnpj), "%", "");
            lV150WWContratanteDS_8_Tfcontratante_telefone = StringUtil.PadR( StringUtil.RTrim( AV150WWContratanteDS_8_Tfcontratante_telefone), 20, "%");
            lV152WWContratanteDS_10_Tfcontratante_ramal = StringUtil.PadR( StringUtil.RTrim( AV152WWContratanteDS_10_Tfcontratante_ramal), 10, "%");
            lV154WWContratanteDS_12_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV154WWContratanteDS_12_Tfmunicipio_nome), 50, "%");
            lV156WWContratanteDS_14_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV156WWContratanteDS_14_Tfestado_uf), 2, "%");
            /* Using cursor H000Y2 */
            pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV144WWContratanteDS_2_Tfcontratante_razaosocial, AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel, lV146WWContratanteDS_4_Tfcontratante_nomefantasia, AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel, lV148WWContratanteDS_6_Tfcontratante_cnpj, AV149WWContratanteDS_7_Tfcontratante_cnpj_sel, lV150WWContratanteDS_8_Tfcontratante_telefone, AV151WWContratanteDS_9_Tfcontratante_telefone_sel, lV152WWContratanteDS_10_Tfcontratante_ramal, AV153WWContratanteDS_11_Tfcontratante_ramal_sel, lV154WWContratanteDS_12_Tfmunicipio_nome, AV155WWContratanteDS_13_Tfmunicipio_nome_sel, lV156WWContratanteDS_14_Tfestado_uf, AV157WWContratanteDS_15_Tfestado_uf_sel, AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente, AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to, AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente, AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_32_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A25Municipio_Codigo = H000Y2_A25Municipio_Codigo[0];
               n25Municipio_Codigo = H000Y2_n25Municipio_Codigo[0];
               A1192Contratante_FimDoExpediente = H000Y2_A1192Contratante_FimDoExpediente[0];
               n1192Contratante_FimDoExpediente = H000Y2_n1192Contratante_FimDoExpediente[0];
               A1448Contratante_InicioDoExpediente = H000Y2_A1448Contratante_InicioDoExpediente[0];
               n1448Contratante_InicioDoExpediente = H000Y2_n1448Contratante_InicioDoExpediente[0];
               A23Estado_UF = H000Y2_A23Estado_UF[0];
               A26Municipio_Nome = H000Y2_A26Municipio_Nome[0];
               A32Contratante_Ramal = H000Y2_A32Contratante_Ramal[0];
               n32Contratante_Ramal = H000Y2_n32Contratante_Ramal[0];
               A31Contratante_Telefone = H000Y2_A31Contratante_Telefone[0];
               A12Contratante_CNPJ = H000Y2_A12Contratante_CNPJ[0];
               n12Contratante_CNPJ = H000Y2_n12Contratante_CNPJ[0];
               A10Contratante_NomeFantasia = H000Y2_A10Contratante_NomeFantasia[0];
               A9Contratante_RazaoSocial = H000Y2_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = H000Y2_n9Contratante_RazaoSocial[0];
               A335Contratante_PessoaCod = H000Y2_A335Contratante_PessoaCod[0];
               A29Contratante_Codigo = H000Y2_A29Contratante_Codigo[0];
               n29Contratante_Codigo = H000Y2_n29Contratante_Codigo[0];
               A5AreaTrabalho_Codigo = H000Y2_A5AreaTrabalho_Codigo[0];
               A25Municipio_Codigo = H000Y2_A25Municipio_Codigo[0];
               n25Municipio_Codigo = H000Y2_n25Municipio_Codigo[0];
               A1192Contratante_FimDoExpediente = H000Y2_A1192Contratante_FimDoExpediente[0];
               n1192Contratante_FimDoExpediente = H000Y2_n1192Contratante_FimDoExpediente[0];
               A1448Contratante_InicioDoExpediente = H000Y2_A1448Contratante_InicioDoExpediente[0];
               n1448Contratante_InicioDoExpediente = H000Y2_n1448Contratante_InicioDoExpediente[0];
               A32Contratante_Ramal = H000Y2_A32Contratante_Ramal[0];
               n32Contratante_Ramal = H000Y2_n32Contratante_Ramal[0];
               A31Contratante_Telefone = H000Y2_A31Contratante_Telefone[0];
               A10Contratante_NomeFantasia = H000Y2_A10Contratante_NomeFantasia[0];
               A335Contratante_PessoaCod = H000Y2_A335Contratante_PessoaCod[0];
               A23Estado_UF = H000Y2_A23Estado_UF[0];
               A26Municipio_Nome = H000Y2_A26Municipio_Nome[0];
               A12Contratante_CNPJ = H000Y2_A12Contratante_CNPJ[0];
               n12Contratante_CNPJ = H000Y2_n12Contratante_CNPJ[0];
               A9Contratante_RazaoSocial = H000Y2_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = H000Y2_n9Contratante_RazaoSocial[0];
               /* Execute user event: E240Y2 */
               E240Y2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 32;
            WB0Y0( ) ;
         }
         nGXsfl_32_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV143WWContratanteDS_1_Areatrabalho_codigo = AV39AreaTrabalho_Codigo;
         AV144WWContratanteDS_2_Tfcontratante_razaosocial = AV79TFContratante_RazaoSocial;
         AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel = AV80TFContratante_RazaoSocial_Sel;
         AV146WWContratanteDS_4_Tfcontratante_nomefantasia = AV83TFContratante_NomeFantasia;
         AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel = AV84TFContratante_NomeFantasia_Sel;
         AV148WWContratanteDS_6_Tfcontratante_cnpj = AV87TFContratante_CNPJ;
         AV149WWContratanteDS_7_Tfcontratante_cnpj_sel = AV88TFContratante_CNPJ_Sel;
         AV150WWContratanteDS_8_Tfcontratante_telefone = AV91TFContratante_Telefone;
         AV151WWContratanteDS_9_Tfcontratante_telefone_sel = AV92TFContratante_Telefone_Sel;
         AV152WWContratanteDS_10_Tfcontratante_ramal = AV95TFContratante_Ramal;
         AV153WWContratanteDS_11_Tfcontratante_ramal_sel = AV96TFContratante_Ramal_Sel;
         AV154WWContratanteDS_12_Tfmunicipio_nome = AV99TFMunicipio_Nome;
         AV155WWContratanteDS_13_Tfmunicipio_nome_sel = AV100TFMunicipio_Nome_Sel;
         AV156WWContratanteDS_14_Tfestado_uf = AV103TFEstado_UF;
         AV157WWContratanteDS_15_Tfestado_uf_sel = AV104TFEstado_UF_Sel;
         AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente = AV107TFContratante_InicioDoExpediente;
         AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to = AV108TFContratante_InicioDoExpediente_To;
         AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente = AV113TFContratante_FimDoExpediente;
         AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to = AV114TFContratante_FimDoExpediente_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel ,
                                              AV144WWContratanteDS_2_Tfcontratante_razaosocial ,
                                              AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel ,
                                              AV146WWContratanteDS_4_Tfcontratante_nomefantasia ,
                                              AV149WWContratanteDS_7_Tfcontratante_cnpj_sel ,
                                              AV148WWContratanteDS_6_Tfcontratante_cnpj ,
                                              AV151WWContratanteDS_9_Tfcontratante_telefone_sel ,
                                              AV150WWContratanteDS_8_Tfcontratante_telefone ,
                                              AV153WWContratanteDS_11_Tfcontratante_ramal_sel ,
                                              AV152WWContratanteDS_10_Tfcontratante_ramal ,
                                              AV155WWContratanteDS_13_Tfmunicipio_nome_sel ,
                                              AV154WWContratanteDS_12_Tfmunicipio_nome ,
                                              AV157WWContratanteDS_15_Tfestado_uf_sel ,
                                              AV156WWContratanteDS_14_Tfestado_uf ,
                                              AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente ,
                                              AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ,
                                              AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente ,
                                              AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ,
                                              A9Contratante_RazaoSocial ,
                                              A10Contratante_NomeFantasia ,
                                              A12Contratante_CNPJ ,
                                              A31Contratante_Telefone ,
                                              A32Contratante_Ramal ,
                                              A26Municipio_Nome ,
                                              A23Estado_UF ,
                                              A1448Contratante_InicioDoExpediente ,
                                              A1192Contratante_FimDoExpediente ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A5AreaTrabalho_Codigo ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV144WWContratanteDS_2_Tfcontratante_razaosocial = StringUtil.PadR( StringUtil.RTrim( AV144WWContratanteDS_2_Tfcontratante_razaosocial), 100, "%");
         lV146WWContratanteDS_4_Tfcontratante_nomefantasia = StringUtil.PadR( StringUtil.RTrim( AV146WWContratanteDS_4_Tfcontratante_nomefantasia), 100, "%");
         lV148WWContratanteDS_6_Tfcontratante_cnpj = StringUtil.Concat( StringUtil.RTrim( AV148WWContratanteDS_6_Tfcontratante_cnpj), "%", "");
         lV150WWContratanteDS_8_Tfcontratante_telefone = StringUtil.PadR( StringUtil.RTrim( AV150WWContratanteDS_8_Tfcontratante_telefone), 20, "%");
         lV152WWContratanteDS_10_Tfcontratante_ramal = StringUtil.PadR( StringUtil.RTrim( AV152WWContratanteDS_10_Tfcontratante_ramal), 10, "%");
         lV154WWContratanteDS_12_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV154WWContratanteDS_12_Tfmunicipio_nome), 50, "%");
         lV156WWContratanteDS_14_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV156WWContratanteDS_14_Tfestado_uf), 2, "%");
         /* Using cursor H000Y3 */
         pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV144WWContratanteDS_2_Tfcontratante_razaosocial, AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel, lV146WWContratanteDS_4_Tfcontratante_nomefantasia, AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel, lV148WWContratanteDS_6_Tfcontratante_cnpj, AV149WWContratanteDS_7_Tfcontratante_cnpj_sel, lV150WWContratanteDS_8_Tfcontratante_telefone, AV151WWContratanteDS_9_Tfcontratante_telefone_sel, lV152WWContratanteDS_10_Tfcontratante_ramal, AV153WWContratanteDS_11_Tfcontratante_ramal_sel, lV154WWContratanteDS_12_Tfmunicipio_nome, AV155WWContratanteDS_13_Tfmunicipio_nome_sel, lV156WWContratanteDS_14_Tfestado_uf, AV157WWContratanteDS_15_Tfestado_uf_sel, AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente, AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to, AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente, AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to});
         GRID_nRecordCount = H000Y3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV143WWContratanteDS_1_Areatrabalho_codigo = AV39AreaTrabalho_Codigo;
         AV144WWContratanteDS_2_Tfcontratante_razaosocial = AV79TFContratante_RazaoSocial;
         AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel = AV80TFContratante_RazaoSocial_Sel;
         AV146WWContratanteDS_4_Tfcontratante_nomefantasia = AV83TFContratante_NomeFantasia;
         AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel = AV84TFContratante_NomeFantasia_Sel;
         AV148WWContratanteDS_6_Tfcontratante_cnpj = AV87TFContratante_CNPJ;
         AV149WWContratanteDS_7_Tfcontratante_cnpj_sel = AV88TFContratante_CNPJ_Sel;
         AV150WWContratanteDS_8_Tfcontratante_telefone = AV91TFContratante_Telefone;
         AV151WWContratanteDS_9_Tfcontratante_telefone_sel = AV92TFContratante_Telefone_Sel;
         AV152WWContratanteDS_10_Tfcontratante_ramal = AV95TFContratante_Ramal;
         AV153WWContratanteDS_11_Tfcontratante_ramal_sel = AV96TFContratante_Ramal_Sel;
         AV154WWContratanteDS_12_Tfmunicipio_nome = AV99TFMunicipio_Nome;
         AV155WWContratanteDS_13_Tfmunicipio_nome_sel = AV100TFMunicipio_Nome_Sel;
         AV156WWContratanteDS_14_Tfestado_uf = AV103TFEstado_UF;
         AV157WWContratanteDS_15_Tfestado_uf_sel = AV104TFEstado_UF_Sel;
         AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente = AV107TFContratante_InicioDoExpediente;
         AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to = AV108TFContratante_InicioDoExpediente_To;
         AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente = AV113TFContratante_FimDoExpediente;
         AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to = AV114TFContratante_FimDoExpediente_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV79TFContratante_RazaoSocial, AV80TFContratante_RazaoSocial_Sel, AV83TFContratante_NomeFantasia, AV84TFContratante_NomeFantasia_Sel, AV87TFContratante_CNPJ, AV88TFContratante_CNPJ_Sel, AV91TFContratante_Telefone, AV92TFContratante_Telefone_Sel, AV95TFContratante_Ramal, AV96TFContratante_Ramal_Sel, AV99TFMunicipio_Nome, AV100TFMunicipio_Nome_Sel, AV103TFEstado_UF, AV104TFEstado_UF_Sel, AV107TFContratante_InicioDoExpediente, AV108TFContratante_InicioDoExpediente_To, AV113TFContratante_FimDoExpediente, AV114TFContratante_FimDoExpediente_To, AV61ManageFiltersExecutionStep, AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace, AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace, AV89ddo_Contratante_CNPJTitleControlIdToReplace, AV93ddo_Contratante_TelefoneTitleControlIdToReplace, AV97ddo_Contratante_RamalTitleControlIdToReplace, AV101ddo_Municipio_NomeTitleControlIdToReplace, AV105ddo_Estado_UFTitleControlIdToReplace, AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace, AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace, AV39AreaTrabalho_Codigo, AV165Pgmname, AV6WWPContext, A29Contratante_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV143WWContratanteDS_1_Areatrabalho_codigo = AV39AreaTrabalho_Codigo;
         AV144WWContratanteDS_2_Tfcontratante_razaosocial = AV79TFContratante_RazaoSocial;
         AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel = AV80TFContratante_RazaoSocial_Sel;
         AV146WWContratanteDS_4_Tfcontratante_nomefantasia = AV83TFContratante_NomeFantasia;
         AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel = AV84TFContratante_NomeFantasia_Sel;
         AV148WWContratanteDS_6_Tfcontratante_cnpj = AV87TFContratante_CNPJ;
         AV149WWContratanteDS_7_Tfcontratante_cnpj_sel = AV88TFContratante_CNPJ_Sel;
         AV150WWContratanteDS_8_Tfcontratante_telefone = AV91TFContratante_Telefone;
         AV151WWContratanteDS_9_Tfcontratante_telefone_sel = AV92TFContratante_Telefone_Sel;
         AV152WWContratanteDS_10_Tfcontratante_ramal = AV95TFContratante_Ramal;
         AV153WWContratanteDS_11_Tfcontratante_ramal_sel = AV96TFContratante_Ramal_Sel;
         AV154WWContratanteDS_12_Tfmunicipio_nome = AV99TFMunicipio_Nome;
         AV155WWContratanteDS_13_Tfmunicipio_nome_sel = AV100TFMunicipio_Nome_Sel;
         AV156WWContratanteDS_14_Tfestado_uf = AV103TFEstado_UF;
         AV157WWContratanteDS_15_Tfestado_uf_sel = AV104TFEstado_UF_Sel;
         AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente = AV107TFContratante_InicioDoExpediente;
         AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to = AV108TFContratante_InicioDoExpediente_To;
         AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente = AV113TFContratante_FimDoExpediente;
         AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to = AV114TFContratante_FimDoExpediente_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV79TFContratante_RazaoSocial, AV80TFContratante_RazaoSocial_Sel, AV83TFContratante_NomeFantasia, AV84TFContratante_NomeFantasia_Sel, AV87TFContratante_CNPJ, AV88TFContratante_CNPJ_Sel, AV91TFContratante_Telefone, AV92TFContratante_Telefone_Sel, AV95TFContratante_Ramal, AV96TFContratante_Ramal_Sel, AV99TFMunicipio_Nome, AV100TFMunicipio_Nome_Sel, AV103TFEstado_UF, AV104TFEstado_UF_Sel, AV107TFContratante_InicioDoExpediente, AV108TFContratante_InicioDoExpediente_To, AV113TFContratante_FimDoExpediente, AV114TFContratante_FimDoExpediente_To, AV61ManageFiltersExecutionStep, AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace, AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace, AV89ddo_Contratante_CNPJTitleControlIdToReplace, AV93ddo_Contratante_TelefoneTitleControlIdToReplace, AV97ddo_Contratante_RamalTitleControlIdToReplace, AV101ddo_Municipio_NomeTitleControlIdToReplace, AV105ddo_Estado_UFTitleControlIdToReplace, AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace, AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace, AV39AreaTrabalho_Codigo, AV165Pgmname, AV6WWPContext, A29Contratante_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV143WWContratanteDS_1_Areatrabalho_codigo = AV39AreaTrabalho_Codigo;
         AV144WWContratanteDS_2_Tfcontratante_razaosocial = AV79TFContratante_RazaoSocial;
         AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel = AV80TFContratante_RazaoSocial_Sel;
         AV146WWContratanteDS_4_Tfcontratante_nomefantasia = AV83TFContratante_NomeFantasia;
         AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel = AV84TFContratante_NomeFantasia_Sel;
         AV148WWContratanteDS_6_Tfcontratante_cnpj = AV87TFContratante_CNPJ;
         AV149WWContratanteDS_7_Tfcontratante_cnpj_sel = AV88TFContratante_CNPJ_Sel;
         AV150WWContratanteDS_8_Tfcontratante_telefone = AV91TFContratante_Telefone;
         AV151WWContratanteDS_9_Tfcontratante_telefone_sel = AV92TFContratante_Telefone_Sel;
         AV152WWContratanteDS_10_Tfcontratante_ramal = AV95TFContratante_Ramal;
         AV153WWContratanteDS_11_Tfcontratante_ramal_sel = AV96TFContratante_Ramal_Sel;
         AV154WWContratanteDS_12_Tfmunicipio_nome = AV99TFMunicipio_Nome;
         AV155WWContratanteDS_13_Tfmunicipio_nome_sel = AV100TFMunicipio_Nome_Sel;
         AV156WWContratanteDS_14_Tfestado_uf = AV103TFEstado_UF;
         AV157WWContratanteDS_15_Tfestado_uf_sel = AV104TFEstado_UF_Sel;
         AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente = AV107TFContratante_InicioDoExpediente;
         AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to = AV108TFContratante_InicioDoExpediente_To;
         AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente = AV113TFContratante_FimDoExpediente;
         AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to = AV114TFContratante_FimDoExpediente_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV79TFContratante_RazaoSocial, AV80TFContratante_RazaoSocial_Sel, AV83TFContratante_NomeFantasia, AV84TFContratante_NomeFantasia_Sel, AV87TFContratante_CNPJ, AV88TFContratante_CNPJ_Sel, AV91TFContratante_Telefone, AV92TFContratante_Telefone_Sel, AV95TFContratante_Ramal, AV96TFContratante_Ramal_Sel, AV99TFMunicipio_Nome, AV100TFMunicipio_Nome_Sel, AV103TFEstado_UF, AV104TFEstado_UF_Sel, AV107TFContratante_InicioDoExpediente, AV108TFContratante_InicioDoExpediente_To, AV113TFContratante_FimDoExpediente, AV114TFContratante_FimDoExpediente_To, AV61ManageFiltersExecutionStep, AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace, AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace, AV89ddo_Contratante_CNPJTitleControlIdToReplace, AV93ddo_Contratante_TelefoneTitleControlIdToReplace, AV97ddo_Contratante_RamalTitleControlIdToReplace, AV101ddo_Municipio_NomeTitleControlIdToReplace, AV105ddo_Estado_UFTitleControlIdToReplace, AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace, AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace, AV39AreaTrabalho_Codigo, AV165Pgmname, AV6WWPContext, A29Contratante_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV143WWContratanteDS_1_Areatrabalho_codigo = AV39AreaTrabalho_Codigo;
         AV144WWContratanteDS_2_Tfcontratante_razaosocial = AV79TFContratante_RazaoSocial;
         AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel = AV80TFContratante_RazaoSocial_Sel;
         AV146WWContratanteDS_4_Tfcontratante_nomefantasia = AV83TFContratante_NomeFantasia;
         AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel = AV84TFContratante_NomeFantasia_Sel;
         AV148WWContratanteDS_6_Tfcontratante_cnpj = AV87TFContratante_CNPJ;
         AV149WWContratanteDS_7_Tfcontratante_cnpj_sel = AV88TFContratante_CNPJ_Sel;
         AV150WWContratanteDS_8_Tfcontratante_telefone = AV91TFContratante_Telefone;
         AV151WWContratanteDS_9_Tfcontratante_telefone_sel = AV92TFContratante_Telefone_Sel;
         AV152WWContratanteDS_10_Tfcontratante_ramal = AV95TFContratante_Ramal;
         AV153WWContratanteDS_11_Tfcontratante_ramal_sel = AV96TFContratante_Ramal_Sel;
         AV154WWContratanteDS_12_Tfmunicipio_nome = AV99TFMunicipio_Nome;
         AV155WWContratanteDS_13_Tfmunicipio_nome_sel = AV100TFMunicipio_Nome_Sel;
         AV156WWContratanteDS_14_Tfestado_uf = AV103TFEstado_UF;
         AV157WWContratanteDS_15_Tfestado_uf_sel = AV104TFEstado_UF_Sel;
         AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente = AV107TFContratante_InicioDoExpediente;
         AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to = AV108TFContratante_InicioDoExpediente_To;
         AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente = AV113TFContratante_FimDoExpediente;
         AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to = AV114TFContratante_FimDoExpediente_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV79TFContratante_RazaoSocial, AV80TFContratante_RazaoSocial_Sel, AV83TFContratante_NomeFantasia, AV84TFContratante_NomeFantasia_Sel, AV87TFContratante_CNPJ, AV88TFContratante_CNPJ_Sel, AV91TFContratante_Telefone, AV92TFContratante_Telefone_Sel, AV95TFContratante_Ramal, AV96TFContratante_Ramal_Sel, AV99TFMunicipio_Nome, AV100TFMunicipio_Nome_Sel, AV103TFEstado_UF, AV104TFEstado_UF_Sel, AV107TFContratante_InicioDoExpediente, AV108TFContratante_InicioDoExpediente_To, AV113TFContratante_FimDoExpediente, AV114TFContratante_FimDoExpediente_To, AV61ManageFiltersExecutionStep, AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace, AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace, AV89ddo_Contratante_CNPJTitleControlIdToReplace, AV93ddo_Contratante_TelefoneTitleControlIdToReplace, AV97ddo_Contratante_RamalTitleControlIdToReplace, AV101ddo_Municipio_NomeTitleControlIdToReplace, AV105ddo_Estado_UFTitleControlIdToReplace, AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace, AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace, AV39AreaTrabalho_Codigo, AV165Pgmname, AV6WWPContext, A29Contratante_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV143WWContratanteDS_1_Areatrabalho_codigo = AV39AreaTrabalho_Codigo;
         AV144WWContratanteDS_2_Tfcontratante_razaosocial = AV79TFContratante_RazaoSocial;
         AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel = AV80TFContratante_RazaoSocial_Sel;
         AV146WWContratanteDS_4_Tfcontratante_nomefantasia = AV83TFContratante_NomeFantasia;
         AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel = AV84TFContratante_NomeFantasia_Sel;
         AV148WWContratanteDS_6_Tfcontratante_cnpj = AV87TFContratante_CNPJ;
         AV149WWContratanteDS_7_Tfcontratante_cnpj_sel = AV88TFContratante_CNPJ_Sel;
         AV150WWContratanteDS_8_Tfcontratante_telefone = AV91TFContratante_Telefone;
         AV151WWContratanteDS_9_Tfcontratante_telefone_sel = AV92TFContratante_Telefone_Sel;
         AV152WWContratanteDS_10_Tfcontratante_ramal = AV95TFContratante_Ramal;
         AV153WWContratanteDS_11_Tfcontratante_ramal_sel = AV96TFContratante_Ramal_Sel;
         AV154WWContratanteDS_12_Tfmunicipio_nome = AV99TFMunicipio_Nome;
         AV155WWContratanteDS_13_Tfmunicipio_nome_sel = AV100TFMunicipio_Nome_Sel;
         AV156WWContratanteDS_14_Tfestado_uf = AV103TFEstado_UF;
         AV157WWContratanteDS_15_Tfestado_uf_sel = AV104TFEstado_UF_Sel;
         AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente = AV107TFContratante_InicioDoExpediente;
         AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to = AV108TFContratante_InicioDoExpediente_To;
         AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente = AV113TFContratante_FimDoExpediente;
         AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to = AV114TFContratante_FimDoExpediente_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV79TFContratante_RazaoSocial, AV80TFContratante_RazaoSocial_Sel, AV83TFContratante_NomeFantasia, AV84TFContratante_NomeFantasia_Sel, AV87TFContratante_CNPJ, AV88TFContratante_CNPJ_Sel, AV91TFContratante_Telefone, AV92TFContratante_Telefone_Sel, AV95TFContratante_Ramal, AV96TFContratante_Ramal_Sel, AV99TFMunicipio_Nome, AV100TFMunicipio_Nome_Sel, AV103TFEstado_UF, AV104TFEstado_UF_Sel, AV107TFContratante_InicioDoExpediente, AV108TFContratante_InicioDoExpediente_To, AV113TFContratante_FimDoExpediente, AV114TFContratante_FimDoExpediente_To, AV61ManageFiltersExecutionStep, AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace, AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace, AV89ddo_Contratante_CNPJTitleControlIdToReplace, AV93ddo_Contratante_TelefoneTitleControlIdToReplace, AV97ddo_Contratante_RamalTitleControlIdToReplace, AV101ddo_Municipio_NomeTitleControlIdToReplace, AV105ddo_Estado_UFTitleControlIdToReplace, AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace, AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace, AV39AreaTrabalho_Codigo, AV165Pgmname, AV6WWPContext, A29Contratante_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP0Y0( )
      {
         /* Before Start, stand alone formulas. */
         AV165Pgmname = "WWContratante";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E220Y2 */
         E220Y2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV65ManageFiltersData);
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV118DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATANTE_RAZAOSOCIALTITLEFILTERDATA"), AV78Contratante_RazaoSocialTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATANTE_NOMEFANTASIATITLEFILTERDATA"), AV82Contratante_NomeFantasiaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATANTE_CNPJTITLEFILTERDATA"), AV86Contratante_CNPJTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATANTE_TELEFONETITLEFILTERDATA"), AV90Contratante_TelefoneTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATANTE_RAMALTITLEFILTERDATA"), AV94Contratante_RamalTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vMUNICIPIO_NOMETITLEFILTERDATA"), AV98Municipio_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vESTADO_UFTITLEFILTERDATA"), AV102Estado_UFTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATANTE_INICIODOEXPEDIENTETITLEFILTERDATA"), AV106Contratante_InicioDoExpedienteTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATANTE_FIMDOEXPEDIENTETITLEFILTERDATA"), AV112Contratante_FimDoExpedienteTitleFilterData);
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vAREATRABALHO_CODIGO");
               GX_FocusControl = edtavAreatrabalho_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39AreaTrabalho_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39AreaTrabalho_Codigo), 6, 0)));
            }
            else
            {
               AV39AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39AreaTrabalho_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV61ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV61ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV61ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV61ManageFiltersExecutionStep), 1, 0));
            }
            AV79TFContratante_RazaoSocial = StringUtil.Upper( cgiGet( edtavTfcontratante_razaosocial_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFContratante_RazaoSocial", AV79TFContratante_RazaoSocial);
            AV80TFContratante_RazaoSocial_Sel = StringUtil.Upper( cgiGet( edtavTfcontratante_razaosocial_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratante_RazaoSocial_Sel", AV80TFContratante_RazaoSocial_Sel);
            AV83TFContratante_NomeFantasia = StringUtil.Upper( cgiGet( edtavTfcontratante_nomefantasia_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFContratante_NomeFantasia", AV83TFContratante_NomeFantasia);
            AV84TFContratante_NomeFantasia_Sel = StringUtil.Upper( cgiGet( edtavTfcontratante_nomefantasia_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContratante_NomeFantasia_Sel", AV84TFContratante_NomeFantasia_Sel);
            AV87TFContratante_CNPJ = cgiGet( edtavTfcontratante_cnpj_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFContratante_CNPJ", AV87TFContratante_CNPJ);
            AV88TFContratante_CNPJ_Sel = cgiGet( edtavTfcontratante_cnpj_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFContratante_CNPJ_Sel", AV88TFContratante_CNPJ_Sel);
            AV91TFContratante_Telefone = cgiGet( edtavTfcontratante_telefone_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFContratante_Telefone", AV91TFContratante_Telefone);
            AV92TFContratante_Telefone_Sel = cgiGet( edtavTfcontratante_telefone_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFContratante_Telefone_Sel", AV92TFContratante_Telefone_Sel);
            AV95TFContratante_Ramal = cgiGet( edtavTfcontratante_ramal_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95TFContratante_Ramal", AV95TFContratante_Ramal);
            AV96TFContratante_Ramal_Sel = cgiGet( edtavTfcontratante_ramal_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96TFContratante_Ramal_Sel", AV96TFContratante_Ramal_Sel);
            AV99TFMunicipio_Nome = StringUtil.Upper( cgiGet( edtavTfmunicipio_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99TFMunicipio_Nome", AV99TFMunicipio_Nome);
            AV100TFMunicipio_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfmunicipio_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFMunicipio_Nome_Sel", AV100TFMunicipio_Nome_Sel);
            AV103TFEstado_UF = StringUtil.Upper( cgiGet( edtavTfestado_uf_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103TFEstado_UF", AV103TFEstado_UF);
            AV104TFEstado_UF_Sel = StringUtil.Upper( cgiGet( edtavTfestado_uf_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFEstado_UF_Sel", AV104TFEstado_UF_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratante_iniciodoexpediente_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badtime", new   object[]  {"TFContratante_Inicio Do Expediente"}), 1, "vTFCONTRATANTE_INICIODOEXPEDIENTE");
               GX_FocusControl = edtavTfcontratante_iniciodoexpediente_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV107TFContratante_InicioDoExpediente = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TFContratante_InicioDoExpediente", context.localUtil.TToC( AV107TFContratante_InicioDoExpediente, 0, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV107TFContratante_InicioDoExpediente = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( edtavTfcontratante_iniciodoexpediente_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TFContratante_InicioDoExpediente", context.localUtil.TToC( AV107TFContratante_InicioDoExpediente, 0, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratante_iniciodoexpediente_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badtime", new   object[]  {"TFContratante_Inicio Do Expediente_To"}), 1, "vTFCONTRATANTE_INICIODOEXPEDIENTE_TO");
               GX_FocusControl = edtavTfcontratante_iniciodoexpediente_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV108TFContratante_InicioDoExpediente_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContratante_InicioDoExpediente_To", context.localUtil.TToC( AV108TFContratante_InicioDoExpediente_To, 0, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV108TFContratante_InicioDoExpediente_To = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( edtavTfcontratante_iniciodoexpediente_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContratante_InicioDoExpediente_To", context.localUtil.TToC( AV108TFContratante_InicioDoExpediente_To, 0, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratante_iniciodoexpedienteauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contratante_Inicio Do Expediente Aux Date"}), 1, "vDDO_CONTRATANTE_INICIODOEXPEDIENTEAUXDATE");
               GX_FocusControl = edtavDdo_contratante_iniciodoexpedienteauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV109DDO_Contratante_InicioDoExpedienteAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109DDO_Contratante_InicioDoExpedienteAuxDate", context.localUtil.Format(AV109DDO_Contratante_InicioDoExpedienteAuxDate, "99/99/99"));
            }
            else
            {
               AV109DDO_Contratante_InicioDoExpedienteAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratante_iniciodoexpedienteauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109DDO_Contratante_InicioDoExpedienteAuxDate", context.localUtil.Format(AV109DDO_Contratante_InicioDoExpedienteAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratante_iniciodoexpedienteauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contratante_Inicio Do Expediente Aux Date To"}), 1, "vDDO_CONTRATANTE_INICIODOEXPEDIENTEAUXDATETO");
               GX_FocusControl = edtavDdo_contratante_iniciodoexpedienteauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV110DDO_Contratante_InicioDoExpedienteAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110DDO_Contratante_InicioDoExpedienteAuxDateTo", context.localUtil.Format(AV110DDO_Contratante_InicioDoExpedienteAuxDateTo, "99/99/99"));
            }
            else
            {
               AV110DDO_Contratante_InicioDoExpedienteAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratante_iniciodoexpedienteauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110DDO_Contratante_InicioDoExpedienteAuxDateTo", context.localUtil.Format(AV110DDO_Contratante_InicioDoExpedienteAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratante_fimdoexpediente_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badtime", new   object[]  {"TFContratante_Fim Do Expediente"}), 1, "vTFCONTRATANTE_FIMDOEXPEDIENTE");
               GX_FocusControl = edtavTfcontratante_fimdoexpediente_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV113TFContratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113TFContratante_FimDoExpediente", context.localUtil.TToC( AV113TFContratante_FimDoExpediente, 0, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV113TFContratante_FimDoExpediente = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( edtavTfcontratante_fimdoexpediente_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113TFContratante_FimDoExpediente", context.localUtil.TToC( AV113TFContratante_FimDoExpediente, 0, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratante_fimdoexpediente_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badtime", new   object[]  {"TFContratante_Fim Do Expediente_To"}), 1, "vTFCONTRATANTE_FIMDOEXPEDIENTE_TO");
               GX_FocusControl = edtavTfcontratante_fimdoexpediente_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV114TFContratante_FimDoExpediente_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114TFContratante_FimDoExpediente_To", context.localUtil.TToC( AV114TFContratante_FimDoExpediente_To, 0, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV114TFContratante_FimDoExpediente_To = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( edtavTfcontratante_fimdoexpediente_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114TFContratante_FimDoExpediente_To", context.localUtil.TToC( AV114TFContratante_FimDoExpediente_To, 0, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratante_fimdoexpedienteauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contratante_Fim Do Expediente Aux Date"}), 1, "vDDO_CONTRATANTE_FIMDOEXPEDIENTEAUXDATE");
               GX_FocusControl = edtavDdo_contratante_fimdoexpedienteauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV115DDO_Contratante_FimDoExpedienteAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115DDO_Contratante_FimDoExpedienteAuxDate", context.localUtil.Format(AV115DDO_Contratante_FimDoExpedienteAuxDate, "99/99/99"));
            }
            else
            {
               AV115DDO_Contratante_FimDoExpedienteAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratante_fimdoexpedienteauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115DDO_Contratante_FimDoExpedienteAuxDate", context.localUtil.Format(AV115DDO_Contratante_FimDoExpedienteAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratante_fimdoexpedienteauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contratante_Fim Do Expediente Aux Date To"}), 1, "vDDO_CONTRATANTE_FIMDOEXPEDIENTEAUXDATETO");
               GX_FocusControl = edtavDdo_contratante_fimdoexpedienteauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV116DDO_Contratante_FimDoExpedienteAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116DDO_Contratante_FimDoExpedienteAuxDateTo", context.localUtil.Format(AV116DDO_Contratante_FimDoExpedienteAuxDateTo, "99/99/99"));
            }
            else
            {
               AV116DDO_Contratante_FimDoExpedienteAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratante_fimdoexpedienteauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116DDO_Contratante_FimDoExpedienteAuxDateTo", context.localUtil.Format(AV116DDO_Contratante_FimDoExpedienteAuxDateTo, "99/99/99"));
            }
            AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace = cgiGet( edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace", AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace);
            AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace = cgiGet( edtavDdo_contratante_nomefantasiatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace", AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace);
            AV89ddo_Contratante_CNPJTitleControlIdToReplace = cgiGet( edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89ddo_Contratante_CNPJTitleControlIdToReplace", AV89ddo_Contratante_CNPJTitleControlIdToReplace);
            AV93ddo_Contratante_TelefoneTitleControlIdToReplace = cgiGet( edtavDdo_contratante_telefonetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93ddo_Contratante_TelefoneTitleControlIdToReplace", AV93ddo_Contratante_TelefoneTitleControlIdToReplace);
            AV97ddo_Contratante_RamalTitleControlIdToReplace = cgiGet( edtavDdo_contratante_ramaltitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97ddo_Contratante_RamalTitleControlIdToReplace", AV97ddo_Contratante_RamalTitleControlIdToReplace);
            AV101ddo_Municipio_NomeTitleControlIdToReplace = cgiGet( edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101ddo_Municipio_NomeTitleControlIdToReplace", AV101ddo_Municipio_NomeTitleControlIdToReplace);
            AV105ddo_Estado_UFTitleControlIdToReplace = cgiGet( edtavDdo_estado_uftitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ddo_Estado_UFTitleControlIdToReplace", AV105ddo_Estado_UFTitleControlIdToReplace);
            AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace = cgiGet( edtavDdo_contratante_iniciodoexpedientetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace", AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace);
            AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace = cgiGet( edtavDdo_contratante_fimdoexpedientetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace", AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_32 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_32"), ",", "."));
            AV120GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV121GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratante_razaosocial_Caption = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Caption");
            Ddo_contratante_razaosocial_Tooltip = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Tooltip");
            Ddo_contratante_razaosocial_Cls = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Cls");
            Ddo_contratante_razaosocial_Filteredtext_set = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Filteredtext_set");
            Ddo_contratante_razaosocial_Selectedvalue_set = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Selectedvalue_set");
            Ddo_contratante_razaosocial_Dropdownoptionstype = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Dropdownoptionstype");
            Ddo_contratante_razaosocial_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Titlecontrolidtoreplace");
            Ddo_contratante_razaosocial_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Includesortasc"));
            Ddo_contratante_razaosocial_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Includesortdsc"));
            Ddo_contratante_razaosocial_Sortedstatus = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Sortedstatus");
            Ddo_contratante_razaosocial_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Includefilter"));
            Ddo_contratante_razaosocial_Filtertype = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Filtertype");
            Ddo_contratante_razaosocial_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Filterisrange"));
            Ddo_contratante_razaosocial_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Includedatalist"));
            Ddo_contratante_razaosocial_Datalisttype = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Datalisttype");
            Ddo_contratante_razaosocial_Datalistproc = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Datalistproc");
            Ddo_contratante_razaosocial_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratante_razaosocial_Sortasc = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Sortasc");
            Ddo_contratante_razaosocial_Sortdsc = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Sortdsc");
            Ddo_contratante_razaosocial_Loadingdata = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Loadingdata");
            Ddo_contratante_razaosocial_Cleanfilter = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Cleanfilter");
            Ddo_contratante_razaosocial_Noresultsfound = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Noresultsfound");
            Ddo_contratante_razaosocial_Searchbuttontext = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Searchbuttontext");
            Ddo_contratante_nomefantasia_Caption = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Caption");
            Ddo_contratante_nomefantasia_Tooltip = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Tooltip");
            Ddo_contratante_nomefantasia_Cls = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Cls");
            Ddo_contratante_nomefantasia_Filteredtext_set = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Filteredtext_set");
            Ddo_contratante_nomefantasia_Selectedvalue_set = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Selectedvalue_set");
            Ddo_contratante_nomefantasia_Dropdownoptionstype = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Dropdownoptionstype");
            Ddo_contratante_nomefantasia_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Titlecontrolidtoreplace");
            Ddo_contratante_nomefantasia_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Includesortasc"));
            Ddo_contratante_nomefantasia_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Includesortdsc"));
            Ddo_contratante_nomefantasia_Sortedstatus = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Sortedstatus");
            Ddo_contratante_nomefantasia_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Includefilter"));
            Ddo_contratante_nomefantasia_Filtertype = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Filtertype");
            Ddo_contratante_nomefantasia_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Filterisrange"));
            Ddo_contratante_nomefantasia_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Includedatalist"));
            Ddo_contratante_nomefantasia_Datalisttype = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Datalisttype");
            Ddo_contratante_nomefantasia_Datalistproc = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Datalistproc");
            Ddo_contratante_nomefantasia_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratante_nomefantasia_Sortasc = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Sortasc");
            Ddo_contratante_nomefantasia_Sortdsc = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Sortdsc");
            Ddo_contratante_nomefantasia_Loadingdata = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Loadingdata");
            Ddo_contratante_nomefantasia_Cleanfilter = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Cleanfilter");
            Ddo_contratante_nomefantasia_Noresultsfound = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Noresultsfound");
            Ddo_contratante_nomefantasia_Searchbuttontext = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Searchbuttontext");
            Ddo_contratante_cnpj_Caption = cgiGet( "DDO_CONTRATANTE_CNPJ_Caption");
            Ddo_contratante_cnpj_Tooltip = cgiGet( "DDO_CONTRATANTE_CNPJ_Tooltip");
            Ddo_contratante_cnpj_Cls = cgiGet( "DDO_CONTRATANTE_CNPJ_Cls");
            Ddo_contratante_cnpj_Filteredtext_set = cgiGet( "DDO_CONTRATANTE_CNPJ_Filteredtext_set");
            Ddo_contratante_cnpj_Selectedvalue_set = cgiGet( "DDO_CONTRATANTE_CNPJ_Selectedvalue_set");
            Ddo_contratante_cnpj_Dropdownoptionstype = cgiGet( "DDO_CONTRATANTE_CNPJ_Dropdownoptionstype");
            Ddo_contratante_cnpj_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATANTE_CNPJ_Titlecontrolidtoreplace");
            Ddo_contratante_cnpj_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_CNPJ_Includesortasc"));
            Ddo_contratante_cnpj_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_CNPJ_Includesortdsc"));
            Ddo_contratante_cnpj_Sortedstatus = cgiGet( "DDO_CONTRATANTE_CNPJ_Sortedstatus");
            Ddo_contratante_cnpj_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_CNPJ_Includefilter"));
            Ddo_contratante_cnpj_Filtertype = cgiGet( "DDO_CONTRATANTE_CNPJ_Filtertype");
            Ddo_contratante_cnpj_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_CNPJ_Filterisrange"));
            Ddo_contratante_cnpj_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_CNPJ_Includedatalist"));
            Ddo_contratante_cnpj_Datalisttype = cgiGet( "DDO_CONTRATANTE_CNPJ_Datalisttype");
            Ddo_contratante_cnpj_Datalistproc = cgiGet( "DDO_CONTRATANTE_CNPJ_Datalistproc");
            Ddo_contratante_cnpj_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATANTE_CNPJ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratante_cnpj_Sortasc = cgiGet( "DDO_CONTRATANTE_CNPJ_Sortasc");
            Ddo_contratante_cnpj_Sortdsc = cgiGet( "DDO_CONTRATANTE_CNPJ_Sortdsc");
            Ddo_contratante_cnpj_Loadingdata = cgiGet( "DDO_CONTRATANTE_CNPJ_Loadingdata");
            Ddo_contratante_cnpj_Cleanfilter = cgiGet( "DDO_CONTRATANTE_CNPJ_Cleanfilter");
            Ddo_contratante_cnpj_Noresultsfound = cgiGet( "DDO_CONTRATANTE_CNPJ_Noresultsfound");
            Ddo_contratante_cnpj_Searchbuttontext = cgiGet( "DDO_CONTRATANTE_CNPJ_Searchbuttontext");
            Ddo_contratante_telefone_Caption = cgiGet( "DDO_CONTRATANTE_TELEFONE_Caption");
            Ddo_contratante_telefone_Tooltip = cgiGet( "DDO_CONTRATANTE_TELEFONE_Tooltip");
            Ddo_contratante_telefone_Cls = cgiGet( "DDO_CONTRATANTE_TELEFONE_Cls");
            Ddo_contratante_telefone_Filteredtext_set = cgiGet( "DDO_CONTRATANTE_TELEFONE_Filteredtext_set");
            Ddo_contratante_telefone_Selectedvalue_set = cgiGet( "DDO_CONTRATANTE_TELEFONE_Selectedvalue_set");
            Ddo_contratante_telefone_Dropdownoptionstype = cgiGet( "DDO_CONTRATANTE_TELEFONE_Dropdownoptionstype");
            Ddo_contratante_telefone_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATANTE_TELEFONE_Titlecontrolidtoreplace");
            Ddo_contratante_telefone_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_TELEFONE_Includesortasc"));
            Ddo_contratante_telefone_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_TELEFONE_Includesortdsc"));
            Ddo_contratante_telefone_Sortedstatus = cgiGet( "DDO_CONTRATANTE_TELEFONE_Sortedstatus");
            Ddo_contratante_telefone_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_TELEFONE_Includefilter"));
            Ddo_contratante_telefone_Filtertype = cgiGet( "DDO_CONTRATANTE_TELEFONE_Filtertype");
            Ddo_contratante_telefone_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_TELEFONE_Filterisrange"));
            Ddo_contratante_telefone_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_TELEFONE_Includedatalist"));
            Ddo_contratante_telefone_Datalisttype = cgiGet( "DDO_CONTRATANTE_TELEFONE_Datalisttype");
            Ddo_contratante_telefone_Datalistproc = cgiGet( "DDO_CONTRATANTE_TELEFONE_Datalistproc");
            Ddo_contratante_telefone_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATANTE_TELEFONE_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratante_telefone_Sortasc = cgiGet( "DDO_CONTRATANTE_TELEFONE_Sortasc");
            Ddo_contratante_telefone_Sortdsc = cgiGet( "DDO_CONTRATANTE_TELEFONE_Sortdsc");
            Ddo_contratante_telefone_Loadingdata = cgiGet( "DDO_CONTRATANTE_TELEFONE_Loadingdata");
            Ddo_contratante_telefone_Cleanfilter = cgiGet( "DDO_CONTRATANTE_TELEFONE_Cleanfilter");
            Ddo_contratante_telefone_Noresultsfound = cgiGet( "DDO_CONTRATANTE_TELEFONE_Noresultsfound");
            Ddo_contratante_telefone_Searchbuttontext = cgiGet( "DDO_CONTRATANTE_TELEFONE_Searchbuttontext");
            Ddo_contratante_ramal_Caption = cgiGet( "DDO_CONTRATANTE_RAMAL_Caption");
            Ddo_contratante_ramal_Tooltip = cgiGet( "DDO_CONTRATANTE_RAMAL_Tooltip");
            Ddo_contratante_ramal_Cls = cgiGet( "DDO_CONTRATANTE_RAMAL_Cls");
            Ddo_contratante_ramal_Filteredtext_set = cgiGet( "DDO_CONTRATANTE_RAMAL_Filteredtext_set");
            Ddo_contratante_ramal_Selectedvalue_set = cgiGet( "DDO_CONTRATANTE_RAMAL_Selectedvalue_set");
            Ddo_contratante_ramal_Dropdownoptionstype = cgiGet( "DDO_CONTRATANTE_RAMAL_Dropdownoptionstype");
            Ddo_contratante_ramal_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATANTE_RAMAL_Titlecontrolidtoreplace");
            Ddo_contratante_ramal_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_RAMAL_Includesortasc"));
            Ddo_contratante_ramal_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_RAMAL_Includesortdsc"));
            Ddo_contratante_ramal_Sortedstatus = cgiGet( "DDO_CONTRATANTE_RAMAL_Sortedstatus");
            Ddo_contratante_ramal_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_RAMAL_Includefilter"));
            Ddo_contratante_ramal_Filtertype = cgiGet( "DDO_CONTRATANTE_RAMAL_Filtertype");
            Ddo_contratante_ramal_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_RAMAL_Filterisrange"));
            Ddo_contratante_ramal_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_RAMAL_Includedatalist"));
            Ddo_contratante_ramal_Datalisttype = cgiGet( "DDO_CONTRATANTE_RAMAL_Datalisttype");
            Ddo_contratante_ramal_Datalistproc = cgiGet( "DDO_CONTRATANTE_RAMAL_Datalistproc");
            Ddo_contratante_ramal_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATANTE_RAMAL_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratante_ramal_Sortasc = cgiGet( "DDO_CONTRATANTE_RAMAL_Sortasc");
            Ddo_contratante_ramal_Sortdsc = cgiGet( "DDO_CONTRATANTE_RAMAL_Sortdsc");
            Ddo_contratante_ramal_Loadingdata = cgiGet( "DDO_CONTRATANTE_RAMAL_Loadingdata");
            Ddo_contratante_ramal_Cleanfilter = cgiGet( "DDO_CONTRATANTE_RAMAL_Cleanfilter");
            Ddo_contratante_ramal_Noresultsfound = cgiGet( "DDO_CONTRATANTE_RAMAL_Noresultsfound");
            Ddo_contratante_ramal_Searchbuttontext = cgiGet( "DDO_CONTRATANTE_RAMAL_Searchbuttontext");
            Ddo_municipio_nome_Caption = cgiGet( "DDO_MUNICIPIO_NOME_Caption");
            Ddo_municipio_nome_Tooltip = cgiGet( "DDO_MUNICIPIO_NOME_Tooltip");
            Ddo_municipio_nome_Cls = cgiGet( "DDO_MUNICIPIO_NOME_Cls");
            Ddo_municipio_nome_Filteredtext_set = cgiGet( "DDO_MUNICIPIO_NOME_Filteredtext_set");
            Ddo_municipio_nome_Selectedvalue_set = cgiGet( "DDO_MUNICIPIO_NOME_Selectedvalue_set");
            Ddo_municipio_nome_Dropdownoptionstype = cgiGet( "DDO_MUNICIPIO_NOME_Dropdownoptionstype");
            Ddo_municipio_nome_Titlecontrolidtoreplace = cgiGet( "DDO_MUNICIPIO_NOME_Titlecontrolidtoreplace");
            Ddo_municipio_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Includesortasc"));
            Ddo_municipio_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Includesortdsc"));
            Ddo_municipio_nome_Sortedstatus = cgiGet( "DDO_MUNICIPIO_NOME_Sortedstatus");
            Ddo_municipio_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Includefilter"));
            Ddo_municipio_nome_Filtertype = cgiGet( "DDO_MUNICIPIO_NOME_Filtertype");
            Ddo_municipio_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Filterisrange"));
            Ddo_municipio_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Includedatalist"));
            Ddo_municipio_nome_Datalisttype = cgiGet( "DDO_MUNICIPIO_NOME_Datalisttype");
            Ddo_municipio_nome_Datalistproc = cgiGet( "DDO_MUNICIPIO_NOME_Datalistproc");
            Ddo_municipio_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_MUNICIPIO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_municipio_nome_Sortasc = cgiGet( "DDO_MUNICIPIO_NOME_Sortasc");
            Ddo_municipio_nome_Sortdsc = cgiGet( "DDO_MUNICIPIO_NOME_Sortdsc");
            Ddo_municipio_nome_Loadingdata = cgiGet( "DDO_MUNICIPIO_NOME_Loadingdata");
            Ddo_municipio_nome_Cleanfilter = cgiGet( "DDO_MUNICIPIO_NOME_Cleanfilter");
            Ddo_municipio_nome_Noresultsfound = cgiGet( "DDO_MUNICIPIO_NOME_Noresultsfound");
            Ddo_municipio_nome_Searchbuttontext = cgiGet( "DDO_MUNICIPIO_NOME_Searchbuttontext");
            Ddo_estado_uf_Caption = cgiGet( "DDO_ESTADO_UF_Caption");
            Ddo_estado_uf_Tooltip = cgiGet( "DDO_ESTADO_UF_Tooltip");
            Ddo_estado_uf_Cls = cgiGet( "DDO_ESTADO_UF_Cls");
            Ddo_estado_uf_Filteredtext_set = cgiGet( "DDO_ESTADO_UF_Filteredtext_set");
            Ddo_estado_uf_Selectedvalue_set = cgiGet( "DDO_ESTADO_UF_Selectedvalue_set");
            Ddo_estado_uf_Dropdownoptionstype = cgiGet( "DDO_ESTADO_UF_Dropdownoptionstype");
            Ddo_estado_uf_Titlecontrolidtoreplace = cgiGet( "DDO_ESTADO_UF_Titlecontrolidtoreplace");
            Ddo_estado_uf_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_UF_Includesortasc"));
            Ddo_estado_uf_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_UF_Includesortdsc"));
            Ddo_estado_uf_Sortedstatus = cgiGet( "DDO_ESTADO_UF_Sortedstatus");
            Ddo_estado_uf_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_UF_Includefilter"));
            Ddo_estado_uf_Filtertype = cgiGet( "DDO_ESTADO_UF_Filtertype");
            Ddo_estado_uf_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_UF_Filterisrange"));
            Ddo_estado_uf_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_UF_Includedatalist"));
            Ddo_estado_uf_Datalisttype = cgiGet( "DDO_ESTADO_UF_Datalisttype");
            Ddo_estado_uf_Datalistproc = cgiGet( "DDO_ESTADO_UF_Datalistproc");
            Ddo_estado_uf_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_ESTADO_UF_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_estado_uf_Sortasc = cgiGet( "DDO_ESTADO_UF_Sortasc");
            Ddo_estado_uf_Sortdsc = cgiGet( "DDO_ESTADO_UF_Sortdsc");
            Ddo_estado_uf_Loadingdata = cgiGet( "DDO_ESTADO_UF_Loadingdata");
            Ddo_estado_uf_Cleanfilter = cgiGet( "DDO_ESTADO_UF_Cleanfilter");
            Ddo_estado_uf_Noresultsfound = cgiGet( "DDO_ESTADO_UF_Noresultsfound");
            Ddo_estado_uf_Searchbuttontext = cgiGet( "DDO_ESTADO_UF_Searchbuttontext");
            Ddo_contratante_iniciodoexpediente_Caption = cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Caption");
            Ddo_contratante_iniciodoexpediente_Tooltip = cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Tooltip");
            Ddo_contratante_iniciodoexpediente_Cls = cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Cls");
            Ddo_contratante_iniciodoexpediente_Filteredtext_set = cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Filteredtext_set");
            Ddo_contratante_iniciodoexpediente_Filteredtextto_set = cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Filteredtextto_set");
            Ddo_contratante_iniciodoexpediente_Dropdownoptionstype = cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Dropdownoptionstype");
            Ddo_contratante_iniciodoexpediente_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Titlecontrolidtoreplace");
            Ddo_contratante_iniciodoexpediente_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Includesortasc"));
            Ddo_contratante_iniciodoexpediente_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Includesortdsc"));
            Ddo_contratante_iniciodoexpediente_Sortedstatus = cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Sortedstatus");
            Ddo_contratante_iniciodoexpediente_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Includefilter"));
            Ddo_contratante_iniciodoexpediente_Filtertype = cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Filtertype");
            Ddo_contratante_iniciodoexpediente_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Filterisrange"));
            Ddo_contratante_iniciodoexpediente_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Includedatalist"));
            Ddo_contratante_iniciodoexpediente_Sortasc = cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Sortasc");
            Ddo_contratante_iniciodoexpediente_Sortdsc = cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Sortdsc");
            Ddo_contratante_iniciodoexpediente_Cleanfilter = cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Cleanfilter");
            Ddo_contratante_iniciodoexpediente_Rangefilterfrom = cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Rangefilterfrom");
            Ddo_contratante_iniciodoexpediente_Rangefilterto = cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Rangefilterto");
            Ddo_contratante_iniciodoexpediente_Searchbuttontext = cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Searchbuttontext");
            Ddo_contratante_fimdoexpediente_Caption = cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Caption");
            Ddo_contratante_fimdoexpediente_Tooltip = cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Tooltip");
            Ddo_contratante_fimdoexpediente_Cls = cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Cls");
            Ddo_contratante_fimdoexpediente_Filteredtext_set = cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Filteredtext_set");
            Ddo_contratante_fimdoexpediente_Filteredtextto_set = cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Filteredtextto_set");
            Ddo_contratante_fimdoexpediente_Dropdownoptionstype = cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Dropdownoptionstype");
            Ddo_contratante_fimdoexpediente_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Titlecontrolidtoreplace");
            Ddo_contratante_fimdoexpediente_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Includesortasc"));
            Ddo_contratante_fimdoexpediente_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Includesortdsc"));
            Ddo_contratante_fimdoexpediente_Sortedstatus = cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Sortedstatus");
            Ddo_contratante_fimdoexpediente_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Includefilter"));
            Ddo_contratante_fimdoexpediente_Filtertype = cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Filtertype");
            Ddo_contratante_fimdoexpediente_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Filterisrange"));
            Ddo_contratante_fimdoexpediente_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Includedatalist"));
            Ddo_contratante_fimdoexpediente_Sortasc = cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Sortasc");
            Ddo_contratante_fimdoexpediente_Sortdsc = cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Sortdsc");
            Ddo_contratante_fimdoexpediente_Cleanfilter = cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Cleanfilter");
            Ddo_contratante_fimdoexpediente_Rangefilterfrom = cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Rangefilterfrom");
            Ddo_contratante_fimdoexpediente_Rangefilterto = cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Rangefilterto");
            Ddo_contratante_fimdoexpediente_Searchbuttontext = cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratante_razaosocial_Activeeventkey = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Activeeventkey");
            Ddo_contratante_razaosocial_Filteredtext_get = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Filteredtext_get");
            Ddo_contratante_razaosocial_Selectedvalue_get = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Selectedvalue_get");
            Ddo_contratante_nomefantasia_Activeeventkey = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Activeeventkey");
            Ddo_contratante_nomefantasia_Filteredtext_get = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Filteredtext_get");
            Ddo_contratante_nomefantasia_Selectedvalue_get = cgiGet( "DDO_CONTRATANTE_NOMEFANTASIA_Selectedvalue_get");
            Ddo_contratante_cnpj_Activeeventkey = cgiGet( "DDO_CONTRATANTE_CNPJ_Activeeventkey");
            Ddo_contratante_cnpj_Filteredtext_get = cgiGet( "DDO_CONTRATANTE_CNPJ_Filteredtext_get");
            Ddo_contratante_cnpj_Selectedvalue_get = cgiGet( "DDO_CONTRATANTE_CNPJ_Selectedvalue_get");
            Ddo_contratante_telefone_Activeeventkey = cgiGet( "DDO_CONTRATANTE_TELEFONE_Activeeventkey");
            Ddo_contratante_telefone_Filteredtext_get = cgiGet( "DDO_CONTRATANTE_TELEFONE_Filteredtext_get");
            Ddo_contratante_telefone_Selectedvalue_get = cgiGet( "DDO_CONTRATANTE_TELEFONE_Selectedvalue_get");
            Ddo_contratante_ramal_Activeeventkey = cgiGet( "DDO_CONTRATANTE_RAMAL_Activeeventkey");
            Ddo_contratante_ramal_Filteredtext_get = cgiGet( "DDO_CONTRATANTE_RAMAL_Filteredtext_get");
            Ddo_contratante_ramal_Selectedvalue_get = cgiGet( "DDO_CONTRATANTE_RAMAL_Selectedvalue_get");
            Ddo_municipio_nome_Activeeventkey = cgiGet( "DDO_MUNICIPIO_NOME_Activeeventkey");
            Ddo_municipio_nome_Filteredtext_get = cgiGet( "DDO_MUNICIPIO_NOME_Filteredtext_get");
            Ddo_municipio_nome_Selectedvalue_get = cgiGet( "DDO_MUNICIPIO_NOME_Selectedvalue_get");
            Ddo_estado_uf_Activeeventkey = cgiGet( "DDO_ESTADO_UF_Activeeventkey");
            Ddo_estado_uf_Filteredtext_get = cgiGet( "DDO_ESTADO_UF_Filteredtext_get");
            Ddo_estado_uf_Selectedvalue_get = cgiGet( "DDO_ESTADO_UF_Selectedvalue_get");
            Ddo_contratante_iniciodoexpediente_Activeeventkey = cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Activeeventkey");
            Ddo_contratante_iniciodoexpediente_Filteredtext_get = cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Filteredtext_get");
            Ddo_contratante_iniciodoexpediente_Filteredtextto_get = cgiGet( "DDO_CONTRATANTE_INICIODOEXPEDIENTE_Filteredtextto_get");
            Ddo_contratante_fimdoexpediente_Activeeventkey = cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Activeeventkey");
            Ddo_contratante_fimdoexpediente_Filteredtext_get = cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Filteredtext_get");
            Ddo_contratante_fimdoexpediente_Filteredtextto_get = cgiGet( "DDO_CONTRATANTE_FIMDOEXPEDIENTE_Filteredtextto_get");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_RAZAOSOCIAL"), AV79TFContratante_RazaoSocial) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_RAZAOSOCIAL_SEL"), AV80TFContratante_RazaoSocial_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_NOMEFANTASIA"), AV83TFContratante_NomeFantasia) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_NOMEFANTASIA_SEL"), AV84TFContratante_NomeFantasia_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_CNPJ"), AV87TFContratante_CNPJ) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_CNPJ_SEL"), AV88TFContratante_CNPJ_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_TELEFONE"), AV91TFContratante_Telefone) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_TELEFONE_SEL"), AV92TFContratante_Telefone_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_RAMAL"), AV95TFContratante_Ramal) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_RAMAL_SEL"), AV96TFContratante_Ramal_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMUNICIPIO_NOME"), AV99TFMunicipio_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMUNICIPIO_NOME_SEL"), AV100TFMunicipio_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_UF"), AV103TFEstado_UF) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_UF_SEL"), AV104TFEstado_UF_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATANTE_INICIODOEXPEDIENTE"), 0) != AV107TFContratante_InicioDoExpediente )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATANTE_INICIODOEXPEDIENTE_TO"), 0) != AV108TFContratante_InicioDoExpediente_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATANTE_FIMDOEXPEDIENTE"), 0) != AV113TFContratante_FimDoExpediente )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATANTE_FIMDOEXPEDIENTE_TO"), 0) != AV114TFContratante_FimDoExpediente_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E220Y2 */
         E220Y2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E220Y2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         edtavTfcontratante_razaosocial_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_razaosocial_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_razaosocial_Visible), 5, 0)));
         edtavTfcontratante_razaosocial_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_razaosocial_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_razaosocial_sel_Visible), 5, 0)));
         edtavTfcontratante_nomefantasia_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_nomefantasia_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_nomefantasia_Visible), 5, 0)));
         edtavTfcontratante_nomefantasia_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_nomefantasia_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_nomefantasia_sel_Visible), 5, 0)));
         edtavTfcontratante_cnpj_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_cnpj_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_cnpj_Visible), 5, 0)));
         edtavTfcontratante_cnpj_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_cnpj_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_cnpj_sel_Visible), 5, 0)));
         edtavTfcontratante_telefone_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_telefone_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_telefone_Visible), 5, 0)));
         edtavTfcontratante_telefone_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_telefone_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_telefone_sel_Visible), 5, 0)));
         edtavTfcontratante_ramal_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_ramal_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_ramal_Visible), 5, 0)));
         edtavTfcontratante_ramal_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_ramal_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_ramal_sel_Visible), 5, 0)));
         edtavTfmunicipio_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmunicipio_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmunicipio_nome_Visible), 5, 0)));
         edtavTfmunicipio_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmunicipio_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmunicipio_nome_sel_Visible), 5, 0)));
         edtavTfestado_uf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfestado_uf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfestado_uf_Visible), 5, 0)));
         edtavTfestado_uf_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfestado_uf_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfestado_uf_sel_Visible), 5, 0)));
         edtavTfcontratante_iniciodoexpediente_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_iniciodoexpediente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_iniciodoexpediente_Visible), 5, 0)));
         edtavTfcontratante_iniciodoexpediente_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_iniciodoexpediente_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_iniciodoexpediente_to_Visible), 5, 0)));
         edtavTfcontratante_fimdoexpediente_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_fimdoexpediente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_fimdoexpediente_Visible), 5, 0)));
         edtavTfcontratante_fimdoexpediente_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_fimdoexpediente_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_fimdoexpediente_to_Visible), 5, 0)));
         Ddo_contratante_razaosocial_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratante_RazaoSocial";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_razaosocial_Internalname, "TitleControlIdToReplace", Ddo_contratante_razaosocial_Titlecontrolidtoreplace);
         AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace = Ddo_contratante_razaosocial_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace", AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace);
         edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratante_nomefantasia_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratante_NomeFantasia";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_nomefantasia_Internalname, "TitleControlIdToReplace", Ddo_contratante_nomefantasia_Titlecontrolidtoreplace);
         AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace = Ddo_contratante_nomefantasia_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace", AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace);
         edtavDdo_contratante_nomefantasiatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratante_nomefantasiatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratante_nomefantasiatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratante_cnpj_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratante_CNPJ";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_cnpj_Internalname, "TitleControlIdToReplace", Ddo_contratante_cnpj_Titlecontrolidtoreplace);
         AV89ddo_Contratante_CNPJTitleControlIdToReplace = Ddo_contratante_cnpj_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89ddo_Contratante_CNPJTitleControlIdToReplace", AV89ddo_Contratante_CNPJTitleControlIdToReplace);
         edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratante_telefone_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratante_Telefone";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_telefone_Internalname, "TitleControlIdToReplace", Ddo_contratante_telefone_Titlecontrolidtoreplace);
         AV93ddo_Contratante_TelefoneTitleControlIdToReplace = Ddo_contratante_telefone_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93ddo_Contratante_TelefoneTitleControlIdToReplace", AV93ddo_Contratante_TelefoneTitleControlIdToReplace);
         edtavDdo_contratante_telefonetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratante_telefonetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratante_telefonetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratante_ramal_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratante_Ramal";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_ramal_Internalname, "TitleControlIdToReplace", Ddo_contratante_ramal_Titlecontrolidtoreplace);
         AV97ddo_Contratante_RamalTitleControlIdToReplace = Ddo_contratante_ramal_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97ddo_Contratante_RamalTitleControlIdToReplace", AV97ddo_Contratante_RamalTitleControlIdToReplace);
         edtavDdo_contratante_ramaltitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratante_ramaltitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratante_ramaltitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_municipio_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Municipio_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "TitleControlIdToReplace", Ddo_municipio_nome_Titlecontrolidtoreplace);
         AV101ddo_Municipio_NomeTitleControlIdToReplace = Ddo_municipio_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101ddo_Municipio_NomeTitleControlIdToReplace", AV101ddo_Municipio_NomeTitleControlIdToReplace);
         edtavDdo_municipio_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_municipio_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_estado_uf_Titlecontrolidtoreplace = subGrid_Internalname+"_Estado_UF";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "TitleControlIdToReplace", Ddo_estado_uf_Titlecontrolidtoreplace);
         AV105ddo_Estado_UFTitleControlIdToReplace = Ddo_estado_uf_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ddo_Estado_UFTitleControlIdToReplace", AV105ddo_Estado_UFTitleControlIdToReplace);
         edtavDdo_estado_uftitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_estado_uftitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_estado_uftitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratante_iniciodoexpediente_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratante_InicioDoExpediente";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_iniciodoexpediente_Internalname, "TitleControlIdToReplace", Ddo_contratante_iniciodoexpediente_Titlecontrolidtoreplace);
         AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace = Ddo_contratante_iniciodoexpediente_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace", AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace);
         edtavDdo_contratante_iniciodoexpedientetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratante_iniciodoexpedientetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratante_iniciodoexpedientetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratante_fimdoexpediente_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratante_FimDoExpediente";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_fimdoexpediente_Internalname, "TitleControlIdToReplace", Ddo_contratante_fimdoexpediente_Titlecontrolidtoreplace);
         AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace = Ddo_contratante_fimdoexpediente_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace", AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace);
         edtavDdo_contratante_fimdoexpedientetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratante_fimdoexpedientetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratante_fimdoexpedientetitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Contratante";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV118DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV118DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E230Y2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV78Contratante_RazaoSocialTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV82Contratante_NomeFantasiaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV86Contratante_CNPJTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV90Contratante_TelefoneTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV94Contratante_RamalTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV98Municipio_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV102Estado_UFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV106Contratante_InicioDoExpedienteTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV112Contratante_FimDoExpedienteTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         if ( AV61ManageFiltersExecutionStep == 1 )
         {
            AV61ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV61ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV61ManageFiltersExecutionStep == 2 )
         {
            AV61ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV61ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratante_RazaoSocial_Titleformat = 2;
         edtContratante_RazaoSocial_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Raz�o Social", AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_RazaoSocial_Internalname, "Title", edtContratante_RazaoSocial_Title);
         edtContratante_NomeFantasia_Titleformat = 2;
         edtContratante_NomeFantasia_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome Fantasia", AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_NomeFantasia_Internalname, "Title", edtContratante_NomeFantasia_Title);
         edtContratante_CNPJ_Titleformat = 2;
         edtContratante_CNPJ_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "CNPJ", AV89ddo_Contratante_CNPJTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_CNPJ_Internalname, "Title", edtContratante_CNPJ_Title);
         edtContratante_Telefone_Titleformat = 2;
         edtContratante_Telefone_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Telefone", AV93ddo_Contratante_TelefoneTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_Telefone_Internalname, "Title", edtContratante_Telefone_Title);
         edtContratante_Ramal_Titleformat = 2;
         edtContratante_Ramal_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ramal", AV97ddo_Contratante_RamalTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_Ramal_Internalname, "Title", edtContratante_Ramal_Title);
         edtMunicipio_Nome_Titleformat = 2;
         edtMunicipio_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Munic�pio", AV101ddo_Municipio_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMunicipio_Nome_Internalname, "Title", edtMunicipio_Nome_Title);
         edtEstado_UF_Titleformat = 2;
         edtEstado_UF_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "UF", AV105ddo_Estado_UFTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEstado_UF_Internalname, "Title", edtEstado_UF_Title);
         edtContratante_InicioDoExpediente_Titleformat = 2;
         edtContratante_InicioDoExpediente_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Exped.", AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_InicioDoExpediente_Internalname, "Title", edtContratante_InicioDoExpediente_Title);
         edtContratante_FimDoExpediente_Titleformat = 2;
         edtContratante_FimDoExpediente_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "�s", AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_FimDoExpediente_Internalname, "Title", edtContratante_FimDoExpediente_Title);
         AV120GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV120GridCurrentPage), 10, 0)));
         AV121GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV121GridPageCount), 10, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         edtavDisplay_Visible = (AV6WWPContext.gxTpr_Display ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDisplay_Visible), 5, 0)));
         AV143WWContratanteDS_1_Areatrabalho_codigo = AV39AreaTrabalho_Codigo;
         AV144WWContratanteDS_2_Tfcontratante_razaosocial = AV79TFContratante_RazaoSocial;
         AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel = AV80TFContratante_RazaoSocial_Sel;
         AV146WWContratanteDS_4_Tfcontratante_nomefantasia = AV83TFContratante_NomeFantasia;
         AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel = AV84TFContratante_NomeFantasia_Sel;
         AV148WWContratanteDS_6_Tfcontratante_cnpj = AV87TFContratante_CNPJ;
         AV149WWContratanteDS_7_Tfcontratante_cnpj_sel = AV88TFContratante_CNPJ_Sel;
         AV150WWContratanteDS_8_Tfcontratante_telefone = AV91TFContratante_Telefone;
         AV151WWContratanteDS_9_Tfcontratante_telefone_sel = AV92TFContratante_Telefone_Sel;
         AV152WWContratanteDS_10_Tfcontratante_ramal = AV95TFContratante_Ramal;
         AV153WWContratanteDS_11_Tfcontratante_ramal_sel = AV96TFContratante_Ramal_Sel;
         AV154WWContratanteDS_12_Tfmunicipio_nome = AV99TFMunicipio_Nome;
         AV155WWContratanteDS_13_Tfmunicipio_nome_sel = AV100TFMunicipio_Nome_Sel;
         AV156WWContratanteDS_14_Tfestado_uf = AV103TFEstado_UF;
         AV157WWContratanteDS_15_Tfestado_uf_sel = AV104TFEstado_UF_Sel;
         AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente = AV107TFContratante_InicioDoExpediente;
         AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to = AV108TFContratante_InicioDoExpediente_To;
         AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente = AV113TFContratante_FimDoExpediente;
         AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to = AV114TFContratante_FimDoExpediente_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV78Contratante_RazaoSocialTitleFilterData", AV78Contratante_RazaoSocialTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV82Contratante_NomeFantasiaTitleFilterData", AV82Contratante_NomeFantasiaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV86Contratante_CNPJTitleFilterData", AV86Contratante_CNPJTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV90Contratante_TelefoneTitleFilterData", AV90Contratante_TelefoneTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV94Contratante_RamalTitleFilterData", AV94Contratante_RamalTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV98Municipio_NomeTitleFilterData", AV98Municipio_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV102Estado_UFTitleFilterData", AV102Estado_UFTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV106Contratante_InicioDoExpedienteTitleFilterData", AV106Contratante_InicioDoExpedienteTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV112Contratante_FimDoExpedienteTitleFilterData", AV112Contratante_FimDoExpedienteTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV65ManageFiltersData", AV65ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E120Y2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV119PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV119PageToGo) ;
         }
      }

      protected void E130Y2( )
      {
         /* Ddo_contratante_razaosocial_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratante_razaosocial_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_razaosocial_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_razaosocial_Internalname, "SortedStatus", Ddo_contratante_razaosocial_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_razaosocial_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_razaosocial_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_razaosocial_Internalname, "SortedStatus", Ddo_contratante_razaosocial_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_razaosocial_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV79TFContratante_RazaoSocial = Ddo_contratante_razaosocial_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFContratante_RazaoSocial", AV79TFContratante_RazaoSocial);
            AV80TFContratante_RazaoSocial_Sel = Ddo_contratante_razaosocial_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratante_RazaoSocial_Sel", AV80TFContratante_RazaoSocial_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E140Y2( )
      {
         /* Ddo_contratante_nomefantasia_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratante_nomefantasia_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_nomefantasia_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_nomefantasia_Internalname, "SortedStatus", Ddo_contratante_nomefantasia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_nomefantasia_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_nomefantasia_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_nomefantasia_Internalname, "SortedStatus", Ddo_contratante_nomefantasia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_nomefantasia_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV83TFContratante_NomeFantasia = Ddo_contratante_nomefantasia_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFContratante_NomeFantasia", AV83TFContratante_NomeFantasia);
            AV84TFContratante_NomeFantasia_Sel = Ddo_contratante_nomefantasia_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContratante_NomeFantasia_Sel", AV84TFContratante_NomeFantasia_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E150Y2( )
      {
         /* Ddo_contratante_cnpj_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratante_cnpj_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_cnpj_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_cnpj_Internalname, "SortedStatus", Ddo_contratante_cnpj_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_cnpj_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_cnpj_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_cnpj_Internalname, "SortedStatus", Ddo_contratante_cnpj_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_cnpj_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV87TFContratante_CNPJ = Ddo_contratante_cnpj_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFContratante_CNPJ", AV87TFContratante_CNPJ);
            AV88TFContratante_CNPJ_Sel = Ddo_contratante_cnpj_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFContratante_CNPJ_Sel", AV88TFContratante_CNPJ_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E160Y2( )
      {
         /* Ddo_contratante_telefone_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratante_telefone_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_telefone_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_telefone_Internalname, "SortedStatus", Ddo_contratante_telefone_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_telefone_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_telefone_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_telefone_Internalname, "SortedStatus", Ddo_contratante_telefone_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_telefone_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV91TFContratante_Telefone = Ddo_contratante_telefone_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFContratante_Telefone", AV91TFContratante_Telefone);
            AV92TFContratante_Telefone_Sel = Ddo_contratante_telefone_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFContratante_Telefone_Sel", AV92TFContratante_Telefone_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E170Y2( )
      {
         /* Ddo_contratante_ramal_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratante_ramal_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_ramal_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_ramal_Internalname, "SortedStatus", Ddo_contratante_ramal_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_ramal_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_ramal_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_ramal_Internalname, "SortedStatus", Ddo_contratante_ramal_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_ramal_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV95TFContratante_Ramal = Ddo_contratante_ramal_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95TFContratante_Ramal", AV95TFContratante_Ramal);
            AV96TFContratante_Ramal_Sel = Ddo_contratante_ramal_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96TFContratante_Ramal_Sel", AV96TFContratante_Ramal_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E180Y2( )
      {
         /* Ddo_municipio_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_municipio_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_municipio_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SortedStatus", Ddo_municipio_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_municipio_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_municipio_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SortedStatus", Ddo_municipio_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_municipio_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV99TFMunicipio_Nome = Ddo_municipio_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99TFMunicipio_Nome", AV99TFMunicipio_Nome);
            AV100TFMunicipio_Nome_Sel = Ddo_municipio_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFMunicipio_Nome_Sel", AV100TFMunicipio_Nome_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E190Y2( )
      {
         /* Ddo_estado_uf_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_estado_uf_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_estado_uf_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_estado_uf_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_estado_uf_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_estado_uf_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV103TFEstado_UF = Ddo_estado_uf_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103TFEstado_UF", AV103TFEstado_UF);
            AV104TFEstado_UF_Sel = Ddo_estado_uf_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFEstado_UF_Sel", AV104TFEstado_UF_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E200Y2( )
      {
         /* Ddo_contratante_iniciodoexpediente_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratante_iniciodoexpediente_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_iniciodoexpediente_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_iniciodoexpediente_Internalname, "SortedStatus", Ddo_contratante_iniciodoexpediente_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_iniciodoexpediente_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_iniciodoexpediente_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_iniciodoexpediente_Internalname, "SortedStatus", Ddo_contratante_iniciodoexpediente_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_iniciodoexpediente_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV107TFContratante_InicioDoExpediente = DateTimeUtil.ResetDate(context.localUtil.CToT( Ddo_contratante_iniciodoexpediente_Filteredtext_get, 2));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TFContratante_InicioDoExpediente", context.localUtil.TToC( AV107TFContratante_InicioDoExpediente, 0, 5, 0, 3, "/", ":", " "));
            AV108TFContratante_InicioDoExpediente_To = DateTimeUtil.ResetDate(context.localUtil.CToT( Ddo_contratante_iniciodoexpediente_Filteredtextto_get, 2));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContratante_InicioDoExpediente_To", context.localUtil.TToC( AV108TFContratante_InicioDoExpediente_To, 0, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV108TFContratante_InicioDoExpediente_To) )
            {
               AV108TFContratante_InicioDoExpediente_To = DateTimeUtil.ResetDate(context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV108TFContratante_InicioDoExpediente_To)), (short)(DateTimeUtil.Month( AV108TFContratante_InicioDoExpediente_To)), (short)(DateTimeUtil.Day( AV108TFContratante_InicioDoExpediente_To)), 23, 59, 59));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContratante_InicioDoExpediente_To", context.localUtil.TToC( AV108TFContratante_InicioDoExpediente_To, 0, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
      }

      protected void E210Y2( )
      {
         /* Ddo_contratante_fimdoexpediente_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratante_fimdoexpediente_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_fimdoexpediente_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_fimdoexpediente_Internalname, "SortedStatus", Ddo_contratante_fimdoexpediente_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_fimdoexpediente_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_fimdoexpediente_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_fimdoexpediente_Internalname, "SortedStatus", Ddo_contratante_fimdoexpediente_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_fimdoexpediente_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV113TFContratante_FimDoExpediente = DateTimeUtil.ResetDate(context.localUtil.CToT( Ddo_contratante_fimdoexpediente_Filteredtext_get, 2));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113TFContratante_FimDoExpediente", context.localUtil.TToC( AV113TFContratante_FimDoExpediente, 0, 5, 0, 3, "/", ":", " "));
            AV114TFContratante_FimDoExpediente_To = DateTimeUtil.ResetDate(context.localUtil.CToT( Ddo_contratante_fimdoexpediente_Filteredtextto_get, 2));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114TFContratante_FimDoExpediente_To", context.localUtil.TToC( AV114TFContratante_FimDoExpediente_To, 0, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV114TFContratante_FimDoExpediente_To) )
            {
               AV114TFContratante_FimDoExpediente_To = DateTimeUtil.ResetDate(context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV114TFContratante_FimDoExpediente_To)), (short)(DateTimeUtil.Month( AV114TFContratante_FimDoExpediente_To)), (short)(DateTimeUtil.Day( AV114TFContratante_FimDoExpediente_To)), 23, 59, 59));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114TFContratante_FimDoExpediente_To", context.localUtil.TToC( AV114TFContratante_FimDoExpediente_To, 0, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
      }

      private void E240Y2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("contratante.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A29Contratante_Codigo);
            AV31Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV31Update);
            AV162Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV31Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV31Update);
            AV162Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("contratante.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A29Contratante_Codigo);
            AV32Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
            AV163Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV32Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
            AV163Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewcontratante.aspx") + "?" + UrlEncode("" +A29Contratante_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV68Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV68Display);
            AV164Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV68Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV68Display);
            AV164Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         edtContratante_RazaoSocial_Link = formatLink("viewcontratante.aspx") + "?" + UrlEncode("" +A29Contratante_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 32;
         }
         sendrow_322( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_32_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(32, GridRow);
         }
      }

      protected void E110Y2( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWContratanteFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika"))), new Object[] {});
            AV61ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV61ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWContratanteFilters")), new Object[] {});
            AV61ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV61ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char2 = AV62ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWContratanteFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV62ManageFiltersXml = GXt_char2;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV165Pgmname+"GridState",  AV62ManageFiltersXml) ;
               AV10GridState.FromXml(AV62ManageFiltersXml, "");
               AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
               S142 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
               S182 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratante_razaosocial_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_razaosocial_Internalname, "SortedStatus", Ddo_contratante_razaosocial_Sortedstatus);
         Ddo_contratante_nomefantasia_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_nomefantasia_Internalname, "SortedStatus", Ddo_contratante_nomefantasia_Sortedstatus);
         Ddo_contratante_cnpj_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_cnpj_Internalname, "SortedStatus", Ddo_contratante_cnpj_Sortedstatus);
         Ddo_contratante_telefone_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_telefone_Internalname, "SortedStatus", Ddo_contratante_telefone_Sortedstatus);
         Ddo_contratante_ramal_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_ramal_Internalname, "SortedStatus", Ddo_contratante_ramal_Sortedstatus);
         Ddo_municipio_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SortedStatus", Ddo_municipio_nome_Sortedstatus);
         Ddo_estado_uf_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
         Ddo_contratante_iniciodoexpediente_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_iniciodoexpediente_Internalname, "SortedStatus", Ddo_contratante_iniciodoexpediente_Sortedstatus);
         Ddo_contratante_fimdoexpediente_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_fimdoexpediente_Internalname, "SortedStatus", Ddo_contratante_fimdoexpediente_Sortedstatus);
      }

      protected void S142( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_contratante_razaosocial_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_razaosocial_Internalname, "SortedStatus", Ddo_contratante_razaosocial_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contratante_nomefantasia_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_nomefantasia_Internalname, "SortedStatus", Ddo_contratante_nomefantasia_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratante_cnpj_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_cnpj_Internalname, "SortedStatus", Ddo_contratante_cnpj_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contratante_telefone_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_telefone_Internalname, "SortedStatus", Ddo_contratante_telefone_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contratante_ramal_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_ramal_Internalname, "SortedStatus", Ddo_contratante_ramal_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_municipio_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SortedStatus", Ddo_municipio_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_estado_uf_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_contratante_iniciodoexpediente_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_iniciodoexpediente_Internalname, "SortedStatus", Ddo_contratante_iniciodoexpediente_Sortedstatus);
         }
         else if ( AV13OrderedBy == 9 )
         {
            Ddo_contratante_fimdoexpediente_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_fimdoexpediente_Internalname, "SortedStatus", Ddo_contratante_fimdoexpediente_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV65ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV66ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV66ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV66ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV66ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV65ManageFiltersData.Add(AV66ManageFiltersDataItem, 0);
         AV66ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV66ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV66ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV66ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV66ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV65ManageFiltersData.Add(AV66ManageFiltersDataItem, 0);
         AV66ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV66ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV65ManageFiltersData.Add(AV66ManageFiltersDataItem, 0);
         AV63ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWContratanteFilters"), "");
         AV166GXV1 = 1;
         while ( AV166GXV1 <= AV63ManageFiltersItems.Count )
         {
            AV64ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV63ManageFiltersItems.Item(AV166GXV1));
            AV66ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV66ManageFiltersDataItem.gxTpr_Title = AV64ManageFiltersItem.gxTpr_Title;
            AV66ManageFiltersDataItem.gxTpr_Eventkey = AV64ManageFiltersItem.gxTpr_Title;
            AV66ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV65ManageFiltersData.Add(AV66ManageFiltersDataItem, 0);
            if ( AV65ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV166GXV1 = (int)(AV166GXV1+1);
         }
         if ( AV65ManageFiltersData.Count > 3 )
         {
            AV66ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV66ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV65ManageFiltersData.Add(AV66ManageFiltersDataItem, 0);
            AV66ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV66ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV66ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV66ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV66ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV66ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV65ManageFiltersData.Add(AV66ManageFiltersDataItem, 0);
         }
      }

      protected void S172( )
      {
         /* 'CLEANFILTERS' Routine */
         AV39AreaTrabalho_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39AreaTrabalho_Codigo), 6, 0)));
         AV79TFContratante_RazaoSocial = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFContratante_RazaoSocial", AV79TFContratante_RazaoSocial);
         Ddo_contratante_razaosocial_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_razaosocial_Internalname, "FilteredText_set", Ddo_contratante_razaosocial_Filteredtext_set);
         AV80TFContratante_RazaoSocial_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratante_RazaoSocial_Sel", AV80TFContratante_RazaoSocial_Sel);
         Ddo_contratante_razaosocial_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_razaosocial_Internalname, "SelectedValue_set", Ddo_contratante_razaosocial_Selectedvalue_set);
         AV83TFContratante_NomeFantasia = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFContratante_NomeFantasia", AV83TFContratante_NomeFantasia);
         Ddo_contratante_nomefantasia_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_nomefantasia_Internalname, "FilteredText_set", Ddo_contratante_nomefantasia_Filteredtext_set);
         AV84TFContratante_NomeFantasia_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContratante_NomeFantasia_Sel", AV84TFContratante_NomeFantasia_Sel);
         Ddo_contratante_nomefantasia_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_nomefantasia_Internalname, "SelectedValue_set", Ddo_contratante_nomefantasia_Selectedvalue_set);
         AV87TFContratante_CNPJ = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFContratante_CNPJ", AV87TFContratante_CNPJ);
         Ddo_contratante_cnpj_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_cnpj_Internalname, "FilteredText_set", Ddo_contratante_cnpj_Filteredtext_set);
         AV88TFContratante_CNPJ_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFContratante_CNPJ_Sel", AV88TFContratante_CNPJ_Sel);
         Ddo_contratante_cnpj_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_cnpj_Internalname, "SelectedValue_set", Ddo_contratante_cnpj_Selectedvalue_set);
         AV91TFContratante_Telefone = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFContratante_Telefone", AV91TFContratante_Telefone);
         Ddo_contratante_telefone_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_telefone_Internalname, "FilteredText_set", Ddo_contratante_telefone_Filteredtext_set);
         AV92TFContratante_Telefone_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFContratante_Telefone_Sel", AV92TFContratante_Telefone_Sel);
         Ddo_contratante_telefone_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_telefone_Internalname, "SelectedValue_set", Ddo_contratante_telefone_Selectedvalue_set);
         AV95TFContratante_Ramal = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95TFContratante_Ramal", AV95TFContratante_Ramal);
         Ddo_contratante_ramal_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_ramal_Internalname, "FilteredText_set", Ddo_contratante_ramal_Filteredtext_set);
         AV96TFContratante_Ramal_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96TFContratante_Ramal_Sel", AV96TFContratante_Ramal_Sel);
         Ddo_contratante_ramal_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_ramal_Internalname, "SelectedValue_set", Ddo_contratante_ramal_Selectedvalue_set);
         AV99TFMunicipio_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99TFMunicipio_Nome", AV99TFMunicipio_Nome);
         Ddo_municipio_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "FilteredText_set", Ddo_municipio_nome_Filteredtext_set);
         AV100TFMunicipio_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFMunicipio_Nome_Sel", AV100TFMunicipio_Nome_Sel);
         Ddo_municipio_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SelectedValue_set", Ddo_municipio_nome_Selectedvalue_set);
         AV103TFEstado_UF = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103TFEstado_UF", AV103TFEstado_UF);
         Ddo_estado_uf_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "FilteredText_set", Ddo_estado_uf_Filteredtext_set);
         AV104TFEstado_UF_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFEstado_UF_Sel", AV104TFEstado_UF_Sel);
         Ddo_estado_uf_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "SelectedValue_set", Ddo_estado_uf_Selectedvalue_set);
         AV107TFContratante_InicioDoExpediente = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TFContratante_InicioDoExpediente", context.localUtil.TToC( AV107TFContratante_InicioDoExpediente, 0, 5, 0, 3, "/", ":", " "));
         Ddo_contratante_iniciodoexpediente_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_iniciodoexpediente_Internalname, "FilteredText_set", Ddo_contratante_iniciodoexpediente_Filteredtext_set);
         AV108TFContratante_InicioDoExpediente_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContratante_InicioDoExpediente_To", context.localUtil.TToC( AV108TFContratante_InicioDoExpediente_To, 0, 5, 0, 3, "/", ":", " "));
         Ddo_contratante_iniciodoexpediente_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_iniciodoexpediente_Internalname, "FilteredTextTo_set", Ddo_contratante_iniciodoexpediente_Filteredtextto_set);
         AV113TFContratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113TFContratante_FimDoExpediente", context.localUtil.TToC( AV113TFContratante_FimDoExpediente, 0, 5, 0, 3, "/", ":", " "));
         Ddo_contratante_fimdoexpediente_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_fimdoexpediente_Internalname, "FilteredText_set", Ddo_contratante_fimdoexpediente_Filteredtext_set);
         AV114TFContratante_FimDoExpediente_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114TFContratante_FimDoExpediente_To", context.localUtil.TToC( AV114TFContratante_FimDoExpediente_To, 0, 5, 0, 3, "/", ":", " "));
         Ddo_contratante_fimdoexpediente_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_fimdoexpediente_Internalname, "FilteredTextTo_set", Ddo_contratante_fimdoexpediente_Filteredtextto_set);
      }

      protected void S132( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get(AV165Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV165Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV35Session.Get(AV165Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S182( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV167GXV2 = 1;
         while ( AV167GXV2 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV167GXV2));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "AREATRABALHO_CODIGO") == 0 )
            {
               AV39AreaTrabalho_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39AreaTrabalho_Codigo), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_RAZAOSOCIAL") == 0 )
            {
               AV79TFContratante_RazaoSocial = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79TFContratante_RazaoSocial", AV79TFContratante_RazaoSocial);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFContratante_RazaoSocial)) )
               {
                  Ddo_contratante_razaosocial_Filteredtext_set = AV79TFContratante_RazaoSocial;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_razaosocial_Internalname, "FilteredText_set", Ddo_contratante_razaosocial_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_RAZAOSOCIAL_SEL") == 0 )
            {
               AV80TFContratante_RazaoSocial_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratante_RazaoSocial_Sel", AV80TFContratante_RazaoSocial_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80TFContratante_RazaoSocial_Sel)) )
               {
                  Ddo_contratante_razaosocial_Selectedvalue_set = AV80TFContratante_RazaoSocial_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_razaosocial_Internalname, "SelectedValue_set", Ddo_contratante_razaosocial_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_NOMEFANTASIA") == 0 )
            {
               AV83TFContratante_NomeFantasia = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFContratante_NomeFantasia", AV83TFContratante_NomeFantasia);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83TFContratante_NomeFantasia)) )
               {
                  Ddo_contratante_nomefantasia_Filteredtext_set = AV83TFContratante_NomeFantasia;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_nomefantasia_Internalname, "FilteredText_set", Ddo_contratante_nomefantasia_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_NOMEFANTASIA_SEL") == 0 )
            {
               AV84TFContratante_NomeFantasia_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContratante_NomeFantasia_Sel", AV84TFContratante_NomeFantasia_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84TFContratante_NomeFantasia_Sel)) )
               {
                  Ddo_contratante_nomefantasia_Selectedvalue_set = AV84TFContratante_NomeFantasia_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_nomefantasia_Internalname, "SelectedValue_set", Ddo_contratante_nomefantasia_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_CNPJ") == 0 )
            {
               AV87TFContratante_CNPJ = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFContratante_CNPJ", AV87TFContratante_CNPJ);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87TFContratante_CNPJ)) )
               {
                  Ddo_contratante_cnpj_Filteredtext_set = AV87TFContratante_CNPJ;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_cnpj_Internalname, "FilteredText_set", Ddo_contratante_cnpj_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_CNPJ_SEL") == 0 )
            {
               AV88TFContratante_CNPJ_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFContratante_CNPJ_Sel", AV88TFContratante_CNPJ_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88TFContratante_CNPJ_Sel)) )
               {
                  Ddo_contratante_cnpj_Selectedvalue_set = AV88TFContratante_CNPJ_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_cnpj_Internalname, "SelectedValue_set", Ddo_contratante_cnpj_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_TELEFONE") == 0 )
            {
               AV91TFContratante_Telefone = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFContratante_Telefone", AV91TFContratante_Telefone);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91TFContratante_Telefone)) )
               {
                  Ddo_contratante_telefone_Filteredtext_set = AV91TFContratante_Telefone;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_telefone_Internalname, "FilteredText_set", Ddo_contratante_telefone_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_TELEFONE_SEL") == 0 )
            {
               AV92TFContratante_Telefone_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFContratante_Telefone_Sel", AV92TFContratante_Telefone_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92TFContratante_Telefone_Sel)) )
               {
                  Ddo_contratante_telefone_Selectedvalue_set = AV92TFContratante_Telefone_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_telefone_Internalname, "SelectedValue_set", Ddo_contratante_telefone_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_RAMAL") == 0 )
            {
               AV95TFContratante_Ramal = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95TFContratante_Ramal", AV95TFContratante_Ramal);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95TFContratante_Ramal)) )
               {
                  Ddo_contratante_ramal_Filteredtext_set = AV95TFContratante_Ramal;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_ramal_Internalname, "FilteredText_set", Ddo_contratante_ramal_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_RAMAL_SEL") == 0 )
            {
               AV96TFContratante_Ramal_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96TFContratante_Ramal_Sel", AV96TFContratante_Ramal_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96TFContratante_Ramal_Sel)) )
               {
                  Ddo_contratante_ramal_Selectedvalue_set = AV96TFContratante_Ramal_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_ramal_Internalname, "SelectedValue_set", Ddo_contratante_ramal_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMUNICIPIO_NOME") == 0 )
            {
               AV99TFMunicipio_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV99TFMunicipio_Nome", AV99TFMunicipio_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99TFMunicipio_Nome)) )
               {
                  Ddo_municipio_nome_Filteredtext_set = AV99TFMunicipio_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "FilteredText_set", Ddo_municipio_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMUNICIPIO_NOME_SEL") == 0 )
            {
               AV100TFMunicipio_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFMunicipio_Nome_Sel", AV100TFMunicipio_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100TFMunicipio_Nome_Sel)) )
               {
                  Ddo_municipio_nome_Selectedvalue_set = AV100TFMunicipio_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SelectedValue_set", Ddo_municipio_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFESTADO_UF") == 0 )
            {
               AV103TFEstado_UF = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103TFEstado_UF", AV103TFEstado_UF);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103TFEstado_UF)) )
               {
                  Ddo_estado_uf_Filteredtext_set = AV103TFEstado_UF;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "FilteredText_set", Ddo_estado_uf_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFESTADO_UF_SEL") == 0 )
            {
               AV104TFEstado_UF_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFEstado_UF_Sel", AV104TFEstado_UF_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104TFEstado_UF_Sel)) )
               {
                  Ddo_estado_uf_Selectedvalue_set = AV104TFEstado_UF_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "SelectedValue_set", Ddo_estado_uf_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_INICIODOEXPEDIENTE") == 0 )
            {
               AV107TFContratante_InicioDoExpediente = DateTimeUtil.ResetDate(context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Value, 2));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107TFContratante_InicioDoExpediente", context.localUtil.TToC( AV107TFContratante_InicioDoExpediente, 0, 5, 0, 3, "/", ":", " "));
               AV108TFContratante_InicioDoExpediente_To = DateTimeUtil.ResetDate(context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Valueto, 2));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContratante_InicioDoExpediente_To", context.localUtil.TToC( AV108TFContratante_InicioDoExpediente_To, 0, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV107TFContratante_InicioDoExpediente) )
               {
                  AV109DDO_Contratante_InicioDoExpedienteAuxDate = DateTimeUtil.ResetTime(AV107TFContratante_InicioDoExpediente);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109DDO_Contratante_InicioDoExpedienteAuxDate", context.localUtil.Format(AV109DDO_Contratante_InicioDoExpedienteAuxDate, "99/99/99"));
                  Ddo_contratante_iniciodoexpediente_Filteredtext_set = context.localUtil.DToC( AV109DDO_Contratante_InicioDoExpedienteAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_iniciodoexpediente_Internalname, "FilteredText_set", Ddo_contratante_iniciodoexpediente_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV108TFContratante_InicioDoExpediente_To) )
               {
                  AV110DDO_Contratante_InicioDoExpedienteAuxDateTo = DateTimeUtil.ResetTime(AV108TFContratante_InicioDoExpediente_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110DDO_Contratante_InicioDoExpedienteAuxDateTo", context.localUtil.Format(AV110DDO_Contratante_InicioDoExpedienteAuxDateTo, "99/99/99"));
                  Ddo_contratante_iniciodoexpediente_Filteredtextto_set = context.localUtil.DToC( AV110DDO_Contratante_InicioDoExpedienteAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_iniciodoexpediente_Internalname, "FilteredTextTo_set", Ddo_contratante_iniciodoexpediente_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_FIMDOEXPEDIENTE") == 0 )
            {
               AV113TFContratante_FimDoExpediente = DateTimeUtil.ResetDate(context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Value, 2));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113TFContratante_FimDoExpediente", context.localUtil.TToC( AV113TFContratante_FimDoExpediente, 0, 5, 0, 3, "/", ":", " "));
               AV114TFContratante_FimDoExpediente_To = DateTimeUtil.ResetDate(context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Valueto, 2));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114TFContratante_FimDoExpediente_To", context.localUtil.TToC( AV114TFContratante_FimDoExpediente_To, 0, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV113TFContratante_FimDoExpediente) )
               {
                  AV115DDO_Contratante_FimDoExpedienteAuxDate = DateTimeUtil.ResetTime(AV113TFContratante_FimDoExpediente);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115DDO_Contratante_FimDoExpedienteAuxDate", context.localUtil.Format(AV115DDO_Contratante_FimDoExpedienteAuxDate, "99/99/99"));
                  Ddo_contratante_fimdoexpediente_Filteredtext_set = context.localUtil.DToC( AV115DDO_Contratante_FimDoExpedienteAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_fimdoexpediente_Internalname, "FilteredText_set", Ddo_contratante_fimdoexpediente_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV114TFContratante_FimDoExpediente_To) )
               {
                  AV116DDO_Contratante_FimDoExpedienteAuxDateTo = DateTimeUtil.ResetTime(AV114TFContratante_FimDoExpediente_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116DDO_Contratante_FimDoExpedienteAuxDateTo", context.localUtil.Format(AV116DDO_Contratante_FimDoExpedienteAuxDateTo, "99/99/99"));
                  Ddo_contratante_fimdoexpediente_Filteredtextto_set = context.localUtil.DToC( AV116DDO_Contratante_FimDoExpedienteAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_fimdoexpediente_Internalname, "FilteredTextTo_set", Ddo_contratante_fimdoexpediente_Filteredtextto_set);
               }
            }
            AV167GXV2 = (int)(AV167GXV2+1);
         }
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV35Session.Get(AV165Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV39AreaTrabalho_Codigo) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "AREATRABALHO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV39AreaTrabalho_Codigo), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79TFContratante_RazaoSocial)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTE_RAZAOSOCIAL";
            AV11GridStateFilterValue.gxTpr_Value = AV79TFContratante_RazaoSocial;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80TFContratante_RazaoSocial_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTE_RAZAOSOCIAL_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV80TFContratante_RazaoSocial_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83TFContratante_NomeFantasia)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTE_NOMEFANTASIA";
            AV11GridStateFilterValue.gxTpr_Value = AV83TFContratante_NomeFantasia;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84TFContratante_NomeFantasia_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTE_NOMEFANTASIA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV84TFContratante_NomeFantasia_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87TFContratante_CNPJ)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTE_CNPJ";
            AV11GridStateFilterValue.gxTpr_Value = AV87TFContratante_CNPJ;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88TFContratante_CNPJ_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTE_CNPJ_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV88TFContratante_CNPJ_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91TFContratante_Telefone)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTE_TELEFONE";
            AV11GridStateFilterValue.gxTpr_Value = AV91TFContratante_Telefone;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92TFContratante_Telefone_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTE_TELEFONE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV92TFContratante_Telefone_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95TFContratante_Ramal)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTE_RAMAL";
            AV11GridStateFilterValue.gxTpr_Value = AV95TFContratante_Ramal;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96TFContratante_Ramal_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTE_RAMAL_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV96TFContratante_Ramal_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99TFMunicipio_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMUNICIPIO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV99TFMunicipio_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100TFMunicipio_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMUNICIPIO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV100TFMunicipio_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103TFEstado_UF)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFESTADO_UF";
            AV11GridStateFilterValue.gxTpr_Value = AV103TFEstado_UF;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104TFEstado_UF_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFESTADO_UF_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV104TFEstado_UF_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV107TFContratante_InicioDoExpediente) && (DateTime.MinValue==AV108TFContratante_InicioDoExpediente_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTE_INICIODOEXPEDIENTE";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV107TFContratante_InicioDoExpediente, 0, 5, 0, 3, "/", ":", " ");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV108TFContratante_InicioDoExpediente_To, 0, 5, 0, 3, "/", ":", " ");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV113TFContratante_FimDoExpediente) && (DateTime.MinValue==AV114TFContratante_FimDoExpediente_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTE_FIMDOEXPEDIENTE";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV113TFContratante_FimDoExpediente, 0, 5, 0, 3, "/", ":", " ");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV114TFContratante_FimDoExpediente_To, 0, 5, 0, 3, "/", ":", " ");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV165Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S122( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV165Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Contratante";
         AV35Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_0Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_0Y2( true) ;
         }
         else
         {
            wb_table2_8_0Y2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_0Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_26_0Y2( true) ;
         }
         else
         {
            wb_table3_26_0Y2( false) ;
         }
         return  ;
      }

      protected void wb_table3_26_0Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0Y2e( true) ;
         }
         else
         {
            wb_table1_2_0Y2e( false) ;
         }
      }

      protected void wb_table3_26_0Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_29_0Y2( true) ;
         }
         else
         {
            wb_table4_29_0Y2( false) ;
         }
         return  ;
      }

      protected void wb_table4_29_0Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_26_0Y2e( true) ;
         }
         else
         {
            wb_table3_26_0Y2e( false) ;
         }
      }

      protected void wb_table4_29_0Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"32\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo Area de Trabalho") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Jur�dica") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_RazaoSocial_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_RazaoSocial_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_RazaoSocial_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_NomeFantasia_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_NomeFantasia_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_NomeFantasia_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_CNPJ_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_CNPJ_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_CNPJ_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_Telefone_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_Telefone_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_Telefone_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(60), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_Ramal_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_Ramal_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_Ramal_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMunicipio_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtMunicipio_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMunicipio_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtEstado_UF_Titleformat == 0 )
               {
                  context.SendWebValue( edtEstado_UF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtEstado_UF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(42), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_InicioDoExpediente_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_InicioDoExpediente_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_InicioDoExpediente_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(40), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_FimDoExpediente_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_FimDoExpediente_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_FimDoExpediente_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV68Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A335Contratante_PessoaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A9Contratante_RazaoSocial));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_RazaoSocial_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_RazaoSocial_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratante_RazaoSocial_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A10Contratante_NomeFantasia));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_NomeFantasia_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_NomeFantasia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A12Contratante_CNPJ);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_CNPJ_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_CNPJ_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A31Contratante_Telefone));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_Telefone_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_Telefone_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A32Contratante_Ramal));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_Ramal_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_Ramal_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A26Municipio_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMunicipio_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMunicipio_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A23Estado_UF));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtEstado_UF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtEstado_UF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1448Contratante_InicioDoExpediente, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_InicioDoExpediente_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_InicioDoExpediente_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1192Contratante_FimDoExpediente, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_FimDoExpediente_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_FimDoExpediente_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 32 )
         {
            wbEnd = 0;
            nRC_GXsfl_32 = (short)(nGXsfl_32_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_29_0Y2e( true) ;
         }
         else
         {
            wb_table4_29_0Y2e( false) ;
         }
      }

      protected void wb_table2_8_0Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_11_0Y2( true) ;
         }
         else
         {
            wb_table5_11_0Y2( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_0Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_16_0Y2( true) ;
         }
         else
         {
            wb_table6_16_0Y2( false) ;
         }
         return  ;
      }

      protected void wb_table6_16_0Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_0Y2e( true) ;
         }
         else
         {
            wb_table2_8_0Y2e( false) ;
         }
      }

      protected void wb_table6_16_0Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextareatrabalho_codigo_Internalname, "C�digo Area de Trabalho", "", "", lblFiltertextareatrabalho_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWContratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAreatrabalho_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39AreaTrabalho_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV39AreaTrabalho_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAreatrabalho_codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_16_0Y2e( true) ;
         }
         else
         {
            wb_table6_16_0Y2e( false) ;
         }
      }

      protected void wb_table5_11_0Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratantetitle_Internalname, "Contratante", "", "", lblContratantetitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContratante.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_0Y2e( true) ;
         }
         else
         {
            wb_table5_11_0Y2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0Y2( ) ;
         WS0Y2( ) ;
         WE0Y2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311731890");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontratante.js", "?2020311731890");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_322( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_32_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_32_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_32_idx;
         edtAreaTrabalho_Codigo_Internalname = "AREATRABALHO_CODIGO_"+sGXsfl_32_idx;
         edtContratante_Codigo_Internalname = "CONTRATANTE_CODIGO_"+sGXsfl_32_idx;
         edtContratante_PessoaCod_Internalname = "CONTRATANTE_PESSOACOD_"+sGXsfl_32_idx;
         edtContratante_RazaoSocial_Internalname = "CONTRATANTE_RAZAOSOCIAL_"+sGXsfl_32_idx;
         edtContratante_NomeFantasia_Internalname = "CONTRATANTE_NOMEFANTASIA_"+sGXsfl_32_idx;
         edtContratante_CNPJ_Internalname = "CONTRATANTE_CNPJ_"+sGXsfl_32_idx;
         edtContratante_Telefone_Internalname = "CONTRATANTE_TELEFONE_"+sGXsfl_32_idx;
         edtContratante_Ramal_Internalname = "CONTRATANTE_RAMAL_"+sGXsfl_32_idx;
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME_"+sGXsfl_32_idx;
         edtEstado_UF_Internalname = "ESTADO_UF_"+sGXsfl_32_idx;
         edtContratante_InicioDoExpediente_Internalname = "CONTRATANTE_INICIODOEXPEDIENTE_"+sGXsfl_32_idx;
         edtContratante_FimDoExpediente_Internalname = "CONTRATANTE_FIMDOEXPEDIENTE_"+sGXsfl_32_idx;
      }

      protected void SubsflControlProps_fel_322( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_32_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_32_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_32_fel_idx;
         edtAreaTrabalho_Codigo_Internalname = "AREATRABALHO_CODIGO_"+sGXsfl_32_fel_idx;
         edtContratante_Codigo_Internalname = "CONTRATANTE_CODIGO_"+sGXsfl_32_fel_idx;
         edtContratante_PessoaCod_Internalname = "CONTRATANTE_PESSOACOD_"+sGXsfl_32_fel_idx;
         edtContratante_RazaoSocial_Internalname = "CONTRATANTE_RAZAOSOCIAL_"+sGXsfl_32_fel_idx;
         edtContratante_NomeFantasia_Internalname = "CONTRATANTE_NOMEFANTASIA_"+sGXsfl_32_fel_idx;
         edtContratante_CNPJ_Internalname = "CONTRATANTE_CNPJ_"+sGXsfl_32_fel_idx;
         edtContratante_Telefone_Internalname = "CONTRATANTE_TELEFONE_"+sGXsfl_32_fel_idx;
         edtContratante_Ramal_Internalname = "CONTRATANTE_RAMAL_"+sGXsfl_32_fel_idx;
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME_"+sGXsfl_32_fel_idx;
         edtEstado_UF_Internalname = "ESTADO_UF_"+sGXsfl_32_fel_idx;
         edtContratante_InicioDoExpediente_Internalname = "CONTRATANTE_INICIODOEXPEDIENTE_"+sGXsfl_32_fel_idx;
         edtContratante_FimDoExpediente_Internalname = "CONTRATANTE_FIMDOEXPEDIENTE_"+sGXsfl_32_fel_idx;
      }

      protected void sendrow_322( )
      {
         SubsflControlProps_322( ) ;
         WB0Y0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_32_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_32_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_32_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV31Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV162Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV162Update_GXI : context.PathToRelativeUrl( AV31Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV31Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV163Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV163Delete_GXI : context.PathToRelativeUrl( AV32Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV68Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV68Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV164Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV68Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV68Display)) ? AV164Display_GXI : context.PathToRelativeUrl( AV68Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDisplay_Visible,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV68Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAreaTrabalho_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAreaTrabalho_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_PessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A335Contratante_PessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A335Contratante_PessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_PessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_RazaoSocial_Internalname,StringUtil.RTrim( A9Contratante_RazaoSocial),StringUtil.RTrim( context.localUtil.Format( A9Contratante_RazaoSocial, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContratante_RazaoSocial_Link,(String)"",(String)"",(String)"",(String)edtContratante_RazaoSocial_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_NomeFantasia_Internalname,StringUtil.RTrim( A10Contratante_NomeFantasia),StringUtil.RTrim( context.localUtil.Format( A10Contratante_NomeFantasia, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_NomeFantasia_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_CNPJ_Internalname,(String)A12Contratante_CNPJ,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_CNPJ_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            if ( context.isSmartDevice( ) )
            {
               gxphoneLink = "tel:" + StringUtil.RTrim( A31Contratante_Telefone);
            }
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_Telefone_Internalname,StringUtil.RTrim( A31Contratante_Telefone),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)gxphoneLink,(String)"",(String)"",(String)"",(String)edtContratante_Telefone_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"tel",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)0,(bool)true,(String)"Phone",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_Ramal_Internalname,StringUtil.RTrim( A32Contratante_Ramal),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_Ramal_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)60,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)-1,(bool)true,(String)"Ramal",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMunicipio_Nome_Internalname,StringUtil.RTrim( A26Municipio_Nome),StringUtil.RTrim( context.localUtil.Format( A26Municipio_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMunicipio_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtEstado_UF_Internalname,StringUtil.RTrim( A23Estado_UF),StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtEstado_UF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)2,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)-1,(bool)true,(String)"UF",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_InicioDoExpediente_Internalname,context.localUtil.TToC( A1448Contratante_InicioDoExpediente, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1448Contratante_InicioDoExpediente, "99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_InicioDoExpediente_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)42,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)0,(bool)true,(String)"Time",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_FimDoExpediente_Internalname,context.localUtil.TToC( A1192Contratante_FimDoExpediente, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1192Contratante_FimDoExpediente, "99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_FimDoExpediente_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)40,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)0,(bool)true,(String)"Time",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_CODIGO"+"_"+sGXsfl_32_idx, GetSecureSignedToken( sGXsfl_32_idx, context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_CODIGO"+"_"+sGXsfl_32_idx, GetSecureSignedToken( sGXsfl_32_idx, context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_32_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_32_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_32_idx+1));
            sGXsfl_32_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_32_idx), 4, 0)), 4, "0");
            SubsflControlProps_322( ) ;
         }
         /* End function sendrow_322 */
      }

      protected void init_default_properties( )
      {
         lblContratantetitle_Internalname = "CONTRATANTETITLE";
         tblTableactions_Internalname = "TABLEACTIONS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         lblFiltertextareatrabalho_codigo_Internalname = "FILTERTEXTAREATRABALHO_CODIGO";
         edtavAreatrabalho_codigo_Internalname = "vAREATRABALHO_CODIGO";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtAreaTrabalho_Codigo_Internalname = "AREATRABALHO_CODIGO";
         edtContratante_Codigo_Internalname = "CONTRATANTE_CODIGO";
         edtContratante_PessoaCod_Internalname = "CONTRATANTE_PESSOACOD";
         edtContratante_RazaoSocial_Internalname = "CONTRATANTE_RAZAOSOCIAL";
         edtContratante_NomeFantasia_Internalname = "CONTRATANTE_NOMEFANTASIA";
         edtContratante_CNPJ_Internalname = "CONTRATANTE_CNPJ";
         edtContratante_Telefone_Internalname = "CONTRATANTE_TELEFONE";
         edtContratante_Ramal_Internalname = "CONTRATANTE_RAMAL";
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME";
         edtEstado_UF_Internalname = "ESTADO_UF";
         edtContratante_InicioDoExpediente_Internalname = "CONTRATANTE_INICIODOEXPEDIENTE";
         edtContratante_FimDoExpediente_Internalname = "CONTRATANTE_FIMDOEXPEDIENTE";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         edtavTfcontratante_razaosocial_Internalname = "vTFCONTRATANTE_RAZAOSOCIAL";
         edtavTfcontratante_razaosocial_sel_Internalname = "vTFCONTRATANTE_RAZAOSOCIAL_SEL";
         edtavTfcontratante_nomefantasia_Internalname = "vTFCONTRATANTE_NOMEFANTASIA";
         edtavTfcontratante_nomefantasia_sel_Internalname = "vTFCONTRATANTE_NOMEFANTASIA_SEL";
         edtavTfcontratante_cnpj_Internalname = "vTFCONTRATANTE_CNPJ";
         edtavTfcontratante_cnpj_sel_Internalname = "vTFCONTRATANTE_CNPJ_SEL";
         edtavTfcontratante_telefone_Internalname = "vTFCONTRATANTE_TELEFONE";
         edtavTfcontratante_telefone_sel_Internalname = "vTFCONTRATANTE_TELEFONE_SEL";
         edtavTfcontratante_ramal_Internalname = "vTFCONTRATANTE_RAMAL";
         edtavTfcontratante_ramal_sel_Internalname = "vTFCONTRATANTE_RAMAL_SEL";
         edtavTfmunicipio_nome_Internalname = "vTFMUNICIPIO_NOME";
         edtavTfmunicipio_nome_sel_Internalname = "vTFMUNICIPIO_NOME_SEL";
         edtavTfestado_uf_Internalname = "vTFESTADO_UF";
         edtavTfestado_uf_sel_Internalname = "vTFESTADO_UF_SEL";
         edtavTfcontratante_iniciodoexpediente_Internalname = "vTFCONTRATANTE_INICIODOEXPEDIENTE";
         edtavTfcontratante_iniciodoexpediente_to_Internalname = "vTFCONTRATANTE_INICIODOEXPEDIENTE_TO";
         edtavDdo_contratante_iniciodoexpedienteauxdate_Internalname = "vDDO_CONTRATANTE_INICIODOEXPEDIENTEAUXDATE";
         edtavDdo_contratante_iniciodoexpedienteauxdateto_Internalname = "vDDO_CONTRATANTE_INICIODOEXPEDIENTEAUXDATETO";
         divDdo_contratante_iniciodoexpedienteauxdates_Internalname = "DDO_CONTRATANTE_INICIODOEXPEDIENTEAUXDATES";
         edtavTfcontratante_fimdoexpediente_Internalname = "vTFCONTRATANTE_FIMDOEXPEDIENTE";
         edtavTfcontratante_fimdoexpediente_to_Internalname = "vTFCONTRATANTE_FIMDOEXPEDIENTE_TO";
         edtavDdo_contratante_fimdoexpedienteauxdate_Internalname = "vDDO_CONTRATANTE_FIMDOEXPEDIENTEAUXDATE";
         edtavDdo_contratante_fimdoexpedienteauxdateto_Internalname = "vDDO_CONTRATANTE_FIMDOEXPEDIENTEAUXDATETO";
         divDdo_contratante_fimdoexpedienteauxdates_Internalname = "DDO_CONTRATANTE_FIMDOEXPEDIENTEAUXDATES";
         Ddo_contratante_razaosocial_Internalname = "DDO_CONTRATANTE_RAZAOSOCIAL";
         edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE";
         Ddo_contratante_nomefantasia_Internalname = "DDO_CONTRATANTE_NOMEFANTASIA";
         edtavDdo_contratante_nomefantasiatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATANTE_NOMEFANTASIATITLECONTROLIDTOREPLACE";
         Ddo_contratante_cnpj_Internalname = "DDO_CONTRATANTE_CNPJ";
         edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE";
         Ddo_contratante_telefone_Internalname = "DDO_CONTRATANTE_TELEFONE";
         edtavDdo_contratante_telefonetitlecontrolidtoreplace_Internalname = "vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE";
         Ddo_contratante_ramal_Internalname = "DDO_CONTRATANTE_RAMAL";
         edtavDdo_contratante_ramaltitlecontrolidtoreplace_Internalname = "vDDO_CONTRATANTE_RAMALTITLECONTROLIDTOREPLACE";
         Ddo_municipio_nome_Internalname = "DDO_MUNICIPIO_NOME";
         edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname = "vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_estado_uf_Internalname = "DDO_ESTADO_UF";
         edtavDdo_estado_uftitlecontrolidtoreplace_Internalname = "vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE";
         Ddo_contratante_iniciodoexpediente_Internalname = "DDO_CONTRATANTE_INICIODOEXPEDIENTE";
         edtavDdo_contratante_iniciodoexpedientetitlecontrolidtoreplace_Internalname = "vDDO_CONTRATANTE_INICIODOEXPEDIENTETITLECONTROLIDTOREPLACE";
         Ddo_contratante_fimdoexpediente_Internalname = "DDO_CONTRATANTE_FIMDOEXPEDIENTE";
         edtavDdo_contratante_fimdoexpedientetitlecontrolidtoreplace_Internalname = "vDDO_CONTRATANTE_FIMDOEXPEDIENTETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratante_FimDoExpediente_Jsonclick = "";
         edtContratante_InicioDoExpediente_Jsonclick = "";
         edtEstado_UF_Jsonclick = "";
         edtMunicipio_Nome_Jsonclick = "";
         edtContratante_Ramal_Jsonclick = "";
         edtContratante_Telefone_Jsonclick = "";
         edtContratante_CNPJ_Jsonclick = "";
         edtContratante_NomeFantasia_Jsonclick = "";
         edtContratante_RazaoSocial_Jsonclick = "";
         edtContratante_PessoaCod_Jsonclick = "";
         edtContratante_Codigo_Jsonclick = "";
         edtAreaTrabalho_Codigo_Jsonclick = "";
         edtavAreatrabalho_codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratante_RazaoSocial_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         edtContratante_FimDoExpediente_Titleformat = 0;
         edtContratante_InicioDoExpediente_Titleformat = 0;
         edtEstado_UF_Titleformat = 0;
         edtMunicipio_Nome_Titleformat = 0;
         edtContratante_Ramal_Titleformat = 0;
         edtContratante_Telefone_Titleformat = 0;
         edtContratante_CNPJ_Titleformat = 0;
         edtContratante_NomeFantasia_Titleformat = 0;
         edtContratante_RazaoSocial_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavDisplay_Visible = -1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         edtContratante_FimDoExpediente_Title = "�s";
         edtContratante_InicioDoExpediente_Title = "Exped.";
         edtEstado_UF_Title = "UF";
         edtMunicipio_Nome_Title = "Munic�pio";
         edtContratante_Ramal_Title = "Ramal";
         edtContratante_Telefone_Title = "Telefone";
         edtContratante_CNPJ_Title = "CNPJ";
         edtContratante_NomeFantasia_Title = "Nome Fantasia";
         edtContratante_RazaoSocial_Title = "Raz�o Social";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_contratante_fimdoexpedientetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratante_iniciodoexpedientetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_estado_uftitlecontrolidtoreplace_Visible = 1;
         edtavDdo_municipio_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratante_ramaltitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratante_telefonetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratante_nomefantasiatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratante_fimdoexpedienteauxdateto_Jsonclick = "";
         edtavDdo_contratante_fimdoexpedienteauxdate_Jsonclick = "";
         edtavTfcontratante_fimdoexpediente_to_Jsonclick = "";
         edtavTfcontratante_fimdoexpediente_to_Visible = 1;
         edtavTfcontratante_fimdoexpediente_Jsonclick = "";
         edtavTfcontratante_fimdoexpediente_Visible = 1;
         edtavDdo_contratante_iniciodoexpedienteauxdateto_Jsonclick = "";
         edtavDdo_contratante_iniciodoexpedienteauxdate_Jsonclick = "";
         edtavTfcontratante_iniciodoexpediente_to_Jsonclick = "";
         edtavTfcontratante_iniciodoexpediente_to_Visible = 1;
         edtavTfcontratante_iniciodoexpediente_Jsonclick = "";
         edtavTfcontratante_iniciodoexpediente_Visible = 1;
         edtavTfestado_uf_sel_Jsonclick = "";
         edtavTfestado_uf_sel_Visible = 1;
         edtavTfestado_uf_Jsonclick = "";
         edtavTfestado_uf_Visible = 1;
         edtavTfmunicipio_nome_sel_Jsonclick = "";
         edtavTfmunicipio_nome_sel_Visible = 1;
         edtavTfmunicipio_nome_Jsonclick = "";
         edtavTfmunicipio_nome_Visible = 1;
         edtavTfcontratante_ramal_sel_Jsonclick = "";
         edtavTfcontratante_ramal_sel_Visible = 1;
         edtavTfcontratante_ramal_Jsonclick = "";
         edtavTfcontratante_ramal_Visible = 1;
         edtavTfcontratante_telefone_sel_Jsonclick = "";
         edtavTfcontratante_telefone_sel_Visible = 1;
         edtavTfcontratante_telefone_Jsonclick = "";
         edtavTfcontratante_telefone_Visible = 1;
         edtavTfcontratante_cnpj_sel_Jsonclick = "";
         edtavTfcontratante_cnpj_sel_Visible = 1;
         edtavTfcontratante_cnpj_Jsonclick = "";
         edtavTfcontratante_cnpj_Visible = 1;
         edtavTfcontratante_nomefantasia_sel_Jsonclick = "";
         edtavTfcontratante_nomefantasia_sel_Visible = 1;
         edtavTfcontratante_nomefantasia_Jsonclick = "";
         edtavTfcontratante_nomefantasia_Visible = 1;
         edtavTfcontratante_razaosocial_sel_Jsonclick = "";
         edtavTfcontratante_razaosocial_sel_Visible = 1;
         edtavTfcontratante_razaosocial_Jsonclick = "";
         edtavTfcontratante_razaosocial_Visible = 1;
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         Ddo_contratante_fimdoexpediente_Searchbuttontext = "Pesquisar";
         Ddo_contratante_fimdoexpediente_Rangefilterto = "At�";
         Ddo_contratante_fimdoexpediente_Rangefilterfrom = "Desde";
         Ddo_contratante_fimdoexpediente_Cleanfilter = "Limpar pesquisa";
         Ddo_contratante_fimdoexpediente_Sortdsc = "Ordenar de Z � A";
         Ddo_contratante_fimdoexpediente_Sortasc = "Ordenar de A � Z";
         Ddo_contratante_fimdoexpediente_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratante_fimdoexpediente_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratante_fimdoexpediente_Filtertype = "Date";
         Ddo_contratante_fimdoexpediente_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratante_fimdoexpediente_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratante_fimdoexpediente_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratante_fimdoexpediente_Titlecontrolidtoreplace = "";
         Ddo_contratante_fimdoexpediente_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratante_fimdoexpediente_Cls = "ColumnSettings";
         Ddo_contratante_fimdoexpediente_Tooltip = "Op��es";
         Ddo_contratante_fimdoexpediente_Caption = "";
         Ddo_contratante_iniciodoexpediente_Searchbuttontext = "Pesquisar";
         Ddo_contratante_iniciodoexpediente_Rangefilterto = "At�";
         Ddo_contratante_iniciodoexpediente_Rangefilterfrom = "Desde";
         Ddo_contratante_iniciodoexpediente_Cleanfilter = "Limpar pesquisa";
         Ddo_contratante_iniciodoexpediente_Sortdsc = "Ordenar de Z � A";
         Ddo_contratante_iniciodoexpediente_Sortasc = "Ordenar de A � Z";
         Ddo_contratante_iniciodoexpediente_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratante_iniciodoexpediente_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratante_iniciodoexpediente_Filtertype = "Date";
         Ddo_contratante_iniciodoexpediente_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratante_iniciodoexpediente_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratante_iniciodoexpediente_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratante_iniciodoexpediente_Titlecontrolidtoreplace = "";
         Ddo_contratante_iniciodoexpediente_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratante_iniciodoexpediente_Cls = "ColumnSettings";
         Ddo_contratante_iniciodoexpediente_Tooltip = "Op��es";
         Ddo_contratante_iniciodoexpediente_Caption = "";
         Ddo_estado_uf_Searchbuttontext = "Pesquisar";
         Ddo_estado_uf_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_estado_uf_Cleanfilter = "Limpar pesquisa";
         Ddo_estado_uf_Loadingdata = "Carregando dados...";
         Ddo_estado_uf_Sortdsc = "Ordenar de Z � A";
         Ddo_estado_uf_Sortasc = "Ordenar de A � Z";
         Ddo_estado_uf_Datalistupdateminimumcharacters = 0;
         Ddo_estado_uf_Datalistproc = "GetWWContratanteFilterData";
         Ddo_estado_uf_Datalisttype = "Dynamic";
         Ddo_estado_uf_Includedatalist = Convert.ToBoolean( -1);
         Ddo_estado_uf_Filterisrange = Convert.ToBoolean( 0);
         Ddo_estado_uf_Filtertype = "Character";
         Ddo_estado_uf_Includefilter = Convert.ToBoolean( -1);
         Ddo_estado_uf_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_estado_uf_Includesortasc = Convert.ToBoolean( -1);
         Ddo_estado_uf_Titlecontrolidtoreplace = "";
         Ddo_estado_uf_Dropdownoptionstype = "GridTitleSettings";
         Ddo_estado_uf_Cls = "ColumnSettings";
         Ddo_estado_uf_Tooltip = "Op��es";
         Ddo_estado_uf_Caption = "";
         Ddo_municipio_nome_Searchbuttontext = "Pesquisar";
         Ddo_municipio_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_municipio_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_municipio_nome_Loadingdata = "Carregando dados...";
         Ddo_municipio_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_municipio_nome_Sortasc = "Ordenar de A � Z";
         Ddo_municipio_nome_Datalistupdateminimumcharacters = 0;
         Ddo_municipio_nome_Datalistproc = "GetWWContratanteFilterData";
         Ddo_municipio_nome_Datalisttype = "Dynamic";
         Ddo_municipio_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_municipio_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_municipio_nome_Filtertype = "Character";
         Ddo_municipio_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_municipio_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_municipio_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_municipio_nome_Titlecontrolidtoreplace = "";
         Ddo_municipio_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_municipio_nome_Cls = "ColumnSettings";
         Ddo_municipio_nome_Tooltip = "Op��es";
         Ddo_municipio_nome_Caption = "";
         Ddo_contratante_ramal_Searchbuttontext = "Pesquisar";
         Ddo_contratante_ramal_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratante_ramal_Cleanfilter = "Limpar pesquisa";
         Ddo_contratante_ramal_Loadingdata = "Carregando dados...";
         Ddo_contratante_ramal_Sortdsc = "Ordenar de Z � A";
         Ddo_contratante_ramal_Sortasc = "Ordenar de A � Z";
         Ddo_contratante_ramal_Datalistupdateminimumcharacters = 0;
         Ddo_contratante_ramal_Datalistproc = "GetWWContratanteFilterData";
         Ddo_contratante_ramal_Datalisttype = "Dynamic";
         Ddo_contratante_ramal_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratante_ramal_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratante_ramal_Filtertype = "Character";
         Ddo_contratante_ramal_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratante_ramal_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratante_ramal_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratante_ramal_Titlecontrolidtoreplace = "";
         Ddo_contratante_ramal_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratante_ramal_Cls = "ColumnSettings";
         Ddo_contratante_ramal_Tooltip = "Op��es";
         Ddo_contratante_ramal_Caption = "";
         Ddo_contratante_telefone_Searchbuttontext = "Pesquisar";
         Ddo_contratante_telefone_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratante_telefone_Cleanfilter = "Limpar pesquisa";
         Ddo_contratante_telefone_Loadingdata = "Carregando dados...";
         Ddo_contratante_telefone_Sortdsc = "Ordenar de Z � A";
         Ddo_contratante_telefone_Sortasc = "Ordenar de A � Z";
         Ddo_contratante_telefone_Datalistupdateminimumcharacters = 0;
         Ddo_contratante_telefone_Datalistproc = "GetWWContratanteFilterData";
         Ddo_contratante_telefone_Datalisttype = "Dynamic";
         Ddo_contratante_telefone_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratante_telefone_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratante_telefone_Filtertype = "Character";
         Ddo_contratante_telefone_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratante_telefone_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratante_telefone_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratante_telefone_Titlecontrolidtoreplace = "";
         Ddo_contratante_telefone_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratante_telefone_Cls = "ColumnSettings";
         Ddo_contratante_telefone_Tooltip = "Op��es";
         Ddo_contratante_telefone_Caption = "";
         Ddo_contratante_cnpj_Searchbuttontext = "Pesquisar";
         Ddo_contratante_cnpj_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratante_cnpj_Cleanfilter = "Limpar pesquisa";
         Ddo_contratante_cnpj_Loadingdata = "Carregando dados...";
         Ddo_contratante_cnpj_Sortdsc = "Ordenar de Z � A";
         Ddo_contratante_cnpj_Sortasc = "Ordenar de A � Z";
         Ddo_contratante_cnpj_Datalistupdateminimumcharacters = 0;
         Ddo_contratante_cnpj_Datalistproc = "GetWWContratanteFilterData";
         Ddo_contratante_cnpj_Datalisttype = "Dynamic";
         Ddo_contratante_cnpj_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratante_cnpj_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratante_cnpj_Filtertype = "Character";
         Ddo_contratante_cnpj_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratante_cnpj_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratante_cnpj_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratante_cnpj_Titlecontrolidtoreplace = "";
         Ddo_contratante_cnpj_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratante_cnpj_Cls = "ColumnSettings";
         Ddo_contratante_cnpj_Tooltip = "Op��es";
         Ddo_contratante_cnpj_Caption = "";
         Ddo_contratante_nomefantasia_Searchbuttontext = "Pesquisar";
         Ddo_contratante_nomefantasia_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratante_nomefantasia_Cleanfilter = "Limpar pesquisa";
         Ddo_contratante_nomefantasia_Loadingdata = "Carregando dados...";
         Ddo_contratante_nomefantasia_Sortdsc = "Ordenar de Z � A";
         Ddo_contratante_nomefantasia_Sortasc = "Ordenar de A � Z";
         Ddo_contratante_nomefantasia_Datalistupdateminimumcharacters = 0;
         Ddo_contratante_nomefantasia_Datalistproc = "GetWWContratanteFilterData";
         Ddo_contratante_nomefantasia_Datalisttype = "Dynamic";
         Ddo_contratante_nomefantasia_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratante_nomefantasia_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratante_nomefantasia_Filtertype = "Character";
         Ddo_contratante_nomefantasia_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratante_nomefantasia_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratante_nomefantasia_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratante_nomefantasia_Titlecontrolidtoreplace = "";
         Ddo_contratante_nomefantasia_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratante_nomefantasia_Cls = "ColumnSettings";
         Ddo_contratante_nomefantasia_Tooltip = "Op��es";
         Ddo_contratante_nomefantasia_Caption = "";
         Ddo_contratante_razaosocial_Searchbuttontext = "Pesquisar";
         Ddo_contratante_razaosocial_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratante_razaosocial_Cleanfilter = "Limpar pesquisa";
         Ddo_contratante_razaosocial_Loadingdata = "Carregando dados...";
         Ddo_contratante_razaosocial_Sortdsc = "Ordenar de Z � A";
         Ddo_contratante_razaosocial_Sortasc = "Ordenar de A � Z";
         Ddo_contratante_razaosocial_Datalistupdateminimumcharacters = 0;
         Ddo_contratante_razaosocial_Datalistproc = "GetWWContratanteFilterData";
         Ddo_contratante_razaosocial_Datalisttype = "Dynamic";
         Ddo_contratante_razaosocial_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratante_razaosocial_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratante_razaosocial_Filtertype = "Character";
         Ddo_contratante_razaosocial_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratante_razaosocial_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratante_razaosocial_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratante_razaosocial_Titlecontrolidtoreplace = "";
         Ddo_contratante_razaosocial_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratante_razaosocial_Cls = "ColumnSettings";
         Ddo_contratante_razaosocial_Tooltip = "Op��es";
         Ddo_contratante_razaosocial_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Contratante";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV61ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_NOMEFANTASIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV97ddo_Contratante_RamalTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAMALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV105ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_INICIODOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_FIMDOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV79TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV80TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV83TFContratante_NomeFantasia',fld:'vTFCONTRATANTE_NOMEFANTASIA',pic:'@!',nv:''},{av:'AV84TFContratante_NomeFantasia_Sel',fld:'vTFCONTRATANTE_NOMEFANTASIA_SEL',pic:'@!',nv:''},{av:'AV87TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV88TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV91TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV92TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV95TFContratante_Ramal',fld:'vTFCONTRATANTE_RAMAL',pic:'',nv:''},{av:'AV96TFContratante_Ramal_Sel',fld:'vTFCONTRATANTE_RAMAL_SEL',pic:'',nv:''},{av:'AV99TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV100TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV103TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV104TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV107TFContratante_InicioDoExpediente',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV108TFContratante_InicioDoExpediente_To',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV113TFContratante_FimDoExpediente',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV114TFContratante_FimDoExpediente_To',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false}],oparms:[{av:'AV78Contratante_RazaoSocialTitleFilterData',fld:'vCONTRATANTE_RAZAOSOCIALTITLEFILTERDATA',pic:'',nv:null},{av:'AV82Contratante_NomeFantasiaTitleFilterData',fld:'vCONTRATANTE_NOMEFANTASIATITLEFILTERDATA',pic:'',nv:null},{av:'AV86Contratante_CNPJTitleFilterData',fld:'vCONTRATANTE_CNPJTITLEFILTERDATA',pic:'',nv:null},{av:'AV90Contratante_TelefoneTitleFilterData',fld:'vCONTRATANTE_TELEFONETITLEFILTERDATA',pic:'',nv:null},{av:'AV94Contratante_RamalTitleFilterData',fld:'vCONTRATANTE_RAMALTITLEFILTERDATA',pic:'',nv:null},{av:'AV98Municipio_NomeTitleFilterData',fld:'vMUNICIPIO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV102Estado_UFTitleFilterData',fld:'vESTADO_UFTITLEFILTERDATA',pic:'',nv:null},{av:'AV106Contratante_InicioDoExpedienteTitleFilterData',fld:'vCONTRATANTE_INICIODOEXPEDIENTETITLEFILTERDATA',pic:'',nv:null},{av:'AV112Contratante_FimDoExpedienteTitleFilterData',fld:'vCONTRATANTE_FIMDOEXPEDIENTETITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV61ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'edtContratante_RazaoSocial_Titleformat',ctrl:'CONTRATANTE_RAZAOSOCIAL',prop:'Titleformat'},{av:'edtContratante_RazaoSocial_Title',ctrl:'CONTRATANTE_RAZAOSOCIAL',prop:'Title'},{av:'edtContratante_NomeFantasia_Titleformat',ctrl:'CONTRATANTE_NOMEFANTASIA',prop:'Titleformat'},{av:'edtContratante_NomeFantasia_Title',ctrl:'CONTRATANTE_NOMEFANTASIA',prop:'Title'},{av:'edtContratante_CNPJ_Titleformat',ctrl:'CONTRATANTE_CNPJ',prop:'Titleformat'},{av:'edtContratante_CNPJ_Title',ctrl:'CONTRATANTE_CNPJ',prop:'Title'},{av:'edtContratante_Telefone_Titleformat',ctrl:'CONTRATANTE_TELEFONE',prop:'Titleformat'},{av:'edtContratante_Telefone_Title',ctrl:'CONTRATANTE_TELEFONE',prop:'Title'},{av:'edtContratante_Ramal_Titleformat',ctrl:'CONTRATANTE_RAMAL',prop:'Titleformat'},{av:'edtContratante_Ramal_Title',ctrl:'CONTRATANTE_RAMAL',prop:'Title'},{av:'edtMunicipio_Nome_Titleformat',ctrl:'MUNICIPIO_NOME',prop:'Titleformat'},{av:'edtMunicipio_Nome_Title',ctrl:'MUNICIPIO_NOME',prop:'Title'},{av:'edtEstado_UF_Titleformat',ctrl:'ESTADO_UF',prop:'Titleformat'},{av:'edtEstado_UF_Title',ctrl:'ESTADO_UF',prop:'Title'},{av:'edtContratante_InicioDoExpediente_Titleformat',ctrl:'CONTRATANTE_INICIODOEXPEDIENTE',prop:'Titleformat'},{av:'edtContratante_InicioDoExpediente_Title',ctrl:'CONTRATANTE_INICIODOEXPEDIENTE',prop:'Title'},{av:'edtContratante_FimDoExpediente_Titleformat',ctrl:'CONTRATANTE_FIMDOEXPEDIENTE',prop:'Titleformat'},{av:'edtContratante_FimDoExpediente_Title',ctrl:'CONTRATANTE_FIMDOEXPEDIENTE',prop:'Title'},{av:'AV120GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV121GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'edtavDisplay_Visible',ctrl:'vDISPLAY',prop:'Visible'},{av:'AV65ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E120Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV79TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV80TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV83TFContratante_NomeFantasia',fld:'vTFCONTRATANTE_NOMEFANTASIA',pic:'@!',nv:''},{av:'AV84TFContratante_NomeFantasia_Sel',fld:'vTFCONTRATANTE_NOMEFANTASIA_SEL',pic:'@!',nv:''},{av:'AV87TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV88TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV91TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV92TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV95TFContratante_Ramal',fld:'vTFCONTRATANTE_RAMAL',pic:'',nv:''},{av:'AV96TFContratante_Ramal_Sel',fld:'vTFCONTRATANTE_RAMAL_SEL',pic:'',nv:''},{av:'AV99TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV100TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV103TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV104TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV107TFContratante_InicioDoExpediente',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV108TFContratante_InicioDoExpediente_To',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV113TFContratante_FimDoExpediente',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV114TFContratante_FimDoExpediente_To',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV61ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_NOMEFANTASIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV97ddo_Contratante_RamalTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAMALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV105ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_INICIODOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_FIMDOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATANTE_RAZAOSOCIAL.ONOPTIONCLICKED","{handler:'E130Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV79TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV80TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV83TFContratante_NomeFantasia',fld:'vTFCONTRATANTE_NOMEFANTASIA',pic:'@!',nv:''},{av:'AV84TFContratante_NomeFantasia_Sel',fld:'vTFCONTRATANTE_NOMEFANTASIA_SEL',pic:'@!',nv:''},{av:'AV87TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV88TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV91TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV92TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV95TFContratante_Ramal',fld:'vTFCONTRATANTE_RAMAL',pic:'',nv:''},{av:'AV96TFContratante_Ramal_Sel',fld:'vTFCONTRATANTE_RAMAL_SEL',pic:'',nv:''},{av:'AV99TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV100TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV103TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV104TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV107TFContratante_InicioDoExpediente',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV108TFContratante_InicioDoExpediente_To',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV113TFContratante_FimDoExpediente',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV114TFContratante_FimDoExpediente_To',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV61ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_NOMEFANTASIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV97ddo_Contratante_RamalTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAMALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV105ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_INICIODOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_FIMDOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratante_razaosocial_Activeeventkey',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'ActiveEventKey'},{av:'Ddo_contratante_razaosocial_Filteredtext_get',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'FilteredText_get'},{av:'Ddo_contratante_razaosocial_Selectedvalue_get',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'AV79TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV80TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'Ddo_contratante_nomefantasia_Sortedstatus',ctrl:'DDO_CONTRATANTE_NOMEFANTASIA',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_contratante_ramal_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAMAL',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_contratante_iniciodoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_INICIODOEXPEDIENTE',prop:'SortedStatus'},{av:'Ddo_contratante_fimdoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_FIMDOEXPEDIENTE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATANTE_NOMEFANTASIA.ONOPTIONCLICKED","{handler:'E140Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV79TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV80TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV83TFContratante_NomeFantasia',fld:'vTFCONTRATANTE_NOMEFANTASIA',pic:'@!',nv:''},{av:'AV84TFContratante_NomeFantasia_Sel',fld:'vTFCONTRATANTE_NOMEFANTASIA_SEL',pic:'@!',nv:''},{av:'AV87TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV88TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV91TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV92TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV95TFContratante_Ramal',fld:'vTFCONTRATANTE_RAMAL',pic:'',nv:''},{av:'AV96TFContratante_Ramal_Sel',fld:'vTFCONTRATANTE_RAMAL_SEL',pic:'',nv:''},{av:'AV99TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV100TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV103TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV104TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV107TFContratante_InicioDoExpediente',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV108TFContratante_InicioDoExpediente_To',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV113TFContratante_FimDoExpediente',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV114TFContratante_FimDoExpediente_To',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV61ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_NOMEFANTASIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV97ddo_Contratante_RamalTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAMALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV105ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_INICIODOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_FIMDOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratante_nomefantasia_Activeeventkey',ctrl:'DDO_CONTRATANTE_NOMEFANTASIA',prop:'ActiveEventKey'},{av:'Ddo_contratante_nomefantasia_Filteredtext_get',ctrl:'DDO_CONTRATANTE_NOMEFANTASIA',prop:'FilteredText_get'},{av:'Ddo_contratante_nomefantasia_Selectedvalue_get',ctrl:'DDO_CONTRATANTE_NOMEFANTASIA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratante_nomefantasia_Sortedstatus',ctrl:'DDO_CONTRATANTE_NOMEFANTASIA',prop:'SortedStatus'},{av:'AV83TFContratante_NomeFantasia',fld:'vTFCONTRATANTE_NOMEFANTASIA',pic:'@!',nv:''},{av:'AV84TFContratante_NomeFantasia_Sel',fld:'vTFCONTRATANTE_NOMEFANTASIA_SEL',pic:'@!',nv:''},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_contratante_ramal_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAMAL',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_contratante_iniciodoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_INICIODOEXPEDIENTE',prop:'SortedStatus'},{av:'Ddo_contratante_fimdoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_FIMDOEXPEDIENTE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATANTE_CNPJ.ONOPTIONCLICKED","{handler:'E150Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV79TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV80TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV83TFContratante_NomeFantasia',fld:'vTFCONTRATANTE_NOMEFANTASIA',pic:'@!',nv:''},{av:'AV84TFContratante_NomeFantasia_Sel',fld:'vTFCONTRATANTE_NOMEFANTASIA_SEL',pic:'@!',nv:''},{av:'AV87TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV88TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV91TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV92TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV95TFContratante_Ramal',fld:'vTFCONTRATANTE_RAMAL',pic:'',nv:''},{av:'AV96TFContratante_Ramal_Sel',fld:'vTFCONTRATANTE_RAMAL_SEL',pic:'',nv:''},{av:'AV99TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV100TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV103TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV104TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV107TFContratante_InicioDoExpediente',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV108TFContratante_InicioDoExpediente_To',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV113TFContratante_FimDoExpediente',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV114TFContratante_FimDoExpediente_To',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV61ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_NOMEFANTASIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV97ddo_Contratante_RamalTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAMALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV105ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_INICIODOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_FIMDOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratante_cnpj_Activeeventkey',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'ActiveEventKey'},{av:'Ddo_contratante_cnpj_Filteredtext_get',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'FilteredText_get'},{av:'Ddo_contratante_cnpj_Selectedvalue_get',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'AV87TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV88TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'Ddo_contratante_nomefantasia_Sortedstatus',ctrl:'DDO_CONTRATANTE_NOMEFANTASIA',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_contratante_ramal_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAMAL',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_contratante_iniciodoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_INICIODOEXPEDIENTE',prop:'SortedStatus'},{av:'Ddo_contratante_fimdoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_FIMDOEXPEDIENTE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATANTE_TELEFONE.ONOPTIONCLICKED","{handler:'E160Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV79TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV80TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV83TFContratante_NomeFantasia',fld:'vTFCONTRATANTE_NOMEFANTASIA',pic:'@!',nv:''},{av:'AV84TFContratante_NomeFantasia_Sel',fld:'vTFCONTRATANTE_NOMEFANTASIA_SEL',pic:'@!',nv:''},{av:'AV87TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV88TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV91TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV92TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV95TFContratante_Ramal',fld:'vTFCONTRATANTE_RAMAL',pic:'',nv:''},{av:'AV96TFContratante_Ramal_Sel',fld:'vTFCONTRATANTE_RAMAL_SEL',pic:'',nv:''},{av:'AV99TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV100TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV103TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV104TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV107TFContratante_InicioDoExpediente',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV108TFContratante_InicioDoExpediente_To',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV113TFContratante_FimDoExpediente',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV114TFContratante_FimDoExpediente_To',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV61ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_NOMEFANTASIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV97ddo_Contratante_RamalTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAMALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV105ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_INICIODOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_FIMDOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratante_telefone_Activeeventkey',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'ActiveEventKey'},{av:'Ddo_contratante_telefone_Filteredtext_get',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'FilteredText_get'},{av:'Ddo_contratante_telefone_Selectedvalue_get',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'AV91TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV92TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'Ddo_contratante_nomefantasia_Sortedstatus',ctrl:'DDO_CONTRATANTE_NOMEFANTASIA',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_contratante_ramal_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAMAL',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_contratante_iniciodoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_INICIODOEXPEDIENTE',prop:'SortedStatus'},{av:'Ddo_contratante_fimdoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_FIMDOEXPEDIENTE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATANTE_RAMAL.ONOPTIONCLICKED","{handler:'E170Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV79TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV80TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV83TFContratante_NomeFantasia',fld:'vTFCONTRATANTE_NOMEFANTASIA',pic:'@!',nv:''},{av:'AV84TFContratante_NomeFantasia_Sel',fld:'vTFCONTRATANTE_NOMEFANTASIA_SEL',pic:'@!',nv:''},{av:'AV87TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV88TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV91TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV92TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV95TFContratante_Ramal',fld:'vTFCONTRATANTE_RAMAL',pic:'',nv:''},{av:'AV96TFContratante_Ramal_Sel',fld:'vTFCONTRATANTE_RAMAL_SEL',pic:'',nv:''},{av:'AV99TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV100TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV103TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV104TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV107TFContratante_InicioDoExpediente',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV108TFContratante_InicioDoExpediente_To',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV113TFContratante_FimDoExpediente',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV114TFContratante_FimDoExpediente_To',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV61ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_NOMEFANTASIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV97ddo_Contratante_RamalTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAMALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV105ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_INICIODOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_FIMDOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratante_ramal_Activeeventkey',ctrl:'DDO_CONTRATANTE_RAMAL',prop:'ActiveEventKey'},{av:'Ddo_contratante_ramal_Filteredtext_get',ctrl:'DDO_CONTRATANTE_RAMAL',prop:'FilteredText_get'},{av:'Ddo_contratante_ramal_Selectedvalue_get',ctrl:'DDO_CONTRATANTE_RAMAL',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratante_ramal_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAMAL',prop:'SortedStatus'},{av:'AV95TFContratante_Ramal',fld:'vTFCONTRATANTE_RAMAL',pic:'',nv:''},{av:'AV96TFContratante_Ramal_Sel',fld:'vTFCONTRATANTE_RAMAL_SEL',pic:'',nv:''},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'Ddo_contratante_nomefantasia_Sortedstatus',ctrl:'DDO_CONTRATANTE_NOMEFANTASIA',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_contratante_iniciodoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_INICIODOEXPEDIENTE',prop:'SortedStatus'},{av:'Ddo_contratante_fimdoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_FIMDOEXPEDIENTE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_MUNICIPIO_NOME.ONOPTIONCLICKED","{handler:'E180Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV79TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV80TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV83TFContratante_NomeFantasia',fld:'vTFCONTRATANTE_NOMEFANTASIA',pic:'@!',nv:''},{av:'AV84TFContratante_NomeFantasia_Sel',fld:'vTFCONTRATANTE_NOMEFANTASIA_SEL',pic:'@!',nv:''},{av:'AV87TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV88TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV91TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV92TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV95TFContratante_Ramal',fld:'vTFCONTRATANTE_RAMAL',pic:'',nv:''},{av:'AV96TFContratante_Ramal_Sel',fld:'vTFCONTRATANTE_RAMAL_SEL',pic:'',nv:''},{av:'AV99TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV100TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV103TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV104TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV107TFContratante_InicioDoExpediente',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV108TFContratante_InicioDoExpediente_To',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV113TFContratante_FimDoExpediente',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV114TFContratante_FimDoExpediente_To',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV61ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_NOMEFANTASIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV97ddo_Contratante_RamalTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAMALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV105ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_INICIODOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_FIMDOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_municipio_nome_Activeeventkey',ctrl:'DDO_MUNICIPIO_NOME',prop:'ActiveEventKey'},{av:'Ddo_municipio_nome_Filteredtext_get',ctrl:'DDO_MUNICIPIO_NOME',prop:'FilteredText_get'},{av:'Ddo_municipio_nome_Selectedvalue_get',ctrl:'DDO_MUNICIPIO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'AV99TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV100TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'Ddo_contratante_nomefantasia_Sortedstatus',ctrl:'DDO_CONTRATANTE_NOMEFANTASIA',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_contratante_ramal_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAMAL',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_contratante_iniciodoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_INICIODOEXPEDIENTE',prop:'SortedStatus'},{av:'Ddo_contratante_fimdoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_FIMDOEXPEDIENTE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ESTADO_UF.ONOPTIONCLICKED","{handler:'E190Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV79TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV80TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV83TFContratante_NomeFantasia',fld:'vTFCONTRATANTE_NOMEFANTASIA',pic:'@!',nv:''},{av:'AV84TFContratante_NomeFantasia_Sel',fld:'vTFCONTRATANTE_NOMEFANTASIA_SEL',pic:'@!',nv:''},{av:'AV87TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV88TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV91TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV92TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV95TFContratante_Ramal',fld:'vTFCONTRATANTE_RAMAL',pic:'',nv:''},{av:'AV96TFContratante_Ramal_Sel',fld:'vTFCONTRATANTE_RAMAL_SEL',pic:'',nv:''},{av:'AV99TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV100TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV103TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV104TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV107TFContratante_InicioDoExpediente',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV108TFContratante_InicioDoExpediente_To',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV113TFContratante_FimDoExpediente',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV114TFContratante_FimDoExpediente_To',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV61ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_NOMEFANTASIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV97ddo_Contratante_RamalTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAMALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV105ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_INICIODOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_FIMDOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_estado_uf_Activeeventkey',ctrl:'DDO_ESTADO_UF',prop:'ActiveEventKey'},{av:'Ddo_estado_uf_Filteredtext_get',ctrl:'DDO_ESTADO_UF',prop:'FilteredText_get'},{av:'Ddo_estado_uf_Selectedvalue_get',ctrl:'DDO_ESTADO_UF',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'AV103TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV104TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'Ddo_contratante_nomefantasia_Sortedstatus',ctrl:'DDO_CONTRATANTE_NOMEFANTASIA',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_contratante_ramal_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAMAL',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_contratante_iniciodoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_INICIODOEXPEDIENTE',prop:'SortedStatus'},{av:'Ddo_contratante_fimdoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_FIMDOEXPEDIENTE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATANTE_INICIODOEXPEDIENTE.ONOPTIONCLICKED","{handler:'E200Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV79TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV80TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV83TFContratante_NomeFantasia',fld:'vTFCONTRATANTE_NOMEFANTASIA',pic:'@!',nv:''},{av:'AV84TFContratante_NomeFantasia_Sel',fld:'vTFCONTRATANTE_NOMEFANTASIA_SEL',pic:'@!',nv:''},{av:'AV87TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV88TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV91TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV92TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV95TFContratante_Ramal',fld:'vTFCONTRATANTE_RAMAL',pic:'',nv:''},{av:'AV96TFContratante_Ramal_Sel',fld:'vTFCONTRATANTE_RAMAL_SEL',pic:'',nv:''},{av:'AV99TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV100TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV103TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV104TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV107TFContratante_InicioDoExpediente',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV108TFContratante_InicioDoExpediente_To',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV113TFContratante_FimDoExpediente',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV114TFContratante_FimDoExpediente_To',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV61ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_NOMEFANTASIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV97ddo_Contratante_RamalTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAMALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV105ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_INICIODOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_FIMDOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratante_iniciodoexpediente_Activeeventkey',ctrl:'DDO_CONTRATANTE_INICIODOEXPEDIENTE',prop:'ActiveEventKey'},{av:'Ddo_contratante_iniciodoexpediente_Filteredtext_get',ctrl:'DDO_CONTRATANTE_INICIODOEXPEDIENTE',prop:'FilteredText_get'},{av:'Ddo_contratante_iniciodoexpediente_Filteredtextto_get',ctrl:'DDO_CONTRATANTE_INICIODOEXPEDIENTE',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratante_iniciodoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_INICIODOEXPEDIENTE',prop:'SortedStatus'},{av:'AV107TFContratante_InicioDoExpediente',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV108TFContratante_InicioDoExpediente_To',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'Ddo_contratante_nomefantasia_Sortedstatus',ctrl:'DDO_CONTRATANTE_NOMEFANTASIA',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_contratante_ramal_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAMAL',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_contratante_fimdoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_FIMDOEXPEDIENTE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATANTE_FIMDOEXPEDIENTE.ONOPTIONCLICKED","{handler:'E210Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV79TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV80TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV83TFContratante_NomeFantasia',fld:'vTFCONTRATANTE_NOMEFANTASIA',pic:'@!',nv:''},{av:'AV84TFContratante_NomeFantasia_Sel',fld:'vTFCONTRATANTE_NOMEFANTASIA_SEL',pic:'@!',nv:''},{av:'AV87TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV88TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV91TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV92TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV95TFContratante_Ramal',fld:'vTFCONTRATANTE_RAMAL',pic:'',nv:''},{av:'AV96TFContratante_Ramal_Sel',fld:'vTFCONTRATANTE_RAMAL_SEL',pic:'',nv:''},{av:'AV99TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV100TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV103TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV104TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV107TFContratante_InicioDoExpediente',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV108TFContratante_InicioDoExpediente_To',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV113TFContratante_FimDoExpediente',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV114TFContratante_FimDoExpediente_To',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV61ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_NOMEFANTASIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV97ddo_Contratante_RamalTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAMALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV105ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_INICIODOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_FIMDOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratante_fimdoexpediente_Activeeventkey',ctrl:'DDO_CONTRATANTE_FIMDOEXPEDIENTE',prop:'ActiveEventKey'},{av:'Ddo_contratante_fimdoexpediente_Filteredtext_get',ctrl:'DDO_CONTRATANTE_FIMDOEXPEDIENTE',prop:'FilteredText_get'},{av:'Ddo_contratante_fimdoexpediente_Filteredtextto_get',ctrl:'DDO_CONTRATANTE_FIMDOEXPEDIENTE',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratante_fimdoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_FIMDOEXPEDIENTE',prop:'SortedStatus'},{av:'AV113TFContratante_FimDoExpediente',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV114TFContratante_FimDoExpediente_To',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'Ddo_contratante_nomefantasia_Sortedstatus',ctrl:'DDO_CONTRATANTE_NOMEFANTASIA',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_contratante_ramal_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAMAL',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_contratante_iniciodoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_INICIODOEXPEDIENTE',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E240Y2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV31Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV32Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV68Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'edtContratante_RazaoSocial_Link',ctrl:'CONTRATANTE_RAZAOSOCIAL',prop:'Link'}]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E110Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV79TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV80TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV83TFContratante_NomeFantasia',fld:'vTFCONTRATANTE_NOMEFANTASIA',pic:'@!',nv:''},{av:'AV84TFContratante_NomeFantasia_Sel',fld:'vTFCONTRATANTE_NOMEFANTASIA_SEL',pic:'@!',nv:''},{av:'AV87TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV88TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV91TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV92TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV95TFContratante_Ramal',fld:'vTFCONTRATANTE_RAMAL',pic:'',nv:''},{av:'AV96TFContratante_Ramal_Sel',fld:'vTFCONTRATANTE_RAMAL_SEL',pic:'',nv:''},{av:'AV99TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV100TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV103TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV104TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV107TFContratante_InicioDoExpediente',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV108TFContratante_InicioDoExpediente_To',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV113TFContratante_FimDoExpediente',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE',pic:'99:99',nv:''},{av:'AV114TFContratante_FimDoExpediente_To',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'AV61ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_NOMEFANTASIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV97ddo_Contratante_RamalTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAMALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV101ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV105ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_INICIODOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_FIMDOEXPEDIENTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV165Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}],oparms:[{av:'AV61ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV39AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV79TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'Ddo_contratante_razaosocial_Filteredtext_set',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'FilteredText_set'},{av:'AV80TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'Ddo_contratante_razaosocial_Selectedvalue_set',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SelectedValue_set'},{av:'AV83TFContratante_NomeFantasia',fld:'vTFCONTRATANTE_NOMEFANTASIA',pic:'@!',nv:''},{av:'Ddo_contratante_nomefantasia_Filteredtext_set',ctrl:'DDO_CONTRATANTE_NOMEFANTASIA',prop:'FilteredText_set'},{av:'AV84TFContratante_NomeFantasia_Sel',fld:'vTFCONTRATANTE_NOMEFANTASIA_SEL',pic:'@!',nv:''},{av:'Ddo_contratante_nomefantasia_Selectedvalue_set',ctrl:'DDO_CONTRATANTE_NOMEFANTASIA',prop:'SelectedValue_set'},{av:'AV87TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'Ddo_contratante_cnpj_Filteredtext_set',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'FilteredText_set'},{av:'AV88TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'Ddo_contratante_cnpj_Selectedvalue_set',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SelectedValue_set'},{av:'AV91TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'Ddo_contratante_telefone_Filteredtext_set',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'FilteredText_set'},{av:'AV92TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'Ddo_contratante_telefone_Selectedvalue_set',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SelectedValue_set'},{av:'AV95TFContratante_Ramal',fld:'vTFCONTRATANTE_RAMAL',pic:'',nv:''},{av:'Ddo_contratante_ramal_Filteredtext_set',ctrl:'DDO_CONTRATANTE_RAMAL',prop:'FilteredText_set'},{av:'AV96TFContratante_Ramal_Sel',fld:'vTFCONTRATANTE_RAMAL_SEL',pic:'',nv:''},{av:'Ddo_contratante_ramal_Selectedvalue_set',ctrl:'DDO_CONTRATANTE_RAMAL',prop:'SelectedValue_set'},{av:'AV99TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'Ddo_municipio_nome_Filteredtext_set',ctrl:'DDO_MUNICIPIO_NOME',prop:'FilteredText_set'},{av:'AV100TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_municipio_nome_Selectedvalue_set',ctrl:'DDO_MUNICIPIO_NOME',prop:'SelectedValue_set'},{av:'AV103TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'Ddo_estado_uf_Filteredtext_set',ctrl:'DDO_ESTADO_UF',prop:'FilteredText_set'},{av:'AV104TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'Ddo_estado_uf_Selectedvalue_set',ctrl:'DDO_ESTADO_UF',prop:'SelectedValue_set'},{av:'AV107TFContratante_InicioDoExpediente',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE',pic:'99:99',nv:''},{av:'Ddo_contratante_iniciodoexpediente_Filteredtext_set',ctrl:'DDO_CONTRATANTE_INICIODOEXPEDIENTE',prop:'FilteredText_set'},{av:'AV108TFContratante_InicioDoExpediente_To',fld:'vTFCONTRATANTE_INICIODOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'Ddo_contratante_iniciodoexpediente_Filteredtextto_set',ctrl:'DDO_CONTRATANTE_INICIODOEXPEDIENTE',prop:'FilteredTextTo_set'},{av:'AV113TFContratante_FimDoExpediente',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE',pic:'99:99',nv:''},{av:'Ddo_contratante_fimdoexpediente_Filteredtext_set',ctrl:'DDO_CONTRATANTE_FIMDOEXPEDIENTE',prop:'FilteredText_set'},{av:'AV114TFContratante_FimDoExpediente_To',fld:'vTFCONTRATANTE_FIMDOEXPEDIENTE_TO',pic:'99:99',nv:''},{av:'Ddo_contratante_fimdoexpediente_Filteredtextto_set',ctrl:'DDO_CONTRATANTE_FIMDOEXPEDIENTE',prop:'FilteredTextTo_set'},{av:'Ddo_contratante_fimdoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_FIMDOEXPEDIENTE',prop:'SortedStatus'},{av:'Ddo_contratante_iniciodoexpediente_Sortedstatus',ctrl:'DDO_CONTRATANTE_INICIODOEXPEDIENTE',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_contratante_ramal_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAMAL',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_contratante_nomefantasia_Sortedstatus',ctrl:'DDO_CONTRATANTE_NOMEFANTASIA',prop:'SortedStatus'},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'AV115DDO_Contratante_FimDoExpedienteAuxDate',fld:'vDDO_CONTRATANTE_FIMDOEXPEDIENTEAUXDATE',pic:'',nv:''},{av:'AV116DDO_Contratante_FimDoExpedienteAuxDateTo',fld:'vDDO_CONTRATANTE_FIMDOEXPEDIENTEAUXDATETO',pic:'',nv:''},{av:'AV109DDO_Contratante_InicioDoExpedienteAuxDate',fld:'vDDO_CONTRATANTE_INICIODOEXPEDIENTEAUXDATE',pic:'',nv:''},{av:'AV110DDO_Contratante_InicioDoExpedienteAuxDateTo',fld:'vDDO_CONTRATANTE_INICIODOEXPEDIENTEAUXDATETO',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratante_razaosocial_Activeeventkey = "";
         Ddo_contratante_razaosocial_Filteredtext_get = "";
         Ddo_contratante_razaosocial_Selectedvalue_get = "";
         Ddo_contratante_nomefantasia_Activeeventkey = "";
         Ddo_contratante_nomefantasia_Filteredtext_get = "";
         Ddo_contratante_nomefantasia_Selectedvalue_get = "";
         Ddo_contratante_cnpj_Activeeventkey = "";
         Ddo_contratante_cnpj_Filteredtext_get = "";
         Ddo_contratante_cnpj_Selectedvalue_get = "";
         Ddo_contratante_telefone_Activeeventkey = "";
         Ddo_contratante_telefone_Filteredtext_get = "";
         Ddo_contratante_telefone_Selectedvalue_get = "";
         Ddo_contratante_ramal_Activeeventkey = "";
         Ddo_contratante_ramal_Filteredtext_get = "";
         Ddo_contratante_ramal_Selectedvalue_get = "";
         Ddo_municipio_nome_Activeeventkey = "";
         Ddo_municipio_nome_Filteredtext_get = "";
         Ddo_municipio_nome_Selectedvalue_get = "";
         Ddo_estado_uf_Activeeventkey = "";
         Ddo_estado_uf_Filteredtext_get = "";
         Ddo_estado_uf_Selectedvalue_get = "";
         Ddo_contratante_iniciodoexpediente_Activeeventkey = "";
         Ddo_contratante_iniciodoexpediente_Filteredtext_get = "";
         Ddo_contratante_iniciodoexpediente_Filteredtextto_get = "";
         Ddo_contratante_fimdoexpediente_Activeeventkey = "";
         Ddo_contratante_fimdoexpediente_Filteredtext_get = "";
         Ddo_contratante_fimdoexpediente_Filteredtextto_get = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV79TFContratante_RazaoSocial = "";
         AV80TFContratante_RazaoSocial_Sel = "";
         AV83TFContratante_NomeFantasia = "";
         AV84TFContratante_NomeFantasia_Sel = "";
         AV87TFContratante_CNPJ = "";
         AV88TFContratante_CNPJ_Sel = "";
         AV91TFContratante_Telefone = "";
         AV92TFContratante_Telefone_Sel = "";
         AV95TFContratante_Ramal = "";
         AV96TFContratante_Ramal_Sel = "";
         AV99TFMunicipio_Nome = "";
         AV100TFMunicipio_Nome_Sel = "";
         AV103TFEstado_UF = "";
         AV104TFEstado_UF_Sel = "";
         AV107TFContratante_InicioDoExpediente = (DateTime)(DateTime.MinValue);
         AV108TFContratante_InicioDoExpediente_To = (DateTime)(DateTime.MinValue);
         AV113TFContratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         AV114TFContratante_FimDoExpediente_To = (DateTime)(DateTime.MinValue);
         AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace = "";
         AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace = "";
         AV89ddo_Contratante_CNPJTitleControlIdToReplace = "";
         AV93ddo_Contratante_TelefoneTitleControlIdToReplace = "";
         AV97ddo_Contratante_RamalTitleControlIdToReplace = "";
         AV101ddo_Municipio_NomeTitleControlIdToReplace = "";
         AV105ddo_Estado_UFTitleControlIdToReplace = "";
         AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace = "";
         AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace = "";
         AV165Pgmname = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV65ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV118DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV78Contratante_RazaoSocialTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV82Contratante_NomeFantasiaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV86Contratante_CNPJTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV90Contratante_TelefoneTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV94Contratante_RamalTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV98Municipio_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV102Estado_UFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV106Contratante_InicioDoExpedienteTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV112Contratante_FimDoExpedienteTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         Ddo_contratante_razaosocial_Filteredtext_set = "";
         Ddo_contratante_razaosocial_Selectedvalue_set = "";
         Ddo_contratante_razaosocial_Sortedstatus = "";
         Ddo_contratante_nomefantasia_Filteredtext_set = "";
         Ddo_contratante_nomefantasia_Selectedvalue_set = "";
         Ddo_contratante_nomefantasia_Sortedstatus = "";
         Ddo_contratante_cnpj_Filteredtext_set = "";
         Ddo_contratante_cnpj_Selectedvalue_set = "";
         Ddo_contratante_cnpj_Sortedstatus = "";
         Ddo_contratante_telefone_Filteredtext_set = "";
         Ddo_contratante_telefone_Selectedvalue_set = "";
         Ddo_contratante_telefone_Sortedstatus = "";
         Ddo_contratante_ramal_Filteredtext_set = "";
         Ddo_contratante_ramal_Selectedvalue_set = "";
         Ddo_contratante_ramal_Sortedstatus = "";
         Ddo_municipio_nome_Filteredtext_set = "";
         Ddo_municipio_nome_Selectedvalue_set = "";
         Ddo_municipio_nome_Sortedstatus = "";
         Ddo_estado_uf_Filteredtext_set = "";
         Ddo_estado_uf_Selectedvalue_set = "";
         Ddo_estado_uf_Sortedstatus = "";
         Ddo_contratante_iniciodoexpediente_Filteredtext_set = "";
         Ddo_contratante_iniciodoexpediente_Filteredtextto_set = "";
         Ddo_contratante_iniciodoexpediente_Sortedstatus = "";
         Ddo_contratante_fimdoexpediente_Filteredtext_set = "";
         Ddo_contratante_fimdoexpediente_Filteredtextto_set = "";
         Ddo_contratante_fimdoexpediente_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         AV109DDO_Contratante_InicioDoExpedienteAuxDate = DateTime.MinValue;
         AV110DDO_Contratante_InicioDoExpedienteAuxDateTo = DateTime.MinValue;
         AV115DDO_Contratante_FimDoExpedienteAuxDate = DateTime.MinValue;
         AV116DDO_Contratante_FimDoExpedienteAuxDateTo = DateTime.MinValue;
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Update = "";
         AV162Update_GXI = "";
         AV32Delete = "";
         AV163Delete_GXI = "";
         AV68Display = "";
         AV164Display_GXI = "";
         A9Contratante_RazaoSocial = "";
         A10Contratante_NomeFantasia = "";
         A12Contratante_CNPJ = "";
         A31Contratante_Telefone = "";
         A32Contratante_Ramal = "";
         A26Municipio_Nome = "";
         A23Estado_UF = "";
         A1448Contratante_InicioDoExpediente = (DateTime)(DateTime.MinValue);
         A1192Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV144WWContratanteDS_2_Tfcontratante_razaosocial = "";
         lV146WWContratanteDS_4_Tfcontratante_nomefantasia = "";
         lV148WWContratanteDS_6_Tfcontratante_cnpj = "";
         lV150WWContratanteDS_8_Tfcontratante_telefone = "";
         lV152WWContratanteDS_10_Tfcontratante_ramal = "";
         lV154WWContratanteDS_12_Tfmunicipio_nome = "";
         lV156WWContratanteDS_14_Tfestado_uf = "";
         AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel = "";
         AV144WWContratanteDS_2_Tfcontratante_razaosocial = "";
         AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel = "";
         AV146WWContratanteDS_4_Tfcontratante_nomefantasia = "";
         AV149WWContratanteDS_7_Tfcontratante_cnpj_sel = "";
         AV148WWContratanteDS_6_Tfcontratante_cnpj = "";
         AV151WWContratanteDS_9_Tfcontratante_telefone_sel = "";
         AV150WWContratanteDS_8_Tfcontratante_telefone = "";
         AV153WWContratanteDS_11_Tfcontratante_ramal_sel = "";
         AV152WWContratanteDS_10_Tfcontratante_ramal = "";
         AV155WWContratanteDS_13_Tfmunicipio_nome_sel = "";
         AV154WWContratanteDS_12_Tfmunicipio_nome = "";
         AV157WWContratanteDS_15_Tfestado_uf_sel = "";
         AV156WWContratanteDS_14_Tfestado_uf = "";
         AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente = (DateTime)(DateTime.MinValue);
         AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to = (DateTime)(DateTime.MinValue);
         AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente = (DateTime)(DateTime.MinValue);
         AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to = (DateTime)(DateTime.MinValue);
         H000Y2_A25Municipio_Codigo = new int[1] ;
         H000Y2_n25Municipio_Codigo = new bool[] {false} ;
         H000Y2_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         H000Y2_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         H000Y2_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         H000Y2_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         H000Y2_A23Estado_UF = new String[] {""} ;
         H000Y2_A26Municipio_Nome = new String[] {""} ;
         H000Y2_A32Contratante_Ramal = new String[] {""} ;
         H000Y2_n32Contratante_Ramal = new bool[] {false} ;
         H000Y2_A31Contratante_Telefone = new String[] {""} ;
         H000Y2_A12Contratante_CNPJ = new String[] {""} ;
         H000Y2_n12Contratante_CNPJ = new bool[] {false} ;
         H000Y2_A10Contratante_NomeFantasia = new String[] {""} ;
         H000Y2_A9Contratante_RazaoSocial = new String[] {""} ;
         H000Y2_n9Contratante_RazaoSocial = new bool[] {false} ;
         H000Y2_A335Contratante_PessoaCod = new int[1] ;
         H000Y2_A29Contratante_Codigo = new int[1] ;
         H000Y2_n29Contratante_Codigo = new bool[] {false} ;
         H000Y2_A5AreaTrabalho_Codigo = new int[1] ;
         H000Y3_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV62ManageFiltersXml = "";
         GXt_char2 = "";
         AV66ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV63ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV64ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV35Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblFiltertextareatrabalho_codigo_Jsonclick = "";
         lblContratantetitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         gxphoneLink = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontratante__default(),
            new Object[][] {
                new Object[] {
               H000Y2_A25Municipio_Codigo, H000Y2_n25Municipio_Codigo, H000Y2_A1192Contratante_FimDoExpediente, H000Y2_n1192Contratante_FimDoExpediente, H000Y2_A1448Contratante_InicioDoExpediente, H000Y2_n1448Contratante_InicioDoExpediente, H000Y2_A23Estado_UF, H000Y2_A26Municipio_Nome, H000Y2_A32Contratante_Ramal, H000Y2_n32Contratante_Ramal,
               H000Y2_A31Contratante_Telefone, H000Y2_A12Contratante_CNPJ, H000Y2_n12Contratante_CNPJ, H000Y2_A10Contratante_NomeFantasia, H000Y2_A9Contratante_RazaoSocial, H000Y2_n9Contratante_RazaoSocial, H000Y2_A335Contratante_PessoaCod, H000Y2_A29Contratante_Codigo, H000Y2_n29Contratante_Codigo, H000Y2_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               H000Y3_AGRID_nRecordCount
               }
            }
         );
         AV165Pgmname = "WWContratante";
         /* GeneXus formulas. */
         AV165Pgmname = "WWContratante";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_32 ;
      private short nGXsfl_32_idx=1 ;
      private short AV13OrderedBy ;
      private short AV61ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_32_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratante_RazaoSocial_Titleformat ;
      private short edtContratante_NomeFantasia_Titleformat ;
      private short edtContratante_CNPJ_Titleformat ;
      private short edtContratante_Telefone_Titleformat ;
      private short edtContratante_Ramal_Titleformat ;
      private short edtMunicipio_Nome_Titleformat ;
      private short edtEstado_UF_Titleformat ;
      private short edtContratante_InicioDoExpediente_Titleformat ;
      private short edtContratante_FimDoExpediente_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV39AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contratante_razaosocial_Datalistupdateminimumcharacters ;
      private int Ddo_contratante_nomefantasia_Datalistupdateminimumcharacters ;
      private int Ddo_contratante_cnpj_Datalistupdateminimumcharacters ;
      private int Ddo_contratante_telefone_Datalistupdateminimumcharacters ;
      private int Ddo_contratante_ramal_Datalistupdateminimumcharacters ;
      private int Ddo_municipio_nome_Datalistupdateminimumcharacters ;
      private int Ddo_estado_uf_Datalistupdateminimumcharacters ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int edtavTfcontratante_razaosocial_Visible ;
      private int edtavTfcontratante_razaosocial_sel_Visible ;
      private int edtavTfcontratante_nomefantasia_Visible ;
      private int edtavTfcontratante_nomefantasia_sel_Visible ;
      private int edtavTfcontratante_cnpj_Visible ;
      private int edtavTfcontratante_cnpj_sel_Visible ;
      private int edtavTfcontratante_telefone_Visible ;
      private int edtavTfcontratante_telefone_sel_Visible ;
      private int edtavTfcontratante_ramal_Visible ;
      private int edtavTfcontratante_ramal_sel_Visible ;
      private int edtavTfmunicipio_nome_Visible ;
      private int edtavTfmunicipio_nome_sel_Visible ;
      private int edtavTfestado_uf_Visible ;
      private int edtavTfestado_uf_sel_Visible ;
      private int edtavTfcontratante_iniciodoexpediente_Visible ;
      private int edtavTfcontratante_iniciodoexpediente_to_Visible ;
      private int edtavTfcontratante_fimdoexpediente_Visible ;
      private int edtavTfcontratante_fimdoexpediente_to_Visible ;
      private int edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratante_nomefantasiatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratante_telefonetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratante_ramaltitlecontrolidtoreplace_Visible ;
      private int edtavDdo_municipio_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_estado_uftitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratante_iniciodoexpedientetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratante_fimdoexpedientetitlecontrolidtoreplace_Visible ;
      private int A5AreaTrabalho_Codigo ;
      private int A335Contratante_PessoaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A25Municipio_Codigo ;
      private int AV143WWContratanteDS_1_Areatrabalho_codigo ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int edtavDisplay_Visible ;
      private int AV119PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int AV166GXV1 ;
      private int AV167GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV120GridCurrentPage ;
      private long AV121GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratante_razaosocial_Activeeventkey ;
      private String Ddo_contratante_razaosocial_Filteredtext_get ;
      private String Ddo_contratante_razaosocial_Selectedvalue_get ;
      private String Ddo_contratante_nomefantasia_Activeeventkey ;
      private String Ddo_contratante_nomefantasia_Filteredtext_get ;
      private String Ddo_contratante_nomefantasia_Selectedvalue_get ;
      private String Ddo_contratante_cnpj_Activeeventkey ;
      private String Ddo_contratante_cnpj_Filteredtext_get ;
      private String Ddo_contratante_cnpj_Selectedvalue_get ;
      private String Ddo_contratante_telefone_Activeeventkey ;
      private String Ddo_contratante_telefone_Filteredtext_get ;
      private String Ddo_contratante_telefone_Selectedvalue_get ;
      private String Ddo_contratante_ramal_Activeeventkey ;
      private String Ddo_contratante_ramal_Filteredtext_get ;
      private String Ddo_contratante_ramal_Selectedvalue_get ;
      private String Ddo_municipio_nome_Activeeventkey ;
      private String Ddo_municipio_nome_Filteredtext_get ;
      private String Ddo_municipio_nome_Selectedvalue_get ;
      private String Ddo_estado_uf_Activeeventkey ;
      private String Ddo_estado_uf_Filteredtext_get ;
      private String Ddo_estado_uf_Selectedvalue_get ;
      private String Ddo_contratante_iniciodoexpediente_Activeeventkey ;
      private String Ddo_contratante_iniciodoexpediente_Filteredtext_get ;
      private String Ddo_contratante_iniciodoexpediente_Filteredtextto_get ;
      private String Ddo_contratante_fimdoexpediente_Activeeventkey ;
      private String Ddo_contratante_fimdoexpediente_Filteredtext_get ;
      private String Ddo_contratante_fimdoexpediente_Filteredtextto_get ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_32_idx="0001" ;
      private String AV79TFContratante_RazaoSocial ;
      private String AV80TFContratante_RazaoSocial_Sel ;
      private String AV83TFContratante_NomeFantasia ;
      private String AV84TFContratante_NomeFantasia_Sel ;
      private String AV91TFContratante_Telefone ;
      private String AV92TFContratante_Telefone_Sel ;
      private String AV95TFContratante_Ramal ;
      private String AV96TFContratante_Ramal_Sel ;
      private String AV99TFMunicipio_Nome ;
      private String AV100TFMunicipio_Nome_Sel ;
      private String AV103TFEstado_UF ;
      private String AV104TFEstado_UF_Sel ;
      private String AV165Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratante_razaosocial_Caption ;
      private String Ddo_contratante_razaosocial_Tooltip ;
      private String Ddo_contratante_razaosocial_Cls ;
      private String Ddo_contratante_razaosocial_Filteredtext_set ;
      private String Ddo_contratante_razaosocial_Selectedvalue_set ;
      private String Ddo_contratante_razaosocial_Dropdownoptionstype ;
      private String Ddo_contratante_razaosocial_Titlecontrolidtoreplace ;
      private String Ddo_contratante_razaosocial_Sortedstatus ;
      private String Ddo_contratante_razaosocial_Filtertype ;
      private String Ddo_contratante_razaosocial_Datalisttype ;
      private String Ddo_contratante_razaosocial_Datalistproc ;
      private String Ddo_contratante_razaosocial_Sortasc ;
      private String Ddo_contratante_razaosocial_Sortdsc ;
      private String Ddo_contratante_razaosocial_Loadingdata ;
      private String Ddo_contratante_razaosocial_Cleanfilter ;
      private String Ddo_contratante_razaosocial_Noresultsfound ;
      private String Ddo_contratante_razaosocial_Searchbuttontext ;
      private String Ddo_contratante_nomefantasia_Caption ;
      private String Ddo_contratante_nomefantasia_Tooltip ;
      private String Ddo_contratante_nomefantasia_Cls ;
      private String Ddo_contratante_nomefantasia_Filteredtext_set ;
      private String Ddo_contratante_nomefantasia_Selectedvalue_set ;
      private String Ddo_contratante_nomefantasia_Dropdownoptionstype ;
      private String Ddo_contratante_nomefantasia_Titlecontrolidtoreplace ;
      private String Ddo_contratante_nomefantasia_Sortedstatus ;
      private String Ddo_contratante_nomefantasia_Filtertype ;
      private String Ddo_contratante_nomefantasia_Datalisttype ;
      private String Ddo_contratante_nomefantasia_Datalistproc ;
      private String Ddo_contratante_nomefantasia_Sortasc ;
      private String Ddo_contratante_nomefantasia_Sortdsc ;
      private String Ddo_contratante_nomefantasia_Loadingdata ;
      private String Ddo_contratante_nomefantasia_Cleanfilter ;
      private String Ddo_contratante_nomefantasia_Noresultsfound ;
      private String Ddo_contratante_nomefantasia_Searchbuttontext ;
      private String Ddo_contratante_cnpj_Caption ;
      private String Ddo_contratante_cnpj_Tooltip ;
      private String Ddo_contratante_cnpj_Cls ;
      private String Ddo_contratante_cnpj_Filteredtext_set ;
      private String Ddo_contratante_cnpj_Selectedvalue_set ;
      private String Ddo_contratante_cnpj_Dropdownoptionstype ;
      private String Ddo_contratante_cnpj_Titlecontrolidtoreplace ;
      private String Ddo_contratante_cnpj_Sortedstatus ;
      private String Ddo_contratante_cnpj_Filtertype ;
      private String Ddo_contratante_cnpj_Datalisttype ;
      private String Ddo_contratante_cnpj_Datalistproc ;
      private String Ddo_contratante_cnpj_Sortasc ;
      private String Ddo_contratante_cnpj_Sortdsc ;
      private String Ddo_contratante_cnpj_Loadingdata ;
      private String Ddo_contratante_cnpj_Cleanfilter ;
      private String Ddo_contratante_cnpj_Noresultsfound ;
      private String Ddo_contratante_cnpj_Searchbuttontext ;
      private String Ddo_contratante_telefone_Caption ;
      private String Ddo_contratante_telefone_Tooltip ;
      private String Ddo_contratante_telefone_Cls ;
      private String Ddo_contratante_telefone_Filteredtext_set ;
      private String Ddo_contratante_telefone_Selectedvalue_set ;
      private String Ddo_contratante_telefone_Dropdownoptionstype ;
      private String Ddo_contratante_telefone_Titlecontrolidtoreplace ;
      private String Ddo_contratante_telefone_Sortedstatus ;
      private String Ddo_contratante_telefone_Filtertype ;
      private String Ddo_contratante_telefone_Datalisttype ;
      private String Ddo_contratante_telefone_Datalistproc ;
      private String Ddo_contratante_telefone_Sortasc ;
      private String Ddo_contratante_telefone_Sortdsc ;
      private String Ddo_contratante_telefone_Loadingdata ;
      private String Ddo_contratante_telefone_Cleanfilter ;
      private String Ddo_contratante_telefone_Noresultsfound ;
      private String Ddo_contratante_telefone_Searchbuttontext ;
      private String Ddo_contratante_ramal_Caption ;
      private String Ddo_contratante_ramal_Tooltip ;
      private String Ddo_contratante_ramal_Cls ;
      private String Ddo_contratante_ramal_Filteredtext_set ;
      private String Ddo_contratante_ramal_Selectedvalue_set ;
      private String Ddo_contratante_ramal_Dropdownoptionstype ;
      private String Ddo_contratante_ramal_Titlecontrolidtoreplace ;
      private String Ddo_contratante_ramal_Sortedstatus ;
      private String Ddo_contratante_ramal_Filtertype ;
      private String Ddo_contratante_ramal_Datalisttype ;
      private String Ddo_contratante_ramal_Datalistproc ;
      private String Ddo_contratante_ramal_Sortasc ;
      private String Ddo_contratante_ramal_Sortdsc ;
      private String Ddo_contratante_ramal_Loadingdata ;
      private String Ddo_contratante_ramal_Cleanfilter ;
      private String Ddo_contratante_ramal_Noresultsfound ;
      private String Ddo_contratante_ramal_Searchbuttontext ;
      private String Ddo_municipio_nome_Caption ;
      private String Ddo_municipio_nome_Tooltip ;
      private String Ddo_municipio_nome_Cls ;
      private String Ddo_municipio_nome_Filteredtext_set ;
      private String Ddo_municipio_nome_Selectedvalue_set ;
      private String Ddo_municipio_nome_Dropdownoptionstype ;
      private String Ddo_municipio_nome_Titlecontrolidtoreplace ;
      private String Ddo_municipio_nome_Sortedstatus ;
      private String Ddo_municipio_nome_Filtertype ;
      private String Ddo_municipio_nome_Datalisttype ;
      private String Ddo_municipio_nome_Datalistproc ;
      private String Ddo_municipio_nome_Sortasc ;
      private String Ddo_municipio_nome_Sortdsc ;
      private String Ddo_municipio_nome_Loadingdata ;
      private String Ddo_municipio_nome_Cleanfilter ;
      private String Ddo_municipio_nome_Noresultsfound ;
      private String Ddo_municipio_nome_Searchbuttontext ;
      private String Ddo_estado_uf_Caption ;
      private String Ddo_estado_uf_Tooltip ;
      private String Ddo_estado_uf_Cls ;
      private String Ddo_estado_uf_Filteredtext_set ;
      private String Ddo_estado_uf_Selectedvalue_set ;
      private String Ddo_estado_uf_Dropdownoptionstype ;
      private String Ddo_estado_uf_Titlecontrolidtoreplace ;
      private String Ddo_estado_uf_Sortedstatus ;
      private String Ddo_estado_uf_Filtertype ;
      private String Ddo_estado_uf_Datalisttype ;
      private String Ddo_estado_uf_Datalistproc ;
      private String Ddo_estado_uf_Sortasc ;
      private String Ddo_estado_uf_Sortdsc ;
      private String Ddo_estado_uf_Loadingdata ;
      private String Ddo_estado_uf_Cleanfilter ;
      private String Ddo_estado_uf_Noresultsfound ;
      private String Ddo_estado_uf_Searchbuttontext ;
      private String Ddo_contratante_iniciodoexpediente_Caption ;
      private String Ddo_contratante_iniciodoexpediente_Tooltip ;
      private String Ddo_contratante_iniciodoexpediente_Cls ;
      private String Ddo_contratante_iniciodoexpediente_Filteredtext_set ;
      private String Ddo_contratante_iniciodoexpediente_Filteredtextto_set ;
      private String Ddo_contratante_iniciodoexpediente_Dropdownoptionstype ;
      private String Ddo_contratante_iniciodoexpediente_Titlecontrolidtoreplace ;
      private String Ddo_contratante_iniciodoexpediente_Sortedstatus ;
      private String Ddo_contratante_iniciodoexpediente_Filtertype ;
      private String Ddo_contratante_iniciodoexpediente_Sortasc ;
      private String Ddo_contratante_iniciodoexpediente_Sortdsc ;
      private String Ddo_contratante_iniciodoexpediente_Cleanfilter ;
      private String Ddo_contratante_iniciodoexpediente_Rangefilterfrom ;
      private String Ddo_contratante_iniciodoexpediente_Rangefilterto ;
      private String Ddo_contratante_iniciodoexpediente_Searchbuttontext ;
      private String Ddo_contratante_fimdoexpediente_Caption ;
      private String Ddo_contratante_fimdoexpediente_Tooltip ;
      private String Ddo_contratante_fimdoexpediente_Cls ;
      private String Ddo_contratante_fimdoexpediente_Filteredtext_set ;
      private String Ddo_contratante_fimdoexpediente_Filteredtextto_set ;
      private String Ddo_contratante_fimdoexpediente_Dropdownoptionstype ;
      private String Ddo_contratante_fimdoexpediente_Titlecontrolidtoreplace ;
      private String Ddo_contratante_fimdoexpediente_Sortedstatus ;
      private String Ddo_contratante_fimdoexpediente_Filtertype ;
      private String Ddo_contratante_fimdoexpediente_Sortasc ;
      private String Ddo_contratante_fimdoexpediente_Sortdsc ;
      private String Ddo_contratante_fimdoexpediente_Cleanfilter ;
      private String Ddo_contratante_fimdoexpediente_Rangefilterfrom ;
      private String Ddo_contratante_fimdoexpediente_Rangefilterto ;
      private String Ddo_contratante_fimdoexpediente_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String edtavTfcontratante_razaosocial_Internalname ;
      private String edtavTfcontratante_razaosocial_Jsonclick ;
      private String edtavTfcontratante_razaosocial_sel_Internalname ;
      private String edtavTfcontratante_razaosocial_sel_Jsonclick ;
      private String edtavTfcontratante_nomefantasia_Internalname ;
      private String edtavTfcontratante_nomefantasia_Jsonclick ;
      private String edtavTfcontratante_nomefantasia_sel_Internalname ;
      private String edtavTfcontratante_nomefantasia_sel_Jsonclick ;
      private String edtavTfcontratante_cnpj_Internalname ;
      private String edtavTfcontratante_cnpj_Jsonclick ;
      private String edtavTfcontratante_cnpj_sel_Internalname ;
      private String edtavTfcontratante_cnpj_sel_Jsonclick ;
      private String edtavTfcontratante_telefone_Internalname ;
      private String edtavTfcontratante_telefone_Jsonclick ;
      private String edtavTfcontratante_telefone_sel_Internalname ;
      private String edtavTfcontratante_telefone_sel_Jsonclick ;
      private String edtavTfcontratante_ramal_Internalname ;
      private String edtavTfcontratante_ramal_Jsonclick ;
      private String edtavTfcontratante_ramal_sel_Internalname ;
      private String edtavTfcontratante_ramal_sel_Jsonclick ;
      private String edtavTfmunicipio_nome_Internalname ;
      private String edtavTfmunicipio_nome_Jsonclick ;
      private String edtavTfmunicipio_nome_sel_Internalname ;
      private String edtavTfmunicipio_nome_sel_Jsonclick ;
      private String edtavTfestado_uf_Internalname ;
      private String edtavTfestado_uf_Jsonclick ;
      private String edtavTfestado_uf_sel_Internalname ;
      private String edtavTfestado_uf_sel_Jsonclick ;
      private String edtavTfcontratante_iniciodoexpediente_Internalname ;
      private String edtavTfcontratante_iniciodoexpediente_Jsonclick ;
      private String edtavTfcontratante_iniciodoexpediente_to_Internalname ;
      private String edtavTfcontratante_iniciodoexpediente_to_Jsonclick ;
      private String divDdo_contratante_iniciodoexpedienteauxdates_Internalname ;
      private String edtavDdo_contratante_iniciodoexpedienteauxdate_Internalname ;
      private String edtavDdo_contratante_iniciodoexpedienteauxdate_Jsonclick ;
      private String edtavDdo_contratante_iniciodoexpedienteauxdateto_Internalname ;
      private String edtavDdo_contratante_iniciodoexpedienteauxdateto_Jsonclick ;
      private String edtavTfcontratante_fimdoexpediente_Internalname ;
      private String edtavTfcontratante_fimdoexpediente_Jsonclick ;
      private String edtavTfcontratante_fimdoexpediente_to_Internalname ;
      private String edtavTfcontratante_fimdoexpediente_to_Jsonclick ;
      private String divDdo_contratante_fimdoexpedienteauxdates_Internalname ;
      private String edtavDdo_contratante_fimdoexpedienteauxdate_Internalname ;
      private String edtavDdo_contratante_fimdoexpedienteauxdate_Jsonclick ;
      private String edtavDdo_contratante_fimdoexpedienteauxdateto_Internalname ;
      private String edtavDdo_contratante_fimdoexpedienteauxdateto_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratante_nomefantasiatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratante_telefonetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratante_ramaltitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_estado_uftitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratante_iniciodoexpedientetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratante_fimdoexpedientetitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtAreaTrabalho_Codigo_Internalname ;
      private String edtContratante_Codigo_Internalname ;
      private String edtContratante_PessoaCod_Internalname ;
      private String A9Contratante_RazaoSocial ;
      private String edtContratante_RazaoSocial_Internalname ;
      private String A10Contratante_NomeFantasia ;
      private String edtContratante_NomeFantasia_Internalname ;
      private String edtContratante_CNPJ_Internalname ;
      private String A31Contratante_Telefone ;
      private String edtContratante_Telefone_Internalname ;
      private String A32Contratante_Ramal ;
      private String edtContratante_Ramal_Internalname ;
      private String A26Municipio_Nome ;
      private String edtMunicipio_Nome_Internalname ;
      private String A23Estado_UF ;
      private String edtEstado_UF_Internalname ;
      private String edtContratante_InicioDoExpediente_Internalname ;
      private String edtContratante_FimDoExpediente_Internalname ;
      private String edtavAreatrabalho_codigo_Internalname ;
      private String scmdbuf ;
      private String lV144WWContratanteDS_2_Tfcontratante_razaosocial ;
      private String lV146WWContratanteDS_4_Tfcontratante_nomefantasia ;
      private String lV150WWContratanteDS_8_Tfcontratante_telefone ;
      private String lV152WWContratanteDS_10_Tfcontratante_ramal ;
      private String lV154WWContratanteDS_12_Tfmunicipio_nome ;
      private String lV156WWContratanteDS_14_Tfestado_uf ;
      private String AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel ;
      private String AV144WWContratanteDS_2_Tfcontratante_razaosocial ;
      private String AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel ;
      private String AV146WWContratanteDS_4_Tfcontratante_nomefantasia ;
      private String AV151WWContratanteDS_9_Tfcontratante_telefone_sel ;
      private String AV150WWContratanteDS_8_Tfcontratante_telefone ;
      private String AV153WWContratanteDS_11_Tfcontratante_ramal_sel ;
      private String AV152WWContratanteDS_10_Tfcontratante_ramal ;
      private String AV155WWContratanteDS_13_Tfmunicipio_nome_sel ;
      private String AV154WWContratanteDS_12_Tfmunicipio_nome ;
      private String AV157WWContratanteDS_15_Tfestado_uf_sel ;
      private String AV156WWContratanteDS_14_Tfestado_uf ;
      private String subGrid_Internalname ;
      private String Ddo_contratante_razaosocial_Internalname ;
      private String Ddo_contratante_nomefantasia_Internalname ;
      private String Ddo_contratante_cnpj_Internalname ;
      private String Ddo_contratante_telefone_Internalname ;
      private String Ddo_contratante_ramal_Internalname ;
      private String Ddo_municipio_nome_Internalname ;
      private String Ddo_estado_uf_Internalname ;
      private String Ddo_contratante_iniciodoexpediente_Internalname ;
      private String Ddo_contratante_fimdoexpediente_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtContratante_RazaoSocial_Title ;
      private String edtContratante_NomeFantasia_Title ;
      private String edtContratante_CNPJ_Title ;
      private String edtContratante_Telefone_Title ;
      private String edtContratante_Ramal_Title ;
      private String edtMunicipio_Nome_Title ;
      private String edtEstado_UF_Title ;
      private String edtContratante_InicioDoExpediente_Title ;
      private String edtContratante_FimDoExpediente_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtContratante_RazaoSocial_Link ;
      private String GXt_char2 ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextareatrabalho_codigo_Internalname ;
      private String lblFiltertextareatrabalho_codigo_Jsonclick ;
      private String edtavAreatrabalho_codigo_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblContratantetitle_Internalname ;
      private String lblContratantetitle_Jsonclick ;
      private String sGXsfl_32_fel_idx="0001" ;
      private String ROClassString ;
      private String edtAreaTrabalho_Codigo_Jsonclick ;
      private String edtContratante_Codigo_Jsonclick ;
      private String edtContratante_PessoaCod_Jsonclick ;
      private String edtContratante_RazaoSocial_Jsonclick ;
      private String edtContratante_NomeFantasia_Jsonclick ;
      private String edtContratante_CNPJ_Jsonclick ;
      private String gxphoneLink ;
      private String edtContratante_Telefone_Jsonclick ;
      private String edtContratante_Ramal_Jsonclick ;
      private String edtMunicipio_Nome_Jsonclick ;
      private String edtEstado_UF_Jsonclick ;
      private String edtContratante_InicioDoExpediente_Jsonclick ;
      private String edtContratante_FimDoExpediente_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV107TFContratante_InicioDoExpediente ;
      private DateTime AV108TFContratante_InicioDoExpediente_To ;
      private DateTime AV113TFContratante_FimDoExpediente ;
      private DateTime AV114TFContratante_FimDoExpediente_To ;
      private DateTime A1448Contratante_InicioDoExpediente ;
      private DateTime A1192Contratante_FimDoExpediente ;
      private DateTime AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente ;
      private DateTime AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ;
      private DateTime AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente ;
      private DateTime AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ;
      private DateTime AV109DDO_Contratante_InicioDoExpedienteAuxDate ;
      private DateTime AV110DDO_Contratante_InicioDoExpedienteAuxDateTo ;
      private DateTime AV115DDO_Contratante_FimDoExpedienteAuxDate ;
      private DateTime AV116DDO_Contratante_FimDoExpedienteAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool n29Contratante_Codigo ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratante_razaosocial_Includesortasc ;
      private bool Ddo_contratante_razaosocial_Includesortdsc ;
      private bool Ddo_contratante_razaosocial_Includefilter ;
      private bool Ddo_contratante_razaosocial_Filterisrange ;
      private bool Ddo_contratante_razaosocial_Includedatalist ;
      private bool Ddo_contratante_nomefantasia_Includesortasc ;
      private bool Ddo_contratante_nomefantasia_Includesortdsc ;
      private bool Ddo_contratante_nomefantasia_Includefilter ;
      private bool Ddo_contratante_nomefantasia_Filterisrange ;
      private bool Ddo_contratante_nomefantasia_Includedatalist ;
      private bool Ddo_contratante_cnpj_Includesortasc ;
      private bool Ddo_contratante_cnpj_Includesortdsc ;
      private bool Ddo_contratante_cnpj_Includefilter ;
      private bool Ddo_contratante_cnpj_Filterisrange ;
      private bool Ddo_contratante_cnpj_Includedatalist ;
      private bool Ddo_contratante_telefone_Includesortasc ;
      private bool Ddo_contratante_telefone_Includesortdsc ;
      private bool Ddo_contratante_telefone_Includefilter ;
      private bool Ddo_contratante_telefone_Filterisrange ;
      private bool Ddo_contratante_telefone_Includedatalist ;
      private bool Ddo_contratante_ramal_Includesortasc ;
      private bool Ddo_contratante_ramal_Includesortdsc ;
      private bool Ddo_contratante_ramal_Includefilter ;
      private bool Ddo_contratante_ramal_Filterisrange ;
      private bool Ddo_contratante_ramal_Includedatalist ;
      private bool Ddo_municipio_nome_Includesortasc ;
      private bool Ddo_municipio_nome_Includesortdsc ;
      private bool Ddo_municipio_nome_Includefilter ;
      private bool Ddo_municipio_nome_Filterisrange ;
      private bool Ddo_municipio_nome_Includedatalist ;
      private bool Ddo_estado_uf_Includesortasc ;
      private bool Ddo_estado_uf_Includesortdsc ;
      private bool Ddo_estado_uf_Includefilter ;
      private bool Ddo_estado_uf_Filterisrange ;
      private bool Ddo_estado_uf_Includedatalist ;
      private bool Ddo_contratante_iniciodoexpediente_Includesortasc ;
      private bool Ddo_contratante_iniciodoexpediente_Includesortdsc ;
      private bool Ddo_contratante_iniciodoexpediente_Includefilter ;
      private bool Ddo_contratante_iniciodoexpediente_Filterisrange ;
      private bool Ddo_contratante_iniciodoexpediente_Includedatalist ;
      private bool Ddo_contratante_fimdoexpediente_Includesortasc ;
      private bool Ddo_contratante_fimdoexpediente_Includesortdsc ;
      private bool Ddo_contratante_fimdoexpediente_Includefilter ;
      private bool Ddo_contratante_fimdoexpediente_Filterisrange ;
      private bool Ddo_contratante_fimdoexpediente_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n9Contratante_RazaoSocial ;
      private bool n12Contratante_CNPJ ;
      private bool n32Contratante_Ramal ;
      private bool n1448Contratante_InicioDoExpediente ;
      private bool n1192Contratante_FimDoExpediente ;
      private bool n25Municipio_Codigo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Update_IsBlob ;
      private bool AV32Delete_IsBlob ;
      private bool AV68Display_IsBlob ;
      private String AV62ManageFiltersXml ;
      private String AV87TFContratante_CNPJ ;
      private String AV88TFContratante_CNPJ_Sel ;
      private String AV81ddo_Contratante_RazaoSocialTitleControlIdToReplace ;
      private String AV85ddo_Contratante_NomeFantasiaTitleControlIdToReplace ;
      private String AV89ddo_Contratante_CNPJTitleControlIdToReplace ;
      private String AV93ddo_Contratante_TelefoneTitleControlIdToReplace ;
      private String AV97ddo_Contratante_RamalTitleControlIdToReplace ;
      private String AV101ddo_Municipio_NomeTitleControlIdToReplace ;
      private String AV105ddo_Estado_UFTitleControlIdToReplace ;
      private String AV111ddo_Contratante_InicioDoExpedienteTitleControlIdToReplace ;
      private String AV117ddo_Contratante_FimDoExpedienteTitleControlIdToReplace ;
      private String AV162Update_GXI ;
      private String AV163Delete_GXI ;
      private String AV164Display_GXI ;
      private String A12Contratante_CNPJ ;
      private String lV148WWContratanteDS_6_Tfcontratante_cnpj ;
      private String AV149WWContratanteDS_7_Tfcontratante_cnpj_sel ;
      private String AV148WWContratanteDS_6_Tfcontratante_cnpj ;
      private String AV31Update ;
      private String AV32Delete ;
      private String AV68Display ;
      private IGxSession AV35Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H000Y2_A25Municipio_Codigo ;
      private bool[] H000Y2_n25Municipio_Codigo ;
      private DateTime[] H000Y2_A1192Contratante_FimDoExpediente ;
      private bool[] H000Y2_n1192Contratante_FimDoExpediente ;
      private DateTime[] H000Y2_A1448Contratante_InicioDoExpediente ;
      private bool[] H000Y2_n1448Contratante_InicioDoExpediente ;
      private String[] H000Y2_A23Estado_UF ;
      private String[] H000Y2_A26Municipio_Nome ;
      private String[] H000Y2_A32Contratante_Ramal ;
      private bool[] H000Y2_n32Contratante_Ramal ;
      private String[] H000Y2_A31Contratante_Telefone ;
      private String[] H000Y2_A12Contratante_CNPJ ;
      private bool[] H000Y2_n12Contratante_CNPJ ;
      private String[] H000Y2_A10Contratante_NomeFantasia ;
      private String[] H000Y2_A9Contratante_RazaoSocial ;
      private bool[] H000Y2_n9Contratante_RazaoSocial ;
      private int[] H000Y2_A335Contratante_PessoaCod ;
      private int[] H000Y2_A29Contratante_Codigo ;
      private bool[] H000Y2_n29Contratante_Codigo ;
      private int[] H000Y2_A5AreaTrabalho_Codigo ;
      private long[] H000Y3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV63ManageFiltersItems ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV65ManageFiltersData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV78Contratante_RazaoSocialTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV82Contratante_NomeFantasiaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV86Contratante_CNPJTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV90Contratante_TelefoneTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV94Contratante_RamalTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV98Municipio_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV102Estado_UFTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV106Contratante_InicioDoExpedienteTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV112Contratante_FimDoExpedienteTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV64ManageFiltersItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV66ManageFiltersDataItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV118DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwcontratante__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H000Y2( IGxContext context ,
                                             String AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel ,
                                             String AV144WWContratanteDS_2_Tfcontratante_razaosocial ,
                                             String AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel ,
                                             String AV146WWContratanteDS_4_Tfcontratante_nomefantasia ,
                                             String AV149WWContratanteDS_7_Tfcontratante_cnpj_sel ,
                                             String AV148WWContratanteDS_6_Tfcontratante_cnpj ,
                                             String AV151WWContratanteDS_9_Tfcontratante_telefone_sel ,
                                             String AV150WWContratanteDS_8_Tfcontratante_telefone ,
                                             String AV153WWContratanteDS_11_Tfcontratante_ramal_sel ,
                                             String AV152WWContratanteDS_10_Tfcontratante_ramal ,
                                             String AV155WWContratanteDS_13_Tfmunicipio_nome_sel ,
                                             String AV154WWContratanteDS_12_Tfmunicipio_nome ,
                                             String AV157WWContratanteDS_15_Tfestado_uf_sel ,
                                             String AV156WWContratanteDS_14_Tfestado_uf ,
                                             DateTime AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente ,
                                             DateTime AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ,
                                             DateTime AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente ,
                                             DateTime AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ,
                                             String A9Contratante_RazaoSocial ,
                                             String A10Contratante_NomeFantasia ,
                                             String A12Contratante_CNPJ ,
                                             String A31Contratante_Telefone ,
                                             String A32Contratante_Ramal ,
                                             String A26Municipio_Nome ,
                                             String A23Estado_UF ,
                                             DateTime A1448Contratante_InicioDoExpediente ,
                                             DateTime A1192Contratante_FimDoExpediente ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A5AreaTrabalho_Codigo ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [24] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Municipio_Codigo], T2.[Contratante_FimDoExpediente], T2.[Contratante_InicioDoExpediente], T3.[Estado_UF], T3.[Municipio_Nome], T2.[Contratante_Ramal], T2.[Contratante_Telefone], T4.[Pessoa_Docto] AS Contratante_CNPJ, T2.[Contratante_NomeFantasia], T4.[Pessoa_Nome] AS Contratante_RazaoSocial, T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[Contratante_Codigo], T1.[AreaTrabalho_Codigo]";
         sFromString = " FROM ((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T2.[Contratante_PessoaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[AreaTrabalho_Codigo] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV144WWContratanteDS_2_Tfcontratante_razaosocial)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV144WWContratanteDS_2_Tfcontratante_razaosocial)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV146WWContratanteDS_4_Tfcontratante_nomefantasia)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] like @lV146WWContratanteDS_4_Tfcontratante_nomefantasia)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] = @AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV149WWContratanteDS_7_Tfcontratante_cnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV148WWContratanteDS_6_Tfcontratante_cnpj)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV148WWContratanteDS_6_Tfcontratante_cnpj)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV149WWContratanteDS_7_Tfcontratante_cnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV149WWContratanteDS_7_Tfcontratante_cnpj_sel)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV151WWContratanteDS_9_Tfcontratante_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV150WWContratanteDS_8_Tfcontratante_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] like @lV150WWContratanteDS_8_Tfcontratante_telefone)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151WWContratanteDS_9_Tfcontratante_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] = @AV151WWContratanteDS_9_Tfcontratante_telefone_sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV153WWContratanteDS_11_Tfcontratante_ramal_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV152WWContratanteDS_10_Tfcontratante_ramal)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Ramal] like @lV152WWContratanteDS_10_Tfcontratante_ramal)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV153WWContratanteDS_11_Tfcontratante_ramal_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Ramal] = @AV153WWContratanteDS_11_Tfcontratante_ramal_sel)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV155WWContratanteDS_13_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV154WWContratanteDS_12_Tfmunicipio_nome)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV154WWContratanteDS_12_Tfmunicipio_nome)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV155WWContratanteDS_13_Tfmunicipio_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV155WWContratanteDS_13_Tfmunicipio_nome_sel)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV157WWContratanteDS_15_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV156WWContratanteDS_14_Tfestado_uf)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Estado_UF] like @lV156WWContratanteDS_14_Tfestado_uf)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV157WWContratanteDS_15_Tfestado_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Estado_UF] = @AV157WWContratanteDS_15_Tfestado_uf_sel)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_InicioDoExpediente] >= @AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_InicioDoExpediente] <= @AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_FimDoExpediente] >= @AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_FimDoExpediente] <= @AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratante_NomeFantasia]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratante_NomeFantasia] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Docto]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Docto] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratante_Telefone]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratante_Telefone] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratante_Ramal]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratante_Ramal] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Municipio_Nome]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Municipio_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Estado_UF]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Estado_UF] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratante_InicioDoExpediente]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratante_InicioDoExpediente] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratante_FimDoExpediente]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratante_FimDoExpediente] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H000Y3( IGxContext context ,
                                             String AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel ,
                                             String AV144WWContratanteDS_2_Tfcontratante_razaosocial ,
                                             String AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel ,
                                             String AV146WWContratanteDS_4_Tfcontratante_nomefantasia ,
                                             String AV149WWContratanteDS_7_Tfcontratante_cnpj_sel ,
                                             String AV148WWContratanteDS_6_Tfcontratante_cnpj ,
                                             String AV151WWContratanteDS_9_Tfcontratante_telefone_sel ,
                                             String AV150WWContratanteDS_8_Tfcontratante_telefone ,
                                             String AV153WWContratanteDS_11_Tfcontratante_ramal_sel ,
                                             String AV152WWContratanteDS_10_Tfcontratante_ramal ,
                                             String AV155WWContratanteDS_13_Tfmunicipio_nome_sel ,
                                             String AV154WWContratanteDS_12_Tfmunicipio_nome ,
                                             String AV157WWContratanteDS_15_Tfestado_uf_sel ,
                                             String AV156WWContratanteDS_14_Tfestado_uf ,
                                             DateTime AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente ,
                                             DateTime AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to ,
                                             DateTime AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente ,
                                             DateTime AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to ,
                                             String A9Contratante_RazaoSocial ,
                                             String A10Contratante_NomeFantasia ,
                                             String A12Contratante_CNPJ ,
                                             String A31Contratante_Telefone ,
                                             String A32Contratante_Ramal ,
                                             String A26Municipio_Nome ,
                                             String A23Estado_UF ,
                                             DateTime A1448Contratante_InicioDoExpediente ,
                                             DateTime A1192Contratante_FimDoExpediente ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A5AreaTrabalho_Codigo ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [19] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T2.[Contratante_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[AreaTrabalho_Codigo] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV144WWContratanteDS_2_Tfcontratante_razaosocial)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV144WWContratanteDS_2_Tfcontratante_razaosocial)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV146WWContratanteDS_4_Tfcontratante_nomefantasia)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] like @lV146WWContratanteDS_4_Tfcontratante_nomefantasia)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_NomeFantasia] = @AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV149WWContratanteDS_7_Tfcontratante_cnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV148WWContratanteDS_6_Tfcontratante_cnpj)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV148WWContratanteDS_6_Tfcontratante_cnpj)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV149WWContratanteDS_7_Tfcontratante_cnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV149WWContratanteDS_7_Tfcontratante_cnpj_sel)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV151WWContratanteDS_9_Tfcontratante_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV150WWContratanteDS_8_Tfcontratante_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] like @lV150WWContratanteDS_8_Tfcontratante_telefone)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151WWContratanteDS_9_Tfcontratante_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] = @AV151WWContratanteDS_9_Tfcontratante_telefone_sel)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV153WWContratanteDS_11_Tfcontratante_ramal_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV152WWContratanteDS_10_Tfcontratante_ramal)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Ramal] like @lV152WWContratanteDS_10_Tfcontratante_ramal)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV153WWContratanteDS_11_Tfcontratante_ramal_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Ramal] = @AV153WWContratanteDS_11_Tfcontratante_ramal_sel)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV155WWContratanteDS_13_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV154WWContratanteDS_12_Tfmunicipio_nome)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV154WWContratanteDS_12_Tfmunicipio_nome)";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV155WWContratanteDS_13_Tfmunicipio_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV155WWContratanteDS_13_Tfmunicipio_nome_sel)";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV157WWContratanteDS_15_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV156WWContratanteDS_14_Tfestado_uf)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Estado_UF] like @lV156WWContratanteDS_14_Tfestado_uf)";
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV157WWContratanteDS_15_Tfestado_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Estado_UF] = @AV157WWContratanteDS_15_Tfestado_uf_sel)";
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_InicioDoExpediente] >= @AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente)";
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_InicioDoExpediente] <= @AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to)";
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_FimDoExpediente] >= @AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente)";
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_FimDoExpediente] <= @AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to)";
         }
         else
         {
            GXv_int5[18] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H000Y2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (short)dynConstraints[27] , (bool)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] );
               case 1 :
                     return conditional_H000Y3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (short)dynConstraints[27] , (bool)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH000Y2 ;
          prmH000Y2 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV144WWContratanteDS_2_Tfcontratante_razaosocial",SqlDbType.Char,100,0} ,
          new Object[] {"@AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV146WWContratanteDS_4_Tfcontratante_nomefantasia",SqlDbType.Char,100,0} ,
          new Object[] {"@AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV148WWContratanteDS_6_Tfcontratante_cnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV149WWContratanteDS_7_Tfcontratante_cnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV150WWContratanteDS_8_Tfcontratante_telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@AV151WWContratanteDS_9_Tfcontratante_telefone_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV152WWContratanteDS_10_Tfcontratante_ramal",SqlDbType.Char,10,0} ,
          new Object[] {"@AV153WWContratanteDS_11_Tfcontratante_ramal_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV154WWContratanteDS_12_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV155WWContratanteDS_13_Tfmunicipio_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV156WWContratanteDS_14_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV157WWContratanteDS_15_Tfestado_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to",SqlDbType.DateTime,0,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH000Y3 ;
          prmH000Y3 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV144WWContratanteDS_2_Tfcontratante_razaosocial",SqlDbType.Char,100,0} ,
          new Object[] {"@AV145WWContratanteDS_3_Tfcontratante_razaosocial_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV146WWContratanteDS_4_Tfcontratante_nomefantasia",SqlDbType.Char,100,0} ,
          new Object[] {"@AV147WWContratanteDS_5_Tfcontratante_nomefantasia_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV148WWContratanteDS_6_Tfcontratante_cnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV149WWContratanteDS_7_Tfcontratante_cnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV150WWContratanteDS_8_Tfcontratante_telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@AV151WWContratanteDS_9_Tfcontratante_telefone_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV152WWContratanteDS_10_Tfcontratante_ramal",SqlDbType.Char,10,0} ,
          new Object[] {"@AV153WWContratanteDS_11_Tfcontratante_ramal_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV154WWContratanteDS_12_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV155WWContratanteDS_13_Tfmunicipio_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV156WWContratanteDS_14_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV157WWContratanteDS_15_Tfestado_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV158WWContratanteDS_16_Tfcontratante_iniciodoexpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV159WWContratanteDS_17_Tfcontratante_iniciodoexpediente_to",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV160WWContratanteDS_18_Tfcontratante_fimdoexpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@AV161WWContratanteDS_19_Tfcontratante_fimdoexpediente_to",SqlDbType.DateTime,0,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H000Y2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000Y2,11,0,true,false )
             ,new CursorDef("H000Y3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000Y3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 2) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 20) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 100) ;
                ((String[]) buf[14])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((int[]) buf[17])[0] = rslt.getInt(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((int[]) buf[19])[0] = rslt.getInt(13) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[37]);
                }
                return;
       }
    }

 }

}
