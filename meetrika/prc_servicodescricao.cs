/*
               File: PRC_ServicoDescricao
        Description: Servico Descricao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:17.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_servicodescricao : GXProcedure
   {
      public prc_servicodescricao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_servicodescricao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Servico_Codigo ,
                           out String aP1_Servico_Descricao )
      {
         this.A155Servico_Codigo = aP0_Servico_Codigo;
         this.AV8Servico_Descricao = "" ;
         initialize();
         executePrivate();
         aP0_Servico_Codigo=this.A155Servico_Codigo;
         aP1_Servico_Descricao=this.AV8Servico_Descricao;
      }

      public String executeUdp( ref int aP0_Servico_Codigo )
      {
         this.A155Servico_Codigo = aP0_Servico_Codigo;
         this.AV8Servico_Descricao = "" ;
         initialize();
         executePrivate();
         aP0_Servico_Codigo=this.A155Servico_Codigo;
         aP1_Servico_Descricao=this.AV8Servico_Descricao;
         return AV8Servico_Descricao ;
      }

      public void executeSubmit( ref int aP0_Servico_Codigo ,
                                 out String aP1_Servico_Descricao )
      {
         prc_servicodescricao objprc_servicodescricao;
         objprc_servicodescricao = new prc_servicodescricao();
         objprc_servicodescricao.A155Servico_Codigo = aP0_Servico_Codigo;
         objprc_servicodescricao.AV8Servico_Descricao = "" ;
         objprc_servicodescricao.context.SetSubmitInitialConfig(context);
         objprc_servicodescricao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_servicodescricao);
         aP0_Servico_Codigo=this.A155Servico_Codigo;
         aP1_Servico_Descricao=this.AV8Servico_Descricao;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_servicodescricao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P004O2 */
         pr_default.execute(0, new Object[] {A155Servico_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A156Servico_Descricao = P004O2_A156Servico_Descricao[0];
            n156Servico_Descricao = P004O2_n156Servico_Descricao[0];
            AV8Servico_Descricao = A156Servico_Descricao;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P004O2_A155Servico_Codigo = new int[1] ;
         P004O2_A156Servico_Descricao = new String[] {""} ;
         P004O2_n156Servico_Descricao = new bool[] {false} ;
         A156Servico_Descricao = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_servicodescricao__default(),
            new Object[][] {
                new Object[] {
               P004O2_A155Servico_Codigo, P004O2_A156Servico_Descricao, P004O2_n156Servico_Descricao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A155Servico_Codigo ;
      private String scmdbuf ;
      private bool n156Servico_Descricao ;
      private String AV8Servico_Descricao ;
      private String A156Servico_Descricao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Servico_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P004O2_A155Servico_Codigo ;
      private String[] P004O2_A156Servico_Descricao ;
      private bool[] P004O2_n156Servico_Descricao ;
      private String aP1_Servico_Descricao ;
   }

   public class prc_servicodescricao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004O2 ;
          prmP004O2 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P004O2", "SELECT TOP 1 [Servico_Codigo], [Servico_Descricao] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004O2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
