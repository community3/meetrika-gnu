/*
               File: GetPromptContratoDadosCertameFilterData
        Description: Get Prompt Contrato Dados Certame Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:48.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontratodadoscertamefilterdata : GXProcedure
   {
      public getpromptcontratodadoscertamefilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontratodadoscertamefilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV32DDOName = aP0_DDOName;
         this.AV30SearchTxt = aP1_SearchTxt;
         this.AV31SearchTxtTo = aP2_SearchTxtTo;
         this.AV36OptionsJson = "" ;
         this.AV39OptionsDescJson = "" ;
         this.AV41OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV36OptionsJson;
         aP4_OptionsDescJson=this.AV39OptionsDescJson;
         aP5_OptionIndexesJson=this.AV41OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV32DDOName = aP0_DDOName;
         this.AV30SearchTxt = aP1_SearchTxt;
         this.AV31SearchTxtTo = aP2_SearchTxtTo;
         this.AV36OptionsJson = "" ;
         this.AV39OptionsDescJson = "" ;
         this.AV41OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV36OptionsJson;
         aP4_OptionsDescJson=this.AV39OptionsDescJson;
         aP5_OptionIndexesJson=this.AV41OptionIndexesJson;
         return AV41OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontratodadoscertamefilterdata objgetpromptcontratodadoscertamefilterdata;
         objgetpromptcontratodadoscertamefilterdata = new getpromptcontratodadoscertamefilterdata();
         objgetpromptcontratodadoscertamefilterdata.AV32DDOName = aP0_DDOName;
         objgetpromptcontratodadoscertamefilterdata.AV30SearchTxt = aP1_SearchTxt;
         objgetpromptcontratodadoscertamefilterdata.AV31SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontratodadoscertamefilterdata.AV36OptionsJson = "" ;
         objgetpromptcontratodadoscertamefilterdata.AV39OptionsDescJson = "" ;
         objgetpromptcontratodadoscertamefilterdata.AV41OptionIndexesJson = "" ;
         objgetpromptcontratodadoscertamefilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontratodadoscertamefilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontratodadoscertamefilterdata);
         aP3_OptionsJson=this.AV36OptionsJson;
         aP4_OptionsDescJson=this.AV39OptionsDescJson;
         aP5_OptionIndexesJson=this.AV41OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontratodadoscertamefilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV35Options = (IGxCollection)(new GxSimpleCollection());
         AV38OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV40OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV32DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV32DDOName), "DDO_CONTRATODADOSCERTAME_MODALIDADE") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATODADOSCERTAME_MODALIDADEOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV32DDOName), "DDO_CONTRATODADOSCERTAME_SITE") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATODADOSCERTAME_SITEOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV36OptionsJson = AV35Options.ToJSonString(false);
         AV39OptionsDescJson = AV38OptionsDesc.ToJSonString(false);
         AV41OptionIndexesJson = AV40OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV43Session.Get("PromptContratoDadosCertameGridState"), "") == 0 )
         {
            AV45GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContratoDadosCertameGridState"), "");
         }
         else
         {
            AV45GridState.FromXml(AV43Session.Get("PromptContratoDadosCertameGridState"), "");
         }
         AV61GXV1 = 1;
         while ( AV61GXV1 <= AV45GridState.gxTpr_Filtervalues.Count )
         {
            AV46GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV45GridState.gxTpr_Filtervalues.Item(AV61GXV1));
            if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_CODIGO") == 0 )
            {
               AV10TFContratoDadosCertame_Codigo = (int)(NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, "."));
               AV11TFContratoDadosCertame_Codigo_To = (int)(NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTRATO_CODIGO") == 0 )
            {
               AV12TFContrato_Codigo = (int)(NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, "."));
               AV13TFContrato_Codigo_To = (int)(NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV14TFContrato_Numero = AV46GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV15TFContrato_Numero_Sel = AV46GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_MODALIDADE") == 0 )
            {
               AV16TFContratoDadosCertame_Modalidade = AV46GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_MODALIDADE_SEL") == 0 )
            {
               AV17TFContratoDadosCertame_Modalidade_Sel = AV46GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_NUMERO") == 0 )
            {
               AV18TFContratoDadosCertame_Numero = (long)(NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, "."));
               AV19TFContratoDadosCertame_Numero_To = (long)(NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_SITE") == 0 )
            {
               AV20TFContratoDadosCertame_Site = AV46GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_SITE_SEL") == 0 )
            {
               AV21TFContratoDadosCertame_Site_Sel = AV46GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_UASG") == 0 )
            {
               AV22TFContratoDadosCertame_Uasg = (short)(NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Value, "."));
               AV23TFContratoDadosCertame_Uasg_To = (short)(NumberUtil.Val( AV46GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_DATA") == 0 )
            {
               AV24TFContratoDadosCertame_Data = context.localUtil.CToD( AV46GridStateFilterValue.gxTpr_Value, 2);
               AV25TFContratoDadosCertame_Data_To = context.localUtil.CToD( AV46GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_DATAHOMOLOGACAO") == 0 )
            {
               AV26TFContratoDadosCertame_DataHomologacao = context.localUtil.CToD( AV46GridStateFilterValue.gxTpr_Value, 2);
               AV27TFContratoDadosCertame_DataHomologacao_To = context.localUtil.CToD( AV46GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV46GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_DATAADJUDICACAO") == 0 )
            {
               AV28TFContratoDadosCertame_DataAdjudicacao = context.localUtil.CToD( AV46GridStateFilterValue.gxTpr_Value, 2);
               AV29TFContratoDadosCertame_DataAdjudicacao_To = context.localUtil.CToD( AV46GridStateFilterValue.gxTpr_Valueto, 2);
            }
            AV61GXV1 = (int)(AV61GXV1+1);
         }
         if ( AV45GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV47GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV45GridState.gxTpr_Dynamicfilters.Item(1));
            AV48DynamicFiltersSelector1 = AV47GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV48DynamicFiltersSelector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 )
            {
               AV49DynamicFiltersOperator1 = AV47GridStateDynamicFilter.gxTpr_Operator;
               AV50ContratoDadosCertame_Modalidade1 = AV47GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV45GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV51DynamicFiltersEnabled2 = true;
               AV47GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV45GridState.gxTpr_Dynamicfilters.Item(2));
               AV52DynamicFiltersSelector2 = AV47GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 )
               {
                  AV53DynamicFiltersOperator2 = AV47GridStateDynamicFilter.gxTpr_Operator;
                  AV54ContratoDadosCertame_Modalidade2 = AV47GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV45GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV55DynamicFiltersEnabled3 = true;
                  AV47GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV45GridState.gxTpr_Dynamicfilters.Item(3));
                  AV56DynamicFiltersSelector3 = AV47GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV56DynamicFiltersSelector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 )
                  {
                     AV57DynamicFiltersOperator3 = AV47GridStateDynamicFilter.gxTpr_Operator;
                     AV58ContratoDadosCertame_Modalidade3 = AV47GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV14TFContrato_Numero = AV30SearchTxt;
         AV15TFContrato_Numero_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV48DynamicFiltersSelector1 ,
                                              AV49DynamicFiltersOperator1 ,
                                              AV50ContratoDadosCertame_Modalidade1 ,
                                              AV51DynamicFiltersEnabled2 ,
                                              AV52DynamicFiltersSelector2 ,
                                              AV53DynamicFiltersOperator2 ,
                                              AV54ContratoDadosCertame_Modalidade2 ,
                                              AV55DynamicFiltersEnabled3 ,
                                              AV56DynamicFiltersSelector3 ,
                                              AV57DynamicFiltersOperator3 ,
                                              AV58ContratoDadosCertame_Modalidade3 ,
                                              AV10TFContratoDadosCertame_Codigo ,
                                              AV11TFContratoDadosCertame_Codigo_To ,
                                              AV12TFContrato_Codigo ,
                                              AV13TFContrato_Codigo_To ,
                                              AV15TFContrato_Numero_Sel ,
                                              AV14TFContrato_Numero ,
                                              AV17TFContratoDadosCertame_Modalidade_Sel ,
                                              AV16TFContratoDadosCertame_Modalidade ,
                                              AV18TFContratoDadosCertame_Numero ,
                                              AV19TFContratoDadosCertame_Numero_To ,
                                              AV21TFContratoDadosCertame_Site_Sel ,
                                              AV20TFContratoDadosCertame_Site ,
                                              AV22TFContratoDadosCertame_Uasg ,
                                              AV23TFContratoDadosCertame_Uasg_To ,
                                              AV24TFContratoDadosCertame_Data ,
                                              AV25TFContratoDadosCertame_Data_To ,
                                              AV26TFContratoDadosCertame_DataHomologacao ,
                                              AV27TFContratoDadosCertame_DataHomologacao_To ,
                                              AV28TFContratoDadosCertame_DataAdjudicacao ,
                                              AV29TFContratoDadosCertame_DataAdjudicacao_To ,
                                              A307ContratoDadosCertame_Modalidade ,
                                              A314ContratoDadosCertame_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A308ContratoDadosCertame_Numero ,
                                              A309ContratoDadosCertame_Site ,
                                              A310ContratoDadosCertame_Uasg ,
                                              A311ContratoDadosCertame_Data ,
                                              A312ContratoDadosCertame_DataHomologacao ,
                                              A313ContratoDadosCertame_DataAdjudicacao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.LONG,
                                              TypeConstants.LONG, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN
                                              }
         });
         lV50ContratoDadosCertame_Modalidade1 = StringUtil.Concat( StringUtil.RTrim( AV50ContratoDadosCertame_Modalidade1), "%", "");
         lV50ContratoDadosCertame_Modalidade1 = StringUtil.Concat( StringUtil.RTrim( AV50ContratoDadosCertame_Modalidade1), "%", "");
         lV54ContratoDadosCertame_Modalidade2 = StringUtil.Concat( StringUtil.RTrim( AV54ContratoDadosCertame_Modalidade2), "%", "");
         lV54ContratoDadosCertame_Modalidade2 = StringUtil.Concat( StringUtil.RTrim( AV54ContratoDadosCertame_Modalidade2), "%", "");
         lV58ContratoDadosCertame_Modalidade3 = StringUtil.Concat( StringUtil.RTrim( AV58ContratoDadosCertame_Modalidade3), "%", "");
         lV58ContratoDadosCertame_Modalidade3 = StringUtil.Concat( StringUtil.RTrim( AV58ContratoDadosCertame_Modalidade3), "%", "");
         lV14TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV14TFContrato_Numero), 20, "%");
         lV16TFContratoDadosCertame_Modalidade = StringUtil.Concat( StringUtil.RTrim( AV16TFContratoDadosCertame_Modalidade), "%", "");
         lV20TFContratoDadosCertame_Site = StringUtil.Concat( StringUtil.RTrim( AV20TFContratoDadosCertame_Site), "%", "");
         /* Using cursor P00KE2 */
         pr_default.execute(0, new Object[] {lV50ContratoDadosCertame_Modalidade1, lV50ContratoDadosCertame_Modalidade1, lV54ContratoDadosCertame_Modalidade2, lV54ContratoDadosCertame_Modalidade2, lV58ContratoDadosCertame_Modalidade3, lV58ContratoDadosCertame_Modalidade3, AV10TFContratoDadosCertame_Codigo, AV11TFContratoDadosCertame_Codigo_To, AV12TFContrato_Codigo, AV13TFContrato_Codigo_To, lV14TFContrato_Numero, AV15TFContrato_Numero_Sel, lV16TFContratoDadosCertame_Modalidade, AV17TFContratoDadosCertame_Modalidade_Sel, AV18TFContratoDadosCertame_Numero, AV19TFContratoDadosCertame_Numero_To, lV20TFContratoDadosCertame_Site, AV21TFContratoDadosCertame_Site_Sel, AV22TFContratoDadosCertame_Uasg, AV23TFContratoDadosCertame_Uasg_To, AV24TFContratoDadosCertame_Data, AV25TFContratoDadosCertame_Data_To, AV26TFContratoDadosCertame_DataHomologacao, AV27TFContratoDadosCertame_DataHomologacao_To, AV28TFContratoDadosCertame_DataAdjudicacao, AV29TFContratoDadosCertame_DataAdjudicacao_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKKE2 = false;
            A77Contrato_Numero = P00KE2_A77Contrato_Numero[0];
            A313ContratoDadosCertame_DataAdjudicacao = P00KE2_A313ContratoDadosCertame_DataAdjudicacao[0];
            n313ContratoDadosCertame_DataAdjudicacao = P00KE2_n313ContratoDadosCertame_DataAdjudicacao[0];
            A312ContratoDadosCertame_DataHomologacao = P00KE2_A312ContratoDadosCertame_DataHomologacao[0];
            n312ContratoDadosCertame_DataHomologacao = P00KE2_n312ContratoDadosCertame_DataHomologacao[0];
            A311ContratoDadosCertame_Data = P00KE2_A311ContratoDadosCertame_Data[0];
            A310ContratoDadosCertame_Uasg = P00KE2_A310ContratoDadosCertame_Uasg[0];
            n310ContratoDadosCertame_Uasg = P00KE2_n310ContratoDadosCertame_Uasg[0];
            A309ContratoDadosCertame_Site = P00KE2_A309ContratoDadosCertame_Site[0];
            n309ContratoDadosCertame_Site = P00KE2_n309ContratoDadosCertame_Site[0];
            A308ContratoDadosCertame_Numero = P00KE2_A308ContratoDadosCertame_Numero[0];
            n308ContratoDadosCertame_Numero = P00KE2_n308ContratoDadosCertame_Numero[0];
            A74Contrato_Codigo = P00KE2_A74Contrato_Codigo[0];
            A314ContratoDadosCertame_Codigo = P00KE2_A314ContratoDadosCertame_Codigo[0];
            A307ContratoDadosCertame_Modalidade = P00KE2_A307ContratoDadosCertame_Modalidade[0];
            A77Contrato_Numero = P00KE2_A77Contrato_Numero[0];
            AV42count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00KE2_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKKE2 = false;
               A74Contrato_Codigo = P00KE2_A74Contrato_Codigo[0];
               A314ContratoDadosCertame_Codigo = P00KE2_A314ContratoDadosCertame_Codigo[0];
               AV42count = (long)(AV42count+1);
               BRKKE2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV34Option = A77Contrato_Numero;
               AV35Options.Add(AV34Option, 0);
               AV40OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV42count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV35Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKE2 )
            {
               BRKKE2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATODADOSCERTAME_MODALIDADEOPTIONS' Routine */
         AV16TFContratoDadosCertame_Modalidade = AV30SearchTxt;
         AV17TFContratoDadosCertame_Modalidade_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV48DynamicFiltersSelector1 ,
                                              AV49DynamicFiltersOperator1 ,
                                              AV50ContratoDadosCertame_Modalidade1 ,
                                              AV51DynamicFiltersEnabled2 ,
                                              AV52DynamicFiltersSelector2 ,
                                              AV53DynamicFiltersOperator2 ,
                                              AV54ContratoDadosCertame_Modalidade2 ,
                                              AV55DynamicFiltersEnabled3 ,
                                              AV56DynamicFiltersSelector3 ,
                                              AV57DynamicFiltersOperator3 ,
                                              AV58ContratoDadosCertame_Modalidade3 ,
                                              AV10TFContratoDadosCertame_Codigo ,
                                              AV11TFContratoDadosCertame_Codigo_To ,
                                              AV12TFContrato_Codigo ,
                                              AV13TFContrato_Codigo_To ,
                                              AV15TFContrato_Numero_Sel ,
                                              AV14TFContrato_Numero ,
                                              AV17TFContratoDadosCertame_Modalidade_Sel ,
                                              AV16TFContratoDadosCertame_Modalidade ,
                                              AV18TFContratoDadosCertame_Numero ,
                                              AV19TFContratoDadosCertame_Numero_To ,
                                              AV21TFContratoDadosCertame_Site_Sel ,
                                              AV20TFContratoDadosCertame_Site ,
                                              AV22TFContratoDadosCertame_Uasg ,
                                              AV23TFContratoDadosCertame_Uasg_To ,
                                              AV24TFContratoDadosCertame_Data ,
                                              AV25TFContratoDadosCertame_Data_To ,
                                              AV26TFContratoDadosCertame_DataHomologacao ,
                                              AV27TFContratoDadosCertame_DataHomologacao_To ,
                                              AV28TFContratoDadosCertame_DataAdjudicacao ,
                                              AV29TFContratoDadosCertame_DataAdjudicacao_To ,
                                              A307ContratoDadosCertame_Modalidade ,
                                              A314ContratoDadosCertame_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A308ContratoDadosCertame_Numero ,
                                              A309ContratoDadosCertame_Site ,
                                              A310ContratoDadosCertame_Uasg ,
                                              A311ContratoDadosCertame_Data ,
                                              A312ContratoDadosCertame_DataHomologacao ,
                                              A313ContratoDadosCertame_DataAdjudicacao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.LONG,
                                              TypeConstants.LONG, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN
                                              }
         });
         lV50ContratoDadosCertame_Modalidade1 = StringUtil.Concat( StringUtil.RTrim( AV50ContratoDadosCertame_Modalidade1), "%", "");
         lV50ContratoDadosCertame_Modalidade1 = StringUtil.Concat( StringUtil.RTrim( AV50ContratoDadosCertame_Modalidade1), "%", "");
         lV54ContratoDadosCertame_Modalidade2 = StringUtil.Concat( StringUtil.RTrim( AV54ContratoDadosCertame_Modalidade2), "%", "");
         lV54ContratoDadosCertame_Modalidade2 = StringUtil.Concat( StringUtil.RTrim( AV54ContratoDadosCertame_Modalidade2), "%", "");
         lV58ContratoDadosCertame_Modalidade3 = StringUtil.Concat( StringUtil.RTrim( AV58ContratoDadosCertame_Modalidade3), "%", "");
         lV58ContratoDadosCertame_Modalidade3 = StringUtil.Concat( StringUtil.RTrim( AV58ContratoDadosCertame_Modalidade3), "%", "");
         lV14TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV14TFContrato_Numero), 20, "%");
         lV16TFContratoDadosCertame_Modalidade = StringUtil.Concat( StringUtil.RTrim( AV16TFContratoDadosCertame_Modalidade), "%", "");
         lV20TFContratoDadosCertame_Site = StringUtil.Concat( StringUtil.RTrim( AV20TFContratoDadosCertame_Site), "%", "");
         /* Using cursor P00KE3 */
         pr_default.execute(1, new Object[] {lV50ContratoDadosCertame_Modalidade1, lV50ContratoDadosCertame_Modalidade1, lV54ContratoDadosCertame_Modalidade2, lV54ContratoDadosCertame_Modalidade2, lV58ContratoDadosCertame_Modalidade3, lV58ContratoDadosCertame_Modalidade3, AV10TFContratoDadosCertame_Codigo, AV11TFContratoDadosCertame_Codigo_To, AV12TFContrato_Codigo, AV13TFContrato_Codigo_To, lV14TFContrato_Numero, AV15TFContrato_Numero_Sel, lV16TFContratoDadosCertame_Modalidade, AV17TFContratoDadosCertame_Modalidade_Sel, AV18TFContratoDadosCertame_Numero, AV19TFContratoDadosCertame_Numero_To, lV20TFContratoDadosCertame_Site, AV21TFContratoDadosCertame_Site_Sel, AV22TFContratoDadosCertame_Uasg, AV23TFContratoDadosCertame_Uasg_To, AV24TFContratoDadosCertame_Data, AV25TFContratoDadosCertame_Data_To, AV26TFContratoDadosCertame_DataHomologacao, AV27TFContratoDadosCertame_DataHomologacao_To, AV28TFContratoDadosCertame_DataAdjudicacao, AV29TFContratoDadosCertame_DataAdjudicacao_To});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKKE4 = false;
            A307ContratoDadosCertame_Modalidade = P00KE3_A307ContratoDadosCertame_Modalidade[0];
            A313ContratoDadosCertame_DataAdjudicacao = P00KE3_A313ContratoDadosCertame_DataAdjudicacao[0];
            n313ContratoDadosCertame_DataAdjudicacao = P00KE3_n313ContratoDadosCertame_DataAdjudicacao[0];
            A312ContratoDadosCertame_DataHomologacao = P00KE3_A312ContratoDadosCertame_DataHomologacao[0];
            n312ContratoDadosCertame_DataHomologacao = P00KE3_n312ContratoDadosCertame_DataHomologacao[0];
            A311ContratoDadosCertame_Data = P00KE3_A311ContratoDadosCertame_Data[0];
            A310ContratoDadosCertame_Uasg = P00KE3_A310ContratoDadosCertame_Uasg[0];
            n310ContratoDadosCertame_Uasg = P00KE3_n310ContratoDadosCertame_Uasg[0];
            A309ContratoDadosCertame_Site = P00KE3_A309ContratoDadosCertame_Site[0];
            n309ContratoDadosCertame_Site = P00KE3_n309ContratoDadosCertame_Site[0];
            A308ContratoDadosCertame_Numero = P00KE3_A308ContratoDadosCertame_Numero[0];
            n308ContratoDadosCertame_Numero = P00KE3_n308ContratoDadosCertame_Numero[0];
            A77Contrato_Numero = P00KE3_A77Contrato_Numero[0];
            A74Contrato_Codigo = P00KE3_A74Contrato_Codigo[0];
            A314ContratoDadosCertame_Codigo = P00KE3_A314ContratoDadosCertame_Codigo[0];
            A77Contrato_Numero = P00KE3_A77Contrato_Numero[0];
            AV42count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00KE3_A307ContratoDadosCertame_Modalidade[0], A307ContratoDadosCertame_Modalidade) == 0 ) )
            {
               BRKKE4 = false;
               A314ContratoDadosCertame_Codigo = P00KE3_A314ContratoDadosCertame_Codigo[0];
               AV42count = (long)(AV42count+1);
               BRKKE4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A307ContratoDadosCertame_Modalidade)) )
            {
               AV34Option = A307ContratoDadosCertame_Modalidade;
               AV35Options.Add(AV34Option, 0);
               AV40OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV42count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV35Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKE4 )
            {
               BRKKE4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATODADOSCERTAME_SITEOPTIONS' Routine */
         AV20TFContratoDadosCertame_Site = AV30SearchTxt;
         AV21TFContratoDadosCertame_Site_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV48DynamicFiltersSelector1 ,
                                              AV49DynamicFiltersOperator1 ,
                                              AV50ContratoDadosCertame_Modalidade1 ,
                                              AV51DynamicFiltersEnabled2 ,
                                              AV52DynamicFiltersSelector2 ,
                                              AV53DynamicFiltersOperator2 ,
                                              AV54ContratoDadosCertame_Modalidade2 ,
                                              AV55DynamicFiltersEnabled3 ,
                                              AV56DynamicFiltersSelector3 ,
                                              AV57DynamicFiltersOperator3 ,
                                              AV58ContratoDadosCertame_Modalidade3 ,
                                              AV10TFContratoDadosCertame_Codigo ,
                                              AV11TFContratoDadosCertame_Codigo_To ,
                                              AV12TFContrato_Codigo ,
                                              AV13TFContrato_Codigo_To ,
                                              AV15TFContrato_Numero_Sel ,
                                              AV14TFContrato_Numero ,
                                              AV17TFContratoDadosCertame_Modalidade_Sel ,
                                              AV16TFContratoDadosCertame_Modalidade ,
                                              AV18TFContratoDadosCertame_Numero ,
                                              AV19TFContratoDadosCertame_Numero_To ,
                                              AV21TFContratoDadosCertame_Site_Sel ,
                                              AV20TFContratoDadosCertame_Site ,
                                              AV22TFContratoDadosCertame_Uasg ,
                                              AV23TFContratoDadosCertame_Uasg_To ,
                                              AV24TFContratoDadosCertame_Data ,
                                              AV25TFContratoDadosCertame_Data_To ,
                                              AV26TFContratoDadosCertame_DataHomologacao ,
                                              AV27TFContratoDadosCertame_DataHomologacao_To ,
                                              AV28TFContratoDadosCertame_DataAdjudicacao ,
                                              AV29TFContratoDadosCertame_DataAdjudicacao_To ,
                                              A307ContratoDadosCertame_Modalidade ,
                                              A314ContratoDadosCertame_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A308ContratoDadosCertame_Numero ,
                                              A309ContratoDadosCertame_Site ,
                                              A310ContratoDadosCertame_Uasg ,
                                              A311ContratoDadosCertame_Data ,
                                              A312ContratoDadosCertame_DataHomologacao ,
                                              A313ContratoDadosCertame_DataAdjudicacao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.LONG,
                                              TypeConstants.LONG, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN
                                              }
         });
         lV50ContratoDadosCertame_Modalidade1 = StringUtil.Concat( StringUtil.RTrim( AV50ContratoDadosCertame_Modalidade1), "%", "");
         lV50ContratoDadosCertame_Modalidade1 = StringUtil.Concat( StringUtil.RTrim( AV50ContratoDadosCertame_Modalidade1), "%", "");
         lV54ContratoDadosCertame_Modalidade2 = StringUtil.Concat( StringUtil.RTrim( AV54ContratoDadosCertame_Modalidade2), "%", "");
         lV54ContratoDadosCertame_Modalidade2 = StringUtil.Concat( StringUtil.RTrim( AV54ContratoDadosCertame_Modalidade2), "%", "");
         lV58ContratoDadosCertame_Modalidade3 = StringUtil.Concat( StringUtil.RTrim( AV58ContratoDadosCertame_Modalidade3), "%", "");
         lV58ContratoDadosCertame_Modalidade3 = StringUtil.Concat( StringUtil.RTrim( AV58ContratoDadosCertame_Modalidade3), "%", "");
         lV14TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV14TFContrato_Numero), 20, "%");
         lV16TFContratoDadosCertame_Modalidade = StringUtil.Concat( StringUtil.RTrim( AV16TFContratoDadosCertame_Modalidade), "%", "");
         lV20TFContratoDadosCertame_Site = StringUtil.Concat( StringUtil.RTrim( AV20TFContratoDadosCertame_Site), "%", "");
         /* Using cursor P00KE4 */
         pr_default.execute(2, new Object[] {lV50ContratoDadosCertame_Modalidade1, lV50ContratoDadosCertame_Modalidade1, lV54ContratoDadosCertame_Modalidade2, lV54ContratoDadosCertame_Modalidade2, lV58ContratoDadosCertame_Modalidade3, lV58ContratoDadosCertame_Modalidade3, AV10TFContratoDadosCertame_Codigo, AV11TFContratoDadosCertame_Codigo_To, AV12TFContrato_Codigo, AV13TFContrato_Codigo_To, lV14TFContrato_Numero, AV15TFContrato_Numero_Sel, lV16TFContratoDadosCertame_Modalidade, AV17TFContratoDadosCertame_Modalidade_Sel, AV18TFContratoDadosCertame_Numero, AV19TFContratoDadosCertame_Numero_To, lV20TFContratoDadosCertame_Site, AV21TFContratoDadosCertame_Site_Sel, AV22TFContratoDadosCertame_Uasg, AV23TFContratoDadosCertame_Uasg_To, AV24TFContratoDadosCertame_Data, AV25TFContratoDadosCertame_Data_To, AV26TFContratoDadosCertame_DataHomologacao, AV27TFContratoDadosCertame_DataHomologacao_To, AV28TFContratoDadosCertame_DataAdjudicacao, AV29TFContratoDadosCertame_DataAdjudicacao_To});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKKE6 = false;
            A309ContratoDadosCertame_Site = P00KE4_A309ContratoDadosCertame_Site[0];
            n309ContratoDadosCertame_Site = P00KE4_n309ContratoDadosCertame_Site[0];
            A313ContratoDadosCertame_DataAdjudicacao = P00KE4_A313ContratoDadosCertame_DataAdjudicacao[0];
            n313ContratoDadosCertame_DataAdjudicacao = P00KE4_n313ContratoDadosCertame_DataAdjudicacao[0];
            A312ContratoDadosCertame_DataHomologacao = P00KE4_A312ContratoDadosCertame_DataHomologacao[0];
            n312ContratoDadosCertame_DataHomologacao = P00KE4_n312ContratoDadosCertame_DataHomologacao[0];
            A311ContratoDadosCertame_Data = P00KE4_A311ContratoDadosCertame_Data[0];
            A310ContratoDadosCertame_Uasg = P00KE4_A310ContratoDadosCertame_Uasg[0];
            n310ContratoDadosCertame_Uasg = P00KE4_n310ContratoDadosCertame_Uasg[0];
            A308ContratoDadosCertame_Numero = P00KE4_A308ContratoDadosCertame_Numero[0];
            n308ContratoDadosCertame_Numero = P00KE4_n308ContratoDadosCertame_Numero[0];
            A77Contrato_Numero = P00KE4_A77Contrato_Numero[0];
            A74Contrato_Codigo = P00KE4_A74Contrato_Codigo[0];
            A314ContratoDadosCertame_Codigo = P00KE4_A314ContratoDadosCertame_Codigo[0];
            A307ContratoDadosCertame_Modalidade = P00KE4_A307ContratoDadosCertame_Modalidade[0];
            A77Contrato_Numero = P00KE4_A77Contrato_Numero[0];
            AV42count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00KE4_A309ContratoDadosCertame_Site[0], A309ContratoDadosCertame_Site) == 0 ) )
            {
               BRKKE6 = false;
               A314ContratoDadosCertame_Codigo = P00KE4_A314ContratoDadosCertame_Codigo[0];
               AV42count = (long)(AV42count+1);
               BRKKE6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A309ContratoDadosCertame_Site)) )
            {
               AV34Option = A309ContratoDadosCertame_Site;
               AV35Options.Add(AV34Option, 0);
               AV40OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV42count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV35Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKE6 )
            {
               BRKKE6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV35Options = new GxSimpleCollection();
         AV38OptionsDesc = new GxSimpleCollection();
         AV40OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV43Session = context.GetSession();
         AV45GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV46GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV14TFContrato_Numero = "";
         AV15TFContrato_Numero_Sel = "";
         AV16TFContratoDadosCertame_Modalidade = "";
         AV17TFContratoDadosCertame_Modalidade_Sel = "";
         AV20TFContratoDadosCertame_Site = "";
         AV21TFContratoDadosCertame_Site_Sel = "";
         AV24TFContratoDadosCertame_Data = DateTime.MinValue;
         AV25TFContratoDadosCertame_Data_To = DateTime.MinValue;
         AV26TFContratoDadosCertame_DataHomologacao = DateTime.MinValue;
         AV27TFContratoDadosCertame_DataHomologacao_To = DateTime.MinValue;
         AV28TFContratoDadosCertame_DataAdjudicacao = DateTime.MinValue;
         AV29TFContratoDadosCertame_DataAdjudicacao_To = DateTime.MinValue;
         AV47GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV48DynamicFiltersSelector1 = "";
         AV50ContratoDadosCertame_Modalidade1 = "";
         AV52DynamicFiltersSelector2 = "";
         AV54ContratoDadosCertame_Modalidade2 = "";
         AV56DynamicFiltersSelector3 = "";
         AV58ContratoDadosCertame_Modalidade3 = "";
         scmdbuf = "";
         lV50ContratoDadosCertame_Modalidade1 = "";
         lV54ContratoDadosCertame_Modalidade2 = "";
         lV58ContratoDadosCertame_Modalidade3 = "";
         lV14TFContrato_Numero = "";
         lV16TFContratoDadosCertame_Modalidade = "";
         lV20TFContratoDadosCertame_Site = "";
         A307ContratoDadosCertame_Modalidade = "";
         A77Contrato_Numero = "";
         A309ContratoDadosCertame_Site = "";
         A311ContratoDadosCertame_Data = DateTime.MinValue;
         A312ContratoDadosCertame_DataHomologacao = DateTime.MinValue;
         A313ContratoDadosCertame_DataAdjudicacao = DateTime.MinValue;
         P00KE2_A77Contrato_Numero = new String[] {""} ;
         P00KE2_A313ContratoDadosCertame_DataAdjudicacao = new DateTime[] {DateTime.MinValue} ;
         P00KE2_n313ContratoDadosCertame_DataAdjudicacao = new bool[] {false} ;
         P00KE2_A312ContratoDadosCertame_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00KE2_n312ContratoDadosCertame_DataHomologacao = new bool[] {false} ;
         P00KE2_A311ContratoDadosCertame_Data = new DateTime[] {DateTime.MinValue} ;
         P00KE2_A310ContratoDadosCertame_Uasg = new short[1] ;
         P00KE2_n310ContratoDadosCertame_Uasg = new bool[] {false} ;
         P00KE2_A309ContratoDadosCertame_Site = new String[] {""} ;
         P00KE2_n309ContratoDadosCertame_Site = new bool[] {false} ;
         P00KE2_A308ContratoDadosCertame_Numero = new long[1] ;
         P00KE2_n308ContratoDadosCertame_Numero = new bool[] {false} ;
         P00KE2_A74Contrato_Codigo = new int[1] ;
         P00KE2_A314ContratoDadosCertame_Codigo = new int[1] ;
         P00KE2_A307ContratoDadosCertame_Modalidade = new String[] {""} ;
         AV34Option = "";
         P00KE3_A307ContratoDadosCertame_Modalidade = new String[] {""} ;
         P00KE3_A313ContratoDadosCertame_DataAdjudicacao = new DateTime[] {DateTime.MinValue} ;
         P00KE3_n313ContratoDadosCertame_DataAdjudicacao = new bool[] {false} ;
         P00KE3_A312ContratoDadosCertame_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00KE3_n312ContratoDadosCertame_DataHomologacao = new bool[] {false} ;
         P00KE3_A311ContratoDadosCertame_Data = new DateTime[] {DateTime.MinValue} ;
         P00KE3_A310ContratoDadosCertame_Uasg = new short[1] ;
         P00KE3_n310ContratoDadosCertame_Uasg = new bool[] {false} ;
         P00KE3_A309ContratoDadosCertame_Site = new String[] {""} ;
         P00KE3_n309ContratoDadosCertame_Site = new bool[] {false} ;
         P00KE3_A308ContratoDadosCertame_Numero = new long[1] ;
         P00KE3_n308ContratoDadosCertame_Numero = new bool[] {false} ;
         P00KE3_A77Contrato_Numero = new String[] {""} ;
         P00KE3_A74Contrato_Codigo = new int[1] ;
         P00KE3_A314ContratoDadosCertame_Codigo = new int[1] ;
         P00KE4_A309ContratoDadosCertame_Site = new String[] {""} ;
         P00KE4_n309ContratoDadosCertame_Site = new bool[] {false} ;
         P00KE4_A313ContratoDadosCertame_DataAdjudicacao = new DateTime[] {DateTime.MinValue} ;
         P00KE4_n313ContratoDadosCertame_DataAdjudicacao = new bool[] {false} ;
         P00KE4_A312ContratoDadosCertame_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00KE4_n312ContratoDadosCertame_DataHomologacao = new bool[] {false} ;
         P00KE4_A311ContratoDadosCertame_Data = new DateTime[] {DateTime.MinValue} ;
         P00KE4_A310ContratoDadosCertame_Uasg = new short[1] ;
         P00KE4_n310ContratoDadosCertame_Uasg = new bool[] {false} ;
         P00KE4_A308ContratoDadosCertame_Numero = new long[1] ;
         P00KE4_n308ContratoDadosCertame_Numero = new bool[] {false} ;
         P00KE4_A77Contrato_Numero = new String[] {""} ;
         P00KE4_A74Contrato_Codigo = new int[1] ;
         P00KE4_A314ContratoDadosCertame_Codigo = new int[1] ;
         P00KE4_A307ContratoDadosCertame_Modalidade = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontratodadoscertamefilterdata__default(),
            new Object[][] {
                new Object[] {
               P00KE2_A77Contrato_Numero, P00KE2_A313ContratoDadosCertame_DataAdjudicacao, P00KE2_n313ContratoDadosCertame_DataAdjudicacao, P00KE2_A312ContratoDadosCertame_DataHomologacao, P00KE2_n312ContratoDadosCertame_DataHomologacao, P00KE2_A311ContratoDadosCertame_Data, P00KE2_A310ContratoDadosCertame_Uasg, P00KE2_n310ContratoDadosCertame_Uasg, P00KE2_A309ContratoDadosCertame_Site, P00KE2_n309ContratoDadosCertame_Site,
               P00KE2_A308ContratoDadosCertame_Numero, P00KE2_n308ContratoDadosCertame_Numero, P00KE2_A74Contrato_Codigo, P00KE2_A314ContratoDadosCertame_Codigo, P00KE2_A307ContratoDadosCertame_Modalidade
               }
               , new Object[] {
               P00KE3_A307ContratoDadosCertame_Modalidade, P00KE3_A313ContratoDadosCertame_DataAdjudicacao, P00KE3_n313ContratoDadosCertame_DataAdjudicacao, P00KE3_A312ContratoDadosCertame_DataHomologacao, P00KE3_n312ContratoDadosCertame_DataHomologacao, P00KE3_A311ContratoDadosCertame_Data, P00KE3_A310ContratoDadosCertame_Uasg, P00KE3_n310ContratoDadosCertame_Uasg, P00KE3_A309ContratoDadosCertame_Site, P00KE3_n309ContratoDadosCertame_Site,
               P00KE3_A308ContratoDadosCertame_Numero, P00KE3_n308ContratoDadosCertame_Numero, P00KE3_A77Contrato_Numero, P00KE3_A74Contrato_Codigo, P00KE3_A314ContratoDadosCertame_Codigo
               }
               , new Object[] {
               P00KE4_A309ContratoDadosCertame_Site, P00KE4_n309ContratoDadosCertame_Site, P00KE4_A313ContratoDadosCertame_DataAdjudicacao, P00KE4_n313ContratoDadosCertame_DataAdjudicacao, P00KE4_A312ContratoDadosCertame_DataHomologacao, P00KE4_n312ContratoDadosCertame_DataHomologacao, P00KE4_A311ContratoDadosCertame_Data, P00KE4_A310ContratoDadosCertame_Uasg, P00KE4_n310ContratoDadosCertame_Uasg, P00KE4_A308ContratoDadosCertame_Numero,
               P00KE4_n308ContratoDadosCertame_Numero, P00KE4_A77Contrato_Numero, P00KE4_A74Contrato_Codigo, P00KE4_A314ContratoDadosCertame_Codigo, P00KE4_A307ContratoDadosCertame_Modalidade
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV22TFContratoDadosCertame_Uasg ;
      private short AV23TFContratoDadosCertame_Uasg_To ;
      private short AV49DynamicFiltersOperator1 ;
      private short AV53DynamicFiltersOperator2 ;
      private short AV57DynamicFiltersOperator3 ;
      private short A310ContratoDadosCertame_Uasg ;
      private int AV61GXV1 ;
      private int AV10TFContratoDadosCertame_Codigo ;
      private int AV11TFContratoDadosCertame_Codigo_To ;
      private int AV12TFContrato_Codigo ;
      private int AV13TFContrato_Codigo_To ;
      private int A314ContratoDadosCertame_Codigo ;
      private int A74Contrato_Codigo ;
      private long AV18TFContratoDadosCertame_Numero ;
      private long AV19TFContratoDadosCertame_Numero_To ;
      private long A308ContratoDadosCertame_Numero ;
      private long AV42count ;
      private String AV14TFContrato_Numero ;
      private String AV15TFContrato_Numero_Sel ;
      private String scmdbuf ;
      private String lV14TFContrato_Numero ;
      private String A77Contrato_Numero ;
      private DateTime AV24TFContratoDadosCertame_Data ;
      private DateTime AV25TFContratoDadosCertame_Data_To ;
      private DateTime AV26TFContratoDadosCertame_DataHomologacao ;
      private DateTime AV27TFContratoDadosCertame_DataHomologacao_To ;
      private DateTime AV28TFContratoDadosCertame_DataAdjudicacao ;
      private DateTime AV29TFContratoDadosCertame_DataAdjudicacao_To ;
      private DateTime A311ContratoDadosCertame_Data ;
      private DateTime A312ContratoDadosCertame_DataHomologacao ;
      private DateTime A313ContratoDadosCertame_DataAdjudicacao ;
      private bool returnInSub ;
      private bool AV51DynamicFiltersEnabled2 ;
      private bool AV55DynamicFiltersEnabled3 ;
      private bool BRKKE2 ;
      private bool n313ContratoDadosCertame_DataAdjudicacao ;
      private bool n312ContratoDadosCertame_DataHomologacao ;
      private bool n310ContratoDadosCertame_Uasg ;
      private bool n309ContratoDadosCertame_Site ;
      private bool n308ContratoDadosCertame_Numero ;
      private bool BRKKE4 ;
      private bool BRKKE6 ;
      private String AV41OptionIndexesJson ;
      private String AV36OptionsJson ;
      private String AV39OptionsDescJson ;
      private String AV32DDOName ;
      private String AV30SearchTxt ;
      private String AV31SearchTxtTo ;
      private String AV16TFContratoDadosCertame_Modalidade ;
      private String AV17TFContratoDadosCertame_Modalidade_Sel ;
      private String AV20TFContratoDadosCertame_Site ;
      private String AV21TFContratoDadosCertame_Site_Sel ;
      private String AV48DynamicFiltersSelector1 ;
      private String AV50ContratoDadosCertame_Modalidade1 ;
      private String AV52DynamicFiltersSelector2 ;
      private String AV54ContratoDadosCertame_Modalidade2 ;
      private String AV56DynamicFiltersSelector3 ;
      private String AV58ContratoDadosCertame_Modalidade3 ;
      private String lV50ContratoDadosCertame_Modalidade1 ;
      private String lV54ContratoDadosCertame_Modalidade2 ;
      private String lV58ContratoDadosCertame_Modalidade3 ;
      private String lV16TFContratoDadosCertame_Modalidade ;
      private String lV20TFContratoDadosCertame_Site ;
      private String A307ContratoDadosCertame_Modalidade ;
      private String A309ContratoDadosCertame_Site ;
      private String AV34Option ;
      private IGxSession AV43Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00KE2_A77Contrato_Numero ;
      private DateTime[] P00KE2_A313ContratoDadosCertame_DataAdjudicacao ;
      private bool[] P00KE2_n313ContratoDadosCertame_DataAdjudicacao ;
      private DateTime[] P00KE2_A312ContratoDadosCertame_DataHomologacao ;
      private bool[] P00KE2_n312ContratoDadosCertame_DataHomologacao ;
      private DateTime[] P00KE2_A311ContratoDadosCertame_Data ;
      private short[] P00KE2_A310ContratoDadosCertame_Uasg ;
      private bool[] P00KE2_n310ContratoDadosCertame_Uasg ;
      private String[] P00KE2_A309ContratoDadosCertame_Site ;
      private bool[] P00KE2_n309ContratoDadosCertame_Site ;
      private long[] P00KE2_A308ContratoDadosCertame_Numero ;
      private bool[] P00KE2_n308ContratoDadosCertame_Numero ;
      private int[] P00KE2_A74Contrato_Codigo ;
      private int[] P00KE2_A314ContratoDadosCertame_Codigo ;
      private String[] P00KE2_A307ContratoDadosCertame_Modalidade ;
      private String[] P00KE3_A307ContratoDadosCertame_Modalidade ;
      private DateTime[] P00KE3_A313ContratoDadosCertame_DataAdjudicacao ;
      private bool[] P00KE3_n313ContratoDadosCertame_DataAdjudicacao ;
      private DateTime[] P00KE3_A312ContratoDadosCertame_DataHomologacao ;
      private bool[] P00KE3_n312ContratoDadosCertame_DataHomologacao ;
      private DateTime[] P00KE3_A311ContratoDadosCertame_Data ;
      private short[] P00KE3_A310ContratoDadosCertame_Uasg ;
      private bool[] P00KE3_n310ContratoDadosCertame_Uasg ;
      private String[] P00KE3_A309ContratoDadosCertame_Site ;
      private bool[] P00KE3_n309ContratoDadosCertame_Site ;
      private long[] P00KE3_A308ContratoDadosCertame_Numero ;
      private bool[] P00KE3_n308ContratoDadosCertame_Numero ;
      private String[] P00KE3_A77Contrato_Numero ;
      private int[] P00KE3_A74Contrato_Codigo ;
      private int[] P00KE3_A314ContratoDadosCertame_Codigo ;
      private String[] P00KE4_A309ContratoDadosCertame_Site ;
      private bool[] P00KE4_n309ContratoDadosCertame_Site ;
      private DateTime[] P00KE4_A313ContratoDadosCertame_DataAdjudicacao ;
      private bool[] P00KE4_n313ContratoDadosCertame_DataAdjudicacao ;
      private DateTime[] P00KE4_A312ContratoDadosCertame_DataHomologacao ;
      private bool[] P00KE4_n312ContratoDadosCertame_DataHomologacao ;
      private DateTime[] P00KE4_A311ContratoDadosCertame_Data ;
      private short[] P00KE4_A310ContratoDadosCertame_Uasg ;
      private bool[] P00KE4_n310ContratoDadosCertame_Uasg ;
      private long[] P00KE4_A308ContratoDadosCertame_Numero ;
      private bool[] P00KE4_n308ContratoDadosCertame_Numero ;
      private String[] P00KE4_A77Contrato_Numero ;
      private int[] P00KE4_A74Contrato_Codigo ;
      private int[] P00KE4_A314ContratoDadosCertame_Codigo ;
      private String[] P00KE4_A307ContratoDadosCertame_Modalidade ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV35Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV38OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV40OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV45GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV46GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV47GridStateDynamicFilter ;
   }

   public class getpromptcontratodadoscertamefilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00KE2( IGxContext context ,
                                             String AV48DynamicFiltersSelector1 ,
                                             short AV49DynamicFiltersOperator1 ,
                                             String AV50ContratoDadosCertame_Modalidade1 ,
                                             bool AV51DynamicFiltersEnabled2 ,
                                             String AV52DynamicFiltersSelector2 ,
                                             short AV53DynamicFiltersOperator2 ,
                                             String AV54ContratoDadosCertame_Modalidade2 ,
                                             bool AV55DynamicFiltersEnabled3 ,
                                             String AV56DynamicFiltersSelector3 ,
                                             short AV57DynamicFiltersOperator3 ,
                                             String AV58ContratoDadosCertame_Modalidade3 ,
                                             int AV10TFContratoDadosCertame_Codigo ,
                                             int AV11TFContratoDadosCertame_Codigo_To ,
                                             int AV12TFContrato_Codigo ,
                                             int AV13TFContrato_Codigo_To ,
                                             String AV15TFContrato_Numero_Sel ,
                                             String AV14TFContrato_Numero ,
                                             String AV17TFContratoDadosCertame_Modalidade_Sel ,
                                             String AV16TFContratoDadosCertame_Modalidade ,
                                             long AV18TFContratoDadosCertame_Numero ,
                                             long AV19TFContratoDadosCertame_Numero_To ,
                                             String AV21TFContratoDadosCertame_Site_Sel ,
                                             String AV20TFContratoDadosCertame_Site ,
                                             short AV22TFContratoDadosCertame_Uasg ,
                                             short AV23TFContratoDadosCertame_Uasg_To ,
                                             DateTime AV24TFContratoDadosCertame_Data ,
                                             DateTime AV25TFContratoDadosCertame_Data_To ,
                                             DateTime AV26TFContratoDadosCertame_DataHomologacao ,
                                             DateTime AV27TFContratoDadosCertame_DataHomologacao_To ,
                                             DateTime AV28TFContratoDadosCertame_DataAdjudicacao ,
                                             DateTime AV29TFContratoDadosCertame_DataAdjudicacao_To ,
                                             String A307ContratoDadosCertame_Modalidade ,
                                             int A314ContratoDadosCertame_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             long A308ContratoDadosCertame_Numero ,
                                             String A309ContratoDadosCertame_Site ,
                                             short A310ContratoDadosCertame_Uasg ,
                                             DateTime A311ContratoDadosCertame_Data ,
                                             DateTime A312ContratoDadosCertame_DataHomologacao ,
                                             DateTime A313ContratoDadosCertame_DataAdjudicacao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [26] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T2.[Contrato_Numero], T1.[ContratoDadosCertame_DataAdjudicacao], T1.[ContratoDadosCertame_DataHomologacao], T1.[ContratoDadosCertame_Data], T1.[ContratoDadosCertame_Uasg], T1.[ContratoDadosCertame_Site], T1.[ContratoDadosCertame_Numero], T1.[Contrato_Codigo], T1.[ContratoDadosCertame_Codigo], T1.[ContratoDadosCertame_Modalidade] FROM ([ContratoDadosCertame] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV48DynamicFiltersSelector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV49DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50ContratoDadosCertame_Modalidade1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV50ContratoDadosCertame_Modalidade1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV50ContratoDadosCertame_Modalidade1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV48DynamicFiltersSelector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV49DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50ContratoDadosCertame_Modalidade1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV50ContratoDadosCertame_Modalidade1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV50ContratoDadosCertame_Modalidade1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV53DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ContratoDadosCertame_Modalidade2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV54ContratoDadosCertame_Modalidade2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV54ContratoDadosCertame_Modalidade2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV53DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ContratoDadosCertame_Modalidade2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV54ContratoDadosCertame_Modalidade2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV54ContratoDadosCertame_Modalidade2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV55DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV56DynamicFiltersSelector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV57DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58ContratoDadosCertame_Modalidade3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV58ContratoDadosCertame_Modalidade3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV58ContratoDadosCertame_Modalidade3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV55DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV56DynamicFiltersSelector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV57DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58ContratoDadosCertame_Modalidade3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV58ContratoDadosCertame_Modalidade3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV58ContratoDadosCertame_Modalidade3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV10TFContratoDadosCertame_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Codigo] >= @AV10TFContratoDadosCertame_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Codigo] >= @AV10TFContratoDadosCertame_Codigo)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV11TFContratoDadosCertame_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Codigo] <= @AV11TFContratoDadosCertame_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Codigo] <= @AV11TFContratoDadosCertame_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (0==AV12TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV13TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoDadosCertame_Modalidade_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratoDadosCertame_Modalidade)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV16TFContratoDadosCertame_Modalidade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV16TFContratoDadosCertame_Modalidade)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoDadosCertame_Modalidade_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] = @AV17TFContratoDadosCertame_Modalidade_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] = @AV17TFContratoDadosCertame_Modalidade_Sel)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (0==AV18TFContratoDadosCertame_Numero) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] >= @AV18TFContratoDadosCertame_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] >= @AV18TFContratoDadosCertame_Numero)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (0==AV19TFContratoDadosCertame_Numero_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] <= @AV19TFContratoDadosCertame_Numero_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] <= @AV19TFContratoDadosCertame_Numero_To)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoDadosCertame_Site_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratoDadosCertame_Site)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Site] like @lV20TFContratoDadosCertame_Site)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Site] like @lV20TFContratoDadosCertame_Site)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoDadosCertame_Site_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Site] = @AV21TFContratoDadosCertame_Site_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Site] = @AV21TFContratoDadosCertame_Site_Sel)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (0==AV22TFContratoDadosCertame_Uasg) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Uasg] >= @AV22TFContratoDadosCertame_Uasg)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Uasg] >= @AV22TFContratoDadosCertame_Uasg)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (0==AV23TFContratoDadosCertame_Uasg_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Uasg] <= @AV23TFContratoDadosCertame_Uasg_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Uasg] <= @AV23TFContratoDadosCertame_Uasg_To)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFContratoDadosCertame_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV24TFContratoDadosCertame_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV24TFContratoDadosCertame_Data)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV25TFContratoDadosCertame_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV25TFContratoDadosCertame_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV25TFContratoDadosCertame_Data_To)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (DateTime.MinValue==AV26TFContratoDadosCertame_DataHomologacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataHomologacao] >= @AV26TFContratoDadosCertame_DataHomologacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataHomologacao] >= @AV26TFContratoDadosCertame_DataHomologacao)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV27TFContratoDadosCertame_DataHomologacao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataHomologacao] <= @AV27TFContratoDadosCertame_DataHomologacao_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataHomologacao] <= @AV27TFContratoDadosCertame_DataHomologacao_To)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV28TFContratoDadosCertame_DataAdjudicacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataAdjudicacao] >= @AV28TFContratoDadosCertame_DataAdjudicacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataAdjudicacao] >= @AV28TFContratoDadosCertame_DataAdjudicacao)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( ! (DateTime.MinValue==AV29TFContratoDadosCertame_DataAdjudicacao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataAdjudicacao] <= @AV29TFContratoDadosCertame_DataAdjudicacao_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataAdjudicacao] <= @AV29TFContratoDadosCertame_DataAdjudicacao_To)";
            }
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00KE3( IGxContext context ,
                                             String AV48DynamicFiltersSelector1 ,
                                             short AV49DynamicFiltersOperator1 ,
                                             String AV50ContratoDadosCertame_Modalidade1 ,
                                             bool AV51DynamicFiltersEnabled2 ,
                                             String AV52DynamicFiltersSelector2 ,
                                             short AV53DynamicFiltersOperator2 ,
                                             String AV54ContratoDadosCertame_Modalidade2 ,
                                             bool AV55DynamicFiltersEnabled3 ,
                                             String AV56DynamicFiltersSelector3 ,
                                             short AV57DynamicFiltersOperator3 ,
                                             String AV58ContratoDadosCertame_Modalidade3 ,
                                             int AV10TFContratoDadosCertame_Codigo ,
                                             int AV11TFContratoDadosCertame_Codigo_To ,
                                             int AV12TFContrato_Codigo ,
                                             int AV13TFContrato_Codigo_To ,
                                             String AV15TFContrato_Numero_Sel ,
                                             String AV14TFContrato_Numero ,
                                             String AV17TFContratoDadosCertame_Modalidade_Sel ,
                                             String AV16TFContratoDadosCertame_Modalidade ,
                                             long AV18TFContratoDadosCertame_Numero ,
                                             long AV19TFContratoDadosCertame_Numero_To ,
                                             String AV21TFContratoDadosCertame_Site_Sel ,
                                             String AV20TFContratoDadosCertame_Site ,
                                             short AV22TFContratoDadosCertame_Uasg ,
                                             short AV23TFContratoDadosCertame_Uasg_To ,
                                             DateTime AV24TFContratoDadosCertame_Data ,
                                             DateTime AV25TFContratoDadosCertame_Data_To ,
                                             DateTime AV26TFContratoDadosCertame_DataHomologacao ,
                                             DateTime AV27TFContratoDadosCertame_DataHomologacao_To ,
                                             DateTime AV28TFContratoDadosCertame_DataAdjudicacao ,
                                             DateTime AV29TFContratoDadosCertame_DataAdjudicacao_To ,
                                             String A307ContratoDadosCertame_Modalidade ,
                                             int A314ContratoDadosCertame_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             long A308ContratoDadosCertame_Numero ,
                                             String A309ContratoDadosCertame_Site ,
                                             short A310ContratoDadosCertame_Uasg ,
                                             DateTime A311ContratoDadosCertame_Data ,
                                             DateTime A312ContratoDadosCertame_DataHomologacao ,
                                             DateTime A313ContratoDadosCertame_DataAdjudicacao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [26] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoDadosCertame_Modalidade], T1.[ContratoDadosCertame_DataAdjudicacao], T1.[ContratoDadosCertame_DataHomologacao], T1.[ContratoDadosCertame_Data], T1.[ContratoDadosCertame_Uasg], T1.[ContratoDadosCertame_Site], T1.[ContratoDadosCertame_Numero], T2.[Contrato_Numero], T1.[Contrato_Codigo], T1.[ContratoDadosCertame_Codigo] FROM ([ContratoDadosCertame] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV48DynamicFiltersSelector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV49DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50ContratoDadosCertame_Modalidade1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV50ContratoDadosCertame_Modalidade1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV50ContratoDadosCertame_Modalidade1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV48DynamicFiltersSelector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV49DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50ContratoDadosCertame_Modalidade1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV50ContratoDadosCertame_Modalidade1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV50ContratoDadosCertame_Modalidade1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV53DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ContratoDadosCertame_Modalidade2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV54ContratoDadosCertame_Modalidade2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV54ContratoDadosCertame_Modalidade2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV53DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ContratoDadosCertame_Modalidade2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV54ContratoDadosCertame_Modalidade2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV54ContratoDadosCertame_Modalidade2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV55DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV56DynamicFiltersSelector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV57DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58ContratoDadosCertame_Modalidade3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV58ContratoDadosCertame_Modalidade3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV58ContratoDadosCertame_Modalidade3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV55DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV56DynamicFiltersSelector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV57DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58ContratoDadosCertame_Modalidade3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV58ContratoDadosCertame_Modalidade3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV58ContratoDadosCertame_Modalidade3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV10TFContratoDadosCertame_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Codigo] >= @AV10TFContratoDadosCertame_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Codigo] >= @AV10TFContratoDadosCertame_Codigo)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (0==AV11TFContratoDadosCertame_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Codigo] <= @AV11TFContratoDadosCertame_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Codigo] <= @AV11TFContratoDadosCertame_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! (0==AV12TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! (0==AV13TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoDadosCertame_Modalidade_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratoDadosCertame_Modalidade)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV16TFContratoDadosCertame_Modalidade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV16TFContratoDadosCertame_Modalidade)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoDadosCertame_Modalidade_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] = @AV17TFContratoDadosCertame_Modalidade_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] = @AV17TFContratoDadosCertame_Modalidade_Sel)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (0==AV18TFContratoDadosCertame_Numero) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] >= @AV18TFContratoDadosCertame_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] >= @AV18TFContratoDadosCertame_Numero)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV19TFContratoDadosCertame_Numero_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] <= @AV19TFContratoDadosCertame_Numero_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] <= @AV19TFContratoDadosCertame_Numero_To)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoDadosCertame_Site_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratoDadosCertame_Site)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Site] like @lV20TFContratoDadosCertame_Site)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Site] like @lV20TFContratoDadosCertame_Site)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoDadosCertame_Site_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Site] = @AV21TFContratoDadosCertame_Site_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Site] = @AV21TFContratoDadosCertame_Site_Sel)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (0==AV22TFContratoDadosCertame_Uasg) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Uasg] >= @AV22TFContratoDadosCertame_Uasg)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Uasg] >= @AV22TFContratoDadosCertame_Uasg)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (0==AV23TFContratoDadosCertame_Uasg_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Uasg] <= @AV23TFContratoDadosCertame_Uasg_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Uasg] <= @AV23TFContratoDadosCertame_Uasg_To)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFContratoDadosCertame_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV24TFContratoDadosCertame_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV24TFContratoDadosCertame_Data)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV25TFContratoDadosCertame_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV25TFContratoDadosCertame_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV25TFContratoDadosCertame_Data_To)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! (DateTime.MinValue==AV26TFContratoDadosCertame_DataHomologacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataHomologacao] >= @AV26TFContratoDadosCertame_DataHomologacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataHomologacao] >= @AV26TFContratoDadosCertame_DataHomologacao)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV27TFContratoDadosCertame_DataHomologacao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataHomologacao] <= @AV27TFContratoDadosCertame_DataHomologacao_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataHomologacao] <= @AV27TFContratoDadosCertame_DataHomologacao_To)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV28TFContratoDadosCertame_DataAdjudicacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataAdjudicacao] >= @AV28TFContratoDadosCertame_DataAdjudicacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataAdjudicacao] >= @AV28TFContratoDadosCertame_DataAdjudicacao)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( ! (DateTime.MinValue==AV29TFContratoDadosCertame_DataAdjudicacao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataAdjudicacao] <= @AV29TFContratoDadosCertame_DataAdjudicacao_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataAdjudicacao] <= @AV29TFContratoDadosCertame_DataAdjudicacao_To)";
            }
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoDadosCertame_Modalidade]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00KE4( IGxContext context ,
                                             String AV48DynamicFiltersSelector1 ,
                                             short AV49DynamicFiltersOperator1 ,
                                             String AV50ContratoDadosCertame_Modalidade1 ,
                                             bool AV51DynamicFiltersEnabled2 ,
                                             String AV52DynamicFiltersSelector2 ,
                                             short AV53DynamicFiltersOperator2 ,
                                             String AV54ContratoDadosCertame_Modalidade2 ,
                                             bool AV55DynamicFiltersEnabled3 ,
                                             String AV56DynamicFiltersSelector3 ,
                                             short AV57DynamicFiltersOperator3 ,
                                             String AV58ContratoDadosCertame_Modalidade3 ,
                                             int AV10TFContratoDadosCertame_Codigo ,
                                             int AV11TFContratoDadosCertame_Codigo_To ,
                                             int AV12TFContrato_Codigo ,
                                             int AV13TFContrato_Codigo_To ,
                                             String AV15TFContrato_Numero_Sel ,
                                             String AV14TFContrato_Numero ,
                                             String AV17TFContratoDadosCertame_Modalidade_Sel ,
                                             String AV16TFContratoDadosCertame_Modalidade ,
                                             long AV18TFContratoDadosCertame_Numero ,
                                             long AV19TFContratoDadosCertame_Numero_To ,
                                             String AV21TFContratoDadosCertame_Site_Sel ,
                                             String AV20TFContratoDadosCertame_Site ,
                                             short AV22TFContratoDadosCertame_Uasg ,
                                             short AV23TFContratoDadosCertame_Uasg_To ,
                                             DateTime AV24TFContratoDadosCertame_Data ,
                                             DateTime AV25TFContratoDadosCertame_Data_To ,
                                             DateTime AV26TFContratoDadosCertame_DataHomologacao ,
                                             DateTime AV27TFContratoDadosCertame_DataHomologacao_To ,
                                             DateTime AV28TFContratoDadosCertame_DataAdjudicacao ,
                                             DateTime AV29TFContratoDadosCertame_DataAdjudicacao_To ,
                                             String A307ContratoDadosCertame_Modalidade ,
                                             int A314ContratoDadosCertame_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             long A308ContratoDadosCertame_Numero ,
                                             String A309ContratoDadosCertame_Site ,
                                             short A310ContratoDadosCertame_Uasg ,
                                             DateTime A311ContratoDadosCertame_Data ,
                                             DateTime A312ContratoDadosCertame_DataHomologacao ,
                                             DateTime A313ContratoDadosCertame_DataAdjudicacao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [26] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoDadosCertame_Site], T1.[ContratoDadosCertame_DataAdjudicacao], T1.[ContratoDadosCertame_DataHomologacao], T1.[ContratoDadosCertame_Data], T1.[ContratoDadosCertame_Uasg], T1.[ContratoDadosCertame_Numero], T2.[Contrato_Numero], T1.[Contrato_Codigo], T1.[ContratoDadosCertame_Codigo], T1.[ContratoDadosCertame_Modalidade] FROM ([ContratoDadosCertame] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV48DynamicFiltersSelector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV49DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50ContratoDadosCertame_Modalidade1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV50ContratoDadosCertame_Modalidade1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV50ContratoDadosCertame_Modalidade1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV48DynamicFiltersSelector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV49DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50ContratoDadosCertame_Modalidade1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV50ContratoDadosCertame_Modalidade1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV50ContratoDadosCertame_Modalidade1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV53DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ContratoDadosCertame_Modalidade2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV54ContratoDadosCertame_Modalidade2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV54ContratoDadosCertame_Modalidade2)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV51DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV53DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ContratoDadosCertame_Modalidade2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV54ContratoDadosCertame_Modalidade2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV54ContratoDadosCertame_Modalidade2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV55DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV56DynamicFiltersSelector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV57DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58ContratoDadosCertame_Modalidade3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV58ContratoDadosCertame_Modalidade3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV58ContratoDadosCertame_Modalidade3)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV55DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV56DynamicFiltersSelector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV57DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58ContratoDadosCertame_Modalidade3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV58ContratoDadosCertame_Modalidade3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV58ContratoDadosCertame_Modalidade3)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! (0==AV10TFContratoDadosCertame_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Codigo] >= @AV10TFContratoDadosCertame_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Codigo] >= @AV10TFContratoDadosCertame_Codigo)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! (0==AV11TFContratoDadosCertame_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Codigo] <= @AV11TFContratoDadosCertame_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Codigo] <= @AV11TFContratoDadosCertame_Codigo_To)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ! (0==AV12TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! (0==AV13TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoDadosCertame_Modalidade_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratoDadosCertame_Modalidade)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV16TFContratoDadosCertame_Modalidade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV16TFContratoDadosCertame_Modalidade)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoDadosCertame_Modalidade_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] = @AV17TFContratoDadosCertame_Modalidade_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] = @AV17TFContratoDadosCertame_Modalidade_Sel)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! (0==AV18TFContratoDadosCertame_Numero) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] >= @AV18TFContratoDadosCertame_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] >= @AV18TFContratoDadosCertame_Numero)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (0==AV19TFContratoDadosCertame_Numero_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] <= @AV19TFContratoDadosCertame_Numero_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] <= @AV19TFContratoDadosCertame_Numero_To)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoDadosCertame_Site_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratoDadosCertame_Site)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Site] like @lV20TFContratoDadosCertame_Site)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Site] like @lV20TFContratoDadosCertame_Site)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoDadosCertame_Site_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Site] = @AV21TFContratoDadosCertame_Site_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Site] = @AV21TFContratoDadosCertame_Site_Sel)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! (0==AV22TFContratoDadosCertame_Uasg) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Uasg] >= @AV22TFContratoDadosCertame_Uasg)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Uasg] >= @AV22TFContratoDadosCertame_Uasg)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! (0==AV23TFContratoDadosCertame_Uasg_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Uasg] <= @AV23TFContratoDadosCertame_Uasg_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Uasg] <= @AV23TFContratoDadosCertame_Uasg_To)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFContratoDadosCertame_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV24TFContratoDadosCertame_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV24TFContratoDadosCertame_Data)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV25TFContratoDadosCertame_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV25TFContratoDadosCertame_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV25TFContratoDadosCertame_Data_To)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( ! (DateTime.MinValue==AV26TFContratoDadosCertame_DataHomologacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataHomologacao] >= @AV26TFContratoDadosCertame_DataHomologacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataHomologacao] >= @AV26TFContratoDadosCertame_DataHomologacao)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV27TFContratoDadosCertame_DataHomologacao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataHomologacao] <= @AV27TFContratoDadosCertame_DataHomologacao_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataHomologacao] <= @AV27TFContratoDadosCertame_DataHomologacao_To)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV28TFContratoDadosCertame_DataAdjudicacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataAdjudicacao] >= @AV28TFContratoDadosCertame_DataAdjudicacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataAdjudicacao] >= @AV28TFContratoDadosCertame_DataAdjudicacao)";
            }
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( ! (DateTime.MinValue==AV29TFContratoDadosCertame_DataAdjudicacao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataAdjudicacao] <= @AV29TFContratoDadosCertame_DataAdjudicacao_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataAdjudicacao] <= @AV29TFContratoDadosCertame_DataAdjudicacao_To)";
            }
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoDadosCertame_Site]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00KE2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (long)dynConstraints[19] , (long)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (short)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] , (String)dynConstraints[34] , (long)dynConstraints[35] , (String)dynConstraints[36] , (short)dynConstraints[37] , (DateTime)dynConstraints[38] , (DateTime)dynConstraints[39] , (DateTime)dynConstraints[40] );
               case 1 :
                     return conditional_P00KE3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (long)dynConstraints[19] , (long)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (short)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] , (String)dynConstraints[34] , (long)dynConstraints[35] , (String)dynConstraints[36] , (short)dynConstraints[37] , (DateTime)dynConstraints[38] , (DateTime)dynConstraints[39] , (DateTime)dynConstraints[40] );
               case 2 :
                     return conditional_P00KE4(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (long)dynConstraints[19] , (long)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (short)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] , (String)dynConstraints[34] , (long)dynConstraints[35] , (String)dynConstraints[36] , (short)dynConstraints[37] , (DateTime)dynConstraints[38] , (DateTime)dynConstraints[39] , (DateTime)dynConstraints[40] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00KE2 ;
          prmP00KE2 = new Object[] {
          new Object[] {"@lV50ContratoDadosCertame_Modalidade1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV50ContratoDadosCertame_Modalidade1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV54ContratoDadosCertame_Modalidade2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV54ContratoDadosCertame_Modalidade2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV58ContratoDadosCertame_Modalidade3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV58ContratoDadosCertame_Modalidade3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV10TFContratoDadosCertame_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoDadosCertame_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV15TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV16TFContratoDadosCertame_Modalidade",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV17TFContratoDadosCertame_Modalidade_Sel",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV18TFContratoDadosCertame_Numero",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV19TFContratoDadosCertame_Numero_To",SqlDbType.Decimal,10,0} ,
          new Object[] {"@lV20TFContratoDadosCertame_Site",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV21TFContratoDadosCertame_Site_Sel",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV22TFContratoDadosCertame_Uasg",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV23TFContratoDadosCertame_Uasg_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV24TFContratoDadosCertame_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25TFContratoDadosCertame_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV26TFContratoDadosCertame_DataHomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV27TFContratoDadosCertame_DataHomologacao_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV28TFContratoDadosCertame_DataAdjudicacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV29TFContratoDadosCertame_DataAdjudicacao_To",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmP00KE3 ;
          prmP00KE3 = new Object[] {
          new Object[] {"@lV50ContratoDadosCertame_Modalidade1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV50ContratoDadosCertame_Modalidade1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV54ContratoDadosCertame_Modalidade2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV54ContratoDadosCertame_Modalidade2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV58ContratoDadosCertame_Modalidade3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV58ContratoDadosCertame_Modalidade3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV10TFContratoDadosCertame_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoDadosCertame_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV15TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV16TFContratoDadosCertame_Modalidade",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV17TFContratoDadosCertame_Modalidade_Sel",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV18TFContratoDadosCertame_Numero",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV19TFContratoDadosCertame_Numero_To",SqlDbType.Decimal,10,0} ,
          new Object[] {"@lV20TFContratoDadosCertame_Site",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV21TFContratoDadosCertame_Site_Sel",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV22TFContratoDadosCertame_Uasg",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV23TFContratoDadosCertame_Uasg_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV24TFContratoDadosCertame_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25TFContratoDadosCertame_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV26TFContratoDadosCertame_DataHomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV27TFContratoDadosCertame_DataHomologacao_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV28TFContratoDadosCertame_DataAdjudicacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV29TFContratoDadosCertame_DataAdjudicacao_To",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmP00KE4 ;
          prmP00KE4 = new Object[] {
          new Object[] {"@lV50ContratoDadosCertame_Modalidade1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV50ContratoDadosCertame_Modalidade1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV54ContratoDadosCertame_Modalidade2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV54ContratoDadosCertame_Modalidade2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV58ContratoDadosCertame_Modalidade3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV58ContratoDadosCertame_Modalidade3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV10TFContratoDadosCertame_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoDadosCertame_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV15TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV16TFContratoDadosCertame_Modalidade",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV17TFContratoDadosCertame_Modalidade_Sel",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV18TFContratoDadosCertame_Numero",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV19TFContratoDadosCertame_Numero_To",SqlDbType.Decimal,10,0} ,
          new Object[] {"@lV20TFContratoDadosCertame_Site",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV21TFContratoDadosCertame_Site_Sel",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV22TFContratoDadosCertame_Uasg",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV23TFContratoDadosCertame_Uasg_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV24TFContratoDadosCertame_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25TFContratoDadosCertame_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV26TFContratoDadosCertame_DataHomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV27TFContratoDadosCertame_DataHomologacao_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV28TFContratoDadosCertame_DataAdjudicacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV29TFContratoDadosCertame_DataAdjudicacao_To",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00KE2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KE2,100,0,true,false )
             ,new CursorDef("P00KE3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KE3,100,0,true,false )
             ,new CursorDef("P00KE4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KE4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((long[]) buf[10])[0] = rslt.getLong(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((String[]) buf[14])[0] = rslt.getVarchar(10) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((long[]) buf[10])[0] = rslt.getLong(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 20) ;
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(4) ;
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((long[]) buf[9])[0] = rslt.getLong(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 20) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((String[]) buf[14])[0] = rslt.getVarchar(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[40]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[41]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[44]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[45]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[40]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[41]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[44]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[45]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[40]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[41]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[44]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[45]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontratodadoscertamefilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontratodadoscertamefilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontratodadoscertamefilterdata") )
          {
             return  ;
          }
          getpromptcontratodadoscertamefilterdata worker = new getpromptcontratodadoscertamefilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
