/*
               File: GetWWGeral_Grupo_CargoFilterData
        Description: Get WWGeral_Grupo_Cargo Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:11.42
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwgeral_grupo_cargofilterdata : GXProcedure
   {
      public getwwgeral_grupo_cargofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwgeral_grupo_cargofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
         return AV24OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwgeral_grupo_cargofilterdata objgetwwgeral_grupo_cargofilterdata;
         objgetwwgeral_grupo_cargofilterdata = new getwwgeral_grupo_cargofilterdata();
         objgetwwgeral_grupo_cargofilterdata.AV15DDOName = aP0_DDOName;
         objgetwwgeral_grupo_cargofilterdata.AV13SearchTxt = aP1_SearchTxt;
         objgetwwgeral_grupo_cargofilterdata.AV14SearchTxtTo = aP2_SearchTxtTo;
         objgetwwgeral_grupo_cargofilterdata.AV19OptionsJson = "" ;
         objgetwwgeral_grupo_cargofilterdata.AV22OptionsDescJson = "" ;
         objgetwwgeral_grupo_cargofilterdata.AV24OptionIndexesJson = "" ;
         objgetwwgeral_grupo_cargofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwgeral_grupo_cargofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwgeral_grupo_cargofilterdata);
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwgeral_grupo_cargofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV18Options = (IGxCollection)(new GxSimpleCollection());
         AV21OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV23OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV15DDOName), "DDO_GRUPOCARGO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADGRUPOCARGO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV19OptionsJson = AV18Options.ToJSonString(false);
         AV22OptionsDescJson = AV21OptionsDesc.ToJSonString(false);
         AV24OptionIndexesJson = AV23OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV26Session.Get("WWGeral_Grupo_CargoGridState"), "") == 0 )
         {
            AV28GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWGeral_Grupo_CargoGridState"), "");
         }
         else
         {
            AV28GridState.FromXml(AV26Session.Get("WWGeral_Grupo_CargoGridState"), "");
         }
         AV41GXV1 = 1;
         while ( AV41GXV1 <= AV28GridState.gxTpr_Filtervalues.Count )
         {
            AV29GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV28GridState.gxTpr_Filtervalues.Item(AV41GXV1));
            if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFGRUPOCARGO_NOME") == 0 )
            {
               AV10TFGrupoCargo_Nome = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFGRUPOCARGO_NOME_SEL") == 0 )
            {
               AV11TFGrupoCargo_Nome_Sel = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFGRUPOCARGO_ATIVO_SEL") == 0 )
            {
               AV12TFGrupoCargo_Ativo_Sel = (short)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            AV41GXV1 = (int)(AV41GXV1+1);
         }
         if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(1));
            AV31DynamicFiltersSelector1 = AV30GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV31DynamicFiltersSelector1, "GRUPOCARGO_NOME") == 0 )
            {
               AV32GrupoCargo_Nome1 = AV30GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV33DynamicFiltersEnabled2 = true;
               AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(2));
               AV34DynamicFiltersSelector2 = AV30GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "GRUPOCARGO_NOME") == 0 )
               {
                  AV35GrupoCargo_Nome2 = AV30GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV36DynamicFiltersEnabled3 = true;
                  AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(3));
                  AV37DynamicFiltersSelector3 = AV30GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV37DynamicFiltersSelector3, "GRUPOCARGO_NOME") == 0 )
                  {
                     AV38GrupoCargo_Nome3 = AV30GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADGRUPOCARGO_NOMEOPTIONS' Routine */
         AV10TFGrupoCargo_Nome = AV13SearchTxt;
         AV11TFGrupoCargo_Nome_Sel = "";
         AV43WWGeral_Grupo_CargoDS_1_Dynamicfiltersselector1 = AV31DynamicFiltersSelector1;
         AV44WWGeral_Grupo_CargoDS_2_Grupocargo_nome1 = AV32GrupoCargo_Nome1;
         AV45WWGeral_Grupo_CargoDS_3_Dynamicfiltersenabled2 = AV33DynamicFiltersEnabled2;
         AV46WWGeral_Grupo_CargoDS_4_Dynamicfiltersselector2 = AV34DynamicFiltersSelector2;
         AV47WWGeral_Grupo_CargoDS_5_Grupocargo_nome2 = AV35GrupoCargo_Nome2;
         AV48WWGeral_Grupo_CargoDS_6_Dynamicfiltersenabled3 = AV36DynamicFiltersEnabled3;
         AV49WWGeral_Grupo_CargoDS_7_Dynamicfiltersselector3 = AV37DynamicFiltersSelector3;
         AV50WWGeral_Grupo_CargoDS_8_Grupocargo_nome3 = AV38GrupoCargo_Nome3;
         AV51WWGeral_Grupo_CargoDS_9_Tfgrupocargo_nome = AV10TFGrupoCargo_Nome;
         AV52WWGeral_Grupo_CargoDS_10_Tfgrupocargo_nome_sel = AV11TFGrupoCargo_Nome_Sel;
         AV53WWGeral_Grupo_CargoDS_11_Tfgrupocargo_ativo_sel = AV12TFGrupoCargo_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV43WWGeral_Grupo_CargoDS_1_Dynamicfiltersselector1 ,
                                              AV44WWGeral_Grupo_CargoDS_2_Grupocargo_nome1 ,
                                              AV45WWGeral_Grupo_CargoDS_3_Dynamicfiltersenabled2 ,
                                              AV46WWGeral_Grupo_CargoDS_4_Dynamicfiltersselector2 ,
                                              AV47WWGeral_Grupo_CargoDS_5_Grupocargo_nome2 ,
                                              AV48WWGeral_Grupo_CargoDS_6_Dynamicfiltersenabled3 ,
                                              AV49WWGeral_Grupo_CargoDS_7_Dynamicfiltersselector3 ,
                                              AV50WWGeral_Grupo_CargoDS_8_Grupocargo_nome3 ,
                                              AV52WWGeral_Grupo_CargoDS_10_Tfgrupocargo_nome_sel ,
                                              AV51WWGeral_Grupo_CargoDS_9_Tfgrupocargo_nome ,
                                              AV53WWGeral_Grupo_CargoDS_11_Tfgrupocargo_ativo_sel ,
                                              A616GrupoCargo_Nome ,
                                              A626GrupoCargo_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV44WWGeral_Grupo_CargoDS_2_Grupocargo_nome1 = StringUtil.Concat( StringUtil.RTrim( AV44WWGeral_Grupo_CargoDS_2_Grupocargo_nome1), "%", "");
         lV47WWGeral_Grupo_CargoDS_5_Grupocargo_nome2 = StringUtil.Concat( StringUtil.RTrim( AV47WWGeral_Grupo_CargoDS_5_Grupocargo_nome2), "%", "");
         lV50WWGeral_Grupo_CargoDS_8_Grupocargo_nome3 = StringUtil.Concat( StringUtil.RTrim( AV50WWGeral_Grupo_CargoDS_8_Grupocargo_nome3), "%", "");
         lV51WWGeral_Grupo_CargoDS_9_Tfgrupocargo_nome = StringUtil.Concat( StringUtil.RTrim( AV51WWGeral_Grupo_CargoDS_9_Tfgrupocargo_nome), "%", "");
         /* Using cursor P00NC2 */
         pr_default.execute(0, new Object[] {lV44WWGeral_Grupo_CargoDS_2_Grupocargo_nome1, lV47WWGeral_Grupo_CargoDS_5_Grupocargo_nome2, lV50WWGeral_Grupo_CargoDS_8_Grupocargo_nome3, lV51WWGeral_Grupo_CargoDS_9_Tfgrupocargo_nome, AV52WWGeral_Grupo_CargoDS_10_Tfgrupocargo_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKNC2 = false;
            A616GrupoCargo_Nome = P00NC2_A616GrupoCargo_Nome[0];
            A626GrupoCargo_Ativo = P00NC2_A626GrupoCargo_Ativo[0];
            A615GrupoCargo_Codigo = P00NC2_A615GrupoCargo_Codigo[0];
            AV25count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00NC2_A616GrupoCargo_Nome[0], A616GrupoCargo_Nome) == 0 ) )
            {
               BRKNC2 = false;
               A615GrupoCargo_Codigo = P00NC2_A615GrupoCargo_Codigo[0];
               AV25count = (long)(AV25count+1);
               BRKNC2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A616GrupoCargo_Nome)) )
            {
               AV17Option = A616GrupoCargo_Nome;
               AV20OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A616GrupoCargo_Nome, "@!")));
               AV18Options.Add(AV17Option, 0);
               AV21OptionsDesc.Add(AV20OptionDesc, 0);
               AV23OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV25count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV18Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKNC2 )
            {
               BRKNC2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV18Options = new GxSimpleCollection();
         AV21OptionsDesc = new GxSimpleCollection();
         AV23OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV26Session = context.GetSession();
         AV28GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV29GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFGrupoCargo_Nome = "";
         AV11TFGrupoCargo_Nome_Sel = "";
         AV30GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV31DynamicFiltersSelector1 = "";
         AV32GrupoCargo_Nome1 = "";
         AV34DynamicFiltersSelector2 = "";
         AV35GrupoCargo_Nome2 = "";
         AV37DynamicFiltersSelector3 = "";
         AV38GrupoCargo_Nome3 = "";
         AV43WWGeral_Grupo_CargoDS_1_Dynamicfiltersselector1 = "";
         AV44WWGeral_Grupo_CargoDS_2_Grupocargo_nome1 = "";
         AV46WWGeral_Grupo_CargoDS_4_Dynamicfiltersselector2 = "";
         AV47WWGeral_Grupo_CargoDS_5_Grupocargo_nome2 = "";
         AV49WWGeral_Grupo_CargoDS_7_Dynamicfiltersselector3 = "";
         AV50WWGeral_Grupo_CargoDS_8_Grupocargo_nome3 = "";
         AV51WWGeral_Grupo_CargoDS_9_Tfgrupocargo_nome = "";
         AV52WWGeral_Grupo_CargoDS_10_Tfgrupocargo_nome_sel = "";
         scmdbuf = "";
         lV44WWGeral_Grupo_CargoDS_2_Grupocargo_nome1 = "";
         lV47WWGeral_Grupo_CargoDS_5_Grupocargo_nome2 = "";
         lV50WWGeral_Grupo_CargoDS_8_Grupocargo_nome3 = "";
         lV51WWGeral_Grupo_CargoDS_9_Tfgrupocargo_nome = "";
         A616GrupoCargo_Nome = "";
         P00NC2_A616GrupoCargo_Nome = new String[] {""} ;
         P00NC2_A626GrupoCargo_Ativo = new bool[] {false} ;
         P00NC2_A615GrupoCargo_Codigo = new int[1] ;
         AV17Option = "";
         AV20OptionDesc = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwgeral_grupo_cargofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00NC2_A616GrupoCargo_Nome, P00NC2_A626GrupoCargo_Ativo, P00NC2_A615GrupoCargo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12TFGrupoCargo_Ativo_Sel ;
      private short AV53WWGeral_Grupo_CargoDS_11_Tfgrupocargo_ativo_sel ;
      private int AV41GXV1 ;
      private int A615GrupoCargo_Codigo ;
      private long AV25count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool AV33DynamicFiltersEnabled2 ;
      private bool AV36DynamicFiltersEnabled3 ;
      private bool AV45WWGeral_Grupo_CargoDS_3_Dynamicfiltersenabled2 ;
      private bool AV48WWGeral_Grupo_CargoDS_6_Dynamicfiltersenabled3 ;
      private bool A626GrupoCargo_Ativo ;
      private bool BRKNC2 ;
      private String AV24OptionIndexesJson ;
      private String AV19OptionsJson ;
      private String AV22OptionsDescJson ;
      private String AV15DDOName ;
      private String AV13SearchTxt ;
      private String AV14SearchTxtTo ;
      private String AV10TFGrupoCargo_Nome ;
      private String AV11TFGrupoCargo_Nome_Sel ;
      private String AV31DynamicFiltersSelector1 ;
      private String AV32GrupoCargo_Nome1 ;
      private String AV34DynamicFiltersSelector2 ;
      private String AV35GrupoCargo_Nome2 ;
      private String AV37DynamicFiltersSelector3 ;
      private String AV38GrupoCargo_Nome3 ;
      private String AV43WWGeral_Grupo_CargoDS_1_Dynamicfiltersselector1 ;
      private String AV44WWGeral_Grupo_CargoDS_2_Grupocargo_nome1 ;
      private String AV46WWGeral_Grupo_CargoDS_4_Dynamicfiltersselector2 ;
      private String AV47WWGeral_Grupo_CargoDS_5_Grupocargo_nome2 ;
      private String AV49WWGeral_Grupo_CargoDS_7_Dynamicfiltersselector3 ;
      private String AV50WWGeral_Grupo_CargoDS_8_Grupocargo_nome3 ;
      private String AV51WWGeral_Grupo_CargoDS_9_Tfgrupocargo_nome ;
      private String AV52WWGeral_Grupo_CargoDS_10_Tfgrupocargo_nome_sel ;
      private String lV44WWGeral_Grupo_CargoDS_2_Grupocargo_nome1 ;
      private String lV47WWGeral_Grupo_CargoDS_5_Grupocargo_nome2 ;
      private String lV50WWGeral_Grupo_CargoDS_8_Grupocargo_nome3 ;
      private String lV51WWGeral_Grupo_CargoDS_9_Tfgrupocargo_nome ;
      private String A616GrupoCargo_Nome ;
      private String AV17Option ;
      private String AV20OptionDesc ;
      private IGxSession AV26Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00NC2_A616GrupoCargo_Nome ;
      private bool[] P00NC2_A626GrupoCargo_Ativo ;
      private int[] P00NC2_A615GrupoCargo_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV18Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV28GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV29GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV30GridStateDynamicFilter ;
   }

   public class getwwgeral_grupo_cargofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00NC2( IGxContext context ,
                                             String AV43WWGeral_Grupo_CargoDS_1_Dynamicfiltersselector1 ,
                                             String AV44WWGeral_Grupo_CargoDS_2_Grupocargo_nome1 ,
                                             bool AV45WWGeral_Grupo_CargoDS_3_Dynamicfiltersenabled2 ,
                                             String AV46WWGeral_Grupo_CargoDS_4_Dynamicfiltersselector2 ,
                                             String AV47WWGeral_Grupo_CargoDS_5_Grupocargo_nome2 ,
                                             bool AV48WWGeral_Grupo_CargoDS_6_Dynamicfiltersenabled3 ,
                                             String AV49WWGeral_Grupo_CargoDS_7_Dynamicfiltersselector3 ,
                                             String AV50WWGeral_Grupo_CargoDS_8_Grupocargo_nome3 ,
                                             String AV52WWGeral_Grupo_CargoDS_10_Tfgrupocargo_nome_sel ,
                                             String AV51WWGeral_Grupo_CargoDS_9_Tfgrupocargo_nome ,
                                             short AV53WWGeral_Grupo_CargoDS_11_Tfgrupocargo_ativo_sel ,
                                             String A616GrupoCargo_Nome ,
                                             bool A626GrupoCargo_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [5] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [GrupoCargo_Nome], [GrupoCargo_Ativo], [GrupoCargo_Codigo] FROM [Geral_Grupo_Cargo] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV43WWGeral_Grupo_CargoDS_1_Dynamicfiltersselector1, "GRUPOCARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44WWGeral_Grupo_CargoDS_2_Grupocargo_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([GrupoCargo_Nome] like '%' + @lV44WWGeral_Grupo_CargoDS_2_Grupocargo_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([GrupoCargo_Nome] like '%' + @lV44WWGeral_Grupo_CargoDS_2_Grupocargo_nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( AV45WWGeral_Grupo_CargoDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV46WWGeral_Grupo_CargoDS_4_Dynamicfiltersselector2, "GRUPOCARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWGeral_Grupo_CargoDS_5_Grupocargo_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([GrupoCargo_Nome] like '%' + @lV47WWGeral_Grupo_CargoDS_5_Grupocargo_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([GrupoCargo_Nome] like '%' + @lV47WWGeral_Grupo_CargoDS_5_Grupocargo_nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV48WWGeral_Grupo_CargoDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV49WWGeral_Grupo_CargoDS_7_Dynamicfiltersselector3, "GRUPOCARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWGeral_Grupo_CargoDS_8_Grupocargo_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([GrupoCargo_Nome] like '%' + @lV50WWGeral_Grupo_CargoDS_8_Grupocargo_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([GrupoCargo_Nome] like '%' + @lV50WWGeral_Grupo_CargoDS_8_Grupocargo_nome3 + '%')";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV52WWGeral_Grupo_CargoDS_10_Tfgrupocargo_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWGeral_Grupo_CargoDS_9_Tfgrupocargo_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([GrupoCargo_Nome] like @lV51WWGeral_Grupo_CargoDS_9_Tfgrupocargo_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([GrupoCargo_Nome] like @lV51WWGeral_Grupo_CargoDS_9_Tfgrupocargo_nome)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWGeral_Grupo_CargoDS_10_Tfgrupocargo_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([GrupoCargo_Nome] = @AV52WWGeral_Grupo_CargoDS_10_Tfgrupocargo_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([GrupoCargo_Nome] = @AV52WWGeral_Grupo_CargoDS_10_Tfgrupocargo_nome_sel)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV53WWGeral_Grupo_CargoDS_11_Tfgrupocargo_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([GrupoCargo_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([GrupoCargo_Ativo] = 1)";
            }
         }
         if ( AV53WWGeral_Grupo_CargoDS_11_Tfgrupocargo_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([GrupoCargo_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([GrupoCargo_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [GrupoCargo_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00NC2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (bool)dynConstraints[12] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00NC2 ;
          prmP00NC2 = new Object[] {
          new Object[] {"@lV44WWGeral_Grupo_CargoDS_2_Grupocargo_nome1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV47WWGeral_Grupo_CargoDS_5_Grupocargo_nome2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV50WWGeral_Grupo_CargoDS_8_Grupocargo_nome3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV51WWGeral_Grupo_CargoDS_9_Tfgrupocargo_nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV52WWGeral_Grupo_CargoDS_10_Tfgrupocargo_nome_sel",SqlDbType.VarChar,80,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00NC2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NC2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwgeral_grupo_cargofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwgeral_grupo_cargofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwgeral_grupo_cargofilterdata") )
          {
             return  ;
          }
          getwwgeral_grupo_cargofilterdata worker = new getwwgeral_grupo_cargofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
