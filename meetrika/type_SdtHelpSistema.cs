/*
               File: type_SdtHelpSistema
        Description: Cadastro de Help do Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:13.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "HelpSistema" )]
   [XmlType(TypeName =  "HelpSistema" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtHelpSistema : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtHelpSistema( )
      {
         /* Constructor for serialization */
         gxTv_SdtHelpSistema_Helpsistema_objeto = "";
         gxTv_SdtHelpSistema_Helpsistema_descricao = "";
         gxTv_SdtHelpSistema_Helpsistema_video = "";
         gxTv_SdtHelpSistema_Mode = "";
         gxTv_SdtHelpSistema_Helpsistema_objeto_Z = "";
         gxTv_SdtHelpSistema_Helpsistema_video_Z = "";
      }

      public SdtHelpSistema( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV1638HelpSistema_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV1638HelpSistema_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"HelpSistema_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "HelpSistema");
         metadata.Set("BT", "HelpSistema");
         metadata.Set("PK", "[ \"HelpSistema_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"HelpSistema_Codigo\" ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Helpsistema_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Helpsistema_objeto_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Helpsistema_video_Z" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtHelpSistema deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtHelpSistema)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtHelpSistema obj ;
         obj = this;
         obj.gxTpr_Helpsistema_codigo = deserialized.gxTpr_Helpsistema_codigo;
         obj.gxTpr_Helpsistema_objeto = deserialized.gxTpr_Helpsistema_objeto;
         obj.gxTpr_Helpsistema_descricao = deserialized.gxTpr_Helpsistema_descricao;
         obj.gxTpr_Helpsistema_video = deserialized.gxTpr_Helpsistema_video;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Helpsistema_codigo_Z = deserialized.gxTpr_Helpsistema_codigo_Z;
         obj.gxTpr_Helpsistema_objeto_Z = deserialized.gxTpr_Helpsistema_objeto_Z;
         obj.gxTpr_Helpsistema_video_Z = deserialized.gxTpr_Helpsistema_video_Z;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "HelpSistema_Codigo") )
               {
                  gxTv_SdtHelpSistema_Helpsistema_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "HelpSistema_Objeto") )
               {
                  gxTv_SdtHelpSistema_Helpsistema_objeto = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "HelpSistema_Descricao") )
               {
                  gxTv_SdtHelpSistema_Helpsistema_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "HelpSistema_Video") )
               {
                  gxTv_SdtHelpSistema_Helpsistema_video = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtHelpSistema_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtHelpSistema_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "HelpSistema_Codigo_Z") )
               {
                  gxTv_SdtHelpSistema_Helpsistema_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "HelpSistema_Objeto_Z") )
               {
                  gxTv_SdtHelpSistema_Helpsistema_objeto_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "HelpSistema_Video_Z") )
               {
                  gxTv_SdtHelpSistema_Helpsistema_video_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "HelpSistema";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("HelpSistema_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtHelpSistema_Helpsistema_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("HelpSistema_Objeto", StringUtil.RTrim( gxTv_SdtHelpSistema_Helpsistema_objeto));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("HelpSistema_Descricao", StringUtil.RTrim( gxTv_SdtHelpSistema_Helpsistema_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("HelpSistema_Video", StringUtil.RTrim( gxTv_SdtHelpSistema_Helpsistema_video));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtHelpSistema_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtHelpSistema_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("HelpSistema_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtHelpSistema_Helpsistema_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("HelpSistema_Objeto_Z", StringUtil.RTrim( gxTv_SdtHelpSistema_Helpsistema_objeto_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("HelpSistema_Video_Z", StringUtil.RTrim( gxTv_SdtHelpSistema_Helpsistema_video_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("HelpSistema_Codigo", gxTv_SdtHelpSistema_Helpsistema_codigo, false);
         AddObjectProperty("HelpSistema_Objeto", gxTv_SdtHelpSistema_Helpsistema_objeto, false);
         AddObjectProperty("HelpSistema_Descricao", gxTv_SdtHelpSistema_Helpsistema_descricao, false);
         AddObjectProperty("HelpSistema_Video", gxTv_SdtHelpSistema_Helpsistema_video, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtHelpSistema_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtHelpSistema_Initialized, false);
            AddObjectProperty("HelpSistema_Codigo_Z", gxTv_SdtHelpSistema_Helpsistema_codigo_Z, false);
            AddObjectProperty("HelpSistema_Objeto_Z", gxTv_SdtHelpSistema_Helpsistema_objeto_Z, false);
            AddObjectProperty("HelpSistema_Video_Z", gxTv_SdtHelpSistema_Helpsistema_video_Z, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "HelpSistema_Codigo" )]
      [  XmlElement( ElementName = "HelpSistema_Codigo"   )]
      public int gxTpr_Helpsistema_codigo
      {
         get {
            return gxTv_SdtHelpSistema_Helpsistema_codigo ;
         }

         set {
            if ( gxTv_SdtHelpSistema_Helpsistema_codigo != value )
            {
               gxTv_SdtHelpSistema_Mode = "INS";
               this.gxTv_SdtHelpSistema_Helpsistema_codigo_Z_SetNull( );
               this.gxTv_SdtHelpSistema_Helpsistema_objeto_Z_SetNull( );
               this.gxTv_SdtHelpSistema_Helpsistema_video_Z_SetNull( );
            }
            gxTv_SdtHelpSistema_Helpsistema_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "HelpSistema_Objeto" )]
      [  XmlElement( ElementName = "HelpSistema_Objeto"   )]
      public String gxTpr_Helpsistema_objeto
      {
         get {
            return gxTv_SdtHelpSistema_Helpsistema_objeto ;
         }

         set {
            gxTv_SdtHelpSistema_Helpsistema_objeto = (String)(value);
         }

      }

      [  SoapElement( ElementName = "HelpSistema_Descricao" )]
      [  XmlElement( ElementName = "HelpSistema_Descricao"   )]
      public String gxTpr_Helpsistema_descricao
      {
         get {
            return gxTv_SdtHelpSistema_Helpsistema_descricao ;
         }

         set {
            gxTv_SdtHelpSistema_Helpsistema_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "HelpSistema_Video" )]
      [  XmlElement( ElementName = "HelpSistema_Video"   )]
      public String gxTpr_Helpsistema_video
      {
         get {
            return gxTv_SdtHelpSistema_Helpsistema_video ;
         }

         set {
            gxTv_SdtHelpSistema_Helpsistema_video = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtHelpSistema_Mode ;
         }

         set {
            gxTv_SdtHelpSistema_Mode = (String)(value);
         }

      }

      public void gxTv_SdtHelpSistema_Mode_SetNull( )
      {
         gxTv_SdtHelpSistema_Mode = "";
         return  ;
      }

      public bool gxTv_SdtHelpSistema_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtHelpSistema_Initialized ;
         }

         set {
            gxTv_SdtHelpSistema_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtHelpSistema_Initialized_SetNull( )
      {
         gxTv_SdtHelpSistema_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtHelpSistema_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "HelpSistema_Codigo_Z" )]
      [  XmlElement( ElementName = "HelpSistema_Codigo_Z"   )]
      public int gxTpr_Helpsistema_codigo_Z
      {
         get {
            return gxTv_SdtHelpSistema_Helpsistema_codigo_Z ;
         }

         set {
            gxTv_SdtHelpSistema_Helpsistema_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtHelpSistema_Helpsistema_codigo_Z_SetNull( )
      {
         gxTv_SdtHelpSistema_Helpsistema_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtHelpSistema_Helpsistema_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "HelpSistema_Objeto_Z" )]
      [  XmlElement( ElementName = "HelpSistema_Objeto_Z"   )]
      public String gxTpr_Helpsistema_objeto_Z
      {
         get {
            return gxTv_SdtHelpSistema_Helpsistema_objeto_Z ;
         }

         set {
            gxTv_SdtHelpSistema_Helpsistema_objeto_Z = (String)(value);
         }

      }

      public void gxTv_SdtHelpSistema_Helpsistema_objeto_Z_SetNull( )
      {
         gxTv_SdtHelpSistema_Helpsistema_objeto_Z = "";
         return  ;
      }

      public bool gxTv_SdtHelpSistema_Helpsistema_objeto_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "HelpSistema_Video_Z" )]
      [  XmlElement( ElementName = "HelpSistema_Video_Z"   )]
      public String gxTpr_Helpsistema_video_Z
      {
         get {
            return gxTv_SdtHelpSistema_Helpsistema_video_Z ;
         }

         set {
            gxTv_SdtHelpSistema_Helpsistema_video_Z = (String)(value);
         }

      }

      public void gxTv_SdtHelpSistema_Helpsistema_video_Z_SetNull( )
      {
         gxTv_SdtHelpSistema_Helpsistema_video_Z = "";
         return  ;
      }

      public bool gxTv_SdtHelpSistema_Helpsistema_video_Z_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtHelpSistema_Helpsistema_objeto = "";
         gxTv_SdtHelpSistema_Helpsistema_descricao = "";
         gxTv_SdtHelpSistema_Helpsistema_video = "";
         gxTv_SdtHelpSistema_Mode = "";
         gxTv_SdtHelpSistema_Helpsistema_objeto_Z = "";
         gxTv_SdtHelpSistema_Helpsistema_video_Z = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "helpsistema", "GeneXus.Programs.helpsistema_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtHelpSistema_Initialized ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtHelpSistema_Helpsistema_codigo ;
      private int gxTv_SdtHelpSistema_Helpsistema_codigo_Z ;
      private String gxTv_SdtHelpSistema_Mode ;
      private String sTagName ;
      private String gxTv_SdtHelpSistema_Helpsistema_descricao ;
      private String gxTv_SdtHelpSistema_Helpsistema_objeto ;
      private String gxTv_SdtHelpSistema_Helpsistema_video ;
      private String gxTv_SdtHelpSistema_Helpsistema_objeto_Z ;
      private String gxTv_SdtHelpSistema_Helpsistema_video_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"HelpSistema", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtHelpSistema_RESTInterface : GxGenericCollectionItem<SdtHelpSistema>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtHelpSistema_RESTInterface( ) : base()
      {
      }

      public SdtHelpSistema_RESTInterface( SdtHelpSistema psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "HelpSistema_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Helpsistema_codigo
      {
         get {
            return sdt.gxTpr_Helpsistema_codigo ;
         }

         set {
            sdt.gxTpr_Helpsistema_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "HelpSistema_Objeto" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Helpsistema_objeto
      {
         get {
            return sdt.gxTpr_Helpsistema_objeto ;
         }

         set {
            sdt.gxTpr_Helpsistema_objeto = (String)(value);
         }

      }

      [DataMember( Name = "HelpSistema_Descricao" , Order = 2 )]
      public String gxTpr_Helpsistema_descricao
      {
         get {
            return sdt.gxTpr_Helpsistema_descricao ;
         }

         set {
            sdt.gxTpr_Helpsistema_descricao = (String)(value);
         }

      }

      [DataMember( Name = "HelpSistema_Video" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Helpsistema_video
      {
         get {
            return sdt.gxTpr_Helpsistema_video ;
         }

         set {
            sdt.gxTpr_Helpsistema_video = (String)(value);
         }

      }

      public SdtHelpSistema sdt
      {
         get {
            return (SdtHelpSistema)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtHelpSistema() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 9 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
