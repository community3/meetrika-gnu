/*
               File: Audit
        Description: Audit
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:28:15.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class audit : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1Usuario_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A1Usuario_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A57Usuario_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A57Usuario_PessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7AuditId = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7AuditId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7AuditId), 18, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAUDITID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7AuditId), "ZZZZZZZZZZZZZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Audit", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtAuditDate_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public audit( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public audit( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           long aP1_AuditId )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7AuditId = aP1_AuditId;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3P170( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3P170e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3P170( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3P170( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3P170e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_61_3P170( true) ;
         }
         return  ;
      }

      protected void wb_table3_61_3P170e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3P170e( true) ;
         }
         else
         {
            wb_table1_2_3P170e( false) ;
         }
      }

      protected void wb_table3_61_3P170( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_61_3P170e( true) ;
         }
         else
         {
            wb_table3_61_3P170e( false) ;
         }
      }

      protected void wb_table2_5_3P170( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_3P170( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_3P170e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3P170e( true) ;
         }
         else
         {
            wb_table2_5_3P170e( false) ;
         }
      }

      protected void wb_table4_13_3P170( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockauditid_Internalname, "Auditoria", "", "", lblTextblockauditid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAuditId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1430AuditId), 18, 0, ",", "")), ((edtAuditId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1430AuditId), "ZZZZZZZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1430AuditId), "ZZZZZZZZZZZZZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAuditId_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAuditId_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockauditdate_Internalname, "/ Hora", "", "", lblTextblockauditdate_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtAuditDate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtAuditDate_Internalname, context.localUtil.TToC( A1431AuditDate, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1431AuditDate, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAuditDate_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtAuditDate_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Audit.htm");
            GxWebStd.gx_bitmap( context, edtAuditDate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtAuditDate_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Audit.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockaudittablename_Internalname, "da Tabela", "", "", lblTextblockaudittablename_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAuditTableName_Internalname, StringUtil.RTrim( A1432AuditTableName), StringUtil.RTrim( context.localUtil.Format( A1432AuditTableName, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAuditTableName_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAuditTableName_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockauditdescription_Internalname, "Descri��o", "", "", lblTextblockauditdescription_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtAuditDescription_Internalname, A1434AuditDescription, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", 0, 1, edtAuditDescription_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockauditshortdescription_Internalname, "Compacta", "", "", lblTextblockauditshortdescription_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtAuditShortDescription_Internalname, A1433AuditShortDescription, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", 0, 1, edtAuditShortDescription_Enabled, 0, 80, "chr", 5, "row", StyleString, ClassString, "", "400", -1, "", "", -1, true, "", "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_codigo_Internalname, "C�digo", "", "", lblTextblockusuario_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtUsuario_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtUsuario_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Audit.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgprompt_1_Internalname, context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )), imgprompt_1_Link, "", "", context.GetTheme( ), imgprompt_1_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_pessoanom_Internalname, "Pessoa", "", "", lblTextblockusuario_pessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_PessoaNom_Internalname, StringUtil.RTrim( A58Usuario_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A58Usuario_PessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_PessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtUsuario_PessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_nome_Internalname, "Usu�rio", "", "", lblTextblockusuario_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_Nome_Internalname, StringUtil.RTrim( A2Usuario_Nome), StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtUsuario_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockauditaction_Internalname, "A��o", "", "", lblTextblockauditaction_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAuditAction_Internalname, StringUtil.RTrim( A1435AuditAction), StringUtil.RTrim( context.localUtil.Format( A1435AuditAction, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAuditAction_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAuditAction_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Audit.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_3P170e( true) ;
         }
         else
         {
            wb_table4_13_3P170e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E113P2 */
         E113P2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A1430AuditId = (long)(context.localUtil.CToN( cgiGet( edtAuditId_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1430AuditId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1430AuditId), 18, 0)));
               if ( context.localUtil.VCDateTime( cgiGet( edtAuditDate_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data / Hora"}), 1, "AUDITDATE");
                  AnyError = 1;
                  GX_FocusControl = edtAuditDate_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1431AuditDate = (DateTime)(DateTime.MinValue);
                  n1431AuditDate = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1431AuditDate", context.localUtil.TToC( A1431AuditDate, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1431AuditDate = context.localUtil.CToT( cgiGet( edtAuditDate_Internalname));
                  n1431AuditDate = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1431AuditDate", context.localUtil.TToC( A1431AuditDate, 8, 5, 0, 3, "/", ":", " "));
               }
               n1431AuditDate = ((DateTime.MinValue==A1431AuditDate) ? true : false);
               A1432AuditTableName = StringUtil.Upper( cgiGet( edtAuditTableName_Internalname));
               n1432AuditTableName = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1432AuditTableName", A1432AuditTableName);
               n1432AuditTableName = (String.IsNullOrEmpty(StringUtil.RTrim( A1432AuditTableName)) ? true : false);
               A1434AuditDescription = cgiGet( edtAuditDescription_Internalname);
               n1434AuditDescription = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1434AuditDescription", A1434AuditDescription);
               n1434AuditDescription = (String.IsNullOrEmpty(StringUtil.RTrim( A1434AuditDescription)) ? true : false);
               A1433AuditShortDescription = cgiGet( edtAuditShortDescription_Internalname);
               n1433AuditShortDescription = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1433AuditShortDescription", A1433AuditShortDescription);
               n1433AuditShortDescription = (String.IsNullOrEmpty(StringUtil.RTrim( A1433AuditShortDescription)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtUsuario_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtUsuario_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "USUARIO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtUsuario_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1Usuario_Codigo = 0;
                  n1Usuario_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               }
               else
               {
                  A1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUsuario_Codigo_Internalname), ",", "."));
                  n1Usuario_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               }
               n1Usuario_Codigo = ((0==A1Usuario_Codigo) ? true : false);
               A58Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtUsuario_PessoaNom_Internalname));
               n58Usuario_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
               A2Usuario_Nome = StringUtil.Upper( cgiGet( edtUsuario_Nome_Internalname));
               n2Usuario_Nome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
               A1435AuditAction = cgiGet( edtAuditAction_Internalname);
               n1435AuditAction = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1435AuditAction", A1435AuditAction);
               n1435AuditAction = (String.IsNullOrEmpty(StringUtil.RTrim( A1435AuditAction)) ? true : false);
               /* Read saved values. */
               Z1430AuditId = (long)(context.localUtil.CToN( cgiGet( "Z1430AuditId"), ",", "."));
               Z1431AuditDate = context.localUtil.CToT( cgiGet( "Z1431AuditDate"), 0);
               n1431AuditDate = ((DateTime.MinValue==A1431AuditDate) ? true : false);
               Z1432AuditTableName = cgiGet( "Z1432AuditTableName");
               n1432AuditTableName = (String.IsNullOrEmpty(StringUtil.RTrim( A1432AuditTableName)) ? true : false);
               Z1433AuditShortDescription = cgiGet( "Z1433AuditShortDescription");
               n1433AuditShortDescription = (String.IsNullOrEmpty(StringUtil.RTrim( A1433AuditShortDescription)) ? true : false);
               Z1435AuditAction = cgiGet( "Z1435AuditAction");
               n1435AuditAction = (String.IsNullOrEmpty(StringUtil.RTrim( A1435AuditAction)) ? true : false);
               Z1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1Usuario_Codigo"), ",", "."));
               n1Usuario_Codigo = ((0==A1Usuario_Codigo) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( "N1Usuario_Codigo"), ",", "."));
               n1Usuario_Codigo = ((0==A1Usuario_Codigo) ? true : false);
               AV7AuditId = (long)(context.localUtil.CToN( cgiGet( "vAUDITID"), ",", "."));
               AV11Insert_Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_USUARIO_CODIGO"), ",", "."));
               A57Usuario_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "USUARIO_PESSOACOD"), ",", "."));
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Audit";
               A1430AuditId = (long)(context.localUtil.CToN( cgiGet( edtAuditId_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1430AuditId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1430AuditId), 18, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1430AuditId), "ZZZZZZZZZZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1430AuditId != Z1430AuditId ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("audit:[SecurityCheckFailed value for]"+"AuditId:"+context.localUtil.Format( (decimal)(A1430AuditId), "ZZZZZZZZZZZZZZZZZ9"));
                  GXUtil.WriteLog("audit:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1430AuditId = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1430AuditId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1430AuditId), 18, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode170 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode170;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound170 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_3P0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "AUDITID");
                        AnyError = 1;
                        GX_FocusControl = edtAuditId_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E113P2 */
                           E113P2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123P2 */
                           E123P2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E123P2 */
            E123P2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3P170( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes3P170( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_3P0( )
      {
         BeforeValidate3P170( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3P170( ) ;
            }
            else
            {
               CheckExtendedTable3P170( ) ;
               CloseExtendedTableCursors3P170( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption3P0( )
      {
      }

      protected void E113P2( )
      {
         /* Start Routine */
         context.wjLoc = formatLink("wwaudit.aspx") ;
         context.wjLocDisableFrm = 1;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Usuario_Codigo") == 0 )
               {
                  AV11Insert_Usuario_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Usuario_Codigo), 6, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
      }

      protected void E123P2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwaudit.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM3P170( short GX_JID )
      {
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1431AuditDate = T003P3_A1431AuditDate[0];
               Z1432AuditTableName = T003P3_A1432AuditTableName[0];
               Z1433AuditShortDescription = T003P3_A1433AuditShortDescription[0];
               Z1435AuditAction = T003P3_A1435AuditAction[0];
               Z1Usuario_Codigo = T003P3_A1Usuario_Codigo[0];
            }
            else
            {
               Z1431AuditDate = A1431AuditDate;
               Z1432AuditTableName = A1432AuditTableName;
               Z1433AuditShortDescription = A1433AuditShortDescription;
               Z1435AuditAction = A1435AuditAction;
               Z1Usuario_Codigo = A1Usuario_Codigo;
            }
         }
         if ( GX_JID == -9 )
         {
            Z1430AuditId = A1430AuditId;
            Z1431AuditDate = A1431AuditDate;
            Z1432AuditTableName = A1432AuditTableName;
            Z1434AuditDescription = A1434AuditDescription;
            Z1433AuditShortDescription = A1433AuditShortDescription;
            Z1435AuditAction = A1435AuditAction;
            Z1Usuario_Codigo = A1Usuario_Codigo;
            Z57Usuario_PessoaCod = A57Usuario_PessoaCod;
            Z2Usuario_Nome = A2Usuario_Nome;
            Z58Usuario_PessoaNom = A58Usuario_PessoaNom;
         }
      }

      protected void standaloneNotModal( )
      {
         edtAuditId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAuditId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAuditId_Enabled), 5, 0)));
         AV13Pgmname = "Audit";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         imgprompt_1_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"promptusuario.aspx"+"',["+"{Ctrl:gx.dom.el('"+"USUARIO_CODIGO"+"'), id:'"+"USUARIO_CODIGO"+"'"+",IOType:'inout'}"+","+"{Ctrl:gx.dom.el('"+"USUARIO_PESSOANOM"+"'), id:'"+"USUARIO_PESSOANOM"+"'"+",IOType:'inout'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         edtAuditId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAuditId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAuditId_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7AuditId) )
         {
            A1430AuditId = AV7AuditId;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1430AuditId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1430AuditId), 18, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Usuario_Codigo) )
         {
            edtUsuario_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtUsuario_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Codigo_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Usuario_Codigo) )
         {
            A1Usuario_Codigo = AV11Insert_Usuario_Codigo;
            n1Usuario_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T003P4 */
            pr_default.execute(2, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            A57Usuario_PessoaCod = T003P4_A57Usuario_PessoaCod[0];
            A2Usuario_Nome = T003P4_A2Usuario_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
            n2Usuario_Nome = T003P4_n2Usuario_Nome[0];
            pr_default.close(2);
            /* Using cursor T003P5 */
            pr_default.execute(3, new Object[] {A57Usuario_PessoaCod});
            A58Usuario_PessoaNom = T003P5_A58Usuario_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
            n58Usuario_PessoaNom = T003P5_n58Usuario_PessoaNom[0];
            pr_default.close(3);
         }
      }

      protected void Load3P170( )
      {
         /* Using cursor T003P6 */
         pr_default.execute(4, new Object[] {A1430AuditId});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound170 = 1;
            A57Usuario_PessoaCod = T003P6_A57Usuario_PessoaCod[0];
            A1431AuditDate = T003P6_A1431AuditDate[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1431AuditDate", context.localUtil.TToC( A1431AuditDate, 8, 5, 0, 3, "/", ":", " "));
            n1431AuditDate = T003P6_n1431AuditDate[0];
            A1432AuditTableName = T003P6_A1432AuditTableName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1432AuditTableName", A1432AuditTableName);
            n1432AuditTableName = T003P6_n1432AuditTableName[0];
            A1434AuditDescription = T003P6_A1434AuditDescription[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1434AuditDescription", A1434AuditDescription);
            n1434AuditDescription = T003P6_n1434AuditDescription[0];
            A1433AuditShortDescription = T003P6_A1433AuditShortDescription[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1433AuditShortDescription", A1433AuditShortDescription);
            n1433AuditShortDescription = T003P6_n1433AuditShortDescription[0];
            A58Usuario_PessoaNom = T003P6_A58Usuario_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
            n58Usuario_PessoaNom = T003P6_n58Usuario_PessoaNom[0];
            A2Usuario_Nome = T003P6_A2Usuario_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
            n2Usuario_Nome = T003P6_n2Usuario_Nome[0];
            A1435AuditAction = T003P6_A1435AuditAction[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1435AuditAction", A1435AuditAction);
            n1435AuditAction = T003P6_n1435AuditAction[0];
            A1Usuario_Codigo = T003P6_A1Usuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            n1Usuario_Codigo = T003P6_n1Usuario_Codigo[0];
            ZM3P170( -9) ;
         }
         pr_default.close(4);
         OnLoadActions3P170( ) ;
      }

      protected void OnLoadActions3P170( )
      {
      }

      protected void CheckExtendedTable3P170( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A1431AuditDate) || ( A1431AuditDate >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data / Hora fora do intervalo", "OutOfRange", 1, "AUDITDATE");
            AnyError = 1;
            GX_FocusControl = edtAuditDate_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T003P4 */
         pr_default.execute(2, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A1Usuario_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Usuario'.", "ForeignKeyNotFound", 1, "USUARIO_CODIGO");
               AnyError = 1;
               GX_FocusControl = edtUsuario_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A57Usuario_PessoaCod = T003P4_A57Usuario_PessoaCod[0];
         A2Usuario_Nome = T003P4_A2Usuario_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
         n2Usuario_Nome = T003P4_n2Usuario_Nome[0];
         pr_default.close(2);
         /* Using cursor T003P5 */
         pr_default.execute(3, new Object[] {A57Usuario_PessoaCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A57Usuario_PessoaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Usuario_Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A58Usuario_PessoaNom = T003P5_A58Usuario_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
         n58Usuario_PessoaNom = T003P5_n58Usuario_PessoaNom[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors3P170( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_10( int A1Usuario_Codigo )
      {
         /* Using cursor T003P7 */
         pr_default.execute(5, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A1Usuario_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Usuario'.", "ForeignKeyNotFound", 1, "USUARIO_CODIGO");
               AnyError = 1;
               GX_FocusControl = edtUsuario_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A57Usuario_PessoaCod = T003P7_A57Usuario_PessoaCod[0];
         A2Usuario_Nome = T003P7_A2Usuario_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
         n2Usuario_Nome = T003P7_n2Usuario_Nome[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A2Usuario_Nome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_11( int A57Usuario_PessoaCod )
      {
         /* Using cursor T003P8 */
         pr_default.execute(6, new Object[] {A57Usuario_PessoaCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A57Usuario_PessoaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Usuario_Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A58Usuario_PessoaNom = T003P8_A58Usuario_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
         n58Usuario_PessoaNom = T003P8_n58Usuario_PessoaNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A58Usuario_PessoaNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey3P170( )
      {
         /* Using cursor T003P9 */
         pr_default.execute(7, new Object[] {A1430AuditId});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound170 = 1;
         }
         else
         {
            RcdFound170 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003P3 */
         pr_default.execute(1, new Object[] {A1430AuditId});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3P170( 9) ;
            RcdFound170 = 1;
            A1430AuditId = T003P3_A1430AuditId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1430AuditId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1430AuditId), 18, 0)));
            A1431AuditDate = T003P3_A1431AuditDate[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1431AuditDate", context.localUtil.TToC( A1431AuditDate, 8, 5, 0, 3, "/", ":", " "));
            n1431AuditDate = T003P3_n1431AuditDate[0];
            A1432AuditTableName = T003P3_A1432AuditTableName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1432AuditTableName", A1432AuditTableName);
            n1432AuditTableName = T003P3_n1432AuditTableName[0];
            A1434AuditDescription = T003P3_A1434AuditDescription[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1434AuditDescription", A1434AuditDescription);
            n1434AuditDescription = T003P3_n1434AuditDescription[0];
            A1433AuditShortDescription = T003P3_A1433AuditShortDescription[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1433AuditShortDescription", A1433AuditShortDescription);
            n1433AuditShortDescription = T003P3_n1433AuditShortDescription[0];
            A1435AuditAction = T003P3_A1435AuditAction[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1435AuditAction", A1435AuditAction);
            n1435AuditAction = T003P3_n1435AuditAction[0];
            A1Usuario_Codigo = T003P3_A1Usuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            n1Usuario_Codigo = T003P3_n1Usuario_Codigo[0];
            Z1430AuditId = A1430AuditId;
            sMode170 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3P170( ) ;
            if ( AnyError == 1 )
            {
               RcdFound170 = 0;
               InitializeNonKey3P170( ) ;
            }
            Gx_mode = sMode170;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound170 = 0;
            InitializeNonKey3P170( ) ;
            sMode170 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode170;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3P170( ) ;
         if ( RcdFound170 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound170 = 0;
         /* Using cursor T003P10 */
         pr_default.execute(8, new Object[] {A1430AuditId});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T003P10_A1430AuditId[0] < A1430AuditId ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T003P10_A1430AuditId[0] > A1430AuditId ) ) )
            {
               A1430AuditId = T003P10_A1430AuditId[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1430AuditId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1430AuditId), 18, 0)));
               RcdFound170 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound170 = 0;
         /* Using cursor T003P11 */
         pr_default.execute(9, new Object[] {A1430AuditId});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T003P11_A1430AuditId[0] > A1430AuditId ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T003P11_A1430AuditId[0] < A1430AuditId ) ) )
            {
               A1430AuditId = T003P11_A1430AuditId[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1430AuditId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1430AuditId), 18, 0)));
               RcdFound170 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3P170( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtAuditDate_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3P170( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound170 == 1 )
            {
               if ( A1430AuditId != Z1430AuditId )
               {
                  A1430AuditId = Z1430AuditId;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1430AuditId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1430AuditId), 18, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "AUDITID");
                  AnyError = 1;
                  GX_FocusControl = edtAuditId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtAuditDate_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update3P170( ) ;
                  GX_FocusControl = edtAuditDate_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1430AuditId != Z1430AuditId )
               {
                  /* Insert record */
                  GX_FocusControl = edtAuditDate_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3P170( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "AUDITID");
                     AnyError = 1;
                     GX_FocusControl = edtAuditId_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtAuditDate_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3P170( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1430AuditId != Z1430AuditId )
         {
            A1430AuditId = Z1430AuditId;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1430AuditId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1430AuditId), 18, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "AUDITID");
            AnyError = 1;
            GX_FocusControl = edtAuditId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtAuditDate_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency3P170( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003P2 */
            pr_default.execute(0, new Object[] {A1430AuditId});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Audit"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1431AuditDate != T003P2_A1431AuditDate[0] ) || ( StringUtil.StrCmp(Z1432AuditTableName, T003P2_A1432AuditTableName[0]) != 0 ) || ( StringUtil.StrCmp(Z1433AuditShortDescription, T003P2_A1433AuditShortDescription[0]) != 0 ) || ( StringUtil.StrCmp(Z1435AuditAction, T003P2_A1435AuditAction[0]) != 0 ) || ( Z1Usuario_Codigo != T003P2_A1Usuario_Codigo[0] ) )
            {
               if ( Z1431AuditDate != T003P2_A1431AuditDate[0] )
               {
                  GXUtil.WriteLog("audit:[seudo value changed for attri]"+"AuditDate");
                  GXUtil.WriteLogRaw("Old: ",Z1431AuditDate);
                  GXUtil.WriteLogRaw("Current: ",T003P2_A1431AuditDate[0]);
               }
               if ( StringUtil.StrCmp(Z1432AuditTableName, T003P2_A1432AuditTableName[0]) != 0 )
               {
                  GXUtil.WriteLog("audit:[seudo value changed for attri]"+"AuditTableName");
                  GXUtil.WriteLogRaw("Old: ",Z1432AuditTableName);
                  GXUtil.WriteLogRaw("Current: ",T003P2_A1432AuditTableName[0]);
               }
               if ( StringUtil.StrCmp(Z1433AuditShortDescription, T003P2_A1433AuditShortDescription[0]) != 0 )
               {
                  GXUtil.WriteLog("audit:[seudo value changed for attri]"+"AuditShortDescription");
                  GXUtil.WriteLogRaw("Old: ",Z1433AuditShortDescription);
                  GXUtil.WriteLogRaw("Current: ",T003P2_A1433AuditShortDescription[0]);
               }
               if ( StringUtil.StrCmp(Z1435AuditAction, T003P2_A1435AuditAction[0]) != 0 )
               {
                  GXUtil.WriteLog("audit:[seudo value changed for attri]"+"AuditAction");
                  GXUtil.WriteLogRaw("Old: ",Z1435AuditAction);
                  GXUtil.WriteLogRaw("Current: ",T003P2_A1435AuditAction[0]);
               }
               if ( Z1Usuario_Codigo != T003P2_A1Usuario_Codigo[0] )
               {
                  GXUtil.WriteLog("audit:[seudo value changed for attri]"+"Usuario_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z1Usuario_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T003P2_A1Usuario_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Audit"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3P170( )
      {
         BeforeValidate3P170( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3P170( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3P170( 0) ;
            CheckOptimisticConcurrency3P170( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3P170( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3P170( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003P12 */
                     pr_default.execute(10, new Object[] {n1431AuditDate, A1431AuditDate, n1432AuditTableName, A1432AuditTableName, n1434AuditDescription, A1434AuditDescription, n1433AuditShortDescription, A1433AuditShortDescription, n1435AuditAction, A1435AuditAction, n1Usuario_Codigo, A1Usuario_Codigo});
                     A1430AuditId = T003P12_A1430AuditId[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1430AuditId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1430AuditId), 18, 0)));
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("Audit") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3P0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3P170( ) ;
            }
            EndLevel3P170( ) ;
         }
         CloseExtendedTableCursors3P170( ) ;
      }

      protected void Update3P170( )
      {
         BeforeValidate3P170( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3P170( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3P170( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3P170( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3P170( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003P13 */
                     pr_default.execute(11, new Object[] {n1431AuditDate, A1431AuditDate, n1432AuditTableName, A1432AuditTableName, n1434AuditDescription, A1434AuditDescription, n1433AuditShortDescription, A1433AuditShortDescription, n1435AuditAction, A1435AuditAction, n1Usuario_Codigo, A1Usuario_Codigo, A1430AuditId});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("Audit") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Audit"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3P170( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3P170( ) ;
         }
         CloseExtendedTableCursors3P170( ) ;
      }

      protected void DeferredUpdate3P170( )
      {
      }

      protected void delete( )
      {
         BeforeValidate3P170( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3P170( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3P170( ) ;
            AfterConfirm3P170( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3P170( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003P14 */
                  pr_default.execute(12, new Object[] {A1430AuditId});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("Audit") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode170 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3P170( ) ;
         Gx_mode = sMode170;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3P170( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003P15 */
            pr_default.execute(13, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
            A57Usuario_PessoaCod = T003P15_A57Usuario_PessoaCod[0];
            A2Usuario_Nome = T003P15_A2Usuario_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
            n2Usuario_Nome = T003P15_n2Usuario_Nome[0];
            pr_default.close(13);
            /* Using cursor T003P16 */
            pr_default.execute(14, new Object[] {A57Usuario_PessoaCod});
            A58Usuario_PessoaNom = T003P16_A58Usuario_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
            n58Usuario_PessoaNom = T003P16_n58Usuario_PessoaNom[0];
            pr_default.close(14);
         }
      }

      protected void EndLevel3P170( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3P170( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(13);
            pr_default.close(14);
            context.CommitDataStores( "Audit");
            if ( AnyError == 0 )
            {
               ConfirmValues3P0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(13);
            pr_default.close(14);
            context.RollbackDataStores( "Audit");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3P170( )
      {
         /* Scan By routine */
         /* Using cursor T003P17 */
         pr_default.execute(15);
         RcdFound170 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound170 = 1;
            A1430AuditId = T003P17_A1430AuditId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1430AuditId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1430AuditId), 18, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3P170( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound170 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound170 = 1;
            A1430AuditId = T003P17_A1430AuditId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1430AuditId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1430AuditId), 18, 0)));
         }
      }

      protected void ScanEnd3P170( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm3P170( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3P170( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3P170( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3P170( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3P170( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3P170( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3P170( )
      {
         edtAuditId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAuditId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAuditId_Enabled), 5, 0)));
         edtAuditDate_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAuditDate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAuditDate_Enabled), 5, 0)));
         edtAuditTableName_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAuditTableName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAuditTableName_Enabled), 5, 0)));
         edtAuditDescription_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAuditDescription_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAuditDescription_Enabled), 5, 0)));
         edtAuditShortDescription_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAuditShortDescription_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAuditShortDescription_Enabled), 5, 0)));
         edtUsuario_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Codigo_Enabled), 5, 0)));
         edtUsuario_PessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaNom_Enabled), 5, 0)));
         edtUsuario_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Nome_Enabled), 5, 0)));
         edtAuditAction_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAuditAction_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAuditAction_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3P0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117281645");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("audit.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7AuditId)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1430AuditId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1430AuditId), 18, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1431AuditDate", context.localUtil.TToC( Z1431AuditDate, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1432AuditTableName", StringUtil.RTrim( Z1432AuditTableName));
         GxWebStd.gx_hidden_field( context, "Z1433AuditShortDescription", Z1433AuditShortDescription);
         GxWebStd.gx_hidden_field( context, "Z1435AuditAction", StringUtil.RTrim( Z1435AuditAction));
         GxWebStd.gx_hidden_field( context, "Z1Usuario_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N1Usuario_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vAUDITID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7AuditId), 18, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vAUDITID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7AuditId), "ZZZZZZZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Audit";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1430AuditId), "ZZZZZZZZZZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("audit:[SendSecurityCheck value for]"+"AuditId:"+context.localUtil.Format( (decimal)(A1430AuditId), "ZZZZZZZZZZZZZZZZZ9"));
         GXUtil.WriteLog("audit:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("audit.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7AuditId) ;
      }

      public override String GetPgmname( )
      {
         return "Audit" ;
      }

      public override String GetPgmdesc( )
      {
         return "Audit" ;
      }

      protected void InitializeNonKey3P170( )
      {
         A57Usuario_PessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
         A1Usuario_Codigo = 0;
         n1Usuario_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         n1Usuario_Codigo = ((0==A1Usuario_Codigo) ? true : false);
         A1431AuditDate = (DateTime)(DateTime.MinValue);
         n1431AuditDate = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1431AuditDate", context.localUtil.TToC( A1431AuditDate, 8, 5, 0, 3, "/", ":", " "));
         n1431AuditDate = ((DateTime.MinValue==A1431AuditDate) ? true : false);
         A1432AuditTableName = "";
         n1432AuditTableName = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1432AuditTableName", A1432AuditTableName);
         n1432AuditTableName = (String.IsNullOrEmpty(StringUtil.RTrim( A1432AuditTableName)) ? true : false);
         A1434AuditDescription = "";
         n1434AuditDescription = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1434AuditDescription", A1434AuditDescription);
         n1434AuditDescription = (String.IsNullOrEmpty(StringUtil.RTrim( A1434AuditDescription)) ? true : false);
         A1433AuditShortDescription = "";
         n1433AuditShortDescription = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1433AuditShortDescription", A1433AuditShortDescription);
         n1433AuditShortDescription = (String.IsNullOrEmpty(StringUtil.RTrim( A1433AuditShortDescription)) ? true : false);
         A58Usuario_PessoaNom = "";
         n58Usuario_PessoaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
         A2Usuario_Nome = "";
         n2Usuario_Nome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
         A1435AuditAction = "";
         n1435AuditAction = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1435AuditAction", A1435AuditAction);
         n1435AuditAction = (String.IsNullOrEmpty(StringUtil.RTrim( A1435AuditAction)) ? true : false);
         Z1431AuditDate = (DateTime)(DateTime.MinValue);
         Z1432AuditTableName = "";
         Z1433AuditShortDescription = "";
         Z1435AuditAction = "";
         Z1Usuario_Codigo = 0;
      }

      protected void InitAll3P170( )
      {
         A1430AuditId = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1430AuditId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1430AuditId), 18, 0)));
         InitializeNonKey3P170( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117281663");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("audit.js", "?20203117281663");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockauditid_Internalname = "TEXTBLOCKAUDITID";
         edtAuditId_Internalname = "AUDITID";
         lblTextblockauditdate_Internalname = "TEXTBLOCKAUDITDATE";
         edtAuditDate_Internalname = "AUDITDATE";
         lblTextblockaudittablename_Internalname = "TEXTBLOCKAUDITTABLENAME";
         edtAuditTableName_Internalname = "AUDITTABLENAME";
         lblTextblockauditdescription_Internalname = "TEXTBLOCKAUDITDESCRIPTION";
         edtAuditDescription_Internalname = "AUDITDESCRIPTION";
         lblTextblockauditshortdescription_Internalname = "TEXTBLOCKAUDITSHORTDESCRIPTION";
         edtAuditShortDescription_Internalname = "AUDITSHORTDESCRIPTION";
         lblTextblockusuario_codigo_Internalname = "TEXTBLOCKUSUARIO_CODIGO";
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO";
         lblTextblockusuario_pessoanom_Internalname = "TEXTBLOCKUSUARIO_PESSOANOM";
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM";
         lblTextblockusuario_nome_Internalname = "TEXTBLOCKUSUARIO_NOME";
         edtUsuario_Nome_Internalname = "USUARIO_NOME";
         lblTextblockauditaction_Internalname = "TEXTBLOCKAUDITACTION";
         edtAuditAction_Internalname = "AUDITACTION";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         imgprompt_1_Internalname = "PROMPT_1";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Audit";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Audit";
         edtAuditAction_Jsonclick = "";
         edtAuditAction_Enabled = 1;
         edtUsuario_Nome_Jsonclick = "";
         edtUsuario_Nome_Enabled = 0;
         edtUsuario_PessoaNom_Jsonclick = "";
         edtUsuario_PessoaNom_Enabled = 0;
         imgprompt_1_Visible = 1;
         imgprompt_1_Link = "";
         edtUsuario_Codigo_Jsonclick = "";
         edtUsuario_Codigo_Enabled = 1;
         edtAuditShortDescription_Enabled = 1;
         edtAuditDescription_Enabled = 1;
         edtAuditTableName_Jsonclick = "";
         edtAuditTableName_Enabled = 1;
         edtAuditDate_Jsonclick = "";
         edtAuditDate_Enabled = 1;
         edtAuditId_Jsonclick = "";
         edtAuditId_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Usuario_codigo( int GX_Parm1 ,
                                        int GX_Parm2 ,
                                        String GX_Parm3 ,
                                        String GX_Parm4 )
      {
         A1Usuario_Codigo = GX_Parm1;
         n1Usuario_Codigo = false;
         A57Usuario_PessoaCod = GX_Parm2;
         A2Usuario_Nome = GX_Parm3;
         n2Usuario_Nome = false;
         A58Usuario_PessoaNom = GX_Parm4;
         n58Usuario_PessoaNom = false;
         /* Using cursor T003P15 */
         pr_default.execute(13, new Object[] {n1Usuario_Codigo, A1Usuario_Codigo});
         if ( (pr_default.getStatus(13) == 101) )
         {
            if ( ! ( (0==A1Usuario_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Usuario'.", "ForeignKeyNotFound", 1, "USUARIO_CODIGO");
               AnyError = 1;
               GX_FocusControl = edtUsuario_Codigo_Internalname;
            }
         }
         A57Usuario_PessoaCod = T003P15_A57Usuario_PessoaCod[0];
         A2Usuario_Nome = T003P15_A2Usuario_Nome[0];
         n2Usuario_Nome = T003P15_n2Usuario_Nome[0];
         pr_default.close(13);
         /* Using cursor T003P16 */
         pr_default.execute(14, new Object[] {A57Usuario_PessoaCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            if ( ! ( (0==A57Usuario_PessoaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Usuario_Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A58Usuario_PessoaNom = T003P16_A58Usuario_PessoaNom[0];
         n58Usuario_PessoaNom = T003P16_n58Usuario_PessoaNom[0];
         pr_default.close(14);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A57Usuario_PessoaCod = 0;
            A2Usuario_Nome = "";
            n2Usuario_Nome = false;
            A58Usuario_PessoaNom = "";
            n58Usuario_PessoaNom = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A2Usuario_Nome));
         isValidOutput.Add(StringUtil.RTrim( A58Usuario_PessoaNom));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7AuditId',fld:'vAUDITID',pic:'ZZZZZZZZZZZZZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E123P2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(13);
         pr_default.close(14);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1431AuditDate = (DateTime)(DateTime.MinValue);
         Z1432AuditTableName = "";
         Z1433AuditShortDescription = "";
         Z1435AuditAction = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockauditid_Jsonclick = "";
         lblTextblockauditdate_Jsonclick = "";
         A1431AuditDate = (DateTime)(DateTime.MinValue);
         lblTextblockaudittablename_Jsonclick = "";
         A1432AuditTableName = "";
         lblTextblockauditdescription_Jsonclick = "";
         A1434AuditDescription = "";
         lblTextblockauditshortdescription_Jsonclick = "";
         A1433AuditShortDescription = "";
         lblTextblockusuario_codigo_Jsonclick = "";
         lblTextblockusuario_pessoanom_Jsonclick = "";
         A58Usuario_PessoaNom = "";
         lblTextblockusuario_nome_Jsonclick = "";
         A2Usuario_Nome = "";
         lblTextblockauditaction_Jsonclick = "";
         A1435AuditAction = "";
         AV13Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode170 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1434AuditDescription = "";
         Z2Usuario_Nome = "";
         Z58Usuario_PessoaNom = "";
         T003P4_A57Usuario_PessoaCod = new int[1] ;
         T003P4_A2Usuario_Nome = new String[] {""} ;
         T003P4_n2Usuario_Nome = new bool[] {false} ;
         T003P5_A58Usuario_PessoaNom = new String[] {""} ;
         T003P5_n58Usuario_PessoaNom = new bool[] {false} ;
         T003P6_A57Usuario_PessoaCod = new int[1] ;
         T003P6_A1430AuditId = new long[1] ;
         T003P6_A1431AuditDate = new DateTime[] {DateTime.MinValue} ;
         T003P6_n1431AuditDate = new bool[] {false} ;
         T003P6_A1432AuditTableName = new String[] {""} ;
         T003P6_n1432AuditTableName = new bool[] {false} ;
         T003P6_A1434AuditDescription = new String[] {""} ;
         T003P6_n1434AuditDescription = new bool[] {false} ;
         T003P6_A1433AuditShortDescription = new String[] {""} ;
         T003P6_n1433AuditShortDescription = new bool[] {false} ;
         T003P6_A58Usuario_PessoaNom = new String[] {""} ;
         T003P6_n58Usuario_PessoaNom = new bool[] {false} ;
         T003P6_A2Usuario_Nome = new String[] {""} ;
         T003P6_n2Usuario_Nome = new bool[] {false} ;
         T003P6_A1435AuditAction = new String[] {""} ;
         T003P6_n1435AuditAction = new bool[] {false} ;
         T003P6_A1Usuario_Codigo = new int[1] ;
         T003P6_n1Usuario_Codigo = new bool[] {false} ;
         T003P7_A57Usuario_PessoaCod = new int[1] ;
         T003P7_A2Usuario_Nome = new String[] {""} ;
         T003P7_n2Usuario_Nome = new bool[] {false} ;
         T003P8_A58Usuario_PessoaNom = new String[] {""} ;
         T003P8_n58Usuario_PessoaNom = new bool[] {false} ;
         T003P9_A1430AuditId = new long[1] ;
         T003P3_A1430AuditId = new long[1] ;
         T003P3_A1431AuditDate = new DateTime[] {DateTime.MinValue} ;
         T003P3_n1431AuditDate = new bool[] {false} ;
         T003P3_A1432AuditTableName = new String[] {""} ;
         T003P3_n1432AuditTableName = new bool[] {false} ;
         T003P3_A1434AuditDescription = new String[] {""} ;
         T003P3_n1434AuditDescription = new bool[] {false} ;
         T003P3_A1433AuditShortDescription = new String[] {""} ;
         T003P3_n1433AuditShortDescription = new bool[] {false} ;
         T003P3_A1435AuditAction = new String[] {""} ;
         T003P3_n1435AuditAction = new bool[] {false} ;
         T003P3_A1Usuario_Codigo = new int[1] ;
         T003P3_n1Usuario_Codigo = new bool[] {false} ;
         T003P10_A1430AuditId = new long[1] ;
         T003P11_A1430AuditId = new long[1] ;
         T003P2_A1430AuditId = new long[1] ;
         T003P2_A1431AuditDate = new DateTime[] {DateTime.MinValue} ;
         T003P2_n1431AuditDate = new bool[] {false} ;
         T003P2_A1432AuditTableName = new String[] {""} ;
         T003P2_n1432AuditTableName = new bool[] {false} ;
         T003P2_A1434AuditDescription = new String[] {""} ;
         T003P2_n1434AuditDescription = new bool[] {false} ;
         T003P2_A1433AuditShortDescription = new String[] {""} ;
         T003P2_n1433AuditShortDescription = new bool[] {false} ;
         T003P2_A1435AuditAction = new String[] {""} ;
         T003P2_n1435AuditAction = new bool[] {false} ;
         T003P2_A1Usuario_Codigo = new int[1] ;
         T003P2_n1Usuario_Codigo = new bool[] {false} ;
         T003P12_A1430AuditId = new long[1] ;
         T003P15_A57Usuario_PessoaCod = new int[1] ;
         T003P15_A2Usuario_Nome = new String[] {""} ;
         T003P15_n2Usuario_Nome = new bool[] {false} ;
         T003P16_A58Usuario_PessoaNom = new String[] {""} ;
         T003P16_n58Usuario_PessoaNom = new bool[] {false} ;
         T003P17_A1430AuditId = new long[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.audit__default(),
            new Object[][] {
                new Object[] {
               T003P2_A1430AuditId, T003P2_A1431AuditDate, T003P2_n1431AuditDate, T003P2_A1432AuditTableName, T003P2_n1432AuditTableName, T003P2_A1434AuditDescription, T003P2_n1434AuditDescription, T003P2_A1433AuditShortDescription, T003P2_n1433AuditShortDescription, T003P2_A1435AuditAction,
               T003P2_n1435AuditAction, T003P2_A1Usuario_Codigo, T003P2_n1Usuario_Codigo
               }
               , new Object[] {
               T003P3_A1430AuditId, T003P3_A1431AuditDate, T003P3_n1431AuditDate, T003P3_A1432AuditTableName, T003P3_n1432AuditTableName, T003P3_A1434AuditDescription, T003P3_n1434AuditDescription, T003P3_A1433AuditShortDescription, T003P3_n1433AuditShortDescription, T003P3_A1435AuditAction,
               T003P3_n1435AuditAction, T003P3_A1Usuario_Codigo, T003P3_n1Usuario_Codigo
               }
               , new Object[] {
               T003P4_A57Usuario_PessoaCod, T003P4_A2Usuario_Nome, T003P4_n2Usuario_Nome
               }
               , new Object[] {
               T003P5_A58Usuario_PessoaNom, T003P5_n58Usuario_PessoaNom
               }
               , new Object[] {
               T003P6_A57Usuario_PessoaCod, T003P6_A1430AuditId, T003P6_A1431AuditDate, T003P6_n1431AuditDate, T003P6_A1432AuditTableName, T003P6_n1432AuditTableName, T003P6_A1434AuditDescription, T003P6_n1434AuditDescription, T003P6_A1433AuditShortDescription, T003P6_n1433AuditShortDescription,
               T003P6_A58Usuario_PessoaNom, T003P6_n58Usuario_PessoaNom, T003P6_A2Usuario_Nome, T003P6_n2Usuario_Nome, T003P6_A1435AuditAction, T003P6_n1435AuditAction, T003P6_A1Usuario_Codigo, T003P6_n1Usuario_Codigo
               }
               , new Object[] {
               T003P7_A57Usuario_PessoaCod, T003P7_A2Usuario_Nome, T003P7_n2Usuario_Nome
               }
               , new Object[] {
               T003P8_A58Usuario_PessoaNom, T003P8_n58Usuario_PessoaNom
               }
               , new Object[] {
               T003P9_A1430AuditId
               }
               , new Object[] {
               T003P10_A1430AuditId
               }
               , new Object[] {
               T003P11_A1430AuditId
               }
               , new Object[] {
               T003P12_A1430AuditId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003P15_A57Usuario_PessoaCod, T003P15_A2Usuario_Nome, T003P15_n2Usuario_Nome
               }
               , new Object[] {
               T003P16_A58Usuario_PessoaNom, T003P16_n58Usuario_PessoaNom
               }
               , new Object[] {
               T003P17_A1430AuditId
               }
            }
         );
         AV13Pgmname = "Audit";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound170 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1Usuario_Codigo ;
      private int N1Usuario_Codigo ;
      private int A1Usuario_Codigo ;
      private int A57Usuario_PessoaCod ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtAuditId_Enabled ;
      private int edtAuditDate_Enabled ;
      private int edtAuditTableName_Enabled ;
      private int edtAuditDescription_Enabled ;
      private int edtAuditShortDescription_Enabled ;
      private int edtUsuario_Codigo_Enabled ;
      private int imgprompt_1_Visible ;
      private int edtUsuario_PessoaNom_Enabled ;
      private int edtUsuario_Nome_Enabled ;
      private int edtAuditAction_Enabled ;
      private int AV11Insert_Usuario_Codigo ;
      private int AV14GXV1 ;
      private int Z57Usuario_PessoaCod ;
      private int idxLst ;
      private long wcpOAV7AuditId ;
      private long Z1430AuditId ;
      private long AV7AuditId ;
      private long A1430AuditId ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z1432AuditTableName ;
      private String Z1435AuditAction ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtAuditDate_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockauditid_Internalname ;
      private String lblTextblockauditid_Jsonclick ;
      private String edtAuditId_Internalname ;
      private String edtAuditId_Jsonclick ;
      private String lblTextblockauditdate_Internalname ;
      private String lblTextblockauditdate_Jsonclick ;
      private String edtAuditDate_Jsonclick ;
      private String lblTextblockaudittablename_Internalname ;
      private String lblTextblockaudittablename_Jsonclick ;
      private String edtAuditTableName_Internalname ;
      private String A1432AuditTableName ;
      private String edtAuditTableName_Jsonclick ;
      private String lblTextblockauditdescription_Internalname ;
      private String lblTextblockauditdescription_Jsonclick ;
      private String edtAuditDescription_Internalname ;
      private String lblTextblockauditshortdescription_Internalname ;
      private String lblTextblockauditshortdescription_Jsonclick ;
      private String edtAuditShortDescription_Internalname ;
      private String lblTextblockusuario_codigo_Internalname ;
      private String lblTextblockusuario_codigo_Jsonclick ;
      private String edtUsuario_Codigo_Internalname ;
      private String edtUsuario_Codigo_Jsonclick ;
      private String imgprompt_1_Internalname ;
      private String imgprompt_1_Link ;
      private String lblTextblockusuario_pessoanom_Internalname ;
      private String lblTextblockusuario_pessoanom_Jsonclick ;
      private String edtUsuario_PessoaNom_Internalname ;
      private String A58Usuario_PessoaNom ;
      private String edtUsuario_PessoaNom_Jsonclick ;
      private String lblTextblockusuario_nome_Internalname ;
      private String lblTextblockusuario_nome_Jsonclick ;
      private String edtUsuario_Nome_Internalname ;
      private String A2Usuario_Nome ;
      private String edtUsuario_Nome_Jsonclick ;
      private String lblTextblockauditaction_Internalname ;
      private String lblTextblockauditaction_Jsonclick ;
      private String edtAuditAction_Internalname ;
      private String A1435AuditAction ;
      private String edtAuditAction_Jsonclick ;
      private String AV13Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode170 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z2Usuario_Nome ;
      private String Z58Usuario_PessoaNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime Z1431AuditDate ;
      private DateTime A1431AuditDate ;
      private bool entryPointCalled ;
      private bool n1Usuario_Codigo ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1431AuditDate ;
      private bool n1432AuditTableName ;
      private bool n1434AuditDescription ;
      private bool n1433AuditShortDescription ;
      private bool n58Usuario_PessoaNom ;
      private bool n2Usuario_Nome ;
      private bool n1435AuditAction ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String A1434AuditDescription ;
      private String Z1434AuditDescription ;
      private String Z1433AuditShortDescription ;
      private String A1433AuditShortDescription ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T003P4_A57Usuario_PessoaCod ;
      private String[] T003P4_A2Usuario_Nome ;
      private bool[] T003P4_n2Usuario_Nome ;
      private String[] T003P5_A58Usuario_PessoaNom ;
      private bool[] T003P5_n58Usuario_PessoaNom ;
      private int[] T003P6_A57Usuario_PessoaCod ;
      private long[] T003P6_A1430AuditId ;
      private DateTime[] T003P6_A1431AuditDate ;
      private bool[] T003P6_n1431AuditDate ;
      private String[] T003P6_A1432AuditTableName ;
      private bool[] T003P6_n1432AuditTableName ;
      private String[] T003P6_A1434AuditDescription ;
      private bool[] T003P6_n1434AuditDescription ;
      private String[] T003P6_A1433AuditShortDescription ;
      private bool[] T003P6_n1433AuditShortDescription ;
      private String[] T003P6_A58Usuario_PessoaNom ;
      private bool[] T003P6_n58Usuario_PessoaNom ;
      private String[] T003P6_A2Usuario_Nome ;
      private bool[] T003P6_n2Usuario_Nome ;
      private String[] T003P6_A1435AuditAction ;
      private bool[] T003P6_n1435AuditAction ;
      private int[] T003P6_A1Usuario_Codigo ;
      private bool[] T003P6_n1Usuario_Codigo ;
      private int[] T003P7_A57Usuario_PessoaCod ;
      private String[] T003P7_A2Usuario_Nome ;
      private bool[] T003P7_n2Usuario_Nome ;
      private String[] T003P8_A58Usuario_PessoaNom ;
      private bool[] T003P8_n58Usuario_PessoaNom ;
      private long[] T003P9_A1430AuditId ;
      private long[] T003P3_A1430AuditId ;
      private DateTime[] T003P3_A1431AuditDate ;
      private bool[] T003P3_n1431AuditDate ;
      private String[] T003P3_A1432AuditTableName ;
      private bool[] T003P3_n1432AuditTableName ;
      private String[] T003P3_A1434AuditDescription ;
      private bool[] T003P3_n1434AuditDescription ;
      private String[] T003P3_A1433AuditShortDescription ;
      private bool[] T003P3_n1433AuditShortDescription ;
      private String[] T003P3_A1435AuditAction ;
      private bool[] T003P3_n1435AuditAction ;
      private int[] T003P3_A1Usuario_Codigo ;
      private bool[] T003P3_n1Usuario_Codigo ;
      private long[] T003P10_A1430AuditId ;
      private long[] T003P11_A1430AuditId ;
      private long[] T003P2_A1430AuditId ;
      private DateTime[] T003P2_A1431AuditDate ;
      private bool[] T003P2_n1431AuditDate ;
      private String[] T003P2_A1432AuditTableName ;
      private bool[] T003P2_n1432AuditTableName ;
      private String[] T003P2_A1434AuditDescription ;
      private bool[] T003P2_n1434AuditDescription ;
      private String[] T003P2_A1433AuditShortDescription ;
      private bool[] T003P2_n1433AuditShortDescription ;
      private String[] T003P2_A1435AuditAction ;
      private bool[] T003P2_n1435AuditAction ;
      private int[] T003P2_A1Usuario_Codigo ;
      private bool[] T003P2_n1Usuario_Codigo ;
      private long[] T003P12_A1430AuditId ;
      private int[] T003P15_A57Usuario_PessoaCod ;
      private String[] T003P15_A2Usuario_Nome ;
      private bool[] T003P15_n2Usuario_Nome ;
      private String[] T003P16_A58Usuario_PessoaNom ;
      private bool[] T003P16_n58Usuario_PessoaNom ;
      private long[] T003P17_A1430AuditId ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class audit__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003P6 ;
          prmT003P6 = new Object[] {
          new Object[] {"@AuditId",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmT003P4 ;
          prmT003P4 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003P5 ;
          prmT003P5 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003P7 ;
          prmT003P7 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003P8 ;
          prmT003P8 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003P9 ;
          prmT003P9 = new Object[] {
          new Object[] {"@AuditId",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmT003P3 ;
          prmT003P3 = new Object[] {
          new Object[] {"@AuditId",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmT003P10 ;
          prmT003P10 = new Object[] {
          new Object[] {"@AuditId",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmT003P11 ;
          prmT003P11 = new Object[] {
          new Object[] {"@AuditId",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmT003P2 ;
          prmT003P2 = new Object[] {
          new Object[] {"@AuditId",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmT003P12 ;
          prmT003P12 = new Object[] {
          new Object[] {"@AuditDate",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AuditTableName",SqlDbType.Char,50,0} ,
          new Object[] {"@AuditDescription",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AuditShortDescription",SqlDbType.VarChar,400,0} ,
          new Object[] {"@AuditAction",SqlDbType.Char,3,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003P13 ;
          prmT003P13 = new Object[] {
          new Object[] {"@AuditDate",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AuditTableName",SqlDbType.Char,50,0} ,
          new Object[] {"@AuditDescription",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@AuditShortDescription",SqlDbType.VarChar,400,0} ,
          new Object[] {"@AuditAction",SqlDbType.Char,3,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AuditId",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmT003P14 ;
          prmT003P14 = new Object[] {
          new Object[] {"@AuditId",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmT003P17 ;
          prmT003P17 = new Object[] {
          } ;
          Object[] prmT003P15 ;
          prmT003P15 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003P16 ;
          prmT003P16 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003P2", "SELECT [AuditId], [AuditDate], [AuditTableName], [AuditDescription], [AuditShortDescription], [AuditAction], [Usuario_Codigo] FROM [Audit] WITH (UPDLOCK) WHERE [AuditId] = @AuditId ",true, GxErrorMask.GX_NOMASK, false, this,prmT003P2,1,0,true,false )
             ,new CursorDef("T003P3", "SELECT [AuditId], [AuditDate], [AuditTableName], [AuditDescription], [AuditShortDescription], [AuditAction], [Usuario_Codigo] FROM [Audit] WITH (NOLOCK) WHERE [AuditId] = @AuditId ",true, GxErrorMask.GX_NOMASK, false, this,prmT003P3,1,0,true,false )
             ,new CursorDef("T003P4", "SELECT [Usuario_PessoaCod] AS Usuario_PessoaCod, [Usuario_Nome] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003P4,1,0,true,false )
             ,new CursorDef("T003P5", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003P5,1,0,true,false )
             ,new CursorDef("T003P6", "SELECT T2.[Usuario_PessoaCod] AS Usuario_PessoaCod, TM1.[AuditId], TM1.[AuditDate], TM1.[AuditTableName], TM1.[AuditDescription], TM1.[AuditShortDescription], T3.[Pessoa_Nome] AS Usuario_PessoaNom, T2.[Usuario_Nome], TM1.[AuditAction], TM1.[Usuario_Codigo] FROM (([Audit] TM1 WITH (NOLOCK) LEFT JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = TM1.[Usuario_Codigo]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE TM1.[AuditId] = @AuditId ORDER BY TM1.[AuditId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003P6,100,0,true,false )
             ,new CursorDef("T003P7", "SELECT [Usuario_PessoaCod] AS Usuario_PessoaCod, [Usuario_Nome] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003P7,1,0,true,false )
             ,new CursorDef("T003P8", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003P8,1,0,true,false )
             ,new CursorDef("T003P9", "SELECT [AuditId] FROM [Audit] WITH (NOLOCK) WHERE [AuditId] = @AuditId  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003P9,1,0,true,false )
             ,new CursorDef("T003P10", "SELECT TOP 1 [AuditId] FROM [Audit] WITH (NOLOCK) WHERE ( [AuditId] > @AuditId) ORDER BY [AuditId]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003P10,1,0,true,true )
             ,new CursorDef("T003P11", "SELECT TOP 1 [AuditId] FROM [Audit] WITH (NOLOCK) WHERE ( [AuditId] < @AuditId) ORDER BY [AuditId] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003P11,1,0,true,true )
             ,new CursorDef("T003P12", "INSERT INTO [Audit]([AuditDate], [AuditTableName], [AuditDescription], [AuditShortDescription], [AuditAction], [Usuario_Codigo]) VALUES(@AuditDate, @AuditTableName, @AuditDescription, @AuditShortDescription, @AuditAction, @Usuario_Codigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003P12)
             ,new CursorDef("T003P13", "UPDATE [Audit] SET [AuditDate]=@AuditDate, [AuditTableName]=@AuditTableName, [AuditDescription]=@AuditDescription, [AuditShortDescription]=@AuditShortDescription, [AuditAction]=@AuditAction, [Usuario_Codigo]=@Usuario_Codigo  WHERE [AuditId] = @AuditId", GxErrorMask.GX_NOMASK,prmT003P13)
             ,new CursorDef("T003P14", "DELETE FROM [Audit]  WHERE [AuditId] = @AuditId", GxErrorMask.GX_NOMASK,prmT003P14)
             ,new CursorDef("T003P15", "SELECT [Usuario_PessoaCod] AS Usuario_PessoaCod, [Usuario_Nome] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003P15,1,0,true,false )
             ,new CursorDef("T003P16", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003P16,1,0,true,false )
             ,new CursorDef("T003P17", "SELECT [AuditId] FROM [Audit] WITH (NOLOCK) ORDER BY [AuditId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003P17,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 3) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 3) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((long[]) buf[1])[0] = rslt.getLong(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 3) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 8 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 9 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 10 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[11]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[11]);
                }
                stmt.SetParameter(7, (long)parms[12]);
                return;
             case 12 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
