/*
               File: PRC_NovaPessoaContratada
        Description: Nova Pessoa Contratada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:15:41.21
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_novapessoacontratada : GXProcedure
   {
      public prc_novapessoacontratada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_novapessoacontratada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_PessoaCod ,
                           String aP1_Contratada_PessoaCNPJ ,
                           String aP2_Contratada_PessoaNom ,
                           String aP3_Pessoa_IE ,
                           String aP4_Pessoa_Endereco ,
                           String aP5_Pessoa_CEP ,
                           String aP6_Pessoa_Telefone ,
                           String aP7_Pessoa_Fax ,
                           int aP8_Pessoa_MunicipioCod ,
                           int aP9_AreaTrabalho_Codigo ,
                           out int aP10_Pessoa_Codigo )
      {
         this.AV23Contratada_PessoaCod = aP0_Contratada_PessoaCod;
         this.AV10Contratada_PessoaCNPJ = aP1_Contratada_PessoaCNPJ;
         this.AV11Contratada_PessoaNom = aP2_Contratada_PessoaNom;
         this.AV16Pessoa_IE = aP3_Pessoa_IE;
         this.AV17Pessoa_Endereco = aP4_Pessoa_Endereco;
         this.AV18Pessoa_CEP = aP5_Pessoa_CEP;
         this.AV19Pessoa_Telefone = aP6_Pessoa_Telefone;
         this.AV20Pessoa_Fax = aP7_Pessoa_Fax;
         this.AV21Pessoa_MunicipioCod = aP8_Pessoa_MunicipioCod;
         this.AV24AreaTrabalho_Codigo = aP9_AreaTrabalho_Codigo;
         this.AV15Pessoa_Codigo = 0 ;
         initialize();
         executePrivate();
         aP10_Pessoa_Codigo=this.AV15Pessoa_Codigo;
      }

      public int executeUdp( int aP0_Contratada_PessoaCod ,
                             String aP1_Contratada_PessoaCNPJ ,
                             String aP2_Contratada_PessoaNom ,
                             String aP3_Pessoa_IE ,
                             String aP4_Pessoa_Endereco ,
                             String aP5_Pessoa_CEP ,
                             String aP6_Pessoa_Telefone ,
                             String aP7_Pessoa_Fax ,
                             int aP8_Pessoa_MunicipioCod ,
                             int aP9_AreaTrabalho_Codigo )
      {
         this.AV23Contratada_PessoaCod = aP0_Contratada_PessoaCod;
         this.AV10Contratada_PessoaCNPJ = aP1_Contratada_PessoaCNPJ;
         this.AV11Contratada_PessoaNom = aP2_Contratada_PessoaNom;
         this.AV16Pessoa_IE = aP3_Pessoa_IE;
         this.AV17Pessoa_Endereco = aP4_Pessoa_Endereco;
         this.AV18Pessoa_CEP = aP5_Pessoa_CEP;
         this.AV19Pessoa_Telefone = aP6_Pessoa_Telefone;
         this.AV20Pessoa_Fax = aP7_Pessoa_Fax;
         this.AV21Pessoa_MunicipioCod = aP8_Pessoa_MunicipioCod;
         this.AV24AreaTrabalho_Codigo = aP9_AreaTrabalho_Codigo;
         this.AV15Pessoa_Codigo = 0 ;
         initialize();
         executePrivate();
         aP10_Pessoa_Codigo=this.AV15Pessoa_Codigo;
         return AV15Pessoa_Codigo ;
      }

      public void executeSubmit( int aP0_Contratada_PessoaCod ,
                                 String aP1_Contratada_PessoaCNPJ ,
                                 String aP2_Contratada_PessoaNom ,
                                 String aP3_Pessoa_IE ,
                                 String aP4_Pessoa_Endereco ,
                                 String aP5_Pessoa_CEP ,
                                 String aP6_Pessoa_Telefone ,
                                 String aP7_Pessoa_Fax ,
                                 int aP8_Pessoa_MunicipioCod ,
                                 int aP9_AreaTrabalho_Codigo ,
                                 out int aP10_Pessoa_Codigo )
      {
         prc_novapessoacontratada objprc_novapessoacontratada;
         objprc_novapessoacontratada = new prc_novapessoacontratada();
         objprc_novapessoacontratada.AV23Contratada_PessoaCod = aP0_Contratada_PessoaCod;
         objprc_novapessoacontratada.AV10Contratada_PessoaCNPJ = aP1_Contratada_PessoaCNPJ;
         objprc_novapessoacontratada.AV11Contratada_PessoaNom = aP2_Contratada_PessoaNom;
         objprc_novapessoacontratada.AV16Pessoa_IE = aP3_Pessoa_IE;
         objprc_novapessoacontratada.AV17Pessoa_Endereco = aP4_Pessoa_Endereco;
         objprc_novapessoacontratada.AV18Pessoa_CEP = aP5_Pessoa_CEP;
         objprc_novapessoacontratada.AV19Pessoa_Telefone = aP6_Pessoa_Telefone;
         objprc_novapessoacontratada.AV20Pessoa_Fax = aP7_Pessoa_Fax;
         objprc_novapessoacontratada.AV21Pessoa_MunicipioCod = aP8_Pessoa_MunicipioCod;
         objprc_novapessoacontratada.AV24AreaTrabalho_Codigo = aP9_AreaTrabalho_Codigo;
         objprc_novapessoacontratada.AV15Pessoa_Codigo = 0 ;
         objprc_novapessoacontratada.context.SetSubmitInitialConfig(context);
         objprc_novapessoacontratada.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_novapessoacontratada);
         aP10_Pessoa_Codigo=this.AV15Pessoa_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_novapessoacontratada)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( (0==AV23Contratada_PessoaCod) )
         {
            AV12Pessoa = new SdtPessoa(context);
            if ( StringUtil.StrCmp(AV10Contratada_PessoaCNPJ, "Interno") == 0 )
            {
               /* Execute user subroutine: 'CONTRATADA_PESSOACNPJ' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
            else
            {
               AV12Pessoa.gxTpr_Pessoa_docto = AV10Contratada_PessoaCNPJ;
            }
         }
         else
         {
            AV12Pessoa.Load(AV23Contratada_PessoaCod);
            AV12Pessoa.gxTpr_Pessoa_docto = AV10Contratada_PessoaCNPJ;
         }
         AV12Pessoa.gxTpr_Pessoa_nome = AV11Contratada_PessoaNom;
         AV12Pessoa.gxTpr_Pessoa_tipopessoa = "J";
         AV12Pessoa.gxTpr_Pessoa_ie = AV16Pessoa_IE;
         AV12Pessoa.gxTpr_Pessoa_endereco = AV17Pessoa_Endereco;
         AV12Pessoa.gxTpr_Pessoa_cep = AV18Pessoa_CEP;
         AV12Pessoa.gxTpr_Pessoa_telefone = AV19Pessoa_Telefone;
         AV12Pessoa.gxTpr_Pessoa_fax = AV20Pessoa_Fax;
         if ( (0==AV21Pessoa_MunicipioCod) )
         {
            AV12Pessoa.gxTv_SdtPessoa_Pessoa_municipiocod_SetNull();
         }
         else
         {
            AV12Pessoa.gxTpr_Pessoa_municipiocod = AV21Pessoa_MunicipioCod;
         }
         AV12Pessoa.gxTpr_Pessoa_ativo = true;
         AV12Pessoa.Save();
         if ( AV12Pessoa.Success() )
         {
            context.CommitDataStores( "PRC_NovaPessoaContratada");
            AV15Pessoa_Codigo = AV12Pessoa.gxTpr_Pessoa_codigo;
         }
         else
         {
            context.RollbackDataStores( "PRC_NovaPessoaContratada");
         }
         this.cleanup();
         if (true) return;
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'CONTRATADA_PESSOACNPJ' Routine */
         /* Optimized group. */
         /* Using cursor P001O2 */
         pr_default.execute(0, new Object[] {AV24AreaTrabalho_Codigo});
         cV25Qtd = P001O2_AV25Qtd[0];
         pr_default.close(0);
         AV25Qtd = (short)(AV25Qtd+cV25Qtd*1);
         /* End optimized group. */
         AV25Qtd = (short)(AV25Qtd+1);
         AV12Pessoa.gxTpr_Pessoa_docto = StringUtil.Trim( StringUtil.Str( (decimal)(AV24AreaTrabalho_Codigo), 6, 0))+"-"+StringUtil.Trim( StringUtil.Str( (decimal)(AV25Qtd), 4, 0));
         AV25Qtd = 0;
         /* Using cursor P001O3 */
         pr_default.execute(1, new Object[] {AV12Pessoa.gxTpr_Pessoa_docto});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A37Pessoa_Docto = P001O3_A37Pessoa_Docto[0];
            A34Pessoa_Codigo = P001O3_A34Pessoa_Codigo[0];
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P001O3_A37Pessoa_Docto[0], A37Pessoa_Docto) == 0 ) )
            {
               A34Pessoa_Codigo = P001O3_A34Pessoa_Codigo[0];
               AV25Qtd = (short)(AV25Qtd+1);
               /* Exiting from a For First loop. */
               if (true) break;
            }
            AV12Pessoa.gxTpr_Pessoa_docto = AV12Pessoa.gxTpr_Pessoa_docto+StringUtil.Trim( StringUtil.Str( (decimal)(AV25Qtd), 4, 0));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV12Pessoa = new SdtPessoa(context);
         scmdbuf = "";
         P001O2_AV25Qtd = new short[1] ;
         P001O3_A37Pessoa_Docto = new String[] {""} ;
         P001O3_A34Pessoa_Codigo = new int[1] ;
         A37Pessoa_Docto = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_novapessoacontratada__default(),
            new Object[][] {
                new Object[] {
               P001O2_AV25Qtd
               }
               , new Object[] {
               P001O3_A37Pessoa_Docto, P001O3_A34Pessoa_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short cV25Qtd ;
      private short AV25Qtd ;
      private int AV23Contratada_PessoaCod ;
      private int AV21Pessoa_MunicipioCod ;
      private int AV24AreaTrabalho_Codigo ;
      private int AV15Pessoa_Codigo ;
      private int A34Pessoa_Codigo ;
      private String AV11Contratada_PessoaNom ;
      private String AV16Pessoa_IE ;
      private String AV18Pessoa_CEP ;
      private String AV19Pessoa_Telefone ;
      private String AV20Pessoa_Fax ;
      private String scmdbuf ;
      private bool returnInSub ;
      private String AV10Contratada_PessoaCNPJ ;
      private String AV17Pessoa_Endereco ;
      private String A37Pessoa_Docto ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P001O2_AV25Qtd ;
      private String[] P001O3_A37Pessoa_Docto ;
      private int[] P001O3_A34Pessoa_Codigo ;
      private int aP10_Pessoa_Codigo ;
      private SdtPessoa AV12Pessoa ;
   }

   public class prc_novapessoacontratada__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP001O2 ;
          prmP001O2 = new Object[] {
          new Object[] {"@AV24AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP001O3 ;
          prmP001O3 = new Object[] {
          new Object[] {"@AV12Pessoa__Pessoa_docto",SqlDbType.VarChar,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P001O2", "SELECT COUNT(*) FROM [Contratada] WITH (NOLOCK) WHERE ([Contratada_AreaTrabalhoCod] = @AV24AreaTrabalho_Codigo) AND ([Contratada_TipoFabrica] = 'I') ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001O2,1,0,true,false )
             ,new CursorDef("P001O3", "SELECT [Pessoa_Docto], [Pessoa_Codigo] FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Docto] = @AV12Pessoa__Pessoa_docto ORDER BY [Pessoa_Docto] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001O3,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
       }
    }

 }

}
