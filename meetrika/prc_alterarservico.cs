/*
               File: PRC_AlterarServico
        Description: Alterar Servico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:6:20.97
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_alterarservico : GXProcedure
   {
      public prc_alterarservico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_alterarservico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           int aP1_ContratoServicos_Codigo ,
                           ref String aP2_Observacao ,
                           int aP3_UserId ,
                           String aP4_novoSrvSigla )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV62ContratoServicos_Codigo = aP1_ContratoServicos_Codigo;
         this.AV11Observacao = aP2_Observacao;
         this.AV9UserId = aP3_UserId;
         this.AV41novoSrvSigla = aP4_novoSrvSigla;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP2_Observacao=this.AV11Observacao;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 int aP1_ContratoServicos_Codigo ,
                                 ref String aP2_Observacao ,
                                 int aP3_UserId ,
                                 String aP4_novoSrvSigla )
      {
         prc_alterarservico objprc_alterarservico;
         objprc_alterarservico = new prc_alterarservico();
         objprc_alterarservico.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_alterarservico.AV62ContratoServicos_Codigo = aP1_ContratoServicos_Codigo;
         objprc_alterarservico.AV11Observacao = aP2_Observacao;
         objprc_alterarservico.AV9UserId = aP3_UserId;
         objprc_alterarservico.AV41novoSrvSigla = aP4_novoSrvSigla;
         objprc_alterarservico.context.SetSubmitInitialConfig(context);
         objprc_alterarservico.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_alterarservico);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP2_Observacao=this.AV11Observacao;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_alterarservico)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV66GXLvl2 = 0;
         /* Using cursor P009A2 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P009A2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P009A2_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P009A2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P009A2_n601ContagemResultado_Servico[0];
            A892LogResponsavel_DemandaCod = P009A2_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = P009A2_n892LogResponsavel_DemandaCod[0];
            A1130LogResponsavel_Status = P009A2_A1130LogResponsavel_Status[0];
            n1130LogResponsavel_Status = P009A2_n1130LogResponsavel_Status[0];
            A1234LogResponsavel_NovoStatus = P009A2_A1234LogResponsavel_NovoStatus[0];
            n1234LogResponsavel_NovoStatus = P009A2_n1234LogResponsavel_NovoStatus[0];
            A896LogResponsavel_Owner = P009A2_A896LogResponsavel_Owner[0];
            A1177LogResponsavel_Prazo = P009A2_A1177LogResponsavel_Prazo[0];
            n1177LogResponsavel_Prazo = P009A2_n1177LogResponsavel_Prazo[0];
            A484ContagemResultado_StatusDmn = P009A2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P009A2_n484ContagemResultado_StatusDmn[0];
            A52Contratada_AreaTrabalhoCod = P009A2_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P009A2_n52Contratada_AreaTrabalhoCod[0];
            A490ContagemResultado_ContratadaCod = P009A2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P009A2_n490ContagemResultado_ContratadaCod[0];
            A801ContagemResultado_ServicoSigla = P009A2_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P009A2_n801ContagemResultado_ServicoSigla[0];
            A493ContagemResultado_DemandaFM = P009A2_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P009A2_n493ContagemResultado_DemandaFM[0];
            A912ContagemResultado_HoraEntrega = P009A2_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P009A2_n912ContagemResultado_HoraEntrega[0];
            A1611ContagemResultado_PrzTpDias = P009A2_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P009A2_n1611ContagemResultado_PrzTpDias[0];
            A1650ContagemResultado_PrzInc = P009A2_A1650ContagemResultado_PrzInc[0];
            n1650ContagemResultado_PrzInc = P009A2_n1650ContagemResultado_PrzInc[0];
            A1603ContagemResultado_CntCod = P009A2_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P009A2_n1603ContagemResultado_CntCod[0];
            A798ContagemResultado_PFBFSImp = P009A2_A798ContagemResultado_PFBFSImp[0];
            n798ContagemResultado_PFBFSImp = P009A2_n798ContagemResultado_PFBFSImp[0];
            A1797LogResponsavel_Codigo = P009A2_A1797LogResponsavel_Codigo[0];
            A1553ContagemResultado_CntSrvCod = P009A2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P009A2_n1553ContagemResultado_CntSrvCod[0];
            A484ContagemResultado_StatusDmn = P009A2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P009A2_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = P009A2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P009A2_n490ContagemResultado_ContratadaCod[0];
            A493ContagemResultado_DemandaFM = P009A2_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P009A2_n493ContagemResultado_DemandaFM[0];
            A912ContagemResultado_HoraEntrega = P009A2_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P009A2_n912ContagemResultado_HoraEntrega[0];
            A798ContagemResultado_PFBFSImp = P009A2_A798ContagemResultado_PFBFSImp[0];
            n798ContagemResultado_PFBFSImp = P009A2_n798ContagemResultado_PFBFSImp[0];
            A601ContagemResultado_Servico = P009A2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P009A2_n601ContagemResultado_Servico[0];
            A1611ContagemResultado_PrzTpDias = P009A2_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P009A2_n1611ContagemResultado_PrzTpDias[0];
            A1650ContagemResultado_PrzInc = P009A2_A1650ContagemResultado_PrzInc[0];
            n1650ContagemResultado_PrzInc = P009A2_n1650ContagemResultado_PrzInc[0];
            A1603ContagemResultado_CntCod = P009A2_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P009A2_n1603ContagemResultado_CntCod[0];
            A801ContagemResultado_ServicoSigla = P009A2_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P009A2_n801ContagemResultado_ServicoSigla[0];
            A52Contratada_AreaTrabalhoCod = P009A2_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P009A2_n52Contratada_AreaTrabalhoCod[0];
            /* Using cursor P009A4 */
            pr_default.execute(1);
            if ( (pr_default.getStatus(1) != 101) )
            {
               A531ContagemResultado_StatusUltCnt = P009A4_A531ContagemResultado_StatusUltCnt[0];
               A684ContagemResultado_PFBFSUltima = P009A4_A684ContagemResultado_PFBFSUltima[0];
            }
            else
            {
               A531ContagemResultado_StatusUltCnt = 0;
               A684ContagemResultado_PFBFSUltima = 0;
            }
            pr_default.close(1);
            OV63PrazoInicio = AV63PrazoInicio;
            AV66GXLvl2 = 1;
            AV13StatusAnterior = A1130LogResponsavel_Status;
            AV14UsuarioAnterior = A896LogResponsavel_Owner;
            AV12Prazo = A1177LogResponsavel_Prazo;
            AV10StatusDemanda = A484ContagemResultado_StatusDmn;
            AV36AreaTrabalho = A52Contratada_AreaTrabalhoCod;
            AV15Contratada = A490ContagemResultado_ContratadaCod;
            AV40ServicoAnterior = A801ContagemResultado_ServicoSigla;
            AV43DemandaFM = A493ContagemResultado_DemandaFM;
            AV61HoraEntrega = A912ContagemResultado_HoraEntrega;
            AV60TipoDias = A1611ContagemResultado_PrzTpDias;
            AV63PrazoInicio = A1650ContagemResultado_PrzInc;
            AV48Contrato_Codigo = A1603ContagemResultado_CntCod;
            if ( ( A684ContagemResultado_PFBFSUltima > Convert.ToDecimal( 0 )) && ( A531ContagemResultado_StatusUltCnt == 5 ) )
            {
               AV32PFBFS = A684ContagemResultado_PFBFSUltima;
            }
            else
            {
               AV32PFBFS = A798ContagemResultado_PFBFSImp;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         pr_default.close(1);
         if ( AV66GXLvl2 == 0 )
         {
            /* Using cursor P009A6 */
            pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P009A6_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P009A6_n1553ContagemResultado_CntSrvCod[0];
               A601ContagemResultado_Servico = P009A6_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P009A6_n601ContagemResultado_Servico[0];
               A484ContagemResultado_StatusDmn = P009A6_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P009A6_n484ContagemResultado_StatusDmn[0];
               A52Contratada_AreaTrabalhoCod = P009A6_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P009A6_n52Contratada_AreaTrabalhoCod[0];
               A490ContagemResultado_ContratadaCod = P009A6_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P009A6_n490ContagemResultado_ContratadaCod[0];
               A801ContagemResultado_ServicoSigla = P009A6_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = P009A6_n801ContagemResultado_ServicoSigla[0];
               A493ContagemResultado_DemandaFM = P009A6_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P009A6_n493ContagemResultado_DemandaFM[0];
               A912ContagemResultado_HoraEntrega = P009A6_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = P009A6_n912ContagemResultado_HoraEntrega[0];
               A1611ContagemResultado_PrzTpDias = P009A6_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P009A6_n1611ContagemResultado_PrzTpDias[0];
               A1650ContagemResultado_PrzInc = P009A6_A1650ContagemResultado_PrzInc[0];
               n1650ContagemResultado_PrzInc = P009A6_n1650ContagemResultado_PrzInc[0];
               A1603ContagemResultado_CntCod = P009A6_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P009A6_n1603ContagemResultado_CntCod[0];
               A798ContagemResultado_PFBFSImp = P009A6_A798ContagemResultado_PFBFSImp[0];
               n798ContagemResultado_PFBFSImp = P009A6_n798ContagemResultado_PFBFSImp[0];
               A531ContagemResultado_StatusUltCnt = P009A6_A531ContagemResultado_StatusUltCnt[0];
               A684ContagemResultado_PFBFSUltima = P009A6_A684ContagemResultado_PFBFSUltima[0];
               A601ContagemResultado_Servico = P009A6_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P009A6_n601ContagemResultado_Servico[0];
               A1611ContagemResultado_PrzTpDias = P009A6_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P009A6_n1611ContagemResultado_PrzTpDias[0];
               A1650ContagemResultado_PrzInc = P009A6_A1650ContagemResultado_PrzInc[0];
               n1650ContagemResultado_PrzInc = P009A6_n1650ContagemResultado_PrzInc[0];
               A1603ContagemResultado_CntCod = P009A6_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P009A6_n1603ContagemResultado_CntCod[0];
               A801ContagemResultado_ServicoSigla = P009A6_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = P009A6_n801ContagemResultado_ServicoSigla[0];
               A52Contratada_AreaTrabalhoCod = P009A6_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P009A6_n52Contratada_AreaTrabalhoCod[0];
               A531ContagemResultado_StatusUltCnt = P009A6_A531ContagemResultado_StatusUltCnt[0];
               A684ContagemResultado_PFBFSUltima = P009A6_A684ContagemResultado_PFBFSUltima[0];
               OV63PrazoInicio = AV63PrazoInicio;
               AV10StatusDemanda = A484ContagemResultado_StatusDmn;
               AV36AreaTrabalho = A52Contratada_AreaTrabalhoCod;
               AV15Contratada = A490ContagemResultado_ContratadaCod;
               AV40ServicoAnterior = A801ContagemResultado_ServicoSigla;
               AV43DemandaFM = A493ContagemResultado_DemandaFM;
               AV61HoraEntrega = A912ContagemResultado_HoraEntrega;
               AV60TipoDias = A1611ContagemResultado_PrzTpDias;
               AV63PrazoInicio = A1650ContagemResultado_PrzInc;
               AV48Contrato_Codigo = A1603ContagemResultado_CntCod;
               if ( ( A684ContagemResultado_PFBFSUltima > Convert.ToDecimal( 0 )) && ( A531ContagemResultado_StatusUltCnt == 5 ) )
               {
                  AV32PFBFS = A684ContagemResultado_PFBFSUltima;
               }
               else
               {
                  AV32PFBFS = A798ContagemResultado_PFBFSImp;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
         }
         AV11Observacao = StringUtil.Trim( AV11Observacao) + " (Servi�o de " + AV40ServicoAnterior + " para " + AV41novoSrvSigla + ")";
         /* Execute user subroutine: 'PRAZOS' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         new prc_inslogresponsavel(context ).execute( ref  A456ContagemResultado_Codigo,  0,  "U",  "D",  AV9UserId,  0,  AV10StatusDemanda,  AV10StatusDemanda,  AV11Observacao,  AV12Prazo,  false) ;
         /* Execute user subroutine: 'GETEMAILS' */
         S131 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13StatusAnterior)) )
         {
            /* Using cursor P009A7 */
            pr_default.execute(3, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P009A7_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P009A7_n1553ContagemResultado_CntSrvCod[0];
               A1237ContagemResultado_PrazoMaisDias = P009A7_A1237ContagemResultado_PrazoMaisDias[0];
               n1237ContagemResultado_PrazoMaisDias = P009A7_n1237ContagemResultado_PrazoMaisDias[0];
               A472ContagemResultado_DataEntrega = P009A7_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P009A7_n472ContagemResultado_DataEntrega[0];
               A912ContagemResultado_HoraEntrega = P009A7_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = P009A7_n912ContagemResultado_HoraEntrega[0];
               A1351ContagemResultado_DataPrevista = P009A7_A1351ContagemResultado_DataPrevista[0];
               n1351ContagemResultado_DataPrevista = P009A7_n1351ContagemResultado_DataPrevista[0];
               A1553ContagemResultado_CntSrvCod = AV62ContratoServicos_Codigo;
               n1553ContagemResultado_CntSrvCod = false;
               A1237ContagemResultado_PrazoMaisDias = (short)(A1237ContagemResultado_PrazoMaisDias+AV31Dias);
               n1237ContagemResultado_PrazoMaisDias = false;
               A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV35DataEntrega);
               n472ContagemResultado_DataEntrega = false;
               A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(AV35DataEntrega);
               n912ContagemResultado_HoraEntrega = false;
               A1351ContagemResultado_DataPrevista = AV12Prazo;
               n1351ContagemResultado_DataPrevista = false;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P009A8 */
               pr_default.execute(4, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod, n1237ContagemResultado_PrazoMaisDias, A1237ContagemResultado_PrazoMaisDias, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, A456ContagemResultado_Codigo});
               pr_default.close(4);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               if (true) break;
               /* Using cursor P009A9 */
               pr_default.execute(5, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod, n1237ContagemResultado_PrazoMaisDias, A1237ContagemResultado_PrazoMaisDias, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, A456ContagemResultado_Codigo});
               pr_default.close(5);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
         }
         else
         {
            AV13StatusAnterior = "E";
            /* Using cursor P009A10 */
            pr_default.execute(6, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A1603ContagemResultado_CntCod = P009A10_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P009A10_n1603ContagemResultado_CntCod[0];
               A1553ContagemResultado_CntSrvCod = P009A10_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P009A10_n1553ContagemResultado_CntSrvCod[0];
               A484ContagemResultado_StatusDmn = P009A10_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P009A10_n484ContagemResultado_StatusDmn[0];
               A1237ContagemResultado_PrazoMaisDias = P009A10_A1237ContagemResultado_PrazoMaisDias[0];
               n1237ContagemResultado_PrazoMaisDias = P009A10_n1237ContagemResultado_PrazoMaisDias[0];
               A472ContagemResultado_DataEntrega = P009A10_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = P009A10_n472ContagemResultado_DataEntrega[0];
               A912ContagemResultado_HoraEntrega = P009A10_A912ContagemResultado_HoraEntrega[0];
               n912ContagemResultado_HoraEntrega = P009A10_n912ContagemResultado_HoraEntrega[0];
               A1351ContagemResultado_DataPrevista = P009A10_A1351ContagemResultado_DataPrevista[0];
               n1351ContagemResultado_DataPrevista = P009A10_n1351ContagemResultado_DataPrevista[0];
               A890ContagemResultado_Responsavel = P009A10_A890ContagemResultado_Responsavel[0];
               n890ContagemResultado_Responsavel = P009A10_n890ContagemResultado_Responsavel[0];
               A1604ContagemResultado_CntPrpCod = P009A10_A1604ContagemResultado_CntPrpCod[0];
               n1604ContagemResultado_CntPrpCod = P009A10_n1604ContagemResultado_CntPrpCod[0];
               A1603ContagemResultado_CntCod = P009A10_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P009A10_n1603ContagemResultado_CntCod[0];
               A1604ContagemResultado_CntPrpCod = P009A10_A1604ContagemResultado_CntPrpCod[0];
               n1604ContagemResultado_CntPrpCod = P009A10_n1604ContagemResultado_CntPrpCod[0];
               A1553ContagemResultado_CntSrvCod = AV62ContratoServicos_Codigo;
               n1553ContagemResultado_CntSrvCod = false;
               A484ContagemResultado_StatusDmn = AV13StatusAnterior;
               n484ContagemResultado_StatusDmn = false;
               A1237ContagemResultado_PrazoMaisDias = (short)(A1237ContagemResultado_PrazoMaisDias+AV31Dias);
               n1237ContagemResultado_PrazoMaisDias = false;
               A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV35DataEntrega);
               n472ContagemResultado_DataEntrega = false;
               A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(AV35DataEntrega);
               n912ContagemResultado_HoraEntrega = false;
               A1351ContagemResultado_DataPrevista = AV12Prazo;
               n1351ContagemResultado_DataPrevista = false;
               if ( AV14UsuarioAnterior > 0 )
               {
                  A890ContagemResultado_Responsavel = AV14UsuarioAnterior;
                  n890ContagemResultado_Responsavel = false;
                  AV39Usuarios.Add(AV14UsuarioAnterior, 0);
               }
               else
               {
                  AV14UsuarioAnterior = A1604ContagemResultado_CntPrpCod;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P009A11 */
               pr_default.execute(7, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n1237ContagemResultado_PrazoMaisDias, A1237ContagemResultado_PrazoMaisDias, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, A456ContagemResultado_Codigo});
               pr_default.close(7);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               if (true) break;
               /* Using cursor P009A12 */
               pr_default.execute(8, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n1237ContagemResultado_PrazoMaisDias, A1237ContagemResultado_PrazoMaisDias, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, A456ContagemResultado_Codigo});
               pr_default.close(8);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(6);
            new prc_inslogresponsavel(context ).execute( ref  A456ContagemResultado_Codigo,  AV14UsuarioAnterior,  "E",  "D",  AV9UserId,  0,  AV10StatusDemanda,  AV13StatusAnterior,  "",  AV35DataEntrega,  false) ;
         }
         /* Execute user subroutine: 'ENVIONOTIFICACAO' */
         S121 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         this.cleanup();
         if (true) return;
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PRAZOS' Routine */
         AV12Prazo = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
         if ( AV63PrazoInicio == 2 )
         {
            new prc_datadeinicioutil(context ).execute( ref  AV12Prazo) ;
         }
         GXt_int1 = AV38DiasAnalise;
         new prc_diasparaanalise(context ).execute( ref  AV62ContratoServicos_Codigo,  AV32PFBFS,  0, out  GXt_int1) ;
         AV38DiasAnalise = GXt_int1;
         GXt_int1 = AV31Dias;
         new prc_diasparaentrega(context ).execute( ref  AV62ContratoServicos_Codigo,  AV32PFBFS,  0, out  GXt_int1) ;
         AV31Dias = GXt_int1;
         if ( DateTimeUtil.Hour( AV61HoraEntrega) + DateTimeUtil.Minute( AV61HoraEntrega) + DateTimeUtil.Second( AV61HoraEntrega) == 0 )
         {
            new prc_gethorasentrega(context ).execute(  AV36AreaTrabalho,  0,  AV62ContratoServicos_Codigo, out  AV33Hours, out  AV34Minutes) ;
            AV12Prazo = DateTimeUtil.TAdd( AV12Prazo, 3600*(AV33Hours));
            AV12Prazo = DateTimeUtil.TAdd( AV12Prazo, 60*(AV34Minutes));
         }
         else
         {
            AV12Prazo = DateTimeUtil.TAdd( AV12Prazo, 3600*(DateTimeUtil.Hour( AV61HoraEntrega)));
            AV12Prazo = DateTimeUtil.TAdd( AV12Prazo, 60*(DateTimeUtil.Minute( AV61HoraEntrega)));
         }
         AV35DataEntrega = AV12Prazo;
         GXt_dtime2 = AV12Prazo;
         new prc_adddiasuteis(context ).execute(  AV12Prazo,  AV31Dias,  AV60TipoDias, out  GXt_dtime2) ;
         AV12Prazo = GXt_dtime2;
         if ( StringUtil.StrCmp(AV13StatusAnterior, "E") == 0 )
         {
            GXt_dtime2 = AV35DataEntrega;
            new prc_adddiasuteis(context ).execute(  AV35DataEntrega,  AV38DiasAnalise,  AV60TipoDias, out  GXt_dtime2) ;
            AV35DataEntrega = GXt_dtime2;
         }
         else
         {
            AV35DataEntrega = AV12Prazo;
         }
      }

      protected void S121( )
      {
         /* 'ENVIONOTIFICACAO' Routine */
         AV50EmailText = StringUtil.NewLine( ) + "Prezado(a)," + StringUtil.NewLine( );
         AV50EmailText = AV50EmailText + "Houve uma altera��o de Servi�o, ficando com prazo previsto de entrega para " + context.localUtil.TToC( AV12Prazo, 8, 5, 0, 3, "/", ":", " ") + StringUtil.NewLine( );
         AV50EmailText = AV50EmailText + "Observa��o: " + AV11Observacao + StringUtil.NewLine( );
         AV50EmailText = AV50EmailText + StringUtil.NewLine( ) + StringUtil.Trim( AV52WWPContext.gxTpr_Areatrabalho_descricao) + ", usu�rio " + StringUtil.Trim( AV52WWPContext.gxTpr_Username) + StringUtil.NewLine( );
         AV50EmailText = AV50EmailText + StringUtil.NewLine( ) + StringUtil.NewLine( ) + "Esta mensagem pode conter informa��o confidencial e/ou privilegiada. Se voc� n�o for o destinat�rio ou a pessoa autorizada a receber esta mensagem, n�o pode usar, copiar ou divulgar as informa��es nela contidas ou tomar qualquer a��o baseada nessas informa��es. Se voc� recebeu esta mensagem por engano, por favor avise imediatamente o remetente, respondendo o e-mail e em seguida apague-o.";
         AV51Subject = "OS " + StringUtil.Trim( AV43DemandaFM) + " - Servi�o alterado (No reply)";
         AV59ContagemResultado_Codigos.Add(A456ContagemResultado_Codigo, 0);
         AV44WebSession.Set("DemandaCodigo", AV59ContagemResultado_Codigos.ToXml(false, true, "Collection", ""));
         new prc_enviaremail(context ).execute(  AV54AreaTrabalho_Codigo,  AV39Usuarios,  AV51Subject,  AV50EmailText,  AV49Attachments, ref  AV53Resultado) ;
         AV44WebSession.Remove("DemandaCodigo");
      }

      protected void S131( )
      {
         /* 'GETEMAILS' Routine */
         new prc_usuariosparanotificar(context ).execute(  AV48Contrato_Codigo,  AV13StatusAnterior) ;
         AV39Usuarios.FromXml(AV44WebSession.Get("Usuarios"), "Collection");
         AV44WebSession.Remove("Usuarios");
         AV39Usuarios.Add(AV9UserId, 0);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_AlterarServico");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P009A2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P009A2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P009A2_A601ContagemResultado_Servico = new int[1] ;
         P009A2_n601ContagemResultado_Servico = new bool[] {false} ;
         P009A2_A892LogResponsavel_DemandaCod = new int[1] ;
         P009A2_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P009A2_A1130LogResponsavel_Status = new String[] {""} ;
         P009A2_n1130LogResponsavel_Status = new bool[] {false} ;
         P009A2_A1234LogResponsavel_NovoStatus = new String[] {""} ;
         P009A2_n1234LogResponsavel_NovoStatus = new bool[] {false} ;
         P009A2_A896LogResponsavel_Owner = new int[1] ;
         P009A2_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         P009A2_n1177LogResponsavel_Prazo = new bool[] {false} ;
         P009A2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P009A2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P009A2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P009A2_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P009A2_A490ContagemResultado_ContratadaCod = new int[1] ;
         P009A2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P009A2_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P009A2_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P009A2_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P009A2_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P009A2_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P009A2_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P009A2_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P009A2_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P009A2_A1650ContagemResultado_PrzInc = new short[1] ;
         P009A2_n1650ContagemResultado_PrzInc = new bool[] {false} ;
         P009A2_A1603ContagemResultado_CntCod = new int[1] ;
         P009A2_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P009A2_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         P009A2_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         P009A2_A1797LogResponsavel_Codigo = new long[1] ;
         A1130LogResponsavel_Status = "";
         A1234LogResponsavel_NovoStatus = "";
         A1177LogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         A484ContagemResultado_StatusDmn = "";
         A801ContagemResultado_ServicoSigla = "";
         A493ContagemResultado_DemandaFM = "";
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A1611ContagemResultado_PrzTpDias = "";
         P009A4_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P009A4_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         AV63PrazoInicio = 1;
         AV13StatusAnterior = "";
         AV12Prazo = (DateTime)(DateTime.MinValue);
         AV10StatusDemanda = "";
         AV40ServicoAnterior = "";
         AV43DemandaFM = "";
         AV61HoraEntrega = (DateTime)(DateTime.MinValue);
         AV60TipoDias = "";
         P009A6_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P009A6_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P009A6_A601ContagemResultado_Servico = new int[1] ;
         P009A6_n601ContagemResultado_Servico = new bool[] {false} ;
         P009A6_A456ContagemResultado_Codigo = new int[1] ;
         P009A6_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P009A6_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P009A6_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P009A6_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P009A6_A490ContagemResultado_ContratadaCod = new int[1] ;
         P009A6_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P009A6_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P009A6_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P009A6_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P009A6_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P009A6_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P009A6_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P009A6_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P009A6_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P009A6_A1650ContagemResultado_PrzInc = new short[1] ;
         P009A6_n1650ContagemResultado_PrzInc = new bool[] {false} ;
         P009A6_A1603ContagemResultado_CntCod = new int[1] ;
         P009A6_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P009A6_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         P009A6_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         P009A6_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P009A6_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P009A7_A456ContagemResultado_Codigo = new int[1] ;
         P009A7_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P009A7_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P009A7_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P009A7_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P009A7_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P009A7_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P009A7_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P009A7_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P009A7_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P009A7_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         AV35DataEntrega = (DateTime)(DateTime.MinValue);
         P009A10_A1603ContagemResultado_CntCod = new int[1] ;
         P009A10_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P009A10_A456ContagemResultado_Codigo = new int[1] ;
         P009A10_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P009A10_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P009A10_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P009A10_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P009A10_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P009A10_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P009A10_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P009A10_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P009A10_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P009A10_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P009A10_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P009A10_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P009A10_A890ContagemResultado_Responsavel = new int[1] ;
         P009A10_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P009A10_A1604ContagemResultado_CntPrpCod = new int[1] ;
         P009A10_n1604ContagemResultado_CntPrpCod = new bool[] {false} ;
         AV39Usuarios = new GxSimpleCollection();
         GXt_dtime2 = (DateTime)(DateTime.MinValue);
         AV50EmailText = "";
         AV52WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV51Subject = "";
         AV59ContagemResultado_Codigos = new GxSimpleCollection();
         AV44WebSession = context.GetSession();
         AV49Attachments = new GxSimpleCollection();
         AV53Resultado = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_alterarservico__default(),
            new Object[][] {
                new Object[] {
               P009A2_A1553ContagemResultado_CntSrvCod, P009A2_n1553ContagemResultado_CntSrvCod, P009A2_A601ContagemResultado_Servico, P009A2_n601ContagemResultado_Servico, P009A2_A892LogResponsavel_DemandaCod, P009A2_n892LogResponsavel_DemandaCod, P009A2_A1130LogResponsavel_Status, P009A2_n1130LogResponsavel_Status, P009A2_A1234LogResponsavel_NovoStatus, P009A2_n1234LogResponsavel_NovoStatus,
               P009A2_A896LogResponsavel_Owner, P009A2_A1177LogResponsavel_Prazo, P009A2_n1177LogResponsavel_Prazo, P009A2_A484ContagemResultado_StatusDmn, P009A2_n484ContagemResultado_StatusDmn, P009A2_A52Contratada_AreaTrabalhoCod, P009A2_n52Contratada_AreaTrabalhoCod, P009A2_A490ContagemResultado_ContratadaCod, P009A2_n490ContagemResultado_ContratadaCod, P009A2_A801ContagemResultado_ServicoSigla,
               P009A2_n801ContagemResultado_ServicoSigla, P009A2_A493ContagemResultado_DemandaFM, P009A2_n493ContagemResultado_DemandaFM, P009A2_A912ContagemResultado_HoraEntrega, P009A2_n912ContagemResultado_HoraEntrega, P009A2_A1611ContagemResultado_PrzTpDias, P009A2_n1611ContagemResultado_PrzTpDias, P009A2_A1650ContagemResultado_PrzInc, P009A2_n1650ContagemResultado_PrzInc, P009A2_A1603ContagemResultado_CntCod,
               P009A2_n1603ContagemResultado_CntCod, P009A2_A798ContagemResultado_PFBFSImp, P009A2_n798ContagemResultado_PFBFSImp, P009A2_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               P009A4_A531ContagemResultado_StatusUltCnt, P009A4_A684ContagemResultado_PFBFSUltima
               }
               , new Object[] {
               P009A6_A1553ContagemResultado_CntSrvCod, P009A6_n1553ContagemResultado_CntSrvCod, P009A6_A601ContagemResultado_Servico, P009A6_n601ContagemResultado_Servico, P009A6_A456ContagemResultado_Codigo, P009A6_A484ContagemResultado_StatusDmn, P009A6_n484ContagemResultado_StatusDmn, P009A6_A52Contratada_AreaTrabalhoCod, P009A6_n52Contratada_AreaTrabalhoCod, P009A6_A490ContagemResultado_ContratadaCod,
               P009A6_n490ContagemResultado_ContratadaCod, P009A6_A801ContagemResultado_ServicoSigla, P009A6_n801ContagemResultado_ServicoSigla, P009A6_A493ContagemResultado_DemandaFM, P009A6_n493ContagemResultado_DemandaFM, P009A6_A912ContagemResultado_HoraEntrega, P009A6_n912ContagemResultado_HoraEntrega, P009A6_A1611ContagemResultado_PrzTpDias, P009A6_n1611ContagemResultado_PrzTpDias, P009A6_A1650ContagemResultado_PrzInc,
               P009A6_n1650ContagemResultado_PrzInc, P009A6_A1603ContagemResultado_CntCod, P009A6_n1603ContagemResultado_CntCod, P009A6_A798ContagemResultado_PFBFSImp, P009A6_n798ContagemResultado_PFBFSImp, P009A6_A531ContagemResultado_StatusUltCnt, P009A6_A684ContagemResultado_PFBFSUltima
               }
               , new Object[] {
               P009A7_A456ContagemResultado_Codigo, P009A7_A1553ContagemResultado_CntSrvCod, P009A7_n1553ContagemResultado_CntSrvCod, P009A7_A1237ContagemResultado_PrazoMaisDias, P009A7_n1237ContagemResultado_PrazoMaisDias, P009A7_A472ContagemResultado_DataEntrega, P009A7_n472ContagemResultado_DataEntrega, P009A7_A912ContagemResultado_HoraEntrega, P009A7_n912ContagemResultado_HoraEntrega, P009A7_A1351ContagemResultado_DataPrevista,
               P009A7_n1351ContagemResultado_DataPrevista
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P009A10_A1603ContagemResultado_CntCod, P009A10_n1603ContagemResultado_CntCod, P009A10_A456ContagemResultado_Codigo, P009A10_A1553ContagemResultado_CntSrvCod, P009A10_n1553ContagemResultado_CntSrvCod, P009A10_A484ContagemResultado_StatusDmn, P009A10_n484ContagemResultado_StatusDmn, P009A10_A1237ContagemResultado_PrazoMaisDias, P009A10_n1237ContagemResultado_PrazoMaisDias, P009A10_A472ContagemResultado_DataEntrega,
               P009A10_n472ContagemResultado_DataEntrega, P009A10_A912ContagemResultado_HoraEntrega, P009A10_n912ContagemResultado_HoraEntrega, P009A10_A1351ContagemResultado_DataPrevista, P009A10_n1351ContagemResultado_DataPrevista, P009A10_A890ContagemResultado_Responsavel, P009A10_n890ContagemResultado_Responsavel, P009A10_A1604ContagemResultado_CntPrpCod, P009A10_n1604ContagemResultado_CntPrpCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV66GXLvl2 ;
      private short A1650ContagemResultado_PrzInc ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short OV63PrazoInicio ;
      private short AV63PrazoInicio ;
      private short A1237ContagemResultado_PrazoMaisDias ;
      private short AV31Dias ;
      private short AV38DiasAnalise ;
      private short GXt_int1 ;
      private short AV33Hours ;
      private short AV34Minutes ;
      private int A456ContagemResultado_Codigo ;
      private int AV62ContratoServicos_Codigo ;
      private int AV9UserId ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int A892LogResponsavel_DemandaCod ;
      private int A896LogResponsavel_Owner ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1603ContagemResultado_CntCod ;
      private int AV14UsuarioAnterior ;
      private int AV36AreaTrabalho ;
      private int AV15Contratada ;
      private int AV48Contrato_Codigo ;
      private int A890ContagemResultado_Responsavel ;
      private int A1604ContagemResultado_CntPrpCod ;
      private int AV54AreaTrabalho_Codigo ;
      private long A1797LogResponsavel_Codigo ;
      private decimal A798ContagemResultado_PFBFSImp ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal AV32PFBFS ;
      private String AV41novoSrvSigla ;
      private String scmdbuf ;
      private String A1130LogResponsavel_Status ;
      private String A1234LogResponsavel_NovoStatus ;
      private String A484ContagemResultado_StatusDmn ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A1611ContagemResultado_PrzTpDias ;
      private String AV13StatusAnterior ;
      private String AV10StatusDemanda ;
      private String AV40ServicoAnterior ;
      private String AV60TipoDias ;
      private String AV50EmailText ;
      private String AV51Subject ;
      private String AV53Resultado ;
      private DateTime A1177LogResponsavel_Prazo ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime AV12Prazo ;
      private DateTime AV61HoraEntrega ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime AV35DataEntrega ;
      private DateTime GXt_dtime2 ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1130LogResponsavel_Status ;
      private bool n1234LogResponsavel_NovoStatus ;
      private bool n1177LogResponsavel_Prazo ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n1650ContagemResultado_PrzInc ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n798ContagemResultado_PFBFSImp ;
      private bool returnInSub ;
      private bool n1237ContagemResultado_PrazoMaisDias ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n1604ContagemResultado_CntPrpCod ;
      private String AV11Observacao ;
      private String A493ContagemResultado_DemandaFM ;
      private String AV43DemandaFM ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private String aP2_Observacao ;
      private IDataStoreProvider pr_default ;
      private int[] P009A2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P009A2_n1553ContagemResultado_CntSrvCod ;
      private int[] P009A2_A601ContagemResultado_Servico ;
      private bool[] P009A2_n601ContagemResultado_Servico ;
      private int[] P009A2_A892LogResponsavel_DemandaCod ;
      private bool[] P009A2_n892LogResponsavel_DemandaCod ;
      private String[] P009A2_A1130LogResponsavel_Status ;
      private bool[] P009A2_n1130LogResponsavel_Status ;
      private String[] P009A2_A1234LogResponsavel_NovoStatus ;
      private bool[] P009A2_n1234LogResponsavel_NovoStatus ;
      private int[] P009A2_A896LogResponsavel_Owner ;
      private DateTime[] P009A2_A1177LogResponsavel_Prazo ;
      private bool[] P009A2_n1177LogResponsavel_Prazo ;
      private String[] P009A2_A484ContagemResultado_StatusDmn ;
      private bool[] P009A2_n484ContagemResultado_StatusDmn ;
      private int[] P009A2_A52Contratada_AreaTrabalhoCod ;
      private bool[] P009A2_n52Contratada_AreaTrabalhoCod ;
      private int[] P009A2_A490ContagemResultado_ContratadaCod ;
      private bool[] P009A2_n490ContagemResultado_ContratadaCod ;
      private String[] P009A2_A801ContagemResultado_ServicoSigla ;
      private bool[] P009A2_n801ContagemResultado_ServicoSigla ;
      private String[] P009A2_A493ContagemResultado_DemandaFM ;
      private bool[] P009A2_n493ContagemResultado_DemandaFM ;
      private DateTime[] P009A2_A912ContagemResultado_HoraEntrega ;
      private bool[] P009A2_n912ContagemResultado_HoraEntrega ;
      private String[] P009A2_A1611ContagemResultado_PrzTpDias ;
      private bool[] P009A2_n1611ContagemResultado_PrzTpDias ;
      private short[] P009A2_A1650ContagemResultado_PrzInc ;
      private bool[] P009A2_n1650ContagemResultado_PrzInc ;
      private int[] P009A2_A1603ContagemResultado_CntCod ;
      private bool[] P009A2_n1603ContagemResultado_CntCod ;
      private decimal[] P009A2_A798ContagemResultado_PFBFSImp ;
      private bool[] P009A2_n798ContagemResultado_PFBFSImp ;
      private long[] P009A2_A1797LogResponsavel_Codigo ;
      private short[] P009A4_A531ContagemResultado_StatusUltCnt ;
      private decimal[] P009A4_A684ContagemResultado_PFBFSUltima ;
      private int[] P009A6_A1553ContagemResultado_CntSrvCod ;
      private bool[] P009A6_n1553ContagemResultado_CntSrvCod ;
      private int[] P009A6_A601ContagemResultado_Servico ;
      private bool[] P009A6_n601ContagemResultado_Servico ;
      private int[] P009A6_A456ContagemResultado_Codigo ;
      private String[] P009A6_A484ContagemResultado_StatusDmn ;
      private bool[] P009A6_n484ContagemResultado_StatusDmn ;
      private int[] P009A6_A52Contratada_AreaTrabalhoCod ;
      private bool[] P009A6_n52Contratada_AreaTrabalhoCod ;
      private int[] P009A6_A490ContagemResultado_ContratadaCod ;
      private bool[] P009A6_n490ContagemResultado_ContratadaCod ;
      private String[] P009A6_A801ContagemResultado_ServicoSigla ;
      private bool[] P009A6_n801ContagemResultado_ServicoSigla ;
      private String[] P009A6_A493ContagemResultado_DemandaFM ;
      private bool[] P009A6_n493ContagemResultado_DemandaFM ;
      private DateTime[] P009A6_A912ContagemResultado_HoraEntrega ;
      private bool[] P009A6_n912ContagemResultado_HoraEntrega ;
      private String[] P009A6_A1611ContagemResultado_PrzTpDias ;
      private bool[] P009A6_n1611ContagemResultado_PrzTpDias ;
      private short[] P009A6_A1650ContagemResultado_PrzInc ;
      private bool[] P009A6_n1650ContagemResultado_PrzInc ;
      private int[] P009A6_A1603ContagemResultado_CntCod ;
      private bool[] P009A6_n1603ContagemResultado_CntCod ;
      private decimal[] P009A6_A798ContagemResultado_PFBFSImp ;
      private bool[] P009A6_n798ContagemResultado_PFBFSImp ;
      private short[] P009A6_A531ContagemResultado_StatusUltCnt ;
      private decimal[] P009A6_A684ContagemResultado_PFBFSUltima ;
      private int[] P009A7_A456ContagemResultado_Codigo ;
      private int[] P009A7_A1553ContagemResultado_CntSrvCod ;
      private bool[] P009A7_n1553ContagemResultado_CntSrvCod ;
      private short[] P009A7_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P009A7_n1237ContagemResultado_PrazoMaisDias ;
      private DateTime[] P009A7_A472ContagemResultado_DataEntrega ;
      private bool[] P009A7_n472ContagemResultado_DataEntrega ;
      private DateTime[] P009A7_A912ContagemResultado_HoraEntrega ;
      private bool[] P009A7_n912ContagemResultado_HoraEntrega ;
      private DateTime[] P009A7_A1351ContagemResultado_DataPrevista ;
      private bool[] P009A7_n1351ContagemResultado_DataPrevista ;
      private int[] P009A10_A1603ContagemResultado_CntCod ;
      private bool[] P009A10_n1603ContagemResultado_CntCod ;
      private int[] P009A10_A456ContagemResultado_Codigo ;
      private int[] P009A10_A1553ContagemResultado_CntSrvCod ;
      private bool[] P009A10_n1553ContagemResultado_CntSrvCod ;
      private String[] P009A10_A484ContagemResultado_StatusDmn ;
      private bool[] P009A10_n484ContagemResultado_StatusDmn ;
      private short[] P009A10_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P009A10_n1237ContagemResultado_PrazoMaisDias ;
      private DateTime[] P009A10_A472ContagemResultado_DataEntrega ;
      private bool[] P009A10_n472ContagemResultado_DataEntrega ;
      private DateTime[] P009A10_A912ContagemResultado_HoraEntrega ;
      private bool[] P009A10_n912ContagemResultado_HoraEntrega ;
      private DateTime[] P009A10_A1351ContagemResultado_DataPrevista ;
      private bool[] P009A10_n1351ContagemResultado_DataPrevista ;
      private int[] P009A10_A890ContagemResultado_Responsavel ;
      private bool[] P009A10_n890ContagemResultado_Responsavel ;
      private int[] P009A10_A1604ContagemResultado_CntPrpCod ;
      private bool[] P009A10_n1604ContagemResultado_CntPrpCod ;
      private IGxSession AV44WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV39Usuarios ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV59ContagemResultado_Codigos ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV49Attachments ;
      private wwpbaseobjects.SdtWWPContext AV52WWPContext ;
   }

   public class prc_alterarservico__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP009A2 ;
          prmP009A2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009A4 ;
          prmP009A4 = new Object[] {
          } ;
          Object[] prmP009A6 ;
          prmP009A6 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009A7 ;
          prmP009A7 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009A8 ;
          prmP009A8 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_PrazoMaisDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009A9 ;
          prmP009A9 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_PrazoMaisDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009A10 ;
          prmP009A10 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009A11 ;
          prmP009A11 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_PrazoMaisDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009A12 ;
          prmP009A12 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_PrazoMaisDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P009A2", "SELECT TOP 1 T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, T1.[LogResponsavel_Status], T1.[LogResponsavel_NovoStatus], T1.[LogResponsavel_Owner], T1.[LogResponsavel_Prazo], T2.[ContagemResultado_StatusDmn], T5.[Contratada_AreaTrabalhoCod], T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T2.[ContagemResultado_DemandaFM], T2.[ContagemResultado_HoraEntrega], T3.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T3.[ContratoServicos_PrazoInicio] AS ContagemResultado_PrzInc, T3.[Contrato_Codigo] AS ContagemResultado_CntCod, T2.[ContagemResultado_PFBFSImp], T1.[LogResponsavel_Codigo] FROM (((([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) WHERE (T1.[LogResponsavel_DemandaCod] = @ContagemResultado_Codigo) AND (Not (T1.[LogResponsavel_Status] = '') and T1.[LogResponsavel_Status] <> 'D') AND (T1.[LogResponsavel_NovoStatus] = 'D') ORDER BY T1.[LogResponsavel_DemandaCod], T1.[LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009A2,1,0,true,true )
             ,new CursorDef("P009A4", "SELECT COALESCE( T1.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T1.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima FROM (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009A4,1,0,true,false )
             ,new CursorDef("P009A6", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_StatusDmn], T4.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_HoraEntrega], T2.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T2.[ContratoServicos_PrazoInicio] AS ContagemResultado_PrzInc, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_PFBFSImp], COALESCE( T5.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T5.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T5 ON T5.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009A6,1,0,false,true )
             ,new CursorDef("P009A7", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_PrazoMaisDias], [ContagemResultado_DataEntrega], [ContagemResultado_HoraEntrega], [ContagemResultado_DataPrevista] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009A7,1,0,true,true )
             ,new CursorDef("P009A8", "UPDATE [ContagemResultado] SET [ContagemResultado_CntSrvCod]=@ContagemResultado_CntSrvCod, [ContagemResultado_PrazoMaisDias]=@ContagemResultado_PrazoMaisDias, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP009A8)
             ,new CursorDef("P009A9", "UPDATE [ContagemResultado] SET [ContagemResultado_CntSrvCod]=@ContagemResultado_CntSrvCod, [ContagemResultado_PrazoMaisDias]=@ContagemResultado_PrazoMaisDias, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP009A9)
             ,new CursorDef("P009A10", "SELECT TOP 1 T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_PrazoMaisDias], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_Responsavel], T3.[Contrato_PrepostoCod] AS ContagemResultado_CntPrpCod FROM (([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009A10,1,0,true,true )
             ,new CursorDef("P009A11", "UPDATE [ContagemResultado] SET [ContagemResultado_CntSrvCod]=@ContagemResultado_CntSrvCod, [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_PrazoMaisDias]=@ContagemResultado_PrazoMaisDias, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP009A11)
             ,new CursorDef("P009A12", "UPDATE [ContagemResultado] SET [ContagemResultado_CntSrvCod]=@ContagemResultado_CntSrvCod, [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_PrazoMaisDias]=@ContagemResultado_PrazoMaisDias, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP009A12)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((DateTime[]) buf[11])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((String[]) buf[21])[0] = rslt.getVarchar(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[23])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((String[]) buf[25])[0] = rslt.getString(14, 1) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((short[]) buf[27])[0] = rslt.getShort(15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((int[]) buf[29])[0] = rslt.getInt(16) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((decimal[]) buf[31])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((long[]) buf[33])[0] = rslt.getLong(18) ;
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[15])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((short[]) buf[19])[0] = rslt.getShort(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((decimal[]) buf[23])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((short[]) buf[25])[0] = rslt.getShort(14) ;
                ((decimal[]) buf[26])[0] = rslt.getDecimal(15) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[11])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(6, (DateTime)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[13]);
                }
                stmt.SetParameter(8, (int)parms[14]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(6, (DateTime)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[13]);
                }
                stmt.SetParameter(8, (int)parms[14]);
                return;
       }
    }

 }

}
