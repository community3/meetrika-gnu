/*
               File: gxuiToolbarItemTypes
        Description: gxuiToolbarItemTypes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:38.83
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxdomaingxuitoolbaritemtypes
   {
      private static Hashtable domain = new Hashtable();
      static gxdomaingxuitoolbaritemtypes ()
      {
         domain["Button"] = "Button";
         domain["Text"] = "Text";
         domain["Edit"] = "Edit";
         domain["Fill"] = "Fill";
         domain["Separator"] = "Separator";
         domain["Menu"] = "Menu";
         domain["SplitButton"] = "Split Button";
         domain["Group"] = "Group";
      }

      public static string getDescription( IGxContext context ,
                                           String key )
      {
         string rtkey ;
         rtkey = StringUtil.Trim( (String)(key));
         return (string)domain[rtkey] ;
      }

      public static GxSimpleCollection getValues( )
      {
         GxSimpleCollection value = new GxSimpleCollection();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (String key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

   }

}
