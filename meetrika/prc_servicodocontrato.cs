/*
               File: PRC_ServicoDoContrato
        Description: Servico Do Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:50.70
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_servicodocontrato : GXProcedure
   {
      public prc_servicodocontrato( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_servicodocontrato( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contrato_Codigo ,
                           out int aP1_ContratoServicos_Codigo )
      {
         this.A74Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV9ContratoServicos_Codigo = 0 ;
         initialize();
         executePrivate();
         aP0_Contrato_Codigo=this.A74Contrato_Codigo;
         aP1_ContratoServicos_Codigo=this.AV9ContratoServicos_Codigo;
      }

      public int executeUdp( ref int aP0_Contrato_Codigo )
      {
         this.A74Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV9ContratoServicos_Codigo = 0 ;
         initialize();
         executePrivate();
         aP0_Contrato_Codigo=this.A74Contrato_Codigo;
         aP1_ContratoServicos_Codigo=this.AV9ContratoServicos_Codigo;
         return AV9ContratoServicos_Codigo ;
      }

      public void executeSubmit( ref int aP0_Contrato_Codigo ,
                                 out int aP1_ContratoServicos_Codigo )
      {
         prc_servicodocontrato objprc_servicodocontrato;
         objprc_servicodocontrato = new prc_servicodocontrato();
         objprc_servicodocontrato.A74Contrato_Codigo = aP0_Contrato_Codigo;
         objprc_servicodocontrato.AV9ContratoServicos_Codigo = 0 ;
         objprc_servicodocontrato.context.SetSubmitInitialConfig(context);
         objprc_servicodocontrato.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_servicodocontrato);
         aP0_Contrato_Codigo=this.A74Contrato_Codigo;
         aP1_ContratoServicos_Codigo=this.AV9ContratoServicos_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_servicodocontrato)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00CM2 */
         pr_default.execute(0, new Object[] {A74Contrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A638ContratoServicos_Ativo = P00CM2_A638ContratoServicos_Ativo[0];
            A160ContratoServicos_Codigo = P00CM2_A160ContratoServicos_Codigo[0];
            AV8q = (short)(AV8q+1);
            AV9ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV8q > 1 )
         {
            AV9ContratoServicos_Codigo = 0;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00CM2_A74Contrato_Codigo = new int[1] ;
         P00CM2_A638ContratoServicos_Ativo = new bool[] {false} ;
         P00CM2_A160ContratoServicos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_servicodocontrato__default(),
            new Object[][] {
                new Object[] {
               P00CM2_A74Contrato_Codigo, P00CM2_A638ContratoServicos_Ativo, P00CM2_A160ContratoServicos_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8q ;
      private int A74Contrato_Codigo ;
      private int AV9ContratoServicos_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private String scmdbuf ;
      private bool A638ContratoServicos_Ativo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contrato_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00CM2_A74Contrato_Codigo ;
      private bool[] P00CM2_A638ContratoServicos_Ativo ;
      private int[] P00CM2_A160ContratoServicos_Codigo ;
      private int aP1_ContratoServicos_Codigo ;
   }

   public class prc_servicodocontrato__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00CM2 ;
          prmP00CM2 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00CM2", "SELECT [Contrato_Codigo], [ContratoServicos_Ativo], [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE ([Contrato_Codigo] = @Contrato_Codigo) AND ([ContratoServicos_Ativo] = 1) ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00CM2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
