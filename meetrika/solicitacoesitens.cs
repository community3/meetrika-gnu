/*
               File: SolicitacoesItens
        Description: Solicitacoes Itens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:21:0.84
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class solicitacoesitens : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
         {
            A439Solicitacoes_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_2( A439Solicitacoes_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A161FuncaoUsuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A161FuncaoUsuario_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Solicitacoes Itens", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtSolicitacoesItens_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public solicitacoesitens( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public solicitacoesitens( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1T68( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1T68e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1T68( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1T68( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1T68e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Solicitacoes Itens", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_SolicitacoesItens.htm");
            wb_table3_28_1T68( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_1T68e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1T68e( true) ;
         }
         else
         {
            wb_table1_2_1T68e( false) ;
         }
      }

      protected void wb_table3_28_1T68( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_1T68( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_1T68e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacoesItens.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacoesItens.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacoesItens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_1T68e( true) ;
         }
         else
         {
            wb_table3_28_1T68e( false) ;
         }
      }

      protected void wb_table4_34_1T68( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoesitens_codigo_Internalname, "Itens_Codigo", "", "", lblTextblocksolicitacoesitens_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_SolicitacoesItens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSolicitacoesItens_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A447SolicitacoesItens_Codigo), 6, 0, ",", "")), ((edtSolicitacoesItens_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A447SolicitacoesItens_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A447SolicitacoesItens_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacoesItens_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtSolicitacoesItens_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacoesItens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoes_codigo_Internalname, "Solicitacoes_Codigo", "", "", lblTextblocksolicitacoes_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_SolicitacoesItens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSolicitacoes_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A439Solicitacoes_Codigo), 6, 0, ",", "")), ((edtSolicitacoes_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A439Solicitacoes_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A439Solicitacoes_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacoes_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtSolicitacoes_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacoesItens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoesitens_descricao_Internalname, "Itens_descricao", "", "", lblTextblocksolicitacoesitens_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_SolicitacoesItens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtSolicitacoesItens_descricao_Internalname, A448SolicitacoesItens_descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", 0, 1, edtSolicitacoesItens_descricao_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_SolicitacoesItens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoesitens_arquivo_Internalname, "Itens_Arquivo", "", "", lblTextblocksolicitacoesitens_arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_SolicitacoesItens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            ClassString = "Image";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            edtSolicitacoesItens_Arquivo_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoesItens_Arquivo_Internalname, "Filetype", edtSolicitacoesItens_Arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A449SolicitacoesItens_Arquivo)) )
            {
               gxblobfileaux.Source = A449SolicitacoesItens_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtSolicitacoesItens_Arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtSolicitacoesItens_Arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A449SolicitacoesItens_Arquivo = gxblobfileaux.GetAbsoluteName();
                  n449SolicitacoesItens_Arquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A449SolicitacoesItens_Arquivo", A449SolicitacoesItens_Arquivo);
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoesItens_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A449SolicitacoesItens_Arquivo));
                  edtSolicitacoesItens_Arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoesItens_Arquivo_Internalname, "Filetype", edtSolicitacoesItens_Arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoesItens_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A449SolicitacoesItens_Arquivo));
            }
            GxWebStd.gx_blob_field( context, edtSolicitacoesItens_Arquivo_Internalname, StringUtil.RTrim( A449SolicitacoesItens_Arquivo), context.PathToRelativeUrl( A449SolicitacoesItens_Arquivo), (String.IsNullOrEmpty(StringUtil.RTrim( edtSolicitacoesItens_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtSolicitacoesItens_Arquivo_Filetype)) ? A449SolicitacoesItens_Arquivo : edtSolicitacoesItens_Arquivo_Filetype)) : edtSolicitacoesItens_Arquivo_Contenttype), false, "", edtSolicitacoesItens_Arquivo_Parameters, 0, edtSolicitacoesItens_Arquivo_Enabled, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtSolicitacoesItens_Arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", "", "HLP_SolicitacoesItens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaousuario_codigo_Internalname, "Fun��o de Usu�rio", "", "", lblTextblockfuncaousuario_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_SolicitacoesItens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoUsuario_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A161FuncaoUsuario_Codigo), 6, 0, ",", "")), ((edtFuncaoUsuario_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoUsuario_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoUsuario_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacoesItens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_1T68e( true) ;
         }
         else
         {
            wb_table4_34_1T68e( false) ;
         }
      }

      protected void wb_table2_5_1T68( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesItens.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesItens.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesItens.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesItens.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesItens.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesItens.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesItens.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesItens.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesItens.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesItens.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesItens.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesItens.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesItens.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesItens.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesItens.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SolicitacoesItens.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1T68e( true) ;
         }
         else
         {
            wb_table2_5_1T68e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtSolicitacoesItens_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSolicitacoesItens_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SOLICITACOESITENS_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSolicitacoesItens_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A447SolicitacoesItens_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A447SolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A447SolicitacoesItens_Codigo), 6, 0)));
               }
               else
               {
                  A447SolicitacoesItens_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSolicitacoesItens_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A447SolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A447SolicitacoesItens_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtSolicitacoes_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSolicitacoes_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SOLICITACOES_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSolicitacoes_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A439Solicitacoes_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
               }
               else
               {
                  A439Solicitacoes_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSolicitacoes_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
               }
               A448SolicitacoesItens_descricao = cgiGet( edtSolicitacoesItens_descricao_Internalname);
               n448SolicitacoesItens_descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A448SolicitacoesItens_descricao", A448SolicitacoesItens_descricao);
               n448SolicitacoesItens_descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A448SolicitacoesItens_descricao)) ? true : false);
               A449SolicitacoesItens_Arquivo = cgiGet( edtSolicitacoesItens_Arquivo_Internalname);
               n449SolicitacoesItens_Arquivo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A449SolicitacoesItens_Arquivo", A449SolicitacoesItens_Arquivo);
               n449SolicitacoesItens_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A449SolicitacoesItens_Arquivo)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtFuncaoUsuario_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFuncaoUsuario_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FUNCAOUSUARIO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoUsuario_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A161FuncaoUsuario_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
               }
               else
               {
                  A161FuncaoUsuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoUsuario_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
               }
               /* Read saved values. */
               Z447SolicitacoesItens_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z447SolicitacoesItens_Codigo"), ",", "."));
               Z439Solicitacoes_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z439Solicitacoes_Codigo"), ",", "."));
               Z161FuncaoUsuario_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z161FuncaoUsuario_Codigo"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               edtSolicitacoesItens_Arquivo_Filename = cgiGet( "SOLICITACOESITENS_ARQUIVO_Filename");
               edtSolicitacoesItens_Arquivo_Filetype = cgiGet( "SOLICITACOESITENS_ARQUIVO_Filetype");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A449SolicitacoesItens_Arquivo)) )
               {
                  edtSolicitacoesItens_Arquivo_Filename = (String)(CGIGetFileName(edtSolicitacoesItens_Arquivo_Internalname));
                  edtSolicitacoesItens_Arquivo_Filetype = (String)(CGIGetFileType(edtSolicitacoesItens_Arquivo_Internalname));
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( A449SolicitacoesItens_Arquivo)) )
               {
                  GXCCtlgxBlob = "SOLICITACOESITENS_ARQUIVO" + "_gxBlob";
                  A449SolicitacoesItens_Arquivo = cgiGet( GXCCtlgxBlob);
                  n449SolicitacoesItens_Arquivo = false;
                  n449SolicitacoesItens_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A449SolicitacoesItens_Arquivo)) ? true : false);
               }
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A447SolicitacoesItens_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A447SolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A447SolicitacoesItens_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1T68( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes1T68( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption1T0( )
      {
      }

      protected void ZM1T68( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z439Solicitacoes_Codigo = T001T3_A439Solicitacoes_Codigo[0];
               Z161FuncaoUsuario_Codigo = T001T3_A161FuncaoUsuario_Codigo[0];
            }
            else
            {
               Z439Solicitacoes_Codigo = A439Solicitacoes_Codigo;
               Z161FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
            }
         }
         if ( GX_JID == -1 )
         {
            Z447SolicitacoesItens_Codigo = A447SolicitacoesItens_Codigo;
            Z448SolicitacoesItens_descricao = A448SolicitacoesItens_descricao;
            Z449SolicitacoesItens_Arquivo = A449SolicitacoesItens_Arquivo;
            Z439Solicitacoes_Codigo = A439Solicitacoes_Codigo;
            Z161FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load1T68( )
      {
         /* Using cursor T001T6 */
         pr_default.execute(4, new Object[] {A447SolicitacoesItens_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound68 = 1;
            A448SolicitacoesItens_descricao = T001T6_A448SolicitacoesItens_descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A448SolicitacoesItens_descricao", A448SolicitacoesItens_descricao);
            n448SolicitacoesItens_descricao = T001T6_n448SolicitacoesItens_descricao[0];
            A439Solicitacoes_Codigo = T001T6_A439Solicitacoes_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
            A161FuncaoUsuario_Codigo = T001T6_A161FuncaoUsuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
            A449SolicitacoesItens_Arquivo = T001T6_A449SolicitacoesItens_Arquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A449SolicitacoesItens_Arquivo", A449SolicitacoesItens_Arquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoesItens_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A449SolicitacoesItens_Arquivo));
            n449SolicitacoesItens_Arquivo = T001T6_n449SolicitacoesItens_Arquivo[0];
            ZM1T68( -1) ;
         }
         pr_default.close(4);
         OnLoadActions1T68( ) ;
      }

      protected void OnLoadActions1T68( )
      {
      }

      protected void CheckExtendedTable1T68( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T001T4 */
         pr_default.execute(2, new Object[] {A439Solicitacoes_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Solicitacoes'.", "ForeignKeyNotFound", 1, "SOLICITACOES_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSolicitacoes_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T001T5 */
         pr_default.execute(3, new Object[] {A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Modulo Funcoes'.", "ForeignKeyNotFound", 1, "FUNCAOUSUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors1T68( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_2( int A439Solicitacoes_Codigo )
      {
         /* Using cursor T001T7 */
         pr_default.execute(5, new Object[] {A439Solicitacoes_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Solicitacoes'.", "ForeignKeyNotFound", 1, "SOLICITACOES_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSolicitacoes_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_3( int A161FuncaoUsuario_Codigo )
      {
         /* Using cursor T001T8 */
         pr_default.execute(6, new Object[] {A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Modulo Funcoes'.", "ForeignKeyNotFound", 1, "FUNCAOUSUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey1T68( )
      {
         /* Using cursor T001T9 */
         pr_default.execute(7, new Object[] {A447SolicitacoesItens_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound68 = 1;
         }
         else
         {
            RcdFound68 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001T3 */
         pr_default.execute(1, new Object[] {A447SolicitacoesItens_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1T68( 1) ;
            RcdFound68 = 1;
            A447SolicitacoesItens_Codigo = T001T3_A447SolicitacoesItens_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A447SolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A447SolicitacoesItens_Codigo), 6, 0)));
            A448SolicitacoesItens_descricao = T001T3_A448SolicitacoesItens_descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A448SolicitacoesItens_descricao", A448SolicitacoesItens_descricao);
            n448SolicitacoesItens_descricao = T001T3_n448SolicitacoesItens_descricao[0];
            A439Solicitacoes_Codigo = T001T3_A439Solicitacoes_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
            A161FuncaoUsuario_Codigo = T001T3_A161FuncaoUsuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
            A449SolicitacoesItens_Arquivo = T001T3_A449SolicitacoesItens_Arquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A449SolicitacoesItens_Arquivo", A449SolicitacoesItens_Arquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoesItens_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A449SolicitacoesItens_Arquivo));
            n449SolicitacoesItens_Arquivo = T001T3_n449SolicitacoesItens_Arquivo[0];
            Z447SolicitacoesItens_Codigo = A447SolicitacoesItens_Codigo;
            sMode68 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load1T68( ) ;
            if ( AnyError == 1 )
            {
               RcdFound68 = 0;
               InitializeNonKey1T68( ) ;
            }
            Gx_mode = sMode68;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound68 = 0;
            InitializeNonKey1T68( ) ;
            sMode68 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode68;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1T68( ) ;
         if ( RcdFound68 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound68 = 0;
         /* Using cursor T001T10 */
         pr_default.execute(8, new Object[] {A447SolicitacoesItens_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T001T10_A447SolicitacoesItens_Codigo[0] < A447SolicitacoesItens_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T001T10_A447SolicitacoesItens_Codigo[0] > A447SolicitacoesItens_Codigo ) ) )
            {
               A447SolicitacoesItens_Codigo = T001T10_A447SolicitacoesItens_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A447SolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A447SolicitacoesItens_Codigo), 6, 0)));
               RcdFound68 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound68 = 0;
         /* Using cursor T001T11 */
         pr_default.execute(9, new Object[] {A447SolicitacoesItens_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T001T11_A447SolicitacoesItens_Codigo[0] > A447SolicitacoesItens_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T001T11_A447SolicitacoesItens_Codigo[0] < A447SolicitacoesItens_Codigo ) ) )
            {
               A447SolicitacoesItens_Codigo = T001T11_A447SolicitacoesItens_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A447SolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A447SolicitacoesItens_Codigo), 6, 0)));
               RcdFound68 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1T68( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtSolicitacoesItens_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1T68( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound68 == 1 )
            {
               if ( A447SolicitacoesItens_Codigo != Z447SolicitacoesItens_Codigo )
               {
                  A447SolicitacoesItens_Codigo = Z447SolicitacoesItens_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A447SolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A447SolicitacoesItens_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "SOLICITACOESITENS_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSolicitacoesItens_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtSolicitacoesItens_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update1T68( ) ;
                  GX_FocusControl = edtSolicitacoesItens_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A447SolicitacoesItens_Codigo != Z447SolicitacoesItens_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtSolicitacoesItens_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1T68( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "SOLICITACOESITENS_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtSolicitacoesItens_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtSolicitacoesItens_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1T68( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A447SolicitacoesItens_Codigo != Z447SolicitacoesItens_Codigo )
         {
            A447SolicitacoesItens_Codigo = Z447SolicitacoesItens_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A447SolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A447SolicitacoesItens_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "SOLICITACOESITENS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSolicitacoesItens_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtSolicitacoesItens_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound68 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "SOLICITACOESITENS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSolicitacoesItens_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtSolicitacoes_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart1T68( ) ;
         if ( RcdFound68 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtSolicitacoes_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd1T68( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound68 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtSolicitacoes_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound68 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtSolicitacoes_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart1T68( ) ;
         if ( RcdFound68 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound68 != 0 )
            {
               ScanNext1T68( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtSolicitacoes_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd1T68( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency1T68( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001T2 */
            pr_default.execute(0, new Object[] {A447SolicitacoesItens_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SolicitacoesItens"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z439Solicitacoes_Codigo != T001T2_A439Solicitacoes_Codigo[0] ) || ( Z161FuncaoUsuario_Codigo != T001T2_A161FuncaoUsuario_Codigo[0] ) )
            {
               if ( Z439Solicitacoes_Codigo != T001T2_A439Solicitacoes_Codigo[0] )
               {
                  GXUtil.WriteLog("solicitacoesitens:[seudo value changed for attri]"+"Solicitacoes_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z439Solicitacoes_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T001T2_A439Solicitacoes_Codigo[0]);
               }
               if ( Z161FuncaoUsuario_Codigo != T001T2_A161FuncaoUsuario_Codigo[0] )
               {
                  GXUtil.WriteLog("solicitacoesitens:[seudo value changed for attri]"+"FuncaoUsuario_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z161FuncaoUsuario_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T001T2_A161FuncaoUsuario_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"SolicitacoesItens"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1T68( )
      {
         BeforeValidate1T68( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1T68( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1T68( 0) ;
            CheckOptimisticConcurrency1T68( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1T68( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1T68( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001T12 */
                     pr_default.execute(10, new Object[] {n448SolicitacoesItens_descricao, A448SolicitacoesItens_descricao, n449SolicitacoesItens_Arquivo, A449SolicitacoesItens_Arquivo, A439Solicitacoes_Codigo, A161FuncaoUsuario_Codigo});
                     A447SolicitacoesItens_Codigo = T001T12_A447SolicitacoesItens_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A447SolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A447SolicitacoesItens_Codigo), 6, 0)));
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("SolicitacoesItens") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1T0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1T68( ) ;
            }
            EndLevel1T68( ) ;
         }
         CloseExtendedTableCursors1T68( ) ;
      }

      protected void Update1T68( )
      {
         BeforeValidate1T68( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1T68( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1T68( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1T68( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1T68( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001T13 */
                     pr_default.execute(11, new Object[] {n448SolicitacoesItens_descricao, A448SolicitacoesItens_descricao, A439Solicitacoes_Codigo, A161FuncaoUsuario_Codigo, A447SolicitacoesItens_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("SolicitacoesItens") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SolicitacoesItens"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1T68( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption1T0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1T68( ) ;
         }
         CloseExtendedTableCursors1T68( ) ;
      }

      protected void DeferredUpdate1T68( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T001T14 */
            pr_default.execute(12, new Object[] {n449SolicitacoesItens_Arquivo, A449SolicitacoesItens_Arquivo, A447SolicitacoesItens_Codigo});
            pr_default.close(12);
            dsDefault.SmartCacheProvider.SetUpdated("SolicitacoesItens") ;
         }
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate1T68( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1T68( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1T68( ) ;
            AfterConfirm1T68( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1T68( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001T15 */
                  pr_default.execute(13, new Object[] {A447SolicitacoesItens_Codigo});
                  pr_default.close(13);
                  dsDefault.SmartCacheProvider.SetUpdated("SolicitacoesItens") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound68 == 0 )
                        {
                           InitAll1T68( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption1T0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode68 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel1T68( ) ;
         Gx_mode = sMode68;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls1T68( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel1T68( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1T68( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "SolicitacoesItens");
            if ( AnyError == 0 )
            {
               ConfirmValues1T0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "SolicitacoesItens");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1T68( )
      {
         /* Using cursor T001T16 */
         pr_default.execute(14);
         RcdFound68 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound68 = 1;
            A447SolicitacoesItens_Codigo = T001T16_A447SolicitacoesItens_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A447SolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A447SolicitacoesItens_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1T68( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound68 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound68 = 1;
            A447SolicitacoesItens_Codigo = T001T16_A447SolicitacoesItens_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A447SolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A447SolicitacoesItens_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd1T68( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm1T68( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1T68( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1T68( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1T68( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1T68( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1T68( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1T68( )
      {
         edtSolicitacoesItens_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoesItens_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacoesItens_Codigo_Enabled), 5, 0)));
         edtSolicitacoes_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacoes_Codigo_Enabled), 5, 0)));
         edtSolicitacoesItens_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoesItens_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacoesItens_descricao_Enabled), 5, 0)));
         edtSolicitacoesItens_Arquivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoesItens_Arquivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacoesItens_Arquivo_Enabled), 5, 0)));
         edtFuncaoUsuario_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoUsuario_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1T0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311721227");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("solicitacoesitens.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z447SolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z447SolicitacoesItens_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z439Solicitacoes_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z161FuncaoUsuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXCCtlgxBlob = "SOLICITACOESITENS_ARQUIVO" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A449SolicitacoesItens_Arquivo);
         GxWebStd.gx_hidden_field( context, "SOLICITACOESITENS_ARQUIVO_Filename", StringUtil.RTrim( edtSolicitacoesItens_Arquivo_Filename));
         GxWebStd.gx_hidden_field( context, "SOLICITACOESITENS_ARQUIVO_Filetype", StringUtil.RTrim( edtSolicitacoesItens_Arquivo_Filetype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("solicitacoesitens.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "SolicitacoesItens" ;
      }

      public override String GetPgmdesc( )
      {
         return "Solicitacoes Itens" ;
      }

      protected void InitializeNonKey1T68( )
      {
         A439Solicitacoes_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
         A448SolicitacoesItens_descricao = "";
         n448SolicitacoesItens_descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A448SolicitacoesItens_descricao", A448SolicitacoesItens_descricao);
         n448SolicitacoesItens_descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A448SolicitacoesItens_descricao)) ? true : false);
         A449SolicitacoesItens_Arquivo = "";
         n449SolicitacoesItens_Arquivo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A449SolicitacoesItens_Arquivo", A449SolicitacoesItens_Arquivo);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoesItens_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A449SolicitacoesItens_Arquivo));
         n449SolicitacoesItens_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A449SolicitacoesItens_Arquivo)) ? true : false);
         A161FuncaoUsuario_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
         Z439Solicitacoes_Codigo = 0;
         Z161FuncaoUsuario_Codigo = 0;
      }

      protected void InitAll1T68( )
      {
         A447SolicitacoesItens_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A447SolicitacoesItens_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A447SolicitacoesItens_Codigo), 6, 0)));
         InitializeNonKey1T68( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311721235");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("solicitacoesitens.js", "?2020311721236");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblocksolicitacoesitens_codigo_Internalname = "TEXTBLOCKSOLICITACOESITENS_CODIGO";
         edtSolicitacoesItens_Codigo_Internalname = "SOLICITACOESITENS_CODIGO";
         lblTextblocksolicitacoes_codigo_Internalname = "TEXTBLOCKSOLICITACOES_CODIGO";
         edtSolicitacoes_Codigo_Internalname = "SOLICITACOES_CODIGO";
         lblTextblocksolicitacoesitens_descricao_Internalname = "TEXTBLOCKSOLICITACOESITENS_DESCRICAO";
         edtSolicitacoesItens_descricao_Internalname = "SOLICITACOESITENS_DESCRICAO";
         lblTextblocksolicitacoesitens_arquivo_Internalname = "TEXTBLOCKSOLICITACOESITENS_ARQUIVO";
         edtSolicitacoesItens_Arquivo_Internalname = "SOLICITACOESITENS_ARQUIVO";
         lblTextblockfuncaousuario_codigo_Internalname = "TEXTBLOCKFUNCAOUSUARIO_CODIGO";
         edtFuncaoUsuario_Codigo_Internalname = "FUNCAOUSUARIO_CODIGO";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtSolicitacoesItens_Arquivo_Filename = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Solicitacoes Itens";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtFuncaoUsuario_Codigo_Jsonclick = "";
         edtFuncaoUsuario_Codigo_Enabled = 1;
         edtSolicitacoesItens_Arquivo_Jsonclick = "";
         edtSolicitacoesItens_Arquivo_Parameters = "";
         edtSolicitacoesItens_Arquivo_Contenttype = "";
         edtSolicitacoesItens_Arquivo_Filetype = "";
         edtSolicitacoesItens_Arquivo_Enabled = 1;
         edtSolicitacoesItens_descricao_Enabled = 1;
         edtSolicitacoes_Codigo_Jsonclick = "";
         edtSolicitacoes_Codigo_Enabled = 1;
         edtSolicitacoesItens_Codigo_Jsonclick = "";
         edtSolicitacoesItens_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtSolicitacoes_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Solicitacoesitens_codigo( int GX_Parm1 ,
                                                  String GX_Parm2 ,
                                                  String GX_Parm3 ,
                                                  int GX_Parm4 ,
                                                  int GX_Parm5 )
      {
         A447SolicitacoesItens_Codigo = GX_Parm1;
         A448SolicitacoesItens_descricao = GX_Parm2;
         n448SolicitacoesItens_descricao = false;
         A449SolicitacoesItens_Arquivo = GX_Parm3;
         n449SolicitacoesItens_Arquivo = false;
         A439Solicitacoes_Codigo = GX_Parm4;
         A161FuncaoUsuario_Codigo = GX_Parm5;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A439Solicitacoes_Codigo), 6, 0, ".", "")));
         isValidOutput.Add(A448SolicitacoesItens_descricao);
         isValidOutput.Add(context.PathToRelativeUrl( A449SolicitacoesItens_Arquivo));
         isValidOutput.Add(A449SolicitacoesItens_Arquivo);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A161FuncaoUsuario_Codigo), 6, 0, ".", "")));
         isValidOutput.Add(edtSolicitacoesItens_Arquivo_Filetype);
         isValidOutput.Add(edtSolicitacoesItens_Arquivo_Filename);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z447SolicitacoesItens_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z439Solicitacoes_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(Z448SolicitacoesItens_descricao);
         isValidOutput.Add(context.PathToRelativeUrl( Z449SolicitacoesItens_Arquivo));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z161FuncaoUsuario_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Solicitacoes_codigo( int GX_Parm1 )
      {
         A439Solicitacoes_Codigo = GX_Parm1;
         /* Using cursor T001T17 */
         pr_default.execute(15, new Object[] {A439Solicitacoes_Codigo});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Solicitacoes'.", "ForeignKeyNotFound", 1, "SOLICITACOES_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSolicitacoes_Codigo_Internalname;
         }
         pr_default.close(15);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Funcaousuario_codigo( int GX_Parm1 )
      {
         A161FuncaoUsuario_Codigo = GX_Parm1;
         /* Using cursor T001T18 */
         pr_default.execute(16, new Object[] {A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(16) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Modulo Funcoes'.", "ForeignKeyNotFound", 1, "FUNCAOUSUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoUsuario_Codigo_Internalname;
         }
         pr_default.close(16);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
         pr_default.close(16);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblocksolicitacoesitens_codigo_Jsonclick = "";
         lblTextblocksolicitacoes_codigo_Jsonclick = "";
         lblTextblocksolicitacoesitens_descricao_Jsonclick = "";
         A448SolicitacoesItens_descricao = "";
         lblTextblocksolicitacoesitens_arquivo_Jsonclick = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         A449SolicitacoesItens_Arquivo = "";
         lblTextblockfuncaousuario_codigo_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         GXCCtlgxBlob = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z448SolicitacoesItens_descricao = "";
         Z449SolicitacoesItens_Arquivo = "";
         T001T6_A447SolicitacoesItens_Codigo = new int[1] ;
         T001T6_A448SolicitacoesItens_descricao = new String[] {""} ;
         T001T6_n448SolicitacoesItens_descricao = new bool[] {false} ;
         T001T6_A439Solicitacoes_Codigo = new int[1] ;
         T001T6_A161FuncaoUsuario_Codigo = new int[1] ;
         T001T6_A449SolicitacoesItens_Arquivo = new String[] {""} ;
         T001T6_n449SolicitacoesItens_Arquivo = new bool[] {false} ;
         T001T4_A439Solicitacoes_Codigo = new int[1] ;
         T001T5_A161FuncaoUsuario_Codigo = new int[1] ;
         T001T7_A439Solicitacoes_Codigo = new int[1] ;
         T001T8_A161FuncaoUsuario_Codigo = new int[1] ;
         T001T9_A447SolicitacoesItens_Codigo = new int[1] ;
         T001T3_A447SolicitacoesItens_Codigo = new int[1] ;
         T001T3_A448SolicitacoesItens_descricao = new String[] {""} ;
         T001T3_n448SolicitacoesItens_descricao = new bool[] {false} ;
         T001T3_A439Solicitacoes_Codigo = new int[1] ;
         T001T3_A161FuncaoUsuario_Codigo = new int[1] ;
         T001T3_A449SolicitacoesItens_Arquivo = new String[] {""} ;
         T001T3_n449SolicitacoesItens_Arquivo = new bool[] {false} ;
         sMode68 = "";
         T001T10_A447SolicitacoesItens_Codigo = new int[1] ;
         T001T11_A447SolicitacoesItens_Codigo = new int[1] ;
         T001T2_A447SolicitacoesItens_Codigo = new int[1] ;
         T001T2_A448SolicitacoesItens_descricao = new String[] {""} ;
         T001T2_n448SolicitacoesItens_descricao = new bool[] {false} ;
         T001T2_A439Solicitacoes_Codigo = new int[1] ;
         T001T2_A161FuncaoUsuario_Codigo = new int[1] ;
         T001T2_A449SolicitacoesItens_Arquivo = new String[] {""} ;
         T001T2_n449SolicitacoesItens_Arquivo = new bool[] {false} ;
         T001T12_A447SolicitacoesItens_Codigo = new int[1] ;
         T001T16_A447SolicitacoesItens_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         T001T17_A439Solicitacoes_Codigo = new int[1] ;
         T001T18_A161FuncaoUsuario_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.solicitacoesitens__default(),
            new Object[][] {
                new Object[] {
               T001T2_A447SolicitacoesItens_Codigo, T001T2_A448SolicitacoesItens_descricao, T001T2_n448SolicitacoesItens_descricao, T001T2_A439Solicitacoes_Codigo, T001T2_A161FuncaoUsuario_Codigo, T001T2_A449SolicitacoesItens_Arquivo, T001T2_n449SolicitacoesItens_Arquivo
               }
               , new Object[] {
               T001T3_A447SolicitacoesItens_Codigo, T001T3_A448SolicitacoesItens_descricao, T001T3_n448SolicitacoesItens_descricao, T001T3_A439Solicitacoes_Codigo, T001T3_A161FuncaoUsuario_Codigo, T001T3_A449SolicitacoesItens_Arquivo, T001T3_n449SolicitacoesItens_Arquivo
               }
               , new Object[] {
               T001T4_A439Solicitacoes_Codigo
               }
               , new Object[] {
               T001T5_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               T001T6_A447SolicitacoesItens_Codigo, T001T6_A448SolicitacoesItens_descricao, T001T6_n448SolicitacoesItens_descricao, T001T6_A439Solicitacoes_Codigo, T001T6_A161FuncaoUsuario_Codigo, T001T6_A449SolicitacoesItens_Arquivo, T001T6_n449SolicitacoesItens_Arquivo
               }
               , new Object[] {
               T001T7_A439Solicitacoes_Codigo
               }
               , new Object[] {
               T001T8_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               T001T9_A447SolicitacoesItens_Codigo
               }
               , new Object[] {
               T001T10_A447SolicitacoesItens_Codigo
               }
               , new Object[] {
               T001T11_A447SolicitacoesItens_Codigo
               }
               , new Object[] {
               T001T12_A447SolicitacoesItens_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001T16_A447SolicitacoesItens_Codigo
               }
               , new Object[] {
               T001T17_A439Solicitacoes_Codigo
               }
               , new Object[] {
               T001T18_A161FuncaoUsuario_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound68 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z447SolicitacoesItens_Codigo ;
      private int Z439Solicitacoes_Codigo ;
      private int Z161FuncaoUsuario_Codigo ;
      private int A439Solicitacoes_Codigo ;
      private int A161FuncaoUsuario_Codigo ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int A447SolicitacoesItens_Codigo ;
      private int edtSolicitacoesItens_Codigo_Enabled ;
      private int edtSolicitacoes_Codigo_Enabled ;
      private int edtSolicitacoesItens_descricao_Enabled ;
      private int edtSolicitacoesItens_Arquivo_Enabled ;
      private int edtFuncaoUsuario_Codigo_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtSolicitacoesItens_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblocksolicitacoesitens_codigo_Internalname ;
      private String lblTextblocksolicitacoesitens_codigo_Jsonclick ;
      private String edtSolicitacoesItens_Codigo_Jsonclick ;
      private String lblTextblocksolicitacoes_codigo_Internalname ;
      private String lblTextblocksolicitacoes_codigo_Jsonclick ;
      private String edtSolicitacoes_Codigo_Internalname ;
      private String edtSolicitacoes_Codigo_Jsonclick ;
      private String lblTextblocksolicitacoesitens_descricao_Internalname ;
      private String lblTextblocksolicitacoesitens_descricao_Jsonclick ;
      private String edtSolicitacoesItens_descricao_Internalname ;
      private String lblTextblocksolicitacoesitens_arquivo_Internalname ;
      private String lblTextblocksolicitacoesitens_arquivo_Jsonclick ;
      private String edtSolicitacoesItens_Arquivo_Filetype ;
      private String edtSolicitacoesItens_Arquivo_Internalname ;
      private String edtSolicitacoesItens_Arquivo_Contenttype ;
      private String edtSolicitacoesItens_Arquivo_Parameters ;
      private String edtSolicitacoesItens_Arquivo_Jsonclick ;
      private String lblTextblockfuncaousuario_codigo_Internalname ;
      private String lblTextblockfuncaousuario_codigo_Jsonclick ;
      private String edtFuncaoUsuario_Codigo_Internalname ;
      private String edtFuncaoUsuario_Codigo_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String edtSolicitacoesItens_Arquivo_Filename ;
      private String GXCCtlgxBlob ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode68 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n449SolicitacoesItens_Arquivo ;
      private bool n448SolicitacoesItens_descricao ;
      private String A448SolicitacoesItens_descricao ;
      private String Z448SolicitacoesItens_descricao ;
      private String A449SolicitacoesItens_Arquivo ;
      private String Z449SolicitacoesItens_Arquivo ;
      private GxUnknownObjectCollection isValidOutput ;
      private GxFile gxblobfileaux ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T001T6_A447SolicitacoesItens_Codigo ;
      private String[] T001T6_A448SolicitacoesItens_descricao ;
      private bool[] T001T6_n448SolicitacoesItens_descricao ;
      private int[] T001T6_A439Solicitacoes_Codigo ;
      private int[] T001T6_A161FuncaoUsuario_Codigo ;
      private String[] T001T6_A449SolicitacoesItens_Arquivo ;
      private bool[] T001T6_n449SolicitacoesItens_Arquivo ;
      private int[] T001T4_A439Solicitacoes_Codigo ;
      private int[] T001T5_A161FuncaoUsuario_Codigo ;
      private int[] T001T7_A439Solicitacoes_Codigo ;
      private int[] T001T8_A161FuncaoUsuario_Codigo ;
      private int[] T001T9_A447SolicitacoesItens_Codigo ;
      private int[] T001T3_A447SolicitacoesItens_Codigo ;
      private String[] T001T3_A448SolicitacoesItens_descricao ;
      private bool[] T001T3_n448SolicitacoesItens_descricao ;
      private int[] T001T3_A439Solicitacoes_Codigo ;
      private int[] T001T3_A161FuncaoUsuario_Codigo ;
      private String[] T001T3_A449SolicitacoesItens_Arquivo ;
      private bool[] T001T3_n449SolicitacoesItens_Arquivo ;
      private int[] T001T10_A447SolicitacoesItens_Codigo ;
      private int[] T001T11_A447SolicitacoesItens_Codigo ;
      private int[] T001T2_A447SolicitacoesItens_Codigo ;
      private String[] T001T2_A448SolicitacoesItens_descricao ;
      private bool[] T001T2_n448SolicitacoesItens_descricao ;
      private int[] T001T2_A439Solicitacoes_Codigo ;
      private int[] T001T2_A161FuncaoUsuario_Codigo ;
      private String[] T001T2_A449SolicitacoesItens_Arquivo ;
      private bool[] T001T2_n449SolicitacoesItens_Arquivo ;
      private int[] T001T12_A447SolicitacoesItens_Codigo ;
      private int[] T001T16_A447SolicitacoesItens_Codigo ;
      private int[] T001T17_A439Solicitacoes_Codigo ;
      private int[] T001T18_A161FuncaoUsuario_Codigo ;
      private GXWebForm Form ;
   }

   public class solicitacoesitens__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001T6 ;
          prmT001T6 = new Object[] {
          new Object[] {"@SolicitacoesItens_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001T4 ;
          prmT001T4 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001T5 ;
          prmT001T5 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001T7 ;
          prmT001T7 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001T8 ;
          prmT001T8 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001T9 ;
          prmT001T9 = new Object[] {
          new Object[] {"@SolicitacoesItens_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001T3 ;
          prmT001T3 = new Object[] {
          new Object[] {"@SolicitacoesItens_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001T10 ;
          prmT001T10 = new Object[] {
          new Object[] {"@SolicitacoesItens_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001T11 ;
          prmT001T11 = new Object[] {
          new Object[] {"@SolicitacoesItens_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001T2 ;
          prmT001T2 = new Object[] {
          new Object[] {"@SolicitacoesItens_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001T12 ;
          prmT001T12 = new Object[] {
          new Object[] {"@SolicitacoesItens_descricao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@SolicitacoesItens_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001T13 ;
          prmT001T13 = new Object[] {
          new Object[] {"@SolicitacoesItens_descricao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicitacoesItens_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001T14 ;
          prmT001T14 = new Object[] {
          new Object[] {"@SolicitacoesItens_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@SolicitacoesItens_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001T15 ;
          prmT001T15 = new Object[] {
          new Object[] {"@SolicitacoesItens_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001T16 ;
          prmT001T16 = new Object[] {
          } ;
          Object[] prmT001T17 ;
          prmT001T17 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001T18 ;
          prmT001T18 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T001T2", "SELECT [SolicitacoesItens_Codigo], [SolicitacoesItens_descricao], [Solicitacoes_Codigo], [FuncaoUsuario_Codigo], [SolicitacoesItens_Arquivo] FROM [SolicitacoesItens] WITH (UPDLOCK) WHERE [SolicitacoesItens_Codigo] = @SolicitacoesItens_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001T2,1,0,true,false )
             ,new CursorDef("T001T3", "SELECT [SolicitacoesItens_Codigo], [SolicitacoesItens_descricao], [Solicitacoes_Codigo], [FuncaoUsuario_Codigo], [SolicitacoesItens_Arquivo] FROM [SolicitacoesItens] WITH (NOLOCK) WHERE [SolicitacoesItens_Codigo] = @SolicitacoesItens_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001T3,1,0,true,false )
             ,new CursorDef("T001T4", "SELECT [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Solicitacoes_Codigo] = @Solicitacoes_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001T4,1,0,true,false )
             ,new CursorDef("T001T5", "SELECT [FuncaoUsuario_Codigo] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001T5,1,0,true,false )
             ,new CursorDef("T001T6", "SELECT TM1.[SolicitacoesItens_Codigo], TM1.[SolicitacoesItens_descricao], TM1.[Solicitacoes_Codigo], TM1.[FuncaoUsuario_Codigo], TM1.[SolicitacoesItens_Arquivo] FROM [SolicitacoesItens] TM1 WITH (NOLOCK) WHERE TM1.[SolicitacoesItens_Codigo] = @SolicitacoesItens_Codigo ORDER BY TM1.[SolicitacoesItens_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001T6,100,0,true,false )
             ,new CursorDef("T001T7", "SELECT [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Solicitacoes_Codigo] = @Solicitacoes_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001T7,1,0,true,false )
             ,new CursorDef("T001T8", "SELECT [FuncaoUsuario_Codigo] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001T8,1,0,true,false )
             ,new CursorDef("T001T9", "SELECT [SolicitacoesItens_Codigo] FROM [SolicitacoesItens] WITH (NOLOCK) WHERE [SolicitacoesItens_Codigo] = @SolicitacoesItens_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001T9,1,0,true,false )
             ,new CursorDef("T001T10", "SELECT TOP 1 [SolicitacoesItens_Codigo] FROM [SolicitacoesItens] WITH (NOLOCK) WHERE ( [SolicitacoesItens_Codigo] > @SolicitacoesItens_Codigo) ORDER BY [SolicitacoesItens_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001T10,1,0,true,true )
             ,new CursorDef("T001T11", "SELECT TOP 1 [SolicitacoesItens_Codigo] FROM [SolicitacoesItens] WITH (NOLOCK) WHERE ( [SolicitacoesItens_Codigo] < @SolicitacoesItens_Codigo) ORDER BY [SolicitacoesItens_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001T11,1,0,true,true )
             ,new CursorDef("T001T12", "INSERT INTO [SolicitacoesItens]([SolicitacoesItens_descricao], [SolicitacoesItens_Arquivo], [Solicitacoes_Codigo], [FuncaoUsuario_Codigo]) VALUES(@SolicitacoesItens_descricao, @SolicitacoesItens_Arquivo, @Solicitacoes_Codigo, @FuncaoUsuario_Codigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT001T12)
             ,new CursorDef("T001T13", "UPDATE [SolicitacoesItens] SET [SolicitacoesItens_descricao]=@SolicitacoesItens_descricao, [Solicitacoes_Codigo]=@Solicitacoes_Codigo, [FuncaoUsuario_Codigo]=@FuncaoUsuario_Codigo  WHERE [SolicitacoesItens_Codigo] = @SolicitacoesItens_Codigo", GxErrorMask.GX_NOMASK,prmT001T13)
             ,new CursorDef("T001T14", "UPDATE [SolicitacoesItens] SET [SolicitacoesItens_Arquivo]=@SolicitacoesItens_Arquivo  WHERE [SolicitacoesItens_Codigo] = @SolicitacoesItens_Codigo", GxErrorMask.GX_NOMASK,prmT001T14)
             ,new CursorDef("T001T15", "DELETE FROM [SolicitacoesItens]  WHERE [SolicitacoesItens_Codigo] = @SolicitacoesItens_Codigo", GxErrorMask.GX_NOMASK,prmT001T15)
             ,new CursorDef("T001T16", "SELECT [SolicitacoesItens_Codigo] FROM [SolicitacoesItens] WITH (NOLOCK) ORDER BY [SolicitacoesItens_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001T16,100,0,true,false )
             ,new CursorDef("T001T17", "SELECT [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Solicitacoes_Codigo] = @Solicitacoes_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001T17,1,0,true,false )
             ,new CursorDef("T001T18", "SELECT [FuncaoUsuario_Codigo] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001T18,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getBLOBFile(5, "tmp", "") ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getBLOBFile(5, "tmp", "") ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getBLOBFile(5, "tmp", "") ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                stmt.SetParameter(4, (int)parms[5]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                stmt.SetParameter(4, (int)parms[4]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
