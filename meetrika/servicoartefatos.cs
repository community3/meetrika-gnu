/*
               File: ServicoArtefatos
        Description: Artefatos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:49.62
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicoartefatos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SERVICOARTEFATO_ARTEFATOCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLASERVICOARTEFATO_ARTEFATOCOD4F195( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_9") == 0 )
         {
            A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_9( A155Servico_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A1766ServicoArtefato_ArtefatoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1766ServicoArtefato_ArtefatoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1766ServicoArtefato_ArtefatoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A1766ServicoArtefato_ArtefatoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV8Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Servico_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Servico_Codigo), "ZZZZZ9")));
               AV9ServicoArtefato_ArtefatoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ServicoArtefato_ArtefatoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ServicoArtefato_ArtefatoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICOARTEFATO_ARTEFATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ServicoArtefato_ArtefatoCod), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynServicoArtefato_ArtefatoCod.Name = "SERVICOARTEFATO_ARTEFATOCOD";
         dynServicoArtefato_ArtefatoCod.WebTags = "";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Artefatos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynServicoArtefato_ArtefatoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public servicoartefatos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public servicoartefatos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Servico_Codigo ,
                           int aP2_ServicoArtefato_ArtefatoCod )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV8Servico_Codigo = aP1_Servico_Codigo;
         this.AV9ServicoArtefato_ArtefatoCod = aP2_ServicoArtefato_ArtefatoCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynServicoArtefato_ArtefatoCod = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynServicoArtefato_ArtefatoCod.ItemCount > 0 )
         {
            A1766ServicoArtefato_ArtefatoCod = (int)(NumberUtil.Val( dynServicoArtefato_ArtefatoCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1766ServicoArtefato_ArtefatoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1766ServicoArtefato_ArtefatoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1766ServicoArtefato_ArtefatoCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_4F195( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_4F195e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServico_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,29);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtServico_Codigo_Visible, edtServico_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoArtefatos.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_4F195( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_4F195( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_4F195e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_21_4F195( true) ;
         }
         return  ;
      }

      protected void wb_table3_21_4F195e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4F195e( true) ;
         }
         else
         {
            wb_table1_2_4F195e( false) ;
         }
      }

      protected void wb_table3_21_4F195( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_21_4F195e( true) ;
         }
         else
         {
            wb_table3_21_4F195e( false) ;
         }
      }

      protected void wb_table2_5_4F195( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_4F195( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_4F195e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_4F195e( true) ;
         }
         else
         {
            wb_table2_5_4F195e( false) ;
         }
      }

      protected void wb_table4_13_4F195( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicoartefato_artefatocod_Internalname, "Descri��o", "", "", lblTextblockservicoartefato_artefatocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynServicoArtefato_ArtefatoCod, dynServicoArtefato_ArtefatoCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1766ServicoArtefato_ArtefatoCod), 6, 0)), 1, dynServicoArtefato_ArtefatoCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynServicoArtefato_ArtefatoCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_ServicoArtefatos.htm");
            dynServicoArtefato_ArtefatoCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1766ServicoArtefato_ArtefatoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServicoArtefato_ArtefatoCod_Internalname, "Values", (String)(dynServicoArtefato_ArtefatoCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_4F195e( true) ;
         }
         else
         {
            wb_table4_13_4F195e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E114F2 */
         E114F2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynServicoArtefato_ArtefatoCod.CurrentValue = cgiGet( dynServicoArtefato_ArtefatoCod_Internalname);
               A1766ServicoArtefato_ArtefatoCod = (int)(NumberUtil.Val( cgiGet( dynServicoArtefato_ArtefatoCod_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1766ServicoArtefato_ArtefatoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1766ServicoArtefato_ArtefatoCod), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SERVICO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtServico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A155Servico_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               }
               else
               {
                  A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               }
               /* Read saved values. */
               Z155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z155Servico_Codigo"), ",", "."));
               Z1766ServicoArtefato_ArtefatoCod = (int)(context.localUtil.CToN( cgiGet( "Z1766ServicoArtefato_ArtefatoCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV8Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSERVICO_CODIGO"), ",", "."));
               AV9ServicoArtefato_ArtefatoCod = (int)(context.localUtil.CToN( cgiGet( "vSERVICOARTEFATO_ARTEFATOCOD"), ",", "."));
               A1767ServicoArtefato_ArtefatoDsc = cgiGet( "SERVICOARTEFATO_ARTEFATODSC");
               n1767ServicoArtefato_ArtefatoDsc = false;
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ServicoArtefatos";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A155Servico_Codigo != Z155Servico_Codigo ) || ( A1766ServicoArtefato_ArtefatoCod != Z1766ServicoArtefato_ArtefatoCod ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("servicoartefatos:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
                  A1766ServicoArtefato_ArtefatoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1766ServicoArtefato_ArtefatoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1766ServicoArtefato_ArtefatoCod), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode195 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode195;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound195 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_4F0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "SERVICO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtServico_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E114F2 */
                           E114F2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E124F2 */
                           E124F2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E124F2 */
            E124F2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll4F195( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes4F195( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_4F0( )
      {
         BeforeValidate4F195( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4F195( ) ;
            }
            else
            {
               CheckExtendedTable4F195( ) ;
               CloseExtendedTableCursors4F195( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption4F0( )
      {
      }

      protected void E114F2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV13WWPContext) ;
         AV10TrnContext.FromXml(AV12WebSession.Get("TrnContext"), "");
         edtServico_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Visible), 5, 0)));
      }

      protected void E124F2( )
      {
         /* After Trn Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM4F195( short GX_JID )
      {
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
            }
            else
            {
            }
         }
         if ( GX_JID == -8 )
         {
            Z155Servico_Codigo = A155Servico_Codigo;
            Z1766ServicoArtefato_ArtefatoCod = A1766ServicoArtefato_ArtefatoCod;
            Z1767ServicoArtefato_ArtefatoDsc = A1767ServicoArtefato_ArtefatoDsc;
         }
      }

      protected void standaloneNotModal( )
      {
         GXASERVICOARTEFATO_ARTEFATOCOD_html4F195( ) ;
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV8Servico_Codigo) )
         {
            A155Servico_Codigo = AV8Servico_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         }
         if ( ! (0==AV8Servico_Codigo) )
         {
            edtServico_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtServico_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV8Servico_Codigo) )
         {
            edtServico_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV9ServicoArtefato_ArtefatoCod) )
         {
            A1766ServicoArtefato_ArtefatoCod = AV9ServicoArtefato_ArtefatoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1766ServicoArtefato_ArtefatoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1766ServicoArtefato_ArtefatoCod), 6, 0)));
         }
         if ( ! (0==AV9ServicoArtefato_ArtefatoCod) )
         {
            dynServicoArtefato_ArtefatoCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServicoArtefato_ArtefatoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServicoArtefato_ArtefatoCod.Enabled), 5, 0)));
         }
         else
         {
            dynServicoArtefato_ArtefatoCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServicoArtefato_ArtefatoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServicoArtefato_ArtefatoCod.Enabled), 5, 0)));
         }
         if ( ! (0==AV9ServicoArtefato_ArtefatoCod) )
         {
            dynServicoArtefato_ArtefatoCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServicoArtefato_ArtefatoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServicoArtefato_ArtefatoCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T004F5 */
            pr_default.execute(3, new Object[] {A1766ServicoArtefato_ArtefatoCod});
            A1767ServicoArtefato_ArtefatoDsc = T004F5_A1767ServicoArtefato_ArtefatoDsc[0];
            n1767ServicoArtefato_ArtefatoDsc = T004F5_n1767ServicoArtefato_ArtefatoDsc[0];
            pr_default.close(3);
         }
      }

      protected void Load4F195( )
      {
         /* Using cursor T004F6 */
         pr_default.execute(4, new Object[] {A155Servico_Codigo, A1766ServicoArtefato_ArtefatoCod});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound195 = 1;
            A1767ServicoArtefato_ArtefatoDsc = T004F6_A1767ServicoArtefato_ArtefatoDsc[0];
            n1767ServicoArtefato_ArtefatoDsc = T004F6_n1767ServicoArtefato_ArtefatoDsc[0];
            ZM4F195( -8) ;
         }
         pr_default.close(4);
         OnLoadActions4F195( ) ;
      }

      protected void OnLoadActions4F195( )
      {
      }

      protected void CheckExtendedTable4F195( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T004F4 */
         pr_default.execute(2, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtServico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T004F5 */
         pr_default.execute(3, new Object[] {A1766ServicoArtefato_ArtefatoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico_Artefatos'.", "ForeignKeyNotFound", 1, "SERVICOARTEFATO_ARTEFATOCOD");
            AnyError = 1;
            GX_FocusControl = dynServicoArtefato_ArtefatoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1767ServicoArtefato_ArtefatoDsc = T004F5_A1767ServicoArtefato_ArtefatoDsc[0];
         n1767ServicoArtefato_ArtefatoDsc = T004F5_n1767ServicoArtefato_ArtefatoDsc[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors4F195( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_9( int A155Servico_Codigo )
      {
         /* Using cursor T004F7 */
         pr_default.execute(5, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtServico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_10( int A1766ServicoArtefato_ArtefatoCod )
      {
         /* Using cursor T004F8 */
         pr_default.execute(6, new Object[] {A1766ServicoArtefato_ArtefatoCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico_Artefatos'.", "ForeignKeyNotFound", 1, "SERVICOARTEFATO_ARTEFATOCOD");
            AnyError = 1;
            GX_FocusControl = dynServicoArtefato_ArtefatoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1767ServicoArtefato_ArtefatoDsc = T004F8_A1767ServicoArtefato_ArtefatoDsc[0];
         n1767ServicoArtefato_ArtefatoDsc = T004F8_n1767ServicoArtefato_ArtefatoDsc[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A1767ServicoArtefato_ArtefatoDsc)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey4F195( )
      {
         /* Using cursor T004F9 */
         pr_default.execute(7, new Object[] {A155Servico_Codigo, A1766ServicoArtefato_ArtefatoCod});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound195 = 1;
         }
         else
         {
            RcdFound195 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T004F3 */
         pr_default.execute(1, new Object[] {A155Servico_Codigo, A1766ServicoArtefato_ArtefatoCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4F195( 8) ;
            RcdFound195 = 1;
            A155Servico_Codigo = T004F3_A155Servico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A1766ServicoArtefato_ArtefatoCod = T004F3_A1766ServicoArtefato_ArtefatoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1766ServicoArtefato_ArtefatoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1766ServicoArtefato_ArtefatoCod), 6, 0)));
            Z155Servico_Codigo = A155Servico_Codigo;
            Z1766ServicoArtefato_ArtefatoCod = A1766ServicoArtefato_ArtefatoCod;
            sMode195 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load4F195( ) ;
            if ( AnyError == 1 )
            {
               RcdFound195 = 0;
               InitializeNonKey4F195( ) ;
            }
            Gx_mode = sMode195;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound195 = 0;
            InitializeNonKey4F195( ) ;
            sMode195 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode195;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4F195( ) ;
         if ( RcdFound195 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound195 = 0;
         /* Using cursor T004F10 */
         pr_default.execute(8, new Object[] {A155Servico_Codigo, A1766ServicoArtefato_ArtefatoCod});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T004F10_A155Servico_Codigo[0] < A155Servico_Codigo ) || ( T004F10_A155Servico_Codigo[0] == A155Servico_Codigo ) && ( T004F10_A1766ServicoArtefato_ArtefatoCod[0] < A1766ServicoArtefato_ArtefatoCod ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T004F10_A155Servico_Codigo[0] > A155Servico_Codigo ) || ( T004F10_A155Servico_Codigo[0] == A155Servico_Codigo ) && ( T004F10_A1766ServicoArtefato_ArtefatoCod[0] > A1766ServicoArtefato_ArtefatoCod ) ) )
            {
               A155Servico_Codigo = T004F10_A155Servico_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               A1766ServicoArtefato_ArtefatoCod = T004F10_A1766ServicoArtefato_ArtefatoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1766ServicoArtefato_ArtefatoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1766ServicoArtefato_ArtefatoCod), 6, 0)));
               RcdFound195 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound195 = 0;
         /* Using cursor T004F11 */
         pr_default.execute(9, new Object[] {A155Servico_Codigo, A1766ServicoArtefato_ArtefatoCod});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T004F11_A155Servico_Codigo[0] > A155Servico_Codigo ) || ( T004F11_A155Servico_Codigo[0] == A155Servico_Codigo ) && ( T004F11_A1766ServicoArtefato_ArtefatoCod[0] > A1766ServicoArtefato_ArtefatoCod ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T004F11_A155Servico_Codigo[0] < A155Servico_Codigo ) || ( T004F11_A155Servico_Codigo[0] == A155Servico_Codigo ) && ( T004F11_A1766ServicoArtefato_ArtefatoCod[0] < A1766ServicoArtefato_ArtefatoCod ) ) )
            {
               A155Servico_Codigo = T004F11_A155Servico_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               A1766ServicoArtefato_ArtefatoCod = T004F11_A1766ServicoArtefato_ArtefatoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1766ServicoArtefato_ArtefatoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1766ServicoArtefato_ArtefatoCod), 6, 0)));
               RcdFound195 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey4F195( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynServicoArtefato_ArtefatoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert4F195( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound195 == 1 )
            {
               if ( ( A155Servico_Codigo != Z155Servico_Codigo ) || ( A1766ServicoArtefato_ArtefatoCod != Z1766ServicoArtefato_ArtefatoCod ) )
               {
                  A155Servico_Codigo = Z155Servico_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
                  A1766ServicoArtefato_ArtefatoCod = Z1766ServicoArtefato_ArtefatoCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1766ServicoArtefato_ArtefatoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1766ServicoArtefato_ArtefatoCod), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "SERVICO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtServico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynServicoArtefato_ArtefatoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update4F195( ) ;
                  GX_FocusControl = dynServicoArtefato_ArtefatoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A155Servico_Codigo != Z155Servico_Codigo ) || ( A1766ServicoArtefato_ArtefatoCod != Z1766ServicoArtefato_ArtefatoCod ) )
               {
                  /* Insert record */
                  GX_FocusControl = dynServicoArtefato_ArtefatoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert4F195( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "SERVICO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtServico_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = dynServicoArtefato_ArtefatoCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert4F195( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( ( A155Servico_Codigo != Z155Servico_Codigo ) || ( A1766ServicoArtefato_ArtefatoCod != Z1766ServicoArtefato_ArtefatoCod ) )
         {
            A155Servico_Codigo = Z155Servico_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A1766ServicoArtefato_ArtefatoCod = Z1766ServicoArtefato_ArtefatoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1766ServicoArtefato_ArtefatoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1766ServicoArtefato_ArtefatoCod), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtServico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynServicoArtefato_ArtefatoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency4F195( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T004F2 */
            pr_default.execute(0, new Object[] {A155Servico_Codigo, A1766ServicoArtefato_ArtefatoCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ServicoArtefatos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ServicoArtefatos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4F195( )
      {
         BeforeValidate4F195( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4F195( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4F195( 0) ;
            CheckOptimisticConcurrency4F195( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4F195( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4F195( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004F12 */
                     pr_default.execute(10, new Object[] {A155Servico_Codigo, A1766ServicoArtefato_ArtefatoCod});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ServicoArtefatos") ;
                     if ( (pr_default.getStatus(10) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption4F0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4F195( ) ;
            }
            EndLevel4F195( ) ;
         }
         CloseExtendedTableCursors4F195( ) ;
      }

      protected void Update4F195( )
      {
         BeforeValidate4F195( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4F195( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4F195( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4F195( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4F195( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ServicoArtefatos] */
                     DeferredUpdate4F195( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4F195( ) ;
         }
         CloseExtendedTableCursors4F195( ) ;
      }

      protected void DeferredUpdate4F195( )
      {
      }

      protected void delete( )
      {
         BeforeValidate4F195( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4F195( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4F195( ) ;
            AfterConfirm4F195( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4F195( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004F13 */
                  pr_default.execute(11, new Object[] {A155Servico_Codigo, A1766ServicoArtefato_ArtefatoCod});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("ServicoArtefatos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode195 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel4F195( ) ;
         Gx_mode = sMode195;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls4F195( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T004F14 */
            pr_default.execute(12, new Object[] {A1766ServicoArtefato_ArtefatoCod});
            A1767ServicoArtefato_ArtefatoDsc = T004F14_A1767ServicoArtefato_ArtefatoDsc[0];
            n1767ServicoArtefato_ArtefatoDsc = T004F14_n1767ServicoArtefato_ArtefatoDsc[0];
            pr_default.close(12);
         }
      }

      protected void EndLevel4F195( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4F195( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(12);
            context.CommitDataStores( "ServicoArtefatos");
            if ( AnyError == 0 )
            {
               ConfirmValues4F0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(12);
            context.RollbackDataStores( "ServicoArtefatos");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart4F195( )
      {
         /* Scan By routine */
         /* Using cursor T004F15 */
         pr_default.execute(13);
         RcdFound195 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound195 = 1;
            A155Servico_Codigo = T004F15_A155Servico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A1766ServicoArtefato_ArtefatoCod = T004F15_A1766ServicoArtefato_ArtefatoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1766ServicoArtefato_ArtefatoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1766ServicoArtefato_ArtefatoCod), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext4F195( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound195 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound195 = 1;
            A155Servico_Codigo = T004F15_A155Servico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A1766ServicoArtefato_ArtefatoCod = T004F15_A1766ServicoArtefato_ArtefatoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1766ServicoArtefato_ArtefatoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1766ServicoArtefato_ArtefatoCod), 6, 0)));
         }
      }

      protected void ScanEnd4F195( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm4F195( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4F195( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4F195( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4F195( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4F195( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4F195( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4F195( )
      {
         dynServicoArtefato_ArtefatoCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServicoArtefato_ArtefatoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServicoArtefato_ArtefatoCod.Enabled), 5, 0)));
         edtServico_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues4F0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117295026");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("servicoartefatos.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV8Servico_Codigo) + "," + UrlEncode("" +AV9ServicoArtefato_ArtefatoCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z155Servico_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1766ServicoArtefato_ArtefatoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1766ServicoArtefato_ArtefatoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vSERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSERVICOARTEFATO_ARTEFATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9ServicoArtefato_ArtefatoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICOARTEFATO_ARTEFATODSC", A1767ServicoArtefato_ArtefatoDsc);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Servico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICOARTEFATO_ARTEFATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ServicoArtefato_ArtefatoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ServicoArtefatos";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("servicoartefatos:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("servicoartefatos.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV8Servico_Codigo) + "," + UrlEncode("" +AV9ServicoArtefato_ArtefatoCod) ;
      }

      public override String GetPgmname( )
      {
         return "ServicoArtefatos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Artefatos" ;
      }

      protected void InitializeNonKey4F195( )
      {
         A1767ServicoArtefato_ArtefatoDsc = "";
         n1767ServicoArtefato_ArtefatoDsc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1767ServicoArtefato_ArtefatoDsc", A1767ServicoArtefato_ArtefatoDsc);
      }

      protected void InitAll4F195( )
      {
         A155Servico_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         A1766ServicoArtefato_ArtefatoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1766ServicoArtefato_ArtefatoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1766ServicoArtefato_ArtefatoCod), 6, 0)));
         InitializeNonKey4F195( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117295037");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("servicoartefatos.js", "?20203117295037");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockservicoartefato_artefatocod_Internalname = "TEXTBLOCKSERVICOARTEFATO_ARTEFATOCOD";
         dynServicoArtefato_ArtefatoCod_Internalname = "SERVICOARTEFATO_ARTEFATOCOD";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtServico_Codigo_Internalname = "SERVICO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Artefato";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Artefatos";
         dynServicoArtefato_ArtefatoCod_Jsonclick = "";
         dynServicoArtefato_ArtefatoCod.Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtServico_Codigo_Jsonclick = "";
         edtServico_Codigo_Enabled = 1;
         edtServico_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLASERVICOARTEFATO_ARTEFATOCOD4F195( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLASERVICOARTEFATO_ARTEFATOCOD_data4F195( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXASERVICOARTEFATO_ARTEFATOCOD_html4F195( )
      {
         int gxdynajaxvalue ;
         GXDLASERVICOARTEFATO_ARTEFATOCOD_data4F195( ) ;
         gxdynajaxindex = 1;
         dynServicoArtefato_ArtefatoCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynServicoArtefato_ArtefatoCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLASERVICOARTEFATO_ARTEFATOCOD_data4F195( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor T004F16 */
         pr_default.execute(14);
         while ( (pr_default.getStatus(14) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T004F16_A1766ServicoArtefato_ArtefatoCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T004F16_A1767ServicoArtefato_ArtefatoDsc[0]);
            pr_default.readNext(14);
         }
         pr_default.close(14);
      }

      public void Valid_Servicoartefato_artefatocod( GXCombobox dynGX_Parm1 ,
                                                     String GX_Parm2 )
      {
         dynServicoArtefato_ArtefatoCod = dynGX_Parm1;
         A1766ServicoArtefato_ArtefatoCod = (int)(NumberUtil.Val( dynServicoArtefato_ArtefatoCod.CurrentValue, "."));
         A1767ServicoArtefato_ArtefatoDsc = GX_Parm2;
         n1767ServicoArtefato_ArtefatoDsc = false;
         /* Using cursor T004F17 */
         pr_default.execute(15, new Object[] {A1766ServicoArtefato_ArtefatoCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico_Artefatos'.", "ForeignKeyNotFound", 1, "SERVICOARTEFATO_ARTEFATOCOD");
            AnyError = 1;
            GX_FocusControl = dynServicoArtefato_ArtefatoCod_Internalname;
         }
         A1767ServicoArtefato_ArtefatoDsc = T004F17_A1767ServicoArtefato_ArtefatoDsc[0];
         n1767ServicoArtefato_ArtefatoDsc = T004F17_n1767ServicoArtefato_ArtefatoDsc[0];
         pr_default.close(15);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1767ServicoArtefato_ArtefatoDsc = "";
            n1767ServicoArtefato_ArtefatoDsc = false;
         }
         isValidOutput.Add(A1767ServicoArtefato_ArtefatoDsc);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Servico_codigo( int GX_Parm1 )
      {
         A155Servico_Codigo = GX_Parm1;
         /* Using cursor T004F18 */
         pr_default.execute(16, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(16) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtServico_Codigo_Internalname;
         }
         pr_default.close(16);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV8Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV9ServicoArtefato_ArtefatoCod',fld:'vSERVICOARTEFATO_ARTEFATOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E124F2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(16);
         pr_default.close(15);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockservicoartefato_artefatocod_Jsonclick = "";
         A1767ServicoArtefato_ArtefatoDsc = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode195 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV13WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV12WebSession = context.GetSession();
         Z1767ServicoArtefato_ArtefatoDsc = "";
         T004F5_A1767ServicoArtefato_ArtefatoDsc = new String[] {""} ;
         T004F5_n1767ServicoArtefato_ArtefatoDsc = new bool[] {false} ;
         T004F6_A1767ServicoArtefato_ArtefatoDsc = new String[] {""} ;
         T004F6_n1767ServicoArtefato_ArtefatoDsc = new bool[] {false} ;
         T004F6_A155Servico_Codigo = new int[1] ;
         T004F6_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         T004F4_A155Servico_Codigo = new int[1] ;
         T004F7_A155Servico_Codigo = new int[1] ;
         T004F8_A1767ServicoArtefato_ArtefatoDsc = new String[] {""} ;
         T004F8_n1767ServicoArtefato_ArtefatoDsc = new bool[] {false} ;
         T004F9_A155Servico_Codigo = new int[1] ;
         T004F9_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         T004F3_A155Servico_Codigo = new int[1] ;
         T004F3_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         T004F10_A155Servico_Codigo = new int[1] ;
         T004F10_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         T004F11_A155Servico_Codigo = new int[1] ;
         T004F11_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         T004F2_A155Servico_Codigo = new int[1] ;
         T004F2_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         T004F14_A1767ServicoArtefato_ArtefatoDsc = new String[] {""} ;
         T004F14_n1767ServicoArtefato_ArtefatoDsc = new bool[] {false} ;
         T004F15_A155Servico_Codigo = new int[1] ;
         T004F15_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T004F16_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         T004F16_A1767ServicoArtefato_ArtefatoDsc = new String[] {""} ;
         T004F16_n1767ServicoArtefato_ArtefatoDsc = new bool[] {false} ;
         T004F17_A1767ServicoArtefato_ArtefatoDsc = new String[] {""} ;
         T004F17_n1767ServicoArtefato_ArtefatoDsc = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         T004F18_A155Servico_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicoartefatos__default(),
            new Object[][] {
                new Object[] {
               T004F2_A155Servico_Codigo, T004F2_A1766ServicoArtefato_ArtefatoCod
               }
               , new Object[] {
               T004F3_A155Servico_Codigo, T004F3_A1766ServicoArtefato_ArtefatoCod
               }
               , new Object[] {
               T004F4_A155Servico_Codigo
               }
               , new Object[] {
               T004F5_A1767ServicoArtefato_ArtefatoDsc, T004F5_n1767ServicoArtefato_ArtefatoDsc
               }
               , new Object[] {
               T004F6_A1767ServicoArtefato_ArtefatoDsc, T004F6_n1767ServicoArtefato_ArtefatoDsc, T004F6_A155Servico_Codigo, T004F6_A1766ServicoArtefato_ArtefatoCod
               }
               , new Object[] {
               T004F7_A155Servico_Codigo
               }
               , new Object[] {
               T004F8_A1767ServicoArtefato_ArtefatoDsc, T004F8_n1767ServicoArtefato_ArtefatoDsc
               }
               , new Object[] {
               T004F9_A155Servico_Codigo, T004F9_A1766ServicoArtefato_ArtefatoCod
               }
               , new Object[] {
               T004F10_A155Servico_Codigo, T004F10_A1766ServicoArtefato_ArtefatoCod
               }
               , new Object[] {
               T004F11_A155Servico_Codigo, T004F11_A1766ServicoArtefato_ArtefatoCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004F14_A1767ServicoArtefato_ArtefatoDsc, T004F14_n1767ServicoArtefato_ArtefatoDsc
               }
               , new Object[] {
               T004F15_A155Servico_Codigo, T004F15_A1766ServicoArtefato_ArtefatoCod
               }
               , new Object[] {
               T004F16_A1766ServicoArtefato_ArtefatoCod, T004F16_A1767ServicoArtefato_ArtefatoDsc, T004F16_n1767ServicoArtefato_ArtefatoDsc
               }
               , new Object[] {
               T004F17_A1767ServicoArtefato_ArtefatoDsc, T004F17_n1767ServicoArtefato_ArtefatoDsc
               }
               , new Object[] {
               T004F18_A155Servico_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound195 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV8Servico_Codigo ;
      private int wcpOAV9ServicoArtefato_ArtefatoCod ;
      private int Z155Servico_Codigo ;
      private int Z1766ServicoArtefato_ArtefatoCod ;
      private int A155Servico_Codigo ;
      private int A1766ServicoArtefato_ArtefatoCod ;
      private int AV8Servico_Codigo ;
      private int AV9ServicoArtefato_ArtefatoCod ;
      private int trnEnded ;
      private int edtServico_Codigo_Visible ;
      private int edtServico_Codigo_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynServicoArtefato_ArtefatoCod_Internalname ;
      private String TempTags ;
      private String edtServico_Codigo_Internalname ;
      private String edtServico_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockservicoartefato_artefatocod_Internalname ;
      private String lblTextblockservicoartefato_artefatocod_Jsonclick ;
      private String dynServicoArtefato_ArtefatoCod_Jsonclick ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode195 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1767ServicoArtefato_ArtefatoDsc ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String A1767ServicoArtefato_ArtefatoDsc ;
      private String Z1767ServicoArtefato_ArtefatoDsc ;
      private IGxSession AV12WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynServicoArtefato_ArtefatoCod ;
      private IDataStoreProvider pr_default ;
      private String[] T004F5_A1767ServicoArtefato_ArtefatoDsc ;
      private bool[] T004F5_n1767ServicoArtefato_ArtefatoDsc ;
      private String[] T004F6_A1767ServicoArtefato_ArtefatoDsc ;
      private bool[] T004F6_n1767ServicoArtefato_ArtefatoDsc ;
      private int[] T004F6_A155Servico_Codigo ;
      private int[] T004F6_A1766ServicoArtefato_ArtefatoCod ;
      private int[] T004F4_A155Servico_Codigo ;
      private int[] T004F7_A155Servico_Codigo ;
      private String[] T004F8_A1767ServicoArtefato_ArtefatoDsc ;
      private bool[] T004F8_n1767ServicoArtefato_ArtefatoDsc ;
      private int[] T004F9_A155Servico_Codigo ;
      private int[] T004F9_A1766ServicoArtefato_ArtefatoCod ;
      private int[] T004F3_A155Servico_Codigo ;
      private int[] T004F3_A1766ServicoArtefato_ArtefatoCod ;
      private int[] T004F10_A155Servico_Codigo ;
      private int[] T004F10_A1766ServicoArtefato_ArtefatoCod ;
      private int[] T004F11_A155Servico_Codigo ;
      private int[] T004F11_A1766ServicoArtefato_ArtefatoCod ;
      private int[] T004F2_A155Servico_Codigo ;
      private int[] T004F2_A1766ServicoArtefato_ArtefatoCod ;
      private String[] T004F14_A1767ServicoArtefato_ArtefatoDsc ;
      private bool[] T004F14_n1767ServicoArtefato_ArtefatoDsc ;
      private int[] T004F15_A155Servico_Codigo ;
      private int[] T004F15_A1766ServicoArtefato_ArtefatoCod ;
      private int[] T004F16_A1766ServicoArtefato_ArtefatoCod ;
      private String[] T004F16_A1767ServicoArtefato_ArtefatoDsc ;
      private bool[] T004F16_n1767ServicoArtefato_ArtefatoDsc ;
      private String[] T004F17_A1767ServicoArtefato_ArtefatoDsc ;
      private bool[] T004F17_n1767ServicoArtefato_ArtefatoDsc ;
      private int[] T004F18_A155Servico_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV13WWPContext ;
   }

   public class servicoartefatos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004F6 ;
          prmT004F6 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004F4 ;
          prmT004F4 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004F5 ;
          prmT004F5 = new Object[] {
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004F7 ;
          prmT004F7 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004F8 ;
          prmT004F8 = new Object[] {
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004F9 ;
          prmT004F9 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004F3 ;
          prmT004F3 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004F10 ;
          prmT004F10 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004F11 ;
          prmT004F11 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004F2 ;
          prmT004F2 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004F12 ;
          prmT004F12 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004F13 ;
          prmT004F13 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004F14 ;
          prmT004F14 = new Object[] {
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004F15 ;
          prmT004F15 = new Object[] {
          } ;
          Object[] prmT004F16 ;
          prmT004F16 = new Object[] {
          } ;
          Object[] prmT004F17 ;
          prmT004F17 = new Object[] {
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004F18 ;
          prmT004F18 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T004F2", "SELECT [Servico_Codigo], [ServicoArtefato_ArtefatoCod] AS ServicoArtefato_ArtefatoCod FROM [ServicoArtefatos] WITH (UPDLOCK) WHERE [Servico_Codigo] = @Servico_Codigo AND [ServicoArtefato_ArtefatoCod] = @ServicoArtefato_ArtefatoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004F2,1,0,true,false )
             ,new CursorDef("T004F3", "SELECT [Servico_Codigo], [ServicoArtefato_ArtefatoCod] AS ServicoArtefato_ArtefatoCod FROM [ServicoArtefatos] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo AND [ServicoArtefato_ArtefatoCod] = @ServicoArtefato_ArtefatoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004F3,1,0,true,false )
             ,new CursorDef("T004F4", "SELECT [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004F4,1,0,true,false )
             ,new CursorDef("T004F5", "SELECT [Artefatos_Descricao] AS ServicoArtefato_ArtefatoDsc FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @ServicoArtefato_ArtefatoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004F5,1,0,true,false )
             ,new CursorDef("T004F6", "SELECT T2.[Artefatos_Descricao] AS ServicoArtefato_ArtefatoDsc, TM1.[Servico_Codigo], TM1.[ServicoArtefato_ArtefatoCod] AS ServicoArtefato_ArtefatoCod FROM ([ServicoArtefatos] TM1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = TM1.[ServicoArtefato_ArtefatoCod]) WHERE TM1.[Servico_Codigo] = @Servico_Codigo and TM1.[ServicoArtefato_ArtefatoCod] = @ServicoArtefato_ArtefatoCod ORDER BY TM1.[Servico_Codigo], TM1.[ServicoArtefato_ArtefatoCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004F6,100,0,true,false )
             ,new CursorDef("T004F7", "SELECT [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004F7,1,0,true,false )
             ,new CursorDef("T004F8", "SELECT [Artefatos_Descricao] AS ServicoArtefato_ArtefatoDsc FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @ServicoArtefato_ArtefatoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004F8,1,0,true,false )
             ,new CursorDef("T004F9", "SELECT [Servico_Codigo], [ServicoArtefato_ArtefatoCod] AS ServicoArtefato_ArtefatoCod FROM [ServicoArtefatos] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo AND [ServicoArtefato_ArtefatoCod] = @ServicoArtefato_ArtefatoCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004F9,1,0,true,false )
             ,new CursorDef("T004F10", "SELECT TOP 1 [Servico_Codigo], [ServicoArtefato_ArtefatoCod] AS ServicoArtefato_ArtefatoCod FROM [ServicoArtefatos] WITH (NOLOCK) WHERE ( [Servico_Codigo] > @Servico_Codigo or [Servico_Codigo] = @Servico_Codigo and [ServicoArtefato_ArtefatoCod] > @ServicoArtefato_ArtefatoCod) ORDER BY [Servico_Codigo], [ServicoArtefato_ArtefatoCod]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004F10,1,0,true,true )
             ,new CursorDef("T004F11", "SELECT TOP 1 [Servico_Codigo], [ServicoArtefato_ArtefatoCod] AS ServicoArtefato_ArtefatoCod FROM [ServicoArtefatos] WITH (NOLOCK) WHERE ( [Servico_Codigo] < @Servico_Codigo or [Servico_Codigo] = @Servico_Codigo and [ServicoArtefato_ArtefatoCod] < @ServicoArtefato_ArtefatoCod) ORDER BY [Servico_Codigo] DESC, [ServicoArtefato_ArtefatoCod] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004F11,1,0,true,true )
             ,new CursorDef("T004F12", "INSERT INTO [ServicoArtefatos]([Servico_Codigo], [ServicoArtefato_ArtefatoCod]) VALUES(@Servico_Codigo, @ServicoArtefato_ArtefatoCod)", GxErrorMask.GX_NOMASK,prmT004F12)
             ,new CursorDef("T004F13", "DELETE FROM [ServicoArtefatos]  WHERE [Servico_Codigo] = @Servico_Codigo AND [ServicoArtefato_ArtefatoCod] = @ServicoArtefato_ArtefatoCod", GxErrorMask.GX_NOMASK,prmT004F13)
             ,new CursorDef("T004F14", "SELECT [Artefatos_Descricao] AS ServicoArtefato_ArtefatoDsc FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @ServicoArtefato_ArtefatoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004F14,1,0,true,false )
             ,new CursorDef("T004F15", "SELECT [Servico_Codigo], [ServicoArtefato_ArtefatoCod] AS ServicoArtefato_ArtefatoCod FROM [ServicoArtefatos] WITH (NOLOCK) ORDER BY [Servico_Codigo], [ServicoArtefato_ArtefatoCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004F15,100,0,true,false )
             ,new CursorDef("T004F16", "SELECT [Artefatos_Codigo] AS ServicoArtefato_ArtefatoCod, [Artefatos_Descricao] AS ServicoArtefato_ArtefatoDsc FROM [Artefatos] WITH (NOLOCK) ORDER BY [Artefatos_Descricao] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004F16,0,0,true,false )
             ,new CursorDef("T004F17", "SELECT [Artefatos_Descricao] AS ServicoArtefato_ArtefatoDsc FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @ServicoArtefato_ArtefatoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004F17,1,0,true,false )
             ,new CursorDef("T004F18", "SELECT [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004F18,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
