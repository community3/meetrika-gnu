/*
               File: GAMExampleRegisterUser
        Description: Register user
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:32:21.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamexampleregisteruser : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public gamexampleregisteruser( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("GeneXusXEv2");
      }

      public gamexampleregisteruser( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavGender = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA2K2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS2K2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE2K2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Register user ") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117322592");
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("gamexampleregisteruser.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm2K2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
      }

      public override String GetPgmname( )
      {
         return "GAMExampleRegisterUser" ;
      }

      public override String GetPgmdesc( )
      {
         return "Register user " ;
      }

      protected void WB2K0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbformtitle_Internalname, "User registration", "", "", lblTbformtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_GAMExampleRegisterUser.htm");
            wb_table1_3_2K2( true) ;
         }
         else
         {
            wb_table1_3_2K2( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_2K2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START2K2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Register user ", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP2K0( ) ;
      }

      protected void WS2K2( )
      {
         START2K2( ) ;
         EVT2K2( ) ;
      }

      protected void EVT2K2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E112K2 */
                           E112K2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E122K2 */
                                 E122K2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E132K2 */
                           E132K2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE2K2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm2K2( ) ;
            }
         }
      }

      protected void PA2K2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            cmbavGender.Name = "vGENDER";
            cmbavGender.WebTags = "";
            cmbavGender.addItem("N", "Not Specified", 0);
            cmbavGender.addItem("F", "Female", 0);
            cmbavGender.addItem("M", "Male", 0);
            if ( cmbavGender.ItemCount > 0 )
            {
               AV11Gender = cmbavGender.getValidValue(AV11Gender);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Gender", AV11Gender);
            }
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavName_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavGender.ItemCount > 0 )
         {
            AV11Gender = cmbavGender.getValidValue(AV11Gender);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Gender", AV11Gender);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF2K2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF2K2( )
      {
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E132K2 */
            E132K2 ();
            WB2K0( ) ;
         }
      }

      protected void STRUP2K0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112K2 */
         E112K2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV27ReqName = cgiGet( imgavReqname_Internalname);
            AV17Name = cgiGet( edtavName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Name", AV17Name);
            AV22ReqEmail = cgiGet( imgavReqemail_Internalname);
            AV7EMail = cgiGet( edtavEmail_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7EMail", AV7EMail);
            AV23ReqFirstName = cgiGet( imgavReqfirstname_Internalname);
            AV10FirstName = cgiGet( edtavFirstname_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10FirstName", AV10FirstName);
            AV26ReqLastName = cgiGet( imgavReqlastname_Internalname);
            AV13LastName = cgiGet( edtavLastname_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13LastName", AV13LastName);
            AV21ReqBirthday = cgiGet( imgavReqbirthday_Internalname);
            if ( context.localUtil.VCDate( cgiGet( edtavBirthday_Internalname), 2) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Birthday"}), 1, "vBIRTHDAY");
               GX_FocusControl = edtavBirthday_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV6Birthday = DateTime.MinValue;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Birthday", context.localUtil.Format(AV6Birthday, "99/99/9999"));
            }
            else
            {
               AV6Birthday = context.localUtil.CToD( cgiGet( edtavBirthday_Internalname), 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Birthday", context.localUtil.Format(AV6Birthday, "99/99/9999"));
            }
            AV24ReqGender = cgiGet( imgavReqgender_Internalname);
            cmbavGender.CurrentValue = cgiGet( cmbavGender_Internalname);
            AV11Gender = cgiGet( cmbavGender_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Gender", AV11Gender);
            AV28ReqPassword = cgiGet( imgavReqpassword_Internalname);
            AV18Password = cgiGet( edtavPassword_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Password", AV18Password);
            AV28ReqPassword = cgiGet( imgavReqpassword_Internalname);
            AV19PasswordConf = cgiGet( edtavPasswordconf_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19PasswordConf", AV19PasswordConf);
            AV25ReqIcon = cgiGet( imgavReqicon_Internalname);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E112K2 */
         E112K2 ();
         if (returnInSub) return;
      }

      protected void E112K2( )
      {
         /* Start Routine */
         AV20Repository = new SdtGAMRepository(context).get();
         AV27ReqName = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV27ReqName)) ? AV33Reqname_GXI : context.convertURL( context.PathToRelativeUrl( AV27ReqName))));
         AV33Reqname_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV27ReqName)) ? AV33Reqname_GXI : context.convertURL( context.PathToRelativeUrl( AV27ReqName))));
         if ( AV20Repository.gxTpr_Requiredemail )
         {
            AV22ReqEmail = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqemail_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV22ReqEmail)) ? AV34Reqemail_GXI : context.convertURL( context.PathToRelativeUrl( AV22ReqEmail))));
            AV34Reqemail_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqemail_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV22ReqEmail)) ? AV34Reqemail_GXI : context.convertURL( context.PathToRelativeUrl( AV22ReqEmail))));
         }
         else
         {
            imgavReqemail_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqemail_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqemail_Visible), 5, 0)));
         }
         if ( AV20Repository.gxTpr_Requiredfirstname )
         {
            AV23ReqFirstName = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqfirstname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV23ReqFirstName)) ? AV35Reqfirstname_GXI : context.convertURL( context.PathToRelativeUrl( AV23ReqFirstName))));
            AV35Reqfirstname_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqfirstname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV23ReqFirstName)) ? AV35Reqfirstname_GXI : context.convertURL( context.PathToRelativeUrl( AV23ReqFirstName))));
         }
         else
         {
            imgavReqfirstname_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqfirstname_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqfirstname_Visible), 5, 0)));
         }
         if ( AV20Repository.gxTpr_Requiredlastname )
         {
            AV26ReqLastName = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqlastname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV26ReqLastName)) ? AV36Reqlastname_GXI : context.convertURL( context.PathToRelativeUrl( AV26ReqLastName))));
            AV36Reqlastname_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqlastname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV26ReqLastName)) ? AV36Reqlastname_GXI : context.convertURL( context.PathToRelativeUrl( AV26ReqLastName))));
         }
         else
         {
            imgavReqlastname_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqlastname_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqlastname_Visible), 5, 0)));
         }
         if ( AV20Repository.gxTpr_Requiredbirthday )
         {
            AV21ReqBirthday = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqbirthday_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV21ReqBirthday)) ? AV37Reqbirthday_GXI : context.convertURL( context.PathToRelativeUrl( AV21ReqBirthday))));
            AV37Reqbirthday_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqbirthday_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV21ReqBirthday)) ? AV37Reqbirthday_GXI : context.convertURL( context.PathToRelativeUrl( AV21ReqBirthday))));
         }
         else
         {
            imgavReqbirthday_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqbirthday_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqbirthday_Visible), 5, 0)));
         }
         if ( AV20Repository.gxTpr_Requiredgender )
         {
            AV24ReqGender = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqgender_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV24ReqGender)) ? AV38Reqgender_GXI : context.convertURL( context.PathToRelativeUrl( AV24ReqGender))));
            AV38Reqgender_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqgender_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV24ReqGender)) ? AV38Reqgender_GXI : context.convertURL( context.PathToRelativeUrl( AV24ReqGender))));
         }
         else
         {
            imgavReqgender_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqgender_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqgender_Visible), 5, 0)));
         }
         if ( AV20Repository.gxTpr_Requiredpassword )
         {
            AV28ReqPassword = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqpassword_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28ReqPassword)) ? AV39Reqpassword_GXI : context.convertURL( context.PathToRelativeUrl( AV28ReqPassword))));
            AV39Reqpassword_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqpassword_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28ReqPassword)) ? AV39Reqpassword_GXI : context.convertURL( context.PathToRelativeUrl( AV28ReqPassword))));
         }
         else
         {
            imgavReqpassword_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqpassword_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqpassword_Visible), 5, 0)));
         }
         AV25ReqIcon = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqicon_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV25ReqIcon)) ? AV40Reqicon_GXI : context.convertURL( context.PathToRelativeUrl( AV25ReqIcon))));
         AV40Reqicon_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqicon_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV25ReqIcon)) ? AV40Reqicon_GXI : context.convertURL( context.PathToRelativeUrl( AV25ReqIcon))));
         lblTbinficonreq_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbinficonreq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbinficonreq_Visible), 5, 0)));
      }

      public void GXEnter( )
      {
         /* Execute user event: E122K2 */
         E122K2 ();
         if (returnInSub) return;
      }

      protected void E122K2( )
      {
         /* Enter Routine */
         if ( StringUtil.StrCmp(AV18Password, AV19PasswordConf) == 0 )
         {
            AV29User.gxTpr_Name = AV17Name;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29User", AV29User);
            AV29User.gxTpr_Email = AV7EMail;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29User", AV29User);
            AV29User.gxTpr_Firstname = AV10FirstName;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29User", AV29User);
            AV29User.gxTpr_Lastname = AV13LastName;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29User", AV29User);
            AV29User.gxTpr_Birthday = AV6Birthday;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29User", AV29User);
            AV29User.gxTpr_Gender = AV11Gender;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29User", AV29User);
            AV29User.gxTpr_Password = AV18Password;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29User", AV29User);
            AV29User.save();
            if ( AV29User.success() )
            {
               context.CommitDataStores( "GAMExampleRegisterUser");
               if ( StringUtil.StrCmp(AV20Repository.gxTpr_Useractivationmethod, "A") == 0 )
               {
                  AV5AdditionalParameter.gxTpr_Rememberusertype = AV30UserRememberMe;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AdditionalParameter", AV5AdditionalParameter);
                  AV14LoginOK = new SdtGAMRepository(context).login(AV17Name, AV18Password, AV5AdditionalParameter, out  AV9Errors);
                  if ( AV14LoginOK )
                  {
                     context.wjLoc = formatLink("gamhome.aspx") ;
                     context.wjLocDisableFrm = 1;
                  }
                  else
                  {
                     /* Execute user subroutine: 'DISPLAYMESSAGES' */
                     S112 ();
                     if (returnInSub) return;
                  }
               }
               else
               {
                  new gamcheckuseractivationmethod(context ).execute(  AV29User.gxTpr_Guid, out  AV16Messages) ;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29User", AV29User);
                  AV41GXV1 = 1;
                  while ( AV41GXV1 <= AV16Messages.Count )
                  {
                     AV15Message = ((SdtMessages_Message)AV16Messages.Item(AV41GXV1));
                     GX_msglist.addItem(AV15Message.gxTpr_Description);
                     AV41GXV1 = (int)(AV41GXV1+1);
                  }
               }
            }
            else
            {
               AV9Errors = AV29User.geterrors();
               /* Execute user subroutine: 'DISPLAYMESSAGES' */
               S112 ();
               if (returnInSub) return;
            }
         }
         else
         {
            GX_msglist.addItem("The password and confirmation password do not match.");
         }
      }

      protected void S112( )
      {
         /* 'DISPLAYMESSAGES' Routine */
         AV42GXV2 = 1;
         while ( AV42GXV2 <= AV9Errors.Count )
         {
            AV8Error = ((SdtGAMError)AV9Errors.Item(AV42GXV2));
            GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV8Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
            AV42GXV2 = (int)(AV42GXV2+1);
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E132K2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_3_2K2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:10px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqname_Internalname, AV27ReqName);
            ClassString = "Image";
            StyleString = "";
            AV27ReqName_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV27ReqName))&&String.IsNullOrEmpty(StringUtil.RTrim( AV33Reqname_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV27ReqName)));
            GxWebStd.gx_bitmap( context, imgavReqname_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV27ReqName)) ? AV33Reqname_GXI : context.PathToRelativeUrl( AV27ReqName)), "", "", "", context.GetTheme( ), 1, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV27ReqName_IsBlob, false, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbname2_Internalname, "User Name", "", "", lblTbname2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavName_Internalname, AV17Name, StringUtil.RTrim( context.localUtil.Format( AV17Name, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavName_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GAMUserIdentification", "left", true, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\">") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqemail_Internalname, AV22ReqEmail);
            ClassString = "Image";
            StyleString = "";
            AV22ReqEmail_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV22ReqEmail))&&String.IsNullOrEmpty(StringUtil.RTrim( AV34Reqemail_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV22ReqEmail)));
            GxWebStd.gx_bitmap( context, imgavReqemail_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV22ReqEmail)) ? AV34Reqemail_GXI : context.PathToRelativeUrl( AV22ReqEmail)), "", "", "", context.GetTheme( ), imgavReqemail_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV22ReqEmail_IsBlob, false, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbemail_Internalname, "Email", "", "", lblTbemail_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEmail_Internalname, AV7EMail, StringUtil.RTrim( context.localUtil.Format( AV7EMail, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEmail_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GAMEMail", "left", true, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\">") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqfirstname_Internalname, AV23ReqFirstName);
            ClassString = "Image";
            StyleString = "";
            AV23ReqFirstName_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV23ReqFirstName))&&String.IsNullOrEmpty(StringUtil.RTrim( AV35Reqfirstname_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV23ReqFirstName)));
            GxWebStd.gx_bitmap( context, imgavReqfirstname_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV23ReqFirstName)) ? AV35Reqfirstname_GXI : context.PathToRelativeUrl( AV23ReqFirstName)), "", "", "", context.GetTheme( ), imgavReqfirstname_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV23ReqFirstName_IsBlob, false, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbfirstname_Internalname, "First Name", "", "", lblTbfirstname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFirstname_Internalname, StringUtil.RTrim( AV10FirstName), StringUtil.RTrim( context.localUtil.Format( AV10FirstName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFirstname_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionShort", "left", true, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqlastname_Internalname, AV26ReqLastName);
            ClassString = "Image";
            StyleString = "";
            AV26ReqLastName_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV26ReqLastName))&&String.IsNullOrEmpty(StringUtil.RTrim( AV36Reqlastname_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV26ReqLastName)));
            GxWebStd.gx_bitmap( context, imgavReqlastname_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV26ReqLastName)) ? AV36Reqlastname_GXI : context.PathToRelativeUrl( AV26ReqLastName)), "", "", "", context.GetTheme( ), imgavReqlastname_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV26ReqLastName_IsBlob, false, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTblastname_Internalname, "Last Name", "", "", lblTblastname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLastname_Internalname, StringUtil.RTrim( AV13LastName), StringUtil.RTrim( context.localUtil.Format( AV13LastName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLastname_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionShort", "left", true, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqbirthday_Internalname, AV21ReqBirthday);
            ClassString = "Image";
            StyleString = "";
            AV21ReqBirthday_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV21ReqBirthday))&&String.IsNullOrEmpty(StringUtil.RTrim( AV37Reqbirthday_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV21ReqBirthday)));
            GxWebStd.gx_bitmap( context, imgavReqbirthday_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV21ReqBirthday)) ? AV37Reqbirthday_GXI : context.PathToRelativeUrl( AV21ReqBirthday)), "", "", "", context.GetTheme( ), imgavReqbirthday_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV21ReqBirthday_IsBlob, false, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbbirthday_Internalname, "Birthday", "", "", lblTbbirthday_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavBirthday_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavBirthday_Internalname, context.localUtil.Format(AV6Birthday, "99/99/9999"), context.localUtil.Format( AV6Birthday, "99/99/9999"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 10,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBirthday_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "GAMDate", "right", false, "HLP_GAMExampleRegisterUser.htm");
            GxWebStd.gx_bitmap( context, edtavBirthday_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqgender_Internalname, AV24ReqGender);
            ClassString = "Image";
            StyleString = "";
            AV24ReqGender_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV24ReqGender))&&String.IsNullOrEmpty(StringUtil.RTrim( AV38Reqgender_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV24ReqGender)));
            GxWebStd.gx_bitmap( context, imgavReqgender_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV24ReqGender)) ? AV38Reqgender_GXI : context.PathToRelativeUrl( AV24ReqGender)), "", "", "", context.GetTheme( ), imgavReqgender_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV24ReqGender_IsBlob, false, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbgender_Internalname, "Gender", "", "", lblTbgender_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:9px")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavGender, cmbavGender_Internalname, StringUtil.RTrim( AV11Gender), 1, cmbavGender_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_GAMExampleRegisterUser.htm");
            cmbavGender.CurrentValue = StringUtil.RTrim( AV11Gender);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGender_Internalname, "Values", (String)(cmbavGender.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:8px")+"\">") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqpassword_Internalname, AV28ReqPassword);
            ClassString = "Image";
            StyleString = "";
            AV28ReqPassword_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28ReqPassword))&&String.IsNullOrEmpty(StringUtil.RTrim( AV39Reqpassword_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28ReqPassword)));
            GxWebStd.gx_bitmap( context, imgavReqpassword_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV28ReqPassword)) ? AV39Reqpassword_GXI : context.PathToRelativeUrl( AV28ReqPassword)), "", "", "", context.GetTheme( ), imgavReqpassword_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV28ReqPassword_IsBlob, false, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbpwd_Internalname, "Password", "", "", lblTbpwd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPassword_Internalname, StringUtil.RTrim( AV18Password), StringUtil.RTrim( context.localUtil.Format( AV18Password, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPassword_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, -1, 0, 0, 1, 0, 0, true, "GAMPassword", "left", true, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqpassword_Internalname, AV28ReqPassword);
            ClassString = "Image";
            StyleString = "";
            AV28ReqPassword_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28ReqPassword))&&String.IsNullOrEmpty(StringUtil.RTrim( AV39Reqpassword_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28ReqPassword)));
            GxWebStd.gx_bitmap( context, imgavReqpassword_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV28ReqPassword)) ? AV39Reqpassword_GXI : context.PathToRelativeUrl( AV28ReqPassword)), "", "", "", context.GetTheme( ), imgavReqpassword_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV28ReqPassword_IsBlob, false, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbpwdconf_Internalname, "Password confirmation", "", "", lblTbpwdconf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPasswordconf_Internalname, StringUtil.RTrim( AV19PasswordConf), StringUtil.RTrim( context.localUtil.Format( AV19PasswordConf, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPasswordconf_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, -1, 0, 0, 1, 0, 0, true, "GAMPassword", "left", true, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqicon_Internalname, AV25ReqIcon);
            ClassString = "Image";
            StyleString = "";
            AV25ReqIcon_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV25ReqIcon))&&String.IsNullOrEmpty(StringUtil.RTrim( AV40Reqicon_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV25ReqIcon)));
            GxWebStd.gx_bitmap( context, imgavReqicon_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV25ReqIcon)) ? AV40Reqicon_GXI : context.PathToRelativeUrl( AV25ReqIcon)), "", "", "", context.GetTheme( ), 1, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV25ReqIcon_IsBlob, false, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbinficonreq_Internalname, "Information required.", "", "", lblTbinficonreq_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Microsoft Sans Serif'; font-size:8.0pt; font-weight:normal; font-style:normal;", "Label", 0, "", lblTbinficonreq_Visible, 1, 0, "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_83_2K2( true) ;
         }
         else
         {
            wb_table2_83_2K2( false) ;
         }
         return  ;
      }

      protected void wb_table2_83_2K2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_2K2e( true) ;
         }
         else
         {
            wb_table1_3_2K2e( false) ;
         }
      }

      protected void wb_table2_83_2K2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblbuttons_Internalname, tblTblbuttons_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirm_Internalname, "", "Confirmar", bttBtnconfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:2px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnclose_Internalname, "", "Fechar", bttBtnclose_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_83_2K2e( true) ;
         }
         else
         {
            wb_table2_83_2K2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA2K2( ) ;
         WS2K2( ) ;
         WE2K2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176033");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117322779");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gamexampleregisteruser.js", "?20203117322780");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbformtitle_Internalname = "TBFORMTITLE";
         imgavReqname_Internalname = "vREQNAME";
         lblTbname2_Internalname = "TBNAME2";
         edtavName_Internalname = "vNAME";
         imgavReqemail_Internalname = "vREQEMAIL";
         lblTbemail_Internalname = "TBEMAIL";
         edtavEmail_Internalname = "vEMAIL";
         imgavReqfirstname_Internalname = "vREQFIRSTNAME";
         lblTbfirstname_Internalname = "TBFIRSTNAME";
         edtavFirstname_Internalname = "vFIRSTNAME";
         imgavReqlastname_Internalname = "vREQLASTNAME";
         lblTblastname_Internalname = "TBLASTNAME";
         edtavLastname_Internalname = "vLASTNAME";
         imgavReqbirthday_Internalname = "vREQBIRTHDAY";
         lblTbbirthday_Internalname = "TBBIRTHDAY";
         edtavBirthday_Internalname = "vBIRTHDAY";
         imgavReqgender_Internalname = "vREQGENDER";
         lblTbgender_Internalname = "TBGENDER";
         cmbavGender_Internalname = "vGENDER";
         imgavReqpassword_Internalname = "vREQPASSWORD";
         lblTbpwd_Internalname = "TBPWD";
         edtavPassword_Internalname = "vPASSWORD";
         imgavReqpassword_Internalname = "vREQPASSWORD";
         lblTbpwdconf_Internalname = "TBPWDCONF";
         edtavPasswordconf_Internalname = "vPASSWORDCONF";
         imgavReqicon_Internalname = "vREQICON";
         lblTbinficonreq_Internalname = "TBINFICONREQ";
         bttBtnconfirm_Internalname = "BTNCONFIRM";
         bttBtnclose_Internalname = "BTNCLOSE";
         tblTblbuttons_Internalname = "TBLBUTTONS";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         lblTbinficonreq_Visible = 1;
         edtavPasswordconf_Jsonclick = "";
         edtavPassword_Jsonclick = "";
         cmbavGender_Jsonclick = "";
         edtavBirthday_Jsonclick = "";
         edtavLastname_Jsonclick = "";
         edtavFirstname_Jsonclick = "";
         edtavEmail_Jsonclick = "";
         edtavName_Jsonclick = "";
         imgavReqpassword_Visible = 1;
         imgavReqgender_Visible = 1;
         imgavReqbirthday_Visible = 1;
         imgavReqlastname_Visible = 1;
         imgavReqfirstname_Visible = 1;
         imgavReqemail_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         lblTbformtitle_Jsonclick = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV11Gender = "";
         AV27ReqName = "";
         AV17Name = "";
         AV22ReqEmail = "";
         AV7EMail = "";
         AV23ReqFirstName = "";
         AV10FirstName = "";
         AV26ReqLastName = "";
         AV13LastName = "";
         AV21ReqBirthday = "";
         AV6Birthday = DateTime.MinValue;
         AV24ReqGender = "";
         AV28ReqPassword = "";
         AV18Password = "";
         AV19PasswordConf = "";
         AV25ReqIcon = "";
         AV20Repository = new SdtGAMRepository(context);
         AV33Reqname_GXI = "";
         AV34Reqemail_GXI = "";
         AV35Reqfirstname_GXI = "";
         AV36Reqlastname_GXI = "";
         AV37Reqbirthday_GXI = "";
         AV38Reqgender_GXI = "";
         AV39Reqpassword_GXI = "";
         AV40Reqicon_GXI = "";
         AV29User = new SdtGAMUser(context);
         AV5AdditionalParameter = new SdtGAMLoginAdditionalParameters(context);
         AV9Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV16Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV15Message = new SdtMessages_Message(context);
         AV8Error = new SdtGAMError(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTbname2_Jsonclick = "";
         TempTags = "";
         lblTbemail_Jsonclick = "";
         lblTbfirstname_Jsonclick = "";
         lblTblastname_Jsonclick = "";
         lblTbbirthday_Jsonclick = "";
         lblTbgender_Jsonclick = "";
         lblTbpwd_Jsonclick = "";
         lblTbpwdconf_Jsonclick = "";
         lblTbinficonreq_Jsonclick = "";
         bttBtnconfirm_Jsonclick = "";
         bttBtnclose_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gamexampleregisteruser__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV30UserRememberMe ;
      private short nGXWrapped ;
      private int imgavReqemail_Visible ;
      private int imgavReqfirstname_Visible ;
      private int imgavReqlastname_Visible ;
      private int imgavReqbirthday_Visible ;
      private int imgavReqgender_Visible ;
      private int imgavReqpassword_Visible ;
      private int lblTbinficonreq_Visible ;
      private int AV41GXV1 ;
      private int AV42GXV2 ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String lblTbformtitle_Internalname ;
      private String lblTbformtitle_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV11Gender ;
      private String edtavName_Internalname ;
      private String imgavReqname_Internalname ;
      private String imgavReqemail_Internalname ;
      private String edtavEmail_Internalname ;
      private String imgavReqfirstname_Internalname ;
      private String AV10FirstName ;
      private String edtavFirstname_Internalname ;
      private String imgavReqlastname_Internalname ;
      private String AV13LastName ;
      private String edtavLastname_Internalname ;
      private String imgavReqbirthday_Internalname ;
      private String edtavBirthday_Internalname ;
      private String imgavReqgender_Internalname ;
      private String cmbavGender_Internalname ;
      private String imgavReqpassword_Internalname ;
      private String AV18Password ;
      private String edtavPassword_Internalname ;
      private String AV19PasswordConf ;
      private String edtavPasswordconf_Internalname ;
      private String imgavReqicon_Internalname ;
      private String lblTbinficonreq_Internalname ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTbname2_Internalname ;
      private String lblTbname2_Jsonclick ;
      private String TempTags ;
      private String edtavName_Jsonclick ;
      private String lblTbemail_Internalname ;
      private String lblTbemail_Jsonclick ;
      private String edtavEmail_Jsonclick ;
      private String lblTbfirstname_Internalname ;
      private String lblTbfirstname_Jsonclick ;
      private String edtavFirstname_Jsonclick ;
      private String lblTblastname_Internalname ;
      private String lblTblastname_Jsonclick ;
      private String edtavLastname_Jsonclick ;
      private String lblTbbirthday_Internalname ;
      private String lblTbbirthday_Jsonclick ;
      private String edtavBirthday_Jsonclick ;
      private String lblTbgender_Internalname ;
      private String lblTbgender_Jsonclick ;
      private String cmbavGender_Jsonclick ;
      private String lblTbpwd_Internalname ;
      private String lblTbpwd_Jsonclick ;
      private String edtavPassword_Jsonclick ;
      private String lblTbpwdconf_Internalname ;
      private String lblTbpwdconf_Jsonclick ;
      private String edtavPasswordconf_Jsonclick ;
      private String lblTbinficonreq_Jsonclick ;
      private String tblTblbuttons_Internalname ;
      private String bttBtnconfirm_Internalname ;
      private String bttBtnconfirm_Jsonclick ;
      private String bttBtnclose_Internalname ;
      private String bttBtnclose_Jsonclick ;
      private DateTime AV6Birthday ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool AV14LoginOK ;
      private bool AV27ReqName_IsBlob ;
      private bool AV22ReqEmail_IsBlob ;
      private bool AV23ReqFirstName_IsBlob ;
      private bool AV26ReqLastName_IsBlob ;
      private bool AV21ReqBirthday_IsBlob ;
      private bool AV24ReqGender_IsBlob ;
      private bool AV28ReqPassword_IsBlob ;
      private bool AV25ReqIcon_IsBlob ;
      private String AV17Name ;
      private String AV7EMail ;
      private String AV33Reqname_GXI ;
      private String AV34Reqemail_GXI ;
      private String AV35Reqfirstname_GXI ;
      private String AV36Reqlastname_GXI ;
      private String AV37Reqbirthday_GXI ;
      private String AV38Reqgender_GXI ;
      private String AV39Reqpassword_GXI ;
      private String AV40Reqicon_GXI ;
      private String AV27ReqName ;
      private String AV22ReqEmail ;
      private String AV23ReqFirstName ;
      private String AV26ReqLastName ;
      private String AV21ReqBirthday ;
      private String AV24ReqGender ;
      private String AV28ReqPassword ;
      private String AV25ReqIcon ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavGender ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_default ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV9Errors ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV16Messages ;
      private SdtGAMLoginAdditionalParameters AV5AdditionalParameter ;
      private SdtGAMError AV8Error ;
      private SdtMessages_Message AV15Message ;
      private SdtGAMRepository AV20Repository ;
      private SdtGAMUser AV29User ;
   }

   public class gamexampleregisteruser__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
