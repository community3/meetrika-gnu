/*
               File: PRC_SistemasFromExcel
        Description: Sistemas e M�dulos From Excel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:17:43.7
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aprc_sistemasfromexcel : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV12FileName = gxfirstwebparm;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV49FileNameReal = GetNextPar( );
                  AV32Aba = GetNextPar( );
                  AV34PraLinea = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV35ColSisNom = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV36ColSisSgl = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV40ColModNom = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV46ColModSglOpc = (short)(NumberUtil.Val( GetNextPar( ), "."));
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aprc_sistemasfromexcel( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aprc_sistemasfromexcel( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_FileName ,
                           String aP1_FileNameReal ,
                           String aP2_Aba ,
                           ref short aP3_PraLinea ,
                           short aP4_ColSisNom ,
                           short aP5_ColSisSgl ,
                           short aP6_ColModNom ,
                           short aP7_ColModSglOpc )
      {
         this.AV12FileName = aP0_FileName;
         this.AV49FileNameReal = aP1_FileNameReal;
         this.AV32Aba = aP2_Aba;
         this.AV34PraLinea = aP3_PraLinea;
         this.AV35ColSisNom = aP4_ColSisNom;
         this.AV36ColSisSgl = aP5_ColSisSgl;
         this.AV40ColModNom = aP6_ColModNom;
         this.AV46ColModSglOpc = aP7_ColModSglOpc;
         initialize();
         executePrivate();
         aP3_PraLinea=this.AV34PraLinea;
      }

      public void executeSubmit( String aP0_FileName ,
                                 String aP1_FileNameReal ,
                                 String aP2_Aba ,
                                 ref short aP3_PraLinea ,
                                 short aP4_ColSisNom ,
                                 short aP5_ColSisSgl ,
                                 short aP6_ColModNom ,
                                 short aP7_ColModSglOpc )
      {
         aprc_sistemasfromexcel objaprc_sistemasfromexcel;
         objaprc_sistemasfromexcel = new aprc_sistemasfromexcel();
         objaprc_sistemasfromexcel.AV12FileName = aP0_FileName;
         objaprc_sistemasfromexcel.AV49FileNameReal = aP1_FileNameReal;
         objaprc_sistemasfromexcel.AV32Aba = aP2_Aba;
         objaprc_sistemasfromexcel.AV34PraLinea = aP3_PraLinea;
         objaprc_sistemasfromexcel.AV35ColSisNom = aP4_ColSisNom;
         objaprc_sistemasfromexcel.AV36ColSisSgl = aP5_ColSisSgl;
         objaprc_sistemasfromexcel.AV40ColModNom = aP6_ColModNom;
         objaprc_sistemasfromexcel.AV46ColModSglOpc = aP7_ColModSglOpc;
         objaprc_sistemasfromexcel.context.SetSubmitInitialConfig(context);
         objaprc_sistemasfromexcel.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaprc_sistemasfromexcel);
         aP3_PraLinea=this.AV34PraLinea;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aprc_sistemasfromexcel)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 4;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 2, 1, 12240, 15840, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*4));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV28WWPContext) ;
            AV30Hoje = DateTimeUtil.ServerNow( context, "DEFAULT");
            AV31NomeArq = "Arquivo: " + AV49FileNameReal;
            AV31NomeArq = AV31NomeArq + (String.IsNullOrEmpty(StringUtil.RTrim( AV32Aba)) ? "" : " ("+AV32Aba+")");
            AV29ServerNow = DateTimeUtil.ServerNow( context, "DEFAULT");
            if ( AV46ColModSglOpc == 0 )
            {
               AV39ColModSgl = AV40ColModNom;
            }
            else
            {
               AV39ColModSgl = AV46ColModSglOpc;
            }
            /* Execute user subroutine: 'OPENFILE' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            AV18Ln = AV34PraLinea;
            while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11ExcelDocument.get_Cells(AV18Ln, AV35ColSisNom, 1, 1).Text)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV11ExcelDocument.get_Cells(AV18Ln, AV36ColSisSgl, 1, 1).Text)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV11ExcelDocument.get_Cells(AV18Ln, AV40ColModNom, 1, 1).Text)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV11ExcelDocument.get_Cells(AV18Ln, AV39ColModSgl, 1, 1).Text)) )
            {
               AV47Celula = AV11ExcelDocument.get_Cells(AV18Ln, AV35ColSisNom, 1, 1).Text;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Celula)) && ( StringUtil.StrCmp(AV25Nome, AV47Celula) != 0 ) )
               {
                  AV25Nome = AV47Celula;
                  AV38String = AV47Celula;
                  /* Execute user subroutine: 'LIMPARNOME' */
                  S131 ();
                  if ( returnInSub )
                  {
                     this.cleanup();
                     if (true) return;
                  }
                  AV22Novo_Nome = AV38String;
                  if ( AV36ColSisSgl > 0 )
                  {
                     AV47Celula = AV11ExcelDocument.get_Cells(AV18Ln, AV36ColSisSgl, 1, 1).Text;
                     if ( String.IsNullOrEmpty(StringUtil.RTrim( AV47Celula)) )
                     {
                        AV24Sigla = StringUtil.Substring( AV48NovoNome, 1, 15);
                     }
                     else
                     {
                        AV38String = AV47Celula;
                        /* Execute user subroutine: 'LIMPARNOME' */
                        S131 ();
                        if ( returnInSub )
                        {
                           this.cleanup();
                           if (true) return;
                        }
                        AV24Sigla = AV38String;
                     }
                  }
                  else
                  {
                     AV24Sigla = StringUtil.Substring( AV48NovoNome, 1, 15);
                  }
                  /* Execute user subroutine: 'NEWSISTEMA' */
                  S111 ();
                  if ( returnInSub )
                  {
                     this.cleanup();
                     if (true) return;
                  }
               }
               if ( AV40ColModNom > 0 )
               {
                  AV47Celula = AV11ExcelDocument.get_Cells(AV18Ln, AV40ColModNom, 1, 1).Text;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Celula)) )
                  {
                     AV38String = AV47Celula;
                     /* Execute user subroutine: 'LIMPARNOME' */
                     S131 ();
                     if ( returnInSub )
                     {
                        this.cleanup();
                        if (true) return;
                     }
                     AV22Novo_Nome = AV38String;
                     if ( AV39ColModSgl > 0 )
                     {
                        AV47Celula = AV11ExcelDocument.get_Cells(AV18Ln, AV39ColModSgl, 1, 1).Text;
                        if ( String.IsNullOrEmpty(StringUtil.RTrim( AV47Celula)) )
                        {
                           AV41Modulo_Sigla = StringUtil.Substring( AV22Novo_Nome, 1, 15);
                        }
                        else
                        {
                           AV38String = AV47Celula;
                           /* Execute user subroutine: 'LIMPARNOME' */
                           S131 ();
                           if ( returnInSub )
                           {
                              this.cleanup();
                              if (true) return;
                           }
                           AV41Modulo_Sigla = StringUtil.Substring( AV38String, 1, 15);
                        }
                     }
                     else
                     {
                        AV41Modulo_Sigla = StringUtil.Substring( AV22Novo_Nome, 1, 15);
                     }
                     /* Execute user subroutine: 'NEWMODULO' */
                     S121 ();
                     if ( returnInSub )
                     {
                        this.cleanup();
                        if (true) return;
                     }
                  }
               }
               AV18Ln = (short)(AV18Ln+1);
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H2Y0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'NEWSISTEMA' Routine */
         AV53GXLvl91 = 0;
         AV54Udparg1 = new prc_padronizastring(context).executeUdp(  AV22Novo_Nome);
         /* Using cursor P002Y2 */
         pr_default.execute(0, new Object[] {AV28WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A135Sistema_AreaTrabalhoCod = P002Y2_A135Sistema_AreaTrabalhoCod[0];
            A416Sistema_Nome = P002Y2_A416Sistema_Nome[0];
            A127Sistema_Codigo = P002Y2_A127Sistema_Codigo[0];
            if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A416Sistema_Nome), AV54Udparg1) == 0 )
            {
               AV53GXLvl91 = 1;
               AV22Novo_Nome = AV22Novo_Nome + " (J� cadastrado)";
               AV26Sistema_Codigo = A127Sistema_Codigo;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV53GXLvl91 == 0 )
         {
            AV44Sistema = new SdtSistema(context);
            AV44Sistema.gxTpr_Sistema_areatrabalhocod = AV28WWPContext.gxTpr_Areatrabalho_codigo;
            AV44Sistema.gxTpr_Sistema_nome = AV22Novo_Nome;
            AV44Sistema.gxTpr_Sistema_sigla = AV24Sigla;
            AV44Sistema.gxTpr_Sistema_impdata = AV29ServerNow;
            AV44Sistema.gxTpr_Sistema_impusercod = AV28WWPContext.gxTpr_Userid;
            AV44Sistema.gxTv_SdtSistema_Ambientetecnologico_codigo_SetNull();
            AV44Sistema.gxTv_SdtSistema_Metodologia_codigo_SetNull();
            AV44Sistema.gxTv_SdtSistema_Sistema_projetocod_SetNull();
            AV44Sistema.gxTv_SdtSistema_Sistema_gpoobjctrlcod_SetNull();
            AV44Sistema.Save();
            AV26Sistema_Codigo = AV44Sistema.gxTpr_Sistema_codigo;
            context.CommitDataStores( "PRC_SistemasFromExcel");
         }
         H2Y0( false, 17) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV22Novo_Nome, "@!")), 183, Gx_line+0, 444, Gx_line+15, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+17);
      }

      protected void S121( )
      {
         /* 'NEWMODULO' Routine */
         AV55GXLvl123 = 0;
         AV56Udparg2 = new prc_padronizastring(context).executeUdp(  AV22Novo_Nome);
         /* Using cursor P002Y3 */
         pr_default.execute(1, new Object[] {AV26Sistema_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A127Sistema_Codigo = P002Y3_A127Sistema_Codigo[0];
            A143Modulo_Nome = P002Y3_A143Modulo_Nome[0];
            A146Modulo_Codigo = P002Y3_A146Modulo_Codigo[0];
            if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A143Modulo_Nome), AV56Udparg2) == 0 )
            {
               AV55GXLvl123 = 1;
               AV22Novo_Nome = AV22Novo_Nome + " (J� cadastrado)";
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( AV55GXLvl123 == 0 )
         {
            AV19Modulo = new SdtModulo(context);
            AV19Modulo.gxTpr_Modulo_nome = StringUtil.Upper( StringUtil.Trim( AV22Novo_Nome));
            AV19Modulo.gxTpr_Modulo_sigla = AV41Modulo_Sigla;
            AV19Modulo.gxTpr_Sistema_codigo = AV26Sistema_Codigo;
            AV19Modulo.Save();
            context.CommitDataStores( "PRC_SistemasFromExcel");
         }
         H2Y0( false, 18) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(edtavnovo_nome_Modulo_sigla, 250, Gx_line+0, 511, Gx_line+15, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+18);
      }

      protected void S131( )
      {
         /* 'LIMPARNOME' Routine */
         AV15i = 1;
         while ( AV15i <= StringUtil.Len( StringUtil.Trim( AV10DadosColados)) )
         {
            AV23s = (short)(AV15i+1);
            AV8Char = StringUtil.Substring( AV10DadosColados, AV15i, 1);
            if ( StringUtil.StrCmp(AV8Char, " ") == 0 )
            {
               if ( StringUtil.Asc( StringUtil.Substring( AV10DadosColados, AV23s, 1)) > 32 )
               {
                  if ( StringUtil.Len( AV22Novo_Nome) > 1 )
                  {
                     AV22Novo_Nome = AV22Novo_Nome + "_";
                  }
               }
               else
               {
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Novo_Nome)) )
                  {
                     if (true) break;
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV8Char, "�") == 0 )
            {
               AV22Novo_Nome = AV22Novo_Nome + "o";
            }
            else if ( StringUtil.StrCmp(AV8Char, "�") == 0 )
            {
               AV22Novo_Nome = AV22Novo_Nome + "a";
            }
            else if ( ( StringUtil.StrCmp(AV8Char, ",") == 0 ) || ( StringUtil.StrCmp(AV8Char, ";") == 0 ) || ( StringUtil.StrCmp(AV8Char, StringUtil.Chr( 9)) == 0 ) || ( StringUtil.StrCmp(AV8Char, StringUtil.Chr( 10)) == 0 ) || ( StringUtil.StrCmp(AV8Char, StringUtil.Chr( 13)) == 0 ) )
            {
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Novo_Nome)) )
               {
                  if (true) break;
               }
            }
            else if ( AV15i == StringUtil.Len( StringUtil.Trim( AV10DadosColados)) )
            {
               AV22Novo_Nome = AV22Novo_Nome + AV8Char;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Novo_Nome)) )
               {
                  if (true) break;
               }
            }
            else
            {
               AV22Novo_Nome = AV22Novo_Nome + AV8Char;
            }
            AV15i = (short)(AV15i+1);
         }
      }

      protected void S141( )
      {
         /* 'OPENFILE' Routine */
         AV45ErrCod = AV11ExcelDocument.Open(AV12FileName);
         if ( ( AV45ErrCod == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV32Aba)) )
         {
            AV45ErrCod = AV11ExcelDocument.SelectSheet(AV32Aba);
         }
      }

      protected void H2Y0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxDrawLine(8, Gx_line+3, 1084, Gx_line+3, 1, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV30Hoje, "99/99/99 99:99"), 508, Gx_line+7, 588, Gx_line+22, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(AV28WWPContext.gxTpr_Username, 8, Gx_line+7, 348, Gx_line+22, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 982, Gx_line+7, 1021, Gx_line+22, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 1032, Gx_line+7, 1081, Gx_line+21, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 1016, Gx_line+7, 1030, Gx_line+21, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("P�gina", 949, Gx_line+7, 984, Gx_line+21, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+25);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxDrawBitMap(context.GetImagePath( "7ad3f77f-79a3-4554-aaa4-e84910e0d464", "", context.GetTheme( )), 0, Gx_line+0, 120, Gx_line+100) ;
               getPrinter().GxAttris("Calibri", 16, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Importa��o de Sistemas e M�dulos", 367, Gx_line+0, 725, Gx_line+28, 1, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV31NomeArq, "")), 246, Gx_line+33, 846, Gx_line+51, 1, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+100);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Calibri", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV28WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV30Hoje = (DateTime)(DateTime.MinValue);
         AV31NomeArq = "";
         AV29ServerNow = (DateTime)(DateTime.MinValue);
         AV11ExcelDocument = new ExcelDocumentI();
         AV47Celula = "";
         AV25Nome = "";
         AV38String = "";
         AV22Novo_Nome = "";
         AV24Sigla = "";
         AV48NovoNome = "";
         AV41Modulo_Sigla = "";
         AV54Udparg1 = "";
         scmdbuf = "";
         P002Y2_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P002Y2_A416Sistema_Nome = new String[] {""} ;
         P002Y2_A127Sistema_Codigo = new int[1] ;
         A416Sistema_Nome = "";
         AV44Sistema = new SdtSistema(context);
         AV56Udparg2 = "";
         P002Y3_A127Sistema_Codigo = new int[1] ;
         P002Y3_A143Modulo_Nome = new String[] {""} ;
         P002Y3_A146Modulo_Codigo = new int[1] ;
         A143Modulo_Nome = "";
         AV19Modulo = new SdtModulo(context);
         edtavnovo_nome_Modulo_sigla = "";
         AV10DadosColados = "";
         AV8Char = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aprc_sistemasfromexcel__default(),
            new Object[][] {
                new Object[] {
               P002Y2_A135Sistema_AreaTrabalhoCod, P002Y2_A416Sistema_Nome, P002Y2_A127Sistema_Codigo
               }
               , new Object[] {
               P002Y3_A127Sistema_Codigo, P002Y3_A143Modulo_Nome, P002Y3_A146Modulo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV34PraLinea ;
      private short AV35ColSisNom ;
      private short AV36ColSisSgl ;
      private short AV40ColModNom ;
      private short AV46ColModSglOpc ;
      private short GxWebError ;
      private short AV39ColModSgl ;
      private short AV18Ln ;
      private short AV53GXLvl91 ;
      private short AV55GXLvl123 ;
      private short AV15i ;
      private short AV23s ;
      private short AV45ErrCod ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A127Sistema_Codigo ;
      private int AV26Sistema_Codigo ;
      private int Gx_OldLine ;
      private int A146Modulo_Codigo ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV12FileName ;
      private String AV49FileNameReal ;
      private String AV32Aba ;
      private String AV31NomeArq ;
      private String AV47Celula ;
      private String AV25Nome ;
      private String AV22Novo_Nome ;
      private String AV24Sigla ;
      private String AV48NovoNome ;
      private String AV41Modulo_Sigla ;
      private String scmdbuf ;
      private String A143Modulo_Nome ;
      private String edtavnovo_nome_Modulo_sigla ;
      private String AV8Char ;
      private DateTime AV30Hoje ;
      private DateTime AV29ServerNow ;
      private bool entryPointCalled ;
      private bool returnInSub ;
      private String AV38String ;
      private String AV54Udparg1 ;
      private String A416Sistema_Nome ;
      private String AV56Udparg2 ;
      private String AV10DadosColados ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private short aP3_PraLinea ;
      private IDataStoreProvider pr_default ;
      private int[] P002Y2_A135Sistema_AreaTrabalhoCod ;
      private String[] P002Y2_A416Sistema_Nome ;
      private int[] P002Y2_A127Sistema_Codigo ;
      private int[] P002Y3_A127Sistema_Codigo ;
      private String[] P002Y3_A143Modulo_Nome ;
      private int[] P002Y3_A146Modulo_Codigo ;
      private ExcelDocumentI AV11ExcelDocument ;
      private SdtSistema AV44Sistema ;
      private SdtModulo AV19Modulo ;
      private wwpbaseobjects.SdtWWPContext AV28WWPContext ;
   }

   public class aprc_sistemasfromexcel__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP002Y2 ;
          prmP002Y2 = new Object[] {
          new Object[] {"@AV28WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002Y3 ;
          prmP002Y3 = new Object[] {
          new Object[] {"@AV26Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P002Y2", "SELECT [Sistema_AreaTrabalhoCod], [Sistema_Nome], [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_AreaTrabalhoCod] = @AV28WWPC_1Areatrabalho_codigo ORDER BY [Sistema_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002Y2,100,0,true,false )
             ,new CursorDef("P002Y3", "SELECT [Sistema_Codigo], [Modulo_Nome], [Modulo_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV26Sistema_Codigo ORDER BY [Modulo_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002Y3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
