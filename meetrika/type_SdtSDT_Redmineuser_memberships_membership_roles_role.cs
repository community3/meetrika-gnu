/*
               File: type_SdtSDT_Redmineuser_memberships_membership_roles_role
        Description: SDT_Redmineuser
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:37:5.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Redmineuser.memberships.membership.roles.role" )]
   [XmlType(TypeName =  "SDT_Redmineuser.memberships.membership.roles.role" , Namespace = "" )]
   [Serializable]
   public class SdtSDT_Redmineuser_memberships_membership_roles_role : GxUserType
   {
      public SdtSDT_Redmineuser_memberships_membership_roles_role( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Name = "";
         gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Inherited = "";
      }

      public SdtSDT_Redmineuser_memberships_membership_roles_role( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Redmineuser_memberships_membership_roles_role deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Redmineuser_memberships_membership_roles_role)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Redmineuser_memberships_membership_roles_role obj ;
         obj = this;
         obj.gxTpr_Id = deserialized.gxTpr_Id;
         obj.gxTpr_Name = deserialized.gxTpr_Name;
         obj.gxTpr_Inherited = deserialized.gxTpr_Inherited;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         if ( oReader.ExistsAttribute("id") == 1 )
         {
            gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Id = (int)(NumberUtil.Val( oReader.GetAttributeByName("id"), "."));
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         if ( oReader.ExistsAttribute("name") == 1 )
         {
            gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Name = oReader.GetAttributeByName("name");
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         if ( oReader.ExistsAttribute("inherited") == 1 )
         {
            gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Inherited = oReader.GetAttributeByName("inherited");
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Redmineuser.memberships.membership.roles.role";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteAttribute("id", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Id), 8, 0)));
         oWriter.WriteAttribute("name", StringUtil.RTrim( gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Name));
         oWriter.WriteAttribute("inherited", StringUtil.RTrim( gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Inherited));
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("id", gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Id, false);
         AddObjectProperty("name", gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Name, false);
         AddObjectProperty("inherited", gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Inherited, false);
         return  ;
      }

      [SoapAttribute( AttributeName = "id" )]
      [XmlAttribute( AttributeName = "id" )]
      public int gxTpr_Id
      {
         get {
            return gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Id ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Id = (int)(value);
         }

      }

      [SoapAttribute( AttributeName = "name" )]
      [XmlAttribute( AttributeName = "name" )]
      public String gxTpr_Name
      {
         get {
            return gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Name ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Name = (String)(value);
         }

      }

      [SoapAttribute( AttributeName = "inherited" )]
      [XmlAttribute( AttributeName = "inherited" )]
      public String gxTpr_Inherited
      {
         get {
            return gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Inherited ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Inherited = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Name = "";
         gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Inherited = "";
         return  ;
      }

      protected short readOk ;
      protected int gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Id ;
      protected String gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Name ;
      protected String gxTv_SdtSDT_Redmineuser_memberships_membership_roles_role_Inherited ;
   }

   [DataContract(Name = @"SDT_Redmineuser.memberships.membership.roles.role", Namespace = "")]
   public class SdtSDT_Redmineuser_memberships_membership_roles_role_RESTInterface : GxGenericCollectionItem<SdtSDT_Redmineuser_memberships_membership_roles_role>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Redmineuser_memberships_membership_roles_role_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Redmineuser_memberships_membership_roles_role_RESTInterface( SdtSDT_Redmineuser_memberships_membership_roles_role psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "id" , Order = 0 )]
      public String gxTpr_Id
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Id), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Id = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "name" , Order = 1 )]
      public String gxTpr_Name
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Name) ;
         }

         set {
            sdt.gxTpr_Name = (String)(value);
         }

      }

      [DataMember( Name = "inherited" , Order = 2 )]
      public String gxTpr_Inherited
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Inherited) ;
         }

         set {
            sdt.gxTpr_Inherited = (String)(value);
         }

      }

      public SdtSDT_Redmineuser_memberships_membership_roles_role sdt
      {
         get {
            return (SdtSDT_Redmineuser_memberships_membership_roles_role)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Redmineuser_memberships_membership_roles_role() ;
         }
      }

   }

}
