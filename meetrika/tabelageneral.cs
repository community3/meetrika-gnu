/*
               File: TabelaGeneral
        Description: Tabela General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:40.31
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tabelageneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public tabelageneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public tabelageneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Tabela_Codigo )
      {
         this.A172Tabela_Codigo = aP0_Tabela_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         chkTabela_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A172Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A172Tabela_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA482( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "TabelaGeneral";
               context.Gx_err = 0;
               WS482( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Tabela General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117184040");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("tabelageneral.aspx") + "?" + UrlEncode("" +A172Tabela_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA172Tabela_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA172Tabela_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A190Tabela_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vTABELA_MODULOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Tabela_ModuloCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_DESCRICAO", GetSecureSignedToken( sPrefix, A175Tabela_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_MELHORACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A746Tabela_MelhoraCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_ATIVO", GetSecureSignedToken( sPrefix, A174Tabela_Ativo));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm482( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("tabelageneral.js", "?20203117184042");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "TabelaGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Tabela General" ;
      }

      protected void WB480( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "tabelageneral.aspx");
            }
            wb_table1_2_482( true) ;
         }
         else
         {
            wb_table1_2_482( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_482e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTabela_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTabela_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtTabela_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_TabelaGeneral.htm");
         }
         wbLoad = true;
      }

      protected void START482( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Tabela General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP480( ) ;
            }
         }
      }

      protected void WS482( )
      {
         START482( ) ;
         EVT482( ) ;
      }

      protected void EVT482( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP480( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP480( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11482 */
                                    E11482 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP480( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12482 */
                                    E12482 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP480( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13482 */
                                    E13482 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP480( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14482 */
                                    E14482 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP480( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15482 */
                                    E15482 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP480( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP480( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE482( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm482( ) ;
            }
         }
      }

      protected void PA482( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            chkTabela_Ativo.Name = "TABELA_ATIVO";
            chkTabela_Ativo.WebTags = "";
            chkTabela_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkTabela_Ativo_Internalname, "TitleCaption", chkTabela_Ativo.Caption);
            chkTabela_Ativo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF482( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "TabelaGeneral";
         context.Gx_err = 0;
      }

      protected void RF482( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00482 */
            pr_default.execute(0, new Object[] {A172Tabela_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A181Tabela_PaiCod = H00482_A181Tabela_PaiCod[0];
               n181Tabela_PaiCod = H00482_n181Tabela_PaiCod[0];
               A188Tabela_ModuloCod = H00482_A188Tabela_ModuloCod[0];
               n188Tabela_ModuloCod = H00482_n188Tabela_ModuloCod[0];
               A190Tabela_SistemaCod = H00482_A190Tabela_SistemaCod[0];
               A174Tabela_Ativo = H00482_A174Tabela_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A174Tabela_Ativo", A174Tabela_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TABELA_ATIVO", GetSecureSignedToken( sPrefix, A174Tabela_Ativo));
               A746Tabela_MelhoraCod = H00482_A746Tabela_MelhoraCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A746Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A746Tabela_MelhoraCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TABELA_MELHORACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A746Tabela_MelhoraCod), "ZZZZZ9")));
               n746Tabela_MelhoraCod = H00482_n746Tabela_MelhoraCod[0];
               A182Tabela_PaiNom = H00482_A182Tabela_PaiNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A182Tabela_PaiNom", A182Tabela_PaiNom);
               n182Tabela_PaiNom = H00482_n182Tabela_PaiNom[0];
               A189Tabela_ModuloDes = H00482_A189Tabela_ModuloDes[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A189Tabela_ModuloDes", A189Tabela_ModuloDes);
               n189Tabela_ModuloDes = H00482_n189Tabela_ModuloDes[0];
               A191Tabela_SistemaDes = H00482_A191Tabela_SistemaDes[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A191Tabela_SistemaDes", A191Tabela_SistemaDes);
               n191Tabela_SistemaDes = H00482_n191Tabela_SistemaDes[0];
               A175Tabela_Descricao = H00482_A175Tabela_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A175Tabela_Descricao", A175Tabela_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TABELA_DESCRICAO", GetSecureSignedToken( sPrefix, A175Tabela_Descricao));
               n175Tabela_Descricao = H00482_n175Tabela_Descricao[0];
               A173Tabela_Nome = H00482_A173Tabela_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A173Tabela_Nome", A173Tabela_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TABELA_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!"))));
               A182Tabela_PaiNom = H00482_A182Tabela_PaiNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A182Tabela_PaiNom", A182Tabela_PaiNom);
               n182Tabela_PaiNom = H00482_n182Tabela_PaiNom[0];
               A189Tabela_ModuloDes = H00482_A189Tabela_ModuloDes[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A189Tabela_ModuloDes", A189Tabela_ModuloDes);
               n189Tabela_ModuloDes = H00482_n189Tabela_ModuloDes[0];
               A191Tabela_SistemaDes = H00482_A191Tabela_SistemaDes[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A191Tabela_SistemaDes", A191Tabela_SistemaDes);
               n191Tabela_SistemaDes = H00482_n191Tabela_SistemaDes[0];
               /* Execute user event: E12482 */
               E12482 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB480( ) ;
         }
      }

      protected void STRUP480( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "TabelaGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11482 */
         E11482 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A173Tabela_Nome = StringUtil.Upper( cgiGet( edtTabela_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A173Tabela_Nome", A173Tabela_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TABELA_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!"))));
            A175Tabela_Descricao = cgiGet( edtTabela_Descricao_Internalname);
            n175Tabela_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A175Tabela_Descricao", A175Tabela_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TABELA_DESCRICAO", GetSecureSignedToken( sPrefix, A175Tabela_Descricao));
            A191Tabela_SistemaDes = StringUtil.Upper( cgiGet( edtTabela_SistemaDes_Internalname));
            n191Tabela_SistemaDes = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A191Tabela_SistemaDes", A191Tabela_SistemaDes);
            A189Tabela_ModuloDes = StringUtil.Upper( cgiGet( edtTabela_ModuloDes_Internalname));
            n189Tabela_ModuloDes = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A189Tabela_ModuloDes", A189Tabela_ModuloDes);
            A182Tabela_PaiNom = StringUtil.Upper( cgiGet( edtTabela_PaiNom_Internalname));
            n182Tabela_PaiNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A182Tabela_PaiNom", A182Tabela_PaiNom);
            A746Tabela_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( edtTabela_MelhoraCod_Internalname), ",", "."));
            n746Tabela_MelhoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A746Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A746Tabela_MelhoraCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TABELA_MELHORACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A746Tabela_MelhoraCod), "ZZZZZ9")));
            A174Tabela_Ativo = StringUtil.StrToBool( cgiGet( chkTabela_Ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A174Tabela_Ativo", A174Tabela_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TABELA_ATIVO", GetSecureSignedToken( sPrefix, A174Tabela_Ativo));
            /* Read saved values. */
            wcpOA172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA172Tabela_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11482 */
         E11482 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11482( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12482( )
      {
         /* Load Routine */
         edtTabela_ModuloDes_Link = formatLink("viewmodulo.aspx") + "?" + UrlEncode("" +A188Tabela_ModuloCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_ModuloDes_Internalname, "Link", edtTabela_ModuloDes_Link);
         edtTabela_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTabela_Codigo_Visible), 5, 0)));
         if ( ! ( ( AV6WWPContext.gxTpr_Update ) ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Delete ) ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
      }

      protected void E13482( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("tabela.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A172Tabela_Codigo) + "," + UrlEncode("" +A190Tabela_SistemaCod) + "," + UrlEncode("" +AV12Tabela_ModuloCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E14482( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("tabela.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A172Tabela_Codigo) + "," + UrlEncode("" +A190Tabela_SistemaCod) + "," + UrlEncode("" +AV12Tabela_ModuloCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E15482( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("wwtabela.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Tabela";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Tabela_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Tabela_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_482( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_482( true) ;
         }
         else
         {
            wb_table2_8_482( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_482e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_46_482( true) ;
         }
         else
         {
            wb_table3_46_482( false) ;
         }
         return  ;
      }

      protected void wb_table3_46_482e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_482e( true) ;
         }
         else
         {
            wb_table1_2_482e( false) ;
         }
      }

      protected void wb_table3_46_482( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_TabelaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_TabelaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_TabelaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_46_482e( true) ;
         }
         else
         {
            wb_table3_46_482e( false) ;
         }
      }

      protected void wb_table2_8_482( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_nome_Internalname, "Nome", "", "", lblTextblocktabela_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TabelaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTabela_Nome_Internalname, StringUtil.RTrim( A173Tabela_Nome), StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTabela_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_TabelaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_descricao_Internalname, "Descri��o", "", "", lblTextblocktabela_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TabelaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtTabela_Descricao_Internalname, A175Tabela_Descricao, "", "", 0, 1, 0, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_TabelaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_sistemades_Internalname, "Sistema", "", "", lblTextblocktabela_sistemades_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TabelaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTabela_SistemaDes_Internalname, A191Tabela_SistemaDes, StringUtil.RTrim( context.localUtil.Format( A191Tabela_SistemaDes, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTabela_SistemaDes_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TabelaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_modulodes_Internalname, "M�dulo", "", "", lblTextblocktabela_modulodes_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TabelaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTabela_ModuloDes_Internalname, StringUtil.RTrim( A189Tabela_ModuloDes), StringUtil.RTrim( context.localUtil.Format( A189Tabela_ModuloDes, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtTabela_ModuloDes_Link, "", "", "", edtTabela_ModuloDes_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_TabelaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_painom_Internalname, "Tabela Pai", "", "", lblTextblocktabela_painom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TabelaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTabela_PaiNom_Internalname, StringUtil.RTrim( A182Tabela_PaiNom), StringUtil.RTrim( context.localUtil.Format( A182Tabela_PaiNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTabela_PaiNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_TabelaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_melhoracod_Internalname, "a melhorar", "", "", lblTextblocktabela_melhoracod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TabelaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTabela_MelhoraCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A746Tabela_MelhoraCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A746Tabela_MelhoraCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTabela_MelhoraCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_TabelaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_ativo_Internalname, "Ativo", "", "", lblTextblocktabela_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TabelaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkTabela_Ativo_Internalname, StringUtil.BoolToStr( A174Tabela_Ativo), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_482e( true) ;
         }
         else
         {
            wb_table2_8_482e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A172Tabela_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA482( ) ;
         WS482( ) ;
         WE482( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA172Tabela_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA482( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "tabelageneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA482( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A172Tabela_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
         }
         wcpOA172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA172Tabela_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A172Tabela_Codigo != wcpOA172Tabela_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA172Tabela_Codigo = A172Tabela_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA172Tabela_Codigo = cgiGet( sPrefix+"A172Tabela_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA172Tabela_Codigo) > 0 )
         {
            A172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA172Tabela_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
         }
         else
         {
            A172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A172Tabela_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA482( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS482( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS482( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A172Tabela_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA172Tabela_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A172Tabela_Codigo_CTRL", StringUtil.RTrim( sCtrlA172Tabela_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE482( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117184099");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("tabelageneral.js", "?2020311718410");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocktabela_nome_Internalname = sPrefix+"TEXTBLOCKTABELA_NOME";
         edtTabela_Nome_Internalname = sPrefix+"TABELA_NOME";
         lblTextblocktabela_descricao_Internalname = sPrefix+"TEXTBLOCKTABELA_DESCRICAO";
         edtTabela_Descricao_Internalname = sPrefix+"TABELA_DESCRICAO";
         lblTextblocktabela_sistemades_Internalname = sPrefix+"TEXTBLOCKTABELA_SISTEMADES";
         edtTabela_SistemaDes_Internalname = sPrefix+"TABELA_SISTEMADES";
         lblTextblocktabela_modulodes_Internalname = sPrefix+"TEXTBLOCKTABELA_MODULODES";
         edtTabela_ModuloDes_Internalname = sPrefix+"TABELA_MODULODES";
         lblTextblocktabela_painom_Internalname = sPrefix+"TEXTBLOCKTABELA_PAINOM";
         edtTabela_PaiNom_Internalname = sPrefix+"TABELA_PAINOM";
         lblTextblocktabela_melhoracod_Internalname = sPrefix+"TEXTBLOCKTABELA_MELHORACOD";
         edtTabela_MelhoraCod_Internalname = sPrefix+"TABELA_MELHORACOD";
         lblTextblocktabela_ativo_Internalname = sPrefix+"TEXTBLOCKTABELA_ATIVO";
         chkTabela_Ativo_Internalname = sPrefix+"TABELA_ATIVO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtTabela_MelhoraCod_Jsonclick = "";
         edtTabela_PaiNom_Jsonclick = "";
         edtTabela_ModuloDes_Jsonclick = "";
         edtTabela_SistemaDes_Jsonclick = "";
         edtTabela_Nome_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtnupdate_Enabled = 1;
         edtTabela_ModuloDes_Link = "";
         chkTabela_Ativo.Caption = "";
         edtTabela_Codigo_Jsonclick = "";
         edtTabela_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13482',iparms:[{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV12Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E14482',iparms:[{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV12Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15482',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A173Tabela_Nome = "";
         A175Tabela_Descricao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00482_A181Tabela_PaiCod = new int[1] ;
         H00482_n181Tabela_PaiCod = new bool[] {false} ;
         H00482_A172Tabela_Codigo = new int[1] ;
         H00482_A188Tabela_ModuloCod = new int[1] ;
         H00482_n188Tabela_ModuloCod = new bool[] {false} ;
         H00482_A190Tabela_SistemaCod = new int[1] ;
         H00482_A174Tabela_Ativo = new bool[] {false} ;
         H00482_A746Tabela_MelhoraCod = new int[1] ;
         H00482_n746Tabela_MelhoraCod = new bool[] {false} ;
         H00482_A182Tabela_PaiNom = new String[] {""} ;
         H00482_n182Tabela_PaiNom = new bool[] {false} ;
         H00482_A189Tabela_ModuloDes = new String[] {""} ;
         H00482_n189Tabela_ModuloDes = new bool[] {false} ;
         H00482_A191Tabela_SistemaDes = new String[] {""} ;
         H00482_n191Tabela_SistemaDes = new bool[] {false} ;
         H00482_A175Tabela_Descricao = new String[] {""} ;
         H00482_n175Tabela_Descricao = new bool[] {false} ;
         H00482_A173Tabela_Nome = new String[] {""} ;
         A182Tabela_PaiNom = "";
         A189Tabela_ModuloDes = "";
         A191Tabela_SistemaDes = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblocktabela_nome_Jsonclick = "";
         lblTextblocktabela_descricao_Jsonclick = "";
         lblTextblocktabela_sistemades_Jsonclick = "";
         lblTextblocktabela_modulodes_Jsonclick = "";
         lblTextblocktabela_painom_Jsonclick = "";
         lblTextblocktabela_melhoracod_Jsonclick = "";
         lblTextblocktabela_ativo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA172Tabela_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.tabelageneral__default(),
            new Object[][] {
                new Object[] {
               H00482_A181Tabela_PaiCod, H00482_n181Tabela_PaiCod, H00482_A172Tabela_Codigo, H00482_A188Tabela_ModuloCod, H00482_n188Tabela_ModuloCod, H00482_A190Tabela_SistemaCod, H00482_A174Tabela_Ativo, H00482_A746Tabela_MelhoraCod, H00482_n746Tabela_MelhoraCod, H00482_A182Tabela_PaiNom,
               H00482_n182Tabela_PaiNom, H00482_A189Tabela_ModuloDes, H00482_n189Tabela_ModuloDes, H00482_A191Tabela_SistemaDes, H00482_n191Tabela_SistemaDes, H00482_A175Tabela_Descricao, H00482_n175Tabela_Descricao, H00482_A173Tabela_Nome
               }
            }
         );
         AV15Pgmname = "TabelaGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "TabelaGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A172Tabela_Codigo ;
      private int wcpOA172Tabela_Codigo ;
      private int A190Tabela_SistemaCod ;
      private int AV12Tabela_ModuloCod ;
      private int A746Tabela_MelhoraCod ;
      private int edtTabela_Codigo_Visible ;
      private int A181Tabela_PaiCod ;
      private int A188Tabela_ModuloCod ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int AV7Tabela_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A173Tabela_Nome ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtTabela_Codigo_Internalname ;
      private String edtTabela_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkTabela_Ativo_Internalname ;
      private String scmdbuf ;
      private String A182Tabela_PaiNom ;
      private String A189Tabela_ModuloDes ;
      private String edtTabela_Nome_Internalname ;
      private String edtTabela_Descricao_Internalname ;
      private String edtTabela_SistemaDes_Internalname ;
      private String edtTabela_ModuloDes_Internalname ;
      private String edtTabela_PaiNom_Internalname ;
      private String edtTabela_MelhoraCod_Internalname ;
      private String edtTabela_ModuloDes_Link ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocktabela_nome_Internalname ;
      private String lblTextblocktabela_nome_Jsonclick ;
      private String edtTabela_Nome_Jsonclick ;
      private String lblTextblocktabela_descricao_Internalname ;
      private String lblTextblocktabela_descricao_Jsonclick ;
      private String lblTextblocktabela_sistemades_Internalname ;
      private String lblTextblocktabela_sistemades_Jsonclick ;
      private String edtTabela_SistemaDes_Jsonclick ;
      private String lblTextblocktabela_modulodes_Internalname ;
      private String lblTextblocktabela_modulodes_Jsonclick ;
      private String edtTabela_ModuloDes_Jsonclick ;
      private String lblTextblocktabela_painom_Internalname ;
      private String lblTextblocktabela_painom_Jsonclick ;
      private String edtTabela_PaiNom_Jsonclick ;
      private String lblTextblocktabela_melhoracod_Internalname ;
      private String lblTextblocktabela_melhoracod_Jsonclick ;
      private String edtTabela_MelhoraCod_Jsonclick ;
      private String lblTextblocktabela_ativo_Internalname ;
      private String lblTextblocktabela_ativo_Jsonclick ;
      private String sCtrlA172Tabela_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A174Tabela_Ativo ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n181Tabela_PaiCod ;
      private bool n188Tabela_ModuloCod ;
      private bool n746Tabela_MelhoraCod ;
      private bool n182Tabela_PaiNom ;
      private bool n189Tabela_ModuloDes ;
      private bool n191Tabela_SistemaDes ;
      private bool n175Tabela_Descricao ;
      private bool returnInSub ;
      private String A175Tabela_Descricao ;
      private String A191Tabela_SistemaDes ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkTabela_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H00482_A181Tabela_PaiCod ;
      private bool[] H00482_n181Tabela_PaiCod ;
      private int[] H00482_A172Tabela_Codigo ;
      private int[] H00482_A188Tabela_ModuloCod ;
      private bool[] H00482_n188Tabela_ModuloCod ;
      private int[] H00482_A190Tabela_SistemaCod ;
      private bool[] H00482_A174Tabela_Ativo ;
      private int[] H00482_A746Tabela_MelhoraCod ;
      private bool[] H00482_n746Tabela_MelhoraCod ;
      private String[] H00482_A182Tabela_PaiNom ;
      private bool[] H00482_n182Tabela_PaiNom ;
      private String[] H00482_A189Tabela_ModuloDes ;
      private bool[] H00482_n189Tabela_ModuloDes ;
      private String[] H00482_A191Tabela_SistemaDes ;
      private bool[] H00482_n191Tabela_SistemaDes ;
      private String[] H00482_A175Tabela_Descricao ;
      private bool[] H00482_n175Tabela_Descricao ;
      private String[] H00482_A173Tabela_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class tabelageneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00482 ;
          prmH00482 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00482", "SELECT T1.[Tabela_PaiCod] AS Tabela_PaiCod, T1.[Tabela_Codigo], T1.[Tabela_ModuloCod] AS Tabela_ModuloCod, T1.[Tabela_SistemaCod] AS Tabela_SistemaCod, T1.[Tabela_Ativo], T1.[Tabela_MelhoraCod], T2.[Tabela_Nome] AS Tabela_PaiNom, T3.[Modulo_Nome] AS Tabela_ModuloDes, T4.[Sistema_Nome] AS Tabela_SistemaDes, T1.[Tabela_Descricao], T1.[Tabela_Nome] FROM ((([Tabela] T1 WITH (NOLOCK) LEFT JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Tabela_PaiCod]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = T1.[Tabela_ModuloCod]) INNER JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = T1.[Tabela_SistemaCod]) WHERE T1.[Tabela_Codigo] = @Tabela_Codigo ORDER BY T1.[Tabela_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00482,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getLongVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
