/*
               File: PRC_SaldoContratoDebitoSaldo
        Description: PRC_Saldo Contrato Debito Saldo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:45.85
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_saldocontratodebitosaldo : GXProcedure
   {
      public prc_saldocontratodebitosaldo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_saldocontratodebitosaldo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_SaldoContrato_Codigo ,
                           int aP1_Contrato_Codigo ,
                           int aP2_NotaEmpenho_Codigo ,
                           int aP3_ContagemResultado_Codigo ,
                           decimal aP4_AuxSaldoContrato_Debito )
      {
         this.AV8SaldoContrato_Codigo = aP0_SaldoContrato_Codigo;
         this.AV13Contrato_Codigo = aP1_Contrato_Codigo;
         this.AV11NotaEmpenho_Codigo = aP2_NotaEmpenho_Codigo;
         this.AV12ContagemResultado_Codigo = aP3_ContagemResultado_Codigo;
         this.AV14AuxSaldoContrato_Debito = aP4_AuxSaldoContrato_Debito;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_SaldoContrato_Codigo ,
                                 int aP1_Contrato_Codigo ,
                                 int aP2_NotaEmpenho_Codigo ,
                                 int aP3_ContagemResultado_Codigo ,
                                 decimal aP4_AuxSaldoContrato_Debito )
      {
         prc_saldocontratodebitosaldo objprc_saldocontratodebitosaldo;
         objprc_saldocontratodebitosaldo = new prc_saldocontratodebitosaldo();
         objprc_saldocontratodebitosaldo.AV8SaldoContrato_Codigo = aP0_SaldoContrato_Codigo;
         objprc_saldocontratodebitosaldo.AV13Contrato_Codigo = aP1_Contrato_Codigo;
         objprc_saldocontratodebitosaldo.AV11NotaEmpenho_Codigo = aP2_NotaEmpenho_Codigo;
         objprc_saldocontratodebitosaldo.AV12ContagemResultado_Codigo = aP3_ContagemResultado_Codigo;
         objprc_saldocontratodebitosaldo.AV14AuxSaldoContrato_Debito = aP4_AuxSaldoContrato_Debito;
         objprc_saldocontratodebitosaldo.context.SetSubmitInitialConfig(context);
         objprc_saldocontratodebitosaldo.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_saldocontratodebitosaldo);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_saldocontratodebitosaldo)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00C12 */
         pr_default.execute(0, new Object[] {AV8SaldoContrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1561SaldoContrato_Codigo = P00C12_A1561SaldoContrato_Codigo[0];
            A1574SaldoContrato_Reservado = P00C12_A1574SaldoContrato_Reservado[0];
            A1576SaldoContrato_Saldo = P00C12_A1576SaldoContrato_Saldo[0];
            if ( ! (0==AV12ContagemResultado_Codigo) )
            {
               if ( AV14AuxSaldoContrato_Debito <= ( A1576SaldoContrato_Saldo + A1574SaldoContrato_Reservado ) )
               {
                  if ( AV14AuxSaldoContrato_Debito <= A1574SaldoContrato_Reservado )
                  {
                     new prc_saldocontratodebitosaldoreserva(context ).execute(  AV8SaldoContrato_Codigo,  AV13Contrato_Codigo,  AV11NotaEmpenho_Codigo,  AV12ContagemResultado_Codigo,  AV14AuxSaldoContrato_Debito) ;
                  }
                  else
                  {
                     new prc_saldocontratodebitosaldoreserva(context ).execute(  AV8SaldoContrato_Codigo,  AV13Contrato_Codigo,  AV11NotaEmpenho_Codigo,  AV12ContagemResultado_Codigo,  A1574SaldoContrato_Reservado) ;
                     AV15AuxResidualSaldoContrato_Debito = (decimal)(AV14AuxSaldoContrato_Debito-A1574SaldoContrato_Reservado);
                     if ( ( AV15AuxResidualSaldoContrato_Debito > Convert.ToDecimal( 0 )) )
                     {
                        A1576SaldoContrato_Saldo = (decimal)(A1576SaldoContrato_Saldo-AV15AuxResidualSaldoContrato_Debito);
                        new prc_historicoconsumo(context ).execute(  AV8SaldoContrato_Codigo,  AV13Contrato_Codigo,  AV11NotaEmpenho_Codigo,  AV12ContagemResultado_Codigo,  0,  AV15AuxResidualSaldoContrato_Debito,  "DEB") ;
                     }
                  }
               }
            }
            else
            {
               if ( AV14AuxSaldoContrato_Debito <= A1576SaldoContrato_Saldo )
               {
                  A1576SaldoContrato_Saldo = (decimal)(A1576SaldoContrato_Saldo-AV14AuxSaldoContrato_Debito);
                  new prc_historicoconsumo(context ).execute(  AV8SaldoContrato_Codigo,  AV13Contrato_Codigo,  AV11NotaEmpenho_Codigo,  AV12ContagemResultado_Codigo,  0,  AV14AuxSaldoContrato_Debito,  "DEB") ;
               }
            }
            /* Using cursor P00C13 */
            pr_default.execute(1, new Object[] {A1576SaldoContrato_Saldo, A1561SaldoContrato_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("SaldoContrato") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00C12_A1561SaldoContrato_Codigo = new int[1] ;
         P00C12_A1574SaldoContrato_Reservado = new decimal[1] ;
         P00C12_A1576SaldoContrato_Saldo = new decimal[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_saldocontratodebitosaldo__default(),
            new Object[][] {
                new Object[] {
               P00C12_A1561SaldoContrato_Codigo, P00C12_A1574SaldoContrato_Reservado, P00C12_A1576SaldoContrato_Saldo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8SaldoContrato_Codigo ;
      private int AV13Contrato_Codigo ;
      private int AV11NotaEmpenho_Codigo ;
      private int AV12ContagemResultado_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private decimal AV14AuxSaldoContrato_Debito ;
      private decimal A1574SaldoContrato_Reservado ;
      private decimal A1576SaldoContrato_Saldo ;
      private decimal AV15AuxResidualSaldoContrato_Debito ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00C12_A1561SaldoContrato_Codigo ;
      private decimal[] P00C12_A1574SaldoContrato_Reservado ;
      private decimal[] P00C12_A1576SaldoContrato_Saldo ;
   }

   public class prc_saldocontratodebitosaldo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00C12 ;
          prmP00C12 = new Object[] {
          new Object[] {"@AV8SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00C13 ;
          prmP00C13 = new Object[] {
          new Object[] {"@SaldoContrato_Saldo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00C12", "SELECT [SaldoContrato_Codigo], [SaldoContrato_Reservado], [SaldoContrato_Saldo] FROM [SaldoContrato] WITH (UPDLOCK) WHERE [SaldoContrato_Codigo] = @AV8SaldoContrato_Codigo ORDER BY [SaldoContrato_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00C12,1,0,true,true )
             ,new CursorDef("P00C13", "UPDATE [SaldoContrato] SET [SaldoContrato_Saldo]=@SaldoContrato_Saldo  WHERE [SaldoContrato_Codigo] = @SaldoContrato_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00C13)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (decimal)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
