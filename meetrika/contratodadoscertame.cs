/*
               File: ContratoDadosCertame
        Description: Contrato Dados Certame
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:19:41.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratodadoscertame : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratoDadosCertame_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoDadosCertame_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoDadosCertame_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATODADOSCERTAME_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoDadosCertame_Codigo), "ZZZZZ9")));
               A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contrato Dados Certame", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContratoDadosCertame_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratodadoscertame( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratodadoscertame( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratoDadosCertame_Codigo ,
                           ref int aP2_Contrato_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratoDadosCertame_Codigo = aP1_ContratoDadosCertame_Codigo;
         this.A74Contrato_Codigo = aP2_Contrato_Codigo;
         executePrivate();
         aP2_Contrato_Codigo=this.A74Contrato_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1D52( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1D52e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1D52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1D52( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1D52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_82_1D52( true) ;
         }
         return  ;
      }

      protected void wb_table3_82_1D52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1D52e( true) ;
         }
         else
         {
            wb_table1_2_1D52e( false) ;
         }
      }

      protected void wb_table3_82_1D52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_82_1D52e( true) ;
         }
         else
         {
            wb_table3_82_1D52e( false) ;
         }
      }

      protected void wb_table2_5_1D52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_1D52( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_1D52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1D52e( true) ;
         }
         else
         {
            wb_table2_5_1D52e( false) ;
         }
      }

      protected void wb_table4_13_1D52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup1_Internalname, "do Contrato", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratoDadosCertame.htm");
            wb_table5_17_1D52( true) ;
         }
         return  ;
      }

      protected void wb_table5_17_1D52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratodadoscertame_data_Internalname, "Data", "", "", lblTextblockcontratodadoscertame_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContratoDadosCertame_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoDadosCertame_Data_Internalname, context.localUtil.Format(A311ContratoDadosCertame_Data, "99/99/99"), context.localUtil.Format( A311ContratoDadosCertame_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoDadosCertame_Data_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContratoDadosCertame_Data_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtContratoDadosCertame_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContratoDadosCertame_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratodadoscertame_modalidade_Internalname, "Modalidade", "", "", lblTextblockcontratodadoscertame_modalidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoDadosCertame_Modalidade_Internalname, A307ContratoDadosCertame_Modalidade, StringUtil.RTrim( context.localUtil.Format( A307ContratoDadosCertame_Modalidade, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoDadosCertame_Modalidade_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoDadosCertame_Modalidade_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratodadoscertame_numero_Internalname, "Numero", "", "", lblTextblockcontratodadoscertame_numero_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoDadosCertame_Numero_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A308ContratoDadosCertame_Numero), 10, 0, ",", "")), ((edtContratoDadosCertame_Numero_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A308ContratoDadosCertame_Numero), "ZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A308ContratoDadosCertame_Numero), "ZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoDadosCertame_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoDadosCertame_Numero_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratodadoscertame_site_Internalname, "Site", "", "", lblTextblockcontratodadoscertame_site_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoDadosCertame_Site_Internalname, A309ContratoDadosCertame_Site, StringUtil.RTrim( context.localUtil.Format( A309ContratoDadosCertame_Site, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoDadosCertame_Site_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoDadosCertame_Site_Enabled, 0, "text", "", 360, "px", 1, "row", 1000, 0, 0, 0, 1, -1, -1, true, "URLString", "left", true, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratodadoscertame_uasg_Internalname, "Uasg", "", "", lblTextblockcontratodadoscertame_uasg_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoDadosCertame_Uasg_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A310ContratoDadosCertame_Uasg), 4, 0, ",", "")), ((edtContratoDadosCertame_Uasg_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A310ContratoDadosCertame_Uasg), "ZZZ9")) : context.localUtil.Format( (decimal)(A310ContratoDadosCertame_Uasg), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoDadosCertame_Uasg_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoDadosCertame_Uasg_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratodadoscertame_datahomologacao_Internalname, "Data de Homologa��o", "", "", lblTextblockcontratodadoscertame_datahomologacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_72_1D52( true) ;
         }
         return  ;
      }

      protected void wb_table6_72_1D52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_1D52e( true) ;
         }
         else
         {
            wb_table4_13_1D52e( false) ;
         }
      }

      protected void wb_table6_72_1D52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratodadoscertame_datahomologacao_Internalname, tblTablemergedcontratodadoscertame_datahomologacao_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContratoDadosCertame_DataHomologacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoDadosCertame_DataHomologacao_Internalname, context.localUtil.Format(A312ContratoDadosCertame_DataHomologacao, "99/99/99"), context.localUtil.Format( A312ContratoDadosCertame_DataHomologacao, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoDadosCertame_DataHomologacao_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContratoDadosCertame_DataHomologacao_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtContratoDadosCertame_DataHomologacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContratoDadosCertame_DataHomologacao_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratodadoscertame_dataadjudicacao_Internalname, "Data de Adjudica��o", "", "", lblTextblockcontratodadoscertame_dataadjudicacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContratoDadosCertame_DataAdjudicacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoDadosCertame_DataAdjudicacao_Internalname, context.localUtil.Format(A313ContratoDadosCertame_DataAdjudicacao, "99/99/99"), context.localUtil.Format( A313ContratoDadosCertame_DataAdjudicacao, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoDadosCertame_DataAdjudicacao_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContratoDadosCertame_DataAdjudicacao_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoDadosCertame.htm");
            GxWebStd.gx_bitmap( context, edtContratoDadosCertame_DataAdjudicacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContratoDadosCertame_DataAdjudicacao_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_72_1D52e( true) ;
         }
         else
         {
            wb_table6_72_1D52e( false) ;
         }
      }

      protected void wb_table5_17_1D52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblContrato_Internalname, tblContrato_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numero_Internalname, "N� do Contrato", "", "", lblTextblockcontrato_numero_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table7_22_1D52( true) ;
         }
         return  ;
      }

      protected void wb_table7_22_1D52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoanom_Internalname, "Contratada", "", "", lblTextblockcontratada_pessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table8_38_1D52( true) ;
         }
         return  ;
      }

      protected void wb_table8_38_1D52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_17_1D52e( true) ;
         }
         else
         {
            wb_table5_17_1D52e( false) ;
         }
      }

      protected void wb_table8_38_1D52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratada_pessoanom_Internalname, tblTablemergedcontratada_pessoanom_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaNom_Internalname, StringUtil.RTrim( A41Contratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_PessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoacnpj_Internalname, "CNPJ", "", "", lblTextblockcontratada_pessoacnpj_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaCNPJ_Internalname, A42Contratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( A42Contratada_PessoaCNPJ, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaCNPJ_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_PessoaCNPJ_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_38_1D52e( true) ;
         }
         else
         {
            wb_table8_38_1D52e( false) ;
         }
      }

      protected void wb_table7_22_1D52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontrato_numero_Internalname, tblTablemergedcontrato_numero_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Numero_Internalname, StringUtil.RTrim( A77Contrato_Numero), StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Numero_Enabled, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "NumeroContrato", "left", true, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numeroata_Internalname, "N� da Ata", "", "", lblTextblockcontrato_numeroata_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_NumeroAta_Internalname, StringUtil.RTrim( A78Contrato_NumeroAta), StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_NumeroAta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_NumeroAta_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "NumeroAta", "left", true, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_ano_Internalname, "Ano", "", "", lblTextblockcontrato_ano_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Ano_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ",", "")), ((edtContrato_Ano_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")) : context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Ano_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Ano_Enabled, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_ContratoDadosCertame.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_22_1D52e( true) ;
         }
         else
         {
            wb_table7_22_1D52e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E111D2 */
         E111D2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
               A78Contrato_NumeroAta = cgiGet( edtContrato_NumeroAta_Internalname);
               n78Contrato_NumeroAta = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
               A79Contrato_Ano = (short)(context.localUtil.CToN( cgiGet( edtContrato_Ano_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
               A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
               n41Contratada_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
               A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
               n42Contratada_PessoaCNPJ = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
               if ( context.localUtil.VCDate( cgiGet( edtContratoDadosCertame_Data_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data do certame"}), 1, "CONTRATODADOSCERTAME_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtContratoDadosCertame_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A311ContratoDadosCertame_Data = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A311ContratoDadosCertame_Data", context.localUtil.Format(A311ContratoDadosCertame_Data, "99/99/99"));
               }
               else
               {
                  A311ContratoDadosCertame_Data = context.localUtil.CToD( cgiGet( edtContratoDadosCertame_Data_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A311ContratoDadosCertame_Data", context.localUtil.Format(A311ContratoDadosCertame_Data, "99/99/99"));
               }
               A307ContratoDadosCertame_Modalidade = cgiGet( edtContratoDadosCertame_Modalidade_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A307ContratoDadosCertame_Modalidade", A307ContratoDadosCertame_Modalidade);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoDadosCertame_Numero_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoDadosCertame_Numero_Internalname), ",", ".") > Convert.ToDecimal( 9999999999L )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATODADOSCERTAME_NUMERO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoDadosCertame_Numero_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A308ContratoDadosCertame_Numero = 0;
                  n308ContratoDadosCertame_Numero = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A308ContratoDadosCertame_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(A308ContratoDadosCertame_Numero), 10, 0)));
               }
               else
               {
                  A308ContratoDadosCertame_Numero = (long)(context.localUtil.CToN( cgiGet( edtContratoDadosCertame_Numero_Internalname), ",", "."));
                  n308ContratoDadosCertame_Numero = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A308ContratoDadosCertame_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(A308ContratoDadosCertame_Numero), 10, 0)));
               }
               n308ContratoDadosCertame_Numero = ((0==A308ContratoDadosCertame_Numero) ? true : false);
               A309ContratoDadosCertame_Site = cgiGet( edtContratoDadosCertame_Site_Internalname);
               n309ContratoDadosCertame_Site = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A309ContratoDadosCertame_Site", A309ContratoDadosCertame_Site);
               n309ContratoDadosCertame_Site = (String.IsNullOrEmpty(StringUtil.RTrim( A309ContratoDadosCertame_Site)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoDadosCertame_Uasg_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoDadosCertame_Uasg_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATODADOSCERTAME_UASG");
                  AnyError = 1;
                  GX_FocusControl = edtContratoDadosCertame_Uasg_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A310ContratoDadosCertame_Uasg = 0;
                  n310ContratoDadosCertame_Uasg = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A310ContratoDadosCertame_Uasg", StringUtil.LTrim( StringUtil.Str( (decimal)(A310ContratoDadosCertame_Uasg), 4, 0)));
               }
               else
               {
                  A310ContratoDadosCertame_Uasg = (short)(context.localUtil.CToN( cgiGet( edtContratoDadosCertame_Uasg_Internalname), ",", "."));
                  n310ContratoDadosCertame_Uasg = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A310ContratoDadosCertame_Uasg", StringUtil.LTrim( StringUtil.Str( (decimal)(A310ContratoDadosCertame_Uasg), 4, 0)));
               }
               n310ContratoDadosCertame_Uasg = ((0==A310ContratoDadosCertame_Uasg) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtContratoDadosCertame_DataHomologacao_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data de homologa��o"}), 1, "CONTRATODADOSCERTAME_DATAHOMOLOGACAO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoDadosCertame_DataHomologacao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A312ContratoDadosCertame_DataHomologacao = DateTime.MinValue;
                  n312ContratoDadosCertame_DataHomologacao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A312ContratoDadosCertame_DataHomologacao", context.localUtil.Format(A312ContratoDadosCertame_DataHomologacao, "99/99/99"));
               }
               else
               {
                  A312ContratoDadosCertame_DataHomologacao = context.localUtil.CToD( cgiGet( edtContratoDadosCertame_DataHomologacao_Internalname), 2);
                  n312ContratoDadosCertame_DataHomologacao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A312ContratoDadosCertame_DataHomologacao", context.localUtil.Format(A312ContratoDadosCertame_DataHomologacao, "99/99/99"));
               }
               n312ContratoDadosCertame_DataHomologacao = ((DateTime.MinValue==A312ContratoDadosCertame_DataHomologacao) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtContratoDadosCertame_DataAdjudicacao_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data de adjudica��o"}), 1, "CONTRATODADOSCERTAME_DATAADJUDICACAO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoDadosCertame_DataAdjudicacao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A313ContratoDadosCertame_DataAdjudicacao = DateTime.MinValue;
                  n313ContratoDadosCertame_DataAdjudicacao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A313ContratoDadosCertame_DataAdjudicacao", context.localUtil.Format(A313ContratoDadosCertame_DataAdjudicacao, "99/99/99"));
               }
               else
               {
                  A313ContratoDadosCertame_DataAdjudicacao = context.localUtil.CToD( cgiGet( edtContratoDadosCertame_DataAdjudicacao_Internalname), 2);
                  n313ContratoDadosCertame_DataAdjudicacao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A313ContratoDadosCertame_DataAdjudicacao", context.localUtil.Format(A313ContratoDadosCertame_DataAdjudicacao, "99/99/99"));
               }
               n313ContratoDadosCertame_DataAdjudicacao = ((DateTime.MinValue==A313ContratoDadosCertame_DataAdjudicacao) ? true : false);
               /* Read saved values. */
               Z314ContratoDadosCertame_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z314ContratoDadosCertame_Codigo"), ",", "."));
               Z307ContratoDadosCertame_Modalidade = cgiGet( "Z307ContratoDadosCertame_Modalidade");
               Z308ContratoDadosCertame_Numero = (long)(context.localUtil.CToN( cgiGet( "Z308ContratoDadosCertame_Numero"), ",", "."));
               n308ContratoDadosCertame_Numero = ((0==A308ContratoDadosCertame_Numero) ? true : false);
               Z309ContratoDadosCertame_Site = cgiGet( "Z309ContratoDadosCertame_Site");
               n309ContratoDadosCertame_Site = (String.IsNullOrEmpty(StringUtil.RTrim( A309ContratoDadosCertame_Site)) ? true : false);
               Z310ContratoDadosCertame_Uasg = (short)(context.localUtil.CToN( cgiGet( "Z310ContratoDadosCertame_Uasg"), ",", "."));
               n310ContratoDadosCertame_Uasg = ((0==A310ContratoDadosCertame_Uasg) ? true : false);
               Z311ContratoDadosCertame_Data = context.localUtil.CToD( cgiGet( "Z311ContratoDadosCertame_Data"), 0);
               Z312ContratoDadosCertame_DataHomologacao = context.localUtil.CToD( cgiGet( "Z312ContratoDadosCertame_DataHomologacao"), 0);
               n312ContratoDadosCertame_DataHomologacao = ((DateTime.MinValue==A312ContratoDadosCertame_DataHomologacao) ? true : false);
               Z313ContratoDadosCertame_DataAdjudicacao = context.localUtil.CToD( cgiGet( "Z313ContratoDadosCertame_DataAdjudicacao"), 0);
               n313ContratoDadosCertame_DataAdjudicacao = ((DateTime.MinValue==A313ContratoDadosCertame_DataAdjudicacao) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "N74Contrato_Codigo"), ",", "."));
               AV7ContratoDadosCertame_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATODADOSCERTAME_CODIGO"), ",", "."));
               A314ContratoDadosCertame_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATODADOSCERTAME_CODIGO"), ",", "."));
               AV11Insert_Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATO_CODIGO"), ",", "."));
               A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATO_CODIGO"), ",", "."));
               A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_CODIGO"), ",", "."));
               A40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_PESSOACOD"), ",", "."));
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratoDadosCertame";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A314ContratoDadosCertame_Codigo), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratodadoscertame:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contratodadoscertame:[SecurityCheckFailed value for]"+"ContratoDadosCertame_Codigo:"+context.localUtil.Format( (decimal)(A314ContratoDadosCertame_Codigo), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A314ContratoDadosCertame_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A314ContratoDadosCertame_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A314ContratoDadosCertame_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode52 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode52;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound52 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_1D0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E111D2 */
                           E111D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E121D2 */
                           E121D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E121D2 */
            E121D2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1D52( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1D52( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_1D0( )
      {
         BeforeValidate1D52( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1D52( ) ;
            }
            else
            {
               CheckExtendedTable1D52( ) ;
               CloseExtendedTableCursors1D52( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption1D0( )
      {
      }

      protected void E111D2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Contrato_Codigo") == 0 )
               {
                  AV11Insert_Contrato_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Contrato_Codigo), 6, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
      }

      protected void E121D2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratodadoscertame.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)A74Contrato_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM1D52( short GX_JID )
      {
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z307ContratoDadosCertame_Modalidade = T001D3_A307ContratoDadosCertame_Modalidade[0];
               Z308ContratoDadosCertame_Numero = T001D3_A308ContratoDadosCertame_Numero[0];
               Z309ContratoDadosCertame_Site = T001D3_A309ContratoDadosCertame_Site[0];
               Z310ContratoDadosCertame_Uasg = T001D3_A310ContratoDadosCertame_Uasg[0];
               Z311ContratoDadosCertame_Data = T001D3_A311ContratoDadosCertame_Data[0];
               Z312ContratoDadosCertame_DataHomologacao = T001D3_A312ContratoDadosCertame_DataHomologacao[0];
               Z313ContratoDadosCertame_DataAdjudicacao = T001D3_A313ContratoDadosCertame_DataAdjudicacao[0];
            }
            else
            {
               Z307ContratoDadosCertame_Modalidade = A307ContratoDadosCertame_Modalidade;
               Z308ContratoDadosCertame_Numero = A308ContratoDadosCertame_Numero;
               Z309ContratoDadosCertame_Site = A309ContratoDadosCertame_Site;
               Z310ContratoDadosCertame_Uasg = A310ContratoDadosCertame_Uasg;
               Z311ContratoDadosCertame_Data = A311ContratoDadosCertame_Data;
               Z312ContratoDadosCertame_DataHomologacao = A312ContratoDadosCertame_DataHomologacao;
               Z313ContratoDadosCertame_DataAdjudicacao = A313ContratoDadosCertame_DataAdjudicacao;
            }
         }
         if ( GX_JID == -12 )
         {
            Z74Contrato_Codigo = A74Contrato_Codigo;
            Z314ContratoDadosCertame_Codigo = A314ContratoDadosCertame_Codigo;
            Z307ContratoDadosCertame_Modalidade = A307ContratoDadosCertame_Modalidade;
            Z308ContratoDadosCertame_Numero = A308ContratoDadosCertame_Numero;
            Z309ContratoDadosCertame_Site = A309ContratoDadosCertame_Site;
            Z310ContratoDadosCertame_Uasg = A310ContratoDadosCertame_Uasg;
            Z311ContratoDadosCertame_Data = A311ContratoDadosCertame_Data;
            Z312ContratoDadosCertame_DataHomologacao = A312ContratoDadosCertame_DataHomologacao;
            Z313ContratoDadosCertame_DataAdjudicacao = A313ContratoDadosCertame_DataAdjudicacao;
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z77Contrato_Numero = A77Contrato_Numero;
            Z78Contrato_NumeroAta = A78Contrato_NumeroAta;
            Z79Contrato_Ano = A79Contrato_Ano;
            Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
            Z41Contratada_PessoaNom = A41Contratada_PessoaNom;
            Z42Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
         }
      }

      protected void standaloneNotModal( )
      {
         AV13Pgmname = "ContratoDadosCertame";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratoDadosCertame_Codigo) )
         {
            A314ContratoDadosCertame_Codigo = AV7ContratoDadosCertame_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A314ContratoDadosCertame_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A314ContratoDadosCertame_Codigo), 6, 0)));
         }
         /* Using cursor T001D4 */
         pr_default.execute(2, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A39Contratada_Codigo = T001D4_A39Contratada_Codigo[0];
         A77Contrato_Numero = T001D4_A77Contrato_Numero[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
         A78Contrato_NumeroAta = T001D4_A78Contrato_NumeroAta[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
         n78Contrato_NumeroAta = T001D4_n78Contrato_NumeroAta[0];
         A79Contrato_Ano = T001D4_A79Contrato_Ano[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
         pr_default.close(2);
         /* Using cursor T001D5 */
         pr_default.execute(3, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A40Contratada_PessoaCod = T001D5_A40Contratada_PessoaCod[0];
         pr_default.close(3);
         /* Using cursor T001D6 */
         pr_default.execute(4, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = T001D6_A41Contratada_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         n41Contratada_PessoaNom = T001D6_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = T001D6_A42Contratada_PessoaCNPJ[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
         n42Contratada_PessoaCNPJ = T001D6_n42Contratada_PessoaCNPJ[0];
         pr_default.close(4);
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
            {
               A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            }
         }
      }

      protected void Load1D52( )
      {
         /* Using cursor T001D7 */
         pr_default.execute(5, new Object[] {A74Contrato_Codigo, A314ContratoDadosCertame_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound52 = 1;
            A39Contratada_Codigo = T001D7_A39Contratada_Codigo[0];
            A77Contrato_Numero = T001D7_A77Contrato_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            A78Contrato_NumeroAta = T001D7_A78Contrato_NumeroAta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
            n78Contrato_NumeroAta = T001D7_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = T001D7_A79Contrato_Ano[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
            A41Contratada_PessoaNom = T001D7_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T001D7_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T001D7_A42Contratada_PessoaCNPJ[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            n42Contratada_PessoaCNPJ = T001D7_n42Contratada_PessoaCNPJ[0];
            A307ContratoDadosCertame_Modalidade = T001D7_A307ContratoDadosCertame_Modalidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A307ContratoDadosCertame_Modalidade", A307ContratoDadosCertame_Modalidade);
            A308ContratoDadosCertame_Numero = T001D7_A308ContratoDadosCertame_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A308ContratoDadosCertame_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(A308ContratoDadosCertame_Numero), 10, 0)));
            n308ContratoDadosCertame_Numero = T001D7_n308ContratoDadosCertame_Numero[0];
            A309ContratoDadosCertame_Site = T001D7_A309ContratoDadosCertame_Site[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A309ContratoDadosCertame_Site", A309ContratoDadosCertame_Site);
            n309ContratoDadosCertame_Site = T001D7_n309ContratoDadosCertame_Site[0];
            A310ContratoDadosCertame_Uasg = T001D7_A310ContratoDadosCertame_Uasg[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A310ContratoDadosCertame_Uasg", StringUtil.LTrim( StringUtil.Str( (decimal)(A310ContratoDadosCertame_Uasg), 4, 0)));
            n310ContratoDadosCertame_Uasg = T001D7_n310ContratoDadosCertame_Uasg[0];
            A311ContratoDadosCertame_Data = T001D7_A311ContratoDadosCertame_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A311ContratoDadosCertame_Data", context.localUtil.Format(A311ContratoDadosCertame_Data, "99/99/99"));
            A312ContratoDadosCertame_DataHomologacao = T001D7_A312ContratoDadosCertame_DataHomologacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A312ContratoDadosCertame_DataHomologacao", context.localUtil.Format(A312ContratoDadosCertame_DataHomologacao, "99/99/99"));
            n312ContratoDadosCertame_DataHomologacao = T001D7_n312ContratoDadosCertame_DataHomologacao[0];
            A313ContratoDadosCertame_DataAdjudicacao = T001D7_A313ContratoDadosCertame_DataAdjudicacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A313ContratoDadosCertame_DataAdjudicacao", context.localUtil.Format(A313ContratoDadosCertame_DataAdjudicacao, "99/99/99"));
            n313ContratoDadosCertame_DataAdjudicacao = T001D7_n313ContratoDadosCertame_DataAdjudicacao[0];
            A40Contratada_PessoaCod = T001D7_A40Contratada_PessoaCod[0];
            ZM1D52( -12) ;
         }
         pr_default.close(5);
         OnLoadActions1D52( ) ;
      }

      protected void OnLoadActions1D52( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
         {
            A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
      }

      protected void CheckExtendedTable1D52( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
         {
            A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A307ContratoDadosCertame_Modalidade)) )
         {
            GX_msglist.addItem("Modalidade � obrigat�rio.", 1, "CONTRATODADOSCERTAME_MODALIDADE");
            AnyError = 1;
            GX_FocusControl = edtContratoDadosCertame_Modalidade_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (0==A308ContratoDadosCertame_Numero) )
         {
            GX_msglist.addItem("Numero � obrigat�rio.", 1, "CONTRATODADOSCERTAME_NUMERO");
            AnyError = 1;
            GX_FocusControl = edtContratoDadosCertame_Numero_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A311ContratoDadosCertame_Data) || ( A311ContratoDadosCertame_Data >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data do certame fora do intervalo", "OutOfRange", 1, "CONTRATODADOSCERTAME_DATA");
            AnyError = 1;
            GX_FocusControl = edtContratoDadosCertame_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (DateTime.MinValue==A311ContratoDadosCertame_Data) )
         {
            GX_msglist.addItem("Data do certame � obrigat�rio.", 1, "CONTRATODADOSCERTAME_DATA");
            AnyError = 1;
            GX_FocusControl = edtContratoDadosCertame_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A312ContratoDadosCertame_DataHomologacao) || ( A312ContratoDadosCertame_DataHomologacao >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data de homologa��o fora do intervalo", "OutOfRange", 1, "CONTRATODADOSCERTAME_DATAHOMOLOGACAO");
            AnyError = 1;
            GX_FocusControl = edtContratoDadosCertame_DataHomologacao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A313ContratoDadosCertame_DataAdjudicacao) || ( A313ContratoDadosCertame_DataAdjudicacao >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data de adjudica��o fora do intervalo", "OutOfRange", 1, "CONTRATODADOSCERTAME_DATAADJUDICACAO");
            AnyError = 1;
            GX_FocusControl = edtContratoDadosCertame_DataAdjudicacao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors1D52( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1D52( )
      {
         /* Using cursor T001D8 */
         pr_default.execute(6, new Object[] {A314ContratoDadosCertame_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound52 = 1;
         }
         else
         {
            RcdFound52 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001D3 */
         pr_default.execute(1, new Object[] {A314ContratoDadosCertame_Codigo});
         if ( (pr_default.getStatus(1) != 101) && ( T001D3_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
         {
            ZM1D52( 12) ;
            RcdFound52 = 1;
            A314ContratoDadosCertame_Codigo = T001D3_A314ContratoDadosCertame_Codigo[0];
            A307ContratoDadosCertame_Modalidade = T001D3_A307ContratoDadosCertame_Modalidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A307ContratoDadosCertame_Modalidade", A307ContratoDadosCertame_Modalidade);
            A308ContratoDadosCertame_Numero = T001D3_A308ContratoDadosCertame_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A308ContratoDadosCertame_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(A308ContratoDadosCertame_Numero), 10, 0)));
            n308ContratoDadosCertame_Numero = T001D3_n308ContratoDadosCertame_Numero[0];
            A309ContratoDadosCertame_Site = T001D3_A309ContratoDadosCertame_Site[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A309ContratoDadosCertame_Site", A309ContratoDadosCertame_Site);
            n309ContratoDadosCertame_Site = T001D3_n309ContratoDadosCertame_Site[0];
            A310ContratoDadosCertame_Uasg = T001D3_A310ContratoDadosCertame_Uasg[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A310ContratoDadosCertame_Uasg", StringUtil.LTrim( StringUtil.Str( (decimal)(A310ContratoDadosCertame_Uasg), 4, 0)));
            n310ContratoDadosCertame_Uasg = T001D3_n310ContratoDadosCertame_Uasg[0];
            A311ContratoDadosCertame_Data = T001D3_A311ContratoDadosCertame_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A311ContratoDadosCertame_Data", context.localUtil.Format(A311ContratoDadosCertame_Data, "99/99/99"));
            A312ContratoDadosCertame_DataHomologacao = T001D3_A312ContratoDadosCertame_DataHomologacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A312ContratoDadosCertame_DataHomologacao", context.localUtil.Format(A312ContratoDadosCertame_DataHomologacao, "99/99/99"));
            n312ContratoDadosCertame_DataHomologacao = T001D3_n312ContratoDadosCertame_DataHomologacao[0];
            A313ContratoDadosCertame_DataAdjudicacao = T001D3_A313ContratoDadosCertame_DataAdjudicacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A313ContratoDadosCertame_DataAdjudicacao", context.localUtil.Format(A313ContratoDadosCertame_DataAdjudicacao, "99/99/99"));
            n313ContratoDadosCertame_DataAdjudicacao = T001D3_n313ContratoDadosCertame_DataAdjudicacao[0];
            Z314ContratoDadosCertame_Codigo = A314ContratoDadosCertame_Codigo;
            sMode52 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1D52( ) ;
            if ( AnyError == 1 )
            {
               RcdFound52 = 0;
               InitializeNonKey1D52( ) ;
            }
            Gx_mode = sMode52;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound52 = 0;
            InitializeNonKey1D52( ) ;
            sMode52 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode52;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1D52( ) ;
         if ( RcdFound52 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound52 = 0;
         /* Using cursor T001D9 */
         pr_default.execute(7, new Object[] {A314ContratoDadosCertame_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T001D9_A314ContratoDadosCertame_Codigo[0] < A314ContratoDadosCertame_Codigo ) ) && ( T001D9_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T001D9_A314ContratoDadosCertame_Codigo[0] > A314ContratoDadosCertame_Codigo ) ) && ( T001D9_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               A314ContratoDadosCertame_Codigo = T001D9_A314ContratoDadosCertame_Codigo[0];
               RcdFound52 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void move_previous( )
      {
         RcdFound52 = 0;
         /* Using cursor T001D10 */
         pr_default.execute(8, new Object[] {A314ContratoDadosCertame_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T001D10_A314ContratoDadosCertame_Codigo[0] > A314ContratoDadosCertame_Codigo ) ) && ( T001D10_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T001D10_A314ContratoDadosCertame_Codigo[0] < A314ContratoDadosCertame_Codigo ) ) && ( T001D10_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               A314ContratoDadosCertame_Codigo = T001D10_A314ContratoDadosCertame_Codigo[0];
               RcdFound52 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1D52( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContratoDadosCertame_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1D52( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound52 == 1 )
            {
               if ( A314ContratoDadosCertame_Codigo != Z314ContratoDadosCertame_Codigo )
               {
                  A314ContratoDadosCertame_Codigo = Z314ContratoDadosCertame_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A314ContratoDadosCertame_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A314ContratoDadosCertame_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContratoDadosCertame_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1D52( ) ;
                  GX_FocusControl = edtContratoDadosCertame_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A314ContratoDadosCertame_Codigo != Z314ContratoDadosCertame_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtContratoDadosCertame_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1D52( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContratoDadosCertame_Data_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1D52( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A314ContratoDadosCertame_Codigo != Z314ContratoDadosCertame_Codigo )
         {
            A314ContratoDadosCertame_Codigo = Z314ContratoDadosCertame_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A314ContratoDadosCertame_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A314ContratoDadosCertame_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContratoDadosCertame_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1D52( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001D2 */
            pr_default.execute(0, new Object[] {A314ContratoDadosCertame_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoDadosCertame"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z307ContratoDadosCertame_Modalidade, T001D2_A307ContratoDadosCertame_Modalidade[0]) != 0 ) || ( Z308ContratoDadosCertame_Numero != T001D2_A308ContratoDadosCertame_Numero[0] ) || ( StringUtil.StrCmp(Z309ContratoDadosCertame_Site, T001D2_A309ContratoDadosCertame_Site[0]) != 0 ) || ( Z310ContratoDadosCertame_Uasg != T001D2_A310ContratoDadosCertame_Uasg[0] ) || ( Z311ContratoDadosCertame_Data != T001D2_A311ContratoDadosCertame_Data[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z312ContratoDadosCertame_DataHomologacao != T001D2_A312ContratoDadosCertame_DataHomologacao[0] ) || ( Z313ContratoDadosCertame_DataAdjudicacao != T001D2_A313ContratoDadosCertame_DataAdjudicacao[0] ) )
            {
               if ( StringUtil.StrCmp(Z307ContratoDadosCertame_Modalidade, T001D2_A307ContratoDadosCertame_Modalidade[0]) != 0 )
               {
                  GXUtil.WriteLog("contratodadoscertame:[seudo value changed for attri]"+"ContratoDadosCertame_Modalidade");
                  GXUtil.WriteLogRaw("Old: ",Z307ContratoDadosCertame_Modalidade);
                  GXUtil.WriteLogRaw("Current: ",T001D2_A307ContratoDadosCertame_Modalidade[0]);
               }
               if ( Z308ContratoDadosCertame_Numero != T001D2_A308ContratoDadosCertame_Numero[0] )
               {
                  GXUtil.WriteLog("contratodadoscertame:[seudo value changed for attri]"+"ContratoDadosCertame_Numero");
                  GXUtil.WriteLogRaw("Old: ",Z308ContratoDadosCertame_Numero);
                  GXUtil.WriteLogRaw("Current: ",T001D2_A308ContratoDadosCertame_Numero[0]);
               }
               if ( StringUtil.StrCmp(Z309ContratoDadosCertame_Site, T001D2_A309ContratoDadosCertame_Site[0]) != 0 )
               {
                  GXUtil.WriteLog("contratodadoscertame:[seudo value changed for attri]"+"ContratoDadosCertame_Site");
                  GXUtil.WriteLogRaw("Old: ",Z309ContratoDadosCertame_Site);
                  GXUtil.WriteLogRaw("Current: ",T001D2_A309ContratoDadosCertame_Site[0]);
               }
               if ( Z310ContratoDadosCertame_Uasg != T001D2_A310ContratoDadosCertame_Uasg[0] )
               {
                  GXUtil.WriteLog("contratodadoscertame:[seudo value changed for attri]"+"ContratoDadosCertame_Uasg");
                  GXUtil.WriteLogRaw("Old: ",Z310ContratoDadosCertame_Uasg);
                  GXUtil.WriteLogRaw("Current: ",T001D2_A310ContratoDadosCertame_Uasg[0]);
               }
               if ( Z311ContratoDadosCertame_Data != T001D2_A311ContratoDadosCertame_Data[0] )
               {
                  GXUtil.WriteLog("contratodadoscertame:[seudo value changed for attri]"+"ContratoDadosCertame_Data");
                  GXUtil.WriteLogRaw("Old: ",Z311ContratoDadosCertame_Data);
                  GXUtil.WriteLogRaw("Current: ",T001D2_A311ContratoDadosCertame_Data[0]);
               }
               if ( Z312ContratoDadosCertame_DataHomologacao != T001D2_A312ContratoDadosCertame_DataHomologacao[0] )
               {
                  GXUtil.WriteLog("contratodadoscertame:[seudo value changed for attri]"+"ContratoDadosCertame_DataHomologacao");
                  GXUtil.WriteLogRaw("Old: ",Z312ContratoDadosCertame_DataHomologacao);
                  GXUtil.WriteLogRaw("Current: ",T001D2_A312ContratoDadosCertame_DataHomologacao[0]);
               }
               if ( Z313ContratoDadosCertame_DataAdjudicacao != T001D2_A313ContratoDadosCertame_DataAdjudicacao[0] )
               {
                  GXUtil.WriteLog("contratodadoscertame:[seudo value changed for attri]"+"ContratoDadosCertame_DataAdjudicacao");
                  GXUtil.WriteLogRaw("Old: ",Z313ContratoDadosCertame_DataAdjudicacao);
                  GXUtil.WriteLogRaw("Current: ",T001D2_A313ContratoDadosCertame_DataAdjudicacao[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoDadosCertame"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1D52( )
      {
         BeforeValidate1D52( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1D52( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1D52( 0) ;
            CheckOptimisticConcurrency1D52( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1D52( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1D52( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001D11 */
                     pr_default.execute(9, new Object[] {A74Contrato_Codigo, A307ContratoDadosCertame_Modalidade, n308ContratoDadosCertame_Numero, A308ContratoDadosCertame_Numero, n309ContratoDadosCertame_Site, A309ContratoDadosCertame_Site, n310ContratoDadosCertame_Uasg, A310ContratoDadosCertame_Uasg, A311ContratoDadosCertame_Data, n312ContratoDadosCertame_DataHomologacao, A312ContratoDadosCertame_DataHomologacao, n313ContratoDadosCertame_DataAdjudicacao, A313ContratoDadosCertame_DataAdjudicacao});
                     A314ContratoDadosCertame_Codigo = T001D11_A314ContratoDadosCertame_Codigo[0];
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoDadosCertame") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1D0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1D52( ) ;
            }
            EndLevel1D52( ) ;
         }
         CloseExtendedTableCursors1D52( ) ;
      }

      protected void Update1D52( )
      {
         BeforeValidate1D52( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1D52( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1D52( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1D52( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1D52( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001D12 */
                     pr_default.execute(10, new Object[] {A74Contrato_Codigo, A307ContratoDadosCertame_Modalidade, n308ContratoDadosCertame_Numero, A308ContratoDadosCertame_Numero, n309ContratoDadosCertame_Site, A309ContratoDadosCertame_Site, n310ContratoDadosCertame_Uasg, A310ContratoDadosCertame_Uasg, A311ContratoDadosCertame_Data, n312ContratoDadosCertame_DataHomologacao, A312ContratoDadosCertame_DataHomologacao, n313ContratoDadosCertame_DataAdjudicacao, A313ContratoDadosCertame_DataAdjudicacao, A314ContratoDadosCertame_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoDadosCertame") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoDadosCertame"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1D52( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1D52( ) ;
         }
         CloseExtendedTableCursors1D52( ) ;
      }

      protected void DeferredUpdate1D52( )
      {
      }

      protected void delete( )
      {
         BeforeValidate1D52( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1D52( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1D52( ) ;
            AfterConfirm1D52( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1D52( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001D13 */
                  pr_default.execute(11, new Object[] {A314ContratoDadosCertame_Codigo});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoDadosCertame") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode52 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1D52( ) ;
         Gx_mode = sMode52;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1D52( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel1D52( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1D52( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "ContratoDadosCertame");
            if ( AnyError == 0 )
            {
               ConfirmValues1D0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "ContratoDadosCertame");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1D52( )
      {
         /* Scan By routine */
         /* Using cursor T001D14 */
         pr_default.execute(12, new Object[] {A74Contrato_Codigo});
         RcdFound52 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound52 = 1;
            A314ContratoDadosCertame_Codigo = T001D14_A314ContratoDadosCertame_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1D52( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound52 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound52 = 1;
            A314ContratoDadosCertame_Codigo = T001D14_A314ContratoDadosCertame_Codigo[0];
         }
      }

      protected void ScanEnd1D52( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm1D52( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1D52( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1D52( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1D52( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1D52( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1D52( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1D52( )
      {
         edtContrato_Numero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Numero_Enabled), 5, 0)));
         edtContrato_NumeroAta_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_NumeroAta_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_NumeroAta_Enabled), 5, 0)));
         edtContrato_Ano_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Ano_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Ano_Enabled), 5, 0)));
         edtContratada_PessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaNom_Enabled), 5, 0)));
         edtContratada_PessoaCNPJ_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCNPJ_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaCNPJ_Enabled), 5, 0)));
         edtContratoDadosCertame_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoDadosCertame_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoDadosCertame_Data_Enabled), 5, 0)));
         edtContratoDadosCertame_Modalidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoDadosCertame_Modalidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoDadosCertame_Modalidade_Enabled), 5, 0)));
         edtContratoDadosCertame_Numero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoDadosCertame_Numero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoDadosCertame_Numero_Enabled), 5, 0)));
         edtContratoDadosCertame_Site_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoDadosCertame_Site_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoDadosCertame_Site_Enabled), 5, 0)));
         edtContratoDadosCertame_Uasg_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoDadosCertame_Uasg_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoDadosCertame_Uasg_Enabled), 5, 0)));
         edtContratoDadosCertame_DataHomologacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoDadosCertame_DataHomologacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoDadosCertame_DataHomologacao_Enabled), 5, 0)));
         edtContratoDadosCertame_DataAdjudicacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoDadosCertame_DataAdjudicacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoDadosCertame_DataAdjudicacao_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1D0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117194385");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratodadoscertame.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoDadosCertame_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z314ContratoDadosCertame_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z314ContratoDadosCertame_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z307ContratoDadosCertame_Modalidade", Z307ContratoDadosCertame_Modalidade);
         GxWebStd.gx_hidden_field( context, "Z308ContratoDadosCertame_Numero", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z308ContratoDadosCertame_Numero), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z309ContratoDadosCertame_Site", Z309ContratoDadosCertame_Site);
         GxWebStd.gx_hidden_field( context, "Z310ContratoDadosCertame_Uasg", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z310ContratoDadosCertame_Uasg), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z311ContratoDadosCertame_Data", context.localUtil.DToC( Z311ContratoDadosCertame_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z312ContratoDadosCertame_DataHomologacao", context.localUtil.DToC( Z312ContratoDadosCertame_DataHomologacao, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z313ContratoDadosCertame_DataAdjudicacao", context.localUtil.DToC( Z313ContratoDadosCertame_DataAdjudicacao, 0, "/"));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N74Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATODADOSCERTAME_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoDadosCertame_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATODADOSCERTAME_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A314ContratoDadosCertame_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATODADOSCERTAME_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoDadosCertame_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratoDadosCertame";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A314ContratoDadosCertame_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratodadoscertame:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contratodadoscertame:[SendSecurityCheck value for]"+"ContratoDadosCertame_Codigo:"+context.localUtil.Format( (decimal)(A314ContratoDadosCertame_Codigo), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratodadoscertame.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoDadosCertame_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ContratoDadosCertame" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Dados Certame" ;
      }

      protected void InitializeNonKey1D52( )
      {
         A307ContratoDadosCertame_Modalidade = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A307ContratoDadosCertame_Modalidade", A307ContratoDadosCertame_Modalidade);
         A308ContratoDadosCertame_Numero = 0;
         n308ContratoDadosCertame_Numero = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A308ContratoDadosCertame_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(A308ContratoDadosCertame_Numero), 10, 0)));
         n308ContratoDadosCertame_Numero = ((0==A308ContratoDadosCertame_Numero) ? true : false);
         A309ContratoDadosCertame_Site = "";
         n309ContratoDadosCertame_Site = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A309ContratoDadosCertame_Site", A309ContratoDadosCertame_Site);
         n309ContratoDadosCertame_Site = (String.IsNullOrEmpty(StringUtil.RTrim( A309ContratoDadosCertame_Site)) ? true : false);
         A310ContratoDadosCertame_Uasg = 0;
         n310ContratoDadosCertame_Uasg = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A310ContratoDadosCertame_Uasg", StringUtil.LTrim( StringUtil.Str( (decimal)(A310ContratoDadosCertame_Uasg), 4, 0)));
         n310ContratoDadosCertame_Uasg = ((0==A310ContratoDadosCertame_Uasg) ? true : false);
         A311ContratoDadosCertame_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A311ContratoDadosCertame_Data", context.localUtil.Format(A311ContratoDadosCertame_Data, "99/99/99"));
         A312ContratoDadosCertame_DataHomologacao = DateTime.MinValue;
         n312ContratoDadosCertame_DataHomologacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A312ContratoDadosCertame_DataHomologacao", context.localUtil.Format(A312ContratoDadosCertame_DataHomologacao, "99/99/99"));
         n312ContratoDadosCertame_DataHomologacao = ((DateTime.MinValue==A312ContratoDadosCertame_DataHomologacao) ? true : false);
         A313ContratoDadosCertame_DataAdjudicacao = DateTime.MinValue;
         n313ContratoDadosCertame_DataAdjudicacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A313ContratoDadosCertame_DataAdjudicacao", context.localUtil.Format(A313ContratoDadosCertame_DataAdjudicacao, "99/99/99"));
         n313ContratoDadosCertame_DataAdjudicacao = ((DateTime.MinValue==A313ContratoDadosCertame_DataAdjudicacao) ? true : false);
         Z307ContratoDadosCertame_Modalidade = "";
         Z308ContratoDadosCertame_Numero = 0;
         Z309ContratoDadosCertame_Site = "";
         Z310ContratoDadosCertame_Uasg = 0;
         Z311ContratoDadosCertame_Data = DateTime.MinValue;
         Z312ContratoDadosCertame_DataHomologacao = DateTime.MinValue;
         Z313ContratoDadosCertame_DataAdjudicacao = DateTime.MinValue;
      }

      protected void InitAll1D52( )
      {
         A314ContratoDadosCertame_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A314ContratoDadosCertame_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A314ContratoDadosCertame_Codigo), 6, 0)));
         InitializeNonKey1D52( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117194413");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratodadoscertame.js", "?20203117194413");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontrato_numero_Internalname = "TEXTBLOCKCONTRATO_NUMERO";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         lblTextblockcontrato_numeroata_Internalname = "TEXTBLOCKCONTRATO_NUMEROATA";
         edtContrato_NumeroAta_Internalname = "CONTRATO_NUMEROATA";
         lblTextblockcontrato_ano_Internalname = "TEXTBLOCKCONTRATO_ANO";
         edtContrato_Ano_Internalname = "CONTRATO_ANO";
         tblTablemergedcontrato_numero_Internalname = "TABLEMERGEDCONTRATO_NUMERO";
         lblTextblockcontratada_pessoanom_Internalname = "TEXTBLOCKCONTRATADA_PESSOANOM";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         lblTextblockcontratada_pessoacnpj_Internalname = "TEXTBLOCKCONTRATADA_PESSOACNPJ";
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ";
         tblTablemergedcontratada_pessoanom_Internalname = "TABLEMERGEDCONTRATADA_PESSOANOM";
         tblContrato_Internalname = "CONTRATO";
         grpUnnamedgroup1_Internalname = "UNNAMEDGROUP1";
         lblTextblockcontratodadoscertame_data_Internalname = "TEXTBLOCKCONTRATODADOSCERTAME_DATA";
         edtContratoDadosCertame_Data_Internalname = "CONTRATODADOSCERTAME_DATA";
         lblTextblockcontratodadoscertame_modalidade_Internalname = "TEXTBLOCKCONTRATODADOSCERTAME_MODALIDADE";
         edtContratoDadosCertame_Modalidade_Internalname = "CONTRATODADOSCERTAME_MODALIDADE";
         lblTextblockcontratodadoscertame_numero_Internalname = "TEXTBLOCKCONTRATODADOSCERTAME_NUMERO";
         edtContratoDadosCertame_Numero_Internalname = "CONTRATODADOSCERTAME_NUMERO";
         lblTextblockcontratodadoscertame_site_Internalname = "TEXTBLOCKCONTRATODADOSCERTAME_SITE";
         edtContratoDadosCertame_Site_Internalname = "CONTRATODADOSCERTAME_SITE";
         lblTextblockcontratodadoscertame_uasg_Internalname = "TEXTBLOCKCONTRATODADOSCERTAME_UASG";
         edtContratoDadosCertame_Uasg_Internalname = "CONTRATODADOSCERTAME_UASG";
         lblTextblockcontratodadoscertame_datahomologacao_Internalname = "TEXTBLOCKCONTRATODADOSCERTAME_DATAHOMOLOGACAO";
         edtContratoDadosCertame_DataHomologacao_Internalname = "CONTRATODADOSCERTAME_DATAHOMOLOGACAO";
         lblTextblockcontratodadoscertame_dataadjudicacao_Internalname = "TEXTBLOCKCONTRATODADOSCERTAME_DATAADJUDICACAO";
         edtContratoDadosCertame_DataAdjudicacao_Internalname = "CONTRATODADOSCERTAME_DATAADJUDICACAO";
         tblTablemergedcontratodadoscertame_datahomologacao_Internalname = "TABLEMERGEDCONTRATODADOSCERTAME_DATAHOMOLOGACAO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Certame";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contrato Dados Certame";
         edtContrato_Ano_Jsonclick = "";
         edtContrato_Ano_Enabled = 0;
         edtContrato_NumeroAta_Jsonclick = "";
         edtContrato_NumeroAta_Enabled = 0;
         edtContrato_Numero_Jsonclick = "";
         edtContrato_Numero_Enabled = 0;
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaCNPJ_Enabled = 0;
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratada_PessoaNom_Enabled = 0;
         edtContratoDadosCertame_DataAdjudicacao_Jsonclick = "";
         edtContratoDadosCertame_DataAdjudicacao_Enabled = 1;
         edtContratoDadosCertame_DataHomologacao_Jsonclick = "";
         edtContratoDadosCertame_DataHomologacao_Enabled = 1;
         edtContratoDadosCertame_Uasg_Jsonclick = "";
         edtContratoDadosCertame_Uasg_Enabled = 1;
         edtContratoDadosCertame_Site_Jsonclick = "";
         edtContratoDadosCertame_Site_Enabled = 1;
         edtContratoDadosCertame_Numero_Jsonclick = "";
         edtContratoDadosCertame_Numero_Enabled = 1;
         edtContratoDadosCertame_Modalidade_Jsonclick = "";
         edtContratoDadosCertame_Modalidade_Enabled = 1;
         edtContratoDadosCertame_Data_Jsonclick = "";
         edtContratoDadosCertame_Data_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratoDadosCertame_Codigo',fld:'vCONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E121D2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z307ContratoDadosCertame_Modalidade = "";
         Z309ContratoDadosCertame_Site = "";
         Z311ContratoDadosCertame_Data = DateTime.MinValue;
         Z312ContratoDadosCertame_DataHomologacao = DateTime.MinValue;
         Z313ContratoDadosCertame_DataAdjudicacao = DateTime.MinValue;
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontratodadoscertame_data_Jsonclick = "";
         A311ContratoDadosCertame_Data = DateTime.MinValue;
         lblTextblockcontratodadoscertame_modalidade_Jsonclick = "";
         A307ContratoDadosCertame_Modalidade = "";
         lblTextblockcontratodadoscertame_numero_Jsonclick = "";
         lblTextblockcontratodadoscertame_site_Jsonclick = "";
         A309ContratoDadosCertame_Site = "";
         lblTextblockcontratodadoscertame_uasg_Jsonclick = "";
         lblTextblockcontratodadoscertame_datahomologacao_Jsonclick = "";
         A312ContratoDadosCertame_DataHomologacao = DateTime.MinValue;
         lblTextblockcontratodadoscertame_dataadjudicacao_Jsonclick = "";
         A313ContratoDadosCertame_DataAdjudicacao = DateTime.MinValue;
         lblTextblockcontrato_numero_Jsonclick = "";
         lblTextblockcontratada_pessoanom_Jsonclick = "";
         A41Contratada_PessoaNom = "";
         lblTextblockcontratada_pessoacnpj_Jsonclick = "";
         A42Contratada_PessoaCNPJ = "";
         A77Contrato_Numero = "";
         lblTextblockcontrato_numeroata_Jsonclick = "";
         A78Contrato_NumeroAta = "";
         lblTextblockcontrato_ano_Jsonclick = "";
         AV13Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode52 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z77Contrato_Numero = "";
         Z78Contrato_NumeroAta = "";
         Z41Contratada_PessoaNom = "";
         Z42Contratada_PessoaCNPJ = "";
         T001D4_A39Contratada_Codigo = new int[1] ;
         T001D4_A77Contrato_Numero = new String[] {""} ;
         T001D4_A78Contrato_NumeroAta = new String[] {""} ;
         T001D4_n78Contrato_NumeroAta = new bool[] {false} ;
         T001D4_A79Contrato_Ano = new short[1] ;
         T001D5_A40Contratada_PessoaCod = new int[1] ;
         T001D6_A41Contratada_PessoaNom = new String[] {""} ;
         T001D6_n41Contratada_PessoaNom = new bool[] {false} ;
         T001D6_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T001D6_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T001D7_A39Contratada_Codigo = new int[1] ;
         T001D7_A74Contrato_Codigo = new int[1] ;
         T001D7_A314ContratoDadosCertame_Codigo = new int[1] ;
         T001D7_A77Contrato_Numero = new String[] {""} ;
         T001D7_A78Contrato_NumeroAta = new String[] {""} ;
         T001D7_n78Contrato_NumeroAta = new bool[] {false} ;
         T001D7_A79Contrato_Ano = new short[1] ;
         T001D7_A41Contratada_PessoaNom = new String[] {""} ;
         T001D7_n41Contratada_PessoaNom = new bool[] {false} ;
         T001D7_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T001D7_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T001D7_A307ContratoDadosCertame_Modalidade = new String[] {""} ;
         T001D7_A308ContratoDadosCertame_Numero = new long[1] ;
         T001D7_n308ContratoDadosCertame_Numero = new bool[] {false} ;
         T001D7_A309ContratoDadosCertame_Site = new String[] {""} ;
         T001D7_n309ContratoDadosCertame_Site = new bool[] {false} ;
         T001D7_A310ContratoDadosCertame_Uasg = new short[1] ;
         T001D7_n310ContratoDadosCertame_Uasg = new bool[] {false} ;
         T001D7_A311ContratoDadosCertame_Data = new DateTime[] {DateTime.MinValue} ;
         T001D7_A312ContratoDadosCertame_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         T001D7_n312ContratoDadosCertame_DataHomologacao = new bool[] {false} ;
         T001D7_A313ContratoDadosCertame_DataAdjudicacao = new DateTime[] {DateTime.MinValue} ;
         T001D7_n313ContratoDadosCertame_DataAdjudicacao = new bool[] {false} ;
         T001D7_A40Contratada_PessoaCod = new int[1] ;
         T001D8_A314ContratoDadosCertame_Codigo = new int[1] ;
         T001D3_A74Contrato_Codigo = new int[1] ;
         T001D3_A314ContratoDadosCertame_Codigo = new int[1] ;
         T001D3_A307ContratoDadosCertame_Modalidade = new String[] {""} ;
         T001D3_A308ContratoDadosCertame_Numero = new long[1] ;
         T001D3_n308ContratoDadosCertame_Numero = new bool[] {false} ;
         T001D3_A309ContratoDadosCertame_Site = new String[] {""} ;
         T001D3_n309ContratoDadosCertame_Site = new bool[] {false} ;
         T001D3_A310ContratoDadosCertame_Uasg = new short[1] ;
         T001D3_n310ContratoDadosCertame_Uasg = new bool[] {false} ;
         T001D3_A311ContratoDadosCertame_Data = new DateTime[] {DateTime.MinValue} ;
         T001D3_A312ContratoDadosCertame_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         T001D3_n312ContratoDadosCertame_DataHomologacao = new bool[] {false} ;
         T001D3_A313ContratoDadosCertame_DataAdjudicacao = new DateTime[] {DateTime.MinValue} ;
         T001D3_n313ContratoDadosCertame_DataAdjudicacao = new bool[] {false} ;
         T001D9_A74Contrato_Codigo = new int[1] ;
         T001D9_A314ContratoDadosCertame_Codigo = new int[1] ;
         T001D10_A74Contrato_Codigo = new int[1] ;
         T001D10_A314ContratoDadosCertame_Codigo = new int[1] ;
         T001D2_A74Contrato_Codigo = new int[1] ;
         T001D2_A314ContratoDadosCertame_Codigo = new int[1] ;
         T001D2_A307ContratoDadosCertame_Modalidade = new String[] {""} ;
         T001D2_A308ContratoDadosCertame_Numero = new long[1] ;
         T001D2_n308ContratoDadosCertame_Numero = new bool[] {false} ;
         T001D2_A309ContratoDadosCertame_Site = new String[] {""} ;
         T001D2_n309ContratoDadosCertame_Site = new bool[] {false} ;
         T001D2_A310ContratoDadosCertame_Uasg = new short[1] ;
         T001D2_n310ContratoDadosCertame_Uasg = new bool[] {false} ;
         T001D2_A311ContratoDadosCertame_Data = new DateTime[] {DateTime.MinValue} ;
         T001D2_A312ContratoDadosCertame_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         T001D2_n312ContratoDadosCertame_DataHomologacao = new bool[] {false} ;
         T001D2_A313ContratoDadosCertame_DataAdjudicacao = new DateTime[] {DateTime.MinValue} ;
         T001D2_n313ContratoDadosCertame_DataAdjudicacao = new bool[] {false} ;
         T001D11_A314ContratoDadosCertame_Codigo = new int[1] ;
         T001D14_A314ContratoDadosCertame_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratodadoscertame__default(),
            new Object[][] {
                new Object[] {
               T001D2_A74Contrato_Codigo, T001D2_A314ContratoDadosCertame_Codigo, T001D2_A307ContratoDadosCertame_Modalidade, T001D2_A308ContratoDadosCertame_Numero, T001D2_n308ContratoDadosCertame_Numero, T001D2_A309ContratoDadosCertame_Site, T001D2_n309ContratoDadosCertame_Site, T001D2_A310ContratoDadosCertame_Uasg, T001D2_n310ContratoDadosCertame_Uasg, T001D2_A311ContratoDadosCertame_Data,
               T001D2_A312ContratoDadosCertame_DataHomologacao, T001D2_n312ContratoDadosCertame_DataHomologacao, T001D2_A313ContratoDadosCertame_DataAdjudicacao, T001D2_n313ContratoDadosCertame_DataAdjudicacao
               }
               , new Object[] {
               T001D3_A74Contrato_Codigo, T001D3_A314ContratoDadosCertame_Codigo, T001D3_A307ContratoDadosCertame_Modalidade, T001D3_A308ContratoDadosCertame_Numero, T001D3_n308ContratoDadosCertame_Numero, T001D3_A309ContratoDadosCertame_Site, T001D3_n309ContratoDadosCertame_Site, T001D3_A310ContratoDadosCertame_Uasg, T001D3_n310ContratoDadosCertame_Uasg, T001D3_A311ContratoDadosCertame_Data,
               T001D3_A312ContratoDadosCertame_DataHomologacao, T001D3_n312ContratoDadosCertame_DataHomologacao, T001D3_A313ContratoDadosCertame_DataAdjudicacao, T001D3_n313ContratoDadosCertame_DataAdjudicacao
               }
               , new Object[] {
               T001D4_A39Contratada_Codigo, T001D4_A77Contrato_Numero, T001D4_A78Contrato_NumeroAta, T001D4_n78Contrato_NumeroAta, T001D4_A79Contrato_Ano
               }
               , new Object[] {
               T001D5_A40Contratada_PessoaCod
               }
               , new Object[] {
               T001D6_A41Contratada_PessoaNom, T001D6_n41Contratada_PessoaNom, T001D6_A42Contratada_PessoaCNPJ, T001D6_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               T001D7_A39Contratada_Codigo, T001D7_A74Contrato_Codigo, T001D7_A314ContratoDadosCertame_Codigo, T001D7_A77Contrato_Numero, T001D7_A78Contrato_NumeroAta, T001D7_n78Contrato_NumeroAta, T001D7_A79Contrato_Ano, T001D7_A41Contratada_PessoaNom, T001D7_n41Contratada_PessoaNom, T001D7_A42Contratada_PessoaCNPJ,
               T001D7_n42Contratada_PessoaCNPJ, T001D7_A307ContratoDadosCertame_Modalidade, T001D7_A308ContratoDadosCertame_Numero, T001D7_n308ContratoDadosCertame_Numero, T001D7_A309ContratoDadosCertame_Site, T001D7_n309ContratoDadosCertame_Site, T001D7_A310ContratoDadosCertame_Uasg, T001D7_n310ContratoDadosCertame_Uasg, T001D7_A311ContratoDadosCertame_Data, T001D7_A312ContratoDadosCertame_DataHomologacao,
               T001D7_n312ContratoDadosCertame_DataHomologacao, T001D7_A313ContratoDadosCertame_DataAdjudicacao, T001D7_n313ContratoDadosCertame_DataAdjudicacao, T001D7_A40Contratada_PessoaCod
               }
               , new Object[] {
               T001D8_A314ContratoDadosCertame_Codigo
               }
               , new Object[] {
               T001D9_A74Contrato_Codigo, T001D9_A314ContratoDadosCertame_Codigo
               }
               , new Object[] {
               T001D10_A74Contrato_Codigo, T001D10_A314ContratoDadosCertame_Codigo
               }
               , new Object[] {
               T001D11_A314ContratoDadosCertame_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001D14_A314ContratoDadosCertame_Codigo
               }
            }
         );
         N74Contrato_Codigo = 0;
         Z74Contrato_Codigo = 0;
         A74Contrato_Codigo = 0;
         AV13Pgmname = "ContratoDadosCertame";
      }

      private short Z310ContratoDadosCertame_Uasg ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A310ContratoDadosCertame_Uasg ;
      private short A79Contrato_Ano ;
      private short RcdFound52 ;
      private short GX_JID ;
      private short Z79Contrato_Ano ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private int wcpOAV7ContratoDadosCertame_Codigo ;
      private int wcpOA74Contrato_Codigo ;
      private int Z314ContratoDadosCertame_Codigo ;
      private int N74Contrato_Codigo ;
      private int AV7ContratoDadosCertame_Codigo ;
      private int A74Contrato_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContratoDadosCertame_Data_Enabled ;
      private int edtContratoDadosCertame_Modalidade_Enabled ;
      private int edtContratoDadosCertame_Numero_Enabled ;
      private int edtContratoDadosCertame_Site_Enabled ;
      private int edtContratoDadosCertame_Uasg_Enabled ;
      private int edtContratoDadosCertame_DataHomologacao_Enabled ;
      private int edtContratoDadosCertame_DataAdjudicacao_Enabled ;
      private int edtContratada_PessoaNom_Enabled ;
      private int edtContratada_PessoaCNPJ_Enabled ;
      private int edtContrato_Numero_Enabled ;
      private int edtContrato_NumeroAta_Enabled ;
      private int edtContrato_Ano_Enabled ;
      private int A314ContratoDadosCertame_Codigo ;
      private int AV11Insert_Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int AV14GXV1 ;
      private int Z74Contrato_Codigo ;
      private int Z39Contratada_Codigo ;
      private int Z40Contratada_PessoaCod ;
      private int idxLst ;
      private long Z308ContratoDadosCertame_Numero ;
      private long A308ContratoDadosCertame_Numero ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContratoDadosCertame_Data_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String grpUnnamedgroup1_Internalname ;
      private String lblTextblockcontratodadoscertame_data_Internalname ;
      private String lblTextblockcontratodadoscertame_data_Jsonclick ;
      private String edtContratoDadosCertame_Data_Jsonclick ;
      private String lblTextblockcontratodadoscertame_modalidade_Internalname ;
      private String lblTextblockcontratodadoscertame_modalidade_Jsonclick ;
      private String edtContratoDadosCertame_Modalidade_Internalname ;
      private String edtContratoDadosCertame_Modalidade_Jsonclick ;
      private String lblTextblockcontratodadoscertame_numero_Internalname ;
      private String lblTextblockcontratodadoscertame_numero_Jsonclick ;
      private String edtContratoDadosCertame_Numero_Internalname ;
      private String edtContratoDadosCertame_Numero_Jsonclick ;
      private String lblTextblockcontratodadoscertame_site_Internalname ;
      private String lblTextblockcontratodadoscertame_site_Jsonclick ;
      private String edtContratoDadosCertame_Site_Internalname ;
      private String edtContratoDadosCertame_Site_Jsonclick ;
      private String lblTextblockcontratodadoscertame_uasg_Internalname ;
      private String lblTextblockcontratodadoscertame_uasg_Jsonclick ;
      private String edtContratoDadosCertame_Uasg_Internalname ;
      private String edtContratoDadosCertame_Uasg_Jsonclick ;
      private String lblTextblockcontratodadoscertame_datahomologacao_Internalname ;
      private String lblTextblockcontratodadoscertame_datahomologacao_Jsonclick ;
      private String tblTablemergedcontratodadoscertame_datahomologacao_Internalname ;
      private String edtContratoDadosCertame_DataHomologacao_Internalname ;
      private String edtContratoDadosCertame_DataHomologacao_Jsonclick ;
      private String lblTextblockcontratodadoscertame_dataadjudicacao_Internalname ;
      private String lblTextblockcontratodadoscertame_dataadjudicacao_Jsonclick ;
      private String edtContratoDadosCertame_DataAdjudicacao_Internalname ;
      private String edtContratoDadosCertame_DataAdjudicacao_Jsonclick ;
      private String tblContrato_Internalname ;
      private String lblTextblockcontrato_numero_Internalname ;
      private String lblTextblockcontrato_numero_Jsonclick ;
      private String lblTextblockcontratada_pessoanom_Internalname ;
      private String lblTextblockcontratada_pessoanom_Jsonclick ;
      private String tblTablemergedcontratada_pessoanom_Internalname ;
      private String edtContratada_PessoaNom_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String lblTextblockcontratada_pessoacnpj_Internalname ;
      private String lblTextblockcontratada_pessoacnpj_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String tblTablemergedcontrato_numero_Internalname ;
      private String edtContrato_Numero_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Jsonclick ;
      private String lblTextblockcontrato_numeroata_Internalname ;
      private String lblTextblockcontrato_numeroata_Jsonclick ;
      private String edtContrato_NumeroAta_Internalname ;
      private String A78Contrato_NumeroAta ;
      private String edtContrato_NumeroAta_Jsonclick ;
      private String lblTextblockcontrato_ano_Internalname ;
      private String lblTextblockcontrato_ano_Jsonclick ;
      private String edtContrato_Ano_Internalname ;
      private String edtContrato_Ano_Jsonclick ;
      private String AV13Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode52 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z77Contrato_Numero ;
      private String Z78Contrato_NumeroAta ;
      private String Z41Contratada_PessoaNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime Z311ContratoDadosCertame_Data ;
      private DateTime Z312ContratoDadosCertame_DataHomologacao ;
      private DateTime Z313ContratoDadosCertame_DataAdjudicacao ;
      private DateTime A311ContratoDadosCertame_Data ;
      private DateTime A312ContratoDadosCertame_DataHomologacao ;
      private DateTime A313ContratoDadosCertame_DataAdjudicacao ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n78Contrato_NumeroAta ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n308ContratoDadosCertame_Numero ;
      private bool n309ContratoDadosCertame_Site ;
      private bool n310ContratoDadosCertame_Uasg ;
      private bool n312ContratoDadosCertame_DataHomologacao ;
      private bool n313ContratoDadosCertame_DataAdjudicacao ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String Z307ContratoDadosCertame_Modalidade ;
      private String Z309ContratoDadosCertame_Site ;
      private String A307ContratoDadosCertame_Modalidade ;
      private String A309ContratoDadosCertame_Site ;
      private String A42Contratada_PessoaCNPJ ;
      private String Z42Contratada_PessoaCNPJ ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_Contrato_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] T001D4_A39Contratada_Codigo ;
      private String[] T001D4_A77Contrato_Numero ;
      private String[] T001D4_A78Contrato_NumeroAta ;
      private bool[] T001D4_n78Contrato_NumeroAta ;
      private short[] T001D4_A79Contrato_Ano ;
      private int[] T001D5_A40Contratada_PessoaCod ;
      private String[] T001D6_A41Contratada_PessoaNom ;
      private bool[] T001D6_n41Contratada_PessoaNom ;
      private String[] T001D6_A42Contratada_PessoaCNPJ ;
      private bool[] T001D6_n42Contratada_PessoaCNPJ ;
      private int[] T001D7_A39Contratada_Codigo ;
      private int[] T001D7_A74Contrato_Codigo ;
      private int[] T001D7_A314ContratoDadosCertame_Codigo ;
      private String[] T001D7_A77Contrato_Numero ;
      private String[] T001D7_A78Contrato_NumeroAta ;
      private bool[] T001D7_n78Contrato_NumeroAta ;
      private short[] T001D7_A79Contrato_Ano ;
      private String[] T001D7_A41Contratada_PessoaNom ;
      private bool[] T001D7_n41Contratada_PessoaNom ;
      private String[] T001D7_A42Contratada_PessoaCNPJ ;
      private bool[] T001D7_n42Contratada_PessoaCNPJ ;
      private String[] T001D7_A307ContratoDadosCertame_Modalidade ;
      private long[] T001D7_A308ContratoDadosCertame_Numero ;
      private bool[] T001D7_n308ContratoDadosCertame_Numero ;
      private String[] T001D7_A309ContratoDadosCertame_Site ;
      private bool[] T001D7_n309ContratoDadosCertame_Site ;
      private short[] T001D7_A310ContratoDadosCertame_Uasg ;
      private bool[] T001D7_n310ContratoDadosCertame_Uasg ;
      private DateTime[] T001D7_A311ContratoDadosCertame_Data ;
      private DateTime[] T001D7_A312ContratoDadosCertame_DataHomologacao ;
      private bool[] T001D7_n312ContratoDadosCertame_DataHomologacao ;
      private DateTime[] T001D7_A313ContratoDadosCertame_DataAdjudicacao ;
      private bool[] T001D7_n313ContratoDadosCertame_DataAdjudicacao ;
      private int[] T001D7_A40Contratada_PessoaCod ;
      private int[] T001D8_A314ContratoDadosCertame_Codigo ;
      private int[] T001D3_A74Contrato_Codigo ;
      private int[] T001D3_A314ContratoDadosCertame_Codigo ;
      private String[] T001D3_A307ContratoDadosCertame_Modalidade ;
      private long[] T001D3_A308ContratoDadosCertame_Numero ;
      private bool[] T001D3_n308ContratoDadosCertame_Numero ;
      private String[] T001D3_A309ContratoDadosCertame_Site ;
      private bool[] T001D3_n309ContratoDadosCertame_Site ;
      private short[] T001D3_A310ContratoDadosCertame_Uasg ;
      private bool[] T001D3_n310ContratoDadosCertame_Uasg ;
      private DateTime[] T001D3_A311ContratoDadosCertame_Data ;
      private DateTime[] T001D3_A312ContratoDadosCertame_DataHomologacao ;
      private bool[] T001D3_n312ContratoDadosCertame_DataHomologacao ;
      private DateTime[] T001D3_A313ContratoDadosCertame_DataAdjudicacao ;
      private bool[] T001D3_n313ContratoDadosCertame_DataAdjudicacao ;
      private int[] T001D9_A74Contrato_Codigo ;
      private int[] T001D9_A314ContratoDadosCertame_Codigo ;
      private int[] T001D10_A74Contrato_Codigo ;
      private int[] T001D10_A314ContratoDadosCertame_Codigo ;
      private int[] T001D2_A74Contrato_Codigo ;
      private int[] T001D2_A314ContratoDadosCertame_Codigo ;
      private String[] T001D2_A307ContratoDadosCertame_Modalidade ;
      private long[] T001D2_A308ContratoDadosCertame_Numero ;
      private bool[] T001D2_n308ContratoDadosCertame_Numero ;
      private String[] T001D2_A309ContratoDadosCertame_Site ;
      private bool[] T001D2_n309ContratoDadosCertame_Site ;
      private short[] T001D2_A310ContratoDadosCertame_Uasg ;
      private bool[] T001D2_n310ContratoDadosCertame_Uasg ;
      private DateTime[] T001D2_A311ContratoDadosCertame_Data ;
      private DateTime[] T001D2_A312ContratoDadosCertame_DataHomologacao ;
      private bool[] T001D2_n312ContratoDadosCertame_DataHomologacao ;
      private DateTime[] T001D2_A313ContratoDadosCertame_DataAdjudicacao ;
      private bool[] T001D2_n313ContratoDadosCertame_DataAdjudicacao ;
      private int[] T001D11_A314ContratoDadosCertame_Codigo ;
      private int[] T001D14_A314ContratoDadosCertame_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class contratodadoscertame__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001D4 ;
          prmT001D4 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001D5 ;
          prmT001D5 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001D6 ;
          prmT001D6 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001D7 ;
          prmT001D7 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoDadosCertame_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001D8 ;
          prmT001D8 = new Object[] {
          new Object[] {"@ContratoDadosCertame_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001D3 ;
          prmT001D3 = new Object[] {
          new Object[] {"@ContratoDadosCertame_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001D9 ;
          prmT001D9 = new Object[] {
          new Object[] {"@ContratoDadosCertame_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001D10 ;
          prmT001D10 = new Object[] {
          new Object[] {"@ContratoDadosCertame_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001D2 ;
          prmT001D2 = new Object[] {
          new Object[] {"@ContratoDadosCertame_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001D11 ;
          prmT001D11 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoDadosCertame_Modalidade",SqlDbType.VarChar,40,0} ,
          new Object[] {"@ContratoDadosCertame_Numero",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContratoDadosCertame_Site",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ContratoDadosCertame_Uasg",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoDadosCertame_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoDadosCertame_DataHomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoDadosCertame_DataAdjudicacao",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmT001D12 ;
          prmT001D12 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoDadosCertame_Modalidade",SqlDbType.VarChar,40,0} ,
          new Object[] {"@ContratoDadosCertame_Numero",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContratoDadosCertame_Site",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ContratoDadosCertame_Uasg",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoDadosCertame_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoDadosCertame_DataHomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoDadosCertame_DataAdjudicacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoDadosCertame_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001D13 ;
          prmT001D13 = new Object[] {
          new Object[] {"@ContratoDadosCertame_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001D14 ;
          prmT001D14 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T001D2", "SELECT [Contrato_Codigo], [ContratoDadosCertame_Codigo], [ContratoDadosCertame_Modalidade], [ContratoDadosCertame_Numero], [ContratoDadosCertame_Site], [ContratoDadosCertame_Uasg], [ContratoDadosCertame_Data], [ContratoDadosCertame_DataHomologacao], [ContratoDadosCertame_DataAdjudicacao] FROM [ContratoDadosCertame] WITH (UPDLOCK) WHERE [ContratoDadosCertame_Codigo] = @ContratoDadosCertame_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001D2,1,0,true,false )
             ,new CursorDef("T001D3", "SELECT [Contrato_Codigo], [ContratoDadosCertame_Codigo], [ContratoDadosCertame_Modalidade], [ContratoDadosCertame_Numero], [ContratoDadosCertame_Site], [ContratoDadosCertame_Uasg], [ContratoDadosCertame_Data], [ContratoDadosCertame_DataHomologacao], [ContratoDadosCertame_DataAdjudicacao] FROM [ContratoDadosCertame] WITH (NOLOCK) WHERE [ContratoDadosCertame_Codigo] = @ContratoDadosCertame_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001D3,1,0,true,false )
             ,new CursorDef("T001D4", "SELECT [Contratada_Codigo], [Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001D4,1,0,true,false )
             ,new CursorDef("T001D5", "SELECT [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001D5,1,0,true,false )
             ,new CursorDef("T001D6", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001D6,1,0,true,false )
             ,new CursorDef("T001D7", "SELECT T2.[Contratada_Codigo], TM1.[Contrato_Codigo], TM1.[ContratoDadosCertame_Codigo], T2.[Contrato_Numero], T2.[Contrato_NumeroAta], T2.[Contrato_Ano], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, TM1.[ContratoDadosCertame_Modalidade], TM1.[ContratoDadosCertame_Numero], TM1.[ContratoDadosCertame_Site], TM1.[ContratoDadosCertame_Uasg], TM1.[ContratoDadosCertame_Data], TM1.[ContratoDadosCertame_DataHomologacao], TM1.[ContratoDadosCertame_DataAdjudicacao], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod FROM ((([ContratoDadosCertame] TM1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = TM1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) WHERE TM1.[Contrato_Codigo] = @Contrato_Codigo and TM1.[ContratoDadosCertame_Codigo] = @ContratoDadosCertame_Codigo ORDER BY TM1.[ContratoDadosCertame_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001D7,100,0,true,false )
             ,new CursorDef("T001D8", "SELECT [ContratoDadosCertame_Codigo] FROM [ContratoDadosCertame] WITH (NOLOCK) WHERE [ContratoDadosCertame_Codigo] = @ContratoDadosCertame_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001D8,1,0,true,false )
             ,new CursorDef("T001D9", "SELECT TOP 1 [Contrato_Codigo], [ContratoDadosCertame_Codigo] FROM [ContratoDadosCertame] WITH (NOLOCK) WHERE ( [ContratoDadosCertame_Codigo] > @ContratoDadosCertame_Codigo) and [Contrato_Codigo] = @Contrato_Codigo ORDER BY [ContratoDadosCertame_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001D9,1,0,true,true )
             ,new CursorDef("T001D10", "SELECT TOP 1 [Contrato_Codigo], [ContratoDadosCertame_Codigo] FROM [ContratoDadosCertame] WITH (NOLOCK) WHERE ( [ContratoDadosCertame_Codigo] < @ContratoDadosCertame_Codigo) and [Contrato_Codigo] = @Contrato_Codigo ORDER BY [ContratoDadosCertame_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001D10,1,0,true,true )
             ,new CursorDef("T001D11", "INSERT INTO [ContratoDadosCertame]([Contrato_Codigo], [ContratoDadosCertame_Modalidade], [ContratoDadosCertame_Numero], [ContratoDadosCertame_Site], [ContratoDadosCertame_Uasg], [ContratoDadosCertame_Data], [ContratoDadosCertame_DataHomologacao], [ContratoDadosCertame_DataAdjudicacao]) VALUES(@Contrato_Codigo, @ContratoDadosCertame_Modalidade, @ContratoDadosCertame_Numero, @ContratoDadosCertame_Site, @ContratoDadosCertame_Uasg, @ContratoDadosCertame_Data, @ContratoDadosCertame_DataHomologacao, @ContratoDadosCertame_DataAdjudicacao); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT001D11)
             ,new CursorDef("T001D12", "UPDATE [ContratoDadosCertame] SET [Contrato_Codigo]=@Contrato_Codigo, [ContratoDadosCertame_Modalidade]=@ContratoDadosCertame_Modalidade, [ContratoDadosCertame_Numero]=@ContratoDadosCertame_Numero, [ContratoDadosCertame_Site]=@ContratoDadosCertame_Site, [ContratoDadosCertame_Uasg]=@ContratoDadosCertame_Uasg, [ContratoDadosCertame_Data]=@ContratoDadosCertame_Data, [ContratoDadosCertame_DataHomologacao]=@ContratoDadosCertame_DataHomologacao, [ContratoDadosCertame_DataAdjudicacao]=@ContratoDadosCertame_DataAdjudicacao  WHERE [ContratoDadosCertame_Codigo] = @ContratoDadosCertame_Codigo", GxErrorMask.GX_NOMASK,prmT001D12)
             ,new CursorDef("T001D13", "DELETE FROM [ContratoDadosCertame]  WHERE [ContratoDadosCertame_Codigo] = @ContratoDadosCertame_Codigo", GxErrorMask.GX_NOMASK,prmT001D13)
             ,new CursorDef("T001D14", "SELECT [ContratoDadosCertame_Codigo] FROM [ContratoDadosCertame] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [ContratoDadosCertame_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001D14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((long[]) buf[3])[0] = rslt.getLong(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(7) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((long[]) buf[3])[0] = rslt.getLong(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(7) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getVarchar(9) ;
                ((long[]) buf[12])[0] = rslt.getLong(10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((String[]) buf[14])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((short[]) buf[16])[0] = rslt.getShort(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(13) ;
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(14) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((int[]) buf[23])[0] = rslt.getInt(16) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (long)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[7]);
                }
                stmt.SetParameter(6, (DateTime)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(7, (DateTime)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(8, (DateTime)parms[12]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (long)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[7]);
                }
                stmt.SetParameter(6, (DateTime)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(7, (DateTime)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(8, (DateTime)parms[12]);
                }
                stmt.SetParameter(9, (int)parms[13]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
