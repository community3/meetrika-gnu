/*
               File: ExportReportExtraWWContagemResultadoOS
        Description: Export Report Extra WWContagem Resultado OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 22:59:6.44
       Program type: HTTP procedure
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class exportreportextrawwcontagemresultadoos : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV12Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV112Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV117TFContagemResultado_OsFsOsFm = GetNextPar( );
                  AV118TFContagemResultado_OsFsOsFm_Sel = GetNextPar( );
                  AV119TFContagemResultado_DataDmn = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV120TFContagemResultado_DataDmn_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV121TFContagemResultado_DataUltCnt = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV122TFContagemResultado_DataUltCnt_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV123TFContagemrResultado_SistemaSigla = GetNextPar( );
                  AV124TFContagemrResultado_SistemaSigla_Sel = GetNextPar( );
                  AV125TFContagemResultado_ContratadaSigla = GetNextPar( );
                  AV126TFContagemResultado_ContratadaSigla_Sel = GetNextPar( );
                  AV127TFContagemResultado_ServicoSigla = GetNextPar( );
                  AV128TFContagemResultado_ServicoSigla_Sel = GetNextPar( );
                  AV129TFContagemResultado_DeflatorCnt = NumberUtil.Val( GetNextPar( ), ".");
                  AV130TFContagemResultado_DeflatorCnt_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV131TFContagemResultado_StatusDmn_SelsJson = GetNextPar( );
                  AV135TFContagemResultado_StatusUltCnt_SelsJson = GetNextPar( );
                  AV139TFContagemResultado_Baseline_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV140TFContagemResultado_PFBFSUltima = NumberUtil.Val( GetNextPar( ), ".");
                  AV141TFContagemResultado_PFBFSUltima_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV142TFContagemResultado_PFLFSUltima = NumberUtil.Val( GetNextPar( ), ".");
                  AV143TFContagemResultado_PFLFSUltima_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV144TFContagemResultado_PFBFMUltima = NumberUtil.Val( GetNextPar( ), ".");
                  AV145TFContagemResultado_PFBFMUltima_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV146TFContagemResultado_PFLFMUltima = NumberUtil.Val( GetNextPar( ), ".");
                  AV147TFContagemResultado_PFLFMUltima_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV148TFContagemResultado_PFFinal = NumberUtil.Val( GetNextPar( ), ".");
                  AV149TFContagemResultado_PFFinal_To = NumberUtil.Val( GetNextPar( ), ".");
                  AV10OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV11OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  AV70GridStateXML = GetNextPar( );
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public exportreportextrawwcontagemresultadoos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public exportreportextrawwcontagemresultadoos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           int aP1_Contratada_Codigo ,
                           String aP2_TFContagemResultado_OsFsOsFm ,
                           String aP3_TFContagemResultado_OsFsOsFm_Sel ,
                           DateTime aP4_TFContagemResultado_DataDmn ,
                           DateTime aP5_TFContagemResultado_DataDmn_To ,
                           DateTime aP6_TFContagemResultado_DataUltCnt ,
                           DateTime aP7_TFContagemResultado_DataUltCnt_To ,
                           String aP8_TFContagemrResultado_SistemaSigla ,
                           String aP9_TFContagemrResultado_SistemaSigla_Sel ,
                           String aP10_TFContagemResultado_ContratadaSigla ,
                           String aP11_TFContagemResultado_ContratadaSigla_Sel ,
                           String aP12_TFContagemResultado_ServicoSigla ,
                           String aP13_TFContagemResultado_ServicoSigla_Sel ,
                           decimal aP14_TFContagemResultado_DeflatorCnt ,
                           decimal aP15_TFContagemResultado_DeflatorCnt_To ,
                           String aP16_TFContagemResultado_StatusDmn_SelsJson ,
                           String aP17_TFContagemResultado_StatusUltCnt_SelsJson ,
                           short aP18_TFContagemResultado_Baseline_Sel ,
                           decimal aP19_TFContagemResultado_PFBFSUltima ,
                           decimal aP20_TFContagemResultado_PFBFSUltima_To ,
                           decimal aP21_TFContagemResultado_PFLFSUltima ,
                           decimal aP22_TFContagemResultado_PFLFSUltima_To ,
                           decimal aP23_TFContagemResultado_PFBFMUltima ,
                           decimal aP24_TFContagemResultado_PFBFMUltima_To ,
                           decimal aP25_TFContagemResultado_PFLFMUltima ,
                           decimal aP26_TFContagemResultado_PFLFMUltima_To ,
                           decimal aP27_TFContagemResultado_PFFinal ,
                           decimal aP28_TFContagemResultado_PFFinal_To ,
                           short aP29_OrderedBy ,
                           bool aP30_OrderedDsc ,
                           String aP31_GridStateXML )
      {
         this.AV12Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV112Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV117TFContagemResultado_OsFsOsFm = aP2_TFContagemResultado_OsFsOsFm;
         this.AV118TFContagemResultado_OsFsOsFm_Sel = aP3_TFContagemResultado_OsFsOsFm_Sel;
         this.AV119TFContagemResultado_DataDmn = aP4_TFContagemResultado_DataDmn;
         this.AV120TFContagemResultado_DataDmn_To = aP5_TFContagemResultado_DataDmn_To;
         this.AV121TFContagemResultado_DataUltCnt = aP6_TFContagemResultado_DataUltCnt;
         this.AV122TFContagemResultado_DataUltCnt_To = aP7_TFContagemResultado_DataUltCnt_To;
         this.AV123TFContagemrResultado_SistemaSigla = aP8_TFContagemrResultado_SistemaSigla;
         this.AV124TFContagemrResultado_SistemaSigla_Sel = aP9_TFContagemrResultado_SistemaSigla_Sel;
         this.AV125TFContagemResultado_ContratadaSigla = aP10_TFContagemResultado_ContratadaSigla;
         this.AV126TFContagemResultado_ContratadaSigla_Sel = aP11_TFContagemResultado_ContratadaSigla_Sel;
         this.AV127TFContagemResultado_ServicoSigla = aP12_TFContagemResultado_ServicoSigla;
         this.AV128TFContagemResultado_ServicoSigla_Sel = aP13_TFContagemResultado_ServicoSigla_Sel;
         this.AV129TFContagemResultado_DeflatorCnt = aP14_TFContagemResultado_DeflatorCnt;
         this.AV130TFContagemResultado_DeflatorCnt_To = aP15_TFContagemResultado_DeflatorCnt_To;
         this.AV131TFContagemResultado_StatusDmn_SelsJson = aP16_TFContagemResultado_StatusDmn_SelsJson;
         this.AV135TFContagemResultado_StatusUltCnt_SelsJson = aP17_TFContagemResultado_StatusUltCnt_SelsJson;
         this.AV139TFContagemResultado_Baseline_Sel = aP18_TFContagemResultado_Baseline_Sel;
         this.AV140TFContagemResultado_PFBFSUltima = aP19_TFContagemResultado_PFBFSUltima;
         this.AV141TFContagemResultado_PFBFSUltima_To = aP20_TFContagemResultado_PFBFSUltima_To;
         this.AV142TFContagemResultado_PFLFSUltima = aP21_TFContagemResultado_PFLFSUltima;
         this.AV143TFContagemResultado_PFLFSUltima_To = aP22_TFContagemResultado_PFLFSUltima_To;
         this.AV144TFContagemResultado_PFBFMUltima = aP23_TFContagemResultado_PFBFMUltima;
         this.AV145TFContagemResultado_PFBFMUltima_To = aP24_TFContagemResultado_PFBFMUltima_To;
         this.AV146TFContagemResultado_PFLFMUltima = aP25_TFContagemResultado_PFLFMUltima;
         this.AV147TFContagemResultado_PFLFMUltima_To = aP26_TFContagemResultado_PFLFMUltima_To;
         this.AV148TFContagemResultado_PFFinal = aP27_TFContagemResultado_PFFinal;
         this.AV149TFContagemResultado_PFFinal_To = aP28_TFContagemResultado_PFFinal_To;
         this.AV10OrderedBy = aP29_OrderedBy;
         this.AV11OrderedDsc = aP30_OrderedDsc;
         this.AV70GridStateXML = aP31_GridStateXML;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 int aP1_Contratada_Codigo ,
                                 String aP2_TFContagemResultado_OsFsOsFm ,
                                 String aP3_TFContagemResultado_OsFsOsFm_Sel ,
                                 DateTime aP4_TFContagemResultado_DataDmn ,
                                 DateTime aP5_TFContagemResultado_DataDmn_To ,
                                 DateTime aP6_TFContagemResultado_DataUltCnt ,
                                 DateTime aP7_TFContagemResultado_DataUltCnt_To ,
                                 String aP8_TFContagemrResultado_SistemaSigla ,
                                 String aP9_TFContagemrResultado_SistemaSigla_Sel ,
                                 String aP10_TFContagemResultado_ContratadaSigla ,
                                 String aP11_TFContagemResultado_ContratadaSigla_Sel ,
                                 String aP12_TFContagemResultado_ServicoSigla ,
                                 String aP13_TFContagemResultado_ServicoSigla_Sel ,
                                 decimal aP14_TFContagemResultado_DeflatorCnt ,
                                 decimal aP15_TFContagemResultado_DeflatorCnt_To ,
                                 String aP16_TFContagemResultado_StatusDmn_SelsJson ,
                                 String aP17_TFContagemResultado_StatusUltCnt_SelsJson ,
                                 short aP18_TFContagemResultado_Baseline_Sel ,
                                 decimal aP19_TFContagemResultado_PFBFSUltima ,
                                 decimal aP20_TFContagemResultado_PFBFSUltima_To ,
                                 decimal aP21_TFContagemResultado_PFLFSUltima ,
                                 decimal aP22_TFContagemResultado_PFLFSUltima_To ,
                                 decimal aP23_TFContagemResultado_PFBFMUltima ,
                                 decimal aP24_TFContagemResultado_PFBFMUltima_To ,
                                 decimal aP25_TFContagemResultado_PFLFMUltima ,
                                 decimal aP26_TFContagemResultado_PFLFMUltima_To ,
                                 decimal aP27_TFContagemResultado_PFFinal ,
                                 decimal aP28_TFContagemResultado_PFFinal_To ,
                                 short aP29_OrderedBy ,
                                 bool aP30_OrderedDsc ,
                                 String aP31_GridStateXML )
      {
         exportreportextrawwcontagemresultadoos objexportreportextrawwcontagemresultadoos;
         objexportreportextrawwcontagemresultadoos = new exportreportextrawwcontagemresultadoos();
         objexportreportextrawwcontagemresultadoos.AV12Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objexportreportextrawwcontagemresultadoos.AV112Contratada_Codigo = aP1_Contratada_Codigo;
         objexportreportextrawwcontagemresultadoos.AV117TFContagemResultado_OsFsOsFm = aP2_TFContagemResultado_OsFsOsFm;
         objexportreportextrawwcontagemresultadoos.AV118TFContagemResultado_OsFsOsFm_Sel = aP3_TFContagemResultado_OsFsOsFm_Sel;
         objexportreportextrawwcontagemresultadoos.AV119TFContagemResultado_DataDmn = aP4_TFContagemResultado_DataDmn;
         objexportreportextrawwcontagemresultadoos.AV120TFContagemResultado_DataDmn_To = aP5_TFContagemResultado_DataDmn_To;
         objexportreportextrawwcontagemresultadoos.AV121TFContagemResultado_DataUltCnt = aP6_TFContagemResultado_DataUltCnt;
         objexportreportextrawwcontagemresultadoos.AV122TFContagemResultado_DataUltCnt_To = aP7_TFContagemResultado_DataUltCnt_To;
         objexportreportextrawwcontagemresultadoos.AV123TFContagemrResultado_SistemaSigla = aP8_TFContagemrResultado_SistemaSigla;
         objexportreportextrawwcontagemresultadoos.AV124TFContagemrResultado_SistemaSigla_Sel = aP9_TFContagemrResultado_SistemaSigla_Sel;
         objexportreportextrawwcontagemresultadoos.AV125TFContagemResultado_ContratadaSigla = aP10_TFContagemResultado_ContratadaSigla;
         objexportreportextrawwcontagemresultadoos.AV126TFContagemResultado_ContratadaSigla_Sel = aP11_TFContagemResultado_ContratadaSigla_Sel;
         objexportreportextrawwcontagemresultadoos.AV127TFContagemResultado_ServicoSigla = aP12_TFContagemResultado_ServicoSigla;
         objexportreportextrawwcontagemresultadoos.AV128TFContagemResultado_ServicoSigla_Sel = aP13_TFContagemResultado_ServicoSigla_Sel;
         objexportreportextrawwcontagemresultadoos.AV129TFContagemResultado_DeflatorCnt = aP14_TFContagemResultado_DeflatorCnt;
         objexportreportextrawwcontagemresultadoos.AV130TFContagemResultado_DeflatorCnt_To = aP15_TFContagemResultado_DeflatorCnt_To;
         objexportreportextrawwcontagemresultadoos.AV131TFContagemResultado_StatusDmn_SelsJson = aP16_TFContagemResultado_StatusDmn_SelsJson;
         objexportreportextrawwcontagemresultadoos.AV135TFContagemResultado_StatusUltCnt_SelsJson = aP17_TFContagemResultado_StatusUltCnt_SelsJson;
         objexportreportextrawwcontagemresultadoos.AV139TFContagemResultado_Baseline_Sel = aP18_TFContagemResultado_Baseline_Sel;
         objexportreportextrawwcontagemresultadoos.AV140TFContagemResultado_PFBFSUltima = aP19_TFContagemResultado_PFBFSUltima;
         objexportreportextrawwcontagemresultadoos.AV141TFContagemResultado_PFBFSUltima_To = aP20_TFContagemResultado_PFBFSUltima_To;
         objexportreportextrawwcontagemresultadoos.AV142TFContagemResultado_PFLFSUltima = aP21_TFContagemResultado_PFLFSUltima;
         objexportreportextrawwcontagemresultadoos.AV143TFContagemResultado_PFLFSUltima_To = aP22_TFContagemResultado_PFLFSUltima_To;
         objexportreportextrawwcontagemresultadoos.AV144TFContagemResultado_PFBFMUltima = aP23_TFContagemResultado_PFBFMUltima;
         objexportreportextrawwcontagemresultadoos.AV145TFContagemResultado_PFBFMUltima_To = aP24_TFContagemResultado_PFBFMUltima_To;
         objexportreportextrawwcontagemresultadoos.AV146TFContagemResultado_PFLFMUltima = aP25_TFContagemResultado_PFLFMUltima;
         objexportreportextrawwcontagemresultadoos.AV147TFContagemResultado_PFLFMUltima_To = aP26_TFContagemResultado_PFLFMUltima_To;
         objexportreportextrawwcontagemresultadoos.AV148TFContagemResultado_PFFinal = aP27_TFContagemResultado_PFFinal;
         objexportreportextrawwcontagemresultadoos.AV149TFContagemResultado_PFFinal_To = aP28_TFContagemResultado_PFFinal_To;
         objexportreportextrawwcontagemresultadoos.AV10OrderedBy = aP29_OrderedBy;
         objexportreportextrawwcontagemresultadoos.AV11OrderedDsc = aP30_OrderedDsc;
         objexportreportextrawwcontagemresultadoos.AV70GridStateXML = aP31_GridStateXML;
         objexportreportextrawwcontagemresultadoos.context.SetSubmitInitialConfig(context);
         objexportreportextrawwcontagemresultadoos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objexportreportextrawwcontagemresultadoos);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((exportreportextrawwcontagemresultadoos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 2, 1, 12240, 15840, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
            /* Execute user subroutine: 'PRINTMAINTITLE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTFILTERS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTCOLUMNTITLES' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTDATA' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTFOOTER' */
            S171 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H540( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PRINTMAINTITLE' Routine */
         AV154Contratada_DoUsuario = AV9WWPContext.gxTpr_Contratada_codigo;
         H540( false, 56) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 18, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Registro de Esfor�o", 5, Gx_line+5, 1035, Gx_line+35, 1, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+56);
      }

      protected void S121( )
      {
         /* 'PRINTFILTERS' Routine */
         if ( ! (0==AV12Contratada_AreaTrabalhoCod) )
         {
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("�rea de Trabalho", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12Contratada_AreaTrabalhoCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! (0==AV112Contratada_Codigo) )
         {
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Contratada", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV112Contratada_Codigo), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         AV71GridState.gxTpr_Dynamicfilters.FromXml(AV70GridStateXML, "");
         if ( AV71GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV72GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV71GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV72GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV72GridStateDynamicFilter.gxTpr_Operator;
               AV17ContagemResultado_OsFsOsFm1 = AV72GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ContagemResultado_OsFsOsFm1)) )
               {
                  if ( AV16DynamicFiltersOperator1 == 0 )
                  {
                     AV114FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                  }
                  else if ( AV16DynamicFiltersOperator1 == 1 )
                  {
                     AV114FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                  }
                  else if ( AV16DynamicFiltersOperator1 == 2 )
                  {
                     AV114FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                  }
                  AV73ContagemResultado_OsFsOsFm = AV17ContagemResultado_OsFsOsFm1;
                  H540( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV114FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV73ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV18ContagemResultado_DataDmn1 = context.localUtil.CToD( AV72GridStateDynamicFilter.gxTpr_Value, 2);
               AV19ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV72GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV18ContagemResultado_DataDmn1) )
               {
                  AV74FilterContagemResultado_DataDmnDescription = "Data Demanda";
                  AV75ContagemResultado_DataDmn = AV18ContagemResultado_DataDmn1;
                  H540( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV74FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV75ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (DateTime.MinValue==AV19ContagemResultado_DataDmn_To1) )
               {
                  AV74FilterContagemResultado_DataDmnDescription = "Data Demanda (at�)";
                  AV75ContagemResultado_DataDmn = AV19ContagemResultado_DataDmn_To1;
                  H540( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV74FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV75ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
            {
               AV96ContagemResultado_DataUltCnt1 = context.localUtil.CToD( AV72GridStateDynamicFilter.gxTpr_Value, 2);
               AV97ContagemResultado_DataUltCnt_To1 = context.localUtil.CToD( AV72GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV96ContagemResultado_DataUltCnt1) )
               {
                  AV102FilterContagemResultado_DataUltCntDescription = "Data Contagem";
                  AV103ContagemResultado_DataUltCnt = AV96ContagemResultado_DataUltCnt1;
                  H540( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV102FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV103ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
               if ( ! (DateTime.MinValue==AV97ContagemResultado_DataUltCnt_To1) )
               {
                  AV102FilterContagemResultado_DataUltCntDescription = "Data Contagem (at�)";
                  AV103ContagemResultado_DataUltCnt = AV97ContagemResultado_DataUltCnt_To1;
                  H540( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV102FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV103ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
            {
               AV20ContagemResultado_ContadorFM1 = (int)(NumberUtil.Val( AV72GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV20ContagemResultado_ContadorFM1) )
               {
                  AV76ContagemResultado_ContadorFM = AV20ContagemResultado_ContadorFM1;
                  H540( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Usu�rio da Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV76ContagemResultado_ContadorFM), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV21ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV72GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV21ContagemResultado_SistemaCod1) )
               {
                  AV77ContagemResultado_SistemaCod = AV21ContagemResultado_SistemaCod1;
                  H540( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV77ContagemResultado_SistemaCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV22ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( AV72GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV22ContagemResultado_ContratadaCod1) )
               {
                  AV78ContagemResultado_ContratadaCod = AV22ContagemResultado_ContratadaCod1;
                  H540( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV78ContagemResultado_ContratadaCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
            {
               AV23ContagemResultado_NaoCnfDmnCod1 = (int)(NumberUtil.Val( AV72GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV23ContagemResultado_NaoCnfDmnCod1) )
               {
                  AV79ContagemResultado_NaoCnfDmnCod = AV23ContagemResultado_NaoCnfDmnCod1;
                  H540( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("N�o Conformidade", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV79ContagemResultado_NaoCnfDmnCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV24ContagemResultado_StatusDmn1 = AV72GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ContagemResultado_StatusDmn1)) )
               {
                  AV26FilterContagemResultado_StatusDmn1ValueDescription = gxdomainstatusdemanda.getDescription(context,AV24ContagemResultado_StatusDmn1);
                  AV25FilterContagemResultado_StatusDmnValueDescription = AV26FilterContagemResultado_StatusDmn1ValueDescription;
                  H540( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Status Demanda", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV25FilterContagemResultado_StatusDmnValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 )
            {
               AV104ContagemResultado_StatusDmnVnc1 = AV72GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104ContagemResultado_StatusDmnVnc1)) )
               {
                  AV106FilterContagemResultado_StatusDmnVnc1ValueDescription = gxdomainstatusdemanda.getDescription(context,AV104ContagemResultado_StatusDmnVnc1);
                  AV105FilterContagemResultado_StatusDmnVncValueDescription = AV106FilterContagemResultado_StatusDmnVnc1ValueDescription;
                  H540( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Status Dmn Vinculada", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV105FilterContagemResultado_StatusDmnVncValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV72GridStateDynamicFilter.gxTpr_Operator;
               AV29ContagemResultado_EsforcoSoma1 = (int)(NumberUtil.Val( AV72GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV29ContagemResultado_EsforcoSoma1) )
               {
                  if ( AV16DynamicFiltersOperator1 == 0 )
                  {
                     AV83FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (>)";
                  }
                  else if ( AV16DynamicFiltersOperator1 == 1 )
                  {
                     AV83FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (<)";
                  }
                  else if ( AV16DynamicFiltersOperator1 == 2 )
                  {
                     AV83FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (=)";
                  }
                  AV84ContagemResultado_EsforcoSoma = AV29ContagemResultado_EsforcoSoma1;
                  H540( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV83FilterContagemResultado_EsforcoSomaDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV84ContagemResultado_EsforcoSoma), "ZZZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_BASELINE") == 0 )
            {
               AV30ContagemResultado_Baseline1 = AV72GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30ContagemResultado_Baseline1)) )
               {
                  if ( StringUtil.StrCmp(StringUtil.Trim( AV30ContagemResultado_Baseline1), "S") == 0 )
                  {
                     AV32FilterContagemResultado_Baseline1ValueDescription = "Sim";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( AV30ContagemResultado_Baseline1), "N") == 0 )
                  {
                     AV32FilterContagemResultado_Baseline1ValueDescription = "N�o";
                  }
                  AV31FilterContagemResultado_BaselineValueDescription = AV32FilterContagemResultado_Baseline1ValueDescription;
                  H540( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV31FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV33ContagemResultado_Servico1 = (int)(NumberUtil.Val( AV72GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV33ContagemResultado_Servico1) )
               {
                  AV86ContagemResultado_Servico = AV33ContagemResultado_Servico1;
                  H540( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV86ContagemResultado_Servico), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
               }
            }
            if ( AV71GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV34DynamicFiltersEnabled2 = true;
               AV72GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV71GridState.gxTpr_Dynamicfilters.Item(2));
               AV35DynamicFiltersSelector2 = AV72GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
               {
                  AV36DynamicFiltersOperator2 = AV72GridStateDynamicFilter.gxTpr_Operator;
                  AV37ContagemResultado_OsFsOsFm2 = AV72GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37ContagemResultado_OsFsOsFm2)) )
                  {
                     if ( AV36DynamicFiltersOperator2 == 0 )
                     {
                        AV114FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                     }
                     else if ( AV36DynamicFiltersOperator2 == 1 )
                     {
                        AV114FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                     }
                     else if ( AV36DynamicFiltersOperator2 == 2 )
                     {
                        AV114FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                     }
                     AV73ContagemResultado_OsFsOsFm = AV37ContagemResultado_OsFsOsFm2;
                     H540( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV114FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV73ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV38ContagemResultado_DataDmn2 = context.localUtil.CToD( AV72GridStateDynamicFilter.gxTpr_Value, 2);
                  AV39ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV72GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV38ContagemResultado_DataDmn2) )
                  {
                     AV74FilterContagemResultado_DataDmnDescription = "Data Demanda";
                     AV75ContagemResultado_DataDmn = AV38ContagemResultado_DataDmn2;
                     H540( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV74FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV75ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (DateTime.MinValue==AV39ContagemResultado_DataDmn_To2) )
                  {
                     AV74FilterContagemResultado_DataDmnDescription = "Data Demanda (at�)";
                     AV75ContagemResultado_DataDmn = AV39ContagemResultado_DataDmn_To2;
                     H540( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV74FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV75ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
               {
                  AV98ContagemResultado_DataUltCnt2 = context.localUtil.CToD( AV72GridStateDynamicFilter.gxTpr_Value, 2);
                  AV99ContagemResultado_DataUltCnt_To2 = context.localUtil.CToD( AV72GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV98ContagemResultado_DataUltCnt2) )
                  {
                     AV102FilterContagemResultado_DataUltCntDescription = "Data Contagem";
                     AV103ContagemResultado_DataUltCnt = AV98ContagemResultado_DataUltCnt2;
                     H540( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV102FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV103ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
                  if ( ! (DateTime.MinValue==AV99ContagemResultado_DataUltCnt_To2) )
                  {
                     AV102FilterContagemResultado_DataUltCntDescription = "Data Contagem (at�)";
                     AV103ContagemResultado_DataUltCnt = AV99ContagemResultado_DataUltCnt_To2;
                     H540( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV102FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV103ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
               {
                  AV40ContagemResultado_ContadorFM2 = (int)(NumberUtil.Val( AV72GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV40ContagemResultado_ContadorFM2) )
                  {
                     AV76ContagemResultado_ContadorFM = AV40ContagemResultado_ContadorFM2;
                     H540( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Usu�rio da Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV76ContagemResultado_ContadorFM), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV41ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV72GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV41ContagemResultado_SistemaCod2) )
                  {
                     AV77ContagemResultado_SistemaCod = AV41ContagemResultado_SistemaCod2;
                     H540( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV77ContagemResultado_SistemaCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
               {
                  AV42ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( AV72GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV42ContagemResultado_ContratadaCod2) )
                  {
                     AV78ContagemResultado_ContratadaCod = AV42ContagemResultado_ContratadaCod2;
                     H540( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV78ContagemResultado_ContratadaCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
               {
                  AV43ContagemResultado_NaoCnfDmnCod2 = (int)(NumberUtil.Val( AV72GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV43ContagemResultado_NaoCnfDmnCod2) )
                  {
                     AV79ContagemResultado_NaoCnfDmnCod = AV43ContagemResultado_NaoCnfDmnCod2;
                     H540( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("N�o Conformidade", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV79ContagemResultado_NaoCnfDmnCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV44ContagemResultado_StatusDmn2 = AV72GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContagemResultado_StatusDmn2)) )
                  {
                     AV45FilterContagemResultado_StatusDmn2ValueDescription = gxdomainstatusdemanda.getDescription(context,AV44ContagemResultado_StatusDmn2);
                     AV25FilterContagemResultado_StatusDmnValueDescription = AV45FilterContagemResultado_StatusDmn2ValueDescription;
                     H540( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Status Demanda", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV25FilterContagemResultado_StatusDmnValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 )
               {
                  AV107ContagemResultado_StatusDmnVnc2 = AV72GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107ContagemResultado_StatusDmnVnc2)) )
                  {
                     AV108FilterContagemResultado_StatusDmnVnc2ValueDescription = gxdomainstatusdemanda.getDescription(context,AV107ContagemResultado_StatusDmnVnc2);
                     AV105FilterContagemResultado_StatusDmnVncValueDescription = AV108FilterContagemResultado_StatusDmnVnc2ValueDescription;
                     H540( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Status Dmn Vinculada", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV105FilterContagemResultado_StatusDmnVncValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
               {
                  AV36DynamicFiltersOperator2 = AV72GridStateDynamicFilter.gxTpr_Operator;
                  AV48ContagemResultado_EsforcoSoma2 = (int)(NumberUtil.Val( AV72GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV48ContagemResultado_EsforcoSoma2) )
                  {
                     if ( AV36DynamicFiltersOperator2 == 0 )
                     {
                        AV83FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (>)";
                     }
                     else if ( AV36DynamicFiltersOperator2 == 1 )
                     {
                        AV83FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (<)";
                     }
                     else if ( AV36DynamicFiltersOperator2 == 2 )
                     {
                        AV83FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (=)";
                     }
                     AV84ContagemResultado_EsforcoSoma = AV48ContagemResultado_EsforcoSoma2;
                     H540( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV83FilterContagemResultado_EsforcoSomaDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV84ContagemResultado_EsforcoSoma), "ZZZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_BASELINE") == 0 )
               {
                  AV49ContagemResultado_Baseline2 = AV72GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ContagemResultado_Baseline2)) )
                  {
                     if ( StringUtil.StrCmp(StringUtil.Trim( AV49ContagemResultado_Baseline2), "S") == 0 )
                     {
                        AV50FilterContagemResultado_Baseline2ValueDescription = "Sim";
                     }
                     else if ( StringUtil.StrCmp(StringUtil.Trim( AV49ContagemResultado_Baseline2), "N") == 0 )
                     {
                        AV50FilterContagemResultado_Baseline2ValueDescription = "N�o";
                     }
                     AV31FilterContagemResultado_BaselineValueDescription = AV50FilterContagemResultado_Baseline2ValueDescription;
                     H540( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV31FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICO") == 0 )
               {
                  AV51ContagemResultado_Servico2 = (int)(NumberUtil.Val( AV72GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV51ContagemResultado_Servico2) )
                  {
                     AV86ContagemResultado_Servico = AV51ContagemResultado_Servico2;
                     H540( false, 20) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV86ContagemResultado_Servico), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+20);
                  }
               }
               if ( AV71GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV52DynamicFiltersEnabled3 = true;
                  AV72GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV71GridState.gxTpr_Dynamicfilters.Item(3));
                  AV53DynamicFiltersSelector3 = AV72GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV53DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                  {
                     AV54DynamicFiltersOperator3 = AV72GridStateDynamicFilter.gxTpr_Operator;
                     AV55ContagemResultado_OsFsOsFm3 = AV72GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ContagemResultado_OsFsOsFm3)) )
                     {
                        if ( AV54DynamicFiltersOperator3 == 0 )
                        {
                           AV114FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Igual)";
                        }
                        else if ( AV54DynamicFiltersOperator3 == 1 )
                        {
                           AV114FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Come�a com)";
                        }
                        else if ( AV54DynamicFiltersOperator3 == 2 )
                        {
                           AV114FilterContagemResultado_OsFsOsFmDescription = "OS / OS Refer�ncia (Cont�m)";
                        }
                        AV73ContagemResultado_OsFsOsFm = AV55ContagemResultado_OsFsOsFm3;
                        H540( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV114FilterContagemResultado_OsFsOsFmDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV73ContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV56ContagemResultado_DataDmn3 = context.localUtil.CToD( AV72GridStateDynamicFilter.gxTpr_Value, 2);
                     AV57ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV72GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV56ContagemResultado_DataDmn3) )
                     {
                        AV74FilterContagemResultado_DataDmnDescription = "Data Demanda";
                        AV75ContagemResultado_DataDmn = AV56ContagemResultado_DataDmn3;
                        H540( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV74FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV75ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (DateTime.MinValue==AV57ContagemResultado_DataDmn_To3) )
                     {
                        AV74FilterContagemResultado_DataDmnDescription = "Data Demanda (at�)";
                        AV75ContagemResultado_DataDmn = AV57ContagemResultado_DataDmn_To3;
                        H540( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV74FilterContagemResultado_DataDmnDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV75ContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
                  {
                     AV100ContagemResultado_DataUltCnt3 = context.localUtil.CToD( AV72GridStateDynamicFilter.gxTpr_Value, 2);
                     AV101ContagemResultado_DataUltCnt_To3 = context.localUtil.CToD( AV72GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV100ContagemResultado_DataUltCnt3) )
                     {
                        AV102FilterContagemResultado_DataUltCntDescription = "Data Contagem";
                        AV103ContagemResultado_DataUltCnt = AV100ContagemResultado_DataUltCnt3;
                        H540( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV102FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV103ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                     if ( ! (DateTime.MinValue==AV101ContagemResultado_DataUltCnt_To3) )
                     {
                        AV102FilterContagemResultado_DataUltCntDescription = "Data Contagem (at�)";
                        AV103ContagemResultado_DataUltCnt = AV101ContagemResultado_DataUltCnt_To3;
                        H540( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV102FilterContagemResultado_DataUltCntDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV103ContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                  {
                     AV58ContagemResultado_ContadorFM3 = (int)(NumberUtil.Val( AV72GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV58ContagemResultado_ContadorFM3) )
                     {
                        AV76ContagemResultado_ContadorFM = AV58ContagemResultado_ContadorFM3;
                        H540( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Usu�rio da Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV76ContagemResultado_ContadorFM), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV59ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV72GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV59ContagemResultado_SistemaCod3) )
                     {
                        AV77ContagemResultado_SistemaCod = AV59ContagemResultado_SistemaCod3;
                        H540( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV77ContagemResultado_SistemaCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                  {
                     AV60ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( AV72GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV60ContagemResultado_ContratadaCod3) )
                     {
                        AV78ContagemResultado_ContratadaCod = AV60ContagemResultado_ContratadaCod3;
                        H540( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV78ContagemResultado_ContratadaCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                  {
                     AV61ContagemResultado_NaoCnfDmnCod3 = (int)(NumberUtil.Val( AV72GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV61ContagemResultado_NaoCnfDmnCod3) )
                     {
                        AV79ContagemResultado_NaoCnfDmnCod = AV61ContagemResultado_NaoCnfDmnCod3;
                        H540( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("N�o Conformidade", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV79ContagemResultado_NaoCnfDmnCod), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV62ContagemResultado_StatusDmn3 = AV72GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62ContagemResultado_StatusDmn3)) )
                     {
                        AV63FilterContagemResultado_StatusDmn3ValueDescription = gxdomainstatusdemanda.getDescription(context,AV62ContagemResultado_StatusDmn3);
                        AV25FilterContagemResultado_StatusDmnValueDescription = AV63FilterContagemResultado_StatusDmn3ValueDescription;
                        H540( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Status Demanda", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV25FilterContagemResultado_StatusDmnValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 )
                  {
                     AV109ContagemResultado_StatusDmnVnc3 = AV72GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109ContagemResultado_StatusDmnVnc3)) )
                     {
                        AV110FilterContagemResultado_StatusDmnVnc3ValueDescription = gxdomainstatusdemanda.getDescription(context,AV109ContagemResultado_StatusDmnVnc3);
                        AV105FilterContagemResultado_StatusDmnVncValueDescription = AV110FilterContagemResultado_StatusDmnVnc3ValueDescription;
                        H540( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Status Dmn Vinculada", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV105FilterContagemResultado_StatusDmnVncValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
                  {
                     AV54DynamicFiltersOperator3 = AV72GridStateDynamicFilter.gxTpr_Operator;
                     AV66ContagemResultado_EsforcoSoma3 = (int)(NumberUtil.Val( AV72GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV66ContagemResultado_EsforcoSoma3) )
                     {
                        if ( AV54DynamicFiltersOperator3 == 0 )
                        {
                           AV83FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (>)";
                        }
                        else if ( AV54DynamicFiltersOperator3 == 1 )
                        {
                           AV83FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (<)";
                        }
                        else if ( AV54DynamicFiltersOperator3 == 2 )
                        {
                           AV83FilterContagemResultado_EsforcoSomaDescription = "Esfor�o Demanda (=)";
                        }
                        AV84ContagemResultado_EsforcoSoma = AV66ContagemResultado_EsforcoSoma3;
                        H540( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV83FilterContagemResultado_EsforcoSomaDescription, "")), 5, Gx_line+2, 186, Gx_line+17, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV84ContagemResultado_EsforcoSoma), "ZZZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector3, "CONTAGEMRESULTADO_BASELINE") == 0 )
                  {
                     AV67ContagemResultado_Baseline3 = AV72GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67ContagemResultado_Baseline3)) )
                     {
                        if ( StringUtil.StrCmp(StringUtil.Trim( AV67ContagemResultado_Baseline3), "S") == 0 )
                        {
                           AV68FilterContagemResultado_Baseline3ValueDescription = "Sim";
                        }
                        else if ( StringUtil.StrCmp(StringUtil.Trim( AV67ContagemResultado_Baseline3), "N") == 0 )
                        {
                           AV68FilterContagemResultado_Baseline3ValueDescription = "N�o";
                        }
                        AV31FilterContagemResultado_BaselineValueDescription = AV68FilterContagemResultado_Baseline3ValueDescription;
                        H540( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Baseline", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV31FilterContagemResultado_BaselineValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV53DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICO") == 0 )
                  {
                     AV69ContagemResultado_Servico3 = (int)(NumberUtil.Val( AV72GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV69ContagemResultado_Servico3) )
                     {
                        AV86ContagemResultado_Servico = AV69ContagemResultado_Servico3;
                        H540( false, 20) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV86ContagemResultado_Servico), "ZZZZZ9")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+20);
                     }
                  }
               }
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118TFContagemResultado_OsFsOsFm_Sel)) )
         {
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("OS Ref|OS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV118TFContagemResultado_OsFsOsFm_Sel, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117TFContagemResultado_OsFsOsFm)) )
            {
               H540( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("OS Ref|OS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV117TFContagemResultado_OsFsOsFm, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! ( (DateTime.MinValue==AV119TFContagemResultado_DataDmn) && (DateTime.MinValue==AV120TFContagemResultado_DataDmn_To) ) )
         {
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Demanda", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV119TFContagemResultado_DataDmn, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Demanda (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV120TFContagemResultado_DataDmn_To, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (DateTime.MinValue==AV121TFContagemResultado_DataUltCnt) && (DateTime.MinValue==AV122TFContagemResultado_DataUltCnt_To) ) )
         {
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Contagem", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV121TFContagemResultado_DataUltCnt, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Contagem (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV122TFContagemResultado_DataUltCnt_To, "99/99/99"), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124TFContagemrResultado_SistemaSigla_Sel)) )
         {
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV124TFContagemrResultado_SistemaSigla_Sel, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123TFContagemrResultado_SistemaSigla)) )
            {
               H540( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Sistema", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV123TFContagemrResultado_SistemaSigla, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV126TFContagemResultado_ContratadaSigla_Sel)) )
         {
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV126TFContagemResultado_ContratadaSigla_Sel, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV125TFContagemResultado_ContratadaSigla)) )
            {
               H540( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Prestadora", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV125TFContagemResultado_ContratadaSigla, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV128TFContagemResultado_ServicoSigla_Sel)) )
         {
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV128TFContagemResultado_ServicoSigla_Sel, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         else
         {
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV127TFContagemResultado_ServicoSigla)) )
            {
               H540( false, 20) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Servi�o", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV127TFContagemResultado_ServicoSigla, "@!")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+20);
            }
         }
         if ( ! ( (Convert.ToDecimal(0)==AV129TFContagemResultado_DeflatorCnt) && (Convert.ToDecimal(0)==AV130TFContagemResultado_DeflatorCnt_To) ) )
         {
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Deflator", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV129TFContagemResultado_DeflatorCnt, "Z9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Deflator (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV130TFContagemResultado_DeflatorCnt_To, "Z9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         AV133TFContagemResultado_StatusDmn_Sels.FromJSonString(AV131TFContagemResultado_StatusDmn_SelsJson);
         if ( ! ( AV133TFContagemResultado_StatusDmn_Sels.Count == 0 ) )
         {
            AV153i = 1;
            AV165GXV1 = 1;
            while ( AV165GXV1 <= AV133TFContagemResultado_StatusDmn_Sels.Count )
            {
               AV134TFContagemResultado_StatusDmn_Sel = AV133TFContagemResultado_StatusDmn_Sels.GetString(AV165GXV1);
               if ( AV153i == 1 )
               {
                  AV132TFContagemResultado_StatusDmn_SelDscs = "";
               }
               else
               {
                  AV132TFContagemResultado_StatusDmn_SelDscs = AV132TFContagemResultado_StatusDmn_SelDscs + ", ";
               }
               AV150FilterTFContagemResultado_StatusDmn_SelValueDescription = gxdomainstatusdemanda.getDescription(context,AV134TFContagemResultado_StatusDmn_Sel);
               AV132TFContagemResultado_StatusDmn_SelDscs = AV132TFContagemResultado_StatusDmn_SelDscs + AV150FilterTFContagemResultado_StatusDmn_SelValueDescription;
               AV153i = (long)(AV153i+1);
               AV165GXV1 = (int)(AV165GXV1+1);
            }
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Status Dmn", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV132TFContagemResultado_StatusDmn_SelDscs, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         AV137TFContagemResultado_StatusUltCnt_Sels.FromJSonString(AV135TFContagemResultado_StatusUltCnt_SelsJson);
         if ( ! ( AV137TFContagemResultado_StatusUltCnt_Sels.Count == 0 ) )
         {
            AV153i = 1;
            AV166GXV2 = 1;
            while ( AV166GXV2 <= AV137TFContagemResultado_StatusUltCnt_Sels.Count )
            {
               AV138TFContagemResultado_StatusUltCnt_Sel = (short)(AV137TFContagemResultado_StatusUltCnt_Sels.GetNumeric(AV166GXV2));
               if ( AV153i == 1 )
               {
                  AV136TFContagemResultado_StatusUltCnt_SelDscs = "";
               }
               else
               {
                  AV136TFContagemResultado_StatusUltCnt_SelDscs = AV136TFContagemResultado_StatusUltCnt_SelDscs + ", ";
               }
               AV151FilterTFContagemResultado_StatusUltCnt_SelValueDescription = gxdomainstatuscontagem.getDescription(context,AV138TFContagemResultado_StatusUltCnt_Sel);
               AV136TFContagemResultado_StatusUltCnt_SelDscs = AV136TFContagemResultado_StatusUltCnt_SelDscs + AV151FilterTFContagemResultado_StatusUltCnt_SelValueDescription;
               AV153i = (long)(AV153i+1);
               AV166GXV2 = (int)(AV166GXV2+1);
            }
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Status Cnt", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV136TFContagemResultado_StatusUltCnt_SelDscs, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! (0==AV139TFContagemResultado_Baseline_Sel) )
         {
            if ( AV139TFContagemResultado_Baseline_Sel == 1 )
            {
               AV152FilterTFContagemResultado_Baseline_SelValueDescription = "Marcado";
            }
            else if ( AV139TFContagemResultado_Baseline_Sel == 2 )
            {
               AV152FilterTFContagemResultado_Baseline_SelValueDescription = "Desmarcado";
            }
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("BS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV152FilterTFContagemResultado_Baseline_SelValueDescription, "")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV140TFContagemResultado_PFBFSUltima) && (Convert.ToDecimal(0)==AV141TFContagemResultado_PFBFSUltima_To) ) )
         {
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFB FS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV140TFContagemResultado_PFBFSUltima, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFB FS (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV141TFContagemResultado_PFBFSUltima_To, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV142TFContagemResultado_PFLFSUltima) && (Convert.ToDecimal(0)==AV143TFContagemResultado_PFLFSUltima_To) ) )
         {
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFL FS", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV142TFContagemResultado_PFLFSUltima, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFL FS (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV143TFContagemResultado_PFLFSUltima_To, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV144TFContagemResultado_PFBFMUltima) && (Convert.ToDecimal(0)==AV145TFContagemResultado_PFBFMUltima_To) ) )
         {
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFB FM", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV144TFContagemResultado_PFBFMUltima, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFB FM (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV145TFContagemResultado_PFBFMUltima_To, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV146TFContagemResultado_PFLFMUltima) && (Convert.ToDecimal(0)==AV147TFContagemResultado_PFLFMUltima_To) ) )
         {
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFL FM", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV146TFContagemResultado_PFLFMUltima, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PFL FM (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV147TFContagemResultado_PFLFMUltima_To, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV148TFContagemResultado_PFFinal) && (Convert.ToDecimal(0)==AV149TFContagemResultado_PFFinal_To) ) )
         {
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PF Final", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV148TFContagemResultado_PFFinal, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
            H540( false, 20) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PF Final (At�)", 5, Gx_line+2, 186, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV149TFContagemResultado_PFFinal_To, "ZZ,ZZZ,ZZ9.999")), 188, Gx_line+2, 1035, Gx_line+17, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
      }

      protected void S131( )
      {
         /* 'PRINTCOLUMNTITLES' Routine */
         H540( false, 35) ;
         getPrinter().GxDrawLine(5, Gx_line+30, 105, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(110, Gx_line+30, 160, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(165, Gx_line+30, 215, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(220, Gx_line+30, 320, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(325, Gx_line+30, 375, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(380, Gx_line+30, 430, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(435, Gx_line+30, 485, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(490, Gx_line+30, 590, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(595, Gx_line+30, 695, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(700, Gx_line+30, 750, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(755, Gx_line+30, 805, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(810, Gx_line+30, 860, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(865, Gx_line+30, 915, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(920, Gx_line+30, 970, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxDrawLine(975, Gx_line+30, 1025, Gx_line+30, 1, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("OS Ref|OS", 5, Gx_line+15, 105, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Demanda", 110, Gx_line+15, 160, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Contagem", 165, Gx_line+15, 215, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Sistema", 220, Gx_line+15, 320, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Prestadora", 325, Gx_line+15, 375, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Servi�o", 380, Gx_line+15, 430, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Deflator", 435, Gx_line+15, 485, Gx_line+29, 2, 0, 0, 0) ;
         getPrinter().GxDrawText("Status Dmn", 490, Gx_line+15, 590, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("Status Cnt", 595, Gx_line+15, 695, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("BS", 700, Gx_line+15, 750, Gx_line+29, 0, 0, 0, 0) ;
         getPrinter().GxDrawText("PFB FS", 755, Gx_line+15, 805, Gx_line+29, 2, 0, 0, 0) ;
         getPrinter().GxDrawText("PFL FS", 810, Gx_line+15, 860, Gx_line+29, 2, 0, 0, 0) ;
         getPrinter().GxDrawText("PFB FM", 865, Gx_line+15, 915, Gx_line+29, 2, 0, 0, 0) ;
         getPrinter().GxDrawText("PFL FM", 920, Gx_line+15, 970, Gx_line+29, 2, 0, 0, 0) ;
         getPrinter().GxDrawText("PF Final", 975, Gx_line+15, 1025, Gx_line+29, 2, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+35);
      }

      protected void S141( )
      {
         /* 'PRINTDATA' Routine */
         AV168ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod = AV12Contratada_AreaTrabalhoCod;
         AV169ExtraWWContagemResultadoOSDS_2_Contratada_codigo = AV112Contratada_Codigo;
         AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV171ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = AV17ContagemResultado_OsFsOsFm1;
         AV173ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 = AV18ContagemResultado_DataDmn1;
         AV174ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 = AV19ContagemResultado_DataDmn_To1;
         AV175ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 = AV96ContagemResultado_DataUltCnt1;
         AV176ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 = AV97ContagemResultado_DataUltCnt_To1;
         AV177ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 = AV20ContagemResultado_ContadorFM1;
         AV178ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 = AV21ContagemResultado_SistemaCod1;
         AV179ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 = AV22ContagemResultado_ContratadaCod1;
         AV180ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 = AV23ContagemResultado_NaoCnfDmnCod1;
         AV181ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 = AV24ContagemResultado_StatusDmn1;
         AV182ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 = AV104ContagemResultado_StatusDmnVnc1;
         AV183ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 = AV29ContagemResultado_EsforcoSoma1;
         AV184ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 = AV30ContagemResultado_Baseline1;
         AV185ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 = AV33ContagemResultado_Servico1;
         AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = AV34DynamicFiltersEnabled2;
         AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = AV35DynamicFiltersSelector2;
         AV188ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 = AV36DynamicFiltersOperator2;
         AV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = AV37ContagemResultado_OsFsOsFm2;
         AV190ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 = AV38ContagemResultado_DataDmn2;
         AV191ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 = AV39ContagemResultado_DataDmn_To2;
         AV192ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 = AV98ContagemResultado_DataUltCnt2;
         AV193ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 = AV99ContagemResultado_DataUltCnt_To2;
         AV194ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 = AV40ContagemResultado_ContadorFM2;
         AV195ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 = AV41ContagemResultado_SistemaCod2;
         AV196ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 = AV42ContagemResultado_ContratadaCod2;
         AV197ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 = AV43ContagemResultado_NaoCnfDmnCod2;
         AV198ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 = AV44ContagemResultado_StatusDmn2;
         AV199ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 = AV107ContagemResultado_StatusDmnVnc2;
         AV200ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 = AV48ContagemResultado_EsforcoSoma2;
         AV201ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 = AV49ContagemResultado_Baseline2;
         AV202ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 = AV51ContagemResultado_Servico2;
         AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = AV52DynamicFiltersEnabled3;
         AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = AV53DynamicFiltersSelector3;
         AV205ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 = AV54DynamicFiltersOperator3;
         AV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = AV55ContagemResultado_OsFsOsFm3;
         AV207ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 = AV56ContagemResultado_DataDmn3;
         AV208ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 = AV57ContagemResultado_DataDmn_To3;
         AV209ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 = AV100ContagemResultado_DataUltCnt3;
         AV210ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 = AV101ContagemResultado_DataUltCnt_To3;
         AV211ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 = AV58ContagemResultado_ContadorFM3;
         AV212ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 = AV59ContagemResultado_SistemaCod3;
         AV213ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 = AV60ContagemResultado_ContratadaCod3;
         AV214ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 = AV61ContagemResultado_NaoCnfDmnCod3;
         AV215ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 = AV62ContagemResultado_StatusDmn3;
         AV216ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 = AV109ContagemResultado_StatusDmnVnc3;
         AV217ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 = AV66ContagemResultado_EsforcoSoma3;
         AV218ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 = AV67ContagemResultado_Baseline3;
         AV219ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 = AV69ContagemResultado_Servico3;
         AV220ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm = AV117TFContagemResultado_OsFsOsFm;
         AV221ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel = AV118TFContagemResultado_OsFsOsFm_Sel;
         AV222ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn = AV119TFContagemResultado_DataDmn;
         AV223ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to = AV120TFContagemResultado_DataDmn_To;
         AV224ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt = AV121TFContagemResultado_DataUltCnt;
         AV225ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to = AV122TFContagemResultado_DataUltCnt_To;
         AV226ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla = AV123TFContagemrResultado_SistemaSigla;
         AV227ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel = AV124TFContagemrResultado_SistemaSigla_Sel;
         AV228ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla = AV125TFContagemResultado_ContratadaSigla;
         AV229ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel = AV126TFContagemResultado_ContratadaSigla_Sel;
         AV230ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla = AV127TFContagemResultado_ServicoSigla;
         AV231ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel = AV128TFContagemResultado_ServicoSigla_Sel;
         AV232ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt = AV129TFContagemResultado_DeflatorCnt;
         AV233ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to = AV130TFContagemResultado_DeflatorCnt_To;
         AV234ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels = AV133TFContagemResultado_StatusDmn_Sels;
         AV235ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels = AV137TFContagemResultado_StatusUltCnt_Sels;
         AV236ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel = AV139TFContagemResultado_Baseline_Sel;
         AV237ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima = AV140TFContagemResultado_PFBFSUltima;
         AV238ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to = AV141TFContagemResultado_PFBFSUltima_To;
         AV239ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima = AV142TFContagemResultado_PFLFSUltima;
         AV240ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to = AV143TFContagemResultado_PFLFSUltima_To;
         AV241ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima = AV144TFContagemResultado_PFBFMUltima;
         AV242ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to = AV145TFContagemResultado_PFBFMUltima_To;
         AV243ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima = AV146TFContagemResultado_PFLFMUltima;
         AV244ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to = AV147TFContagemResultado_PFLFMUltima_To;
         AV245ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal = AV148TFContagemResultado_PFFinal;
         AV246ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to = AV149TFContagemResultado_PFFinal_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A531ContagemResultado_StatusUltCnt ,
                                              AV235ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV234ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels ,
                                              AV168ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod ,
                                              AV154Contratada_DoUsuario ,
                                              AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 ,
                                              AV171ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 ,
                                              AV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 ,
                                              AV173ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 ,
                                              AV174ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 ,
                                              AV178ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 ,
                                              AV179ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 ,
                                              AV180ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 ,
                                              AV181ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 ,
                                              AV182ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 ,
                                              AV183ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 ,
                                              AV184ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 ,
                                              AV185ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 ,
                                              AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 ,
                                              AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 ,
                                              AV188ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 ,
                                              AV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 ,
                                              AV190ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 ,
                                              AV191ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 ,
                                              AV195ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 ,
                                              AV196ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 ,
                                              AV197ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 ,
                                              AV198ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 ,
                                              AV199ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 ,
                                              AV200ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 ,
                                              AV201ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 ,
                                              AV202ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 ,
                                              AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 ,
                                              AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 ,
                                              AV205ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 ,
                                              AV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 ,
                                              AV207ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 ,
                                              AV208ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 ,
                                              AV212ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 ,
                                              AV213ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 ,
                                              AV214ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 ,
                                              AV215ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 ,
                                              AV216ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 ,
                                              AV217ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 ,
                                              AV218ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 ,
                                              AV219ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 ,
                                              AV221ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel ,
                                              AV220ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm ,
                                              AV222ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn ,
                                              AV223ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to ,
                                              AV227ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel ,
                                              AV226ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla ,
                                              AV229ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel ,
                                              AV228ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla ,
                                              AV231ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel ,
                                              AV230ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla ,
                                              AV234ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV236ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel ,
                                              AV71GridState.gxTpr_Dynamicfilters.Count ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A489ContagemResultado_SistemaCod ,
                                              A468ContagemResultado_NaoCnfDmnCod ,
                                              A771ContagemResultado_StatusDmnVnc ,
                                              A510ContagemResultado_EsforcoSoma ,
                                              A598ContagemResultado_Baseline ,
                                              A601ContagemResultado_Servico ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A803ContagemResultado_ContratadaSigla ,
                                              A801ContagemResultado_ServicoSigla ,
                                              A456ContagemResultado_Codigo ,
                                              AV10OrderedBy ,
                                              AV11OrderedDsc ,
                                              AV175ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV176ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 ,
                                              AV177ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 ,
                                              A584ContagemResultado_ContadorFM ,
                                              A508ContagemResultado_Owner ,
                                              AV192ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 ,
                                              AV193ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 ,
                                              AV194ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 ,
                                              AV209ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 ,
                                              AV210ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 ,
                                              AV211ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 ,
                                              AV224ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt ,
                                              AV225ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to ,
                                              AV232ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt ,
                                              A802ContagemResultado_DeflatorCnt ,
                                              AV233ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to ,
                                              AV235ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels.Count ,
                                              AV237ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima ,
                                              A684ContagemResultado_PFBFSUltima ,
                                              AV238ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to ,
                                              AV239ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima ,
                                              A685ContagemResultado_PFLFSUltima ,
                                              AV240ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to ,
                                              AV241ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima ,
                                              A682ContagemResultado_PFBFMUltima ,
                                              AV242ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to ,
                                              AV243ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima ,
                                              A683ContagemResultado_PFLFMUltima ,
                                              AV244ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to ,
                                              AV245ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ,
                                              A574ContagemResultado_PFFinal ,
                                              AV246ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT
                                              }
         });
         lV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV220ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm = StringUtil.Concat( StringUtil.RTrim( AV220ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm), "%", "");
         lV226ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV226ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla), 25, "%");
         lV228ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla = StringUtil.PadR( StringUtil.RTrim( AV228ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla), 15, "%");
         lV230ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV230ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P00544 */
         pr_default.execute(0, new Object[] {AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, AV175ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1, AV175ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1, AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, AV176ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1, AV176ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1, AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, AV177ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1, AV177ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1, AV177ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1, AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2, AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, AV192ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2, AV192ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2, AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2, AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, AV193ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2, AV193ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2, AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2, AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, AV194ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2, AV194ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2, AV194ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2, AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3, AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, AV209ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3, AV209ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3, AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3, AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, AV210ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3, AV210ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3, AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3, AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, AV211ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3, AV211ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3, AV211ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3, AV224ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt, AV224ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt, AV225ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to, AV225ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to, AV232ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt, AV232ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt, AV233ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to, AV233ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to, AV235ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels.Count, AV237ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima, AV237ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima, AV238ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to, AV238ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to, AV239ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima, AV239ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima, AV240ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to, AV240ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to, AV241ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima, AV241ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima, AV242ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to, AV242ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to, AV243ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima, AV243ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima, AV244ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to, AV244ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to, AV168ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod, AV154Contratada_DoUsuario, AV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, AV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, AV173ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1, AV174ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1, AV178ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1, AV179ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1, AV180ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1, AV181ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1, AV182ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1, AV183ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1, AV183ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1, AV183ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1, AV185ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1, AV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, AV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, AV190ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2, AV191ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2, AV195ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2, AV196ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2, AV197ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2, AV198ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2, AV199ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2, AV200ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2, AV200ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2, AV200ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2, AV202ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2, AV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, AV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, AV207ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3, AV208ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3, AV212ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3, AV213ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3, AV214ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3, AV215ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3, AV216ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3, AV217ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3, AV217ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3, AV217ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3, AV219ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3, lV220ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm, AV221ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel, AV222ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn, AV223ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to, lV226ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla, AV227ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel, lV228ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla, AV229ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel,
         lV230ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla, AV231ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00544_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00544_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = P00544_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00544_n602ContagemResultado_OSVinculada[0];
            A1583ContagemResultado_TipoRegistro = P00544_A1583ContagemResultado_TipoRegistro[0];
            A801ContagemResultado_ServicoSigla = P00544_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00544_n801ContagemResultado_ServicoSigla[0];
            A803ContagemResultado_ContratadaSigla = P00544_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00544_n803ContagemResultado_ContratadaSigla[0];
            A509ContagemrResultado_SistemaSigla = P00544_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00544_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00544_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00544_n601ContagemResultado_Servico[0];
            A598ContagemResultado_Baseline = P00544_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P00544_n598ContagemResultado_Baseline[0];
            A771ContagemResultado_StatusDmnVnc = P00544_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = P00544_n771ContagemResultado_StatusDmnVnc[0];
            A484ContagemResultado_StatusDmn = P00544_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00544_n484ContagemResultado_StatusDmn[0];
            A468ContagemResultado_NaoCnfDmnCod = P00544_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P00544_n468ContagemResultado_NaoCnfDmnCod[0];
            A489ContagemResultado_SistemaCod = P00544_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00544_n489ContagemResultado_SistemaCod[0];
            A508ContagemResultado_Owner = P00544_A508ContagemResultado_Owner[0];
            A471ContagemResultado_DataDmn = P00544_A471ContagemResultado_DataDmn[0];
            A490ContagemResultado_ContratadaCod = P00544_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00544_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00544_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00544_n52Contratada_AreaTrabalhoCod[0];
            A683ContagemResultado_PFLFMUltima = P00544_A683ContagemResultado_PFLFMUltima[0];
            A682ContagemResultado_PFBFMUltima = P00544_A682ContagemResultado_PFBFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P00544_A685ContagemResultado_PFLFSUltima[0];
            A684ContagemResultado_PFBFSUltima = P00544_A684ContagemResultado_PFBFSUltima[0];
            A531ContagemResultado_StatusUltCnt = P00544_A531ContagemResultado_StatusUltCnt[0];
            A802ContagemResultado_DeflatorCnt = P00544_A802ContagemResultado_DeflatorCnt[0];
            A510ContagemResultado_EsforcoSoma = P00544_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P00544_n510ContagemResultado_EsforcoSoma[0];
            A584ContagemResultado_ContadorFM = P00544_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P00544_A566ContagemResultado_DataUltCnt[0];
            A493ContagemResultado_DemandaFM = P00544_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00544_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00544_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00544_n457ContagemResultado_Demanda[0];
            A456ContagemResultado_Codigo = P00544_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = P00544_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00544_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00544_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00544_n801ContagemResultado_ServicoSigla[0];
            A771ContagemResultado_StatusDmnVnc = P00544_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = P00544_n771ContagemResultado_StatusDmnVnc[0];
            A509ContagemrResultado_SistemaSigla = P00544_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00544_n509ContagemrResultado_SistemaSigla[0];
            A803ContagemResultado_ContratadaSigla = P00544_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00544_n803ContagemResultado_ContratadaSigla[0];
            A52Contratada_AreaTrabalhoCod = P00544_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00544_n52Contratada_AreaTrabalhoCod[0];
            A683ContagemResultado_PFLFMUltima = P00544_A683ContagemResultado_PFLFMUltima[0];
            A682ContagemResultado_PFBFMUltima = P00544_A682ContagemResultado_PFBFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P00544_A685ContagemResultado_PFLFSUltima[0];
            A684ContagemResultado_PFBFSUltima = P00544_A684ContagemResultado_PFBFSUltima[0];
            A531ContagemResultado_StatusUltCnt = P00544_A531ContagemResultado_StatusUltCnt[0];
            A802ContagemResultado_DeflatorCnt = P00544_A802ContagemResultado_DeflatorCnt[0];
            A584ContagemResultado_ContadorFM = P00544_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P00544_A566ContagemResultado_DataUltCnt[0];
            A510ContagemResultado_EsforcoSoma = P00544_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P00544_n510ContagemResultado_EsforcoSoma[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            if ( (Convert.ToDecimal(0)==AV245ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal) || ( ( A574ContagemResultado_PFFinal >= AV245ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ) ) )
            {
               if ( (Convert.ToDecimal(0)==AV246ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to) || ( ( A574ContagemResultado_PFFinal <= AV246ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ) ) )
               {
                  A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
                  AV13ContagemResultado_StatusDmnDescription = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
                  AV87ContagemResultado_StatusUltCntDescription = gxdomainstatuscontagem.getDescription(context,A531ContagemResultado_StatusUltCnt);
                  if ( StringUtil.StrCmp(StringUtil.Trim( StringUtil.BoolToStr( A598ContagemResultado_Baseline)), "True") == 0 )
                  {
                     AV14ContagemResultado_BaselineDescription = "*";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( StringUtil.BoolToStr( A598ContagemResultado_Baseline)), "False") == 0 )
                  {
                     AV14ContagemResultado_BaselineDescription = "";
                  }
                  /* Execute user subroutine: 'BEFOREPRINTLINE' */
                  S152 ();
                  if ( returnInSub )
                  {
                     pr_default.close(0);
                     returnInSub = true;
                     if (true) return;
                  }
                  H540( false, 20) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, "")), 5, Gx_line+2, 105, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"), 110, Gx_line+2, 160, Gx_line+17, 2, 0, 0, 0) ;
                  getPrinter().GxDrawText(context.localUtil.Format( A566ContagemResultado_DataUltCnt, "99/99/99"), 165, Gx_line+2, 215, Gx_line+17, 2, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), 220, Gx_line+2, 320, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A803ContagemResultado_ContratadaSigla, "@!")), 325, Gx_line+2, 375, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A801ContagemResultado_ServicoSigla, "@!")), 380, Gx_line+2, 430, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A802ContagemResultado_DeflatorCnt, "Z9.999")), 435, Gx_line+2, 485, Gx_line+17, 2, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV13ContagemResultado_StatusDmnDescription, "")), 490, Gx_line+2, 590, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV87ContagemResultado_StatusUltCntDescription, "")), 595, Gx_line+2, 695, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV14ContagemResultado_BaselineDescription, "")), 700, Gx_line+2, 750, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A684ContagemResultado_PFBFSUltima, "ZZ,ZZZ,ZZ9.999")), 755, Gx_line+2, 805, Gx_line+17, 2, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A685ContagemResultado_PFLFSUltima, "ZZ,ZZZ,ZZ9.999")), 810, Gx_line+2, 860, Gx_line+17, 2, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A682ContagemResultado_PFBFMUltima, "ZZ,ZZZ,ZZ9.999")), 865, Gx_line+2, 915, Gx_line+17, 2, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A683ContagemResultado_PFLFMUltima, "ZZ,ZZZ,ZZ9.999")), 920, Gx_line+2, 970, Gx_line+17, 2, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A574ContagemResultado_PFFinal, "ZZ,ZZZ,ZZ9.999")), 975, Gx_line+2, 1025, Gx_line+17, 2, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+20);
                  /* Execute user subroutine: 'AFTERPRINTLINE' */
                  S162 ();
                  if ( returnInSub )
                  {
                     pr_default.close(0);
                     returnInSub = true;
                     if (true) return;
                  }
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void S152( )
      {
         /* 'BEFOREPRINTLINE' Routine */
      }

      protected void S162( )
      {
         /* 'AFTERPRINTLINE' Routine */
      }

      protected void S171( )
      {
         /* 'PRINTFOOTER' Routine */
      }

      protected void H540( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, true, 56, 14, 70, 118,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 18, 22, 35, 35, 56, 42, 12, 21, 21, 25, 37, 18, 21, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 18, 18, 37, 37, 37, 35, 64, 42, 42, 45, 45, 42, 38, 49, 45, 18, 32, 42, 35, 53, 45, 49, 42, 49, 45, 42, 38, 45, 42, 61, 42, 42, 38, 18, 18, 18, 30, 35, 21, 35, 35, 32, 35, 35, 18, 35, 35, 14, 14, 32, 14, 52, 35, 35, 35, 35, 21, 32, 18, 35, 32, 45, 32, 32, 29, 21, 16, 21, 37, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 21, 35, 35, 34, 35, 16, 35, 21, 46, 23, 35, 37, 21, 46, 35, 25, 35, 21, 20, 21, 35, 34, 21, 21, 20, 23, 35, 53, 53, 53, 38, 42, 42, 42, 42, 42, 42, 63, 45, 42, 42, 42, 42, 18, 18, 18, 18, 45, 45, 49, 49, 49, 49, 49, 37, 49, 45, 45, 45, 45, 42, 42, 38, 35, 35, 35, 35, 35, 35, 56, 32, 35, 35, 35, 35, 18, 18, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 38, 35, 35, 35, 35, 32, 35, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV71GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV72GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV15DynamicFiltersSelector1 = "";
         AV17ContagemResultado_OsFsOsFm1 = "";
         AV114FilterContagemResultado_OsFsOsFmDescription = "";
         AV73ContagemResultado_OsFsOsFm = "";
         AV18ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV19ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV74FilterContagemResultado_DataDmnDescription = "";
         AV75ContagemResultado_DataDmn = DateTime.MinValue;
         AV96ContagemResultado_DataUltCnt1 = DateTime.MinValue;
         AV97ContagemResultado_DataUltCnt_To1 = DateTime.MinValue;
         AV102FilterContagemResultado_DataUltCntDescription = "";
         AV103ContagemResultado_DataUltCnt = DateTime.MinValue;
         AV24ContagemResultado_StatusDmn1 = "";
         AV26FilterContagemResultado_StatusDmn1ValueDescription = "";
         AV25FilterContagemResultado_StatusDmnValueDescription = "";
         AV104ContagemResultado_StatusDmnVnc1 = "";
         AV106FilterContagemResultado_StatusDmnVnc1ValueDescription = "";
         AV105FilterContagemResultado_StatusDmnVncValueDescription = "";
         AV83FilterContagemResultado_EsforcoSomaDescription = "";
         AV30ContagemResultado_Baseline1 = "";
         AV32FilterContagemResultado_Baseline1ValueDescription = "";
         AV31FilterContagemResultado_BaselineValueDescription = "";
         AV35DynamicFiltersSelector2 = "";
         AV37ContagemResultado_OsFsOsFm2 = "";
         AV38ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV39ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV98ContagemResultado_DataUltCnt2 = DateTime.MinValue;
         AV99ContagemResultado_DataUltCnt_To2 = DateTime.MinValue;
         AV44ContagemResultado_StatusDmn2 = "";
         AV45FilterContagemResultado_StatusDmn2ValueDescription = "";
         AV107ContagemResultado_StatusDmnVnc2 = "";
         AV108FilterContagemResultado_StatusDmnVnc2ValueDescription = "";
         AV49ContagemResultado_Baseline2 = "";
         AV50FilterContagemResultado_Baseline2ValueDescription = "";
         AV53DynamicFiltersSelector3 = "";
         AV55ContagemResultado_OsFsOsFm3 = "";
         AV56ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV57ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV100ContagemResultado_DataUltCnt3 = DateTime.MinValue;
         AV101ContagemResultado_DataUltCnt_To3 = DateTime.MinValue;
         AV62ContagemResultado_StatusDmn3 = "";
         AV63FilterContagemResultado_StatusDmn3ValueDescription = "";
         AV109ContagemResultado_StatusDmnVnc3 = "";
         AV110FilterContagemResultado_StatusDmnVnc3ValueDescription = "";
         AV67ContagemResultado_Baseline3 = "";
         AV68FilterContagemResultado_Baseline3ValueDescription = "";
         AV133TFContagemResultado_StatusDmn_Sels = new GxSimpleCollection();
         AV134TFContagemResultado_StatusDmn_Sel = "";
         AV132TFContagemResultado_StatusDmn_SelDscs = "";
         AV150FilterTFContagemResultado_StatusDmn_SelValueDescription = "";
         AV137TFContagemResultado_StatusUltCnt_Sels = new GxSimpleCollection();
         AV136TFContagemResultado_StatusUltCnt_SelDscs = "";
         AV151FilterTFContagemResultado_StatusUltCnt_SelValueDescription = "";
         AV152FilterTFContagemResultado_Baseline_SelValueDescription = "";
         AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = "";
         AV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = "";
         AV173ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV174ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV175ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 = DateTime.MinValue;
         AV176ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 = DateTime.MinValue;
         AV181ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 = "";
         AV182ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 = "";
         AV184ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 = "";
         AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = "";
         AV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = "";
         AV190ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV191ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV192ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 = DateTime.MinValue;
         AV193ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 = DateTime.MinValue;
         AV198ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 = "";
         AV199ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 = "";
         AV201ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 = "";
         AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = "";
         AV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = "";
         AV207ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV208ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV209ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 = DateTime.MinValue;
         AV210ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 = DateTime.MinValue;
         AV215ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 = "";
         AV216ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 = "";
         AV218ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 = "";
         AV220ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm = "";
         AV221ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel = "";
         AV222ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn = DateTime.MinValue;
         AV223ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to = DateTime.MinValue;
         AV224ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt = DateTime.MinValue;
         AV225ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to = DateTime.MinValue;
         AV226ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla = "";
         AV227ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel = "";
         AV228ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla = "";
         AV229ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel = "";
         AV230ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla = "";
         AV231ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel = "";
         AV234ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels = new GxSimpleCollection();
         AV235ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = "";
         lV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = "";
         lV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = "";
         lV220ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm = "";
         lV226ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla = "";
         lV228ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla = "";
         lV230ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla = "";
         A484ContagemResultado_StatusDmn = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A771ContagemResultado_StatusDmnVnc = "";
         A509ContagemrResultado_SistemaSigla = "";
         A803ContagemResultado_ContratadaSigla = "";
         A801ContagemResultado_ServicoSigla = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         P00544_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00544_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00544_A602ContagemResultado_OSVinculada = new int[1] ;
         P00544_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00544_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P00544_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00544_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00544_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00544_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00544_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00544_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00544_A601ContagemResultado_Servico = new int[1] ;
         P00544_n601ContagemResultado_Servico = new bool[] {false} ;
         P00544_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00544_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00544_A771ContagemResultado_StatusDmnVnc = new String[] {""} ;
         P00544_n771ContagemResultado_StatusDmnVnc = new bool[] {false} ;
         P00544_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00544_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00544_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P00544_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P00544_A489ContagemResultado_SistemaCod = new int[1] ;
         P00544_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00544_A508ContagemResultado_Owner = new int[1] ;
         P00544_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00544_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00544_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00544_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00544_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00544_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         P00544_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00544_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         P00544_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00544_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00544_A802ContagemResultado_DeflatorCnt = new decimal[1] ;
         P00544_A510ContagemResultado_EsforcoSoma = new int[1] ;
         P00544_n510ContagemResultado_EsforcoSoma = new bool[] {false} ;
         P00544_A584ContagemResultado_ContadorFM = new int[1] ;
         P00544_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00544_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00544_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00544_A457ContagemResultado_Demanda = new String[] {""} ;
         P00544_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00544_A456ContagemResultado_Codigo = new int[1] ;
         A501ContagemResultado_OsFsOsFm = "";
         AV13ContagemResultado_StatusDmnDescription = "";
         AV87ContagemResultado_StatusUltCntDescription = "";
         AV14ContagemResultado_BaselineDescription = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.exportreportextrawwcontagemresultadoos__default(),
            new Object[][] {
                new Object[] {
               P00544_A1553ContagemResultado_CntSrvCod, P00544_n1553ContagemResultado_CntSrvCod, P00544_A602ContagemResultado_OSVinculada, P00544_n602ContagemResultado_OSVinculada, P00544_A1583ContagemResultado_TipoRegistro, P00544_A801ContagemResultado_ServicoSigla, P00544_n801ContagemResultado_ServicoSigla, P00544_A803ContagemResultado_ContratadaSigla, P00544_n803ContagemResultado_ContratadaSigla, P00544_A509ContagemrResultado_SistemaSigla,
               P00544_n509ContagemrResultado_SistemaSigla, P00544_A601ContagemResultado_Servico, P00544_n601ContagemResultado_Servico, P00544_A598ContagemResultado_Baseline, P00544_n598ContagemResultado_Baseline, P00544_A771ContagemResultado_StatusDmnVnc, P00544_n771ContagemResultado_StatusDmnVnc, P00544_A484ContagemResultado_StatusDmn, P00544_n484ContagemResultado_StatusDmn, P00544_A468ContagemResultado_NaoCnfDmnCod,
               P00544_n468ContagemResultado_NaoCnfDmnCod, P00544_A489ContagemResultado_SistemaCod, P00544_n489ContagemResultado_SistemaCod, P00544_A508ContagemResultado_Owner, P00544_A471ContagemResultado_DataDmn, P00544_A490ContagemResultado_ContratadaCod, P00544_n490ContagemResultado_ContratadaCod, P00544_A52Contratada_AreaTrabalhoCod, P00544_n52Contratada_AreaTrabalhoCod, P00544_A683ContagemResultado_PFLFMUltima,
               P00544_A682ContagemResultado_PFBFMUltima, P00544_A685ContagemResultado_PFLFSUltima, P00544_A684ContagemResultado_PFBFSUltima, P00544_A531ContagemResultado_StatusUltCnt, P00544_A802ContagemResultado_DeflatorCnt, P00544_A510ContagemResultado_EsforcoSoma, P00544_n510ContagemResultado_EsforcoSoma, P00544_A584ContagemResultado_ContadorFM, P00544_A566ContagemResultado_DataUltCnt, P00544_A493ContagemResultado_DemandaFM,
               P00544_n493ContagemResultado_DemandaFM, P00544_A457ContagemResultado_Demanda, P00544_n457ContagemResultado_Demanda, P00544_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV139TFContagemResultado_Baseline_Sel ;
      private short AV10OrderedBy ;
      private short GxWebError ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV36DynamicFiltersOperator2 ;
      private short AV54DynamicFiltersOperator3 ;
      private short AV138TFContagemResultado_StatusUltCnt_Sel ;
      private short AV171ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 ;
      private short AV188ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 ;
      private short AV205ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 ;
      private short AV236ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short A1583ContagemResultado_TipoRegistro ;
      private int AV12Contratada_AreaTrabalhoCod ;
      private int AV112Contratada_Codigo ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int AV154Contratada_DoUsuario ;
      private int Gx_OldLine ;
      private int AV20ContagemResultado_ContadorFM1 ;
      private int AV76ContagemResultado_ContadorFM ;
      private int AV21ContagemResultado_SistemaCod1 ;
      private int AV77ContagemResultado_SistemaCod ;
      private int AV22ContagemResultado_ContratadaCod1 ;
      private int AV78ContagemResultado_ContratadaCod ;
      private int AV23ContagemResultado_NaoCnfDmnCod1 ;
      private int AV79ContagemResultado_NaoCnfDmnCod ;
      private int AV29ContagemResultado_EsforcoSoma1 ;
      private int AV84ContagemResultado_EsforcoSoma ;
      private int AV33ContagemResultado_Servico1 ;
      private int AV86ContagemResultado_Servico ;
      private int AV40ContagemResultado_ContadorFM2 ;
      private int AV41ContagemResultado_SistemaCod2 ;
      private int AV42ContagemResultado_ContratadaCod2 ;
      private int AV43ContagemResultado_NaoCnfDmnCod2 ;
      private int AV48ContagemResultado_EsforcoSoma2 ;
      private int AV51ContagemResultado_Servico2 ;
      private int AV58ContagemResultado_ContadorFM3 ;
      private int AV59ContagemResultado_SistemaCod3 ;
      private int AV60ContagemResultado_ContratadaCod3 ;
      private int AV61ContagemResultado_NaoCnfDmnCod3 ;
      private int AV66ContagemResultado_EsforcoSoma3 ;
      private int AV69ContagemResultado_Servico3 ;
      private int AV165GXV1 ;
      private int AV166GXV2 ;
      private int AV168ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod ;
      private int AV169ExtraWWContagemResultadoOSDS_2_Contratada_codigo ;
      private int AV177ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 ;
      private int AV178ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 ;
      private int AV179ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 ;
      private int AV180ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 ;
      private int AV183ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 ;
      private int AV185ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 ;
      private int AV194ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 ;
      private int AV195ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 ;
      private int AV196ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 ;
      private int AV197ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 ;
      private int AV200ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 ;
      private int AV202ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 ;
      private int AV211ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 ;
      private int AV212ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 ;
      private int AV213ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 ;
      private int AV214ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 ;
      private int AV217ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 ;
      private int AV219ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 ;
      private int AV234ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels_Count ;
      private int AV71GridState_gxTpr_Dynamicfilters_Count ;
      private int AV235ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels_Count ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A510ContagemResultado_EsforcoSoma ;
      private int A601ContagemResultado_Servico ;
      private int A456ContagemResultado_Codigo ;
      private int A584ContagemResultado_ContadorFM ;
      private int A508ContagemResultado_Owner ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A602ContagemResultado_OSVinculada ;
      private long AV153i ;
      private decimal AV129TFContagemResultado_DeflatorCnt ;
      private decimal AV130TFContagemResultado_DeflatorCnt_To ;
      private decimal AV140TFContagemResultado_PFBFSUltima ;
      private decimal AV141TFContagemResultado_PFBFSUltima_To ;
      private decimal AV142TFContagemResultado_PFLFSUltima ;
      private decimal AV143TFContagemResultado_PFLFSUltima_To ;
      private decimal AV144TFContagemResultado_PFBFMUltima ;
      private decimal AV145TFContagemResultado_PFBFMUltima_To ;
      private decimal AV146TFContagemResultado_PFLFMUltima ;
      private decimal AV147TFContagemResultado_PFLFMUltima_To ;
      private decimal AV148TFContagemResultado_PFFinal ;
      private decimal AV149TFContagemResultado_PFFinal_To ;
      private decimal AV232ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt ;
      private decimal AV233ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to ;
      private decimal AV237ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima ;
      private decimal AV238ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to ;
      private decimal AV239ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima ;
      private decimal AV240ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to ;
      private decimal AV241ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima ;
      private decimal AV242ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to ;
      private decimal AV243ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima ;
      private decimal AV244ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to ;
      private decimal AV245ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ;
      private decimal AV246ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ;
      private decimal A802ContagemResultado_DeflatorCnt ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A683ContagemResultado_PFLFMUltima ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV123TFContagemrResultado_SistemaSigla ;
      private String AV124TFContagemrResultado_SistemaSigla_Sel ;
      private String AV125TFContagemResultado_ContratadaSigla ;
      private String AV126TFContagemResultado_ContratadaSigla_Sel ;
      private String AV127TFContagemResultado_ServicoSigla ;
      private String AV128TFContagemResultado_ServicoSigla_Sel ;
      private String AV24ContagemResultado_StatusDmn1 ;
      private String AV104ContagemResultado_StatusDmnVnc1 ;
      private String AV30ContagemResultado_Baseline1 ;
      private String AV44ContagemResultado_StatusDmn2 ;
      private String AV107ContagemResultado_StatusDmnVnc2 ;
      private String AV49ContagemResultado_Baseline2 ;
      private String AV62ContagemResultado_StatusDmn3 ;
      private String AV109ContagemResultado_StatusDmnVnc3 ;
      private String AV67ContagemResultado_Baseline3 ;
      private String AV134TFContagemResultado_StatusDmn_Sel ;
      private String AV181ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 ;
      private String AV182ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 ;
      private String AV184ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 ;
      private String AV198ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 ;
      private String AV199ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 ;
      private String AV201ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 ;
      private String AV215ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 ;
      private String AV216ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 ;
      private String AV218ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 ;
      private String AV226ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla ;
      private String AV227ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel ;
      private String AV228ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla ;
      private String AV229ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel ;
      private String AV230ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla ;
      private String AV231ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel ;
      private String scmdbuf ;
      private String lV226ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla ;
      private String lV228ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla ;
      private String lV230ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String A771ContagemResultado_StatusDmnVnc ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A801ContagemResultado_ServicoSigla ;
      private DateTime AV119TFContagemResultado_DataDmn ;
      private DateTime AV120TFContagemResultado_DataDmn_To ;
      private DateTime AV121TFContagemResultado_DataUltCnt ;
      private DateTime AV122TFContagemResultado_DataUltCnt_To ;
      private DateTime AV18ContagemResultado_DataDmn1 ;
      private DateTime AV19ContagemResultado_DataDmn_To1 ;
      private DateTime AV75ContagemResultado_DataDmn ;
      private DateTime AV96ContagemResultado_DataUltCnt1 ;
      private DateTime AV97ContagemResultado_DataUltCnt_To1 ;
      private DateTime AV103ContagemResultado_DataUltCnt ;
      private DateTime AV38ContagemResultado_DataDmn2 ;
      private DateTime AV39ContagemResultado_DataDmn_To2 ;
      private DateTime AV98ContagemResultado_DataUltCnt2 ;
      private DateTime AV99ContagemResultado_DataUltCnt_To2 ;
      private DateTime AV56ContagemResultado_DataDmn3 ;
      private DateTime AV57ContagemResultado_DataDmn_To3 ;
      private DateTime AV100ContagemResultado_DataUltCnt3 ;
      private DateTime AV101ContagemResultado_DataUltCnt_To3 ;
      private DateTime AV173ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 ;
      private DateTime AV174ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 ;
      private DateTime AV175ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 ;
      private DateTime AV176ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 ;
      private DateTime AV190ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 ;
      private DateTime AV191ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 ;
      private DateTime AV192ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 ;
      private DateTime AV193ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 ;
      private DateTime AV207ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 ;
      private DateTime AV208ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 ;
      private DateTime AV209ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 ;
      private DateTime AV210ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 ;
      private DateTime AV222ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn ;
      private DateTime AV223ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to ;
      private DateTime AV224ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt ;
      private DateTime AV225ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private bool entryPointCalled ;
      private bool AV11OrderedDsc ;
      private bool returnInSub ;
      private bool AV34DynamicFiltersEnabled2 ;
      private bool AV52DynamicFiltersEnabled3 ;
      private bool AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 ;
      private bool AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 ;
      private bool A598ContagemResultado_Baseline ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n601ContagemResultado_Servico ;
      private bool n598ContagemResultado_Baseline ;
      private bool n771ContagemResultado_StatusDmnVnc ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n510ContagemResultado_EsforcoSoma ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private String AV131TFContagemResultado_StatusDmn_SelsJson ;
      private String AV135TFContagemResultado_StatusUltCnt_SelsJson ;
      private String AV70GridStateXML ;
      private String AV117TFContagemResultado_OsFsOsFm ;
      private String AV118TFContagemResultado_OsFsOsFm_Sel ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17ContagemResultado_OsFsOsFm1 ;
      private String AV114FilterContagemResultado_OsFsOsFmDescription ;
      private String AV73ContagemResultado_OsFsOsFm ;
      private String AV74FilterContagemResultado_DataDmnDescription ;
      private String AV102FilterContagemResultado_DataUltCntDescription ;
      private String AV26FilterContagemResultado_StatusDmn1ValueDescription ;
      private String AV25FilterContagemResultado_StatusDmnValueDescription ;
      private String AV106FilterContagemResultado_StatusDmnVnc1ValueDescription ;
      private String AV105FilterContagemResultado_StatusDmnVncValueDescription ;
      private String AV83FilterContagemResultado_EsforcoSomaDescription ;
      private String AV32FilterContagemResultado_Baseline1ValueDescription ;
      private String AV31FilterContagemResultado_BaselineValueDescription ;
      private String AV35DynamicFiltersSelector2 ;
      private String AV37ContagemResultado_OsFsOsFm2 ;
      private String AV45FilterContagemResultado_StatusDmn2ValueDescription ;
      private String AV108FilterContagemResultado_StatusDmnVnc2ValueDescription ;
      private String AV50FilterContagemResultado_Baseline2ValueDescription ;
      private String AV53DynamicFiltersSelector3 ;
      private String AV55ContagemResultado_OsFsOsFm3 ;
      private String AV63FilterContagemResultado_StatusDmn3ValueDescription ;
      private String AV110FilterContagemResultado_StatusDmnVnc3ValueDescription ;
      private String AV68FilterContagemResultado_Baseline3ValueDescription ;
      private String AV132TFContagemResultado_StatusDmn_SelDscs ;
      private String AV150FilterTFContagemResultado_StatusDmn_SelValueDescription ;
      private String AV136TFContagemResultado_StatusUltCnt_SelDscs ;
      private String AV151FilterTFContagemResultado_StatusUltCnt_SelValueDescription ;
      private String AV152FilterTFContagemResultado_Baseline_SelValueDescription ;
      private String AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 ;
      private String AV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 ;
      private String AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 ;
      private String AV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 ;
      private String AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 ;
      private String AV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 ;
      private String AV220ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm ;
      private String AV221ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel ;
      private String lV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 ;
      private String lV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 ;
      private String lV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 ;
      private String lV220ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String AV13ContagemResultado_StatusDmnDescription ;
      private String AV87ContagemResultado_StatusUltCntDescription ;
      private String AV14ContagemResultado_BaselineDescription ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00544_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00544_n1553ContagemResultado_CntSrvCod ;
      private int[] P00544_A602ContagemResultado_OSVinculada ;
      private bool[] P00544_n602ContagemResultado_OSVinculada ;
      private short[] P00544_A1583ContagemResultado_TipoRegistro ;
      private String[] P00544_A801ContagemResultado_ServicoSigla ;
      private bool[] P00544_n801ContagemResultado_ServicoSigla ;
      private String[] P00544_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00544_n803ContagemResultado_ContratadaSigla ;
      private String[] P00544_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00544_n509ContagemrResultado_SistemaSigla ;
      private int[] P00544_A601ContagemResultado_Servico ;
      private bool[] P00544_n601ContagemResultado_Servico ;
      private bool[] P00544_A598ContagemResultado_Baseline ;
      private bool[] P00544_n598ContagemResultado_Baseline ;
      private String[] P00544_A771ContagemResultado_StatusDmnVnc ;
      private bool[] P00544_n771ContagemResultado_StatusDmnVnc ;
      private String[] P00544_A484ContagemResultado_StatusDmn ;
      private bool[] P00544_n484ContagemResultado_StatusDmn ;
      private int[] P00544_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P00544_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] P00544_A489ContagemResultado_SistemaCod ;
      private bool[] P00544_n489ContagemResultado_SistemaCod ;
      private int[] P00544_A508ContagemResultado_Owner ;
      private DateTime[] P00544_A471ContagemResultado_DataDmn ;
      private int[] P00544_A490ContagemResultado_ContratadaCod ;
      private bool[] P00544_n490ContagemResultado_ContratadaCod ;
      private int[] P00544_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00544_n52Contratada_AreaTrabalhoCod ;
      private decimal[] P00544_A683ContagemResultado_PFLFMUltima ;
      private decimal[] P00544_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P00544_A685ContagemResultado_PFLFSUltima ;
      private decimal[] P00544_A684ContagemResultado_PFBFSUltima ;
      private short[] P00544_A531ContagemResultado_StatusUltCnt ;
      private decimal[] P00544_A802ContagemResultado_DeflatorCnt ;
      private int[] P00544_A510ContagemResultado_EsforcoSoma ;
      private bool[] P00544_n510ContagemResultado_EsforcoSoma ;
      private int[] P00544_A584ContagemResultado_ContadorFM ;
      private DateTime[] P00544_A566ContagemResultado_DataUltCnt ;
      private String[] P00544_A493ContagemResultado_DemandaFM ;
      private bool[] P00544_n493ContagemResultado_DemandaFM ;
      private String[] P00544_A457ContagemResultado_Demanda ;
      private bool[] P00544_n457ContagemResultado_Demanda ;
      private int[] P00544_A456ContagemResultado_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV137TFContagemResultado_StatusUltCnt_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV235ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV133TFContagemResultado_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV234ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV71GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV72GridStateDynamicFilter ;
   }

   public class exportreportextrawwcontagemresultadoos__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00544( IGxContext context ,
                                             short A531ContagemResultado_StatusUltCnt ,
                                             IGxCollection AV235ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV234ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels ,
                                             int AV168ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod ,
                                             int AV154Contratada_DoUsuario ,
                                             String AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 ,
                                             short AV171ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 ,
                                             String AV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 ,
                                             DateTime AV173ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 ,
                                             DateTime AV174ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 ,
                                             int AV178ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 ,
                                             int AV179ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 ,
                                             int AV180ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 ,
                                             String AV181ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 ,
                                             String AV182ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 ,
                                             int AV183ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 ,
                                             String AV184ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 ,
                                             int AV185ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 ,
                                             bool AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 ,
                                             String AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 ,
                                             short AV188ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 ,
                                             String AV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 ,
                                             DateTime AV190ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 ,
                                             DateTime AV191ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 ,
                                             int AV195ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 ,
                                             int AV196ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 ,
                                             int AV197ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 ,
                                             String AV198ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 ,
                                             String AV199ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 ,
                                             int AV200ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 ,
                                             String AV201ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 ,
                                             int AV202ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 ,
                                             bool AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 ,
                                             String AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 ,
                                             short AV205ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 ,
                                             String AV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 ,
                                             DateTime AV207ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 ,
                                             DateTime AV208ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 ,
                                             int AV212ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 ,
                                             int AV213ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 ,
                                             int AV214ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 ,
                                             String AV215ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 ,
                                             String AV216ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 ,
                                             int AV217ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 ,
                                             String AV218ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 ,
                                             int AV219ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 ,
                                             String AV221ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel ,
                                             String AV220ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm ,
                                             DateTime AV222ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn ,
                                             DateTime AV223ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to ,
                                             String AV227ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel ,
                                             String AV226ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla ,
                                             String AV229ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel ,
                                             String AV228ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla ,
                                             String AV231ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel ,
                                             String AV230ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla ,
                                             int AV234ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels_Count ,
                                             short AV236ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel ,
                                             int AV71GridState_gxTpr_Dynamicfilters_Count ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             int A489ContagemResultado_SistemaCod ,
                                             int A468ContagemResultado_NaoCnfDmnCod ,
                                             String A771ContagemResultado_StatusDmnVnc ,
                                             int A510ContagemResultado_EsforcoSoma ,
                                             bool A598ContagemResultado_Baseline ,
                                             int A601ContagemResultado_Servico ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             String A803ContagemResultado_ContratadaSigla ,
                                             String A801ContagemResultado_ServicoSigla ,
                                             int A456ContagemResultado_Codigo ,
                                             short AV10OrderedBy ,
                                             bool AV11OrderedDsc ,
                                             DateTime AV175ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             DateTime AV176ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 ,
                                             int AV177ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 ,
                                             int A584ContagemResultado_ContadorFM ,
                                             int A508ContagemResultado_Owner ,
                                             DateTime AV192ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 ,
                                             DateTime AV193ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 ,
                                             int AV194ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 ,
                                             DateTime AV209ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 ,
                                             DateTime AV210ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 ,
                                             int AV211ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 ,
                                             DateTime AV224ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt ,
                                             DateTime AV225ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to ,
                                             decimal AV232ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt ,
                                             decimal A802ContagemResultado_DeflatorCnt ,
                                             decimal AV233ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to ,
                                             int AV235ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels_Count ,
                                             decimal AV237ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima ,
                                             decimal A684ContagemResultado_PFBFSUltima ,
                                             decimal AV238ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to ,
                                             decimal AV239ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima ,
                                             decimal A685ContagemResultado_PFLFSUltima ,
                                             decimal AV240ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to ,
                                             decimal AV241ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima ,
                                             decimal A682ContagemResultado_PFBFMUltima ,
                                             decimal AV242ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to ,
                                             decimal AV243ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima ,
                                             decimal A683ContagemResultado_PFLFMUltima ,
                                             decimal AV244ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to ,
                                             decimal AV245ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ,
                                             decimal A574ContagemResultado_PFFinal ,
                                             decimal AV246ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ,
                                             short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [124] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T1.[ContagemResultado_TipoRegistro], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T6.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T5.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Baseline], T4.[ContagemResultado_StatusDmn] AS ContagemResultado_StatusDmnVnc, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_NaoCnfDmnCod], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_Owner], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T6.[Contratada_AreaTrabalhoCod], COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T7.[ContagemResultado_DeflatorCnt], 0) AS ContagemResultado_DeflatorCnt, COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) AS ContagemResultado_EsforcoSoma, COALESCE( T7.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_Codigo] FROM ((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo]";
         scmdbuf = scmdbuf + " = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContagemResultado] T4 WITH (NOLOCK) ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada]) LEFT JOIN [Sistema] T5 WITH (NOLOCK) ON T5.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, MIN([ContagemResultado_Deflator]) AS ContagemResultado_DeflatorCnt, MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T7 ON T7.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultadoContagens_Esforco]) AS ContagemResultado_EsforcoSoma, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV175ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV175ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV176ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV176ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV177ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV177ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV177ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (Not ( @AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = 1 and @AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV192ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV192ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = 1 and @AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV193ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV193ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = 1 and @AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV194ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV194ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV194ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = 1 and @AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV209ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV209ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = 1 and @AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV210ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV210ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = 1 and @AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV211ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV211ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV211ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3)))";
         scmdbuf = scmdbuf + " and ((@AV224ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV224ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV225ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV225ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and ((@AV232ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_DeflatorCnt], 0) >= @AV232ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt))";
         scmdbuf = scmdbuf + " and ((@AV233ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_DeflatorCnt], 0) <= @AV233ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to))";
         scmdbuf = scmdbuf + " and (@AV235ExtCount <= 0 or ( " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV235ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels, "COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) IN (", ")") + "))";
         scmdbuf = scmdbuf + " and ((@AV237ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) >= @AV237ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima))";
         scmdbuf = scmdbuf + " and ((@AV238ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <= @AV238ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to))";
         scmdbuf = scmdbuf + " and ((@AV239ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) >= @AV239ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima))";
         scmdbuf = scmdbuf + " and ((@AV240ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <= @AV240ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to))";
         scmdbuf = scmdbuf + " and ((@AV241ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) >= @AV241ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima))";
         scmdbuf = scmdbuf + " and ((@AV242ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) <= @AV242ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to))";
         scmdbuf = scmdbuf + " and ((@AV243ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) >= @AV243ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima))";
         scmdbuf = scmdbuf + " and ((@AV244ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) <= @AV244ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to))";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( AV168ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod > 0 )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_AreaTrabalhoCod] = @AV168ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod)";
         }
         else
         {
            GXv_int2[61] = 1;
         }
         if ( AV154Contratada_DoUsuario > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV154Contratada_DoUsuario)";
         }
         else
         {
            GXv_int2[62] = 1;
         }
         if ( ( StringUtil.StrCmp(AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV171ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] = @AV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int2[63] = 1;
            GXv_int2[64] = 1;
         }
         if ( ( StringUtil.StrCmp(AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV171ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like @lV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int2[65] = 1;
            GXv_int2[66] = 1;
         }
         if ( ( StringUtil.StrCmp(AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV171ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int2[67] = 1;
            GXv_int2[68] = 1;
         }
         if ( ( StringUtil.StrCmp(AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV173ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV173ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int2[69] = 1;
         }
         if ( ( StringUtil.StrCmp(AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV174ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV174ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int2[70] = 1;
         }
         if ( ( StringUtil.StrCmp(AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV178ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV178ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int2[71] = 1;
         }
         if ( ( StringUtil.StrCmp(AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV179ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1) && ! (0==AV168ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV179ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int2[72] = 1;
         }
         if ( ( StringUtil.StrCmp(AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV180ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV180ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1)";
         }
         else
         {
            GXv_int2[73] = 1;
         }
         if ( ( StringUtil.StrCmp(AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV181ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV181ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int2[74] = 1;
         }
         if ( ( StringUtil.StrCmp(AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV182ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_StatusDmn] = @AV182ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1)";
         }
         else
         {
            GXv_int2[75] = 1;
         }
         if ( ( StringUtil.StrCmp(AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV171ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV183ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) > @AV183ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int2[76] = 1;
         }
         if ( ( StringUtil.StrCmp(AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV171ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV183ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) < @AV183ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int2[77] = 1;
         }
         if ( ( StringUtil.StrCmp(AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV171ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV183ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) = @AV183ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int2[78] = 1;
         }
         if ( ( StringUtil.StrCmp(AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV184ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV184ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( ( StringUtil.StrCmp(AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV185ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV185ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int2[79] = 1;
         }
         if ( AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV188ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] = @AV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int2[80] = 1;
            GXv_int2[81] = 1;
         }
         if ( AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV188ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like @lV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int2[82] = 1;
            GXv_int2[83] = 1;
         }
         if ( AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV188ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int2[84] = 1;
            GXv_int2[85] = 1;
         }
         if ( AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV190ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV190ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int2[86] = 1;
         }
         if ( AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV191ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV191ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int2[87] = 1;
         }
         if ( AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV195ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV195ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int2[88] = 1;
         }
         if ( AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV196ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2) && ! (0==AV168ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV196ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int2[89] = 1;
         }
         if ( AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV197ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV197ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2)";
         }
         else
         {
            GXv_int2[90] = 1;
         }
         if ( AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV198ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV198ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int2[91] = 1;
         }
         if ( AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV199ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_StatusDmn] = @AV199ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2)";
         }
         else
         {
            GXv_int2[92] = 1;
         }
         if ( AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV188ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV200ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) > @AV200ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int2[93] = 1;
         }
         if ( AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV188ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV200ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) < @AV200ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int2[94] = 1;
         }
         if ( AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV188ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV200ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) = @AV200ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int2[95] = 1;
         }
         if ( AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV201ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV201ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV202ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV202ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int2[96] = 1;
         }
         if ( AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV205ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] = @AV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int2[97] = 1;
            GXv_int2[98] = 1;
         }
         if ( AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV205ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like @lV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int2[99] = 1;
            GXv_int2[100] = 1;
         }
         if ( AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV205ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int2[101] = 1;
            GXv_int2[102] = 1;
         }
         if ( AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV207ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV207ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int2[103] = 1;
         }
         if ( AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV208ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV208ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int2[104] = 1;
         }
         if ( AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV212ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV212ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int2[105] = 1;
         }
         if ( AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV213ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3) && ! (0==AV168ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV213ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int2[106] = 1;
         }
         if ( AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV214ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV214ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3)";
         }
         else
         {
            GXv_int2[107] = 1;
         }
         if ( AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV215ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV215ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int2[108] = 1;
         }
         if ( AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV216ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_StatusDmn] = @AV216ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3)";
         }
         else
         {
            GXv_int2[109] = 1;
         }
         if ( AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV205ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV217ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) > @AV217ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int2[110] = 1;
         }
         if ( AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV205ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV217ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) < @AV217ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int2[111] = 1;
         }
         if ( AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV205ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV217ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) = @AV217ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int2[112] = 1;
         }
         if ( AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV218ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV218ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV219ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV219ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int2[113] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV221ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV220ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm)) ) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T1.[ContagemResultado_Demanda])) + CASE  WHEN (T1.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T1.[ContagemResultado_DemandaFM])) END) like @lV220ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm)";
         }
         else
         {
            GXv_int2[114] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV221ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel)) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T1.[ContagemResultado_Demanda])) + CASE  WHEN (T1.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T1.[ContagemResultado_DemandaFM])) END) = @AV221ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel)";
         }
         else
         {
            GXv_int2[115] = 1;
         }
         if ( ! (DateTime.MinValue==AV222ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV222ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int2[116] = 1;
         }
         if ( ! (DateTime.MinValue==AV223ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV223ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int2[117] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV227ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV226ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] like @lV226ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla)";
         }
         else
         {
            GXv_int2[118] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV227ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] = @AV227ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel)";
         }
         else
         {
            GXv_int2[119] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV229ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV228ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] like @lV228ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla)";
         }
         else
         {
            GXv_int2[120] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV229ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] = @AV229ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel)";
         }
         else
         {
            GXv_int2[121] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV231ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV230ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV230ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int2[122] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV231ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV231ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int2[123] = 1;
         }
         if ( AV234ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV234ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV236ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV236ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 0)";
         }
         if ( AV71GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV10OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo]";
         }
         else if ( AV10OrderedBy == 2 )
         {
            scmdbuf = scmdbuf + " ORDER BY T6.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_DataDmn] DESC";
         }
         else if ( AV10OrderedBy == 3 )
         {
            scmdbuf = scmdbuf + " ORDER BY T6.[Contratada_AreaTrabalhoCod], T2.[Servico_Codigo], T1.[ContagemResultado_DataDmn] DESC";
         }
         else if ( AV10OrderedBy == 4 )
         {
            scmdbuf = scmdbuf + " ORDER BY T6.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataDmn] DESC";
         }
         else if ( AV10OrderedBy == 5 )
         {
            scmdbuf = scmdbuf + " ORDER BY T6.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_StatusDmn], T2.[Servico_Codigo], T1.[ContagemResultado_DataDmn] DESC";
         }
         else if ( ( AV10OrderedBy == 6 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T3.[Servico_Sigla]";
         }
         else if ( ( AV10OrderedBy == 6 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T3.[Servico_Sigla] DESC";
         }
         else if ( ( AV10OrderedBy == 7 ) && ! AV11OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Baseline]";
         }
         else if ( ( AV10OrderedBy == 7 ) && ( AV11OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Baseline] DESC";
         }
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00544(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (bool)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (String)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (bool)dynConstraints[33] , (String)dynConstraints[34] , (short)dynConstraints[35] , (String)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (String)dynConstraints[45] , (int)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (DateTime)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (String)dynConstraints[53] , (String)dynConstraints[54] , (String)dynConstraints[55] , (String)dynConstraints[56] , (int)dynConstraints[57] , (short)dynConstraints[58] , (int)dynConstraints[59] , (int)dynConstraints[60] , (int)dynConstraints[61] , (String)dynConstraints[62] , (String)dynConstraints[63] , (DateTime)dynConstraints[64] , (int)dynConstraints[65] , (int)dynConstraints[66] , (String)dynConstraints[67] , (int)dynConstraints[68] , (bool)dynConstraints[69] , (int)dynConstraints[70] , (String)dynConstraints[71] , (String)dynConstraints[72] , (String)dynConstraints[73] , (int)dynConstraints[74] , (short)dynConstraints[75] , (bool)dynConstraints[76] , (DateTime)dynConstraints[77] , (DateTime)dynConstraints[78] , (DateTime)dynConstraints[79] , (int)dynConstraints[80] , (int)dynConstraints[81] , (int)dynConstraints[82] , (DateTime)dynConstraints[83] , (DateTime)dynConstraints[84] , (int)dynConstraints[85] , (DateTime)dynConstraints[86] , (DateTime)dynConstraints[87] , (int)dynConstraints[88] , (DateTime)dynConstraints[89] , (DateTime)dynConstraints[90] , (decimal)dynConstraints[91] , (decimal)dynConstraints[92] , (decimal)dynConstraints[93] , (int)dynConstraints[94] , (decimal)dynConstraints[95] , (decimal)dynConstraints[96] , (decimal)dynConstraints[97] , (decimal)dynConstraints[98] , (decimal)dynConstraints[99] , (decimal)dynConstraints[100] , (decimal)dynConstraints[101] , (decimal)dynConstraints[102] , (decimal)dynConstraints[103] , (decimal)dynConstraints[104] , (decimal)dynConstraints[105] , (decimal)dynConstraints[106] , (decimal)dynConstraints[107] , (decimal)dynConstraints[108] , (decimal)dynConstraints[109] , (short)dynConstraints[110] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00544 ;
          prmP00544 = new Object[] {
          new Object[] {"@AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV175ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV175ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV176ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV176ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV170ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV177ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV177ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV177ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV192ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV192ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV193ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV193ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV186ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV187ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV194ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV194ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV194ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV209ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV209ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV210ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV210ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV203ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV204ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV211ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV211ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV211ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV224ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV224ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV225ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV225ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV232ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV232ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV233ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV233ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV235ExtCount",SqlDbType.Int,9,0} ,
          new Object[] {"@AV237ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV237ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV238ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV238ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV239ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV239ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV240ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV240ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV241ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV241ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV242ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV242ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV243ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV243ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV244ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV244ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV168ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV154Contratada_DoUsuario",SqlDbType.Int,6,0} ,
          new Object[] {"@AV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV172ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV173ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV174ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV178ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV179ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV180ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV181ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV183ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV183ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV183ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV185ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV189ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV190ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV191ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV195ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV196ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV197ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV198ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV199ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV200ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV200ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV200ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV202ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV206ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV207ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV208ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV212ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV213ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV214ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV215ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV219ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV220ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV221ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV222ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV223ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV226ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV227ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV228ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV229ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV230ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV231ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00544", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00544,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 25) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((bool[]) buf[13])[0] = rslt.getBool(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(14) ;
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((decimal[]) buf[29])[0] = rslt.getDecimal(17) ;
                ((decimal[]) buf[30])[0] = rslt.getDecimal(18) ;
                ((decimal[]) buf[31])[0] = rslt.getDecimal(19) ;
                ((decimal[]) buf[32])[0] = rslt.getDecimal(20) ;
                ((short[]) buf[33])[0] = rslt.getShort(21) ;
                ((decimal[]) buf[34])[0] = rslt.getDecimal(22) ;
                ((int[]) buf[35])[0] = rslt.getInt(23) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((int[]) buf[37])[0] = rslt.getInt(24) ;
                ((DateTime[]) buf[38])[0] = rslt.getGXDate(25) ;
                ((String[]) buf[39])[0] = rslt.getVarchar(26) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(26);
                ((String[]) buf[41])[0] = rslt.getVarchar(27) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(27);
                ((int[]) buf[43])[0] = rslt.getInt(28) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[124]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[125]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[126]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[127]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[128]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[129]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[130]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[131]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[132]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[133]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[134]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[135]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[136]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[137]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[138]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[139]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[140]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[141]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[142]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[143]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[144]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[145]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[146]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[147]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[148]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[149]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[150]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[151]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[152]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[153]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[154]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[155]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[156]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[157]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[158]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[159]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[160]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[161]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[162]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[163]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[164]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[165]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[166]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[167]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[168]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[169]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[170]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[171]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[172]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[173]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[174]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[175]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[176]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[177]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[178]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[179]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[180]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[181]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[182]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[183]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[184]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[185]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[186]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[187]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[188]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[189]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[190]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[191]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[192]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[193]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[194]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[195]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[196]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[197]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[198]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[199]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[200]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[201]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[202]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[203]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[204]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[205]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[206]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[207]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[208]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[209]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[210]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[211]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[212]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[213]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[214]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[215]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[216]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[217]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[218]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[219]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[220]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[221]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[222]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[223]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[224]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[225]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[226]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[227]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[228]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[229]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[230]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[231]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[232]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[233]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[234]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[235]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[236]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[237]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[238]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[239]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[240]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[241]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[242]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[243]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[244]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[245]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[246]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[247]);
                }
                return;
       }
    }

 }

}
