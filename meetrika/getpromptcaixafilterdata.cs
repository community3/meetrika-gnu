/*
               File: GetPromptCaixaFilterData
        Description: Get Prompt Caixa Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:29.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcaixafilterdata : GXProcedure
   {
      public getpromptcaixafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcaixafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
         return AV35OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcaixafilterdata objgetpromptcaixafilterdata;
         objgetpromptcaixafilterdata = new getpromptcaixafilterdata();
         objgetpromptcaixafilterdata.AV26DDOName = aP0_DDOName;
         objgetpromptcaixafilterdata.AV24SearchTxt = aP1_SearchTxt;
         objgetpromptcaixafilterdata.AV25SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcaixafilterdata.AV30OptionsJson = "" ;
         objgetpromptcaixafilterdata.AV33OptionsDescJson = "" ;
         objgetpromptcaixafilterdata.AV35OptionIndexesJson = "" ;
         objgetpromptcaixafilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcaixafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcaixafilterdata);
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcaixafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV29Options = (IGxCollection)(new GxSimpleCollection());
         AV32OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV34OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CAIXA_DOCUMENTO") == 0 )
         {
            /* Execute user subroutine: 'LOADCAIXA_DOCUMENTOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CAIXA_TIPODECONTACOD") == 0 )
         {
            /* Execute user subroutine: 'LOADCAIXA_TIPODECONTACODOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CAIXA_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCAIXA_DESCRICAOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV30OptionsJson = AV29Options.ToJSonString(false);
         AV33OptionsDescJson = AV32OptionsDesc.ToJSonString(false);
         AV35OptionIndexesJson = AV34OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV37Session.Get("PromptCaixaGridState"), "") == 0 )
         {
            AV39GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptCaixaGridState"), "");
         }
         else
         {
            AV39GridState.FromXml(AV37Session.Get("PromptCaixaGridState"), "");
         }
         AV55GXV1 = 1;
         while ( AV55GXV1 <= AV39GridState.gxTpr_Filtervalues.Count )
         {
            AV40GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV39GridState.gxTpr_Filtervalues.Item(AV55GXV1));
            if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_CODIGO") == 0 )
            {
               AV10TFCaixa_Codigo = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV11TFCaixa_Codigo_To = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_DOCUMENTO") == 0 )
            {
               AV12TFCaixa_Documento = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_DOCUMENTO_SEL") == 0 )
            {
               AV13TFCaixa_Documento_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_EMISSAO") == 0 )
            {
               AV14TFCaixa_Emissao = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Value, 2);
               AV15TFCaixa_Emissao_To = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_VENCIMENTO") == 0 )
            {
               AV16TFCaixa_Vencimento = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Value, 2);
               AV17TFCaixa_Vencimento_To = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_TIPODECONTACOD") == 0 )
            {
               AV18TFCaixa_TipoDeContaCod = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_TIPODECONTACOD_SEL") == 0 )
            {
               AV19TFCaixa_TipoDeContaCod_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_DESCRICAO") == 0 )
            {
               AV20TFCaixa_Descricao = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_DESCRICAO_SEL") == 0 )
            {
               AV21TFCaixa_Descricao_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCAIXA_VALOR") == 0 )
            {
               AV22TFCaixa_Valor = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, ".");
               AV23TFCaixa_Valor_To = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, ".");
            }
            AV55GXV1 = (int)(AV55GXV1+1);
         }
         if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(1));
            AV42DynamicFiltersSelector1 = AV41GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "CAIXA_DOCUMENTO") == 0 )
            {
               AV43DynamicFiltersOperator1 = AV41GridStateDynamicFilter.gxTpr_Operator;
               AV44Caixa_Documento1 = AV41GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV45DynamicFiltersEnabled2 = true;
               AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(2));
               AV46DynamicFiltersSelector2 = AV41GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "CAIXA_DOCUMENTO") == 0 )
               {
                  AV47DynamicFiltersOperator2 = AV41GridStateDynamicFilter.gxTpr_Operator;
                  AV48Caixa_Documento2 = AV41GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV49DynamicFiltersEnabled3 = true;
                  AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(3));
                  AV50DynamicFiltersSelector3 = AV41GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CAIXA_DOCUMENTO") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV41GridStateDynamicFilter.gxTpr_Operator;
                     AV52Caixa_Documento3 = AV41GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCAIXA_DOCUMENTOOPTIONS' Routine */
         AV12TFCaixa_Documento = AV24SearchTxt;
         AV13TFCaixa_Documento_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV42DynamicFiltersSelector1 ,
                                              AV43DynamicFiltersOperator1 ,
                                              AV44Caixa_Documento1 ,
                                              AV45DynamicFiltersEnabled2 ,
                                              AV46DynamicFiltersSelector2 ,
                                              AV47DynamicFiltersOperator2 ,
                                              AV48Caixa_Documento2 ,
                                              AV49DynamicFiltersEnabled3 ,
                                              AV50DynamicFiltersSelector3 ,
                                              AV51DynamicFiltersOperator3 ,
                                              AV52Caixa_Documento3 ,
                                              AV10TFCaixa_Codigo ,
                                              AV11TFCaixa_Codigo_To ,
                                              AV13TFCaixa_Documento_Sel ,
                                              AV12TFCaixa_Documento ,
                                              AV14TFCaixa_Emissao ,
                                              AV15TFCaixa_Emissao_To ,
                                              AV16TFCaixa_Vencimento ,
                                              AV17TFCaixa_Vencimento_To ,
                                              AV19TFCaixa_TipoDeContaCod_Sel ,
                                              AV18TFCaixa_TipoDeContaCod ,
                                              AV21TFCaixa_Descricao_Sel ,
                                              AV20TFCaixa_Descricao ,
                                              AV22TFCaixa_Valor ,
                                              AV23TFCaixa_Valor_To ,
                                              A875Caixa_Documento ,
                                              A874Caixa_Codigo ,
                                              A876Caixa_Emissao ,
                                              A877Caixa_Vencimento ,
                                              A881Caixa_TipoDeContaCod ,
                                              A879Caixa_Descricao ,
                                              A880Caixa_Valor },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL
                                              }
         });
         lV44Caixa_Documento1 = StringUtil.Concat( StringUtil.RTrim( AV44Caixa_Documento1), "%", "");
         lV44Caixa_Documento1 = StringUtil.Concat( StringUtil.RTrim( AV44Caixa_Documento1), "%", "");
         lV48Caixa_Documento2 = StringUtil.Concat( StringUtil.RTrim( AV48Caixa_Documento2), "%", "");
         lV48Caixa_Documento2 = StringUtil.Concat( StringUtil.RTrim( AV48Caixa_Documento2), "%", "");
         lV52Caixa_Documento3 = StringUtil.Concat( StringUtil.RTrim( AV52Caixa_Documento3), "%", "");
         lV52Caixa_Documento3 = StringUtil.Concat( StringUtil.RTrim( AV52Caixa_Documento3), "%", "");
         lV12TFCaixa_Documento = StringUtil.Concat( StringUtil.RTrim( AV12TFCaixa_Documento), "%", "");
         lV18TFCaixa_TipoDeContaCod = StringUtil.PadR( StringUtil.RTrim( AV18TFCaixa_TipoDeContaCod), 20, "%");
         lV20TFCaixa_Descricao = StringUtil.Concat( StringUtil.RTrim( AV20TFCaixa_Descricao), "%", "");
         /* Using cursor P00P42 */
         pr_default.execute(0, new Object[] {lV44Caixa_Documento1, lV44Caixa_Documento1, lV48Caixa_Documento2, lV48Caixa_Documento2, lV52Caixa_Documento3, lV52Caixa_Documento3, AV10TFCaixa_Codigo, AV11TFCaixa_Codigo_To, lV12TFCaixa_Documento, AV13TFCaixa_Documento_Sel, AV14TFCaixa_Emissao, AV15TFCaixa_Emissao_To, AV16TFCaixa_Vencimento, AV17TFCaixa_Vencimento_To, lV18TFCaixa_TipoDeContaCod, AV19TFCaixa_TipoDeContaCod_Sel, lV20TFCaixa_Descricao, AV21TFCaixa_Descricao_Sel, AV22TFCaixa_Valor, AV23TFCaixa_Valor_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKP42 = false;
            A875Caixa_Documento = P00P42_A875Caixa_Documento[0];
            A880Caixa_Valor = P00P42_A880Caixa_Valor[0];
            A879Caixa_Descricao = P00P42_A879Caixa_Descricao[0];
            n879Caixa_Descricao = P00P42_n879Caixa_Descricao[0];
            A881Caixa_TipoDeContaCod = P00P42_A881Caixa_TipoDeContaCod[0];
            A877Caixa_Vencimento = P00P42_A877Caixa_Vencimento[0];
            n877Caixa_Vencimento = P00P42_n877Caixa_Vencimento[0];
            A876Caixa_Emissao = P00P42_A876Caixa_Emissao[0];
            n876Caixa_Emissao = P00P42_n876Caixa_Emissao[0];
            A874Caixa_Codigo = P00P42_A874Caixa_Codigo[0];
            AV36count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00P42_A875Caixa_Documento[0], A875Caixa_Documento) == 0 ) )
            {
               BRKP42 = false;
               A874Caixa_Codigo = P00P42_A874Caixa_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKP42 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A875Caixa_Documento)) )
            {
               AV28Option = A875Caixa_Documento;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKP42 )
            {
               BRKP42 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCAIXA_TIPODECONTACODOPTIONS' Routine */
         AV18TFCaixa_TipoDeContaCod = AV24SearchTxt;
         AV19TFCaixa_TipoDeContaCod_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV42DynamicFiltersSelector1 ,
                                              AV43DynamicFiltersOperator1 ,
                                              AV44Caixa_Documento1 ,
                                              AV45DynamicFiltersEnabled2 ,
                                              AV46DynamicFiltersSelector2 ,
                                              AV47DynamicFiltersOperator2 ,
                                              AV48Caixa_Documento2 ,
                                              AV49DynamicFiltersEnabled3 ,
                                              AV50DynamicFiltersSelector3 ,
                                              AV51DynamicFiltersOperator3 ,
                                              AV52Caixa_Documento3 ,
                                              AV10TFCaixa_Codigo ,
                                              AV11TFCaixa_Codigo_To ,
                                              AV13TFCaixa_Documento_Sel ,
                                              AV12TFCaixa_Documento ,
                                              AV14TFCaixa_Emissao ,
                                              AV15TFCaixa_Emissao_To ,
                                              AV16TFCaixa_Vencimento ,
                                              AV17TFCaixa_Vencimento_To ,
                                              AV19TFCaixa_TipoDeContaCod_Sel ,
                                              AV18TFCaixa_TipoDeContaCod ,
                                              AV21TFCaixa_Descricao_Sel ,
                                              AV20TFCaixa_Descricao ,
                                              AV22TFCaixa_Valor ,
                                              AV23TFCaixa_Valor_To ,
                                              A875Caixa_Documento ,
                                              A874Caixa_Codigo ,
                                              A876Caixa_Emissao ,
                                              A877Caixa_Vencimento ,
                                              A881Caixa_TipoDeContaCod ,
                                              A879Caixa_Descricao ,
                                              A880Caixa_Valor },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL
                                              }
         });
         lV44Caixa_Documento1 = StringUtil.Concat( StringUtil.RTrim( AV44Caixa_Documento1), "%", "");
         lV44Caixa_Documento1 = StringUtil.Concat( StringUtil.RTrim( AV44Caixa_Documento1), "%", "");
         lV48Caixa_Documento2 = StringUtil.Concat( StringUtil.RTrim( AV48Caixa_Documento2), "%", "");
         lV48Caixa_Documento2 = StringUtil.Concat( StringUtil.RTrim( AV48Caixa_Documento2), "%", "");
         lV52Caixa_Documento3 = StringUtil.Concat( StringUtil.RTrim( AV52Caixa_Documento3), "%", "");
         lV52Caixa_Documento3 = StringUtil.Concat( StringUtil.RTrim( AV52Caixa_Documento3), "%", "");
         lV12TFCaixa_Documento = StringUtil.Concat( StringUtil.RTrim( AV12TFCaixa_Documento), "%", "");
         lV18TFCaixa_TipoDeContaCod = StringUtil.PadR( StringUtil.RTrim( AV18TFCaixa_TipoDeContaCod), 20, "%");
         lV20TFCaixa_Descricao = StringUtil.Concat( StringUtil.RTrim( AV20TFCaixa_Descricao), "%", "");
         /* Using cursor P00P43 */
         pr_default.execute(1, new Object[] {lV44Caixa_Documento1, lV44Caixa_Documento1, lV48Caixa_Documento2, lV48Caixa_Documento2, lV52Caixa_Documento3, lV52Caixa_Documento3, AV10TFCaixa_Codigo, AV11TFCaixa_Codigo_To, lV12TFCaixa_Documento, AV13TFCaixa_Documento_Sel, AV14TFCaixa_Emissao, AV15TFCaixa_Emissao_To, AV16TFCaixa_Vencimento, AV17TFCaixa_Vencimento_To, lV18TFCaixa_TipoDeContaCod, AV19TFCaixa_TipoDeContaCod_Sel, lV20TFCaixa_Descricao, AV21TFCaixa_Descricao_Sel, AV22TFCaixa_Valor, AV23TFCaixa_Valor_To});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKP44 = false;
            A881Caixa_TipoDeContaCod = P00P43_A881Caixa_TipoDeContaCod[0];
            A880Caixa_Valor = P00P43_A880Caixa_Valor[0];
            A879Caixa_Descricao = P00P43_A879Caixa_Descricao[0];
            n879Caixa_Descricao = P00P43_n879Caixa_Descricao[0];
            A877Caixa_Vencimento = P00P43_A877Caixa_Vencimento[0];
            n877Caixa_Vencimento = P00P43_n877Caixa_Vencimento[0];
            A876Caixa_Emissao = P00P43_A876Caixa_Emissao[0];
            n876Caixa_Emissao = P00P43_n876Caixa_Emissao[0];
            A874Caixa_Codigo = P00P43_A874Caixa_Codigo[0];
            A875Caixa_Documento = P00P43_A875Caixa_Documento[0];
            AV36count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00P43_A881Caixa_TipoDeContaCod[0], A881Caixa_TipoDeContaCod) == 0 ) )
            {
               BRKP44 = false;
               A874Caixa_Codigo = P00P43_A874Caixa_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKP44 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A881Caixa_TipoDeContaCod)) )
            {
               AV28Option = A881Caixa_TipoDeContaCod;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKP44 )
            {
               BRKP44 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCAIXA_DESCRICAOOPTIONS' Routine */
         AV20TFCaixa_Descricao = AV24SearchTxt;
         AV21TFCaixa_Descricao_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV42DynamicFiltersSelector1 ,
                                              AV43DynamicFiltersOperator1 ,
                                              AV44Caixa_Documento1 ,
                                              AV45DynamicFiltersEnabled2 ,
                                              AV46DynamicFiltersSelector2 ,
                                              AV47DynamicFiltersOperator2 ,
                                              AV48Caixa_Documento2 ,
                                              AV49DynamicFiltersEnabled3 ,
                                              AV50DynamicFiltersSelector3 ,
                                              AV51DynamicFiltersOperator3 ,
                                              AV52Caixa_Documento3 ,
                                              AV10TFCaixa_Codigo ,
                                              AV11TFCaixa_Codigo_To ,
                                              AV13TFCaixa_Documento_Sel ,
                                              AV12TFCaixa_Documento ,
                                              AV14TFCaixa_Emissao ,
                                              AV15TFCaixa_Emissao_To ,
                                              AV16TFCaixa_Vencimento ,
                                              AV17TFCaixa_Vencimento_To ,
                                              AV19TFCaixa_TipoDeContaCod_Sel ,
                                              AV18TFCaixa_TipoDeContaCod ,
                                              AV21TFCaixa_Descricao_Sel ,
                                              AV20TFCaixa_Descricao ,
                                              AV22TFCaixa_Valor ,
                                              AV23TFCaixa_Valor_To ,
                                              A875Caixa_Documento ,
                                              A874Caixa_Codigo ,
                                              A876Caixa_Emissao ,
                                              A877Caixa_Vencimento ,
                                              A881Caixa_TipoDeContaCod ,
                                              A879Caixa_Descricao ,
                                              A880Caixa_Valor },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL
                                              }
         });
         lV44Caixa_Documento1 = StringUtil.Concat( StringUtil.RTrim( AV44Caixa_Documento1), "%", "");
         lV44Caixa_Documento1 = StringUtil.Concat( StringUtil.RTrim( AV44Caixa_Documento1), "%", "");
         lV48Caixa_Documento2 = StringUtil.Concat( StringUtil.RTrim( AV48Caixa_Documento2), "%", "");
         lV48Caixa_Documento2 = StringUtil.Concat( StringUtil.RTrim( AV48Caixa_Documento2), "%", "");
         lV52Caixa_Documento3 = StringUtil.Concat( StringUtil.RTrim( AV52Caixa_Documento3), "%", "");
         lV52Caixa_Documento3 = StringUtil.Concat( StringUtil.RTrim( AV52Caixa_Documento3), "%", "");
         lV12TFCaixa_Documento = StringUtil.Concat( StringUtil.RTrim( AV12TFCaixa_Documento), "%", "");
         lV18TFCaixa_TipoDeContaCod = StringUtil.PadR( StringUtil.RTrim( AV18TFCaixa_TipoDeContaCod), 20, "%");
         lV20TFCaixa_Descricao = StringUtil.Concat( StringUtil.RTrim( AV20TFCaixa_Descricao), "%", "");
         /* Using cursor P00P44 */
         pr_default.execute(2, new Object[] {lV44Caixa_Documento1, lV44Caixa_Documento1, lV48Caixa_Documento2, lV48Caixa_Documento2, lV52Caixa_Documento3, lV52Caixa_Documento3, AV10TFCaixa_Codigo, AV11TFCaixa_Codigo_To, lV12TFCaixa_Documento, AV13TFCaixa_Documento_Sel, AV14TFCaixa_Emissao, AV15TFCaixa_Emissao_To, AV16TFCaixa_Vencimento, AV17TFCaixa_Vencimento_To, lV18TFCaixa_TipoDeContaCod, AV19TFCaixa_TipoDeContaCod_Sel, lV20TFCaixa_Descricao, AV21TFCaixa_Descricao_Sel, AV22TFCaixa_Valor, AV23TFCaixa_Valor_To});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKP46 = false;
            A879Caixa_Descricao = P00P44_A879Caixa_Descricao[0];
            n879Caixa_Descricao = P00P44_n879Caixa_Descricao[0];
            A880Caixa_Valor = P00P44_A880Caixa_Valor[0];
            A881Caixa_TipoDeContaCod = P00P44_A881Caixa_TipoDeContaCod[0];
            A877Caixa_Vencimento = P00P44_A877Caixa_Vencimento[0];
            n877Caixa_Vencimento = P00P44_n877Caixa_Vencimento[0];
            A876Caixa_Emissao = P00P44_A876Caixa_Emissao[0];
            n876Caixa_Emissao = P00P44_n876Caixa_Emissao[0];
            A874Caixa_Codigo = P00P44_A874Caixa_Codigo[0];
            A875Caixa_Documento = P00P44_A875Caixa_Documento[0];
            AV36count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00P44_A879Caixa_Descricao[0], A879Caixa_Descricao) == 0 ) )
            {
               BRKP46 = false;
               A874Caixa_Codigo = P00P44_A874Caixa_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKP46 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A879Caixa_Descricao)) )
            {
               AV28Option = A879Caixa_Descricao;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKP46 )
            {
               BRKP46 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV29Options = new GxSimpleCollection();
         AV32OptionsDesc = new GxSimpleCollection();
         AV34OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV37Session = context.GetSession();
         AV39GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV40GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFCaixa_Documento = "";
         AV13TFCaixa_Documento_Sel = "";
         AV14TFCaixa_Emissao = DateTime.MinValue;
         AV15TFCaixa_Emissao_To = DateTime.MinValue;
         AV16TFCaixa_Vencimento = DateTime.MinValue;
         AV17TFCaixa_Vencimento_To = DateTime.MinValue;
         AV18TFCaixa_TipoDeContaCod = "";
         AV19TFCaixa_TipoDeContaCod_Sel = "";
         AV20TFCaixa_Descricao = "";
         AV21TFCaixa_Descricao_Sel = "";
         AV41GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV42DynamicFiltersSelector1 = "";
         AV44Caixa_Documento1 = "";
         AV46DynamicFiltersSelector2 = "";
         AV48Caixa_Documento2 = "";
         AV50DynamicFiltersSelector3 = "";
         AV52Caixa_Documento3 = "";
         scmdbuf = "";
         lV44Caixa_Documento1 = "";
         lV48Caixa_Documento2 = "";
         lV52Caixa_Documento3 = "";
         lV12TFCaixa_Documento = "";
         lV18TFCaixa_TipoDeContaCod = "";
         lV20TFCaixa_Descricao = "";
         A875Caixa_Documento = "";
         A876Caixa_Emissao = DateTime.MinValue;
         A877Caixa_Vencimento = DateTime.MinValue;
         A881Caixa_TipoDeContaCod = "";
         A879Caixa_Descricao = "";
         P00P42_A875Caixa_Documento = new String[] {""} ;
         P00P42_A880Caixa_Valor = new decimal[1] ;
         P00P42_A879Caixa_Descricao = new String[] {""} ;
         P00P42_n879Caixa_Descricao = new bool[] {false} ;
         P00P42_A881Caixa_TipoDeContaCod = new String[] {""} ;
         P00P42_A877Caixa_Vencimento = new DateTime[] {DateTime.MinValue} ;
         P00P42_n877Caixa_Vencimento = new bool[] {false} ;
         P00P42_A876Caixa_Emissao = new DateTime[] {DateTime.MinValue} ;
         P00P42_n876Caixa_Emissao = new bool[] {false} ;
         P00P42_A874Caixa_Codigo = new int[1] ;
         AV28Option = "";
         P00P43_A881Caixa_TipoDeContaCod = new String[] {""} ;
         P00P43_A880Caixa_Valor = new decimal[1] ;
         P00P43_A879Caixa_Descricao = new String[] {""} ;
         P00P43_n879Caixa_Descricao = new bool[] {false} ;
         P00P43_A877Caixa_Vencimento = new DateTime[] {DateTime.MinValue} ;
         P00P43_n877Caixa_Vencimento = new bool[] {false} ;
         P00P43_A876Caixa_Emissao = new DateTime[] {DateTime.MinValue} ;
         P00P43_n876Caixa_Emissao = new bool[] {false} ;
         P00P43_A874Caixa_Codigo = new int[1] ;
         P00P43_A875Caixa_Documento = new String[] {""} ;
         P00P44_A879Caixa_Descricao = new String[] {""} ;
         P00P44_n879Caixa_Descricao = new bool[] {false} ;
         P00P44_A880Caixa_Valor = new decimal[1] ;
         P00P44_A881Caixa_TipoDeContaCod = new String[] {""} ;
         P00P44_A877Caixa_Vencimento = new DateTime[] {DateTime.MinValue} ;
         P00P44_n877Caixa_Vencimento = new bool[] {false} ;
         P00P44_A876Caixa_Emissao = new DateTime[] {DateTime.MinValue} ;
         P00P44_n876Caixa_Emissao = new bool[] {false} ;
         P00P44_A874Caixa_Codigo = new int[1] ;
         P00P44_A875Caixa_Documento = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcaixafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00P42_A875Caixa_Documento, P00P42_A880Caixa_Valor, P00P42_A879Caixa_Descricao, P00P42_n879Caixa_Descricao, P00P42_A881Caixa_TipoDeContaCod, P00P42_A877Caixa_Vencimento, P00P42_n877Caixa_Vencimento, P00P42_A876Caixa_Emissao, P00P42_n876Caixa_Emissao, P00P42_A874Caixa_Codigo
               }
               , new Object[] {
               P00P43_A881Caixa_TipoDeContaCod, P00P43_A880Caixa_Valor, P00P43_A879Caixa_Descricao, P00P43_n879Caixa_Descricao, P00P43_A877Caixa_Vencimento, P00P43_n877Caixa_Vencimento, P00P43_A876Caixa_Emissao, P00P43_n876Caixa_Emissao, P00P43_A874Caixa_Codigo, P00P43_A875Caixa_Documento
               }
               , new Object[] {
               P00P44_A879Caixa_Descricao, P00P44_n879Caixa_Descricao, P00P44_A880Caixa_Valor, P00P44_A881Caixa_TipoDeContaCod, P00P44_A877Caixa_Vencimento, P00P44_n877Caixa_Vencimento, P00P44_A876Caixa_Emissao, P00P44_n876Caixa_Emissao, P00P44_A874Caixa_Codigo, P00P44_A875Caixa_Documento
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV43DynamicFiltersOperator1 ;
      private short AV47DynamicFiltersOperator2 ;
      private short AV51DynamicFiltersOperator3 ;
      private int AV55GXV1 ;
      private int AV10TFCaixa_Codigo ;
      private int AV11TFCaixa_Codigo_To ;
      private int A874Caixa_Codigo ;
      private long AV36count ;
      private decimal AV22TFCaixa_Valor ;
      private decimal AV23TFCaixa_Valor_To ;
      private decimal A880Caixa_Valor ;
      private String AV18TFCaixa_TipoDeContaCod ;
      private String AV19TFCaixa_TipoDeContaCod_Sel ;
      private String scmdbuf ;
      private String lV18TFCaixa_TipoDeContaCod ;
      private String A881Caixa_TipoDeContaCod ;
      private DateTime AV14TFCaixa_Emissao ;
      private DateTime AV15TFCaixa_Emissao_To ;
      private DateTime AV16TFCaixa_Vencimento ;
      private DateTime AV17TFCaixa_Vencimento_To ;
      private DateTime A876Caixa_Emissao ;
      private DateTime A877Caixa_Vencimento ;
      private bool returnInSub ;
      private bool AV45DynamicFiltersEnabled2 ;
      private bool AV49DynamicFiltersEnabled3 ;
      private bool BRKP42 ;
      private bool n879Caixa_Descricao ;
      private bool n877Caixa_Vencimento ;
      private bool n876Caixa_Emissao ;
      private bool BRKP44 ;
      private bool BRKP46 ;
      private String AV35OptionIndexesJson ;
      private String AV30OptionsJson ;
      private String AV33OptionsDescJson ;
      private String AV26DDOName ;
      private String AV24SearchTxt ;
      private String AV25SearchTxtTo ;
      private String AV12TFCaixa_Documento ;
      private String AV13TFCaixa_Documento_Sel ;
      private String AV20TFCaixa_Descricao ;
      private String AV21TFCaixa_Descricao_Sel ;
      private String AV42DynamicFiltersSelector1 ;
      private String AV44Caixa_Documento1 ;
      private String AV46DynamicFiltersSelector2 ;
      private String AV48Caixa_Documento2 ;
      private String AV50DynamicFiltersSelector3 ;
      private String AV52Caixa_Documento3 ;
      private String lV44Caixa_Documento1 ;
      private String lV48Caixa_Documento2 ;
      private String lV52Caixa_Documento3 ;
      private String lV12TFCaixa_Documento ;
      private String lV20TFCaixa_Descricao ;
      private String A875Caixa_Documento ;
      private String A879Caixa_Descricao ;
      private String AV28Option ;
      private IGxSession AV37Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00P42_A875Caixa_Documento ;
      private decimal[] P00P42_A880Caixa_Valor ;
      private String[] P00P42_A879Caixa_Descricao ;
      private bool[] P00P42_n879Caixa_Descricao ;
      private String[] P00P42_A881Caixa_TipoDeContaCod ;
      private DateTime[] P00P42_A877Caixa_Vencimento ;
      private bool[] P00P42_n877Caixa_Vencimento ;
      private DateTime[] P00P42_A876Caixa_Emissao ;
      private bool[] P00P42_n876Caixa_Emissao ;
      private int[] P00P42_A874Caixa_Codigo ;
      private String[] P00P43_A881Caixa_TipoDeContaCod ;
      private decimal[] P00P43_A880Caixa_Valor ;
      private String[] P00P43_A879Caixa_Descricao ;
      private bool[] P00P43_n879Caixa_Descricao ;
      private DateTime[] P00P43_A877Caixa_Vencimento ;
      private bool[] P00P43_n877Caixa_Vencimento ;
      private DateTime[] P00P43_A876Caixa_Emissao ;
      private bool[] P00P43_n876Caixa_Emissao ;
      private int[] P00P43_A874Caixa_Codigo ;
      private String[] P00P43_A875Caixa_Documento ;
      private String[] P00P44_A879Caixa_Descricao ;
      private bool[] P00P44_n879Caixa_Descricao ;
      private decimal[] P00P44_A880Caixa_Valor ;
      private String[] P00P44_A881Caixa_TipoDeContaCod ;
      private DateTime[] P00P44_A877Caixa_Vencimento ;
      private bool[] P00P44_n877Caixa_Vencimento ;
      private DateTime[] P00P44_A876Caixa_Emissao ;
      private bool[] P00P44_n876Caixa_Emissao ;
      private int[] P00P44_A874Caixa_Codigo ;
      private String[] P00P44_A875Caixa_Documento ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV39GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV40GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV41GridStateDynamicFilter ;
   }

   public class getpromptcaixafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00P42( IGxContext context ,
                                             String AV42DynamicFiltersSelector1 ,
                                             short AV43DynamicFiltersOperator1 ,
                                             String AV44Caixa_Documento1 ,
                                             bool AV45DynamicFiltersEnabled2 ,
                                             String AV46DynamicFiltersSelector2 ,
                                             short AV47DynamicFiltersOperator2 ,
                                             String AV48Caixa_Documento2 ,
                                             bool AV49DynamicFiltersEnabled3 ,
                                             String AV50DynamicFiltersSelector3 ,
                                             short AV51DynamicFiltersOperator3 ,
                                             String AV52Caixa_Documento3 ,
                                             int AV10TFCaixa_Codigo ,
                                             int AV11TFCaixa_Codigo_To ,
                                             String AV13TFCaixa_Documento_Sel ,
                                             String AV12TFCaixa_Documento ,
                                             DateTime AV14TFCaixa_Emissao ,
                                             DateTime AV15TFCaixa_Emissao_To ,
                                             DateTime AV16TFCaixa_Vencimento ,
                                             DateTime AV17TFCaixa_Vencimento_To ,
                                             String AV19TFCaixa_TipoDeContaCod_Sel ,
                                             String AV18TFCaixa_TipoDeContaCod ,
                                             String AV21TFCaixa_Descricao_Sel ,
                                             String AV20TFCaixa_Descricao ,
                                             decimal AV22TFCaixa_Valor ,
                                             decimal AV23TFCaixa_Valor_To ,
                                             String A875Caixa_Documento ,
                                             int A874Caixa_Codigo ,
                                             DateTime A876Caixa_Emissao ,
                                             DateTime A877Caixa_Vencimento ,
                                             String A881Caixa_TipoDeContaCod ,
                                             String A879Caixa_Descricao ,
                                             decimal A880Caixa_Valor )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [20] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Caixa_Documento], [Caixa_Valor], [Caixa_Descricao], [Caixa_TipoDeContaCod], [Caixa_Vencimento], [Caixa_Emissao], [Caixa_Codigo] FROM [Caixa] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "CAIXA_DOCUMENTO") == 0 ) && ( AV43DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Caixa_Documento1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV44Caixa_Documento1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV44Caixa_Documento1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "CAIXA_DOCUMENTO") == 0 ) && ( AV43DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Caixa_Documento1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV44Caixa_Documento1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV44Caixa_Documento1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV45DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "CAIXA_DOCUMENTO") == 0 ) && ( AV47DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48Caixa_Documento2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV48Caixa_Documento2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV48Caixa_Documento2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV45DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "CAIXA_DOCUMENTO") == 0 ) && ( AV47DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48Caixa_Documento2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV48Caixa_Documento2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV48Caixa_Documento2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CAIXA_DOCUMENTO") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Caixa_Documento3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV52Caixa_Documento3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV52Caixa_Documento3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CAIXA_DOCUMENTO") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Caixa_Documento3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV52Caixa_Documento3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV52Caixa_Documento3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV10TFCaixa_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Codigo] >= @AV10TFCaixa_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Codigo] >= @AV10TFCaixa_Codigo)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV11TFCaixa_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Codigo] <= @AV11TFCaixa_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Codigo] <= @AV11TFCaixa_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFCaixa_Documento_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFCaixa_Documento)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV12TFCaixa_Documento)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV12TFCaixa_Documento)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFCaixa_Documento_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] = @AV13TFCaixa_Documento_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] = @AV13TFCaixa_Documento_Sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFCaixa_Emissao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Emissao] >= @AV14TFCaixa_Emissao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Emissao] >= @AV14TFCaixa_Emissao)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFCaixa_Emissao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Emissao] <= @AV15TFCaixa_Emissao_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Emissao] <= @AV15TFCaixa_Emissao_To)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV16TFCaixa_Vencimento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Vencimento] >= @AV16TFCaixa_Vencimento)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Vencimento] >= @AV16TFCaixa_Vencimento)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV17TFCaixa_Vencimento_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Vencimento] <= @AV17TFCaixa_Vencimento_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Vencimento] <= @AV17TFCaixa_Vencimento_To)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFCaixa_TipoDeContaCod_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFCaixa_TipoDeContaCod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_TipoDeContaCod] like @lV18TFCaixa_TipoDeContaCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_TipoDeContaCod] like @lV18TFCaixa_TipoDeContaCod)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFCaixa_TipoDeContaCod_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_TipoDeContaCod] = @AV19TFCaixa_TipoDeContaCod_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_TipoDeContaCod] = @AV19TFCaixa_TipoDeContaCod_Sel)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFCaixa_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFCaixa_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Descricao] like @lV20TFCaixa_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Descricao] like @lV20TFCaixa_Descricao)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFCaixa_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Descricao] = @AV21TFCaixa_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Descricao] = @AV21TFCaixa_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFCaixa_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Valor] >= @AV22TFCaixa_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Valor] >= @AV22TFCaixa_Valor)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV23TFCaixa_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Valor] <= @AV23TFCaixa_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Valor] <= @AV23TFCaixa_Valor_To)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Caixa_Documento]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00P43( IGxContext context ,
                                             String AV42DynamicFiltersSelector1 ,
                                             short AV43DynamicFiltersOperator1 ,
                                             String AV44Caixa_Documento1 ,
                                             bool AV45DynamicFiltersEnabled2 ,
                                             String AV46DynamicFiltersSelector2 ,
                                             short AV47DynamicFiltersOperator2 ,
                                             String AV48Caixa_Documento2 ,
                                             bool AV49DynamicFiltersEnabled3 ,
                                             String AV50DynamicFiltersSelector3 ,
                                             short AV51DynamicFiltersOperator3 ,
                                             String AV52Caixa_Documento3 ,
                                             int AV10TFCaixa_Codigo ,
                                             int AV11TFCaixa_Codigo_To ,
                                             String AV13TFCaixa_Documento_Sel ,
                                             String AV12TFCaixa_Documento ,
                                             DateTime AV14TFCaixa_Emissao ,
                                             DateTime AV15TFCaixa_Emissao_To ,
                                             DateTime AV16TFCaixa_Vencimento ,
                                             DateTime AV17TFCaixa_Vencimento_To ,
                                             String AV19TFCaixa_TipoDeContaCod_Sel ,
                                             String AV18TFCaixa_TipoDeContaCod ,
                                             String AV21TFCaixa_Descricao_Sel ,
                                             String AV20TFCaixa_Descricao ,
                                             decimal AV22TFCaixa_Valor ,
                                             decimal AV23TFCaixa_Valor_To ,
                                             String A875Caixa_Documento ,
                                             int A874Caixa_Codigo ,
                                             DateTime A876Caixa_Emissao ,
                                             DateTime A877Caixa_Vencimento ,
                                             String A881Caixa_TipoDeContaCod ,
                                             String A879Caixa_Descricao ,
                                             decimal A880Caixa_Valor )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [20] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [Caixa_TipoDeContaCod], [Caixa_Valor], [Caixa_Descricao], [Caixa_Vencimento], [Caixa_Emissao], [Caixa_Codigo], [Caixa_Documento] FROM [Caixa] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "CAIXA_DOCUMENTO") == 0 ) && ( AV43DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Caixa_Documento1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV44Caixa_Documento1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV44Caixa_Documento1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "CAIXA_DOCUMENTO") == 0 ) && ( AV43DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Caixa_Documento1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV44Caixa_Documento1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV44Caixa_Documento1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV45DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "CAIXA_DOCUMENTO") == 0 ) && ( AV47DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48Caixa_Documento2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV48Caixa_Documento2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV48Caixa_Documento2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV45DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "CAIXA_DOCUMENTO") == 0 ) && ( AV47DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48Caixa_Documento2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV48Caixa_Documento2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV48Caixa_Documento2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CAIXA_DOCUMENTO") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Caixa_Documento3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV52Caixa_Documento3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV52Caixa_Documento3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CAIXA_DOCUMENTO") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Caixa_Documento3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV52Caixa_Documento3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV52Caixa_Documento3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV10TFCaixa_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Codigo] >= @AV10TFCaixa_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Codigo] >= @AV10TFCaixa_Codigo)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (0==AV11TFCaixa_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Codigo] <= @AV11TFCaixa_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Codigo] <= @AV11TFCaixa_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFCaixa_Documento_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFCaixa_Documento)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV12TFCaixa_Documento)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV12TFCaixa_Documento)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFCaixa_Documento_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] = @AV13TFCaixa_Documento_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] = @AV13TFCaixa_Documento_Sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFCaixa_Emissao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Emissao] >= @AV14TFCaixa_Emissao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Emissao] >= @AV14TFCaixa_Emissao)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFCaixa_Emissao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Emissao] <= @AV15TFCaixa_Emissao_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Emissao] <= @AV15TFCaixa_Emissao_To)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV16TFCaixa_Vencimento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Vencimento] >= @AV16TFCaixa_Vencimento)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Vencimento] >= @AV16TFCaixa_Vencimento)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV17TFCaixa_Vencimento_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Vencimento] <= @AV17TFCaixa_Vencimento_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Vencimento] <= @AV17TFCaixa_Vencimento_To)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFCaixa_TipoDeContaCod_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFCaixa_TipoDeContaCod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_TipoDeContaCod] like @lV18TFCaixa_TipoDeContaCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_TipoDeContaCod] like @lV18TFCaixa_TipoDeContaCod)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFCaixa_TipoDeContaCod_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_TipoDeContaCod] = @AV19TFCaixa_TipoDeContaCod_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_TipoDeContaCod] = @AV19TFCaixa_TipoDeContaCod_Sel)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFCaixa_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFCaixa_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Descricao] like @lV20TFCaixa_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Descricao] like @lV20TFCaixa_Descricao)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFCaixa_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Descricao] = @AV21TFCaixa_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Descricao] = @AV21TFCaixa_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFCaixa_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Valor] >= @AV22TFCaixa_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Valor] >= @AV22TFCaixa_Valor)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV23TFCaixa_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Valor] <= @AV23TFCaixa_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Valor] <= @AV23TFCaixa_Valor_To)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Caixa_TipoDeContaCod]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00P44( IGxContext context ,
                                             String AV42DynamicFiltersSelector1 ,
                                             short AV43DynamicFiltersOperator1 ,
                                             String AV44Caixa_Documento1 ,
                                             bool AV45DynamicFiltersEnabled2 ,
                                             String AV46DynamicFiltersSelector2 ,
                                             short AV47DynamicFiltersOperator2 ,
                                             String AV48Caixa_Documento2 ,
                                             bool AV49DynamicFiltersEnabled3 ,
                                             String AV50DynamicFiltersSelector3 ,
                                             short AV51DynamicFiltersOperator3 ,
                                             String AV52Caixa_Documento3 ,
                                             int AV10TFCaixa_Codigo ,
                                             int AV11TFCaixa_Codigo_To ,
                                             String AV13TFCaixa_Documento_Sel ,
                                             String AV12TFCaixa_Documento ,
                                             DateTime AV14TFCaixa_Emissao ,
                                             DateTime AV15TFCaixa_Emissao_To ,
                                             DateTime AV16TFCaixa_Vencimento ,
                                             DateTime AV17TFCaixa_Vencimento_To ,
                                             String AV19TFCaixa_TipoDeContaCod_Sel ,
                                             String AV18TFCaixa_TipoDeContaCod ,
                                             String AV21TFCaixa_Descricao_Sel ,
                                             String AV20TFCaixa_Descricao ,
                                             decimal AV22TFCaixa_Valor ,
                                             decimal AV23TFCaixa_Valor_To ,
                                             String A875Caixa_Documento ,
                                             int A874Caixa_Codigo ,
                                             DateTime A876Caixa_Emissao ,
                                             DateTime A877Caixa_Vencimento ,
                                             String A881Caixa_TipoDeContaCod ,
                                             String A879Caixa_Descricao ,
                                             decimal A880Caixa_Valor )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [20] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT [Caixa_Descricao], [Caixa_Valor], [Caixa_TipoDeContaCod], [Caixa_Vencimento], [Caixa_Emissao], [Caixa_Codigo], [Caixa_Documento] FROM [Caixa] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "CAIXA_DOCUMENTO") == 0 ) && ( AV43DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Caixa_Documento1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV44Caixa_Documento1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV44Caixa_Documento1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "CAIXA_DOCUMENTO") == 0 ) && ( AV43DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Caixa_Documento1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV44Caixa_Documento1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV44Caixa_Documento1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV45DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "CAIXA_DOCUMENTO") == 0 ) && ( AV47DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48Caixa_Documento2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV48Caixa_Documento2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV48Caixa_Documento2)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV45DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "CAIXA_DOCUMENTO") == 0 ) && ( AV47DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48Caixa_Documento2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV48Caixa_Documento2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV48Caixa_Documento2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CAIXA_DOCUMENTO") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Caixa_Documento3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV52Caixa_Documento3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV52Caixa_Documento3)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CAIXA_DOCUMENTO") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Caixa_Documento3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like '%' + @lV52Caixa_Documento3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like '%' + @lV52Caixa_Documento3)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! (0==AV10TFCaixa_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Codigo] >= @AV10TFCaixa_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Codigo] >= @AV10TFCaixa_Codigo)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! (0==AV11TFCaixa_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Codigo] <= @AV11TFCaixa_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Codigo] <= @AV11TFCaixa_Codigo_To)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFCaixa_Documento_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFCaixa_Documento)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] like @lV12TFCaixa_Documento)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] like @lV12TFCaixa_Documento)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFCaixa_Documento_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Documento] = @AV13TFCaixa_Documento_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Documento] = @AV13TFCaixa_Documento_Sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFCaixa_Emissao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Emissao] >= @AV14TFCaixa_Emissao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Emissao] >= @AV14TFCaixa_Emissao)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFCaixa_Emissao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Emissao] <= @AV15TFCaixa_Emissao_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Emissao] <= @AV15TFCaixa_Emissao_To)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV16TFCaixa_Vencimento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Vencimento] >= @AV16TFCaixa_Vencimento)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Vencimento] >= @AV16TFCaixa_Vencimento)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV17TFCaixa_Vencimento_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Vencimento] <= @AV17TFCaixa_Vencimento_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Vencimento] <= @AV17TFCaixa_Vencimento_To)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFCaixa_TipoDeContaCod_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFCaixa_TipoDeContaCod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_TipoDeContaCod] like @lV18TFCaixa_TipoDeContaCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_TipoDeContaCod] like @lV18TFCaixa_TipoDeContaCod)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFCaixa_TipoDeContaCod_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_TipoDeContaCod] = @AV19TFCaixa_TipoDeContaCod_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_TipoDeContaCod] = @AV19TFCaixa_TipoDeContaCod_Sel)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFCaixa_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFCaixa_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Descricao] like @lV20TFCaixa_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Descricao] like @lV20TFCaixa_Descricao)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFCaixa_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Descricao] = @AV21TFCaixa_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Descricao] = @AV21TFCaixa_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFCaixa_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Valor] >= @AV22TFCaixa_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Valor] >= @AV22TFCaixa_Valor)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV23TFCaixa_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Caixa_Valor] <= @AV23TFCaixa_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Caixa_Valor] <= @AV23TFCaixa_Valor_To)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Caixa_Descricao]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00P42(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (decimal)dynConstraints[31] );
               case 1 :
                     return conditional_P00P43(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (decimal)dynConstraints[31] );
               case 2 :
                     return conditional_P00P44(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (decimal)dynConstraints[31] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00P42 ;
          prmP00P42 = new Object[] {
          new Object[] {"@lV44Caixa_Documento1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV44Caixa_Documento1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV48Caixa_Documento2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV48Caixa_Documento2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV52Caixa_Documento3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV52Caixa_Documento3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV10TFCaixa_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFCaixa_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFCaixa_Documento",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV13TFCaixa_Documento_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV14TFCaixa_Emissao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15TFCaixa_Emissao_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV16TFCaixa_Vencimento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17TFCaixa_Vencimento_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV18TFCaixa_TipoDeContaCod",SqlDbType.Char,20,0} ,
          new Object[] {"@AV19TFCaixa_TipoDeContaCod_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV20TFCaixa_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV21TFCaixa_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV22TFCaixa_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV23TFCaixa_Valor_To",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP00P43 ;
          prmP00P43 = new Object[] {
          new Object[] {"@lV44Caixa_Documento1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV44Caixa_Documento1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV48Caixa_Documento2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV48Caixa_Documento2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV52Caixa_Documento3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV52Caixa_Documento3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV10TFCaixa_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFCaixa_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFCaixa_Documento",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV13TFCaixa_Documento_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV14TFCaixa_Emissao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15TFCaixa_Emissao_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV16TFCaixa_Vencimento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17TFCaixa_Vencimento_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV18TFCaixa_TipoDeContaCod",SqlDbType.Char,20,0} ,
          new Object[] {"@AV19TFCaixa_TipoDeContaCod_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV20TFCaixa_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV21TFCaixa_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV22TFCaixa_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV23TFCaixa_Valor_To",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP00P44 ;
          prmP00P44 = new Object[] {
          new Object[] {"@lV44Caixa_Documento1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV44Caixa_Documento1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV48Caixa_Documento2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV48Caixa_Documento2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV52Caixa_Documento3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV52Caixa_Documento3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV10TFCaixa_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFCaixa_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFCaixa_Documento",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV13TFCaixa_Documento_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV14TFCaixa_Emissao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15TFCaixa_Emissao_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV16TFCaixa_Vencimento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17TFCaixa_Vencimento_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV18TFCaixa_TipoDeContaCod",SqlDbType.Char,20,0} ,
          new Object[] {"@AV19TFCaixa_TipoDeContaCod_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV20TFCaixa_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV21TFCaixa_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV22TFCaixa_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV23TFCaixa_Valor_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00P42", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00P42,100,0,true,false )
             ,new CursorDef("P00P43", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00P43,100,0,true,false )
             ,new CursorDef("P00P44", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00P44,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 20) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcaixafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcaixafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcaixafilterdata") )
          {
             return  ;
          }
          getpromptcaixafilterdata worker = new getpromptcaixafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
