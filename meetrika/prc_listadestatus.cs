/*
               File: PRC_ListaDeStatus
        Description: 'Lista De Status
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:43.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_listadestatus : GXProcedure
   {
      public prc_listadestatus( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_listadestatus( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_StrStatus ,
                           out String aP1_ListaDeStatus )
      {
         this.AV11StrStatus = aP0_StrStatus;
         this.AV10ListaDeStatus = "" ;
         initialize();
         executePrivate();
         aP1_ListaDeStatus=this.AV10ListaDeStatus;
      }

      public String executeUdp( String aP0_StrStatus )
      {
         this.AV11StrStatus = aP0_StrStatus;
         this.AV10ListaDeStatus = "" ;
         initialize();
         executePrivate();
         aP1_ListaDeStatus=this.AV10ListaDeStatus;
         return AV10ListaDeStatus ;
      }

      public void executeSubmit( String aP0_StrStatus ,
                                 out String aP1_ListaDeStatus )
      {
         prc_listadestatus objprc_listadestatus;
         objprc_listadestatus = new prc_listadestatus();
         objprc_listadestatus.AV11StrStatus = aP0_StrStatus;
         objprc_listadestatus.AV10ListaDeStatus = "" ;
         objprc_listadestatus.context.SetSubmitInitialConfig(context);
         objprc_listadestatus.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_listadestatus);
         aP1_ListaDeStatus=this.AV10ListaDeStatus;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_listadestatus)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV11StrStatus, "*") == 0 )
         {
            AV10ListaDeStatus = "Todos";
            this.cleanup();
            if (true) return;
         }
         AV9i = 1;
         while ( AV9i <= StringUtil.Len( AV11StrStatus) )
         {
            AV10ListaDeStatus = AV10ListaDeStatus + gxdomainstatusdemanda.getDescription(context,StringUtil.Substring( AV11StrStatus, AV9i, 1)) + ", ";
            AV9i = (short)(AV9i+1);
         }
         AV10ListaDeStatus = StringUtil.Substring( AV10ListaDeStatus, 1, StringUtil.StringSearchRev( AV10ListaDeStatus, ",", -1)-1);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9i ;
      private String AV10ListaDeStatus ;
      private String AV11StrStatus ;
      private String aP1_ListaDeStatus ;
   }

}
