/*
               File: GetPromptTecnologiaFilterData
        Description: Get Prompt Tecnologia Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:46.49
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getprompttecnologiafilterdata : GXProcedure
   {
      public getprompttecnologiafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getprompttecnologiafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getprompttecnologiafilterdata objgetprompttecnologiafilterdata;
         objgetprompttecnologiafilterdata = new getprompttecnologiafilterdata();
         objgetprompttecnologiafilterdata.AV16DDOName = aP0_DDOName;
         objgetprompttecnologiafilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetprompttecnologiafilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetprompttecnologiafilterdata.AV20OptionsJson = "" ;
         objgetprompttecnologiafilterdata.AV23OptionsDescJson = "" ;
         objgetprompttecnologiafilterdata.AV25OptionIndexesJson = "" ;
         objgetprompttecnologiafilterdata.context.SetSubmitInitialConfig(context);
         objgetprompttecnologiafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetprompttecnologiafilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getprompttecnologiafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_TECNOLOGIA_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADTECNOLOGIA_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("PromptTecnologiaGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptTecnologiaGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("PromptTecnologiaGridState"), "");
         }
         AV48GXV1 = 1;
         while ( AV48GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV48GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFTECNOLOGIA_NOME") == 0 )
            {
               AV10TFTecnologia_Nome = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFTECNOLOGIA_NOME_SEL") == 0 )
            {
               AV11TFTecnologia_Nome_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFTECNOLOGIA_TIPOTECNOLOGIA_SEL") == 0 )
            {
               AV12TFTecnologia_TipoTecnologia_SelsJson = AV30GridStateFilterValue.gxTpr_Value;
               AV13TFTecnologia_TipoTecnologia_Sels.FromJSonString(AV12TFTecnologia_TipoTecnologia_SelsJson);
            }
            AV48GXV1 = (int)(AV48GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV32DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "TECNOLOGIA_NOME") == 0 )
            {
               AV33DynamicFiltersOperator1 = AV31GridStateDynamicFilter.gxTpr_Operator;
               AV34Tecnologia_Nome1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 )
            {
               AV35Tecnologia_TipoTecnologia1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV36DynamicFiltersEnabled2 = true;
               AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(2));
               AV37DynamicFiltersSelector2 = AV31GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "TECNOLOGIA_NOME") == 0 )
               {
                  AV38DynamicFiltersOperator2 = AV31GridStateDynamicFilter.gxTpr_Operator;
                  AV39Tecnologia_Nome2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 )
               {
                  AV40Tecnologia_TipoTecnologia2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV41DynamicFiltersEnabled3 = true;
                  AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(3));
                  AV42DynamicFiltersSelector3 = AV31GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "TECNOLOGIA_NOME") == 0 )
                  {
                     AV43DynamicFiltersOperator3 = AV31GridStateDynamicFilter.gxTpr_Operator;
                     AV44Tecnologia_Nome3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 )
                  {
                     AV45Tecnologia_TipoTecnologia3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADTECNOLOGIA_NOMEOPTIONS' Routine */
         AV10TFTecnologia_Nome = AV14SearchTxt;
         AV11TFTecnologia_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A355Tecnologia_TipoTecnologia ,
                                              AV13TFTecnologia_TipoTecnologia_Sels ,
                                              AV32DynamicFiltersSelector1 ,
                                              AV33DynamicFiltersOperator1 ,
                                              AV34Tecnologia_Nome1 ,
                                              AV35Tecnologia_TipoTecnologia1 ,
                                              AV36DynamicFiltersEnabled2 ,
                                              AV37DynamicFiltersSelector2 ,
                                              AV38DynamicFiltersOperator2 ,
                                              AV39Tecnologia_Nome2 ,
                                              AV40Tecnologia_TipoTecnologia2 ,
                                              AV41DynamicFiltersEnabled3 ,
                                              AV42DynamicFiltersSelector3 ,
                                              AV43DynamicFiltersOperator3 ,
                                              AV44Tecnologia_Nome3 ,
                                              AV45Tecnologia_TipoTecnologia3 ,
                                              AV11TFTecnologia_Nome_Sel ,
                                              AV10TFTecnologia_Nome ,
                                              AV13TFTecnologia_TipoTecnologia_Sels.Count ,
                                              A132Tecnologia_Nome },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING
                                              }
         });
         lV34Tecnologia_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV34Tecnologia_Nome1), 50, "%");
         lV34Tecnologia_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV34Tecnologia_Nome1), 50, "%");
         lV39Tecnologia_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV39Tecnologia_Nome2), 50, "%");
         lV39Tecnologia_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV39Tecnologia_Nome2), 50, "%");
         lV44Tecnologia_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV44Tecnologia_Nome3), 50, "%");
         lV44Tecnologia_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV44Tecnologia_Nome3), 50, "%");
         lV10TFTecnologia_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFTecnologia_Nome), 50, "%");
         /* Using cursor P00UQ2 */
         pr_default.execute(0, new Object[] {lV34Tecnologia_Nome1, lV34Tecnologia_Nome1, AV35Tecnologia_TipoTecnologia1, lV39Tecnologia_Nome2, lV39Tecnologia_Nome2, AV40Tecnologia_TipoTecnologia2, lV44Tecnologia_Nome3, lV44Tecnologia_Nome3, AV45Tecnologia_TipoTecnologia3, lV10TFTecnologia_Nome, AV11TFTecnologia_Nome_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUQ2 = false;
            A132Tecnologia_Nome = P00UQ2_A132Tecnologia_Nome[0];
            A355Tecnologia_TipoTecnologia = P00UQ2_A355Tecnologia_TipoTecnologia[0];
            n355Tecnologia_TipoTecnologia = P00UQ2_n355Tecnologia_TipoTecnologia[0];
            A131Tecnologia_Codigo = P00UQ2_A131Tecnologia_Codigo[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00UQ2_A132Tecnologia_Nome[0], A132Tecnologia_Nome) == 0 ) )
            {
               BRKUQ2 = false;
               A131Tecnologia_Codigo = P00UQ2_A131Tecnologia_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKUQ2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A132Tecnologia_Nome)) )
            {
               AV18Option = A132Tecnologia_Nome;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUQ2 )
            {
               BRKUQ2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFTecnologia_Nome = "";
         AV11TFTecnologia_Nome_Sel = "";
         AV12TFTecnologia_TipoTecnologia_SelsJson = "";
         AV13TFTecnologia_TipoTecnologia_Sels = new GxSimpleCollection();
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV32DynamicFiltersSelector1 = "";
         AV34Tecnologia_Nome1 = "";
         AV35Tecnologia_TipoTecnologia1 = "";
         AV37DynamicFiltersSelector2 = "";
         AV39Tecnologia_Nome2 = "";
         AV40Tecnologia_TipoTecnologia2 = "";
         AV42DynamicFiltersSelector3 = "";
         AV44Tecnologia_Nome3 = "";
         AV45Tecnologia_TipoTecnologia3 = "";
         scmdbuf = "";
         lV34Tecnologia_Nome1 = "";
         lV39Tecnologia_Nome2 = "";
         lV44Tecnologia_Nome3 = "";
         lV10TFTecnologia_Nome = "";
         A355Tecnologia_TipoTecnologia = "";
         A132Tecnologia_Nome = "";
         P00UQ2_A132Tecnologia_Nome = new String[] {""} ;
         P00UQ2_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         P00UQ2_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         P00UQ2_A131Tecnologia_Codigo = new int[1] ;
         AV18Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getprompttecnologiafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UQ2_A132Tecnologia_Nome, P00UQ2_A355Tecnologia_TipoTecnologia, P00UQ2_n355Tecnologia_TipoTecnologia, P00UQ2_A131Tecnologia_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV33DynamicFiltersOperator1 ;
      private short AV38DynamicFiltersOperator2 ;
      private short AV43DynamicFiltersOperator3 ;
      private int AV48GXV1 ;
      private int AV13TFTecnologia_TipoTecnologia_Sels_Count ;
      private int A131Tecnologia_Codigo ;
      private long AV26count ;
      private String AV10TFTecnologia_Nome ;
      private String AV11TFTecnologia_Nome_Sel ;
      private String AV34Tecnologia_Nome1 ;
      private String AV35Tecnologia_TipoTecnologia1 ;
      private String AV39Tecnologia_Nome2 ;
      private String AV40Tecnologia_TipoTecnologia2 ;
      private String AV44Tecnologia_Nome3 ;
      private String AV45Tecnologia_TipoTecnologia3 ;
      private String scmdbuf ;
      private String lV34Tecnologia_Nome1 ;
      private String lV39Tecnologia_Nome2 ;
      private String lV44Tecnologia_Nome3 ;
      private String lV10TFTecnologia_Nome ;
      private String A355Tecnologia_TipoTecnologia ;
      private String A132Tecnologia_Nome ;
      private bool returnInSub ;
      private bool AV36DynamicFiltersEnabled2 ;
      private bool AV41DynamicFiltersEnabled3 ;
      private bool BRKUQ2 ;
      private bool n355Tecnologia_TipoTecnologia ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV12TFTecnologia_TipoTecnologia_SelsJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV32DynamicFiltersSelector1 ;
      private String AV37DynamicFiltersSelector2 ;
      private String AV42DynamicFiltersSelector3 ;
      private String AV18Option ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00UQ2_A132Tecnologia_Nome ;
      private String[] P00UQ2_A355Tecnologia_TipoTecnologia ;
      private bool[] P00UQ2_n355Tecnologia_TipoTecnologia ;
      private int[] P00UQ2_A131Tecnologia_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV13TFTecnologia_TipoTecnologia_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getprompttecnologiafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UQ2( IGxContext context ,
                                             String A355Tecnologia_TipoTecnologia ,
                                             IGxCollection AV13TFTecnologia_TipoTecnologia_Sels ,
                                             String AV32DynamicFiltersSelector1 ,
                                             short AV33DynamicFiltersOperator1 ,
                                             String AV34Tecnologia_Nome1 ,
                                             String AV35Tecnologia_TipoTecnologia1 ,
                                             bool AV36DynamicFiltersEnabled2 ,
                                             String AV37DynamicFiltersSelector2 ,
                                             short AV38DynamicFiltersOperator2 ,
                                             String AV39Tecnologia_Nome2 ,
                                             String AV40Tecnologia_TipoTecnologia2 ,
                                             bool AV41DynamicFiltersEnabled3 ,
                                             String AV42DynamicFiltersSelector3 ,
                                             short AV43DynamicFiltersOperator3 ,
                                             String AV44Tecnologia_Nome3 ,
                                             String AV45Tecnologia_TipoTecnologia3 ,
                                             String AV11TFTecnologia_Nome_Sel ,
                                             String AV10TFTecnologia_Nome ,
                                             int AV13TFTecnologia_TipoTecnologia_Sels_Count ,
                                             String A132Tecnologia_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [11] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Tecnologia_Nome], [Tecnologia_TipoTecnologia], [Tecnologia_Codigo] FROM [Tecnologia] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "TECNOLOGIA_NOME") == 0 ) && ( AV33DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Tecnologia_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like @lV34Tecnologia_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like @lV34Tecnologia_Nome1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "TECNOLOGIA_NOME") == 0 ) && ( AV33DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Tecnologia_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like '%' + @lV34Tecnologia_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like '%' + @lV34Tecnologia_Nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Tecnologia_TipoTecnologia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_TipoTecnologia] = @AV35Tecnologia_TipoTecnologia1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_TipoTecnologia] = @AV35Tecnologia_TipoTecnologia1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "TECNOLOGIA_NOME") == 0 ) && ( AV38DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Tecnologia_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like @lV39Tecnologia_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like @lV39Tecnologia_Nome2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "TECNOLOGIA_NOME") == 0 ) && ( AV38DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Tecnologia_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like '%' + @lV39Tecnologia_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like '%' + @lV39Tecnologia_Nome2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Tecnologia_TipoTecnologia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_TipoTecnologia] = @AV40Tecnologia_TipoTecnologia2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_TipoTecnologia] = @AV40Tecnologia_TipoTecnologia2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "TECNOLOGIA_NOME") == 0 ) && ( AV43DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Tecnologia_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like @lV44Tecnologia_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like @lV44Tecnologia_Nome3)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "TECNOLOGIA_NOME") == 0 ) && ( AV43DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Tecnologia_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like '%' + @lV44Tecnologia_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like '%' + @lV44Tecnologia_Nome3)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Tecnologia_TipoTecnologia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_TipoTecnologia] = @AV45Tecnologia_TipoTecnologia3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_TipoTecnologia] = @AV45Tecnologia_TipoTecnologia3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFTecnologia_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFTecnologia_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like @lV10TFTecnologia_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like @lV10TFTecnologia_Nome)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFTecnologia_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] = @AV11TFTecnologia_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] = @AV11TFTecnologia_Nome_Sel)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV13TFTecnologia_TipoTecnologia_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFTecnologia_TipoTecnologia_Sels, "[Tecnologia_TipoTecnologia] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFTecnologia_TipoTecnologia_Sels, "[Tecnologia_TipoTecnologia] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Tecnologia_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UQ2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UQ2 ;
          prmP00UQ2 = new Object[] {
          new Object[] {"@lV34Tecnologia_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV34Tecnologia_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV35Tecnologia_TipoTecnologia1",SqlDbType.Char,3,0} ,
          new Object[] {"@lV39Tecnologia_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV39Tecnologia_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV40Tecnologia_TipoTecnologia2",SqlDbType.Char,3,0} ,
          new Object[] {"@lV44Tecnologia_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV44Tecnologia_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV45Tecnologia_TipoTecnologia3",SqlDbType.Char,3,0} ,
          new Object[] {"@lV10TFTecnologia_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFTecnologia_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UQ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UQ2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getprompttecnologiafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getprompttecnologiafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getprompttecnologiafilterdata") )
          {
             return  ;
          }
          getprompttecnologiafilterdata worker = new getprompttecnologiafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
