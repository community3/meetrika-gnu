/*
               File: PRC_GetDataHoraEntrega
        Description: Get Data Hora Entrega
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:2.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_getdatahoraentrega : GXProcedure
   {
      public prc_getdatahoraentrega( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_getdatahoraentrega( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           out DateTime aP1_Prazo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8Prazo = DateTime.MinValue ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_Prazo=this.AV8Prazo;
      }

      public DateTime executeUdp( ref int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8Prazo = DateTime.MinValue ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_Prazo=this.AV8Prazo;
         return AV8Prazo ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 out DateTime aP1_Prazo )
      {
         prc_getdatahoraentrega objprc_getdatahoraentrega;
         objprc_getdatahoraentrega = new prc_getdatahoraentrega();
         objprc_getdatahoraentrega.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_getdatahoraentrega.AV8Prazo = DateTime.MinValue ;
         objprc_getdatahoraentrega.context.SetSubmitInitialConfig(context);
         objprc_getdatahoraentrega.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_getdatahoraentrega);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_Prazo=this.AV8Prazo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_getdatahoraentrega)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00912 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A472ContagemResultado_DataEntrega = P00912_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00912_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = P00912_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P00912_n912ContagemResultado_HoraEntrega[0];
            AV8Prazo = DateTimeUtil.ResetTime( A472ContagemResultado_DataEntrega ) ;
            AV8Prazo = DateTimeUtil.TAdd( AV8Prazo, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
            AV8Prazo = DateTimeUtil.TAdd( AV8Prazo, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00912_A456ContagemResultado_Codigo = new int[1] ;
         P00912_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00912_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00912_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00912_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_getdatahoraentrega__default(),
            new Object[][] {
                new Object[] {
               P00912_A456ContagemResultado_Codigo, P00912_A472ContagemResultado_DataEntrega, P00912_n472ContagemResultado_DataEntrega, P00912_A912ContagemResultado_HoraEntrega, P00912_n912ContagemResultado_HoraEntrega
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private String scmdbuf ;
      private DateTime AV8Prazo ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00912_A456ContagemResultado_Codigo ;
      private DateTime[] P00912_A472ContagemResultado_DataEntrega ;
      private bool[] P00912_n472ContagemResultado_DataEntrega ;
      private DateTime[] P00912_A912ContagemResultado_HoraEntrega ;
      private bool[] P00912_n912ContagemResultado_HoraEntrega ;
      private DateTime aP1_Prazo ;
   }

   public class prc_getdatahoraentrega__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00912 ;
          prmP00912 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00912", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_DataEntrega], [ContagemResultado_HoraEntrega] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00912,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
