/*
               File: type_SdtFuncaoDadosTabela
        Description: Tabelas da Fun��o de Dados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:20:14.72
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "FuncaoDadosTabela" )]
   [XmlType(TypeName =  "FuncaoDadosTabela" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtFuncaoDadosTabela : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtFuncaoDadosTabela( )
      {
         /* Constructor for serialization */
         gxTv_SdtFuncaoDadosTabela_Funcaodados_nome = "";
         gxTv_SdtFuncaoDadosTabela_Tabela_nome = "";
         gxTv_SdtFuncaoDadosTabela_Mode = "";
         gxTv_SdtFuncaoDadosTabela_Funcaodados_nome_Z = "";
         gxTv_SdtFuncaoDadosTabela_Tabela_nome_Z = "";
      }

      public SdtFuncaoDadosTabela( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV368FuncaoDados_Codigo ,
                        int AV172Tabela_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV368FuncaoDados_Codigo,(int)AV172Tabela_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"FuncaoDados_Codigo", typeof(int)}, new Object[]{"Tabela_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "FuncaoDadosTabela");
         metadata.Set("BT", "FuncaoDadosTabela");
         metadata.Set("PK", "[ \"FuncaoDados_Codigo\",\"Tabela_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"FuncaoDados_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Tabela_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaodados_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Funcaodados_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_nome_Z" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtFuncaoDadosTabela deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtFuncaoDadosTabela)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtFuncaoDadosTabela obj ;
         obj = this;
         obj.gxTpr_Funcaodados_codigo = deserialized.gxTpr_Funcaodados_codigo;
         obj.gxTpr_Funcaodados_nome = deserialized.gxTpr_Funcaodados_nome;
         obj.gxTpr_Tabela_codigo = deserialized.gxTpr_Tabela_codigo;
         obj.gxTpr_Tabela_nome = deserialized.gxTpr_Tabela_nome;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Funcaodados_codigo_Z = deserialized.gxTpr_Funcaodados_codigo_Z;
         obj.gxTpr_Funcaodados_nome_Z = deserialized.gxTpr_Funcaodados_nome_Z;
         obj.gxTpr_Tabela_codigo_Z = deserialized.gxTpr_Tabela_codigo_Z;
         obj.gxTpr_Tabela_nome_Z = deserialized.gxTpr_Tabela_nome_Z;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoDados_Codigo") )
               {
                  gxTv_SdtFuncaoDadosTabela_Funcaodados_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoDados_Nome") )
               {
                  gxTv_SdtFuncaoDadosTabela_Funcaodados_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_Codigo") )
               {
                  gxTv_SdtFuncaoDadosTabela_Tabela_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_Nome") )
               {
                  gxTv_SdtFuncaoDadosTabela_Tabela_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtFuncaoDadosTabela_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtFuncaoDadosTabela_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoDados_Codigo_Z") )
               {
                  gxTv_SdtFuncaoDadosTabela_Funcaodados_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoDados_Nome_Z") )
               {
                  gxTv_SdtFuncaoDadosTabela_Funcaodados_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_Codigo_Z") )
               {
                  gxTv_SdtFuncaoDadosTabela_Tabela_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_Nome_Z") )
               {
                  gxTv_SdtFuncaoDadosTabela_Tabela_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "FuncaoDadosTabela";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("FuncaoDados_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncaoDadosTabela_Funcaodados_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("FuncaoDados_Nome", StringUtil.RTrim( gxTv_SdtFuncaoDadosTabela_Funcaodados_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Tabela_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncaoDadosTabela_Tabela_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Tabela_Nome", StringUtil.RTrim( gxTv_SdtFuncaoDadosTabela_Tabela_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtFuncaoDadosTabela_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncaoDadosTabela_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("FuncaoDados_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncaoDadosTabela_Funcaodados_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("FuncaoDados_Nome_Z", StringUtil.RTrim( gxTv_SdtFuncaoDadosTabela_Funcaodados_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtFuncaoDadosTabela_Tabela_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_Nome_Z", StringUtil.RTrim( gxTv_SdtFuncaoDadosTabela_Tabela_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("FuncaoDados_Codigo", gxTv_SdtFuncaoDadosTabela_Funcaodados_codigo, false);
         AddObjectProperty("FuncaoDados_Nome", gxTv_SdtFuncaoDadosTabela_Funcaodados_nome, false);
         AddObjectProperty("Tabela_Codigo", gxTv_SdtFuncaoDadosTabela_Tabela_codigo, false);
         AddObjectProperty("Tabela_Nome", gxTv_SdtFuncaoDadosTabela_Tabela_nome, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtFuncaoDadosTabela_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtFuncaoDadosTabela_Initialized, false);
            AddObjectProperty("FuncaoDados_Codigo_Z", gxTv_SdtFuncaoDadosTabela_Funcaodados_codigo_Z, false);
            AddObjectProperty("FuncaoDados_Nome_Z", gxTv_SdtFuncaoDadosTabela_Funcaodados_nome_Z, false);
            AddObjectProperty("Tabela_Codigo_Z", gxTv_SdtFuncaoDadosTabela_Tabela_codigo_Z, false);
            AddObjectProperty("Tabela_Nome_Z", gxTv_SdtFuncaoDadosTabela_Tabela_nome_Z, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "FuncaoDados_Codigo" )]
      [  XmlElement( ElementName = "FuncaoDados_Codigo"   )]
      public int gxTpr_Funcaodados_codigo
      {
         get {
            return gxTv_SdtFuncaoDadosTabela_Funcaodados_codigo ;
         }

         set {
            if ( gxTv_SdtFuncaoDadosTabela_Funcaodados_codigo != value )
            {
               gxTv_SdtFuncaoDadosTabela_Mode = "INS";
               this.gxTv_SdtFuncaoDadosTabela_Funcaodados_codigo_Z_SetNull( );
               this.gxTv_SdtFuncaoDadosTabela_Funcaodados_nome_Z_SetNull( );
               this.gxTv_SdtFuncaoDadosTabela_Tabela_codigo_Z_SetNull( );
               this.gxTv_SdtFuncaoDadosTabela_Tabela_nome_Z_SetNull( );
            }
            gxTv_SdtFuncaoDadosTabela_Funcaodados_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "FuncaoDados_Nome" )]
      [  XmlElement( ElementName = "FuncaoDados_Nome"   )]
      public String gxTpr_Funcaodados_nome
      {
         get {
            return gxTv_SdtFuncaoDadosTabela_Funcaodados_nome ;
         }

         set {
            gxTv_SdtFuncaoDadosTabela_Funcaodados_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Tabela_Codigo" )]
      [  XmlElement( ElementName = "Tabela_Codigo"   )]
      public int gxTpr_Tabela_codigo
      {
         get {
            return gxTv_SdtFuncaoDadosTabela_Tabela_codigo ;
         }

         set {
            if ( gxTv_SdtFuncaoDadosTabela_Tabela_codigo != value )
            {
               gxTv_SdtFuncaoDadosTabela_Mode = "INS";
               this.gxTv_SdtFuncaoDadosTabela_Funcaodados_codigo_Z_SetNull( );
               this.gxTv_SdtFuncaoDadosTabela_Funcaodados_nome_Z_SetNull( );
               this.gxTv_SdtFuncaoDadosTabela_Tabela_codigo_Z_SetNull( );
               this.gxTv_SdtFuncaoDadosTabela_Tabela_nome_Z_SetNull( );
            }
            gxTv_SdtFuncaoDadosTabela_Tabela_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Tabela_Nome" )]
      [  XmlElement( ElementName = "Tabela_Nome"   )]
      public String gxTpr_Tabela_nome
      {
         get {
            return gxTv_SdtFuncaoDadosTabela_Tabela_nome ;
         }

         set {
            gxTv_SdtFuncaoDadosTabela_Tabela_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtFuncaoDadosTabela_Mode ;
         }

         set {
            gxTv_SdtFuncaoDadosTabela_Mode = (String)(value);
         }

      }

      public void gxTv_SdtFuncaoDadosTabela_Mode_SetNull( )
      {
         gxTv_SdtFuncaoDadosTabela_Mode = "";
         return  ;
      }

      public bool gxTv_SdtFuncaoDadosTabela_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtFuncaoDadosTabela_Initialized ;
         }

         set {
            gxTv_SdtFuncaoDadosTabela_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtFuncaoDadosTabela_Initialized_SetNull( )
      {
         gxTv_SdtFuncaoDadosTabela_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtFuncaoDadosTabela_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoDados_Codigo_Z" )]
      [  XmlElement( ElementName = "FuncaoDados_Codigo_Z"   )]
      public int gxTpr_Funcaodados_codigo_Z
      {
         get {
            return gxTv_SdtFuncaoDadosTabela_Funcaodados_codigo_Z ;
         }

         set {
            gxTv_SdtFuncaoDadosTabela_Funcaodados_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtFuncaoDadosTabela_Funcaodados_codigo_Z_SetNull( )
      {
         gxTv_SdtFuncaoDadosTabela_Funcaodados_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtFuncaoDadosTabela_Funcaodados_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "FuncaoDados_Nome_Z" )]
      [  XmlElement( ElementName = "FuncaoDados_Nome_Z"   )]
      public String gxTpr_Funcaodados_nome_Z
      {
         get {
            return gxTv_SdtFuncaoDadosTabela_Funcaodados_nome_Z ;
         }

         set {
            gxTv_SdtFuncaoDadosTabela_Funcaodados_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtFuncaoDadosTabela_Funcaodados_nome_Z_SetNull( )
      {
         gxTv_SdtFuncaoDadosTabela_Funcaodados_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtFuncaoDadosTabela_Funcaodados_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_Codigo_Z" )]
      [  XmlElement( ElementName = "Tabela_Codigo_Z"   )]
      public int gxTpr_Tabela_codigo_Z
      {
         get {
            return gxTv_SdtFuncaoDadosTabela_Tabela_codigo_Z ;
         }

         set {
            gxTv_SdtFuncaoDadosTabela_Tabela_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtFuncaoDadosTabela_Tabela_codigo_Z_SetNull( )
      {
         gxTv_SdtFuncaoDadosTabela_Tabela_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtFuncaoDadosTabela_Tabela_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_Nome_Z" )]
      [  XmlElement( ElementName = "Tabela_Nome_Z"   )]
      public String gxTpr_Tabela_nome_Z
      {
         get {
            return gxTv_SdtFuncaoDadosTabela_Tabela_nome_Z ;
         }

         set {
            gxTv_SdtFuncaoDadosTabela_Tabela_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtFuncaoDadosTabela_Tabela_nome_Z_SetNull( )
      {
         gxTv_SdtFuncaoDadosTabela_Tabela_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtFuncaoDadosTabela_Tabela_nome_Z_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtFuncaoDadosTabela_Funcaodados_nome = "";
         gxTv_SdtFuncaoDadosTabela_Tabela_nome = "";
         gxTv_SdtFuncaoDadosTabela_Mode = "";
         gxTv_SdtFuncaoDadosTabela_Funcaodados_nome_Z = "";
         gxTv_SdtFuncaoDadosTabela_Tabela_nome_Z = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "funcaodadostabela", "GeneXus.Programs.funcaodadostabela_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtFuncaoDadosTabela_Initialized ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtFuncaoDadosTabela_Funcaodados_codigo ;
      private int gxTv_SdtFuncaoDadosTabela_Tabela_codigo ;
      private int gxTv_SdtFuncaoDadosTabela_Funcaodados_codigo_Z ;
      private int gxTv_SdtFuncaoDadosTabela_Tabela_codigo_Z ;
      private String gxTv_SdtFuncaoDadosTabela_Tabela_nome ;
      private String gxTv_SdtFuncaoDadosTabela_Mode ;
      private String gxTv_SdtFuncaoDadosTabela_Tabela_nome_Z ;
      private String sTagName ;
      private String gxTv_SdtFuncaoDadosTabela_Funcaodados_nome ;
      private String gxTv_SdtFuncaoDadosTabela_Funcaodados_nome_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"FuncaoDadosTabela", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtFuncaoDadosTabela_RESTInterface : GxGenericCollectionItem<SdtFuncaoDadosTabela>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtFuncaoDadosTabela_RESTInterface( ) : base()
      {
      }

      public SdtFuncaoDadosTabela_RESTInterface( SdtFuncaoDadosTabela psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "FuncaoDados_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Funcaodados_codigo
      {
         get {
            return sdt.gxTpr_Funcaodados_codigo ;
         }

         set {
            sdt.gxTpr_Funcaodados_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "FuncaoDados_Nome" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Funcaodados_nome
      {
         get {
            return sdt.gxTpr_Funcaodados_nome ;
         }

         set {
            sdt.gxTpr_Funcaodados_nome = (String)(value);
         }

      }

      [DataMember( Name = "Tabela_Codigo" , Order = 2 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Tabela_codigo
      {
         get {
            return sdt.gxTpr_Tabela_codigo ;
         }

         set {
            sdt.gxTpr_Tabela_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Tabela_Nome" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Tabela_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tabela_nome) ;
         }

         set {
            sdt.gxTpr_Tabela_nome = (String)(value);
         }

      }

      public SdtFuncaoDadosTabela sdt
      {
         get {
            return (SdtFuncaoDadosTabela)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtFuncaoDadosTabela() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 10 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
