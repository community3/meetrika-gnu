/*
               File: PadroesArtefatos
        Description: Padroes Artefatos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:17:36.18
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class padroesartefatos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7PadroesArtefatos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7PadroesArtefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7PadroesArtefatos_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPADROESARTEFATOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7PadroesArtefatos_Codigo), "ZZZZZ9")));
               A147MetodologiaFases_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A147MetodologiaFases_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A147MetodologiaFases_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Padroes Artefatos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtPadroesArtefatos_Descricao_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public padroesartefatos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public padroesartefatos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_PadroesArtefatos_Codigo ,
                           ref int aP2_MetodologiaFases_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7PadroesArtefatos_Codigo = aP1_PadroesArtefatos_Codigo;
         this.A147MetodologiaFases_Codigo = aP2_MetodologiaFases_Codigo;
         executePrivate();
         aP2_MetodologiaFases_Codigo=this.A147MetodologiaFases_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0T30( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0T30e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPadroesArtefatos_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A150PadroesArtefatos_Codigo), 6, 0, ",", "")), ((edtPadroesArtefatos_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A150PadroesArtefatos_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A150PadroesArtefatos_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPadroesArtefatos_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtPadroesArtefatos_Codigo_Visible, edtPadroesArtefatos_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_PadroesArtefatos.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0T30( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_0T30( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_0T30e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_33_0T30( true) ;
         }
         return  ;
      }

      protected void wb_table3_33_0T30e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0T30e( true) ;
         }
         else
         {
            wb_table1_2_0T30e( false) ;
         }
      }

      protected void wb_table3_33_0T30( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_PadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_PadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_PadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_33_0T30e( true) ;
         }
         else
         {
            wb_table3_33_0T30e( false) ;
         }
      }

      protected void wb_table2_5_0T30( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_0T30( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_0T30e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0T30e( true) ;
         }
         else
         {
            wb_table2_5_0T30e( false) ;
         }
      }

      protected void wb_table4_13_0T30( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpadroesartefatos_descricao_Internalname, "Descri��o", "", "", lblTextblockpadroesartefatos_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_PadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPadroesArtefatos_Descricao_Internalname, A151PadroesArtefatos_Descricao, StringUtil.RTrim( context.localUtil.Format( A151PadroesArtefatos_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPadroesArtefatos_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtPadroesArtefatos_Descricao_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_PadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmetodologiafases_codigo_Internalname, "Fase", "", "", lblTextblockmetodologiafases_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_PadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_23_0T30( true) ;
         }
         return  ;
      }

      protected void wb_table5_23_0T30e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_0T30e( true) ;
         }
         else
         {
            wb_table4_13_0T30e( false) ;
         }
      }

      protected void wb_table5_23_0T30( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedmetodologiafases_codigo_Internalname, tblTablemergedmetodologiafases_codigo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtMetodologiaFases_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A147MetodologiaFases_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A147MetodologiaFases_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMetodologiaFases_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtMetodologiaFases_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_PadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmetodologiafases_nome_Internalname, "", "", "", lblTextblockmetodologiafases_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_PadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMetodologiaFases_Nome_Internalname, StringUtil.RTrim( A148MetodologiaFases_Nome), StringUtil.RTrim( context.localUtil.Format( A148MetodologiaFases_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMetodologiaFases_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtMetodologiaFases_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_PadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_23_0T30e( true) ;
         }
         else
         {
            wb_table5_23_0T30e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E110T2 */
         E110T2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A151PadroesArtefatos_Descricao = StringUtil.Upper( cgiGet( edtPadroesArtefatos_Descricao_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A151PadroesArtefatos_Descricao", A151PadroesArtefatos_Descricao);
               A147MetodologiaFases_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMetodologiaFases_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A147MetodologiaFases_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A147MetodologiaFases_Codigo), 6, 0)));
               A148MetodologiaFases_Nome = StringUtil.Upper( cgiGet( edtMetodologiaFases_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A148MetodologiaFases_Nome", A148MetodologiaFases_Nome);
               A150PadroesArtefatos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPadroesArtefatos_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A150PadroesArtefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A150PadroesArtefatos_Codigo), 6, 0)));
               /* Read saved values. */
               Z150PadroesArtefatos_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z150PadroesArtefatos_Codigo"), ",", "."));
               Z151PadroesArtefatos_Descricao = cgiGet( "Z151PadroesArtefatos_Descricao");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N147MetodologiaFases_Codigo = (int)(context.localUtil.CToN( cgiGet( "N147MetodologiaFases_Codigo"), ",", "."));
               AV7PadroesArtefatos_Codigo = (int)(context.localUtil.CToN( cgiGet( "vPADROESARTEFATOS_CODIGO"), ",", "."));
               AV11Insert_MetodologiaFases_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_METODOLOGIAFASES_CODIGO"), ",", "."));
               A137Metodologia_Codigo = (int)(context.localUtil.CToN( cgiGet( "METODOLOGIA_CODIGO"), ",", "."));
               A138Metodologia_Descricao = cgiGet( "METODOLOGIA_DESCRICAO");
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "PadroesArtefatos";
               A150PadroesArtefatos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPadroesArtefatos_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A150PadroesArtefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A150PadroesArtefatos_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A150PadroesArtefatos_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A150PadroesArtefatos_Codigo != Z150PadroesArtefatos_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("padroesartefatos:[SecurityCheckFailed value for]"+"PadroesArtefatos_Codigo:"+context.localUtil.Format( (decimal)(A150PadroesArtefatos_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("padroesartefatos:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A150PadroesArtefatos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A150PadroesArtefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A150PadroesArtefatos_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode30 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode30;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound30 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0T0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "PADROESARTEFATOS_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtPadroesArtefatos_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110T2 */
                           E110T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120T2 */
                           E120T2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E120T2 */
            E120T2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0T30( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0T30( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0T0( )
      {
         BeforeValidate0T30( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0T30( ) ;
            }
            else
            {
               CheckExtendedTable0T30( ) ;
               CloseExtendedTableCursors0T30( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0T0( )
      {
      }

      protected void E110T2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "MetodologiaFases_Codigo") == 0 )
               {
                  AV11Insert_MetodologiaFases_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_MetodologiaFases_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_MetodologiaFases_Codigo), 6, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
         edtPadroesArtefatos_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPadroesArtefatos_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPadroesArtefatos_Codigo_Visible), 5, 0)));
      }

      protected void E120T2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwpadroesartefatos.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)A147MetodologiaFases_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM0T30( short GX_JID )
      {
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z151PadroesArtefatos_Descricao = T000T3_A151PadroesArtefatos_Descricao[0];
            }
            else
            {
               Z151PadroesArtefatos_Descricao = A151PadroesArtefatos_Descricao;
            }
         }
         if ( GX_JID == -8 )
         {
            Z147MetodologiaFases_Codigo = A147MetodologiaFases_Codigo;
            Z150PadroesArtefatos_Codigo = A150PadroesArtefatos_Codigo;
            Z151PadroesArtefatos_Descricao = A151PadroesArtefatos_Descricao;
            Z148MetodologiaFases_Nome = A148MetodologiaFases_Nome;
            Z137Metodologia_Codigo = A137Metodologia_Codigo;
            Z138Metodologia_Descricao = A138Metodologia_Descricao;
         }
      }

      protected void standaloneNotModal( )
      {
         edtPadroesArtefatos_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPadroesArtefatos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPadroesArtefatos_Codigo_Enabled), 5, 0)));
         AV13Pgmname = "PadroesArtefatos";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         edtPadroesArtefatos_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPadroesArtefatos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPadroesArtefatos_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7PadroesArtefatos_Codigo) )
         {
            A150PadroesArtefatos_Codigo = AV7PadroesArtefatos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A150PadroesArtefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A150PadroesArtefatos_Codigo), 6, 0)));
         }
         /* Using cursor T000T4 */
         pr_default.execute(2, new Object[] {A147MetodologiaFases_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Metodologia Fases'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A148MetodologiaFases_Nome = T000T4_A148MetodologiaFases_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A148MetodologiaFases_Nome", A148MetodologiaFases_Nome);
         A137Metodologia_Codigo = T000T4_A137Metodologia_Codigo[0];
         pr_default.close(2);
         /* Using cursor T000T5 */
         pr_default.execute(3, new Object[] {A137Metodologia_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Metodologia'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A138Metodologia_Descricao = T000T5_A138Metodologia_Descricao[0];
         pr_default.close(3);
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_MetodologiaFases_Codigo) )
         {
            edtMetodologiaFases_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMetodologiaFases_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMetodologiaFases_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtMetodologiaFases_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMetodologiaFases_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMetodologiaFases_Codigo_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_MetodologiaFases_Codigo) )
            {
               A147MetodologiaFases_Codigo = AV11Insert_MetodologiaFases_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A147MetodologiaFases_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A147MetodologiaFases_Codigo), 6, 0)));
            }
         }
      }

      protected void Load0T30( )
      {
         /* Using cursor T000T6 */
         pr_default.execute(4, new Object[] {A150PadroesArtefatos_Codigo, A147MetodologiaFases_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound30 = 1;
            A151PadroesArtefatos_Descricao = T000T6_A151PadroesArtefatos_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A151PadroesArtefatos_Descricao", A151PadroesArtefatos_Descricao);
            A148MetodologiaFases_Nome = T000T6_A148MetodologiaFases_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A148MetodologiaFases_Nome", A148MetodologiaFases_Nome);
            A138Metodologia_Descricao = T000T6_A138Metodologia_Descricao[0];
            A137Metodologia_Codigo = T000T6_A137Metodologia_Codigo[0];
            ZM0T30( -8) ;
         }
         pr_default.close(4);
         OnLoadActions0T30( ) ;
      }

      protected void OnLoadActions0T30( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_MetodologiaFases_Codigo) )
         {
            A147MetodologiaFases_Codigo = AV11Insert_MetodologiaFases_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A147MetodologiaFases_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A147MetodologiaFases_Codigo), 6, 0)));
         }
      }

      protected void CheckExtendedTable0T30( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_MetodologiaFases_Codigo) )
         {
            A147MetodologiaFases_Codigo = AV11Insert_MetodologiaFases_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A147MetodologiaFases_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A147MetodologiaFases_Codigo), 6, 0)));
         }
      }

      protected void CloseExtendedTableCursors0T30( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0T30( )
      {
         /* Using cursor T000T7 */
         pr_default.execute(5, new Object[] {A150PadroesArtefatos_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound30 = 1;
         }
         else
         {
            RcdFound30 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000T3 */
         pr_default.execute(1, new Object[] {A150PadroesArtefatos_Codigo});
         if ( (pr_default.getStatus(1) != 101) && ( T000T3_A147MetodologiaFases_Codigo[0] == A147MetodologiaFases_Codigo ) )
         {
            ZM0T30( 8) ;
            RcdFound30 = 1;
            A150PadroesArtefatos_Codigo = T000T3_A150PadroesArtefatos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A150PadroesArtefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A150PadroesArtefatos_Codigo), 6, 0)));
            A151PadroesArtefatos_Descricao = T000T3_A151PadroesArtefatos_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A151PadroesArtefatos_Descricao", A151PadroesArtefatos_Descricao);
            Z150PadroesArtefatos_Codigo = A150PadroesArtefatos_Codigo;
            sMode30 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0T30( ) ;
            if ( AnyError == 1 )
            {
               RcdFound30 = 0;
               InitializeNonKey0T30( ) ;
            }
            Gx_mode = sMode30;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound30 = 0;
            InitializeNonKey0T30( ) ;
            sMode30 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode30;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0T30( ) ;
         if ( RcdFound30 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound30 = 0;
         /* Using cursor T000T8 */
         pr_default.execute(6, new Object[] {A150PadroesArtefatos_Codigo, A147MetodologiaFases_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T000T8_A150PadroesArtefatos_Codigo[0] < A150PadroesArtefatos_Codigo ) ) && ( T000T8_A147MetodologiaFases_Codigo[0] == A147MetodologiaFases_Codigo ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T000T8_A150PadroesArtefatos_Codigo[0] > A150PadroesArtefatos_Codigo ) ) && ( T000T8_A147MetodologiaFases_Codigo[0] == A147MetodologiaFases_Codigo ) )
            {
               A150PadroesArtefatos_Codigo = T000T8_A150PadroesArtefatos_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A150PadroesArtefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A150PadroesArtefatos_Codigo), 6, 0)));
               RcdFound30 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound30 = 0;
         /* Using cursor T000T9 */
         pr_default.execute(7, new Object[] {A150PadroesArtefatos_Codigo, A147MetodologiaFases_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T000T9_A150PadroesArtefatos_Codigo[0] > A150PadroesArtefatos_Codigo ) ) && ( T000T9_A147MetodologiaFases_Codigo[0] == A147MetodologiaFases_Codigo ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T000T9_A150PadroesArtefatos_Codigo[0] < A150PadroesArtefatos_Codigo ) ) && ( T000T9_A147MetodologiaFases_Codigo[0] == A147MetodologiaFases_Codigo ) )
            {
               A150PadroesArtefatos_Codigo = T000T9_A150PadroesArtefatos_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A150PadroesArtefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A150PadroesArtefatos_Codigo), 6, 0)));
               RcdFound30 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0T30( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtPadroesArtefatos_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0T30( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound30 == 1 )
            {
               if ( A150PadroesArtefatos_Codigo != Z150PadroesArtefatos_Codigo )
               {
                  A150PadroesArtefatos_Codigo = Z150PadroesArtefatos_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A150PadroesArtefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A150PadroesArtefatos_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "PADROESARTEFATOS_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtPadroesArtefatos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtPadroesArtefatos_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0T30( ) ;
                  GX_FocusControl = edtPadroesArtefatos_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A150PadroesArtefatos_Codigo != Z150PadroesArtefatos_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtPadroesArtefatos_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0T30( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "PADROESARTEFATOS_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtPadroesArtefatos_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtPadroesArtefatos_Descricao_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0T30( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A150PadroesArtefatos_Codigo != Z150PadroesArtefatos_Codigo )
         {
            A150PadroesArtefatos_Codigo = Z150PadroesArtefatos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A150PadroesArtefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A150PadroesArtefatos_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "PADROESARTEFATOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtPadroesArtefatos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtPadroesArtefatos_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0T30( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000T2 */
            pr_default.execute(0, new Object[] {A150PadroesArtefatos_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"PadroesArtefatos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z151PadroesArtefatos_Descricao, T000T2_A151PadroesArtefatos_Descricao[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z151PadroesArtefatos_Descricao, T000T2_A151PadroesArtefatos_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("padroesartefatos:[seudo value changed for attri]"+"PadroesArtefatos_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z151PadroesArtefatos_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T000T2_A151PadroesArtefatos_Descricao[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"PadroesArtefatos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0T30( )
      {
         BeforeValidate0T30( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0T30( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0T30( 0) ;
            CheckOptimisticConcurrency0T30( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0T30( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0T30( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000T10 */
                     pr_default.execute(8, new Object[] {A147MetodologiaFases_Codigo, A151PadroesArtefatos_Descricao});
                     A150PadroesArtefatos_Codigo = T000T10_A150PadroesArtefatos_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A150PadroesArtefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A150PadroesArtefatos_Codigo), 6, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("PadroesArtefatos") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption0T0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0T30( ) ;
            }
            EndLevel0T30( ) ;
         }
         CloseExtendedTableCursors0T30( ) ;
      }

      protected void Update0T30( )
      {
         BeforeValidate0T30( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0T30( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0T30( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0T30( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0T30( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000T11 */
                     pr_default.execute(9, new Object[] {A147MetodologiaFases_Codigo, A151PadroesArtefatos_Descricao, A150PadroesArtefatos_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("PadroesArtefatos") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"PadroesArtefatos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0T30( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0T30( ) ;
         }
         CloseExtendedTableCursors0T30( ) ;
      }

      protected void DeferredUpdate0T30( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0T30( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0T30( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0T30( ) ;
            AfterConfirm0T30( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0T30( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000T12 */
                  pr_default.execute(10, new Object[] {A150PadroesArtefatos_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("PadroesArtefatos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode30 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0T30( ) ;
         Gx_mode = sMode30;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0T30( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel0T30( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0T30( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "PadroesArtefatos");
            if ( AnyError == 0 )
            {
               ConfirmValues0T0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "PadroesArtefatos");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0T30( )
      {
         /* Scan By routine */
         /* Using cursor T000T13 */
         pr_default.execute(11, new Object[] {A147MetodologiaFases_Codigo});
         RcdFound30 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound30 = 1;
            A150PadroesArtefatos_Codigo = T000T13_A150PadroesArtefatos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A150PadroesArtefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A150PadroesArtefatos_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0T30( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound30 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound30 = 1;
            A150PadroesArtefatos_Codigo = T000T13_A150PadroesArtefatos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A150PadroesArtefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A150PadroesArtefatos_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd0T30( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm0T30( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0T30( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0T30( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0T30( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0T30( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0T30( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0T30( )
      {
         edtPadroesArtefatos_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPadroesArtefatos_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPadroesArtefatos_Descricao_Enabled), 5, 0)));
         edtMetodologiaFases_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMetodologiaFases_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMetodologiaFases_Codigo_Enabled), 5, 0)));
         edtMetodologiaFases_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMetodologiaFases_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMetodologiaFases_Nome_Enabled), 5, 0)));
         edtPadroesArtefatos_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPadroesArtefatos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPadroesArtefatos_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0T0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117173733");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("padroesartefatos.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7PadroesArtefatos_Codigo) + "," + UrlEncode("" +A147MetodologiaFases_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z150PadroesArtefatos_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z150PadroesArtefatos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z151PadroesArtefatos_Descricao", Z151PadroesArtefatos_Descricao);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N147MetodologiaFases_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A147MetodologiaFases_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vPADROESARTEFATOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7PadroesArtefatos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_METODOLOGIAFASES_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_MetodologiaFases_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "METODOLOGIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A137Metodologia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "METODOLOGIA_DESCRICAO", A138Metodologia_Descricao);
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vPADROESARTEFATOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7PadroesArtefatos_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "PadroesArtefatos";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A150PadroesArtefatos_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("padroesartefatos:[SendSecurityCheck value for]"+"PadroesArtefatos_Codigo:"+context.localUtil.Format( (decimal)(A150PadroesArtefatos_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("padroesartefatos:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("padroesartefatos.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7PadroesArtefatos_Codigo) + "," + UrlEncode("" +A147MetodologiaFases_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "PadroesArtefatos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Padroes Artefatos" ;
      }

      protected void InitializeNonKey0T30( )
      {
         A151PadroesArtefatos_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A151PadroesArtefatos_Descricao", A151PadroesArtefatos_Descricao);
         Z151PadroesArtefatos_Descricao = "";
      }

      protected void InitAll0T30( )
      {
         A150PadroesArtefatos_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A150PadroesArtefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A150PadroesArtefatos_Codigo), 6, 0)));
         InitializeNonKey0T30( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117173755");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("padroesartefatos.js", "?20203117173755");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockpadroesartefatos_descricao_Internalname = "TEXTBLOCKPADROESARTEFATOS_DESCRICAO";
         edtPadroesArtefatos_Descricao_Internalname = "PADROESARTEFATOS_DESCRICAO";
         lblTextblockmetodologiafases_codigo_Internalname = "TEXTBLOCKMETODOLOGIAFASES_CODIGO";
         edtMetodologiaFases_Codigo_Internalname = "METODOLOGIAFASES_CODIGO";
         lblTextblockmetodologiafases_nome_Internalname = "TEXTBLOCKMETODOLOGIAFASES_NOME";
         edtMetodologiaFases_Nome_Internalname = "METODOLOGIAFASES_NOME";
         tblTablemergedmetodologiafases_codigo_Internalname = "TABLEMERGEDMETODOLOGIAFASES_CODIGO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtPadroesArtefatos_Codigo_Internalname = "PADROESARTEFATOS_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Artefato";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Padroes Artefatos";
         edtMetodologiaFases_Nome_Jsonclick = "";
         edtMetodologiaFases_Nome_Enabled = 0;
         edtMetodologiaFases_Codigo_Jsonclick = "";
         edtMetodologiaFases_Codigo_Enabled = 0;
         edtPadroesArtefatos_Descricao_Jsonclick = "";
         edtPadroesArtefatos_Descricao_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtPadroesArtefatos_Codigo_Jsonclick = "";
         edtPadroesArtefatos_Codigo_Enabled = 0;
         edtPadroesArtefatos_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7PadroesArtefatos_Codigo',fld:'vPADROESARTEFATOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A147MetodologiaFases_Codigo',fld:'METODOLOGIAFASES_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120T2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z151PadroesArtefatos_Descricao = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockpadroesartefatos_descricao_Jsonclick = "";
         A151PadroesArtefatos_Descricao = "";
         lblTextblockmetodologiafases_codigo_Jsonclick = "";
         lblTextblockmetodologiafases_nome_Jsonclick = "";
         A148MetodologiaFases_Nome = "";
         A138Metodologia_Descricao = "";
         AV13Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode30 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z148MetodologiaFases_Nome = "";
         Z138Metodologia_Descricao = "";
         T000T4_A148MetodologiaFases_Nome = new String[] {""} ;
         T000T4_A137Metodologia_Codigo = new int[1] ;
         T000T5_A138Metodologia_Descricao = new String[] {""} ;
         T000T6_A147MetodologiaFases_Codigo = new int[1] ;
         T000T6_A150PadroesArtefatos_Codigo = new int[1] ;
         T000T6_A151PadroesArtefatos_Descricao = new String[] {""} ;
         T000T6_A148MetodologiaFases_Nome = new String[] {""} ;
         T000T6_A138Metodologia_Descricao = new String[] {""} ;
         T000T6_A137Metodologia_Codigo = new int[1] ;
         T000T7_A150PadroesArtefatos_Codigo = new int[1] ;
         T000T3_A147MetodologiaFases_Codigo = new int[1] ;
         T000T3_A150PadroesArtefatos_Codigo = new int[1] ;
         T000T3_A151PadroesArtefatos_Descricao = new String[] {""} ;
         T000T8_A150PadroesArtefatos_Codigo = new int[1] ;
         T000T8_A147MetodologiaFases_Codigo = new int[1] ;
         T000T9_A150PadroesArtefatos_Codigo = new int[1] ;
         T000T9_A147MetodologiaFases_Codigo = new int[1] ;
         T000T2_A147MetodologiaFases_Codigo = new int[1] ;
         T000T2_A150PadroesArtefatos_Codigo = new int[1] ;
         T000T2_A151PadroesArtefatos_Descricao = new String[] {""} ;
         T000T10_A150PadroesArtefatos_Codigo = new int[1] ;
         T000T13_A150PadroesArtefatos_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.padroesartefatos__default(),
            new Object[][] {
                new Object[] {
               T000T2_A147MetodologiaFases_Codigo, T000T2_A150PadroesArtefatos_Codigo, T000T2_A151PadroesArtefatos_Descricao
               }
               , new Object[] {
               T000T3_A147MetodologiaFases_Codigo, T000T3_A150PadroesArtefatos_Codigo, T000T3_A151PadroesArtefatos_Descricao
               }
               , new Object[] {
               T000T4_A148MetodologiaFases_Nome, T000T4_A137Metodologia_Codigo
               }
               , new Object[] {
               T000T5_A138Metodologia_Descricao
               }
               , new Object[] {
               T000T6_A147MetodologiaFases_Codigo, T000T6_A150PadroesArtefatos_Codigo, T000T6_A151PadroesArtefatos_Descricao, T000T6_A148MetodologiaFases_Nome, T000T6_A138Metodologia_Descricao, T000T6_A137Metodologia_Codigo
               }
               , new Object[] {
               T000T7_A150PadroesArtefatos_Codigo
               }
               , new Object[] {
               T000T8_A150PadroesArtefatos_Codigo, T000T8_A147MetodologiaFases_Codigo
               }
               , new Object[] {
               T000T9_A150PadroesArtefatos_Codigo, T000T9_A147MetodologiaFases_Codigo
               }
               , new Object[] {
               T000T10_A150PadroesArtefatos_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000T13_A150PadroesArtefatos_Codigo
               }
            }
         );
         N147MetodologiaFases_Codigo = 0;
         Z147MetodologiaFases_Codigo = 0;
         A147MetodologiaFases_Codigo = 0;
         AV13Pgmname = "PadroesArtefatos";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound30 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private int wcpOAV7PadroesArtefatos_Codigo ;
      private int wcpOA147MetodologiaFases_Codigo ;
      private int Z150PadroesArtefatos_Codigo ;
      private int N147MetodologiaFases_Codigo ;
      private int AV7PadroesArtefatos_Codigo ;
      private int A147MetodologiaFases_Codigo ;
      private int trnEnded ;
      private int A150PadroesArtefatos_Codigo ;
      private int edtPadroesArtefatos_Codigo_Enabled ;
      private int edtPadroesArtefatos_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtPadroesArtefatos_Descricao_Enabled ;
      private int edtMetodologiaFases_Codigo_Enabled ;
      private int edtMetodologiaFases_Nome_Enabled ;
      private int AV11Insert_MetodologiaFases_Codigo ;
      private int A137Metodologia_Codigo ;
      private int AV14GXV1 ;
      private int Z147MetodologiaFases_Codigo ;
      private int Z137Metodologia_Codigo ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtPadroesArtefatos_Descricao_Internalname ;
      private String edtPadroesArtefatos_Codigo_Internalname ;
      private String edtPadroesArtefatos_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockpadroesartefatos_descricao_Internalname ;
      private String lblTextblockpadroesartefatos_descricao_Jsonclick ;
      private String edtPadroesArtefatos_Descricao_Jsonclick ;
      private String lblTextblockmetodologiafases_codigo_Internalname ;
      private String lblTextblockmetodologiafases_codigo_Jsonclick ;
      private String tblTablemergedmetodologiafases_codigo_Internalname ;
      private String edtMetodologiaFases_Codigo_Internalname ;
      private String edtMetodologiaFases_Codigo_Jsonclick ;
      private String lblTextblockmetodologiafases_nome_Internalname ;
      private String lblTextblockmetodologiafases_nome_Jsonclick ;
      private String edtMetodologiaFases_Nome_Internalname ;
      private String A148MetodologiaFases_Nome ;
      private String edtMetodologiaFases_Nome_Jsonclick ;
      private String AV13Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode30 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z148MetodologiaFases_Nome ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String Z151PadroesArtefatos_Descricao ;
      private String A151PadroesArtefatos_Descricao ;
      private String A138Metodologia_Descricao ;
      private String Z138Metodologia_Descricao ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_MetodologiaFases_Codigo ;
      private IDataStoreProvider pr_default ;
      private String[] T000T4_A148MetodologiaFases_Nome ;
      private int[] T000T4_A137Metodologia_Codigo ;
      private String[] T000T5_A138Metodologia_Descricao ;
      private int[] T000T6_A147MetodologiaFases_Codigo ;
      private int[] T000T6_A150PadroesArtefatos_Codigo ;
      private String[] T000T6_A151PadroesArtefatos_Descricao ;
      private String[] T000T6_A148MetodologiaFases_Nome ;
      private String[] T000T6_A138Metodologia_Descricao ;
      private int[] T000T6_A137Metodologia_Codigo ;
      private int[] T000T7_A150PadroesArtefatos_Codigo ;
      private int[] T000T3_A147MetodologiaFases_Codigo ;
      private int[] T000T3_A150PadroesArtefatos_Codigo ;
      private String[] T000T3_A151PadroesArtefatos_Descricao ;
      private int[] T000T8_A150PadroesArtefatos_Codigo ;
      private int[] T000T8_A147MetodologiaFases_Codigo ;
      private int[] T000T9_A150PadroesArtefatos_Codigo ;
      private int[] T000T9_A147MetodologiaFases_Codigo ;
      private int[] T000T2_A147MetodologiaFases_Codigo ;
      private int[] T000T2_A150PadroesArtefatos_Codigo ;
      private String[] T000T2_A151PadroesArtefatos_Descricao ;
      private int[] T000T10_A150PadroesArtefatos_Codigo ;
      private int[] T000T13_A150PadroesArtefatos_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class padroesartefatos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000T4 ;
          prmT000T4 = new Object[] {
          new Object[] {"@MetodologiaFases_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000T5 ;
          prmT000T5 = new Object[] {
          new Object[] {"@Metodologia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000T6 ;
          prmT000T6 = new Object[] {
          new Object[] {"@PadroesArtefatos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@MetodologiaFases_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000T7 ;
          prmT000T7 = new Object[] {
          new Object[] {"@PadroesArtefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000T3 ;
          prmT000T3 = new Object[] {
          new Object[] {"@PadroesArtefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000T8 ;
          prmT000T8 = new Object[] {
          new Object[] {"@PadroesArtefatos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@MetodologiaFases_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000T9 ;
          prmT000T9 = new Object[] {
          new Object[] {"@PadroesArtefatos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@MetodologiaFases_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000T2 ;
          prmT000T2 = new Object[] {
          new Object[] {"@PadroesArtefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000T10 ;
          prmT000T10 = new Object[] {
          new Object[] {"@MetodologiaFases_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@PadroesArtefatos_Descricao",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmT000T11 ;
          prmT000T11 = new Object[] {
          new Object[] {"@MetodologiaFases_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@PadroesArtefatos_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@PadroesArtefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000T12 ;
          prmT000T12 = new Object[] {
          new Object[] {"@PadroesArtefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000T13 ;
          prmT000T13 = new Object[] {
          new Object[] {"@MetodologiaFases_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000T2", "SELECT [MetodologiaFases_Codigo], [PadroesArtefatos_Codigo], [PadroesArtefatos_Descricao] FROM [PadroesArtefatos] WITH (UPDLOCK) WHERE [PadroesArtefatos_Codigo] = @PadroesArtefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000T2,1,0,true,false )
             ,new CursorDef("T000T3", "SELECT [MetodologiaFases_Codigo], [PadroesArtefatos_Codigo], [PadroesArtefatos_Descricao] FROM [PadroesArtefatos] WITH (NOLOCK) WHERE [PadroesArtefatos_Codigo] = @PadroesArtefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000T3,1,0,true,false )
             ,new CursorDef("T000T4", "SELECT [MetodologiaFases_Nome], [Metodologia_Codigo] FROM [MetodologiaFases] WITH (NOLOCK) WHERE [MetodologiaFases_Codigo] = @MetodologiaFases_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000T4,1,0,true,false )
             ,new CursorDef("T000T5", "SELECT [Metodologia_Descricao] FROM [Metodologia] WITH (NOLOCK) WHERE [Metodologia_Codigo] = @Metodologia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000T5,1,0,true,false )
             ,new CursorDef("T000T6", "SELECT TM1.[MetodologiaFases_Codigo], TM1.[PadroesArtefatos_Codigo], TM1.[PadroesArtefatos_Descricao], T2.[MetodologiaFases_Nome], T3.[Metodologia_Descricao], T2.[Metodologia_Codigo] FROM (([PadroesArtefatos] TM1 WITH (NOLOCK) INNER JOIN [MetodologiaFases] T2 WITH (NOLOCK) ON T2.[MetodologiaFases_Codigo] = TM1.[MetodologiaFases_Codigo]) INNER JOIN [Metodologia] T3 WITH (NOLOCK) ON T3.[Metodologia_Codigo] = T2.[Metodologia_Codigo]) WHERE TM1.[PadroesArtefatos_Codigo] = @PadroesArtefatos_Codigo and TM1.[MetodologiaFases_Codigo] = @MetodologiaFases_Codigo ORDER BY TM1.[PadroesArtefatos_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000T6,100,0,true,false )
             ,new CursorDef("T000T7", "SELECT [PadroesArtefatos_Codigo] FROM [PadroesArtefatos] WITH (NOLOCK) WHERE [PadroesArtefatos_Codigo] = @PadroesArtefatos_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000T7,1,0,true,false )
             ,new CursorDef("T000T8", "SELECT TOP 1 [PadroesArtefatos_Codigo], [MetodologiaFases_Codigo] FROM [PadroesArtefatos] WITH (NOLOCK) WHERE ( [PadroesArtefatos_Codigo] > @PadroesArtefatos_Codigo) and [MetodologiaFases_Codigo] = @MetodologiaFases_Codigo ORDER BY [PadroesArtefatos_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000T8,1,0,true,true )
             ,new CursorDef("T000T9", "SELECT TOP 1 [PadroesArtefatos_Codigo], [MetodologiaFases_Codigo] FROM [PadroesArtefatos] WITH (NOLOCK) WHERE ( [PadroesArtefatos_Codigo] < @PadroesArtefatos_Codigo) and [MetodologiaFases_Codigo] = @MetodologiaFases_Codigo ORDER BY [PadroesArtefatos_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000T9,1,0,true,true )
             ,new CursorDef("T000T10", "INSERT INTO [PadroesArtefatos]([MetodologiaFases_Codigo], [PadroesArtefatos_Descricao]) VALUES(@MetodologiaFases_Codigo, @PadroesArtefatos_Descricao); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000T10)
             ,new CursorDef("T000T11", "UPDATE [PadroesArtefatos] SET [MetodologiaFases_Codigo]=@MetodologiaFases_Codigo, [PadroesArtefatos_Descricao]=@PadroesArtefatos_Descricao  WHERE [PadroesArtefatos_Codigo] = @PadroesArtefatos_Codigo", GxErrorMask.GX_NOMASK,prmT000T11)
             ,new CursorDef("T000T12", "DELETE FROM [PadroesArtefatos]  WHERE [PadroesArtefatos_Codigo] = @PadroesArtefatos_Codigo", GxErrorMask.GX_NOMASK,prmT000T12)
             ,new CursorDef("T000T13", "SELECT [PadroesArtefatos_Codigo] FROM [PadroesArtefatos] WITH (NOLOCK) WHERE [MetodologiaFases_Codigo] = @MetodologiaFases_Codigo ORDER BY [PadroesArtefatos_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000T13,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
