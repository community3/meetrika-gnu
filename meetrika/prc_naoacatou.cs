/*
               File: PRC_NaoAcatou
        Description: Nao Acatou
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:5.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_naoacatou : GXProcedure
   {
      public prc_naoacatou( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_naoacatou( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( decimal aP0_NewPFB ,
                           decimal aP1_NewPFL ,
                           bool aP2_NovaContagem ,
                           String aP3_Observacao )
      {
         this.AV15NewPFB = aP0_NewPFB;
         this.AV14NewPFL = aP1_NewPFL;
         this.AV13NovaContagem = aP2_NovaContagem;
         this.AV8Observacao = aP3_Observacao;
         initialize();
         executePrivate();
      }

      public void executeSubmit( decimal aP0_NewPFB ,
                                 decimal aP1_NewPFL ,
                                 bool aP2_NovaContagem ,
                                 String aP3_Observacao )
      {
         prc_naoacatou objprc_naoacatou;
         objprc_naoacatou = new prc_naoacatou();
         objprc_naoacatou.AV15NewPFB = aP0_NewPFB;
         objprc_naoacatou.AV14NewPFL = aP1_NewPFL;
         objprc_naoacatou.AV13NovaContagem = aP2_NovaContagem;
         objprc_naoacatou.AV8Observacao = aP3_Observacao;
         objprc_naoacatou.context.SetSubmitInitialConfig(context);
         objprc_naoacatou.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_naoacatou);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_naoacatou)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17Selecionadas.FromXml(AV18WebSession.Get("Codigos"), "Collection");
         AV22GXV1 = 1;
         while ( AV22GXV1 <= AV17Selecionadas.Count )
         {
            AV11Codigo = (int)(AV17Selecionadas.GetNumeric(AV22GXV1));
            /* Using cursor P00922 */
            pr_default.execute(0, new Object[] {AV11Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A456ContagemResultado_Codigo = P00922_A456ContagemResultado_Codigo[0];
               A602ContagemResultado_OSVinculada = P00922_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = P00922_n602ContagemResultado_OSVinculada[0];
               A890ContagemResultado_Responsavel = P00922_A890ContagemResultado_Responsavel[0];
               n890ContagemResultado_Responsavel = P00922_n890ContagemResultado_Responsavel[0];
               AV16OSVinculada = A602ContagemResultado_OSVinculada;
               AV10UserId = A890ContagemResultado_Responsavel;
               if ( AV16OSVinculada > 0 )
               {
                  AV16OSVinculada = AV11Codigo;
                  AV11Codigo = A602ContagemResultado_OSVinculada;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            if ( (0==AV16OSVinculada) )
            {
               AV24GXLvl19 = 0;
               /* Using cursor P00924 */
               pr_default.execute(1, new Object[] {AV11Codigo});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A602ContagemResultado_OSVinculada = P00924_A602ContagemResultado_OSVinculada[0];
                  n602ContagemResultado_OSVinculada = P00924_n602ContagemResultado_OSVinculada[0];
                  A456ContagemResultado_Codigo = P00924_A456ContagemResultado_Codigo[0];
                  A531ContagemResultado_StatusUltCnt = P00924_A531ContagemResultado_StatusUltCnt[0];
                  A584ContagemResultado_ContadorFM = P00924_A584ContagemResultado_ContadorFM[0];
                  n584ContagemResultado_ContadorFM = P00924_n584ContagemResultado_ContadorFM[0];
                  A531ContagemResultado_StatusUltCnt = P00924_A531ContagemResultado_StatusUltCnt[0];
                  A584ContagemResultado_ContadorFM = P00924_A584ContagemResultado_ContadorFM[0];
                  n584ContagemResultado_ContadorFM = P00924_n584ContagemResultado_ContadorFM[0];
                  AV24GXLvl19 = 1;
                  AV16OSVinculada = AV11Codigo;
                  AV11Codigo = A456ContagemResultado_Codigo;
                  AV10UserId = A584ContagemResultado_ContadorFM;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(1);
               }
               pr_default.close(1);
               if ( AV24GXLvl19 == 0 )
               {
                  AV19Acoes = "ND";
                  /* Using cursor P00925 */
                  pr_default.execute(2, new Object[] {AV11Codigo, AV19Acoes});
                  while ( (pr_default.getStatus(2) != 101) )
                  {
                     A894LogResponsavel_Acao = P00925_A894LogResponsavel_Acao[0];
                     A1797LogResponsavel_Codigo = P00925_A1797LogResponsavel_Codigo[0];
                     A896LogResponsavel_Owner = P00925_A896LogResponsavel_Owner[0];
                     A892LogResponsavel_DemandaCod = P00925_A892LogResponsavel_DemandaCod[0];
                     n892LogResponsavel_DemandaCod = P00925_n892LogResponsavel_DemandaCod[0];
                     GXt_boolean1 = A1149LogResponsavel_OwnerEhContratante;
                     new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean1) ;
                     A1149LogResponsavel_OwnerEhContratante = GXt_boolean1;
                     if ( ! A1149LogResponsavel_OwnerEhContratante )
                     {
                        AV12Responsavel_Codigo = A896LogResponsavel_Owner;
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                     }
                     pr_default.readNext(2);
                  }
                  pr_default.close(2);
               }
            }
            else
            {
               /* Using cursor P00927 */
               pr_default.execute(3, new Object[] {AV11Codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A456ContagemResultado_Codigo = P00927_A456ContagemResultado_Codigo[0];
                  A584ContagemResultado_ContadorFM = P00927_A584ContagemResultado_ContadorFM[0];
                  n584ContagemResultado_ContadorFM = P00927_n584ContagemResultado_ContadorFM[0];
                  A584ContagemResultado_ContadorFM = P00927_A584ContagemResultado_ContadorFM[0];
                  n584ContagemResultado_ContadorFM = P00927_n584ContagemResultado_ContadorFM[0];
                  AV10UserId = A584ContagemResultado_ContadorFM;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(3);
            }
            new prc_naoacata(context ).execute(  AV11Codigo,  AV16OSVinculada,  AV10UserId,  AV15NewPFB,  AV14NewPFL,  AV12Responsavel_Codigo,  AV8Observacao,  AV13NovaContagem,  false,  true) ;
            AV22GXV1 = (int)(AV22GXV1+1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Selecionadas = new GxSimpleCollection();
         AV18WebSession = context.GetSession();
         scmdbuf = "";
         P00922_A456ContagemResultado_Codigo = new int[1] ;
         P00922_A602ContagemResultado_OSVinculada = new int[1] ;
         P00922_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00922_A890ContagemResultado_Responsavel = new int[1] ;
         P00922_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00924_A602ContagemResultado_OSVinculada = new int[1] ;
         P00924_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00924_A456ContagemResultado_Codigo = new int[1] ;
         P00924_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00924_A584ContagemResultado_ContadorFM = new int[1] ;
         P00924_n584ContagemResultado_ContadorFM = new bool[] {false} ;
         AV19Acoes = "";
         P00925_A894LogResponsavel_Acao = new String[] {""} ;
         P00925_A1797LogResponsavel_Codigo = new long[1] ;
         P00925_A896LogResponsavel_Owner = new int[1] ;
         P00925_A892LogResponsavel_DemandaCod = new int[1] ;
         P00925_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         A894LogResponsavel_Acao = "";
         P00927_A456ContagemResultado_Codigo = new int[1] ;
         P00927_A584ContagemResultado_ContadorFM = new int[1] ;
         P00927_n584ContagemResultado_ContadorFM = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_naoacatou__default(),
            new Object[][] {
                new Object[] {
               P00922_A456ContagemResultado_Codigo, P00922_A602ContagemResultado_OSVinculada, P00922_n602ContagemResultado_OSVinculada, P00922_A890ContagemResultado_Responsavel, P00922_n890ContagemResultado_Responsavel
               }
               , new Object[] {
               P00924_A602ContagemResultado_OSVinculada, P00924_n602ContagemResultado_OSVinculada, P00924_A456ContagemResultado_Codigo, P00924_A531ContagemResultado_StatusUltCnt, P00924_A584ContagemResultado_ContadorFM
               }
               , new Object[] {
               P00925_A894LogResponsavel_Acao, P00925_A1797LogResponsavel_Codigo, P00925_A896LogResponsavel_Owner, P00925_A892LogResponsavel_DemandaCod, P00925_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               P00927_A456ContagemResultado_Codigo, P00927_A584ContagemResultado_ContadorFM, P00927_n584ContagemResultado_ContadorFM
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV24GXLvl19 ;
      private short A531ContagemResultado_StatusUltCnt ;
      private int AV22GXV1 ;
      private int AV11Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private int A890ContagemResultado_Responsavel ;
      private int AV16OSVinculada ;
      private int AV10UserId ;
      private int A584ContagemResultado_ContadorFM ;
      private int A896LogResponsavel_Owner ;
      private int A892LogResponsavel_DemandaCod ;
      private int AV12Responsavel_Codigo ;
      private long A1797LogResponsavel_Codigo ;
      private decimal AV15NewPFB ;
      private decimal AV14NewPFL ;
      private String scmdbuf ;
      private String AV19Acoes ;
      private String A894LogResponsavel_Acao ;
      private bool AV13NovaContagem ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n584ContagemResultado_ContadorFM ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool A1149LogResponsavel_OwnerEhContratante ;
      private bool GXt_boolean1 ;
      private String AV8Observacao ;
      private IGxSession AV18WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00922_A456ContagemResultado_Codigo ;
      private int[] P00922_A602ContagemResultado_OSVinculada ;
      private bool[] P00922_n602ContagemResultado_OSVinculada ;
      private int[] P00922_A890ContagemResultado_Responsavel ;
      private bool[] P00922_n890ContagemResultado_Responsavel ;
      private int[] P00924_A602ContagemResultado_OSVinculada ;
      private bool[] P00924_n602ContagemResultado_OSVinculada ;
      private int[] P00924_A456ContagemResultado_Codigo ;
      private short[] P00924_A531ContagemResultado_StatusUltCnt ;
      private int[] P00924_A584ContagemResultado_ContadorFM ;
      private bool[] P00924_n584ContagemResultado_ContadorFM ;
      private String[] P00925_A894LogResponsavel_Acao ;
      private long[] P00925_A1797LogResponsavel_Codigo ;
      private int[] P00925_A896LogResponsavel_Owner ;
      private int[] P00925_A892LogResponsavel_DemandaCod ;
      private bool[] P00925_n892LogResponsavel_DemandaCod ;
      private int[] P00927_A456ContagemResultado_Codigo ;
      private int[] P00927_A584ContagemResultado_ContadorFM ;
      private bool[] P00927_n584ContagemResultado_ContadorFM ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV17Selecionadas ;
   }

   public class prc_naoacatou__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00922 ;
          prmP00922 = new Object[] {
          new Object[] {"@AV11Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00924 ;
          prmP00924 = new Object[] {
          new Object[] {"@AV11Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00925 ;
          prmP00925 = new Object[] {
          new Object[] {"@AV11Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19Acoes",SqlDbType.Char,20,0}
          } ;
          Object[] prmP00927 ;
          prmP00927 = new Object[] {
          new Object[] {"@AV11Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00922", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_OSVinculada], [ContagemResultado_Responsavel] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV11Codigo ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00922,1,0,false,true )
             ,new CursorDef("P00924", "SELECT TOP 1 T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_Codigo], COALESCE( T2.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T2.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo], MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_OSVinculada] = @AV11Codigo) AND (COALESCE( T2.[ContagemResultado_StatusUltCnt], 0) = 7) ORDER BY T1.[ContagemResultado_OSVinculada] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00924,1,0,false,true )
             ,new CursorDef("P00925", "SELECT [LogResponsavel_Acao], [LogResponsavel_Codigo], [LogResponsavel_Owner], [LogResponsavel_DemandaCod] FROM [LogResponsavel] WITH (NOLOCK) WHERE ([LogResponsavel_DemandaCod] = @AV11Codigo) AND ((CHARINDEX(RTRIM([LogResponsavel_Acao]), @AV19Acoes)) > 0) ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00925,100,0,true,false )
             ,new CursorDef("P00927", "SELECT TOP 1 T1.[ContagemResultado_Codigo], COALESCE( T2.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV11Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00927,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((long[]) buf[1])[0] = rslt.getLong(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
