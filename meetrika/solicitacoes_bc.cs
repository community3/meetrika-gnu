/*
               File: Solicitacoes_BC
        Description: Solicitacoes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:21:4.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class solicitacoes_bc : GXHttpHandler, IGxSilentTrn
   {
      public solicitacoes_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public solicitacoes_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow1S67( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey1S67( ) ;
         standaloneModal( ) ;
         AddRow1S67( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E111S2 */
            E111S2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z439Solicitacoes_Codigo = A439Solicitacoes_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_1S0( )
      {
         BeforeValidate1S67( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1S67( ) ;
            }
            else
            {
               CheckExtendedTable1S67( ) ;
               if ( AnyError == 0 )
               {
                  ZM1S67( 7) ;
                  ZM1S67( 8) ;
                  ZM1S67( 9) ;
                  ZM1S67( 10) ;
                  ZM1S67( 11) ;
               }
               CloseExtendedTableCursors1S67( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E121S2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV7WWPContext) ;
         AV8TrnContext.FromXml(AV9WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV8TrnContext.gxTpr_Transactionname, AV18Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV19GXV1 = 1;
            while ( AV19GXV1 <= AV8TrnContext.gxTpr_Attributes.Count )
            {
               AV15TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV8TrnContext.gxTpr_Attributes.Item(AV19GXV1));
               if ( StringUtil.StrCmp(AV15TrnContextAtt.gxTpr_Attributename, "Contratada_Codigo") == 0 )
               {
                  AV10Insert_Contratada_Codigo = (int)(NumberUtil.Val( AV15TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV15TrnContextAtt.gxTpr_Attributename, "Sistema_Codigo") == 0 )
               {
                  AV16Insert_Sistema_Codigo = (int)(NumberUtil.Val( AV15TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV15TrnContextAtt.gxTpr_Attributename, "Servico_Codigo") == 0 )
               {
                  AV11Insert_Servico_Codigo = (int)(NumberUtil.Val( AV15TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV15TrnContextAtt.gxTpr_Attributename, "Solicitacoes_Usuario") == 0 )
               {
                  AV13Insert_Solicitacoes_Usuario = (int)(NumberUtil.Val( AV15TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV15TrnContextAtt.gxTpr_Attributename, "Solicitacoes_Usuario_Ult") == 0 )
               {
                  AV14Insert_Solicitacoes_Usuario_Ult = (int)(NumberUtil.Val( AV15TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV19GXV1 = (int)(AV19GXV1+1);
            }
         }
      }

      protected void E111S2( )
      {
         /* After Trn Routine */
      }

      protected void ZM1S67( short GX_JID )
      {
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            Z440Solicitacoes_Novo_Projeto = A440Solicitacoes_Novo_Projeto;
            Z444Solicitacoes_Data = A444Solicitacoes_Data;
            Z445Solicitacoes_Data_Ult = A445Solicitacoes_Data_Ult;
            Z446Solicitacoes_Status = A446Solicitacoes_Status;
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z155Servico_Codigo = A155Servico_Codigo;
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z442Solicitacoes_Usuario = A442Solicitacoes_Usuario;
            Z443Solicitacoes_Usuario_Ult = A443Solicitacoes_Usuario_Ult;
         }
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -6 )
         {
            Z439Solicitacoes_Codigo = A439Solicitacoes_Codigo;
            Z440Solicitacoes_Novo_Projeto = A440Solicitacoes_Novo_Projeto;
            Z441Solicitacoes_Objetivo = A441Solicitacoes_Objetivo;
            Z444Solicitacoes_Data = A444Solicitacoes_Data;
            Z445Solicitacoes_Data_Ult = A445Solicitacoes_Data_Ult;
            Z446Solicitacoes_Status = A446Solicitacoes_Status;
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z155Servico_Codigo = A155Servico_Codigo;
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z442Solicitacoes_Usuario = A442Solicitacoes_Usuario;
            Z443Solicitacoes_Usuario_Ult = A443Solicitacoes_Usuario_Ult;
         }
      }

      protected void standaloneNotModal( )
      {
         AV18Pgmname = "Solicitacoes_BC";
      }

      protected void standaloneModal( )
      {
      }

      protected void Load1S67( )
      {
         /* Using cursor BC001S9 */
         pr_default.execute(7, new Object[] {A439Solicitacoes_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound67 = 1;
            A440Solicitacoes_Novo_Projeto = BC001S9_A440Solicitacoes_Novo_Projeto[0];
            A441Solicitacoes_Objetivo = BC001S9_A441Solicitacoes_Objetivo[0];
            n441Solicitacoes_Objetivo = BC001S9_n441Solicitacoes_Objetivo[0];
            A444Solicitacoes_Data = BC001S9_A444Solicitacoes_Data[0];
            A445Solicitacoes_Data_Ult = BC001S9_A445Solicitacoes_Data_Ult[0];
            A446Solicitacoes_Status = BC001S9_A446Solicitacoes_Status[0];
            A127Sistema_Codigo = BC001S9_A127Sistema_Codigo[0];
            A155Servico_Codigo = BC001S9_A155Servico_Codigo[0];
            A39Contratada_Codigo = BC001S9_A39Contratada_Codigo[0];
            A442Solicitacoes_Usuario = BC001S9_A442Solicitacoes_Usuario[0];
            A443Solicitacoes_Usuario_Ult = BC001S9_A443Solicitacoes_Usuario_Ult[0];
            ZM1S67( -6) ;
         }
         pr_default.close(7);
         OnLoadActions1S67( ) ;
      }

      protected void OnLoadActions1S67( )
      {
      }

      protected void CheckExtendedTable1S67( )
      {
         standaloneModal( ) ;
         /* Using cursor BC001S6 */
         pr_default.execute(4, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "CONTRATADA_CODIGO");
            AnyError = 1;
         }
         pr_default.close(4);
         /* Using cursor BC001S4 */
         pr_default.execute(2, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "SISTEMA_CODIGO");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor BC001S5 */
         pr_default.execute(3, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
            AnyError = 1;
         }
         pr_default.close(3);
         /* Using cursor BC001S7 */
         pr_default.execute(5, new Object[] {A442Solicitacoes_Usuario});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Solicitacoes_Usuario'.", "ForeignKeyNotFound", 1, "SOLICITACOES_USUARIO");
            AnyError = 1;
         }
         pr_default.close(5);
         if ( ! ( (DateTime.MinValue==A444Solicitacoes_Data) || ( A444Solicitacoes_Data >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data de inclus�o fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC001S8 */
         pr_default.execute(6, new Object[] {A443Solicitacoes_Usuario_Ult});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Solicitacoes_Usuario Ult'.", "ForeignKeyNotFound", 1, "SOLICITACOES_USUARIO_ULT");
            AnyError = 1;
         }
         pr_default.close(6);
         if ( ! ( (DateTime.MinValue==A445Solicitacoes_Data_Ult) || ( A445Solicitacoes_Data_Ult >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data da Ultima Modifica��o fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( StringUtil.StrCmp(A446Solicitacoes_Status, "A") == 0 ) || ( StringUtil.StrCmp(A446Solicitacoes_Status, "E") == 0 ) || ( StringUtil.StrCmp(A446Solicitacoes_Status, "R") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Solicitacoes_Status fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors1S67( )
      {
         pr_default.close(4);
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(5);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1S67( )
      {
         /* Using cursor BC001S10 */
         pr_default.execute(8, new Object[] {A439Solicitacoes_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound67 = 1;
         }
         else
         {
            RcdFound67 = 0;
         }
         pr_default.close(8);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC001S3 */
         pr_default.execute(1, new Object[] {A439Solicitacoes_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1S67( 6) ;
            RcdFound67 = 1;
            A439Solicitacoes_Codigo = BC001S3_A439Solicitacoes_Codigo[0];
            A440Solicitacoes_Novo_Projeto = BC001S3_A440Solicitacoes_Novo_Projeto[0];
            A441Solicitacoes_Objetivo = BC001S3_A441Solicitacoes_Objetivo[0];
            n441Solicitacoes_Objetivo = BC001S3_n441Solicitacoes_Objetivo[0];
            A444Solicitacoes_Data = BC001S3_A444Solicitacoes_Data[0];
            A445Solicitacoes_Data_Ult = BC001S3_A445Solicitacoes_Data_Ult[0];
            A446Solicitacoes_Status = BC001S3_A446Solicitacoes_Status[0];
            A127Sistema_Codigo = BC001S3_A127Sistema_Codigo[0];
            A155Servico_Codigo = BC001S3_A155Servico_Codigo[0];
            A39Contratada_Codigo = BC001S3_A39Contratada_Codigo[0];
            A442Solicitacoes_Usuario = BC001S3_A442Solicitacoes_Usuario[0];
            A443Solicitacoes_Usuario_Ult = BC001S3_A443Solicitacoes_Usuario_Ult[0];
            Z439Solicitacoes_Codigo = A439Solicitacoes_Codigo;
            sMode67 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load1S67( ) ;
            if ( AnyError == 1 )
            {
               RcdFound67 = 0;
               InitializeNonKey1S67( ) ;
            }
            Gx_mode = sMode67;
         }
         else
         {
            RcdFound67 = 0;
            InitializeNonKey1S67( ) ;
            sMode67 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode67;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1S67( ) ;
         if ( RcdFound67 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_1S0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency1S67( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC001S2 */
            pr_default.execute(0, new Object[] {A439Solicitacoes_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Solicitacoes"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z440Solicitacoes_Novo_Projeto, BC001S2_A440Solicitacoes_Novo_Projeto[0]) != 0 ) || ( Z444Solicitacoes_Data != BC001S2_A444Solicitacoes_Data[0] ) || ( Z445Solicitacoes_Data_Ult != BC001S2_A445Solicitacoes_Data_Ult[0] ) || ( StringUtil.StrCmp(Z446Solicitacoes_Status, BC001S2_A446Solicitacoes_Status[0]) != 0 ) || ( Z127Sistema_Codigo != BC001S2_A127Sistema_Codigo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z155Servico_Codigo != BC001S2_A155Servico_Codigo[0] ) || ( Z39Contratada_Codigo != BC001S2_A39Contratada_Codigo[0] ) || ( Z442Solicitacoes_Usuario != BC001S2_A442Solicitacoes_Usuario[0] ) || ( Z443Solicitacoes_Usuario_Ult != BC001S2_A443Solicitacoes_Usuario_Ult[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Solicitacoes"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1S67( )
      {
         BeforeValidate1S67( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1S67( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1S67( 0) ;
            CheckOptimisticConcurrency1S67( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1S67( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1S67( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001S11 */
                     pr_default.execute(9, new Object[] {A440Solicitacoes_Novo_Projeto, n441Solicitacoes_Objetivo, A441Solicitacoes_Objetivo, A444Solicitacoes_Data, A445Solicitacoes_Data_Ult, A446Solicitacoes_Status, A127Sistema_Codigo, A155Servico_Codigo, A39Contratada_Codigo, A442Solicitacoes_Usuario, A443Solicitacoes_Usuario_Ult});
                     A439Solicitacoes_Codigo = BC001S11_A439Solicitacoes_Codigo[0];
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Solicitacoes") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1S67( ) ;
            }
            EndLevel1S67( ) ;
         }
         CloseExtendedTableCursors1S67( ) ;
      }

      protected void Update1S67( )
      {
         BeforeValidate1S67( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1S67( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1S67( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1S67( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1S67( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001S12 */
                     pr_default.execute(10, new Object[] {A440Solicitacoes_Novo_Projeto, n441Solicitacoes_Objetivo, A441Solicitacoes_Objetivo, A444Solicitacoes_Data, A445Solicitacoes_Data_Ult, A446Solicitacoes_Status, A127Sistema_Codigo, A155Servico_Codigo, A39Contratada_Codigo, A442Solicitacoes_Usuario, A443Solicitacoes_Usuario_Ult, A439Solicitacoes_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("Solicitacoes") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Solicitacoes"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1S67( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1S67( ) ;
         }
         CloseExtendedTableCursors1S67( ) ;
      }

      protected void DeferredUpdate1S67( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate1S67( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1S67( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1S67( ) ;
            AfterConfirm1S67( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1S67( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC001S13 */
                  pr_default.execute(11, new Object[] {A439Solicitacoes_Codigo});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("Solicitacoes") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode67 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel1S67( ) ;
         Gx_mode = sMode67;
      }

      protected void OnDeleteControls1S67( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor BC001S14 */
            pr_default.execute(12, new Object[] {A439Solicitacoes_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Solicitacoes Itens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
         }
      }

      protected void EndLevel1S67( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1S67( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart1S67( )
      {
         /* Scan By routine */
         /* Using cursor BC001S15 */
         pr_default.execute(13, new Object[] {A439Solicitacoes_Codigo});
         RcdFound67 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound67 = 1;
            A439Solicitacoes_Codigo = BC001S15_A439Solicitacoes_Codigo[0];
            A440Solicitacoes_Novo_Projeto = BC001S15_A440Solicitacoes_Novo_Projeto[0];
            A441Solicitacoes_Objetivo = BC001S15_A441Solicitacoes_Objetivo[0];
            n441Solicitacoes_Objetivo = BC001S15_n441Solicitacoes_Objetivo[0];
            A444Solicitacoes_Data = BC001S15_A444Solicitacoes_Data[0];
            A445Solicitacoes_Data_Ult = BC001S15_A445Solicitacoes_Data_Ult[0];
            A446Solicitacoes_Status = BC001S15_A446Solicitacoes_Status[0];
            A127Sistema_Codigo = BC001S15_A127Sistema_Codigo[0];
            A155Servico_Codigo = BC001S15_A155Servico_Codigo[0];
            A39Contratada_Codigo = BC001S15_A39Contratada_Codigo[0];
            A442Solicitacoes_Usuario = BC001S15_A442Solicitacoes_Usuario[0];
            A443Solicitacoes_Usuario_Ult = BC001S15_A443Solicitacoes_Usuario_Ult[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext1S67( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound67 = 0;
         ScanKeyLoad1S67( ) ;
      }

      protected void ScanKeyLoad1S67( )
      {
         sMode67 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound67 = 1;
            A439Solicitacoes_Codigo = BC001S15_A439Solicitacoes_Codigo[0];
            A440Solicitacoes_Novo_Projeto = BC001S15_A440Solicitacoes_Novo_Projeto[0];
            A441Solicitacoes_Objetivo = BC001S15_A441Solicitacoes_Objetivo[0];
            n441Solicitacoes_Objetivo = BC001S15_n441Solicitacoes_Objetivo[0];
            A444Solicitacoes_Data = BC001S15_A444Solicitacoes_Data[0];
            A445Solicitacoes_Data_Ult = BC001S15_A445Solicitacoes_Data_Ult[0];
            A446Solicitacoes_Status = BC001S15_A446Solicitacoes_Status[0];
            A127Sistema_Codigo = BC001S15_A127Sistema_Codigo[0];
            A155Servico_Codigo = BC001S15_A155Servico_Codigo[0];
            A39Contratada_Codigo = BC001S15_A39Contratada_Codigo[0];
            A442Solicitacoes_Usuario = BC001S15_A442Solicitacoes_Usuario[0];
            A443Solicitacoes_Usuario_Ult = BC001S15_A443Solicitacoes_Usuario_Ult[0];
         }
         Gx_mode = sMode67;
      }

      protected void ScanKeyEnd1S67( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm1S67( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1S67( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1S67( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1S67( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1S67( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1S67( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1S67( )
      {
      }

      protected void AddRow1S67( )
      {
         VarsToRow67( bcSolicitacoes) ;
      }

      protected void ReadRow1S67( )
      {
         RowToVars67( bcSolicitacoes, 1) ;
      }

      protected void InitializeNonKey1S67( )
      {
         A39Contratada_Codigo = 0;
         A127Sistema_Codigo = 0;
         A155Servico_Codigo = 0;
         A440Solicitacoes_Novo_Projeto = "";
         A441Solicitacoes_Objetivo = "";
         n441Solicitacoes_Objetivo = false;
         A442Solicitacoes_Usuario = 0;
         A444Solicitacoes_Data = (DateTime)(DateTime.MinValue);
         A443Solicitacoes_Usuario_Ult = 0;
         A445Solicitacoes_Data_Ult = (DateTime)(DateTime.MinValue);
         A446Solicitacoes_Status = "";
         Z440Solicitacoes_Novo_Projeto = "";
         Z444Solicitacoes_Data = (DateTime)(DateTime.MinValue);
         Z445Solicitacoes_Data_Ult = (DateTime)(DateTime.MinValue);
         Z446Solicitacoes_Status = "";
         Z127Sistema_Codigo = 0;
         Z155Servico_Codigo = 0;
         Z39Contratada_Codigo = 0;
         Z442Solicitacoes_Usuario = 0;
         Z443Solicitacoes_Usuario_Ult = 0;
      }

      protected void InitAll1S67( )
      {
         A439Solicitacoes_Codigo = 0;
         InitializeNonKey1S67( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow67( SdtSolicitacoes obj67 )
      {
         obj67.gxTpr_Mode = Gx_mode;
         obj67.gxTpr_Contratada_codigo = A39Contratada_Codigo;
         obj67.gxTpr_Sistema_codigo = A127Sistema_Codigo;
         obj67.gxTpr_Servico_codigo = A155Servico_Codigo;
         obj67.gxTpr_Solicitacoes_novo_projeto = A440Solicitacoes_Novo_Projeto;
         obj67.gxTpr_Solicitacoes_objetivo = A441Solicitacoes_Objetivo;
         obj67.gxTpr_Solicitacoes_usuario = A442Solicitacoes_Usuario;
         obj67.gxTpr_Solicitacoes_data = A444Solicitacoes_Data;
         obj67.gxTpr_Solicitacoes_usuario_ult = A443Solicitacoes_Usuario_Ult;
         obj67.gxTpr_Solicitacoes_data_ult = A445Solicitacoes_Data_Ult;
         obj67.gxTpr_Solicitacoes_status = A446Solicitacoes_Status;
         obj67.gxTpr_Solicitacoes_codigo = A439Solicitacoes_Codigo;
         obj67.gxTpr_Solicitacoes_codigo_Z = Z439Solicitacoes_Codigo;
         obj67.gxTpr_Contratada_codigo_Z = Z39Contratada_Codigo;
         obj67.gxTpr_Sistema_codigo_Z = Z127Sistema_Codigo;
         obj67.gxTpr_Servico_codigo_Z = Z155Servico_Codigo;
         obj67.gxTpr_Solicitacoes_novo_projeto_Z = Z440Solicitacoes_Novo_Projeto;
         obj67.gxTpr_Solicitacoes_usuario_Z = Z442Solicitacoes_Usuario;
         obj67.gxTpr_Solicitacoes_data_Z = Z444Solicitacoes_Data;
         obj67.gxTpr_Solicitacoes_usuario_ult_Z = Z443Solicitacoes_Usuario_Ult;
         obj67.gxTpr_Solicitacoes_data_ult_Z = Z445Solicitacoes_Data_Ult;
         obj67.gxTpr_Solicitacoes_status_Z = Z446Solicitacoes_Status;
         obj67.gxTpr_Solicitacoes_objetivo_N = (short)(Convert.ToInt16(n441Solicitacoes_Objetivo));
         obj67.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow67( SdtSolicitacoes obj67 )
      {
         obj67.gxTpr_Solicitacoes_codigo = A439Solicitacoes_Codigo;
         return  ;
      }

      public void RowToVars67( SdtSolicitacoes obj67 ,
                               int forceLoad )
      {
         Gx_mode = obj67.gxTpr_Mode;
         A39Contratada_Codigo = obj67.gxTpr_Contratada_codigo;
         A127Sistema_Codigo = obj67.gxTpr_Sistema_codigo;
         A155Servico_Codigo = obj67.gxTpr_Servico_codigo;
         A440Solicitacoes_Novo_Projeto = obj67.gxTpr_Solicitacoes_novo_projeto;
         A441Solicitacoes_Objetivo = obj67.gxTpr_Solicitacoes_objetivo;
         n441Solicitacoes_Objetivo = false;
         A442Solicitacoes_Usuario = obj67.gxTpr_Solicitacoes_usuario;
         A444Solicitacoes_Data = obj67.gxTpr_Solicitacoes_data;
         A443Solicitacoes_Usuario_Ult = obj67.gxTpr_Solicitacoes_usuario_ult;
         A445Solicitacoes_Data_Ult = obj67.gxTpr_Solicitacoes_data_ult;
         A446Solicitacoes_Status = obj67.gxTpr_Solicitacoes_status;
         A439Solicitacoes_Codigo = obj67.gxTpr_Solicitacoes_codigo;
         Z439Solicitacoes_Codigo = obj67.gxTpr_Solicitacoes_codigo_Z;
         Z39Contratada_Codigo = obj67.gxTpr_Contratada_codigo_Z;
         Z127Sistema_Codigo = obj67.gxTpr_Sistema_codigo_Z;
         Z155Servico_Codigo = obj67.gxTpr_Servico_codigo_Z;
         Z440Solicitacoes_Novo_Projeto = obj67.gxTpr_Solicitacoes_novo_projeto_Z;
         Z442Solicitacoes_Usuario = obj67.gxTpr_Solicitacoes_usuario_Z;
         Z444Solicitacoes_Data = obj67.gxTpr_Solicitacoes_data_Z;
         Z443Solicitacoes_Usuario_Ult = obj67.gxTpr_Solicitacoes_usuario_ult_Z;
         Z445Solicitacoes_Data_Ult = obj67.gxTpr_Solicitacoes_data_ult_Z;
         Z446Solicitacoes_Status = obj67.gxTpr_Solicitacoes_status_Z;
         n441Solicitacoes_Objetivo = (bool)(Convert.ToBoolean(obj67.gxTpr_Solicitacoes_objetivo_N));
         Gx_mode = obj67.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A439Solicitacoes_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey1S67( ) ;
         ScanKeyStart1S67( ) ;
         if ( RcdFound67 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z439Solicitacoes_Codigo = A439Solicitacoes_Codigo;
         }
         ZM1S67( -6) ;
         OnLoadActions1S67( ) ;
         AddRow1S67( ) ;
         ScanKeyEnd1S67( ) ;
         if ( RcdFound67 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars67( bcSolicitacoes, 0) ;
         ScanKeyStart1S67( ) ;
         if ( RcdFound67 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z439Solicitacoes_Codigo = A439Solicitacoes_Codigo;
         }
         ZM1S67( -6) ;
         OnLoadActions1S67( ) ;
         AddRow1S67( ) ;
         ScanKeyEnd1S67( ) ;
         if ( RcdFound67 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars67( bcSolicitacoes, 0) ;
         nKeyPressed = 1;
         GetKey1S67( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert1S67( ) ;
         }
         else
         {
            if ( RcdFound67 == 1 )
            {
               if ( A439Solicitacoes_Codigo != Z439Solicitacoes_Codigo )
               {
                  A439Solicitacoes_Codigo = Z439Solicitacoes_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update1S67( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A439Solicitacoes_Codigo != Z439Solicitacoes_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1S67( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1S67( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow67( bcSolicitacoes) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars67( bcSolicitacoes, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey1S67( ) ;
         if ( RcdFound67 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A439Solicitacoes_Codigo != Z439Solicitacoes_Codigo )
            {
               A439Solicitacoes_Codigo = Z439Solicitacoes_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A439Solicitacoes_Codigo != Z439Solicitacoes_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         context.RollbackDataStores( "Solicitacoes_BC");
         VarsToRow67( bcSolicitacoes) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcSolicitacoes.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcSolicitacoes.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcSolicitacoes )
         {
            bcSolicitacoes = (SdtSolicitacoes)(sdt);
            if ( StringUtil.StrCmp(bcSolicitacoes.gxTpr_Mode, "") == 0 )
            {
               bcSolicitacoes.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow67( bcSolicitacoes) ;
            }
            else
            {
               RowToVars67( bcSolicitacoes, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcSolicitacoes.gxTpr_Mode, "") == 0 )
            {
               bcSolicitacoes.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars67( bcSolicitacoes, 1) ;
         return  ;
      }

      public SdtSolicitacoes Solicitacoes_BC
      {
         get {
            return bcSolicitacoes ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV7WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9WebSession = context.GetSession();
         AV18Pgmname = "";
         AV15TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z440Solicitacoes_Novo_Projeto = "";
         A440Solicitacoes_Novo_Projeto = "";
         Z444Solicitacoes_Data = (DateTime)(DateTime.MinValue);
         A444Solicitacoes_Data = (DateTime)(DateTime.MinValue);
         Z445Solicitacoes_Data_Ult = (DateTime)(DateTime.MinValue);
         A445Solicitacoes_Data_Ult = (DateTime)(DateTime.MinValue);
         Z446Solicitacoes_Status = "";
         A446Solicitacoes_Status = "";
         Z441Solicitacoes_Objetivo = "";
         A441Solicitacoes_Objetivo = "";
         BC001S9_A439Solicitacoes_Codigo = new int[1] ;
         BC001S9_A440Solicitacoes_Novo_Projeto = new String[] {""} ;
         BC001S9_A441Solicitacoes_Objetivo = new String[] {""} ;
         BC001S9_n441Solicitacoes_Objetivo = new bool[] {false} ;
         BC001S9_A444Solicitacoes_Data = new DateTime[] {DateTime.MinValue} ;
         BC001S9_A445Solicitacoes_Data_Ult = new DateTime[] {DateTime.MinValue} ;
         BC001S9_A446Solicitacoes_Status = new String[] {""} ;
         BC001S9_A127Sistema_Codigo = new int[1] ;
         BC001S9_A155Servico_Codigo = new int[1] ;
         BC001S9_A39Contratada_Codigo = new int[1] ;
         BC001S9_A442Solicitacoes_Usuario = new int[1] ;
         BC001S9_A443Solicitacoes_Usuario_Ult = new int[1] ;
         BC001S6_A39Contratada_Codigo = new int[1] ;
         BC001S4_A127Sistema_Codigo = new int[1] ;
         BC001S5_A155Servico_Codigo = new int[1] ;
         BC001S7_A442Solicitacoes_Usuario = new int[1] ;
         BC001S8_A443Solicitacoes_Usuario_Ult = new int[1] ;
         BC001S10_A439Solicitacoes_Codigo = new int[1] ;
         BC001S3_A439Solicitacoes_Codigo = new int[1] ;
         BC001S3_A440Solicitacoes_Novo_Projeto = new String[] {""} ;
         BC001S3_A441Solicitacoes_Objetivo = new String[] {""} ;
         BC001S3_n441Solicitacoes_Objetivo = new bool[] {false} ;
         BC001S3_A444Solicitacoes_Data = new DateTime[] {DateTime.MinValue} ;
         BC001S3_A445Solicitacoes_Data_Ult = new DateTime[] {DateTime.MinValue} ;
         BC001S3_A446Solicitacoes_Status = new String[] {""} ;
         BC001S3_A127Sistema_Codigo = new int[1] ;
         BC001S3_A155Servico_Codigo = new int[1] ;
         BC001S3_A39Contratada_Codigo = new int[1] ;
         BC001S3_A442Solicitacoes_Usuario = new int[1] ;
         BC001S3_A443Solicitacoes_Usuario_Ult = new int[1] ;
         sMode67 = "";
         BC001S2_A439Solicitacoes_Codigo = new int[1] ;
         BC001S2_A440Solicitacoes_Novo_Projeto = new String[] {""} ;
         BC001S2_A441Solicitacoes_Objetivo = new String[] {""} ;
         BC001S2_n441Solicitacoes_Objetivo = new bool[] {false} ;
         BC001S2_A444Solicitacoes_Data = new DateTime[] {DateTime.MinValue} ;
         BC001S2_A445Solicitacoes_Data_Ult = new DateTime[] {DateTime.MinValue} ;
         BC001S2_A446Solicitacoes_Status = new String[] {""} ;
         BC001S2_A127Sistema_Codigo = new int[1] ;
         BC001S2_A155Servico_Codigo = new int[1] ;
         BC001S2_A39Contratada_Codigo = new int[1] ;
         BC001S2_A442Solicitacoes_Usuario = new int[1] ;
         BC001S2_A443Solicitacoes_Usuario_Ult = new int[1] ;
         BC001S11_A439Solicitacoes_Codigo = new int[1] ;
         BC001S14_A447SolicitacoesItens_Codigo = new int[1] ;
         BC001S15_A439Solicitacoes_Codigo = new int[1] ;
         BC001S15_A440Solicitacoes_Novo_Projeto = new String[] {""} ;
         BC001S15_A441Solicitacoes_Objetivo = new String[] {""} ;
         BC001S15_n441Solicitacoes_Objetivo = new bool[] {false} ;
         BC001S15_A444Solicitacoes_Data = new DateTime[] {DateTime.MinValue} ;
         BC001S15_A445Solicitacoes_Data_Ult = new DateTime[] {DateTime.MinValue} ;
         BC001S15_A446Solicitacoes_Status = new String[] {""} ;
         BC001S15_A127Sistema_Codigo = new int[1] ;
         BC001S15_A155Servico_Codigo = new int[1] ;
         BC001S15_A39Contratada_Codigo = new int[1] ;
         BC001S15_A442Solicitacoes_Usuario = new int[1] ;
         BC001S15_A443Solicitacoes_Usuario_Ult = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.solicitacoes_bc__default(),
            new Object[][] {
                new Object[] {
               BC001S2_A439Solicitacoes_Codigo, BC001S2_A440Solicitacoes_Novo_Projeto, BC001S2_A441Solicitacoes_Objetivo, BC001S2_n441Solicitacoes_Objetivo, BC001S2_A444Solicitacoes_Data, BC001S2_A445Solicitacoes_Data_Ult, BC001S2_A446Solicitacoes_Status, BC001S2_A127Sistema_Codigo, BC001S2_A155Servico_Codigo, BC001S2_A39Contratada_Codigo,
               BC001S2_A442Solicitacoes_Usuario, BC001S2_A443Solicitacoes_Usuario_Ult
               }
               , new Object[] {
               BC001S3_A439Solicitacoes_Codigo, BC001S3_A440Solicitacoes_Novo_Projeto, BC001S3_A441Solicitacoes_Objetivo, BC001S3_n441Solicitacoes_Objetivo, BC001S3_A444Solicitacoes_Data, BC001S3_A445Solicitacoes_Data_Ult, BC001S3_A446Solicitacoes_Status, BC001S3_A127Sistema_Codigo, BC001S3_A155Servico_Codigo, BC001S3_A39Contratada_Codigo,
               BC001S3_A442Solicitacoes_Usuario, BC001S3_A443Solicitacoes_Usuario_Ult
               }
               , new Object[] {
               BC001S4_A127Sistema_Codigo
               }
               , new Object[] {
               BC001S5_A155Servico_Codigo
               }
               , new Object[] {
               BC001S6_A39Contratada_Codigo
               }
               , new Object[] {
               BC001S7_A442Solicitacoes_Usuario
               }
               , new Object[] {
               BC001S8_A443Solicitacoes_Usuario_Ult
               }
               , new Object[] {
               BC001S9_A439Solicitacoes_Codigo, BC001S9_A440Solicitacoes_Novo_Projeto, BC001S9_A441Solicitacoes_Objetivo, BC001S9_n441Solicitacoes_Objetivo, BC001S9_A444Solicitacoes_Data, BC001S9_A445Solicitacoes_Data_Ult, BC001S9_A446Solicitacoes_Status, BC001S9_A127Sistema_Codigo, BC001S9_A155Servico_Codigo, BC001S9_A39Contratada_Codigo,
               BC001S9_A442Solicitacoes_Usuario, BC001S9_A443Solicitacoes_Usuario_Ult
               }
               , new Object[] {
               BC001S10_A439Solicitacoes_Codigo
               }
               , new Object[] {
               BC001S11_A439Solicitacoes_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC001S14_A447SolicitacoesItens_Codigo
               }
               , new Object[] {
               BC001S15_A439Solicitacoes_Codigo, BC001S15_A440Solicitacoes_Novo_Projeto, BC001S15_A441Solicitacoes_Objetivo, BC001S15_n441Solicitacoes_Objetivo, BC001S15_A444Solicitacoes_Data, BC001S15_A445Solicitacoes_Data_Ult, BC001S15_A446Solicitacoes_Status, BC001S15_A127Sistema_Codigo, BC001S15_A155Servico_Codigo, BC001S15_A39Contratada_Codigo,
               BC001S15_A442Solicitacoes_Usuario, BC001S15_A443Solicitacoes_Usuario_Ult
               }
            }
         );
         AV18Pgmname = "Solicitacoes_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E121S2 */
         E121S2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound67 ;
      private int trnEnded ;
      private int Z439Solicitacoes_Codigo ;
      private int A439Solicitacoes_Codigo ;
      private int AV19GXV1 ;
      private int AV10Insert_Contratada_Codigo ;
      private int AV16Insert_Sistema_Codigo ;
      private int AV11Insert_Servico_Codigo ;
      private int AV13Insert_Solicitacoes_Usuario ;
      private int AV14Insert_Solicitacoes_Usuario_Ult ;
      private int Z127Sistema_Codigo ;
      private int A127Sistema_Codigo ;
      private int Z155Servico_Codigo ;
      private int A155Servico_Codigo ;
      private int Z39Contratada_Codigo ;
      private int A39Contratada_Codigo ;
      private int Z442Solicitacoes_Usuario ;
      private int A442Solicitacoes_Usuario ;
      private int Z443Solicitacoes_Usuario_Ult ;
      private int A443Solicitacoes_Usuario_Ult ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV18Pgmname ;
      private String Z440Solicitacoes_Novo_Projeto ;
      private String A440Solicitacoes_Novo_Projeto ;
      private String Z446Solicitacoes_Status ;
      private String A446Solicitacoes_Status ;
      private String sMode67 ;
      private DateTime Z444Solicitacoes_Data ;
      private DateTime A444Solicitacoes_Data ;
      private DateTime Z445Solicitacoes_Data_Ult ;
      private DateTime A445Solicitacoes_Data_Ult ;
      private bool n441Solicitacoes_Objetivo ;
      private bool Gx_longc ;
      private String Z441Solicitacoes_Objetivo ;
      private String A441Solicitacoes_Objetivo ;
      private IGxSession AV9WebSession ;
      private SdtSolicitacoes bcSolicitacoes ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC001S9_A439Solicitacoes_Codigo ;
      private String[] BC001S9_A440Solicitacoes_Novo_Projeto ;
      private String[] BC001S9_A441Solicitacoes_Objetivo ;
      private bool[] BC001S9_n441Solicitacoes_Objetivo ;
      private DateTime[] BC001S9_A444Solicitacoes_Data ;
      private DateTime[] BC001S9_A445Solicitacoes_Data_Ult ;
      private String[] BC001S9_A446Solicitacoes_Status ;
      private int[] BC001S9_A127Sistema_Codigo ;
      private int[] BC001S9_A155Servico_Codigo ;
      private int[] BC001S9_A39Contratada_Codigo ;
      private int[] BC001S9_A442Solicitacoes_Usuario ;
      private int[] BC001S9_A443Solicitacoes_Usuario_Ult ;
      private int[] BC001S6_A39Contratada_Codigo ;
      private int[] BC001S4_A127Sistema_Codigo ;
      private int[] BC001S5_A155Servico_Codigo ;
      private int[] BC001S7_A442Solicitacoes_Usuario ;
      private int[] BC001S8_A443Solicitacoes_Usuario_Ult ;
      private int[] BC001S10_A439Solicitacoes_Codigo ;
      private int[] BC001S3_A439Solicitacoes_Codigo ;
      private String[] BC001S3_A440Solicitacoes_Novo_Projeto ;
      private String[] BC001S3_A441Solicitacoes_Objetivo ;
      private bool[] BC001S3_n441Solicitacoes_Objetivo ;
      private DateTime[] BC001S3_A444Solicitacoes_Data ;
      private DateTime[] BC001S3_A445Solicitacoes_Data_Ult ;
      private String[] BC001S3_A446Solicitacoes_Status ;
      private int[] BC001S3_A127Sistema_Codigo ;
      private int[] BC001S3_A155Servico_Codigo ;
      private int[] BC001S3_A39Contratada_Codigo ;
      private int[] BC001S3_A442Solicitacoes_Usuario ;
      private int[] BC001S3_A443Solicitacoes_Usuario_Ult ;
      private int[] BC001S2_A439Solicitacoes_Codigo ;
      private String[] BC001S2_A440Solicitacoes_Novo_Projeto ;
      private String[] BC001S2_A441Solicitacoes_Objetivo ;
      private bool[] BC001S2_n441Solicitacoes_Objetivo ;
      private DateTime[] BC001S2_A444Solicitacoes_Data ;
      private DateTime[] BC001S2_A445Solicitacoes_Data_Ult ;
      private String[] BC001S2_A446Solicitacoes_Status ;
      private int[] BC001S2_A127Sistema_Codigo ;
      private int[] BC001S2_A155Servico_Codigo ;
      private int[] BC001S2_A39Contratada_Codigo ;
      private int[] BC001S2_A442Solicitacoes_Usuario ;
      private int[] BC001S2_A443Solicitacoes_Usuario_Ult ;
      private int[] BC001S11_A439Solicitacoes_Codigo ;
      private int[] BC001S14_A447SolicitacoesItens_Codigo ;
      private int[] BC001S15_A439Solicitacoes_Codigo ;
      private String[] BC001S15_A440Solicitacoes_Novo_Projeto ;
      private String[] BC001S15_A441Solicitacoes_Objetivo ;
      private bool[] BC001S15_n441Solicitacoes_Objetivo ;
      private DateTime[] BC001S15_A444Solicitacoes_Data ;
      private DateTime[] BC001S15_A445Solicitacoes_Data_Ult ;
      private String[] BC001S15_A446Solicitacoes_Status ;
      private int[] BC001S15_A127Sistema_Codigo ;
      private int[] BC001S15_A155Servico_Codigo ;
      private int[] BC001S15_A39Contratada_Codigo ;
      private int[] BC001S15_A442Solicitacoes_Usuario ;
      private int[] BC001S15_A443Solicitacoes_Usuario_Ult ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV7WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV15TrnContextAtt ;
   }

   public class solicitacoes_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC001S9 ;
          prmBC001S9 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001S6 ;
          prmBC001S6 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001S4 ;
          prmBC001S4 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001S5 ;
          prmBC001S5 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001S7 ;
          prmBC001S7 = new Object[] {
          new Object[] {"@Solicitacoes_Usuario",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001S8 ;
          prmBC001S8 = new Object[] {
          new Object[] {"@Solicitacoes_Usuario_Ult",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001S10 ;
          prmBC001S10 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001S3 ;
          prmBC001S3 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001S2 ;
          prmBC001S2 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001S11 ;
          prmBC001S11 = new Object[] {
          new Object[] {"@Solicitacoes_Novo_Projeto",SqlDbType.Char,1,0} ,
          new Object[] {"@Solicitacoes_Objetivo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Solicitacoes_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Solicitacoes_Data_Ult",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Solicitacoes_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Solicitacoes_Usuario",SqlDbType.Int,6,0} ,
          new Object[] {"@Solicitacoes_Usuario_Ult",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001S12 ;
          prmBC001S12 = new Object[] {
          new Object[] {"@Solicitacoes_Novo_Projeto",SqlDbType.Char,1,0} ,
          new Object[] {"@Solicitacoes_Objetivo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Solicitacoes_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Solicitacoes_Data_Ult",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Solicitacoes_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Solicitacoes_Usuario",SqlDbType.Int,6,0} ,
          new Object[] {"@Solicitacoes_Usuario_Ult",SqlDbType.Int,6,0} ,
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001S13 ;
          prmBC001S13 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001S14 ;
          prmBC001S14 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001S15 ;
          prmBC001S15 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC001S2", "SELECT [Solicitacoes_Codigo], [Solicitacoes_Novo_Projeto], [Solicitacoes_Objetivo], [Solicitacoes_Data], [Solicitacoes_Data_Ult], [Solicitacoes_Status], [Sistema_Codigo], [Servico_Codigo], [Contratada_Codigo], [Solicitacoes_Usuario] AS Solicitacoes_Usuario, [Solicitacoes_Usuario_Ult] AS Solicitacoes_Usuario_Ult FROM [Solicitacoes] WITH (UPDLOCK) WHERE [Solicitacoes_Codigo] = @Solicitacoes_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001S2,1,0,true,false )
             ,new CursorDef("BC001S3", "SELECT [Solicitacoes_Codigo], [Solicitacoes_Novo_Projeto], [Solicitacoes_Objetivo], [Solicitacoes_Data], [Solicitacoes_Data_Ult], [Solicitacoes_Status], [Sistema_Codigo], [Servico_Codigo], [Contratada_Codigo], [Solicitacoes_Usuario] AS Solicitacoes_Usuario, [Solicitacoes_Usuario_Ult] AS Solicitacoes_Usuario_Ult FROM [Solicitacoes] WITH (NOLOCK) WHERE [Solicitacoes_Codigo] = @Solicitacoes_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001S3,1,0,true,false )
             ,new CursorDef("BC001S4", "SELECT [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001S4,1,0,true,false )
             ,new CursorDef("BC001S5", "SELECT [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001S5,1,0,true,false )
             ,new CursorDef("BC001S6", "SELECT [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001S6,1,0,true,false )
             ,new CursorDef("BC001S7", "SELECT [Usuario_Codigo] AS Solicitacoes_Usuario FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Solicitacoes_Usuario ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001S7,1,0,true,false )
             ,new CursorDef("BC001S8", "SELECT [Usuario_Codigo] AS Solicitacoes_Usuario_Ult FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Solicitacoes_Usuario_Ult ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001S8,1,0,true,false )
             ,new CursorDef("BC001S9", "SELECT TM1.[Solicitacoes_Codigo], TM1.[Solicitacoes_Novo_Projeto], TM1.[Solicitacoes_Objetivo], TM1.[Solicitacoes_Data], TM1.[Solicitacoes_Data_Ult], TM1.[Solicitacoes_Status], TM1.[Sistema_Codigo], TM1.[Servico_Codigo], TM1.[Contratada_Codigo], TM1.[Solicitacoes_Usuario] AS Solicitacoes_Usuario, TM1.[Solicitacoes_Usuario_Ult] AS Solicitacoes_Usuario_Ult FROM [Solicitacoes] TM1 WITH (NOLOCK) WHERE TM1.[Solicitacoes_Codigo] = @Solicitacoes_Codigo ORDER BY TM1.[Solicitacoes_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001S9,100,0,true,false )
             ,new CursorDef("BC001S10", "SELECT [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Solicitacoes_Codigo] = @Solicitacoes_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001S10,1,0,true,false )
             ,new CursorDef("BC001S11", "INSERT INTO [Solicitacoes]([Solicitacoes_Novo_Projeto], [Solicitacoes_Objetivo], [Solicitacoes_Data], [Solicitacoes_Data_Ult], [Solicitacoes_Status], [Sistema_Codigo], [Servico_Codigo], [Contratada_Codigo], [Solicitacoes_Usuario], [Solicitacoes_Usuario_Ult]) VALUES(@Solicitacoes_Novo_Projeto, @Solicitacoes_Objetivo, @Solicitacoes_Data, @Solicitacoes_Data_Ult, @Solicitacoes_Status, @Sistema_Codigo, @Servico_Codigo, @Contratada_Codigo, @Solicitacoes_Usuario, @Solicitacoes_Usuario_Ult); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC001S11)
             ,new CursorDef("BC001S12", "UPDATE [Solicitacoes] SET [Solicitacoes_Novo_Projeto]=@Solicitacoes_Novo_Projeto, [Solicitacoes_Objetivo]=@Solicitacoes_Objetivo, [Solicitacoes_Data]=@Solicitacoes_Data, [Solicitacoes_Data_Ult]=@Solicitacoes_Data_Ult, [Solicitacoes_Status]=@Solicitacoes_Status, [Sistema_Codigo]=@Sistema_Codigo, [Servico_Codigo]=@Servico_Codigo, [Contratada_Codigo]=@Contratada_Codigo, [Solicitacoes_Usuario]=@Solicitacoes_Usuario, [Solicitacoes_Usuario_Ult]=@Solicitacoes_Usuario_Ult  WHERE [Solicitacoes_Codigo] = @Solicitacoes_Codigo", GxErrorMask.GX_NOMASK,prmBC001S12)
             ,new CursorDef("BC001S13", "DELETE FROM [Solicitacoes]  WHERE [Solicitacoes_Codigo] = @Solicitacoes_Codigo", GxErrorMask.GX_NOMASK,prmBC001S13)
             ,new CursorDef("BC001S14", "SELECT TOP 1 [SolicitacoesItens_Codigo] FROM [SolicitacoesItens] WITH (NOLOCK) WHERE [Solicitacoes_Codigo] = @Solicitacoes_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001S14,1,0,true,true )
             ,new CursorDef("BC001S15", "SELECT TM1.[Solicitacoes_Codigo], TM1.[Solicitacoes_Novo_Projeto], TM1.[Solicitacoes_Objetivo], TM1.[Solicitacoes_Data], TM1.[Solicitacoes_Data_Ult], TM1.[Solicitacoes_Status], TM1.[Sistema_Codigo], TM1.[Servico_Codigo], TM1.[Contratada_Codigo], TM1.[Solicitacoes_Usuario] AS Solicitacoes_Usuario, TM1.[Solicitacoes_Usuario_Ult] AS Solicitacoes_Usuario_Ult FROM [Solicitacoes] TM1 WITH (NOLOCK) WHERE TM1.[Solicitacoes_Codigo] = @Solicitacoes_Codigo ORDER BY TM1.[Solicitacoes_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001S15,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                ((int[]) buf[11])[0] = rslt.getInt(11) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                ((int[]) buf[11])[0] = rslt.getInt(11) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                ((int[]) buf[11])[0] = rslt.getInt(11) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                ((int[]) buf[11])[0] = rslt.getInt(11) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameterDatetime(3, (DateTime)parms[3]);
                stmt.SetParameterDatetime(4, (DateTime)parms[4]);
                stmt.SetParameter(5, (String)parms[5]);
                stmt.SetParameter(6, (int)parms[6]);
                stmt.SetParameter(7, (int)parms[7]);
                stmt.SetParameter(8, (int)parms[8]);
                stmt.SetParameter(9, (int)parms[9]);
                stmt.SetParameter(10, (int)parms[10]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameterDatetime(3, (DateTime)parms[3]);
                stmt.SetParameterDatetime(4, (DateTime)parms[4]);
                stmt.SetParameter(5, (String)parms[5]);
                stmt.SetParameter(6, (int)parms[6]);
                stmt.SetParameter(7, (int)parms[7]);
                stmt.SetParameter(8, (int)parms[8]);
                stmt.SetParameter(9, (int)parms[9]);
                stmt.SetParameter(10, (int)parms[10]);
                stmt.SetParameter(11, (int)parms[11]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
