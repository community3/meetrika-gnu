/*
               File: GetWWArtefatosFilterData
        Description: Get WWArtefatos Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:6.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwartefatosfilterdata : GXProcedure
   {
      public getwwartefatosfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwartefatosfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
         return AV23OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwartefatosfilterdata objgetwwartefatosfilterdata;
         objgetwwartefatosfilterdata = new getwwartefatosfilterdata();
         objgetwwartefatosfilterdata.AV14DDOName = aP0_DDOName;
         objgetwwartefatosfilterdata.AV12SearchTxt = aP1_SearchTxt;
         objgetwwartefatosfilterdata.AV13SearchTxtTo = aP2_SearchTxtTo;
         objgetwwartefatosfilterdata.AV18OptionsJson = "" ;
         objgetwwartefatosfilterdata.AV21OptionsDescJson = "" ;
         objgetwwartefatosfilterdata.AV23OptionIndexesJson = "" ;
         objgetwwartefatosfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwartefatosfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwartefatosfilterdata);
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwartefatosfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17Options = (IGxCollection)(new GxSimpleCollection());
         AV20OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV22OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV14DDOName), "DDO_ARTEFATOS_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADARTEFATOS_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV18OptionsJson = AV17Options.ToJSonString(false);
         AV21OptionsDescJson = AV20OptionsDesc.ToJSonString(false);
         AV23OptionIndexesJson = AV22OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get("WWArtefatosGridState"), "") == 0 )
         {
            AV27GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWArtefatosGridState"), "");
         }
         else
         {
            AV27GridState.FromXml(AV25Session.Get("WWArtefatosGridState"), "");
         }
         AV43GXV1 = 1;
         while ( AV43GXV1 <= AV27GridState.gxTpr_Filtervalues.Count )
         {
            AV28GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV27GridState.gxTpr_Filtervalues.Item(AV43GXV1));
            if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFARTEFATOS_DESCRICAO") == 0 )
            {
               AV10TFArtefatos_Descricao = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFARTEFATOS_DESCRICAO_SEL") == 0 )
            {
               AV11TFArtefatos_Descricao_Sel = AV28GridStateFilterValue.gxTpr_Value;
            }
            AV43GXV1 = (int)(AV43GXV1+1);
         }
         if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(1));
            AV30DynamicFiltersSelector1 = AV29GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV30DynamicFiltersSelector1, "ARTEFATOS_DESCRICAO") == 0 )
            {
               AV31DynamicFiltersOperator1 = AV29GridStateDynamicFilter.gxTpr_Operator;
               AV32Artefatos_Descricao1 = AV29GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV33DynamicFiltersEnabled2 = true;
               AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(2));
               AV34DynamicFiltersSelector2 = AV29GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "ARTEFATOS_DESCRICAO") == 0 )
               {
                  AV35DynamicFiltersOperator2 = AV29GridStateDynamicFilter.gxTpr_Operator;
                  AV36Artefatos_Descricao2 = AV29GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV27GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV37DynamicFiltersEnabled3 = true;
                  AV29GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV27GridState.gxTpr_Dynamicfilters.Item(3));
                  AV38DynamicFiltersSelector3 = AV29GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "ARTEFATOS_DESCRICAO") == 0 )
                  {
                     AV39DynamicFiltersOperator3 = AV29GridStateDynamicFilter.gxTpr_Operator;
                     AV40Artefatos_Descricao3 = AV29GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADARTEFATOS_DESCRICAOOPTIONS' Routine */
         AV10TFArtefatos_Descricao = AV12SearchTxt;
         AV11TFArtefatos_Descricao_Sel = "";
         AV45WWArtefatosDS_1_Dynamicfiltersselector1 = AV30DynamicFiltersSelector1;
         AV46WWArtefatosDS_2_Dynamicfiltersoperator1 = AV31DynamicFiltersOperator1;
         AV47WWArtefatosDS_3_Artefatos_descricao1 = AV32Artefatos_Descricao1;
         AV48WWArtefatosDS_4_Dynamicfiltersenabled2 = AV33DynamicFiltersEnabled2;
         AV49WWArtefatosDS_5_Dynamicfiltersselector2 = AV34DynamicFiltersSelector2;
         AV50WWArtefatosDS_6_Dynamicfiltersoperator2 = AV35DynamicFiltersOperator2;
         AV51WWArtefatosDS_7_Artefatos_descricao2 = AV36Artefatos_Descricao2;
         AV52WWArtefatosDS_8_Dynamicfiltersenabled3 = AV37DynamicFiltersEnabled3;
         AV53WWArtefatosDS_9_Dynamicfiltersselector3 = AV38DynamicFiltersSelector3;
         AV54WWArtefatosDS_10_Dynamicfiltersoperator3 = AV39DynamicFiltersOperator3;
         AV55WWArtefatosDS_11_Artefatos_descricao3 = AV40Artefatos_Descricao3;
         AV56WWArtefatosDS_12_Tfartefatos_descricao = AV10TFArtefatos_Descricao;
         AV57WWArtefatosDS_13_Tfartefatos_descricao_sel = AV11TFArtefatos_Descricao_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV45WWArtefatosDS_1_Dynamicfiltersselector1 ,
                                              AV46WWArtefatosDS_2_Dynamicfiltersoperator1 ,
                                              AV47WWArtefatosDS_3_Artefatos_descricao1 ,
                                              AV48WWArtefatosDS_4_Dynamicfiltersenabled2 ,
                                              AV49WWArtefatosDS_5_Dynamicfiltersselector2 ,
                                              AV50WWArtefatosDS_6_Dynamicfiltersoperator2 ,
                                              AV51WWArtefatosDS_7_Artefatos_descricao2 ,
                                              AV52WWArtefatosDS_8_Dynamicfiltersenabled3 ,
                                              AV53WWArtefatosDS_9_Dynamicfiltersselector3 ,
                                              AV54WWArtefatosDS_10_Dynamicfiltersoperator3 ,
                                              AV55WWArtefatosDS_11_Artefatos_descricao3 ,
                                              AV57WWArtefatosDS_13_Tfartefatos_descricao_sel ,
                                              AV56WWArtefatosDS_12_Tfartefatos_descricao ,
                                              A1751Artefatos_Descricao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV47WWArtefatosDS_3_Artefatos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV47WWArtefatosDS_3_Artefatos_descricao1), "%", "");
         lV47WWArtefatosDS_3_Artefatos_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV47WWArtefatosDS_3_Artefatos_descricao1), "%", "");
         lV51WWArtefatosDS_7_Artefatos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV51WWArtefatosDS_7_Artefatos_descricao2), "%", "");
         lV51WWArtefatosDS_7_Artefatos_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV51WWArtefatosDS_7_Artefatos_descricao2), "%", "");
         lV55WWArtefatosDS_11_Artefatos_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV55WWArtefatosDS_11_Artefatos_descricao3), "%", "");
         lV55WWArtefatosDS_11_Artefatos_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV55WWArtefatosDS_11_Artefatos_descricao3), "%", "");
         lV56WWArtefatosDS_12_Tfartefatos_descricao = StringUtil.Concat( StringUtil.RTrim( AV56WWArtefatosDS_12_Tfartefatos_descricao), "%", "");
         /* Using cursor P00SH2 */
         pr_default.execute(0, new Object[] {lV47WWArtefatosDS_3_Artefatos_descricao1, lV47WWArtefatosDS_3_Artefatos_descricao1, lV51WWArtefatosDS_7_Artefatos_descricao2, lV51WWArtefatosDS_7_Artefatos_descricao2, lV55WWArtefatosDS_11_Artefatos_descricao3, lV55WWArtefatosDS_11_Artefatos_descricao3, lV56WWArtefatosDS_12_Tfartefatos_descricao, AV57WWArtefatosDS_13_Tfartefatos_descricao_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKSH2 = false;
            A1751Artefatos_Descricao = P00SH2_A1751Artefatos_Descricao[0];
            A1749Artefatos_Codigo = P00SH2_A1749Artefatos_Codigo[0];
            AV24count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00SH2_A1751Artefatos_Descricao[0], A1751Artefatos_Descricao) == 0 ) )
            {
               BRKSH2 = false;
               A1749Artefatos_Codigo = P00SH2_A1749Artefatos_Codigo[0];
               AV24count = (long)(AV24count+1);
               BRKSH2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1751Artefatos_Descricao)) )
            {
               AV16Option = A1751Artefatos_Descricao;
               AV17Options.Add(AV16Option, 0);
               AV22OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV24count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV17Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSH2 )
            {
               BRKSH2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Options = new GxSimpleCollection();
         AV20OptionsDesc = new GxSimpleCollection();
         AV22OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV25Session = context.GetSession();
         AV27GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV28GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFArtefatos_Descricao = "";
         AV11TFArtefatos_Descricao_Sel = "";
         AV29GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV30DynamicFiltersSelector1 = "";
         AV32Artefatos_Descricao1 = "";
         AV34DynamicFiltersSelector2 = "";
         AV36Artefatos_Descricao2 = "";
         AV38DynamicFiltersSelector3 = "";
         AV40Artefatos_Descricao3 = "";
         AV45WWArtefatosDS_1_Dynamicfiltersselector1 = "";
         AV47WWArtefatosDS_3_Artefatos_descricao1 = "";
         AV49WWArtefatosDS_5_Dynamicfiltersselector2 = "";
         AV51WWArtefatosDS_7_Artefatos_descricao2 = "";
         AV53WWArtefatosDS_9_Dynamicfiltersselector3 = "";
         AV55WWArtefatosDS_11_Artefatos_descricao3 = "";
         AV56WWArtefatosDS_12_Tfartefatos_descricao = "";
         AV57WWArtefatosDS_13_Tfartefatos_descricao_sel = "";
         scmdbuf = "";
         lV47WWArtefatosDS_3_Artefatos_descricao1 = "";
         lV51WWArtefatosDS_7_Artefatos_descricao2 = "";
         lV55WWArtefatosDS_11_Artefatos_descricao3 = "";
         lV56WWArtefatosDS_12_Tfartefatos_descricao = "";
         A1751Artefatos_Descricao = "";
         P00SH2_A1751Artefatos_Descricao = new String[] {""} ;
         P00SH2_A1749Artefatos_Codigo = new int[1] ;
         AV16Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwartefatosfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00SH2_A1751Artefatos_Descricao, P00SH2_A1749Artefatos_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV31DynamicFiltersOperator1 ;
      private short AV35DynamicFiltersOperator2 ;
      private short AV39DynamicFiltersOperator3 ;
      private short AV46WWArtefatosDS_2_Dynamicfiltersoperator1 ;
      private short AV50WWArtefatosDS_6_Dynamicfiltersoperator2 ;
      private short AV54WWArtefatosDS_10_Dynamicfiltersoperator3 ;
      private int AV43GXV1 ;
      private int A1749Artefatos_Codigo ;
      private long AV24count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool AV33DynamicFiltersEnabled2 ;
      private bool AV37DynamicFiltersEnabled3 ;
      private bool AV48WWArtefatosDS_4_Dynamicfiltersenabled2 ;
      private bool AV52WWArtefatosDS_8_Dynamicfiltersenabled3 ;
      private bool BRKSH2 ;
      private String AV23OptionIndexesJson ;
      private String AV18OptionsJson ;
      private String AV21OptionsDescJson ;
      private String AV14DDOName ;
      private String AV12SearchTxt ;
      private String AV13SearchTxtTo ;
      private String AV10TFArtefatos_Descricao ;
      private String AV11TFArtefatos_Descricao_Sel ;
      private String AV30DynamicFiltersSelector1 ;
      private String AV32Artefatos_Descricao1 ;
      private String AV34DynamicFiltersSelector2 ;
      private String AV36Artefatos_Descricao2 ;
      private String AV38DynamicFiltersSelector3 ;
      private String AV40Artefatos_Descricao3 ;
      private String AV45WWArtefatosDS_1_Dynamicfiltersselector1 ;
      private String AV47WWArtefatosDS_3_Artefatos_descricao1 ;
      private String AV49WWArtefatosDS_5_Dynamicfiltersselector2 ;
      private String AV51WWArtefatosDS_7_Artefatos_descricao2 ;
      private String AV53WWArtefatosDS_9_Dynamicfiltersselector3 ;
      private String AV55WWArtefatosDS_11_Artefatos_descricao3 ;
      private String AV56WWArtefatosDS_12_Tfartefatos_descricao ;
      private String AV57WWArtefatosDS_13_Tfartefatos_descricao_sel ;
      private String lV47WWArtefatosDS_3_Artefatos_descricao1 ;
      private String lV51WWArtefatosDS_7_Artefatos_descricao2 ;
      private String lV55WWArtefatosDS_11_Artefatos_descricao3 ;
      private String lV56WWArtefatosDS_12_Tfartefatos_descricao ;
      private String A1751Artefatos_Descricao ;
      private String AV16Option ;
      private IGxSession AV25Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00SH2_A1751Artefatos_Descricao ;
      private int[] P00SH2_A1749Artefatos_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV27GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV28GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV29GridStateDynamicFilter ;
   }

   public class getwwartefatosfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00SH2( IGxContext context ,
                                             String AV45WWArtefatosDS_1_Dynamicfiltersselector1 ,
                                             short AV46WWArtefatosDS_2_Dynamicfiltersoperator1 ,
                                             String AV47WWArtefatosDS_3_Artefatos_descricao1 ,
                                             bool AV48WWArtefatosDS_4_Dynamicfiltersenabled2 ,
                                             String AV49WWArtefatosDS_5_Dynamicfiltersselector2 ,
                                             short AV50WWArtefatosDS_6_Dynamicfiltersoperator2 ,
                                             String AV51WWArtefatosDS_7_Artefatos_descricao2 ,
                                             bool AV52WWArtefatosDS_8_Dynamicfiltersenabled3 ,
                                             String AV53WWArtefatosDS_9_Dynamicfiltersselector3 ,
                                             short AV54WWArtefatosDS_10_Dynamicfiltersoperator3 ,
                                             String AV55WWArtefatosDS_11_Artefatos_descricao3 ,
                                             String AV57WWArtefatosDS_13_Tfartefatos_descricao_sel ,
                                             String AV56WWArtefatosDS_12_Tfartefatos_descricao ,
                                             String A1751Artefatos_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [8] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Artefatos_Descricao], [Artefatos_Codigo] FROM [Artefatos] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV45WWArtefatosDS_1_Dynamicfiltersselector1, "ARTEFATOS_DESCRICAO") == 0 ) && ( AV46WWArtefatosDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWArtefatosDS_3_Artefatos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Artefatos_Descricao] like @lV47WWArtefatosDS_3_Artefatos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Artefatos_Descricao] like @lV47WWArtefatosDS_3_Artefatos_descricao1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45WWArtefatosDS_1_Dynamicfiltersselector1, "ARTEFATOS_DESCRICAO") == 0 ) && ( AV46WWArtefatosDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWArtefatosDS_3_Artefatos_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Artefatos_Descricao] like '%' + @lV47WWArtefatosDS_3_Artefatos_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Artefatos_Descricao] like '%' + @lV47WWArtefatosDS_3_Artefatos_descricao1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV48WWArtefatosDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV49WWArtefatosDS_5_Dynamicfiltersselector2, "ARTEFATOS_DESCRICAO") == 0 ) && ( AV50WWArtefatosDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWArtefatosDS_7_Artefatos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Artefatos_Descricao] like @lV51WWArtefatosDS_7_Artefatos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Artefatos_Descricao] like @lV51WWArtefatosDS_7_Artefatos_descricao2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV48WWArtefatosDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV49WWArtefatosDS_5_Dynamicfiltersselector2, "ARTEFATOS_DESCRICAO") == 0 ) && ( AV50WWArtefatosDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWArtefatosDS_7_Artefatos_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Artefatos_Descricao] like '%' + @lV51WWArtefatosDS_7_Artefatos_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Artefatos_Descricao] like '%' + @lV51WWArtefatosDS_7_Artefatos_descricao2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV52WWArtefatosDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV53WWArtefatosDS_9_Dynamicfiltersselector3, "ARTEFATOS_DESCRICAO") == 0 ) && ( AV54WWArtefatosDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWArtefatosDS_11_Artefatos_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Artefatos_Descricao] like @lV55WWArtefatosDS_11_Artefatos_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Artefatos_Descricao] like @lV55WWArtefatosDS_11_Artefatos_descricao3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV52WWArtefatosDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV53WWArtefatosDS_9_Dynamicfiltersselector3, "ARTEFATOS_DESCRICAO") == 0 ) && ( AV54WWArtefatosDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWArtefatosDS_11_Artefatos_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Artefatos_Descricao] like '%' + @lV55WWArtefatosDS_11_Artefatos_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Artefatos_Descricao] like '%' + @lV55WWArtefatosDS_11_Artefatos_descricao3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57WWArtefatosDS_13_Tfartefatos_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWArtefatosDS_12_Tfartefatos_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Artefatos_Descricao] like @lV56WWArtefatosDS_12_Tfartefatos_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([Artefatos_Descricao] like @lV56WWArtefatosDS_12_Tfartefatos_descricao)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWArtefatosDS_13_Tfartefatos_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Artefatos_Descricao] = @AV57WWArtefatosDS_13_Tfartefatos_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Artefatos_Descricao] = @AV57WWArtefatosDS_13_Tfartefatos_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Artefatos_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00SH2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00SH2 ;
          prmP00SH2 = new Object[] {
          new Object[] {"@lV47WWArtefatosDS_3_Artefatos_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV47WWArtefatosDS_3_Artefatos_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV51WWArtefatosDS_7_Artefatos_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV51WWArtefatosDS_7_Artefatos_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55WWArtefatosDS_11_Artefatos_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55WWArtefatosDS_11_Artefatos_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV56WWArtefatosDS_12_Tfartefatos_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV57WWArtefatosDS_13_Tfartefatos_descricao_sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00SH2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SH2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwartefatosfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwartefatosfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwartefatosfilterdata") )
          {
             return  ;
          }
          getwwartefatosfilterdata worker = new getwwartefatosfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
