/*
               File: FuncaoDadosTabela_BC
        Description: Tabelas da Fun��o de Dados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:20:13.94
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcaodadostabela_bc : GXHttpHandler, IGxSilentTrn
   {
      public funcaodadostabela_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public funcaodadostabela_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow1M60( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey1M60( ) ;
         standaloneModal( ) ;
         AddRow1M60( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z368FuncaoDados_Codigo = A368FuncaoDados_Codigo;
               Z172Tabela_Codigo = A172Tabela_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_1M0( )
      {
         BeforeValidate1M60( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1M60( ) ;
            }
            else
            {
               CheckExtendedTable1M60( ) ;
               if ( AnyError == 0 )
               {
                  ZM1M60( 2) ;
                  ZM1M60( 3) ;
               }
               CloseExtendedTableCursors1M60( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM1M60( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z369FuncaoDados_Nome = A369FuncaoDados_Nome;
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z173Tabela_Nome = A173Tabela_Nome;
         }
         if ( GX_JID == -1 )
         {
            Z368FuncaoDados_Codigo = A368FuncaoDados_Codigo;
            Z172Tabela_Codigo = A172Tabela_Codigo;
            Z369FuncaoDados_Nome = A369FuncaoDados_Nome;
            Z173Tabela_Nome = A173Tabela_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load1M60( )
      {
         /* Using cursor BC001M6 */
         pr_default.execute(4, new Object[] {A368FuncaoDados_Codigo, A172Tabela_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound60 = 1;
            A369FuncaoDados_Nome = BC001M6_A369FuncaoDados_Nome[0];
            A173Tabela_Nome = BC001M6_A173Tabela_Nome[0];
            ZM1M60( -1) ;
         }
         pr_default.close(4);
         OnLoadActions1M60( ) ;
      }

      protected void OnLoadActions1M60( )
      {
      }

      protected void CheckExtendedTable1M60( )
      {
         standaloneModal( ) ;
         /* Using cursor BC001M4 */
         pr_default.execute(2, new Object[] {A368FuncaoDados_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Grupo L�gico de Dados'.", "ForeignKeyNotFound", 1, "FUNCAODADOS_CODIGO");
            AnyError = 1;
         }
         A369FuncaoDados_Nome = BC001M4_A369FuncaoDados_Nome[0];
         pr_default.close(2);
         /* Using cursor BC001M5 */
         pr_default.execute(3, new Object[] {A172Tabela_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "TABELA_CODIGO");
            AnyError = 1;
         }
         A173Tabela_Nome = BC001M5_A173Tabela_Nome[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors1M60( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1M60( )
      {
         /* Using cursor BC001M7 */
         pr_default.execute(5, new Object[] {A368FuncaoDados_Codigo, A172Tabela_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound60 = 1;
         }
         else
         {
            RcdFound60 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC001M3 */
         pr_default.execute(1, new Object[] {A368FuncaoDados_Codigo, A172Tabela_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1M60( 1) ;
            RcdFound60 = 1;
            A368FuncaoDados_Codigo = BC001M3_A368FuncaoDados_Codigo[0];
            A172Tabela_Codigo = BC001M3_A172Tabela_Codigo[0];
            Z368FuncaoDados_Codigo = A368FuncaoDados_Codigo;
            Z172Tabela_Codigo = A172Tabela_Codigo;
            sMode60 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load1M60( ) ;
            if ( AnyError == 1 )
            {
               RcdFound60 = 0;
               InitializeNonKey1M60( ) ;
            }
            Gx_mode = sMode60;
         }
         else
         {
            RcdFound60 = 0;
            InitializeNonKey1M60( ) ;
            sMode60 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode60;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1M60( ) ;
         if ( RcdFound60 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_1M0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency1M60( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC001M2 */
            pr_default.execute(0, new Object[] {A368FuncaoDados_Codigo, A172Tabela_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FuncaoDadosTabela"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"FuncaoDadosTabela"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1M60( )
      {
         BeforeValidate1M60( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1M60( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1M60( 0) ;
            CheckOptimisticConcurrency1M60( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1M60( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1M60( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001M8 */
                     pr_default.execute(6, new Object[] {A368FuncaoDados_Codigo, A172Tabela_Codigo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncaoDadosTabela") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1M60( ) ;
            }
            EndLevel1M60( ) ;
         }
         CloseExtendedTableCursors1M60( ) ;
      }

      protected void Update1M60( )
      {
         BeforeValidate1M60( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1M60( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1M60( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1M60( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1M60( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [FuncaoDadosTabela] */
                     DeferredUpdate1M60( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1M60( ) ;
         }
         CloseExtendedTableCursors1M60( ) ;
      }

      protected void DeferredUpdate1M60( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate1M60( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1M60( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1M60( ) ;
            AfterConfirm1M60( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1M60( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC001M9 */
                  pr_default.execute(7, new Object[] {A368FuncaoDados_Codigo, A172Tabela_Codigo});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("FuncaoDadosTabela") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode60 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel1M60( ) ;
         Gx_mode = sMode60;
      }

      protected void OnDeleteControls1M60( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC001M10 */
            pr_default.execute(8, new Object[] {A368FuncaoDados_Codigo});
            A369FuncaoDados_Nome = BC001M10_A369FuncaoDados_Nome[0];
            pr_default.close(8);
            /* Using cursor BC001M11 */
            pr_default.execute(9, new Object[] {A172Tabela_Codigo});
            A173Tabela_Nome = BC001M11_A173Tabela_Nome[0];
            pr_default.close(9);
         }
      }

      protected void EndLevel1M60( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1M60( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart1M60( )
      {
         /* Using cursor BC001M12 */
         pr_default.execute(10, new Object[] {A368FuncaoDados_Codigo, A172Tabela_Codigo});
         RcdFound60 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound60 = 1;
            A369FuncaoDados_Nome = BC001M12_A369FuncaoDados_Nome[0];
            A173Tabela_Nome = BC001M12_A173Tabela_Nome[0];
            A368FuncaoDados_Codigo = BC001M12_A368FuncaoDados_Codigo[0];
            A172Tabela_Codigo = BC001M12_A172Tabela_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext1M60( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound60 = 0;
         ScanKeyLoad1M60( ) ;
      }

      protected void ScanKeyLoad1M60( )
      {
         sMode60 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound60 = 1;
            A369FuncaoDados_Nome = BC001M12_A369FuncaoDados_Nome[0];
            A173Tabela_Nome = BC001M12_A173Tabela_Nome[0];
            A368FuncaoDados_Codigo = BC001M12_A368FuncaoDados_Codigo[0];
            A172Tabela_Codigo = BC001M12_A172Tabela_Codigo[0];
         }
         Gx_mode = sMode60;
      }

      protected void ScanKeyEnd1M60( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm1M60( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1M60( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1M60( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1M60( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1M60( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1M60( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1M60( )
      {
      }

      protected void AddRow1M60( )
      {
         VarsToRow60( bcFuncaoDadosTabela) ;
      }

      protected void ReadRow1M60( )
      {
         RowToVars60( bcFuncaoDadosTabela, 1) ;
      }

      protected void InitializeNonKey1M60( )
      {
         A369FuncaoDados_Nome = "";
         A173Tabela_Nome = "";
      }

      protected void InitAll1M60( )
      {
         A368FuncaoDados_Codigo = 0;
         A172Tabela_Codigo = 0;
         InitializeNonKey1M60( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow60( SdtFuncaoDadosTabela obj60 )
      {
         obj60.gxTpr_Mode = Gx_mode;
         obj60.gxTpr_Funcaodados_nome = A369FuncaoDados_Nome;
         obj60.gxTpr_Tabela_nome = A173Tabela_Nome;
         obj60.gxTpr_Funcaodados_codigo = A368FuncaoDados_Codigo;
         obj60.gxTpr_Tabela_codigo = A172Tabela_Codigo;
         obj60.gxTpr_Funcaodados_codigo_Z = Z368FuncaoDados_Codigo;
         obj60.gxTpr_Funcaodados_nome_Z = Z369FuncaoDados_Nome;
         obj60.gxTpr_Tabela_codigo_Z = Z172Tabela_Codigo;
         obj60.gxTpr_Tabela_nome_Z = Z173Tabela_Nome;
         obj60.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow60( SdtFuncaoDadosTabela obj60 )
      {
         obj60.gxTpr_Funcaodados_codigo = A368FuncaoDados_Codigo;
         obj60.gxTpr_Tabela_codigo = A172Tabela_Codigo;
         return  ;
      }

      public void RowToVars60( SdtFuncaoDadosTabela obj60 ,
                               int forceLoad )
      {
         Gx_mode = obj60.gxTpr_Mode;
         A369FuncaoDados_Nome = obj60.gxTpr_Funcaodados_nome;
         A173Tabela_Nome = obj60.gxTpr_Tabela_nome;
         A368FuncaoDados_Codigo = obj60.gxTpr_Funcaodados_codigo;
         A172Tabela_Codigo = obj60.gxTpr_Tabela_codigo;
         Z368FuncaoDados_Codigo = obj60.gxTpr_Funcaodados_codigo_Z;
         Z369FuncaoDados_Nome = obj60.gxTpr_Funcaodados_nome_Z;
         Z172Tabela_Codigo = obj60.gxTpr_Tabela_codigo_Z;
         Z173Tabela_Nome = obj60.gxTpr_Tabela_nome_Z;
         Gx_mode = obj60.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A368FuncaoDados_Codigo = (int)getParm(obj,0);
         A172Tabela_Codigo = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey1M60( ) ;
         ScanKeyStart1M60( ) ;
         if ( RcdFound60 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001M10 */
            pr_default.execute(8, new Object[] {A368FuncaoDados_Codigo});
            if ( (pr_default.getStatus(8) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Grupo L�gico de Dados'.", "ForeignKeyNotFound", 1, "FUNCAODADOS_CODIGO");
               AnyError = 1;
            }
            A369FuncaoDados_Nome = BC001M10_A369FuncaoDados_Nome[0];
            pr_default.close(8);
            /* Using cursor BC001M11 */
            pr_default.execute(9, new Object[] {A172Tabela_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "TABELA_CODIGO");
               AnyError = 1;
            }
            A173Tabela_Nome = BC001M11_A173Tabela_Nome[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z368FuncaoDados_Codigo = A368FuncaoDados_Codigo;
            Z172Tabela_Codigo = A172Tabela_Codigo;
         }
         ZM1M60( -1) ;
         OnLoadActions1M60( ) ;
         AddRow1M60( ) ;
         ScanKeyEnd1M60( ) ;
         if ( RcdFound60 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars60( bcFuncaoDadosTabela, 0) ;
         ScanKeyStart1M60( ) ;
         if ( RcdFound60 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001M10 */
            pr_default.execute(8, new Object[] {A368FuncaoDados_Codigo});
            if ( (pr_default.getStatus(8) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Grupo L�gico de Dados'.", "ForeignKeyNotFound", 1, "FUNCAODADOS_CODIGO");
               AnyError = 1;
            }
            A369FuncaoDados_Nome = BC001M10_A369FuncaoDados_Nome[0];
            pr_default.close(8);
            /* Using cursor BC001M11 */
            pr_default.execute(9, new Object[] {A172Tabela_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "TABELA_CODIGO");
               AnyError = 1;
            }
            A173Tabela_Nome = BC001M11_A173Tabela_Nome[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z368FuncaoDados_Codigo = A368FuncaoDados_Codigo;
            Z172Tabela_Codigo = A172Tabela_Codigo;
         }
         ZM1M60( -1) ;
         OnLoadActions1M60( ) ;
         AddRow1M60( ) ;
         ScanKeyEnd1M60( ) ;
         if ( RcdFound60 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars60( bcFuncaoDadosTabela, 0) ;
         nKeyPressed = 1;
         GetKey1M60( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert1M60( ) ;
         }
         else
         {
            if ( RcdFound60 == 1 )
            {
               if ( ( A368FuncaoDados_Codigo != Z368FuncaoDados_Codigo ) || ( A172Tabela_Codigo != Z172Tabela_Codigo ) )
               {
                  A368FuncaoDados_Codigo = Z368FuncaoDados_Codigo;
                  A172Tabela_Codigo = Z172Tabela_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update1M60( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A368FuncaoDados_Codigo != Z368FuncaoDados_Codigo ) || ( A172Tabela_Codigo != Z172Tabela_Codigo ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1M60( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1M60( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow60( bcFuncaoDadosTabela) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars60( bcFuncaoDadosTabela, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey1M60( ) ;
         if ( RcdFound60 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A368FuncaoDados_Codigo != Z368FuncaoDados_Codigo ) || ( A172Tabela_Codigo != Z172Tabela_Codigo ) )
            {
               A368FuncaoDados_Codigo = Z368FuncaoDados_Codigo;
               A172Tabela_Codigo = Z172Tabela_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A368FuncaoDados_Codigo != Z368FuncaoDados_Codigo ) || ( A172Tabela_Codigo != Z172Tabela_Codigo ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(8);
         pr_default.close(9);
         context.RollbackDataStores( "FuncaoDadosTabela_BC");
         VarsToRow60( bcFuncaoDadosTabela) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcFuncaoDadosTabela.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcFuncaoDadosTabela.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcFuncaoDadosTabela )
         {
            bcFuncaoDadosTabela = (SdtFuncaoDadosTabela)(sdt);
            if ( StringUtil.StrCmp(bcFuncaoDadosTabela.gxTpr_Mode, "") == 0 )
            {
               bcFuncaoDadosTabela.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow60( bcFuncaoDadosTabela) ;
            }
            else
            {
               RowToVars60( bcFuncaoDadosTabela, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcFuncaoDadosTabela.gxTpr_Mode, "") == 0 )
            {
               bcFuncaoDadosTabela.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars60( bcFuncaoDadosTabela, 1) ;
         return  ;
      }

      public SdtFuncaoDadosTabela FuncaoDadosTabela_BC
      {
         get {
            return bcFuncaoDadosTabela ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(8);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z369FuncaoDados_Nome = "";
         A369FuncaoDados_Nome = "";
         Z173Tabela_Nome = "";
         A173Tabela_Nome = "";
         BC001M6_A369FuncaoDados_Nome = new String[] {""} ;
         BC001M6_A173Tabela_Nome = new String[] {""} ;
         BC001M6_A368FuncaoDados_Codigo = new int[1] ;
         BC001M6_A172Tabela_Codigo = new int[1] ;
         BC001M4_A369FuncaoDados_Nome = new String[] {""} ;
         BC001M5_A173Tabela_Nome = new String[] {""} ;
         BC001M7_A368FuncaoDados_Codigo = new int[1] ;
         BC001M7_A172Tabela_Codigo = new int[1] ;
         BC001M3_A368FuncaoDados_Codigo = new int[1] ;
         BC001M3_A172Tabela_Codigo = new int[1] ;
         sMode60 = "";
         BC001M2_A368FuncaoDados_Codigo = new int[1] ;
         BC001M2_A172Tabela_Codigo = new int[1] ;
         BC001M10_A369FuncaoDados_Nome = new String[] {""} ;
         BC001M11_A173Tabela_Nome = new String[] {""} ;
         BC001M12_A369FuncaoDados_Nome = new String[] {""} ;
         BC001M12_A173Tabela_Nome = new String[] {""} ;
         BC001M12_A368FuncaoDados_Codigo = new int[1] ;
         BC001M12_A172Tabela_Codigo = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcaodadostabela_bc__default(),
            new Object[][] {
                new Object[] {
               BC001M2_A368FuncaoDados_Codigo, BC001M2_A172Tabela_Codigo
               }
               , new Object[] {
               BC001M3_A368FuncaoDados_Codigo, BC001M3_A172Tabela_Codigo
               }
               , new Object[] {
               BC001M4_A369FuncaoDados_Nome
               }
               , new Object[] {
               BC001M5_A173Tabela_Nome
               }
               , new Object[] {
               BC001M6_A369FuncaoDados_Nome, BC001M6_A173Tabela_Nome, BC001M6_A368FuncaoDados_Codigo, BC001M6_A172Tabela_Codigo
               }
               , new Object[] {
               BC001M7_A368FuncaoDados_Codigo, BC001M7_A172Tabela_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC001M10_A369FuncaoDados_Nome
               }
               , new Object[] {
               BC001M11_A173Tabela_Nome
               }
               , new Object[] {
               BC001M12_A369FuncaoDados_Nome, BC001M12_A173Tabela_Nome, BC001M12_A368FuncaoDados_Codigo, BC001M12_A172Tabela_Codigo
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound60 ;
      private int trnEnded ;
      private int Z368FuncaoDados_Codigo ;
      private int A368FuncaoDados_Codigo ;
      private int Z172Tabela_Codigo ;
      private int A172Tabela_Codigo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z173Tabela_Nome ;
      private String A173Tabela_Nome ;
      private String sMode60 ;
      private String Z369FuncaoDados_Nome ;
      private String A369FuncaoDados_Nome ;
      private SdtFuncaoDadosTabela bcFuncaoDadosTabela ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC001M6_A369FuncaoDados_Nome ;
      private String[] BC001M6_A173Tabela_Nome ;
      private int[] BC001M6_A368FuncaoDados_Codigo ;
      private int[] BC001M6_A172Tabela_Codigo ;
      private String[] BC001M4_A369FuncaoDados_Nome ;
      private String[] BC001M5_A173Tabela_Nome ;
      private int[] BC001M7_A368FuncaoDados_Codigo ;
      private int[] BC001M7_A172Tabela_Codigo ;
      private int[] BC001M3_A368FuncaoDados_Codigo ;
      private int[] BC001M3_A172Tabela_Codigo ;
      private int[] BC001M2_A368FuncaoDados_Codigo ;
      private int[] BC001M2_A172Tabela_Codigo ;
      private String[] BC001M10_A369FuncaoDados_Nome ;
      private String[] BC001M11_A173Tabela_Nome ;
      private String[] BC001M12_A369FuncaoDados_Nome ;
      private String[] BC001M12_A173Tabela_Nome ;
      private int[] BC001M12_A368FuncaoDados_Codigo ;
      private int[] BC001M12_A172Tabela_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class funcaodadostabela_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC001M6 ;
          prmBC001M6 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001M4 ;
          prmBC001M4 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001M5 ;
          prmBC001M5 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001M7 ;
          prmBC001M7 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001M3 ;
          prmBC001M3 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001M2 ;
          prmBC001M2 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001M8 ;
          prmBC001M8 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001M9 ;
          prmBC001M9 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001M12 ;
          prmBC001M12 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001M10 ;
          prmBC001M10 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001M11 ;
          prmBC001M11 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC001M2", "SELECT [FuncaoDados_Codigo], [Tabela_Codigo] FROM [FuncaoDadosTabela] WITH (UPDLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo AND [Tabela_Codigo] = @Tabela_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001M2,1,0,true,false )
             ,new CursorDef("BC001M3", "SELECT [FuncaoDados_Codigo], [Tabela_Codigo] FROM [FuncaoDadosTabela] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo AND [Tabela_Codigo] = @Tabela_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001M3,1,0,true,false )
             ,new CursorDef("BC001M4", "SELECT [FuncaoDados_Nome] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001M4,1,0,true,false )
             ,new CursorDef("BC001M5", "SELECT [Tabela_Nome] FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001M5,1,0,true,false )
             ,new CursorDef("BC001M6", "SELECT T2.[FuncaoDados_Nome], T3.[Tabela_Nome], TM1.[FuncaoDados_Codigo], TM1.[Tabela_Codigo] FROM (([FuncaoDadosTabela] TM1 WITH (NOLOCK) INNER JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = TM1.[FuncaoDados_Codigo]) INNER JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = TM1.[Tabela_Codigo]) WHERE TM1.[FuncaoDados_Codigo] = @FuncaoDados_Codigo and TM1.[Tabela_Codigo] = @Tabela_Codigo ORDER BY TM1.[FuncaoDados_Codigo], TM1.[Tabela_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001M6,100,0,true,false )
             ,new CursorDef("BC001M7", "SELECT [FuncaoDados_Codigo], [Tabela_Codigo] FROM [FuncaoDadosTabela] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo AND [Tabela_Codigo] = @Tabela_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001M7,1,0,true,false )
             ,new CursorDef("BC001M8", "INSERT INTO [FuncaoDadosTabela]([FuncaoDados_Codigo], [Tabela_Codigo]) VALUES(@FuncaoDados_Codigo, @Tabela_Codigo)", GxErrorMask.GX_NOMASK,prmBC001M8)
             ,new CursorDef("BC001M9", "DELETE FROM [FuncaoDadosTabela]  WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo AND [Tabela_Codigo] = @Tabela_Codigo", GxErrorMask.GX_NOMASK,prmBC001M9)
             ,new CursorDef("BC001M10", "SELECT [FuncaoDados_Nome] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001M10,1,0,true,false )
             ,new CursorDef("BC001M11", "SELECT [Tabela_Nome] FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001M11,1,0,true,false )
             ,new CursorDef("BC001M12", "SELECT T2.[FuncaoDados_Nome], T3.[Tabela_Nome], TM1.[FuncaoDados_Codigo], TM1.[Tabela_Codigo] FROM (([FuncaoDadosTabela] TM1 WITH (NOLOCK) INNER JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = TM1.[FuncaoDados_Codigo]) INNER JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = TM1.[Tabela_Codigo]) WHERE TM1.[FuncaoDados_Codigo] = @FuncaoDados_Codigo and TM1.[Tabela_Codigo] = @Tabela_Codigo ORDER BY TM1.[FuncaoDados_Codigo], TM1.[Tabela_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001M12,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
