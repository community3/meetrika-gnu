/*
               File: type_SdtServicoResponsavel
        Description: Servico Responsavel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:2.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ServicoResponsavel" )]
   [XmlType(TypeName =  "ServicoResponsavel" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtServicoResponsavel : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtServicoResponsavel( )
      {
         /* Constructor for serialization */
         gxTv_SdtServicoResponsavel_Mode = "";
      }

      public SdtServicoResponsavel( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV1548ServicoResponsavel_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV1548ServicoResponsavel_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ServicoResponsavel_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ServicoResponsavel");
         metadata.Set("BT", "ServicoResponsavel");
         metadata.Set("PK", "[ \"ServicoResponsavel_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"ServicoResponsavel_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"ContratanteUsuario_ContratanteCod\",\"ContratanteUsuario_UsuarioCod\" ],\"FKMap\":[ \"ServicoResponsavel_CteCteCod-ContratanteUsuario_ContratanteCod\",\"ServicoResponsavel_CteUsrCod-ContratanteUsuario_UsuarioCod\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicoresponsavel_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicoresponsavel_srvcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicoresponsavel_ctectecod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicoresponsavel_cteusrcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicoresponsavel_cteareacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicoresponsavel_srvcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicoresponsavel_ctectecod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicoresponsavel_cteusrcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicoresponsavel_cteareacod_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtServicoResponsavel deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtServicoResponsavel)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtServicoResponsavel obj ;
         obj = this;
         obj.gxTpr_Servicoresponsavel_codigo = deserialized.gxTpr_Servicoresponsavel_codigo;
         obj.gxTpr_Servicoresponsavel_srvcod = deserialized.gxTpr_Servicoresponsavel_srvcod;
         obj.gxTpr_Servicoresponsavel_ctectecod = deserialized.gxTpr_Servicoresponsavel_ctectecod;
         obj.gxTpr_Servicoresponsavel_cteusrcod = deserialized.gxTpr_Servicoresponsavel_cteusrcod;
         obj.gxTpr_Servicoresponsavel_cteareacod = deserialized.gxTpr_Servicoresponsavel_cteareacod;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Servicoresponsavel_codigo_Z = deserialized.gxTpr_Servicoresponsavel_codigo_Z;
         obj.gxTpr_Servicoresponsavel_srvcod_Z = deserialized.gxTpr_Servicoresponsavel_srvcod_Z;
         obj.gxTpr_Servicoresponsavel_ctectecod_Z = deserialized.gxTpr_Servicoresponsavel_ctectecod_Z;
         obj.gxTpr_Servicoresponsavel_cteusrcod_Z = deserialized.gxTpr_Servicoresponsavel_cteusrcod_Z;
         obj.gxTpr_Servicoresponsavel_cteareacod_Z = deserialized.gxTpr_Servicoresponsavel_cteareacod_Z;
         obj.gxTpr_Servicoresponsavel_srvcod_N = deserialized.gxTpr_Servicoresponsavel_srvcod_N;
         obj.gxTpr_Servicoresponsavel_ctectecod_N = deserialized.gxTpr_Servicoresponsavel_ctectecod_N;
         obj.gxTpr_Servicoresponsavel_cteusrcod_N = deserialized.gxTpr_Servicoresponsavel_cteusrcod_N;
         obj.gxTpr_Servicoresponsavel_cteareacod_N = deserialized.gxTpr_Servicoresponsavel_cteareacod_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoResponsavel_Codigo") )
               {
                  gxTv_SdtServicoResponsavel_Servicoresponsavel_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoResponsavel_SrvCod") )
               {
                  gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoResponsavel_CteCteCod") )
               {
                  gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoResponsavel_CteUsrCod") )
               {
                  gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoResponsavel_CteAreaCod") )
               {
                  gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtServicoResponsavel_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtServicoResponsavel_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoResponsavel_Codigo_Z") )
               {
                  gxTv_SdtServicoResponsavel_Servicoresponsavel_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoResponsavel_SrvCod_Z") )
               {
                  gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoResponsavel_CteCteCod_Z") )
               {
                  gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoResponsavel_CteUsrCod_Z") )
               {
                  gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoResponsavel_CteAreaCod_Z") )
               {
                  gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoResponsavel_SrvCod_N") )
               {
                  gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoResponsavel_CteCteCod_N") )
               {
                  gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoResponsavel_CteUsrCod_N") )
               {
                  gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoResponsavel_CteAreaCod_N") )
               {
                  gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ServicoResponsavel";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ServicoResponsavel_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoResponsavel_Servicoresponsavel_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ServicoResponsavel_SrvCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ServicoResponsavel_CteCteCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ServicoResponsavel_CteUsrCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ServicoResponsavel_CteAreaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtServicoResponsavel_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoResponsavel_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ServicoResponsavel_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoResponsavel_Servicoresponsavel_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ServicoResponsavel_SrvCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ServicoResponsavel_CteCteCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ServicoResponsavel_CteUsrCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ServicoResponsavel_CteAreaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ServicoResponsavel_SrvCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ServicoResponsavel_CteCteCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ServicoResponsavel_CteUsrCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ServicoResponsavel_CteAreaCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ServicoResponsavel_Codigo", gxTv_SdtServicoResponsavel_Servicoresponsavel_codigo, false);
         AddObjectProperty("ServicoResponsavel_SrvCod", gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod, false);
         AddObjectProperty("ServicoResponsavel_CteCteCod", gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod, false);
         AddObjectProperty("ServicoResponsavel_CteUsrCod", gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod, false);
         AddObjectProperty("ServicoResponsavel_CteAreaCod", gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtServicoResponsavel_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtServicoResponsavel_Initialized, false);
            AddObjectProperty("ServicoResponsavel_Codigo_Z", gxTv_SdtServicoResponsavel_Servicoresponsavel_codigo_Z, false);
            AddObjectProperty("ServicoResponsavel_SrvCod_Z", gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_Z, false);
            AddObjectProperty("ServicoResponsavel_CteCteCod_Z", gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_Z, false);
            AddObjectProperty("ServicoResponsavel_CteUsrCod_Z", gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_Z, false);
            AddObjectProperty("ServicoResponsavel_CteAreaCod_Z", gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_Z, false);
            AddObjectProperty("ServicoResponsavel_SrvCod_N", gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_N, false);
            AddObjectProperty("ServicoResponsavel_CteCteCod_N", gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_N, false);
            AddObjectProperty("ServicoResponsavel_CteUsrCod_N", gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_N, false);
            AddObjectProperty("ServicoResponsavel_CteAreaCod_N", gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ServicoResponsavel_Codigo" )]
      [  XmlElement( ElementName = "ServicoResponsavel_Codigo"   )]
      public int gxTpr_Servicoresponsavel_codigo
      {
         get {
            return gxTv_SdtServicoResponsavel_Servicoresponsavel_codigo ;
         }

         set {
            if ( gxTv_SdtServicoResponsavel_Servicoresponsavel_codigo != value )
            {
               gxTv_SdtServicoResponsavel_Mode = "INS";
               this.gxTv_SdtServicoResponsavel_Servicoresponsavel_codigo_Z_SetNull( );
               this.gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_Z_SetNull( );
               this.gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_Z_SetNull( );
               this.gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_Z_SetNull( );
               this.gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_Z_SetNull( );
            }
            gxTv_SdtServicoResponsavel_Servicoresponsavel_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ServicoResponsavel_SrvCod" )]
      [  XmlElement( ElementName = "ServicoResponsavel_SrvCod"   )]
      public int gxTpr_Servicoresponsavel_srvcod
      {
         get {
            return gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod ;
         }

         set {
            gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_N = 0;
            gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod = (int)(value);
         }

      }

      public void gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_SetNull( )
      {
         gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_N = 1;
         gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod = 0;
         return  ;
      }

      public bool gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoResponsavel_CteCteCod" )]
      [  XmlElement( ElementName = "ServicoResponsavel_CteCteCod"   )]
      public int gxTpr_Servicoresponsavel_ctectecod
      {
         get {
            return gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod ;
         }

         set {
            gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_N = 0;
            gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod = (int)(value);
         }

      }

      public void gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_SetNull( )
      {
         gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_N = 1;
         gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod = 0;
         return  ;
      }

      public bool gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoResponsavel_CteUsrCod" )]
      [  XmlElement( ElementName = "ServicoResponsavel_CteUsrCod"   )]
      public int gxTpr_Servicoresponsavel_cteusrcod
      {
         get {
            return gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod ;
         }

         set {
            gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_N = 0;
            gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod = (int)(value);
         }

      }

      public void gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_SetNull( )
      {
         gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_N = 1;
         gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod = 0;
         return  ;
      }

      public bool gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoResponsavel_CteAreaCod" )]
      [  XmlElement( ElementName = "ServicoResponsavel_CteAreaCod"   )]
      public int gxTpr_Servicoresponsavel_cteareacod
      {
         get {
            return gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod ;
         }

         set {
            gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_N = 0;
            gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod = (int)(value);
         }

      }

      public void gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_SetNull( )
      {
         gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_N = 1;
         gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod = 0;
         return  ;
      }

      public bool gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtServicoResponsavel_Mode ;
         }

         set {
            gxTv_SdtServicoResponsavel_Mode = (String)(value);
         }

      }

      public void gxTv_SdtServicoResponsavel_Mode_SetNull( )
      {
         gxTv_SdtServicoResponsavel_Mode = "";
         return  ;
      }

      public bool gxTv_SdtServicoResponsavel_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtServicoResponsavel_Initialized ;
         }

         set {
            gxTv_SdtServicoResponsavel_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtServicoResponsavel_Initialized_SetNull( )
      {
         gxTv_SdtServicoResponsavel_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtServicoResponsavel_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoResponsavel_Codigo_Z" )]
      [  XmlElement( ElementName = "ServicoResponsavel_Codigo_Z"   )]
      public int gxTpr_Servicoresponsavel_codigo_Z
      {
         get {
            return gxTv_SdtServicoResponsavel_Servicoresponsavel_codigo_Z ;
         }

         set {
            gxTv_SdtServicoResponsavel_Servicoresponsavel_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtServicoResponsavel_Servicoresponsavel_codigo_Z_SetNull( )
      {
         gxTv_SdtServicoResponsavel_Servicoresponsavel_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServicoResponsavel_Servicoresponsavel_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoResponsavel_SrvCod_Z" )]
      [  XmlElement( ElementName = "ServicoResponsavel_SrvCod_Z"   )]
      public int gxTpr_Servicoresponsavel_srvcod_Z
      {
         get {
            return gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_Z ;
         }

         set {
            gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_Z_SetNull( )
      {
         gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoResponsavel_CteCteCod_Z" )]
      [  XmlElement( ElementName = "ServicoResponsavel_CteCteCod_Z"   )]
      public int gxTpr_Servicoresponsavel_ctectecod_Z
      {
         get {
            return gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_Z ;
         }

         set {
            gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_Z = (int)(value);
         }

      }

      public void gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_Z_SetNull( )
      {
         gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoResponsavel_CteUsrCod_Z" )]
      [  XmlElement( ElementName = "ServicoResponsavel_CteUsrCod_Z"   )]
      public int gxTpr_Servicoresponsavel_cteusrcod_Z
      {
         get {
            return gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_Z ;
         }

         set {
            gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_Z_SetNull( )
      {
         gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoResponsavel_CteAreaCod_Z" )]
      [  XmlElement( ElementName = "ServicoResponsavel_CteAreaCod_Z"   )]
      public int gxTpr_Servicoresponsavel_cteareacod_Z
      {
         get {
            return gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_Z ;
         }

         set {
            gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_Z_SetNull( )
      {
         gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoResponsavel_SrvCod_N" )]
      [  XmlElement( ElementName = "ServicoResponsavel_SrvCod_N"   )]
      public short gxTpr_Servicoresponsavel_srvcod_N
      {
         get {
            return gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_N ;
         }

         set {
            gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_N = (short)(value);
         }

      }

      public void gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_N_SetNull( )
      {
         gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoResponsavel_CteCteCod_N" )]
      [  XmlElement( ElementName = "ServicoResponsavel_CteCteCod_N"   )]
      public short gxTpr_Servicoresponsavel_ctectecod_N
      {
         get {
            return gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_N ;
         }

         set {
            gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_N = (short)(value);
         }

      }

      public void gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_N_SetNull( )
      {
         gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_N = 0;
         return  ;
      }

      public bool gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoResponsavel_CteUsrCod_N" )]
      [  XmlElement( ElementName = "ServicoResponsavel_CteUsrCod_N"   )]
      public short gxTpr_Servicoresponsavel_cteusrcod_N
      {
         get {
            return gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_N ;
         }

         set {
            gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_N = (short)(value);
         }

      }

      public void gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_N_SetNull( )
      {
         gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoResponsavel_CteAreaCod_N" )]
      [  XmlElement( ElementName = "ServicoResponsavel_CteAreaCod_N"   )]
      public short gxTpr_Servicoresponsavel_cteareacod_N
      {
         get {
            return gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_N ;
         }

         set {
            gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_N = (short)(value);
         }

      }

      public void gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_N_SetNull( )
      {
         gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_N = 0;
         return  ;
      }

      public bool gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtServicoResponsavel_Mode = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "servicoresponsavel", "GeneXus.Programs.servicoresponsavel_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtServicoResponsavel_Initialized ;
      private short gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_N ;
      private short gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_N ;
      private short gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_N ;
      private short gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtServicoResponsavel_Servicoresponsavel_codigo ;
      private int gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod ;
      private int gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod ;
      private int gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod ;
      private int gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod ;
      private int gxTv_SdtServicoResponsavel_Servicoresponsavel_codigo_Z ;
      private int gxTv_SdtServicoResponsavel_Servicoresponsavel_srvcod_Z ;
      private int gxTv_SdtServicoResponsavel_Servicoresponsavel_ctectecod_Z ;
      private int gxTv_SdtServicoResponsavel_Servicoresponsavel_cteusrcod_Z ;
      private int gxTv_SdtServicoResponsavel_Servicoresponsavel_cteareacod_Z ;
      private String gxTv_SdtServicoResponsavel_Mode ;
      private String sTagName ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ServicoResponsavel", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtServicoResponsavel_RESTInterface : GxGenericCollectionItem<SdtServicoResponsavel>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtServicoResponsavel_RESTInterface( ) : base()
      {
      }

      public SdtServicoResponsavel_RESTInterface( SdtServicoResponsavel psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ServicoResponsavel_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servicoresponsavel_codigo
      {
         get {
            return sdt.gxTpr_Servicoresponsavel_codigo ;
         }

         set {
            sdt.gxTpr_Servicoresponsavel_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ServicoResponsavel_SrvCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servicoresponsavel_srvcod
      {
         get {
            return sdt.gxTpr_Servicoresponsavel_srvcod ;
         }

         set {
            sdt.gxTpr_Servicoresponsavel_srvcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ServicoResponsavel_CteCteCod" , Order = 2 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servicoresponsavel_ctectecod
      {
         get {
            return sdt.gxTpr_Servicoresponsavel_ctectecod ;
         }

         set {
            sdt.gxTpr_Servicoresponsavel_ctectecod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ServicoResponsavel_CteUsrCod" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servicoresponsavel_cteusrcod
      {
         get {
            return sdt.gxTpr_Servicoresponsavel_cteusrcod ;
         }

         set {
            sdt.gxTpr_Servicoresponsavel_cteusrcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ServicoResponsavel_CteAreaCod" , Order = 4 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servicoresponsavel_cteareacod
      {
         get {
            return sdt.gxTpr_Servicoresponsavel_cteareacod ;
         }

         set {
            sdt.gxTpr_Servicoresponsavel_cteareacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtServicoResponsavel sdt
      {
         get {
            return (SdtServicoResponsavel)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtServicoResponsavel() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 16 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
