/*
               File: GetWWAuditFilterData
        Description: Get WWAudit Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:41.34
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwauditfilterdata : GXProcedure
   {
      public getwwauditfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwauditfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
         return AV29OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwauditfilterdata objgetwwauditfilterdata;
         objgetwwauditfilterdata = new getwwauditfilterdata();
         objgetwwauditfilterdata.AV20DDOName = aP0_DDOName;
         objgetwwauditfilterdata.AV18SearchTxt = aP1_SearchTxt;
         objgetwwauditfilterdata.AV19SearchTxtTo = aP2_SearchTxtTo;
         objgetwwauditfilterdata.AV24OptionsJson = "" ;
         objgetwwauditfilterdata.AV27OptionsDescJson = "" ;
         objgetwwauditfilterdata.AV29OptionIndexesJson = "" ;
         objgetwwauditfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwauditfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwauditfilterdata);
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwauditfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV23Options = (IGxCollection)(new GxSimpleCollection());
         AV26OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV28OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_AUDITDESCRIPTION") == 0 )
         {
            /* Execute user subroutine: 'LOADAUDITDESCRIPTIONOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_USUARIO_PESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADUSUARIO_PESSOANOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_AUDITACTION") == 0 )
         {
            /* Execute user subroutine: 'LOADAUDITACTIONOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV24OptionsJson = AV23Options.ToJSonString(false);
         AV27OptionsDescJson = AV26OptionsDesc.ToJSonString(false);
         AV29OptionIndexesJson = AV28OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get("WWAuditGridState"), "") == 0 )
         {
            AV33GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWAuditGridState"), "");
         }
         else
         {
            AV33GridState.FromXml(AV31Session.Get("WWAuditGridState"), "");
         }
         AV43GXV1 = 1;
         while ( AV43GXV1 <= AV33GridState.gxTpr_Filtervalues.Count )
         {
            AV34GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV33GridState.gxTpr_Filtervalues.Item(AV43GXV1));
            if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "AUDITTABLENAME") == 0 )
            {
               AV36AuditTableName = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "AUDITDATE") == 0 )
            {
               AV37AuditDate = context.localUtil.CToT( AV34GridStateFilterValue.gxTpr_Value, 2);
               AV38AuditDate_To = context.localUtil.CToT( AV34GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "AUDITACTION") == 0 )
            {
               AV39AuditAction = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "USUARIO_PESSOANOM") == 0 )
            {
               AV40Usuario_PessoaNom = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFAUDITDATE") == 0 )
            {
               AV10TFAuditDate = context.localUtil.CToT( AV34GridStateFilterValue.gxTpr_Value, 2);
               AV11TFAuditDate_To = context.localUtil.CToT( AV34GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFAUDITDESCRIPTION") == 0 )
            {
               AV12TFAuditDescription = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFAUDITDESCRIPTION_SEL") == 0 )
            {
               AV13TFAuditDescription_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFUSUARIO_PESSOANOM") == 0 )
            {
               AV14TFUsuario_PessoaNom = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFUSUARIO_PESSOANOM_SEL") == 0 )
            {
               AV15TFUsuario_PessoaNom_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFAUDITACTION") == 0 )
            {
               AV16TFAuditAction = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFAUDITACTION_SEL") == 0 )
            {
               AV17TFAuditAction_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            AV43GXV1 = (int)(AV43GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADAUDITDESCRIPTIONOPTIONS' Routine */
         AV12TFAuditDescription = AV18SearchTxt;
         AV13TFAuditDescription_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV37AuditDate ,
                                              AV38AuditDate_To ,
                                              AV39AuditAction ,
                                              AV40Usuario_PessoaNom ,
                                              AV10TFAuditDate ,
                                              AV11TFAuditDate_To ,
                                              AV13TFAuditDescription_Sel ,
                                              AV12TFAuditDescription ,
                                              AV15TFUsuario_PessoaNom_Sel ,
                                              AV14TFUsuario_PessoaNom ,
                                              AV17TFAuditAction_Sel ,
                                              AV16TFAuditAction ,
                                              A1431AuditDate ,
                                              A1435AuditAction ,
                                              A58Usuario_PessoaNom ,
                                              A1434AuditDescription ,
                                              A1432AuditTableName ,
                                              AV36AuditTableName },
                                              new int[] {
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING
                                              }
         });
         lV40Usuario_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV40Usuario_PessoaNom), 100, "%");
         lV12TFAuditDescription = StringUtil.Concat( StringUtil.RTrim( AV12TFAuditDescription), "%", "");
         lV14TFUsuario_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV14TFUsuario_PessoaNom), 100, "%");
         lV16TFAuditAction = StringUtil.PadR( StringUtil.RTrim( AV16TFAuditAction), 3, "%");
         /* Using cursor P00QT2 */
         pr_default.execute(0, new Object[] {AV36AuditTableName, AV37AuditDate, AV38AuditDate_To, AV39AuditAction, lV40Usuario_PessoaNom, AV10TFAuditDate, AV11TFAuditDate_To, lV12TFAuditDescription, AV13TFAuditDescription_Sel, lV14TFUsuario_PessoaNom, AV15TFUsuario_PessoaNom_Sel, lV16TFAuditAction, AV17TFAuditAction_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKQT2 = false;
            A1Usuario_Codigo = P00QT2_A1Usuario_Codigo[0];
            n1Usuario_Codigo = P00QT2_n1Usuario_Codigo[0];
            A57Usuario_PessoaCod = P00QT2_A57Usuario_PessoaCod[0];
            A1432AuditTableName = P00QT2_A1432AuditTableName[0];
            n1432AuditTableName = P00QT2_n1432AuditTableName[0];
            A1434AuditDescription = P00QT2_A1434AuditDescription[0];
            n1434AuditDescription = P00QT2_n1434AuditDescription[0];
            A58Usuario_PessoaNom = P00QT2_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00QT2_n58Usuario_PessoaNom[0];
            A1435AuditAction = P00QT2_A1435AuditAction[0];
            n1435AuditAction = P00QT2_n1435AuditAction[0];
            A1431AuditDate = P00QT2_A1431AuditDate[0];
            n1431AuditDate = P00QT2_n1431AuditDate[0];
            A1430AuditId = P00QT2_A1430AuditId[0];
            A57Usuario_PessoaCod = P00QT2_A57Usuario_PessoaCod[0];
            A58Usuario_PessoaNom = P00QT2_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00QT2_n58Usuario_PessoaNom[0];
            AV30count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00QT2_A1434AuditDescription[0], A1434AuditDescription) == 0 ) )
            {
               BRKQT2 = false;
               A1430AuditId = P00QT2_A1430AuditId[0];
               AV30count = (long)(AV30count+1);
               BRKQT2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1434AuditDescription)) )
            {
               AV22Option = A1434AuditDescription;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQT2 )
            {
               BRKQT2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADUSUARIO_PESSOANOMOPTIONS' Routine */
         AV14TFUsuario_PessoaNom = AV18SearchTxt;
         AV15TFUsuario_PessoaNom_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV37AuditDate ,
                                              AV38AuditDate_To ,
                                              AV39AuditAction ,
                                              AV40Usuario_PessoaNom ,
                                              AV10TFAuditDate ,
                                              AV11TFAuditDate_To ,
                                              AV13TFAuditDescription_Sel ,
                                              AV12TFAuditDescription ,
                                              AV15TFUsuario_PessoaNom_Sel ,
                                              AV14TFUsuario_PessoaNom ,
                                              AV17TFAuditAction_Sel ,
                                              AV16TFAuditAction ,
                                              A1431AuditDate ,
                                              A1435AuditAction ,
                                              A58Usuario_PessoaNom ,
                                              A1434AuditDescription ,
                                              A1432AuditTableName ,
                                              AV36AuditTableName },
                                              new int[] {
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING
                                              }
         });
         lV40Usuario_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV40Usuario_PessoaNom), 100, "%");
         lV12TFAuditDescription = StringUtil.Concat( StringUtil.RTrim( AV12TFAuditDescription), "%", "");
         lV14TFUsuario_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV14TFUsuario_PessoaNom), 100, "%");
         lV16TFAuditAction = StringUtil.PadR( StringUtil.RTrim( AV16TFAuditAction), 3, "%");
         /* Using cursor P00QT3 */
         pr_default.execute(1, new Object[] {AV36AuditTableName, AV37AuditDate, AV38AuditDate_To, AV39AuditAction, lV40Usuario_PessoaNom, AV10TFAuditDate, AV11TFAuditDate_To, lV12TFAuditDescription, AV13TFAuditDescription_Sel, lV14TFUsuario_PessoaNom, AV15TFUsuario_PessoaNom_Sel, lV16TFAuditAction, AV17TFAuditAction_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKQT4 = false;
            A1Usuario_Codigo = P00QT3_A1Usuario_Codigo[0];
            n1Usuario_Codigo = P00QT3_n1Usuario_Codigo[0];
            A57Usuario_PessoaCod = P00QT3_A57Usuario_PessoaCod[0];
            A1432AuditTableName = P00QT3_A1432AuditTableName[0];
            n1432AuditTableName = P00QT3_n1432AuditTableName[0];
            A58Usuario_PessoaNom = P00QT3_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00QT3_n58Usuario_PessoaNom[0];
            A1434AuditDescription = P00QT3_A1434AuditDescription[0];
            n1434AuditDescription = P00QT3_n1434AuditDescription[0];
            A1435AuditAction = P00QT3_A1435AuditAction[0];
            n1435AuditAction = P00QT3_n1435AuditAction[0];
            A1431AuditDate = P00QT3_A1431AuditDate[0];
            n1431AuditDate = P00QT3_n1431AuditDate[0];
            A1430AuditId = P00QT3_A1430AuditId[0];
            A57Usuario_PessoaCod = P00QT3_A57Usuario_PessoaCod[0];
            A58Usuario_PessoaNom = P00QT3_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00QT3_n58Usuario_PessoaNom[0];
            AV30count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00QT3_A58Usuario_PessoaNom[0], A58Usuario_PessoaNom) == 0 ) )
            {
               BRKQT4 = false;
               A1Usuario_Codigo = P00QT3_A1Usuario_Codigo[0];
               n1Usuario_Codigo = P00QT3_n1Usuario_Codigo[0];
               A57Usuario_PessoaCod = P00QT3_A57Usuario_PessoaCod[0];
               A1430AuditId = P00QT3_A1430AuditId[0];
               A57Usuario_PessoaCod = P00QT3_A57Usuario_PessoaCod[0];
               AV30count = (long)(AV30count+1);
               BRKQT4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A58Usuario_PessoaNom)) )
            {
               AV22Option = A58Usuario_PessoaNom;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQT4 )
            {
               BRKQT4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADAUDITACTIONOPTIONS' Routine */
         AV16TFAuditAction = AV18SearchTxt;
         AV17TFAuditAction_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV37AuditDate ,
                                              AV38AuditDate_To ,
                                              AV39AuditAction ,
                                              AV40Usuario_PessoaNom ,
                                              AV10TFAuditDate ,
                                              AV11TFAuditDate_To ,
                                              AV13TFAuditDescription_Sel ,
                                              AV12TFAuditDescription ,
                                              AV15TFUsuario_PessoaNom_Sel ,
                                              AV14TFUsuario_PessoaNom ,
                                              AV17TFAuditAction_Sel ,
                                              AV16TFAuditAction ,
                                              A1431AuditDate ,
                                              A1435AuditAction ,
                                              A58Usuario_PessoaNom ,
                                              A1434AuditDescription ,
                                              A1432AuditTableName ,
                                              AV36AuditTableName },
                                              new int[] {
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING
                                              }
         });
         lV40Usuario_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV40Usuario_PessoaNom), 100, "%");
         lV12TFAuditDescription = StringUtil.Concat( StringUtil.RTrim( AV12TFAuditDescription), "%", "");
         lV14TFUsuario_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV14TFUsuario_PessoaNom), 100, "%");
         lV16TFAuditAction = StringUtil.PadR( StringUtil.RTrim( AV16TFAuditAction), 3, "%");
         /* Using cursor P00QT4 */
         pr_default.execute(2, new Object[] {AV36AuditTableName, AV37AuditDate, AV38AuditDate_To, AV39AuditAction, lV40Usuario_PessoaNom, AV10TFAuditDate, AV11TFAuditDate_To, lV12TFAuditDescription, AV13TFAuditDescription_Sel, lV14TFUsuario_PessoaNom, AV15TFUsuario_PessoaNom_Sel, lV16TFAuditAction, AV17TFAuditAction_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKQT6 = false;
            A1Usuario_Codigo = P00QT4_A1Usuario_Codigo[0];
            n1Usuario_Codigo = P00QT4_n1Usuario_Codigo[0];
            A57Usuario_PessoaCod = P00QT4_A57Usuario_PessoaCod[0];
            A1432AuditTableName = P00QT4_A1432AuditTableName[0];
            n1432AuditTableName = P00QT4_n1432AuditTableName[0];
            A1435AuditAction = P00QT4_A1435AuditAction[0];
            n1435AuditAction = P00QT4_n1435AuditAction[0];
            A1434AuditDescription = P00QT4_A1434AuditDescription[0];
            n1434AuditDescription = P00QT4_n1434AuditDescription[0];
            A58Usuario_PessoaNom = P00QT4_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00QT4_n58Usuario_PessoaNom[0];
            A1431AuditDate = P00QT4_A1431AuditDate[0];
            n1431AuditDate = P00QT4_n1431AuditDate[0];
            A1430AuditId = P00QT4_A1430AuditId[0];
            A57Usuario_PessoaCod = P00QT4_A57Usuario_PessoaCod[0];
            A58Usuario_PessoaNom = P00QT4_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00QT4_n58Usuario_PessoaNom[0];
            AV30count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00QT4_A1435AuditAction[0], A1435AuditAction) == 0 ) )
            {
               BRKQT6 = false;
               A1430AuditId = P00QT4_A1430AuditId[0];
               AV30count = (long)(AV30count+1);
               BRKQT6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1435AuditAction)) )
            {
               AV22Option = A1435AuditAction;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQT6 )
            {
               BRKQT6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV23Options = new GxSimpleCollection();
         AV26OptionsDesc = new GxSimpleCollection();
         AV28OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV31Session = context.GetSession();
         AV33GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV36AuditTableName = "";
         AV37AuditDate = (DateTime)(DateTime.MinValue);
         AV38AuditDate_To = (DateTime)(DateTime.MinValue);
         AV39AuditAction = "";
         AV40Usuario_PessoaNom = "";
         AV10TFAuditDate = (DateTime)(DateTime.MinValue);
         AV11TFAuditDate_To = (DateTime)(DateTime.MinValue);
         AV12TFAuditDescription = "";
         AV13TFAuditDescription_Sel = "";
         AV14TFUsuario_PessoaNom = "";
         AV15TFUsuario_PessoaNom_Sel = "";
         AV16TFAuditAction = "";
         AV17TFAuditAction_Sel = "";
         scmdbuf = "";
         lV40Usuario_PessoaNom = "";
         lV12TFAuditDescription = "";
         lV14TFUsuario_PessoaNom = "";
         lV16TFAuditAction = "";
         A1431AuditDate = (DateTime)(DateTime.MinValue);
         A1435AuditAction = "";
         A58Usuario_PessoaNom = "";
         A1434AuditDescription = "";
         A1432AuditTableName = "";
         P00QT2_A1Usuario_Codigo = new int[1] ;
         P00QT2_n1Usuario_Codigo = new bool[] {false} ;
         P00QT2_A57Usuario_PessoaCod = new int[1] ;
         P00QT2_A1432AuditTableName = new String[] {""} ;
         P00QT2_n1432AuditTableName = new bool[] {false} ;
         P00QT2_A1434AuditDescription = new String[] {""} ;
         P00QT2_n1434AuditDescription = new bool[] {false} ;
         P00QT2_A58Usuario_PessoaNom = new String[] {""} ;
         P00QT2_n58Usuario_PessoaNom = new bool[] {false} ;
         P00QT2_A1435AuditAction = new String[] {""} ;
         P00QT2_n1435AuditAction = new bool[] {false} ;
         P00QT2_A1431AuditDate = new DateTime[] {DateTime.MinValue} ;
         P00QT2_n1431AuditDate = new bool[] {false} ;
         P00QT2_A1430AuditId = new long[1] ;
         AV22Option = "";
         P00QT3_A1Usuario_Codigo = new int[1] ;
         P00QT3_n1Usuario_Codigo = new bool[] {false} ;
         P00QT3_A57Usuario_PessoaCod = new int[1] ;
         P00QT3_A1432AuditTableName = new String[] {""} ;
         P00QT3_n1432AuditTableName = new bool[] {false} ;
         P00QT3_A58Usuario_PessoaNom = new String[] {""} ;
         P00QT3_n58Usuario_PessoaNom = new bool[] {false} ;
         P00QT3_A1434AuditDescription = new String[] {""} ;
         P00QT3_n1434AuditDescription = new bool[] {false} ;
         P00QT3_A1435AuditAction = new String[] {""} ;
         P00QT3_n1435AuditAction = new bool[] {false} ;
         P00QT3_A1431AuditDate = new DateTime[] {DateTime.MinValue} ;
         P00QT3_n1431AuditDate = new bool[] {false} ;
         P00QT3_A1430AuditId = new long[1] ;
         P00QT4_A1Usuario_Codigo = new int[1] ;
         P00QT4_n1Usuario_Codigo = new bool[] {false} ;
         P00QT4_A57Usuario_PessoaCod = new int[1] ;
         P00QT4_A1432AuditTableName = new String[] {""} ;
         P00QT4_n1432AuditTableName = new bool[] {false} ;
         P00QT4_A1435AuditAction = new String[] {""} ;
         P00QT4_n1435AuditAction = new bool[] {false} ;
         P00QT4_A1434AuditDescription = new String[] {""} ;
         P00QT4_n1434AuditDescription = new bool[] {false} ;
         P00QT4_A58Usuario_PessoaNom = new String[] {""} ;
         P00QT4_n58Usuario_PessoaNom = new bool[] {false} ;
         P00QT4_A1431AuditDate = new DateTime[] {DateTime.MinValue} ;
         P00QT4_n1431AuditDate = new bool[] {false} ;
         P00QT4_A1430AuditId = new long[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwauditfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00QT2_A1Usuario_Codigo, P00QT2_n1Usuario_Codigo, P00QT2_A57Usuario_PessoaCod, P00QT2_A1432AuditTableName, P00QT2_n1432AuditTableName, P00QT2_A1434AuditDescription, P00QT2_n1434AuditDescription, P00QT2_A58Usuario_PessoaNom, P00QT2_n58Usuario_PessoaNom, P00QT2_A1435AuditAction,
               P00QT2_n1435AuditAction, P00QT2_A1431AuditDate, P00QT2_n1431AuditDate, P00QT2_A1430AuditId
               }
               , new Object[] {
               P00QT3_A1Usuario_Codigo, P00QT3_n1Usuario_Codigo, P00QT3_A57Usuario_PessoaCod, P00QT3_A1432AuditTableName, P00QT3_n1432AuditTableName, P00QT3_A58Usuario_PessoaNom, P00QT3_n58Usuario_PessoaNom, P00QT3_A1434AuditDescription, P00QT3_n1434AuditDescription, P00QT3_A1435AuditAction,
               P00QT3_n1435AuditAction, P00QT3_A1431AuditDate, P00QT3_n1431AuditDate, P00QT3_A1430AuditId
               }
               , new Object[] {
               P00QT4_A1Usuario_Codigo, P00QT4_n1Usuario_Codigo, P00QT4_A57Usuario_PessoaCod, P00QT4_A1432AuditTableName, P00QT4_n1432AuditTableName, P00QT4_A1435AuditAction, P00QT4_n1435AuditAction, P00QT4_A1434AuditDescription, P00QT4_n1434AuditDescription, P00QT4_A58Usuario_PessoaNom,
               P00QT4_n58Usuario_PessoaNom, P00QT4_A1431AuditDate, P00QT4_n1431AuditDate, P00QT4_A1430AuditId
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV43GXV1 ;
      private int A1Usuario_Codigo ;
      private int A57Usuario_PessoaCod ;
      private long A1430AuditId ;
      private long AV30count ;
      private String AV36AuditTableName ;
      private String AV39AuditAction ;
      private String AV40Usuario_PessoaNom ;
      private String AV14TFUsuario_PessoaNom ;
      private String AV15TFUsuario_PessoaNom_Sel ;
      private String AV16TFAuditAction ;
      private String AV17TFAuditAction_Sel ;
      private String scmdbuf ;
      private String lV40Usuario_PessoaNom ;
      private String lV14TFUsuario_PessoaNom ;
      private String lV16TFAuditAction ;
      private String A1435AuditAction ;
      private String A58Usuario_PessoaNom ;
      private String A1432AuditTableName ;
      private DateTime AV37AuditDate ;
      private DateTime AV38AuditDate_To ;
      private DateTime AV10TFAuditDate ;
      private DateTime AV11TFAuditDate_To ;
      private DateTime A1431AuditDate ;
      private bool returnInSub ;
      private bool BRKQT2 ;
      private bool n1Usuario_Codigo ;
      private bool n1432AuditTableName ;
      private bool n1434AuditDescription ;
      private bool n58Usuario_PessoaNom ;
      private bool n1435AuditAction ;
      private bool n1431AuditDate ;
      private bool BRKQT4 ;
      private bool BRKQT6 ;
      private String AV29OptionIndexesJson ;
      private String AV24OptionsJson ;
      private String AV27OptionsDescJson ;
      private String A1434AuditDescription ;
      private String AV20DDOName ;
      private String AV18SearchTxt ;
      private String AV19SearchTxtTo ;
      private String AV12TFAuditDescription ;
      private String AV13TFAuditDescription_Sel ;
      private String lV12TFAuditDescription ;
      private String AV22Option ;
      private IGxSession AV31Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00QT2_A1Usuario_Codigo ;
      private bool[] P00QT2_n1Usuario_Codigo ;
      private int[] P00QT2_A57Usuario_PessoaCod ;
      private String[] P00QT2_A1432AuditTableName ;
      private bool[] P00QT2_n1432AuditTableName ;
      private String[] P00QT2_A1434AuditDescription ;
      private bool[] P00QT2_n1434AuditDescription ;
      private String[] P00QT2_A58Usuario_PessoaNom ;
      private bool[] P00QT2_n58Usuario_PessoaNom ;
      private String[] P00QT2_A1435AuditAction ;
      private bool[] P00QT2_n1435AuditAction ;
      private DateTime[] P00QT2_A1431AuditDate ;
      private bool[] P00QT2_n1431AuditDate ;
      private long[] P00QT2_A1430AuditId ;
      private int[] P00QT3_A1Usuario_Codigo ;
      private bool[] P00QT3_n1Usuario_Codigo ;
      private int[] P00QT3_A57Usuario_PessoaCod ;
      private String[] P00QT3_A1432AuditTableName ;
      private bool[] P00QT3_n1432AuditTableName ;
      private String[] P00QT3_A58Usuario_PessoaNom ;
      private bool[] P00QT3_n58Usuario_PessoaNom ;
      private String[] P00QT3_A1434AuditDescription ;
      private bool[] P00QT3_n1434AuditDescription ;
      private String[] P00QT3_A1435AuditAction ;
      private bool[] P00QT3_n1435AuditAction ;
      private DateTime[] P00QT3_A1431AuditDate ;
      private bool[] P00QT3_n1431AuditDate ;
      private long[] P00QT3_A1430AuditId ;
      private int[] P00QT4_A1Usuario_Codigo ;
      private bool[] P00QT4_n1Usuario_Codigo ;
      private int[] P00QT4_A57Usuario_PessoaCod ;
      private String[] P00QT4_A1432AuditTableName ;
      private bool[] P00QT4_n1432AuditTableName ;
      private String[] P00QT4_A1435AuditAction ;
      private bool[] P00QT4_n1435AuditAction ;
      private String[] P00QT4_A1434AuditDescription ;
      private bool[] P00QT4_n1434AuditDescription ;
      private String[] P00QT4_A58Usuario_PessoaNom ;
      private bool[] P00QT4_n58Usuario_PessoaNom ;
      private DateTime[] P00QT4_A1431AuditDate ;
      private bool[] P00QT4_n1431AuditDate ;
      private long[] P00QT4_A1430AuditId ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV33GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV34GridStateFilterValue ;
   }

   public class getwwauditfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00QT2( IGxContext context ,
                                             DateTime AV37AuditDate ,
                                             DateTime AV38AuditDate_To ,
                                             String AV39AuditAction ,
                                             String AV40Usuario_PessoaNom ,
                                             DateTime AV10TFAuditDate ,
                                             DateTime AV11TFAuditDate_To ,
                                             String AV13TFAuditDescription_Sel ,
                                             String AV12TFAuditDescription ,
                                             String AV15TFUsuario_PessoaNom_Sel ,
                                             String AV14TFUsuario_PessoaNom ,
                                             String AV17TFAuditAction_Sel ,
                                             String AV16TFAuditAction ,
                                             DateTime A1431AuditDate ,
                                             String A1435AuditAction ,
                                             String A58Usuario_PessoaNom ,
                                             String A1434AuditDescription ,
                                             String A1432AuditTableName ,
                                             String AV36AuditTableName )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [13] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Usuario_Codigo], T2.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[AuditTableName], T1.[AuditDescription], T3.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[AuditAction], T1.[AuditDate], T1.[AuditId] FROM (([Audit] T1 WITH (NOLOCK) LEFT JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Usuario_Codigo]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[AuditTableName] = @AV36AuditTableName)";
         if ( ! (DateTime.MinValue==AV37AuditDate) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] >= @AV37AuditDate)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV38AuditDate_To) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] <= @AV38AuditDate_To)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39AuditAction)) )
         {
            sWhereString = sWhereString + " and (T1.[AuditAction] = @AV39AuditAction)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Usuario_PessoaNom)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV40Usuario_PessoaNom)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV10TFAuditDate) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] >= @AV10TFAuditDate)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFAuditDate_To) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] <= @AV11TFAuditDate_To)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFAuditDescription_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFAuditDescription)) ) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDescription] like @lV12TFAuditDescription)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFAuditDescription_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDescription] = @AV13TFAuditDescription_Sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFUsuario_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFUsuario_PessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV14TFUsuario_PessoaNom)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFUsuario_PessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV15TFUsuario_PessoaNom_Sel)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAuditAction_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFAuditAction)) ) )
         {
            sWhereString = sWhereString + " and (T1.[AuditAction] like @lV16TFAuditAction)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAuditAction_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[AuditAction] = @AV17TFAuditAction_Sel)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[AuditDescription]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00QT3( IGxContext context ,
                                             DateTime AV37AuditDate ,
                                             DateTime AV38AuditDate_To ,
                                             String AV39AuditAction ,
                                             String AV40Usuario_PessoaNom ,
                                             DateTime AV10TFAuditDate ,
                                             DateTime AV11TFAuditDate_To ,
                                             String AV13TFAuditDescription_Sel ,
                                             String AV12TFAuditDescription ,
                                             String AV15TFUsuario_PessoaNom_Sel ,
                                             String AV14TFUsuario_PessoaNom ,
                                             String AV17TFAuditAction_Sel ,
                                             String AV16TFAuditAction ,
                                             DateTime A1431AuditDate ,
                                             String A1435AuditAction ,
                                             String A58Usuario_PessoaNom ,
                                             String A1434AuditDescription ,
                                             String A1432AuditTableName ,
                                             String AV36AuditTableName )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [13] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Usuario_Codigo], T2.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[AuditTableName], T3.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[AuditDescription], T1.[AuditAction], T1.[AuditDate], T1.[AuditId] FROM (([Audit] T1 WITH (NOLOCK) LEFT JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Usuario_Codigo]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[AuditTableName] = @AV36AuditTableName)";
         if ( ! (DateTime.MinValue==AV37AuditDate) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] >= @AV37AuditDate)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV38AuditDate_To) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] <= @AV38AuditDate_To)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39AuditAction)) )
         {
            sWhereString = sWhereString + " and (T1.[AuditAction] = @AV39AuditAction)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Usuario_PessoaNom)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV40Usuario_PessoaNom)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV10TFAuditDate) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] >= @AV10TFAuditDate)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFAuditDate_To) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] <= @AV11TFAuditDate_To)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFAuditDescription_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFAuditDescription)) ) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDescription] like @lV12TFAuditDescription)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFAuditDescription_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDescription] = @AV13TFAuditDescription_Sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFUsuario_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFUsuario_PessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV14TFUsuario_PessoaNom)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFUsuario_PessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV15TFUsuario_PessoaNom_Sel)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAuditAction_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFAuditAction)) ) )
         {
            sWhereString = sWhereString + " and (T1.[AuditAction] like @lV16TFAuditAction)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAuditAction_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[AuditAction] = @AV17TFAuditAction_Sel)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00QT4( IGxContext context ,
                                             DateTime AV37AuditDate ,
                                             DateTime AV38AuditDate_To ,
                                             String AV39AuditAction ,
                                             String AV40Usuario_PessoaNom ,
                                             DateTime AV10TFAuditDate ,
                                             DateTime AV11TFAuditDate_To ,
                                             String AV13TFAuditDescription_Sel ,
                                             String AV12TFAuditDescription ,
                                             String AV15TFUsuario_PessoaNom_Sel ,
                                             String AV14TFUsuario_PessoaNom ,
                                             String AV17TFAuditAction_Sel ,
                                             String AV16TFAuditAction ,
                                             DateTime A1431AuditDate ,
                                             String A1435AuditAction ,
                                             String A58Usuario_PessoaNom ,
                                             String A1434AuditDescription ,
                                             String A1432AuditTableName ,
                                             String AV36AuditTableName )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [13] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Usuario_Codigo], T2.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[AuditTableName], T1.[AuditAction], T1.[AuditDescription], T3.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[AuditDate], T1.[AuditId] FROM (([Audit] T1 WITH (NOLOCK) LEFT JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Usuario_Codigo]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[AuditTableName] = @AV36AuditTableName)";
         if ( ! (DateTime.MinValue==AV37AuditDate) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] >= @AV37AuditDate)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV38AuditDate_To) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] <= @AV38AuditDate_To)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39AuditAction)) )
         {
            sWhereString = sWhereString + " and (T1.[AuditAction] = @AV39AuditAction)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Usuario_PessoaNom)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV40Usuario_PessoaNom)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV10TFAuditDate) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] >= @AV10TFAuditDate)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFAuditDate_To) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDate] <= @AV11TFAuditDate_To)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFAuditDescription_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFAuditDescription)) ) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDescription] like @lV12TFAuditDescription)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFAuditDescription_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[AuditDescription] = @AV13TFAuditDescription_Sel)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFUsuario_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFUsuario_PessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV14TFUsuario_PessoaNom)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFUsuario_PessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV15TFUsuario_PessoaNom_Sel)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAuditAction_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFAuditAction)) ) )
         {
            sWhereString = sWhereString + " and (T1.[AuditAction] like @lV16TFAuditAction)";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFAuditAction_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[AuditAction] = @AV17TFAuditAction_Sel)";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[AuditAction]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00QT2(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] );
               case 1 :
                     return conditional_P00QT3(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] );
               case 2 :
                     return conditional_P00QT4(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00QT2 ;
          prmP00QT2 = new Object[] {
          new Object[] {"@AV36AuditTableName",SqlDbType.Char,50,0} ,
          new Object[] {"@AV37AuditDate",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV38AuditDate_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV39AuditAction",SqlDbType.Char,3,0} ,
          new Object[] {"@lV40Usuario_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV10TFAuditDate",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV11TFAuditDate_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV12TFAuditDescription",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV13TFAuditDescription_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV14TFUsuario_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV15TFUsuario_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV16TFAuditAction",SqlDbType.Char,3,0} ,
          new Object[] {"@AV17TFAuditAction_Sel",SqlDbType.Char,3,0}
          } ;
          Object[] prmP00QT3 ;
          prmP00QT3 = new Object[] {
          new Object[] {"@AV36AuditTableName",SqlDbType.Char,50,0} ,
          new Object[] {"@AV37AuditDate",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV38AuditDate_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV39AuditAction",SqlDbType.Char,3,0} ,
          new Object[] {"@lV40Usuario_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV10TFAuditDate",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV11TFAuditDate_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV12TFAuditDescription",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV13TFAuditDescription_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV14TFUsuario_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV15TFUsuario_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV16TFAuditAction",SqlDbType.Char,3,0} ,
          new Object[] {"@AV17TFAuditAction_Sel",SqlDbType.Char,3,0}
          } ;
          Object[] prmP00QT4 ;
          prmP00QT4 = new Object[] {
          new Object[] {"@AV36AuditTableName",SqlDbType.Char,50,0} ,
          new Object[] {"@AV37AuditDate",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV38AuditDate_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV39AuditAction",SqlDbType.Char,3,0} ,
          new Object[] {"@lV40Usuario_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV10TFAuditDate",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV11TFAuditDate_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV12TFAuditDescription",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV13TFAuditDescription_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV14TFUsuario_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV15TFUsuario_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV16TFAuditAction",SqlDbType.Char,3,0} ,
          new Object[] {"@AV17TFAuditAction_Sel",SqlDbType.Char,3,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00QT2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00QT2,100,0,true,false )
             ,new CursorDef("P00QT3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00QT3,100,0,true,false )
             ,new CursorDef("P00QT4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00QT4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 3) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[11])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((long[]) buf[13])[0] = rslt.getLong(8) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 3) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[11])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((long[]) buf[13])[0] = rslt.getLong(8) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[11])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((long[]) buf[13])[0] = rslt.getLong(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwauditfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwauditfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwauditfilterdata") )
          {
             return  ;
          }
          getwwauditfilterdata worker = new getwwauditfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
