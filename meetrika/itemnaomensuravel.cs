/*
               File: ItemNaoMensuravel
        Description: Item N�o Mensuravel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:22:38.10
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class itemnaomensuravel : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"REFERENCIAINM_CODIGO") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAREFERENCIAINM_CODIGO2D91( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_19") == 0 )
         {
            A718ItemNaoMensuravel_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_19( A718ItemNaoMensuravel_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_18") == 0 )
         {
            A709ReferenciaINM_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_18( A709ReferenciaINM_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV14ItemNaoMensuravel_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vITEMNAOMENSURAVEL_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV14ItemNaoMensuravel_AreaTrabalhoCod), "ZZZZZ9")));
               AV7ItemNaoMensuravel_Codigo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ItemNaoMensuravel_Codigo", AV7ItemNaoMensuravel_Codigo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vITEMNAOMENSURAVEL_CODIGO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7ItemNaoMensuravel_Codigo, "@!"))));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynReferenciaINM_Codigo.Name = "REFERENCIAINM_CODIGO";
         dynReferenciaINM_Codigo.WebTags = "";
         cmbItemNaoMensuravel_Tipo.Name = "ITEMNAOMENSURAVEL_TIPO";
         cmbItemNaoMensuravel_Tipo.WebTags = "";
         cmbItemNaoMensuravel_Tipo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "(Nenhum)", 0);
         cmbItemNaoMensuravel_Tipo.addItem("1", "PC", 0);
         cmbItemNaoMensuravel_Tipo.addItem("2", "PF", 0);
         if ( cmbItemNaoMensuravel_Tipo.ItemCount > 0 )
         {
            A717ItemNaoMensuravel_Tipo = (short)(NumberUtil.Val( cmbItemNaoMensuravel_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A717ItemNaoMensuravel_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0)));
         }
         chkItemNaoMensuravel_Ativo.Name = "ITEMNAOMENSURAVEL_ATIVO";
         chkItemNaoMensuravel_Ativo.WebTags = "";
         chkItemNaoMensuravel_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkItemNaoMensuravel_Ativo_Internalname, "TitleCaption", chkItemNaoMensuravel_Ativo.Caption);
         chkItemNaoMensuravel_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Item N�o Mensuravel", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtItemNaoMensuravel_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public itemnaomensuravel( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public itemnaomensuravel( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ItemNaoMensuravel_AreaTrabalhoCod ,
                           String aP2_ItemNaoMensuravel_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV14ItemNaoMensuravel_AreaTrabalhoCod = aP1_ItemNaoMensuravel_AreaTrabalhoCod;
         this.AV7ItemNaoMensuravel_Codigo = aP2_ItemNaoMensuravel_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynReferenciaINM_Codigo = new GXCombobox();
         cmbItemNaoMensuravel_Tipo = new GXCombobox();
         chkItemNaoMensuravel_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynReferenciaINM_Codigo.ItemCount > 0 )
         {
            A709ReferenciaINM_Codigo = (int)(NumberUtil.Val( dynReferenciaINM_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
         }
         if ( cmbItemNaoMensuravel_Tipo.ItemCount > 0 )
         {
            A717ItemNaoMensuravel_Tipo = (short)(NumberUtil.Val( cmbItemNaoMensuravel_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A717ItemNaoMensuravel_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2D91( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2D91e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtItemNaoMensuravel_AreaTrabalhoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtItemNaoMensuravel_AreaTrabalhoCod_Jsonclick, 0, "Attribute", "", "", "", edtItemNaoMensuravel_AreaTrabalhoCod_Visible, edtItemNaoMensuravel_AreaTrabalhoCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ItemNaoMensuravel.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2D91( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2D91( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2D91e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_53_2D91( true) ;
         }
         return  ;
      }

      protected void wb_table3_53_2D91e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2D91e( true) ;
         }
         else
         {
            wb_table1_2_2D91e( false) ;
         }
      }

      protected void wb_table3_53_2D91( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_53_2D91e( true) ;
         }
         else
         {
            wb_table3_53_2D91e( false) ;
         }
      }

      protected void wb_table2_5_2D91( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2D91( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2D91e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2D91e( true) ;
         }
         else
         {
            wb_table2_5_2D91e( false) ;
         }
      }

      protected void wb_table4_13_2D91( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockitemnaomensuravel_codigo_Internalname, "C�digo", "", "", lblTextblockitemnaomensuravel_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtItemNaoMensuravel_Codigo_Internalname, StringUtil.RTrim( A715ItemNaoMensuravel_Codigo), StringUtil.RTrim( context.localUtil.Format( A715ItemNaoMensuravel_Codigo, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtItemNaoMensuravel_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtItemNaoMensuravel_Codigo_Enabled, 1, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "CodigoINM20", "left", true, "HLP_ItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockitemnaomensuravel_descricao_Internalname, "Descri��o", "", "", lblTextblockitemnaomensuravel_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtItemNaoMensuravel_Descricao_Internalname, A714ItemNaoMensuravel_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", 0, 1, edtItemNaoMensuravel_Descricao_Enabled, 0, 120, "chr", 12, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "DescricaoLonga2M", "HLP_ItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockitemnaomensuravel_valor_Internalname, "Valor", "", "", lblTextblockitemnaomensuravel_valor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtItemNaoMensuravel_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A719ItemNaoMensuravel_Valor, 18, 5, ",", "")), ((edtItemNaoMensuravel_Valor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A719ItemNaoMensuravel_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A719ItemNaoMensuravel_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtItemNaoMensuravel_Valor_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtItemNaoMensuravel_Valor_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockitemnaomensuravel_referencia_Internalname, "Refer�ncia", "", "", lblTextblockitemnaomensuravel_referencia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtItemNaoMensuravel_Referencia_Internalname, StringUtil.RTrim( A1804ItemNaoMensuravel_Referencia), StringUtil.RTrim( context.localUtil.Format( A1804ItemNaoMensuravel_Referencia, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtItemNaoMensuravel_Referencia_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtItemNaoMensuravel_Referencia_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockreferenciainm_codigo_Internalname, "Guia", "", "", lblTextblockreferenciainm_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynReferenciaINM_Codigo, dynReferenciaINM_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)), 1, dynReferenciaINM_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynReferenciaINM_Codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_ItemNaoMensuravel.htm");
            dynReferenciaINM_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynReferenciaINM_Codigo_Internalname, "Values", (String)(dynReferenciaINM_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockitemnaomensuravel_tipo_Internalname, "Tipo do Item", "", "", lblTextblockitemnaomensuravel_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbItemNaoMensuravel_Tipo, cmbItemNaoMensuravel_Tipo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0)), 1, cmbItemNaoMensuravel_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbItemNaoMensuravel_Tipo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_ItemNaoMensuravel.htm");
            cmbItemNaoMensuravel_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbItemNaoMensuravel_Tipo_Internalname, "Values", (String)(cmbItemNaoMensuravel_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockitemnaomensuravel_ativo_Internalname, "Ativo", "", "", lblTextblockitemnaomensuravel_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockitemnaomensuravel_ativo_Visible, 1, 0, "HLP_ItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkItemNaoMensuravel_Ativo_Internalname, StringUtil.BoolToStr( A716ItemNaoMensuravel_Ativo), "", "", chkItemNaoMensuravel_Ativo.Visible, chkItemNaoMensuravel_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(48, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2D91e( true) ;
         }
         else
         {
            wb_table4_13_2D91e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112D2 */
         E112D2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A715ItemNaoMensuravel_Codigo = StringUtil.Upper( cgiGet( edtItemNaoMensuravel_Codigo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
               A714ItemNaoMensuravel_Descricao = cgiGet( edtItemNaoMensuravel_Descricao_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A714ItemNaoMensuravel_Descricao", A714ItemNaoMensuravel_Descricao);
               if ( ( ( context.localUtil.CToN( cgiGet( edtItemNaoMensuravel_Valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtItemNaoMensuravel_Valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "ITEMNAOMENSURAVEL_VALOR");
                  AnyError = 1;
                  GX_FocusControl = edtItemNaoMensuravel_Valor_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A719ItemNaoMensuravel_Valor = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A719ItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( A719ItemNaoMensuravel_Valor, 18, 5)));
               }
               else
               {
                  A719ItemNaoMensuravel_Valor = context.localUtil.CToN( cgiGet( edtItemNaoMensuravel_Valor_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A719ItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( A719ItemNaoMensuravel_Valor, 18, 5)));
               }
               A1804ItemNaoMensuravel_Referencia = cgiGet( edtItemNaoMensuravel_Referencia_Internalname);
               n1804ItemNaoMensuravel_Referencia = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1804ItemNaoMensuravel_Referencia", A1804ItemNaoMensuravel_Referencia);
               n1804ItemNaoMensuravel_Referencia = (String.IsNullOrEmpty(StringUtil.RTrim( A1804ItemNaoMensuravel_Referencia)) ? true : false);
               dynReferenciaINM_Codigo.CurrentValue = cgiGet( dynReferenciaINM_Codigo_Internalname);
               A709ReferenciaINM_Codigo = (int)(NumberUtil.Val( cgiGet( dynReferenciaINM_Codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
               cmbItemNaoMensuravel_Tipo.CurrentValue = cgiGet( cmbItemNaoMensuravel_Tipo_Internalname);
               A717ItemNaoMensuravel_Tipo = (short)(NumberUtil.Val( cgiGet( cmbItemNaoMensuravel_Tipo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A717ItemNaoMensuravel_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0)));
               A716ItemNaoMensuravel_Ativo = StringUtil.StrToBool( cgiGet( chkItemNaoMensuravel_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A716ItemNaoMensuravel_Ativo", A716ItemNaoMensuravel_Ativo);
               if ( ( ( context.localUtil.CToN( cgiGet( edtItemNaoMensuravel_AreaTrabalhoCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtItemNaoMensuravel_AreaTrabalhoCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "ITEMNAOMENSURAVEL_AREATRABALHOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtItemNaoMensuravel_AreaTrabalhoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A718ItemNaoMensuravel_AreaTrabalhoCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
               }
               else
               {
                  A718ItemNaoMensuravel_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtItemNaoMensuravel_AreaTrabalhoCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
               }
               /* Read saved values. */
               Z718ItemNaoMensuravel_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z718ItemNaoMensuravel_AreaTrabalhoCod"), ",", "."));
               Z715ItemNaoMensuravel_Codigo = cgiGet( "Z715ItemNaoMensuravel_Codigo");
               Z1804ItemNaoMensuravel_Referencia = cgiGet( "Z1804ItemNaoMensuravel_Referencia");
               n1804ItemNaoMensuravel_Referencia = (String.IsNullOrEmpty(StringUtil.RTrim( A1804ItemNaoMensuravel_Referencia)) ? true : false);
               Z719ItemNaoMensuravel_Valor = context.localUtil.CToN( cgiGet( "Z719ItemNaoMensuravel_Valor"), ",", ".");
               Z717ItemNaoMensuravel_Tipo = (short)(context.localUtil.CToN( cgiGet( "Z717ItemNaoMensuravel_Tipo"), ",", "."));
               Z716ItemNaoMensuravel_Ativo = StringUtil.StrToBool( cgiGet( "Z716ItemNaoMensuravel_Ativo"));
               Z709ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z709ReferenciaINM_Codigo"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N709ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( "N709ReferenciaINM_Codigo"), ",", "."));
               AV14ItemNaoMensuravel_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vITEMNAOMENSURAVEL_AREATRABALHOCOD"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               AV7ItemNaoMensuravel_Codigo = cgiGet( "vITEMNAOMENSURAVEL_CODIGO");
               AV11Insert_ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_REFERENCIAINM_CODIGO"), ",", "."));
               A710ReferenciaINM_Descricao = cgiGet( "REFERENCIAINM_DESCRICAO");
               A720ItemNaoMensuravel_AreaTrabalhoDes = cgiGet( "ITEMNAOMENSURAVEL_AREATRABALHODES");
               n720ItemNaoMensuravel_AreaTrabalhoDes = false;
               AV16Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ItemNaoMensuravel";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A718ItemNaoMensuravel_AreaTrabalhoCod != Z718ItemNaoMensuravel_AreaTrabalhoCod ) || ( StringUtil.StrCmp(A715ItemNaoMensuravel_Codigo, Z715ItemNaoMensuravel_Codigo) != 0 ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("itemnaomensuravel:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A718ItemNaoMensuravel_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
                  A715ItemNaoMensuravel_Codigo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode91 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode91;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound91 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_2D0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "ITEMNAOMENSURAVEL_AREATRABALHOCOD");
                        AnyError = 1;
                        GX_FocusControl = edtItemNaoMensuravel_AreaTrabalhoCod_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E112D2 */
                           E112D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E122D2 */
                           E122D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E122D2 */
            E122D2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2D91( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2D91( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_2D0( )
      {
         BeforeValidate2D91( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2D91( ) ;
            }
            else
            {
               CheckExtendedTable2D91( ) ;
               CloseExtendedTableCursors2D91( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption2D0( )
      {
      }

      protected void E112D2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV16Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV17GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17GXV1), 8, 0)));
            while ( AV17GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV17GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "ReferenciaINM_Codigo") == 0 )
               {
                  AV11Insert_ReferenciaINM_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_ReferenciaINM_Codigo), 6, 0)));
               }
               AV17GXV1 = (int)(AV17GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17GXV1), 8, 0)));
            }
         }
         edtItemNaoMensuravel_AreaTrabalhoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtItemNaoMensuravel_AreaTrabalhoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtItemNaoMensuravel_AreaTrabalhoCod_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            GX_FocusControl = Dvpanel_tableattributes_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
      }

      protected void E122D2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwitemnaomensuravel.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2D91( short GX_JID )
      {
         if ( ( GX_JID == 17 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1804ItemNaoMensuravel_Referencia = T002D3_A1804ItemNaoMensuravel_Referencia[0];
               Z719ItemNaoMensuravel_Valor = T002D3_A719ItemNaoMensuravel_Valor[0];
               Z717ItemNaoMensuravel_Tipo = T002D3_A717ItemNaoMensuravel_Tipo[0];
               Z716ItemNaoMensuravel_Ativo = T002D3_A716ItemNaoMensuravel_Ativo[0];
               Z709ReferenciaINM_Codigo = T002D3_A709ReferenciaINM_Codigo[0];
            }
            else
            {
               Z1804ItemNaoMensuravel_Referencia = A1804ItemNaoMensuravel_Referencia;
               Z719ItemNaoMensuravel_Valor = A719ItemNaoMensuravel_Valor;
               Z717ItemNaoMensuravel_Tipo = A717ItemNaoMensuravel_Tipo;
               Z716ItemNaoMensuravel_Ativo = A716ItemNaoMensuravel_Ativo;
               Z709ReferenciaINM_Codigo = A709ReferenciaINM_Codigo;
            }
         }
         if ( GX_JID == -17 )
         {
            Z715ItemNaoMensuravel_Codigo = A715ItemNaoMensuravel_Codigo;
            Z714ItemNaoMensuravel_Descricao = A714ItemNaoMensuravel_Descricao;
            Z1804ItemNaoMensuravel_Referencia = A1804ItemNaoMensuravel_Referencia;
            Z719ItemNaoMensuravel_Valor = A719ItemNaoMensuravel_Valor;
            Z717ItemNaoMensuravel_Tipo = A717ItemNaoMensuravel_Tipo;
            Z716ItemNaoMensuravel_Ativo = A716ItemNaoMensuravel_Ativo;
            Z709ReferenciaINM_Codigo = A709ReferenciaINM_Codigo;
            Z718ItemNaoMensuravel_AreaTrabalhoCod = A718ItemNaoMensuravel_AreaTrabalhoCod;
            Z720ItemNaoMensuravel_AreaTrabalhoDes = A720ItemNaoMensuravel_AreaTrabalhoDes;
            Z710ReferenciaINM_Descricao = A710ReferenciaINM_Descricao;
         }
      }

      protected void standaloneNotModal( )
      {
         GXAREFERENCIAINM_CODIGO_html2D91( ) ;
         AV16Pgmname = "ItemNaoMensuravel";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Pgmname", AV16Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV14ItemNaoMensuravel_AreaTrabalhoCod) )
         {
            edtItemNaoMensuravel_AreaTrabalhoCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtItemNaoMensuravel_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtItemNaoMensuravel_AreaTrabalhoCod_Enabled), 5, 0)));
         }
         else
         {
            edtItemNaoMensuravel_AreaTrabalhoCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtItemNaoMensuravel_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtItemNaoMensuravel_AreaTrabalhoCod_Enabled), 5, 0)));
         }
         if ( ! (0==AV14ItemNaoMensuravel_AreaTrabalhoCod) )
         {
            edtItemNaoMensuravel_AreaTrabalhoCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtItemNaoMensuravel_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtItemNaoMensuravel_AreaTrabalhoCod_Enabled), 5, 0)));
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV7ItemNaoMensuravel_Codigo)) )
         {
            A715ItemNaoMensuravel_Codigo = AV7ItemNaoMensuravel_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV7ItemNaoMensuravel_Codigo)) )
         {
            edtItemNaoMensuravel_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtItemNaoMensuravel_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtItemNaoMensuravel_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtItemNaoMensuravel_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtItemNaoMensuravel_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtItemNaoMensuravel_Codigo_Enabled), 5, 0)));
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV7ItemNaoMensuravel_Codigo)) )
         {
            edtItemNaoMensuravel_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtItemNaoMensuravel_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtItemNaoMensuravel_Codigo_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ReferenciaINM_Codigo) )
         {
            dynReferenciaINM_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynReferenciaINM_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynReferenciaINM_Codigo.Enabled), 5, 0)));
         }
         else
         {
            dynReferenciaINM_Codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynReferenciaINM_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynReferenciaINM_Codigo.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         chkItemNaoMensuravel_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkItemNaoMensuravel_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkItemNaoMensuravel_Ativo.Visible), 5, 0)));
         lblTextblockitemnaomensuravel_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockitemnaomensuravel_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockitemnaomensuravel_ativo_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ReferenciaINM_Codigo) )
         {
            A709ReferenciaINM_Codigo = AV11Insert_ReferenciaINM_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ! (0==AV14ItemNaoMensuravel_AreaTrabalhoCod) )
         {
            A718ItemNaoMensuravel_AreaTrabalhoCod = AV14ItemNaoMensuravel_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A718ItemNaoMensuravel_AreaTrabalhoCod) && ( Gx_BScreen == 0 ) )
            {
               A718ItemNaoMensuravel_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A716ItemNaoMensuravel_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A716ItemNaoMensuravel_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A716ItemNaoMensuravel_Ativo", A716ItemNaoMensuravel_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T002D4 */
            pr_default.execute(2, new Object[] {A709ReferenciaINM_Codigo});
            A710ReferenciaINM_Descricao = T002D4_A710ReferenciaINM_Descricao[0];
            pr_default.close(2);
            /* Using cursor T002D5 */
            pr_default.execute(3, new Object[] {A718ItemNaoMensuravel_AreaTrabalhoCod});
            A720ItemNaoMensuravel_AreaTrabalhoDes = T002D5_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
            n720ItemNaoMensuravel_AreaTrabalhoDes = T002D5_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
            pr_default.close(3);
         }
      }

      protected void Load2D91( )
      {
         /* Using cursor T002D6 */
         pr_default.execute(4, new Object[] {A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound91 = 1;
            A720ItemNaoMensuravel_AreaTrabalhoDes = T002D6_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
            n720ItemNaoMensuravel_AreaTrabalhoDes = T002D6_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
            A714ItemNaoMensuravel_Descricao = T002D6_A714ItemNaoMensuravel_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A714ItemNaoMensuravel_Descricao", A714ItemNaoMensuravel_Descricao);
            A1804ItemNaoMensuravel_Referencia = T002D6_A1804ItemNaoMensuravel_Referencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1804ItemNaoMensuravel_Referencia", A1804ItemNaoMensuravel_Referencia);
            n1804ItemNaoMensuravel_Referencia = T002D6_n1804ItemNaoMensuravel_Referencia[0];
            A719ItemNaoMensuravel_Valor = T002D6_A719ItemNaoMensuravel_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A719ItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( A719ItemNaoMensuravel_Valor, 18, 5)));
            A710ReferenciaINM_Descricao = T002D6_A710ReferenciaINM_Descricao[0];
            A717ItemNaoMensuravel_Tipo = T002D6_A717ItemNaoMensuravel_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A717ItemNaoMensuravel_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0)));
            A716ItemNaoMensuravel_Ativo = T002D6_A716ItemNaoMensuravel_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A716ItemNaoMensuravel_Ativo", A716ItemNaoMensuravel_Ativo);
            A709ReferenciaINM_Codigo = T002D6_A709ReferenciaINM_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
            ZM2D91( -17) ;
         }
         pr_default.close(4);
         OnLoadActions2D91( ) ;
      }

      protected void OnLoadActions2D91( )
      {
      }

      protected void CheckExtendedTable2D91( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T002D5 */
         pr_default.execute(3, new Object[] {A718ItemNaoMensuravel_AreaTrabalhoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Item Nao Mensuravel_Area Trabalho'.", "ForeignKeyNotFound", 1, "ITEMNAOMENSURAVEL_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtItemNaoMensuravel_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A720ItemNaoMensuravel_AreaTrabalhoDes = T002D5_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
         n720ItemNaoMensuravel_AreaTrabalhoDes = T002D5_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
         pr_default.close(3);
         /* Using cursor T002D4 */
         pr_default.execute(2, new Object[] {A709ReferenciaINM_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Referencia INM'.", "ForeignKeyNotFound", 1, "REFERENCIAINM_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynReferenciaINM_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A710ReferenciaINM_Descricao = T002D4_A710ReferenciaINM_Descricao[0];
         pr_default.close(2);
         if ( ! ( ( A717ItemNaoMensuravel_Tipo == 1 ) || ( A717ItemNaoMensuravel_Tipo == 2 ) ) )
         {
            GX_msglist.addItem("Campo Tipo do Item fora do intervalo", "OutOfRange", 1, "ITEMNAOMENSURAVEL_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbItemNaoMensuravel_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors2D91( )
      {
         pr_default.close(3);
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_19( int A718ItemNaoMensuravel_AreaTrabalhoCod )
      {
         /* Using cursor T002D7 */
         pr_default.execute(5, new Object[] {A718ItemNaoMensuravel_AreaTrabalhoCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Item Nao Mensuravel_Area Trabalho'.", "ForeignKeyNotFound", 1, "ITEMNAOMENSURAVEL_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtItemNaoMensuravel_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A720ItemNaoMensuravel_AreaTrabalhoDes = T002D7_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
         n720ItemNaoMensuravel_AreaTrabalhoDes = T002D7_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A720ItemNaoMensuravel_AreaTrabalhoDes)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_18( int A709ReferenciaINM_Codigo )
      {
         /* Using cursor T002D8 */
         pr_default.execute(6, new Object[] {A709ReferenciaINM_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Referencia INM'.", "ForeignKeyNotFound", 1, "REFERENCIAINM_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynReferenciaINM_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A710ReferenciaINM_Descricao = T002D8_A710ReferenciaINM_Descricao[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A710ReferenciaINM_Descricao)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey2D91( )
      {
         /* Using cursor T002D9 */
         pr_default.execute(7, new Object[] {A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound91 = 1;
         }
         else
         {
            RcdFound91 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002D3 */
         pr_default.execute(1, new Object[] {A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2D91( 17) ;
            RcdFound91 = 1;
            A715ItemNaoMensuravel_Codigo = T002D3_A715ItemNaoMensuravel_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
            A714ItemNaoMensuravel_Descricao = T002D3_A714ItemNaoMensuravel_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A714ItemNaoMensuravel_Descricao", A714ItemNaoMensuravel_Descricao);
            A1804ItemNaoMensuravel_Referencia = T002D3_A1804ItemNaoMensuravel_Referencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1804ItemNaoMensuravel_Referencia", A1804ItemNaoMensuravel_Referencia);
            n1804ItemNaoMensuravel_Referencia = T002D3_n1804ItemNaoMensuravel_Referencia[0];
            A719ItemNaoMensuravel_Valor = T002D3_A719ItemNaoMensuravel_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A719ItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( A719ItemNaoMensuravel_Valor, 18, 5)));
            A717ItemNaoMensuravel_Tipo = T002D3_A717ItemNaoMensuravel_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A717ItemNaoMensuravel_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0)));
            A716ItemNaoMensuravel_Ativo = T002D3_A716ItemNaoMensuravel_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A716ItemNaoMensuravel_Ativo", A716ItemNaoMensuravel_Ativo);
            A709ReferenciaINM_Codigo = T002D3_A709ReferenciaINM_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
            A718ItemNaoMensuravel_AreaTrabalhoCod = T002D3_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
            Z718ItemNaoMensuravel_AreaTrabalhoCod = A718ItemNaoMensuravel_AreaTrabalhoCod;
            Z715ItemNaoMensuravel_Codigo = A715ItemNaoMensuravel_Codigo;
            sMode91 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2D91( ) ;
            if ( AnyError == 1 )
            {
               RcdFound91 = 0;
               InitializeNonKey2D91( ) ;
            }
            Gx_mode = sMode91;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound91 = 0;
            InitializeNonKey2D91( ) ;
            sMode91 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode91;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2D91( ) ;
         if ( RcdFound91 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound91 = 0;
         /* Using cursor T002D10 */
         pr_default.execute(8, new Object[] {A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T002D10_A718ItemNaoMensuravel_AreaTrabalhoCod[0] < A718ItemNaoMensuravel_AreaTrabalhoCod ) || ( T002D10_A718ItemNaoMensuravel_AreaTrabalhoCod[0] == A718ItemNaoMensuravel_AreaTrabalhoCod ) && ( StringUtil.StrCmp(T002D10_A715ItemNaoMensuravel_Codigo[0], A715ItemNaoMensuravel_Codigo) < 0 ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T002D10_A718ItemNaoMensuravel_AreaTrabalhoCod[0] > A718ItemNaoMensuravel_AreaTrabalhoCod ) || ( T002D10_A718ItemNaoMensuravel_AreaTrabalhoCod[0] == A718ItemNaoMensuravel_AreaTrabalhoCod ) && ( StringUtil.StrCmp(T002D10_A715ItemNaoMensuravel_Codigo[0], A715ItemNaoMensuravel_Codigo) > 0 ) ) )
            {
               A718ItemNaoMensuravel_AreaTrabalhoCod = T002D10_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
               A715ItemNaoMensuravel_Codigo = T002D10_A715ItemNaoMensuravel_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
               RcdFound91 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound91 = 0;
         /* Using cursor T002D11 */
         pr_default.execute(9, new Object[] {A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T002D11_A718ItemNaoMensuravel_AreaTrabalhoCod[0] > A718ItemNaoMensuravel_AreaTrabalhoCod ) || ( T002D11_A718ItemNaoMensuravel_AreaTrabalhoCod[0] == A718ItemNaoMensuravel_AreaTrabalhoCod ) && ( StringUtil.StrCmp(T002D11_A715ItemNaoMensuravel_Codigo[0], A715ItemNaoMensuravel_Codigo) > 0 ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T002D11_A718ItemNaoMensuravel_AreaTrabalhoCod[0] < A718ItemNaoMensuravel_AreaTrabalhoCod ) || ( T002D11_A718ItemNaoMensuravel_AreaTrabalhoCod[0] == A718ItemNaoMensuravel_AreaTrabalhoCod ) && ( StringUtil.StrCmp(T002D11_A715ItemNaoMensuravel_Codigo[0], A715ItemNaoMensuravel_Codigo) < 0 ) ) )
            {
               A718ItemNaoMensuravel_AreaTrabalhoCod = T002D11_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
               A715ItemNaoMensuravel_Codigo = T002D11_A715ItemNaoMensuravel_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
               RcdFound91 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2D91( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtItemNaoMensuravel_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2D91( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound91 == 1 )
            {
               if ( ( A718ItemNaoMensuravel_AreaTrabalhoCod != Z718ItemNaoMensuravel_AreaTrabalhoCod ) || ( StringUtil.StrCmp(A715ItemNaoMensuravel_Codigo, Z715ItemNaoMensuravel_Codigo) != 0 ) )
               {
                  A718ItemNaoMensuravel_AreaTrabalhoCod = Z718ItemNaoMensuravel_AreaTrabalhoCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
                  A715ItemNaoMensuravel_Codigo = Z715ItemNaoMensuravel_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "ITEMNAOMENSURAVEL_AREATRABALHOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtItemNaoMensuravel_AreaTrabalhoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtItemNaoMensuravel_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2D91( ) ;
                  GX_FocusControl = edtItemNaoMensuravel_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A718ItemNaoMensuravel_AreaTrabalhoCod != Z718ItemNaoMensuravel_AreaTrabalhoCod ) || ( StringUtil.StrCmp(A715ItemNaoMensuravel_Codigo, Z715ItemNaoMensuravel_Codigo) != 0 ) )
               {
                  /* Insert record */
                  GX_FocusControl = edtItemNaoMensuravel_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2D91( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "ITEMNAOMENSURAVEL_AREATRABALHOCOD");
                     AnyError = 1;
                     GX_FocusControl = edtItemNaoMensuravel_AreaTrabalhoCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtItemNaoMensuravel_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2D91( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( ( A718ItemNaoMensuravel_AreaTrabalhoCod != Z718ItemNaoMensuravel_AreaTrabalhoCod ) || ( StringUtil.StrCmp(A715ItemNaoMensuravel_Codigo, Z715ItemNaoMensuravel_Codigo) != 0 ) )
         {
            A718ItemNaoMensuravel_AreaTrabalhoCod = Z718ItemNaoMensuravel_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
            A715ItemNaoMensuravel_Codigo = Z715ItemNaoMensuravel_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "ITEMNAOMENSURAVEL_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtItemNaoMensuravel_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtItemNaoMensuravel_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2D91( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002D2 */
            pr_default.execute(0, new Object[] {A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ItemNaoMensuravel"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1804ItemNaoMensuravel_Referencia, T002D2_A1804ItemNaoMensuravel_Referencia[0]) != 0 ) || ( Z719ItemNaoMensuravel_Valor != T002D2_A719ItemNaoMensuravel_Valor[0] ) || ( Z717ItemNaoMensuravel_Tipo != T002D2_A717ItemNaoMensuravel_Tipo[0] ) || ( Z716ItemNaoMensuravel_Ativo != T002D2_A716ItemNaoMensuravel_Ativo[0] ) || ( Z709ReferenciaINM_Codigo != T002D2_A709ReferenciaINM_Codigo[0] ) )
            {
               if ( StringUtil.StrCmp(Z1804ItemNaoMensuravel_Referencia, T002D2_A1804ItemNaoMensuravel_Referencia[0]) != 0 )
               {
                  GXUtil.WriteLog("itemnaomensuravel:[seudo value changed for attri]"+"ItemNaoMensuravel_Referencia");
                  GXUtil.WriteLogRaw("Old: ",Z1804ItemNaoMensuravel_Referencia);
                  GXUtil.WriteLogRaw("Current: ",T002D2_A1804ItemNaoMensuravel_Referencia[0]);
               }
               if ( Z719ItemNaoMensuravel_Valor != T002D2_A719ItemNaoMensuravel_Valor[0] )
               {
                  GXUtil.WriteLog("itemnaomensuravel:[seudo value changed for attri]"+"ItemNaoMensuravel_Valor");
                  GXUtil.WriteLogRaw("Old: ",Z719ItemNaoMensuravel_Valor);
                  GXUtil.WriteLogRaw("Current: ",T002D2_A719ItemNaoMensuravel_Valor[0]);
               }
               if ( Z717ItemNaoMensuravel_Tipo != T002D2_A717ItemNaoMensuravel_Tipo[0] )
               {
                  GXUtil.WriteLog("itemnaomensuravel:[seudo value changed for attri]"+"ItemNaoMensuravel_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z717ItemNaoMensuravel_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T002D2_A717ItemNaoMensuravel_Tipo[0]);
               }
               if ( Z716ItemNaoMensuravel_Ativo != T002D2_A716ItemNaoMensuravel_Ativo[0] )
               {
                  GXUtil.WriteLog("itemnaomensuravel:[seudo value changed for attri]"+"ItemNaoMensuravel_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z716ItemNaoMensuravel_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T002D2_A716ItemNaoMensuravel_Ativo[0]);
               }
               if ( Z709ReferenciaINM_Codigo != T002D2_A709ReferenciaINM_Codigo[0] )
               {
                  GXUtil.WriteLog("itemnaomensuravel:[seudo value changed for attri]"+"ReferenciaINM_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z709ReferenciaINM_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T002D2_A709ReferenciaINM_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ItemNaoMensuravel"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2D91( )
      {
         BeforeValidate2D91( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2D91( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2D91( 0) ;
            CheckOptimisticConcurrency2D91( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2D91( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2D91( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002D12 */
                     pr_default.execute(10, new Object[] {A715ItemNaoMensuravel_Codigo, A714ItemNaoMensuravel_Descricao, n1804ItemNaoMensuravel_Referencia, A1804ItemNaoMensuravel_Referencia, A719ItemNaoMensuravel_Valor, A717ItemNaoMensuravel_Tipo, A716ItemNaoMensuravel_Ativo, A709ReferenciaINM_Codigo, A718ItemNaoMensuravel_AreaTrabalhoCod});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ItemNaoMensuravel") ;
                     if ( (pr_default.getStatus(10) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2D0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2D91( ) ;
            }
            EndLevel2D91( ) ;
         }
         CloseExtendedTableCursors2D91( ) ;
      }

      protected void Update2D91( )
      {
         BeforeValidate2D91( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2D91( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2D91( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2D91( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2D91( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002D13 */
                     pr_default.execute(11, new Object[] {A714ItemNaoMensuravel_Descricao, n1804ItemNaoMensuravel_Referencia, A1804ItemNaoMensuravel_Referencia, A719ItemNaoMensuravel_Valor, A717ItemNaoMensuravel_Tipo, A716ItemNaoMensuravel_Ativo, A709ReferenciaINM_Codigo, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("ItemNaoMensuravel") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ItemNaoMensuravel"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2D91( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2D91( ) ;
         }
         CloseExtendedTableCursors2D91( ) ;
      }

      protected void DeferredUpdate2D91( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2D91( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2D91( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2D91( ) ;
            AfterConfirm2D91( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2D91( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002D14 */
                  pr_default.execute(12, new Object[] {A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("ItemNaoMensuravel") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode91 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2D91( ) ;
         Gx_mode = sMode91;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2D91( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002D15 */
            pr_default.execute(13, new Object[] {A718ItemNaoMensuravel_AreaTrabalhoCod});
            A720ItemNaoMensuravel_AreaTrabalhoDes = T002D15_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
            n720ItemNaoMensuravel_AreaTrabalhoDes = T002D15_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
            pr_default.close(13);
            /* Using cursor T002D16 */
            pr_default.execute(14, new Object[] {A709ReferenciaINM_Codigo});
            A710ReferenciaINM_Descricao = T002D16_A710ReferenciaINM_Descricao[0];
            pr_default.close(14);
         }
      }

      protected void EndLevel2D91( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2D91( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(14);
            pr_default.close(13);
            context.CommitDataStores( "ItemNaoMensuravel");
            if ( AnyError == 0 )
            {
               ConfirmValues2D0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(14);
            pr_default.close(13);
            context.RollbackDataStores( "ItemNaoMensuravel");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2D91( )
      {
         /* Scan By routine */
         /* Using cursor T002D17 */
         pr_default.execute(15);
         RcdFound91 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound91 = 1;
            A718ItemNaoMensuravel_AreaTrabalhoCod = T002D17_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
            A715ItemNaoMensuravel_Codigo = T002D17_A715ItemNaoMensuravel_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2D91( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound91 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound91 = 1;
            A718ItemNaoMensuravel_AreaTrabalhoCod = T002D17_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
            A715ItemNaoMensuravel_Codigo = T002D17_A715ItemNaoMensuravel_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
         }
      }

      protected void ScanEnd2D91( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm2D91( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2D91( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2D91( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2D91( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2D91( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2D91( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2D91( )
      {
         edtItemNaoMensuravel_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtItemNaoMensuravel_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtItemNaoMensuravel_Codigo_Enabled), 5, 0)));
         edtItemNaoMensuravel_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtItemNaoMensuravel_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtItemNaoMensuravel_Descricao_Enabled), 5, 0)));
         edtItemNaoMensuravel_Valor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtItemNaoMensuravel_Valor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtItemNaoMensuravel_Valor_Enabled), 5, 0)));
         edtItemNaoMensuravel_Referencia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtItemNaoMensuravel_Referencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtItemNaoMensuravel_Referencia_Enabled), 5, 0)));
         dynReferenciaINM_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynReferenciaINM_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynReferenciaINM_Codigo.Enabled), 5, 0)));
         cmbItemNaoMensuravel_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbItemNaoMensuravel_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbItemNaoMensuravel_Tipo.Enabled), 5, 0)));
         chkItemNaoMensuravel_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkItemNaoMensuravel_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkItemNaoMensuravel_Ativo.Enabled), 5, 0)));
         edtItemNaoMensuravel_AreaTrabalhoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtItemNaoMensuravel_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtItemNaoMensuravel_AreaTrabalhoCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2D0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117223987");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("itemnaomensuravel.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV14ItemNaoMensuravel_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(AV7ItemNaoMensuravel_Codigo))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z715ItemNaoMensuravel_Codigo", StringUtil.RTrim( Z715ItemNaoMensuravel_Codigo));
         GxWebStd.gx_hidden_field( context, "Z1804ItemNaoMensuravel_Referencia", StringUtil.RTrim( Z1804ItemNaoMensuravel_Referencia));
         GxWebStd.gx_hidden_field( context, "Z719ItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.NToC( Z719ItemNaoMensuravel_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z717ItemNaoMensuravel_Tipo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z717ItemNaoMensuravel_Tipo), 2, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z716ItemNaoMensuravel_Ativo", Z716ItemNaoMensuravel_Ativo);
         GxWebStd.gx_hidden_field( context, "Z709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z709ReferenciaINM_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A709ReferenciaINM_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vITEMNAOMENSURAVEL_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14ItemNaoMensuravel_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vITEMNAOMENSURAVEL_CODIGO", StringUtil.RTrim( AV7ItemNaoMensuravel_Codigo));
         GxWebStd.gx_hidden_field( context, "vINSERT_REFERENCIAINM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_ReferenciaINM_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REFERENCIAINM_DESCRICAO", A710ReferenciaINM_Descricao);
         GxWebStd.gx_hidden_field( context, "ITEMNAOMENSURAVEL_AREATRABALHODES", A720ItemNaoMensuravel_AreaTrabalhoDes);
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV16Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vITEMNAOMENSURAVEL_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV14ItemNaoMensuravel_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vITEMNAOMENSURAVEL_CODIGO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7ItemNaoMensuravel_Codigo, "@!"))));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ItemNaoMensuravel";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("itemnaomensuravel:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("itemnaomensuravel.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV14ItemNaoMensuravel_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(AV7ItemNaoMensuravel_Codigo)) ;
      }

      public override String GetPgmname( )
      {
         return "ItemNaoMensuravel" ;
      }

      public override String GetPgmdesc( )
      {
         return "Item N�o Mensuravel" ;
      }

      protected void InitializeNonKey2D91( )
      {
         A709ReferenciaINM_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A709ReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A709ReferenciaINM_Codigo), 6, 0)));
         A720ItemNaoMensuravel_AreaTrabalhoDes = "";
         n720ItemNaoMensuravel_AreaTrabalhoDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A720ItemNaoMensuravel_AreaTrabalhoDes", A720ItemNaoMensuravel_AreaTrabalhoDes);
         A714ItemNaoMensuravel_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A714ItemNaoMensuravel_Descricao", A714ItemNaoMensuravel_Descricao);
         A1804ItemNaoMensuravel_Referencia = "";
         n1804ItemNaoMensuravel_Referencia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1804ItemNaoMensuravel_Referencia", A1804ItemNaoMensuravel_Referencia);
         n1804ItemNaoMensuravel_Referencia = (String.IsNullOrEmpty(StringUtil.RTrim( A1804ItemNaoMensuravel_Referencia)) ? true : false);
         A719ItemNaoMensuravel_Valor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A719ItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( A719ItemNaoMensuravel_Valor, 18, 5)));
         A710ReferenciaINM_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A710ReferenciaINM_Descricao", A710ReferenciaINM_Descricao);
         A717ItemNaoMensuravel_Tipo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A717ItemNaoMensuravel_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0)));
         A716ItemNaoMensuravel_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A716ItemNaoMensuravel_Ativo", A716ItemNaoMensuravel_Ativo);
         Z1804ItemNaoMensuravel_Referencia = "";
         Z719ItemNaoMensuravel_Valor = 0;
         Z717ItemNaoMensuravel_Tipo = 0;
         Z716ItemNaoMensuravel_Ativo = false;
         Z709ReferenciaINM_Codigo = 0;
      }

      protected void InitAll2D91( )
      {
         A718ItemNaoMensuravel_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A718ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
         A715ItemNaoMensuravel_Codigo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
         InitializeNonKey2D91( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A716ItemNaoMensuravel_Ativo = i716ItemNaoMensuravel_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A716ItemNaoMensuravel_Ativo", A716ItemNaoMensuravel_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117224018");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("itemnaomensuravel.js", "?20203117224019");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockitemnaomensuravel_codigo_Internalname = "TEXTBLOCKITEMNAOMENSURAVEL_CODIGO";
         edtItemNaoMensuravel_Codigo_Internalname = "ITEMNAOMENSURAVEL_CODIGO";
         lblTextblockitemnaomensuravel_descricao_Internalname = "TEXTBLOCKITEMNAOMENSURAVEL_DESCRICAO";
         edtItemNaoMensuravel_Descricao_Internalname = "ITEMNAOMENSURAVEL_DESCRICAO";
         lblTextblockitemnaomensuravel_valor_Internalname = "TEXTBLOCKITEMNAOMENSURAVEL_VALOR";
         edtItemNaoMensuravel_Valor_Internalname = "ITEMNAOMENSURAVEL_VALOR";
         lblTextblockitemnaomensuravel_referencia_Internalname = "TEXTBLOCKITEMNAOMENSURAVEL_REFERENCIA";
         edtItemNaoMensuravel_Referencia_Internalname = "ITEMNAOMENSURAVEL_REFERENCIA";
         lblTextblockreferenciainm_codigo_Internalname = "TEXTBLOCKREFERENCIAINM_CODIGO";
         dynReferenciaINM_Codigo_Internalname = "REFERENCIAINM_CODIGO";
         lblTextblockitemnaomensuravel_tipo_Internalname = "TEXTBLOCKITEMNAOMENSURAVEL_TIPO";
         cmbItemNaoMensuravel_Tipo_Internalname = "ITEMNAOMENSURAVEL_TIPO";
         lblTextblockitemnaomensuravel_ativo_Internalname = "TEXTBLOCKITEMNAOMENSURAVEL_ATIVO";
         chkItemNaoMensuravel_Ativo_Internalname = "ITEMNAOMENSURAVEL_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtItemNaoMensuravel_AreaTrabalhoCod_Internalname = "ITEMNAOMENSURAVEL_AREATRABALHOCOD";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Item N�o Mensuravel";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Item N�o Mensuravel";
         chkItemNaoMensuravel_Ativo.Enabled = 1;
         chkItemNaoMensuravel_Ativo.Visible = 1;
         lblTextblockitemnaomensuravel_ativo_Visible = 1;
         cmbItemNaoMensuravel_Tipo_Jsonclick = "";
         cmbItemNaoMensuravel_Tipo.Enabled = 1;
         dynReferenciaINM_Codigo_Jsonclick = "";
         dynReferenciaINM_Codigo.Enabled = 1;
         edtItemNaoMensuravel_Referencia_Jsonclick = "";
         edtItemNaoMensuravel_Referencia_Enabled = 1;
         edtItemNaoMensuravel_Valor_Jsonclick = "";
         edtItemNaoMensuravel_Valor_Enabled = 1;
         edtItemNaoMensuravel_Descricao_Enabled = 1;
         edtItemNaoMensuravel_Codigo_Jsonclick = "";
         edtItemNaoMensuravel_Codigo_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtItemNaoMensuravel_AreaTrabalhoCod_Jsonclick = "";
         edtItemNaoMensuravel_AreaTrabalhoCod_Enabled = 1;
         edtItemNaoMensuravel_AreaTrabalhoCod_Visible = 1;
         chkItemNaoMensuravel_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAREFERENCIAINM_CODIGO2D91( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAREFERENCIAINM_CODIGO_data2D91( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAREFERENCIAINM_CODIGO_html2D91( )
      {
         int gxdynajaxvalue ;
         GXDLAREFERENCIAINM_CODIGO_data2D91( ) ;
         gxdynajaxindex = 1;
         dynReferenciaINM_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynReferenciaINM_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAREFERENCIAINM_CODIGO_data2D91( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T002D18 */
         pr_default.execute(16);
         while ( (pr_default.getStatus(16) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002D18_A709ReferenciaINM_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T002D18_A710ReferenciaINM_Descricao[0]);
            pr_default.readNext(16);
         }
         pr_default.close(16);
      }

      public void Valid_Itemnaomensuravel_areatrabalhocod( int GX_Parm1 ,
                                                           String GX_Parm2 )
      {
         A718ItemNaoMensuravel_AreaTrabalhoCod = GX_Parm1;
         A720ItemNaoMensuravel_AreaTrabalhoDes = GX_Parm2;
         n720ItemNaoMensuravel_AreaTrabalhoDes = false;
         /* Using cursor T002D19 */
         pr_default.execute(17, new Object[] {A718ItemNaoMensuravel_AreaTrabalhoCod});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Item Nao Mensuravel_Area Trabalho'.", "ForeignKeyNotFound", 1, "ITEMNAOMENSURAVEL_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtItemNaoMensuravel_AreaTrabalhoCod_Internalname;
         }
         A720ItemNaoMensuravel_AreaTrabalhoDes = T002D19_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
         n720ItemNaoMensuravel_AreaTrabalhoDes = T002D19_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
         pr_default.close(17);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A720ItemNaoMensuravel_AreaTrabalhoDes = "";
            n720ItemNaoMensuravel_AreaTrabalhoDes = false;
         }
         isValidOutput.Add(A720ItemNaoMensuravel_AreaTrabalhoDes);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Referenciainm_codigo( GXCombobox dynGX_Parm1 ,
                                              String GX_Parm2 )
      {
         dynReferenciaINM_Codigo = dynGX_Parm1;
         A709ReferenciaINM_Codigo = (int)(NumberUtil.Val( dynReferenciaINM_Codigo.CurrentValue, "."));
         A710ReferenciaINM_Descricao = GX_Parm2;
         /* Using cursor T002D20 */
         pr_default.execute(18, new Object[] {A709ReferenciaINM_Codigo});
         if ( (pr_default.getStatus(18) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Referencia INM'.", "ForeignKeyNotFound", 1, "REFERENCIAINM_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynReferenciaINM_Codigo_Internalname;
         }
         A710ReferenciaINM_Descricao = T002D20_A710ReferenciaINM_Descricao[0];
         pr_default.close(18);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A710ReferenciaINM_Descricao = "";
         }
         isValidOutput.Add(A710ReferenciaINM_Descricao);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV14ItemNaoMensuravel_AreaTrabalhoCod',fld:'vITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV7ItemNaoMensuravel_Codigo',fld:'vITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E122D2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(18);
         pr_default.close(14);
         pr_default.close(17);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         wcpOAV7ItemNaoMensuravel_Codigo = "";
         Z715ItemNaoMensuravel_Codigo = "";
         Z1804ItemNaoMensuravel_Referencia = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockitemnaomensuravel_codigo_Jsonclick = "";
         A715ItemNaoMensuravel_Codigo = "";
         lblTextblockitemnaomensuravel_descricao_Jsonclick = "";
         A714ItemNaoMensuravel_Descricao = "";
         lblTextblockitemnaomensuravel_valor_Jsonclick = "";
         lblTextblockitemnaomensuravel_referencia_Jsonclick = "";
         A1804ItemNaoMensuravel_Referencia = "";
         lblTextblockreferenciainm_codigo_Jsonclick = "";
         lblTextblockitemnaomensuravel_tipo_Jsonclick = "";
         lblTextblockitemnaomensuravel_ativo_Jsonclick = "";
         A710ReferenciaINM_Descricao = "";
         A720ItemNaoMensuravel_AreaTrabalhoDes = "";
         AV16Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode91 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z714ItemNaoMensuravel_Descricao = "";
         Z720ItemNaoMensuravel_AreaTrabalhoDes = "";
         Z710ReferenciaINM_Descricao = "";
         T002D4_A710ReferenciaINM_Descricao = new String[] {""} ;
         T002D5_A720ItemNaoMensuravel_AreaTrabalhoDes = new String[] {""} ;
         T002D5_n720ItemNaoMensuravel_AreaTrabalhoDes = new bool[] {false} ;
         T002D6_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         T002D6_A720ItemNaoMensuravel_AreaTrabalhoDes = new String[] {""} ;
         T002D6_n720ItemNaoMensuravel_AreaTrabalhoDes = new bool[] {false} ;
         T002D6_A714ItemNaoMensuravel_Descricao = new String[] {""} ;
         T002D6_A1804ItemNaoMensuravel_Referencia = new String[] {""} ;
         T002D6_n1804ItemNaoMensuravel_Referencia = new bool[] {false} ;
         T002D6_A719ItemNaoMensuravel_Valor = new decimal[1] ;
         T002D6_A710ReferenciaINM_Descricao = new String[] {""} ;
         T002D6_A717ItemNaoMensuravel_Tipo = new short[1] ;
         T002D6_A716ItemNaoMensuravel_Ativo = new bool[] {false} ;
         T002D6_A709ReferenciaINM_Codigo = new int[1] ;
         T002D6_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         T002D7_A720ItemNaoMensuravel_AreaTrabalhoDes = new String[] {""} ;
         T002D7_n720ItemNaoMensuravel_AreaTrabalhoDes = new bool[] {false} ;
         T002D8_A710ReferenciaINM_Descricao = new String[] {""} ;
         T002D9_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         T002D9_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         T002D3_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         T002D3_A714ItemNaoMensuravel_Descricao = new String[] {""} ;
         T002D3_A1804ItemNaoMensuravel_Referencia = new String[] {""} ;
         T002D3_n1804ItemNaoMensuravel_Referencia = new bool[] {false} ;
         T002D3_A719ItemNaoMensuravel_Valor = new decimal[1] ;
         T002D3_A717ItemNaoMensuravel_Tipo = new short[1] ;
         T002D3_A716ItemNaoMensuravel_Ativo = new bool[] {false} ;
         T002D3_A709ReferenciaINM_Codigo = new int[1] ;
         T002D3_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         T002D10_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         T002D10_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         T002D11_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         T002D11_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         T002D2_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         T002D2_A714ItemNaoMensuravel_Descricao = new String[] {""} ;
         T002D2_A1804ItemNaoMensuravel_Referencia = new String[] {""} ;
         T002D2_n1804ItemNaoMensuravel_Referencia = new bool[] {false} ;
         T002D2_A719ItemNaoMensuravel_Valor = new decimal[1] ;
         T002D2_A717ItemNaoMensuravel_Tipo = new short[1] ;
         T002D2_A716ItemNaoMensuravel_Ativo = new bool[] {false} ;
         T002D2_A709ReferenciaINM_Codigo = new int[1] ;
         T002D2_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         T002D15_A720ItemNaoMensuravel_AreaTrabalhoDes = new String[] {""} ;
         T002D15_n720ItemNaoMensuravel_AreaTrabalhoDes = new bool[] {false} ;
         T002D16_A710ReferenciaINM_Descricao = new String[] {""} ;
         T002D17_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         T002D17_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T002D18_A709ReferenciaINM_Codigo = new int[1] ;
         T002D18_A710ReferenciaINM_Descricao = new String[] {""} ;
         T002D19_A720ItemNaoMensuravel_AreaTrabalhoDes = new String[] {""} ;
         T002D19_n720ItemNaoMensuravel_AreaTrabalhoDes = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         T002D20_A710ReferenciaINM_Descricao = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.itemnaomensuravel__default(),
            new Object[][] {
                new Object[] {
               T002D2_A715ItemNaoMensuravel_Codigo, T002D2_A714ItemNaoMensuravel_Descricao, T002D2_A1804ItemNaoMensuravel_Referencia, T002D2_n1804ItemNaoMensuravel_Referencia, T002D2_A719ItemNaoMensuravel_Valor, T002D2_A717ItemNaoMensuravel_Tipo, T002D2_A716ItemNaoMensuravel_Ativo, T002D2_A709ReferenciaINM_Codigo, T002D2_A718ItemNaoMensuravel_AreaTrabalhoCod
               }
               , new Object[] {
               T002D3_A715ItemNaoMensuravel_Codigo, T002D3_A714ItemNaoMensuravel_Descricao, T002D3_A1804ItemNaoMensuravel_Referencia, T002D3_n1804ItemNaoMensuravel_Referencia, T002D3_A719ItemNaoMensuravel_Valor, T002D3_A717ItemNaoMensuravel_Tipo, T002D3_A716ItemNaoMensuravel_Ativo, T002D3_A709ReferenciaINM_Codigo, T002D3_A718ItemNaoMensuravel_AreaTrabalhoCod
               }
               , new Object[] {
               T002D4_A710ReferenciaINM_Descricao
               }
               , new Object[] {
               T002D5_A720ItemNaoMensuravel_AreaTrabalhoDes, T002D5_n720ItemNaoMensuravel_AreaTrabalhoDes
               }
               , new Object[] {
               T002D6_A715ItemNaoMensuravel_Codigo, T002D6_A720ItemNaoMensuravel_AreaTrabalhoDes, T002D6_n720ItemNaoMensuravel_AreaTrabalhoDes, T002D6_A714ItemNaoMensuravel_Descricao, T002D6_A1804ItemNaoMensuravel_Referencia, T002D6_n1804ItemNaoMensuravel_Referencia, T002D6_A719ItemNaoMensuravel_Valor, T002D6_A710ReferenciaINM_Descricao, T002D6_A717ItemNaoMensuravel_Tipo, T002D6_A716ItemNaoMensuravel_Ativo,
               T002D6_A709ReferenciaINM_Codigo, T002D6_A718ItemNaoMensuravel_AreaTrabalhoCod
               }
               , new Object[] {
               T002D7_A720ItemNaoMensuravel_AreaTrabalhoDes, T002D7_n720ItemNaoMensuravel_AreaTrabalhoDes
               }
               , new Object[] {
               T002D8_A710ReferenciaINM_Descricao
               }
               , new Object[] {
               T002D9_A718ItemNaoMensuravel_AreaTrabalhoCod, T002D9_A715ItemNaoMensuravel_Codigo
               }
               , new Object[] {
               T002D10_A718ItemNaoMensuravel_AreaTrabalhoCod, T002D10_A715ItemNaoMensuravel_Codigo
               }
               , new Object[] {
               T002D11_A718ItemNaoMensuravel_AreaTrabalhoCod, T002D11_A715ItemNaoMensuravel_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002D15_A720ItemNaoMensuravel_AreaTrabalhoDes, T002D15_n720ItemNaoMensuravel_AreaTrabalhoDes
               }
               , new Object[] {
               T002D16_A710ReferenciaINM_Descricao
               }
               , new Object[] {
               T002D17_A718ItemNaoMensuravel_AreaTrabalhoCod, T002D17_A715ItemNaoMensuravel_Codigo
               }
               , new Object[] {
               T002D18_A709ReferenciaINM_Codigo, T002D18_A710ReferenciaINM_Descricao
               }
               , new Object[] {
               T002D19_A720ItemNaoMensuravel_AreaTrabalhoDes, T002D19_n720ItemNaoMensuravel_AreaTrabalhoDes
               }
               , new Object[] {
               T002D20_A710ReferenciaINM_Descricao
               }
            }
         );
         Z716ItemNaoMensuravel_Ativo = true;
         A716ItemNaoMensuravel_Ativo = true;
         i716ItemNaoMensuravel_Ativo = true;
         AV16Pgmname = "ItemNaoMensuravel";
         Z718ItemNaoMensuravel_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         A718ItemNaoMensuravel_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
      }

      private short Z717ItemNaoMensuravel_Tipo ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A717ItemNaoMensuravel_Tipo ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound91 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV14ItemNaoMensuravel_AreaTrabalhoCod ;
      private int Z718ItemNaoMensuravel_AreaTrabalhoCod ;
      private int Z709ReferenciaINM_Codigo ;
      private int N709ReferenciaINM_Codigo ;
      private int A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private int A709ReferenciaINM_Codigo ;
      private int AV14ItemNaoMensuravel_AreaTrabalhoCod ;
      private int trnEnded ;
      private int edtItemNaoMensuravel_AreaTrabalhoCod_Visible ;
      private int edtItemNaoMensuravel_AreaTrabalhoCod_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtItemNaoMensuravel_Codigo_Enabled ;
      private int edtItemNaoMensuravel_Descricao_Enabled ;
      private int edtItemNaoMensuravel_Valor_Enabled ;
      private int edtItemNaoMensuravel_Referencia_Enabled ;
      private int lblTextblockitemnaomensuravel_ativo_Visible ;
      private int AV11Insert_ReferenciaINM_Codigo ;
      private int AV17GXV1 ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal Z719ItemNaoMensuravel_Valor ;
      private decimal A719ItemNaoMensuravel_Valor ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String wcpOAV7ItemNaoMensuravel_Codigo ;
      private String Z715ItemNaoMensuravel_Codigo ;
      private String Z1804ItemNaoMensuravel_Referencia ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String AV7ItemNaoMensuravel_Codigo ;
      private String GXKey ;
      private String chkItemNaoMensuravel_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtItemNaoMensuravel_Codigo_Internalname ;
      private String TempTags ;
      private String edtItemNaoMensuravel_AreaTrabalhoCod_Internalname ;
      private String edtItemNaoMensuravel_AreaTrabalhoCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockitemnaomensuravel_codigo_Internalname ;
      private String lblTextblockitemnaomensuravel_codigo_Jsonclick ;
      private String A715ItemNaoMensuravel_Codigo ;
      private String edtItemNaoMensuravel_Codigo_Jsonclick ;
      private String lblTextblockitemnaomensuravel_descricao_Internalname ;
      private String lblTextblockitemnaomensuravel_descricao_Jsonclick ;
      private String edtItemNaoMensuravel_Descricao_Internalname ;
      private String lblTextblockitemnaomensuravel_valor_Internalname ;
      private String lblTextblockitemnaomensuravel_valor_Jsonclick ;
      private String edtItemNaoMensuravel_Valor_Internalname ;
      private String edtItemNaoMensuravel_Valor_Jsonclick ;
      private String lblTextblockitemnaomensuravel_referencia_Internalname ;
      private String lblTextblockitemnaomensuravel_referencia_Jsonclick ;
      private String edtItemNaoMensuravel_Referencia_Internalname ;
      private String A1804ItemNaoMensuravel_Referencia ;
      private String edtItemNaoMensuravel_Referencia_Jsonclick ;
      private String lblTextblockreferenciainm_codigo_Internalname ;
      private String lblTextblockreferenciainm_codigo_Jsonclick ;
      private String dynReferenciaINM_Codigo_Internalname ;
      private String dynReferenciaINM_Codigo_Jsonclick ;
      private String lblTextblockitemnaomensuravel_tipo_Internalname ;
      private String lblTextblockitemnaomensuravel_tipo_Jsonclick ;
      private String cmbItemNaoMensuravel_Tipo_Internalname ;
      private String cmbItemNaoMensuravel_Tipo_Jsonclick ;
      private String lblTextblockitemnaomensuravel_ativo_Internalname ;
      private String lblTextblockitemnaomensuravel_ativo_Jsonclick ;
      private String AV16Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode91 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Dvpanel_tableattributes_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private bool Z716ItemNaoMensuravel_Ativo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A716ItemNaoMensuravel_Ativo ;
      private bool n1804ItemNaoMensuravel_Referencia ;
      private bool n720ItemNaoMensuravel_AreaTrabalhoDes ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool i716ItemNaoMensuravel_Ativo ;
      private String A714ItemNaoMensuravel_Descricao ;
      private String Z714ItemNaoMensuravel_Descricao ;
      private String A710ReferenciaINM_Descricao ;
      private String A720ItemNaoMensuravel_AreaTrabalhoDes ;
      private String Z720ItemNaoMensuravel_AreaTrabalhoDes ;
      private String Z710ReferenciaINM_Descricao ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynReferenciaINM_Codigo ;
      private GXCombobox cmbItemNaoMensuravel_Tipo ;
      private GXCheckbox chkItemNaoMensuravel_Ativo ;
      private IDataStoreProvider pr_default ;
      private String[] T002D4_A710ReferenciaINM_Descricao ;
      private String[] T002D5_A720ItemNaoMensuravel_AreaTrabalhoDes ;
      private bool[] T002D5_n720ItemNaoMensuravel_AreaTrabalhoDes ;
      private String[] T002D6_A715ItemNaoMensuravel_Codigo ;
      private String[] T002D6_A720ItemNaoMensuravel_AreaTrabalhoDes ;
      private bool[] T002D6_n720ItemNaoMensuravel_AreaTrabalhoDes ;
      private String[] T002D6_A714ItemNaoMensuravel_Descricao ;
      private String[] T002D6_A1804ItemNaoMensuravel_Referencia ;
      private bool[] T002D6_n1804ItemNaoMensuravel_Referencia ;
      private decimal[] T002D6_A719ItemNaoMensuravel_Valor ;
      private String[] T002D6_A710ReferenciaINM_Descricao ;
      private short[] T002D6_A717ItemNaoMensuravel_Tipo ;
      private bool[] T002D6_A716ItemNaoMensuravel_Ativo ;
      private int[] T002D6_A709ReferenciaINM_Codigo ;
      private int[] T002D6_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] T002D7_A720ItemNaoMensuravel_AreaTrabalhoDes ;
      private bool[] T002D7_n720ItemNaoMensuravel_AreaTrabalhoDes ;
      private String[] T002D8_A710ReferenciaINM_Descricao ;
      private int[] T002D9_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] T002D9_A715ItemNaoMensuravel_Codigo ;
      private String[] T002D3_A715ItemNaoMensuravel_Codigo ;
      private String[] T002D3_A714ItemNaoMensuravel_Descricao ;
      private String[] T002D3_A1804ItemNaoMensuravel_Referencia ;
      private bool[] T002D3_n1804ItemNaoMensuravel_Referencia ;
      private decimal[] T002D3_A719ItemNaoMensuravel_Valor ;
      private short[] T002D3_A717ItemNaoMensuravel_Tipo ;
      private bool[] T002D3_A716ItemNaoMensuravel_Ativo ;
      private int[] T002D3_A709ReferenciaINM_Codigo ;
      private int[] T002D3_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private int[] T002D10_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] T002D10_A715ItemNaoMensuravel_Codigo ;
      private int[] T002D11_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] T002D11_A715ItemNaoMensuravel_Codigo ;
      private String[] T002D2_A715ItemNaoMensuravel_Codigo ;
      private String[] T002D2_A714ItemNaoMensuravel_Descricao ;
      private String[] T002D2_A1804ItemNaoMensuravel_Referencia ;
      private bool[] T002D2_n1804ItemNaoMensuravel_Referencia ;
      private decimal[] T002D2_A719ItemNaoMensuravel_Valor ;
      private short[] T002D2_A717ItemNaoMensuravel_Tipo ;
      private bool[] T002D2_A716ItemNaoMensuravel_Ativo ;
      private int[] T002D2_A709ReferenciaINM_Codigo ;
      private int[] T002D2_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] T002D15_A720ItemNaoMensuravel_AreaTrabalhoDes ;
      private bool[] T002D15_n720ItemNaoMensuravel_AreaTrabalhoDes ;
      private String[] T002D16_A710ReferenciaINM_Descricao ;
      private int[] T002D17_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] T002D17_A715ItemNaoMensuravel_Codigo ;
      private int[] T002D18_A709ReferenciaINM_Codigo ;
      private String[] T002D18_A710ReferenciaINM_Descricao ;
      private String[] T002D19_A720ItemNaoMensuravel_AreaTrabalhoDes ;
      private bool[] T002D19_n720ItemNaoMensuravel_AreaTrabalhoDes ;
      private String[] T002D20_A710ReferenciaINM_Descricao ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class itemnaomensuravel__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002D6 ;
          prmT002D6 = new Object[] {
          new Object[] {"@ItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ItemNaoMensuravel_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002D5 ;
          prmT002D5 = new Object[] {
          new Object[] {"@ItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002D4 ;
          prmT002D4 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002D7 ;
          prmT002D7 = new Object[] {
          new Object[] {"@ItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002D8 ;
          prmT002D8 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002D9 ;
          prmT002D9 = new Object[] {
          new Object[] {"@ItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ItemNaoMensuravel_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002D3 ;
          prmT002D3 = new Object[] {
          new Object[] {"@ItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ItemNaoMensuravel_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002D10 ;
          prmT002D10 = new Object[] {
          new Object[] {"@ItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ItemNaoMensuravel_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002D11 ;
          prmT002D11 = new Object[] {
          new Object[] {"@ItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ItemNaoMensuravel_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002D2 ;
          prmT002D2 = new Object[] {
          new Object[] {"@ItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ItemNaoMensuravel_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002D12 ;
          prmT002D12 = new Object[] {
          new Object[] {"@ItemNaoMensuravel_Codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@ItemNaoMensuravel_Descricao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ItemNaoMensuravel_Referencia",SqlDbType.Char,15,0} ,
          new Object[] {"@ItemNaoMensuravel_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ItemNaoMensuravel_Tipo",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ItemNaoMensuravel_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002D13 ;
          prmT002D13 = new Object[] {
          new Object[] {"@ItemNaoMensuravel_Descricao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ItemNaoMensuravel_Referencia",SqlDbType.Char,15,0} ,
          new Object[] {"@ItemNaoMensuravel_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ItemNaoMensuravel_Tipo",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ItemNaoMensuravel_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ItemNaoMensuravel_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002D14 ;
          prmT002D14 = new Object[] {
          new Object[] {"@ItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ItemNaoMensuravel_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmT002D15 ;
          prmT002D15 = new Object[] {
          new Object[] {"@ItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002D16 ;
          prmT002D16 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002D17 ;
          prmT002D17 = new Object[] {
          } ;
          Object[] prmT002D18 ;
          prmT002D18 = new Object[] {
          } ;
          Object[] prmT002D19 ;
          prmT002D19 = new Object[] {
          new Object[] {"@ItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002D20 ;
          prmT002D20 = new Object[] {
          new Object[] {"@ReferenciaINM_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002D2", "SELECT [ItemNaoMensuravel_Codigo], [ItemNaoMensuravel_Descricao], [ItemNaoMensuravel_Referencia], [ItemNaoMensuravel_Valor], [ItemNaoMensuravel_Tipo], [ItemNaoMensuravel_Ativo], [ReferenciaINM_Codigo], [ItemNaoMensuravel_AreaTrabalhoCod] AS ItemNaoMensuravel_AreaTrabalhoCod FROM [ItemNaoMensuravel] WITH (UPDLOCK) WHERE [ItemNaoMensuravel_AreaTrabalhoCod] = @ItemNaoMensuravel_AreaTrabalhoCod AND [ItemNaoMensuravel_Codigo] = @ItemNaoMensuravel_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002D2,1,0,true,false )
             ,new CursorDef("T002D3", "SELECT [ItemNaoMensuravel_Codigo], [ItemNaoMensuravel_Descricao], [ItemNaoMensuravel_Referencia], [ItemNaoMensuravel_Valor], [ItemNaoMensuravel_Tipo], [ItemNaoMensuravel_Ativo], [ReferenciaINM_Codigo], [ItemNaoMensuravel_AreaTrabalhoCod] AS ItemNaoMensuravel_AreaTrabalhoCod FROM [ItemNaoMensuravel] WITH (NOLOCK) WHERE [ItemNaoMensuravel_AreaTrabalhoCod] = @ItemNaoMensuravel_AreaTrabalhoCod AND [ItemNaoMensuravel_Codigo] = @ItemNaoMensuravel_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002D3,1,0,true,false )
             ,new CursorDef("T002D4", "SELECT [ReferenciaINM_Descricao] FROM [ReferenciaINM] WITH (NOLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002D4,1,0,true,false )
             ,new CursorDef("T002D5", "SELECT [AreaTrabalho_Descricao] AS ItemNaoMensuravel_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ItemNaoMensuravel_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002D5,1,0,true,false )
             ,new CursorDef("T002D6", "SELECT TM1.[ItemNaoMensuravel_Codigo], T2.[AreaTrabalho_Descricao] AS ItemNaoMensuravel_AreaTrabalhoDes, TM1.[ItemNaoMensuravel_Descricao], TM1.[ItemNaoMensuravel_Referencia], TM1.[ItemNaoMensuravel_Valor], T3.[ReferenciaINM_Descricao], TM1.[ItemNaoMensuravel_Tipo], TM1.[ItemNaoMensuravel_Ativo], TM1.[ReferenciaINM_Codigo], TM1.[ItemNaoMensuravel_AreaTrabalhoCod] AS ItemNaoMensuravel_AreaTrabalhoCod FROM (([ItemNaoMensuravel] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[ItemNaoMensuravel_AreaTrabalhoCod]) INNER JOIN [ReferenciaINM] T3 WITH (NOLOCK) ON T3.[ReferenciaINM_Codigo] = TM1.[ReferenciaINM_Codigo]) WHERE TM1.[ItemNaoMensuravel_AreaTrabalhoCod] = @ItemNaoMensuravel_AreaTrabalhoCod and TM1.[ItemNaoMensuravel_Codigo] = @ItemNaoMensuravel_Codigo ORDER BY TM1.[ItemNaoMensuravel_AreaTrabalhoCod], TM1.[ItemNaoMensuravel_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002D6,100,0,true,false )
             ,new CursorDef("T002D7", "SELECT [AreaTrabalho_Descricao] AS ItemNaoMensuravel_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ItemNaoMensuravel_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002D7,1,0,true,false )
             ,new CursorDef("T002D8", "SELECT [ReferenciaINM_Descricao] FROM [ReferenciaINM] WITH (NOLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002D8,1,0,true,false )
             ,new CursorDef("T002D9", "SELECT [ItemNaoMensuravel_AreaTrabalhoCod] AS ItemNaoMensuravel_AreaTrabalhoCod, [ItemNaoMensuravel_Codigo] FROM [ItemNaoMensuravel] WITH (NOLOCK) WHERE [ItemNaoMensuravel_AreaTrabalhoCod] = @ItemNaoMensuravel_AreaTrabalhoCod AND [ItemNaoMensuravel_Codigo] = @ItemNaoMensuravel_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002D9,1,0,true,false )
             ,new CursorDef("T002D10", "SELECT TOP 1 [ItemNaoMensuravel_AreaTrabalhoCod] AS ItemNaoMensuravel_AreaTrabalhoCod, [ItemNaoMensuravel_Codigo] FROM [ItemNaoMensuravel] WITH (NOLOCK) WHERE ( [ItemNaoMensuravel_AreaTrabalhoCod] > @ItemNaoMensuravel_AreaTrabalhoCod or [ItemNaoMensuravel_AreaTrabalhoCod] = @ItemNaoMensuravel_AreaTrabalhoCod and [ItemNaoMensuravel_Codigo] > @ItemNaoMensuravel_Codigo) ORDER BY [ItemNaoMensuravel_AreaTrabalhoCod], [ItemNaoMensuravel_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002D10,1,0,true,true )
             ,new CursorDef("T002D11", "SELECT TOP 1 [ItemNaoMensuravel_AreaTrabalhoCod] AS ItemNaoMensuravel_AreaTrabalhoCod, [ItemNaoMensuravel_Codigo] FROM [ItemNaoMensuravel] WITH (NOLOCK) WHERE ( [ItemNaoMensuravel_AreaTrabalhoCod] < @ItemNaoMensuravel_AreaTrabalhoCod or [ItemNaoMensuravel_AreaTrabalhoCod] = @ItemNaoMensuravel_AreaTrabalhoCod and [ItemNaoMensuravel_Codigo] < @ItemNaoMensuravel_Codigo) ORDER BY [ItemNaoMensuravel_AreaTrabalhoCod] DESC, [ItemNaoMensuravel_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002D11,1,0,true,true )
             ,new CursorDef("T002D12", "INSERT INTO [ItemNaoMensuravel]([ItemNaoMensuravel_Codigo], [ItemNaoMensuravel_Descricao], [ItemNaoMensuravel_Referencia], [ItemNaoMensuravel_Valor], [ItemNaoMensuravel_Tipo], [ItemNaoMensuravel_Ativo], [ReferenciaINM_Codigo], [ItemNaoMensuravel_AreaTrabalhoCod]) VALUES(@ItemNaoMensuravel_Codigo, @ItemNaoMensuravel_Descricao, @ItemNaoMensuravel_Referencia, @ItemNaoMensuravel_Valor, @ItemNaoMensuravel_Tipo, @ItemNaoMensuravel_Ativo, @ReferenciaINM_Codigo, @ItemNaoMensuravel_AreaTrabalhoCod)", GxErrorMask.GX_NOMASK,prmT002D12)
             ,new CursorDef("T002D13", "UPDATE [ItemNaoMensuravel] SET [ItemNaoMensuravel_Descricao]=@ItemNaoMensuravel_Descricao, [ItemNaoMensuravel_Referencia]=@ItemNaoMensuravel_Referencia, [ItemNaoMensuravel_Valor]=@ItemNaoMensuravel_Valor, [ItemNaoMensuravel_Tipo]=@ItemNaoMensuravel_Tipo, [ItemNaoMensuravel_Ativo]=@ItemNaoMensuravel_Ativo, [ReferenciaINM_Codigo]=@ReferenciaINM_Codigo  WHERE [ItemNaoMensuravel_AreaTrabalhoCod] = @ItemNaoMensuravel_AreaTrabalhoCod AND [ItemNaoMensuravel_Codigo] = @ItemNaoMensuravel_Codigo", GxErrorMask.GX_NOMASK,prmT002D13)
             ,new CursorDef("T002D14", "DELETE FROM [ItemNaoMensuravel]  WHERE [ItemNaoMensuravel_AreaTrabalhoCod] = @ItemNaoMensuravel_AreaTrabalhoCod AND [ItemNaoMensuravel_Codigo] = @ItemNaoMensuravel_Codigo", GxErrorMask.GX_NOMASK,prmT002D14)
             ,new CursorDef("T002D15", "SELECT [AreaTrabalho_Descricao] AS ItemNaoMensuravel_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ItemNaoMensuravel_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002D15,1,0,true,false )
             ,new CursorDef("T002D16", "SELECT [ReferenciaINM_Descricao] FROM [ReferenciaINM] WITH (NOLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002D16,1,0,true,false )
             ,new CursorDef("T002D17", "SELECT [ItemNaoMensuravel_AreaTrabalhoCod] AS ItemNaoMensuravel_AreaTrabalhoCod, [ItemNaoMensuravel_Codigo] FROM [ItemNaoMensuravel] WITH (NOLOCK) ORDER BY [ItemNaoMensuravel_AreaTrabalhoCod], [ItemNaoMensuravel_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002D17,100,0,true,false )
             ,new CursorDef("T002D18", "SELECT [ReferenciaINM_Codigo], [ReferenciaINM_Descricao] FROM [ReferenciaINM] WITH (NOLOCK) ORDER BY [ReferenciaINM_Descricao] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002D18,0,0,true,false )
             ,new CursorDef("T002D19", "SELECT [AreaTrabalho_Descricao] AS ItemNaoMensuravel_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ItemNaoMensuravel_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002D19,1,0,true,false )
             ,new CursorDef("T002D20", "SELECT [ReferenciaINM_Descricao] FROM [ReferenciaINM] WITH (NOLOCK) WHERE [ReferenciaINM_Codigo] = @ReferenciaINM_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002D20,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((short[]) buf[8])[0] = rslt.getShort(7) ;
                ((bool[]) buf[9])[0] = rslt.getBool(8) ;
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((int[]) buf[11])[0] = rslt.getInt(10) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                stmt.SetParameter(4, (decimal)parms[4]);
                stmt.SetParameter(5, (short)parms[5]);
                stmt.SetParameter(6, (bool)parms[6]);
                stmt.SetParameter(7, (int)parms[7]);
                stmt.SetParameter(8, (int)parms[8]);
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (decimal)parms[3]);
                stmt.SetParameter(4, (short)parms[4]);
                stmt.SetParameter(5, (bool)parms[5]);
                stmt.SetParameter(6, (int)parms[6]);
                stmt.SetParameter(7, (int)parms[7]);
                stmt.SetParameter(8, (String)parms[8]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
