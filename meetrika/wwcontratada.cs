/*
               File: WWContratada
        Description:  Contratadas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:32:57.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontratada : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontratada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontratada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         dynavContratada_areatrabalhocod = new GXCombobox();
         dynavContratada_codigo = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         chkContratada_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADA_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADA_CODIGO352( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_77 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_77_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_77_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV17Contratada_PessoaNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_PessoaNom1", AV17Contratada_PessoaNom1);
               AV73Contratada_PessoaCNPJ1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Contratada_PessoaCNPJ1", AV73Contratada_PessoaCNPJ1);
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV22Contratada_PessoaNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratada_PessoaNom2", AV22Contratada_PessoaNom2);
               AV74Contratada_PessoaCNPJ2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74Contratada_PessoaCNPJ2", AV74Contratada_PessoaCNPJ2);
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV80TFContratada_PessoaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratada_PessoaNom", AV80TFContratada_PessoaNom);
               AV81TFContratada_PessoaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratada_PessoaNom_Sel", AV81TFContratada_PessoaNom_Sel);
               AV84TFContratada_PessoaCNPJ = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContratada_PessoaCNPJ", AV84TFContratada_PessoaCNPJ);
               AV85TFContratada_PessoaCNPJ_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratada_PessoaCNPJ_Sel", AV85TFContratada_PessoaCNPJ_Sel);
               AV88TFPessoa_IE = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFPessoa_IE", AV88TFPessoa_IE);
               AV89TFPessoa_IE_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFPessoa_IE_Sel", AV89TFPessoa_IE_Sel);
               AV92TFPessoa_Telefone = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFPessoa_Telefone", AV92TFPessoa_Telefone);
               AV93TFPessoa_Telefone_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93TFPessoa_Telefone_Sel", AV93TFPessoa_Telefone_Sel);
               AV96TFContratada_SS = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96TFContratada_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96TFContratada_SS), 8, 0)));
               AV97TFContratada_SS_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97TFContratada_SS_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV97TFContratada_SS_To), 8, 0)));
               AV100TFContratada_OS = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFContratada_OS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100TFContratada_OS), 8, 0)));
               AV101TFContratada_OS_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFContratada_OS_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV101TFContratada_OS_To), 8, 0)));
               AV104TFContratada_Lote = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFContratada_Lote", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104TFContratada_Lote), 4, 0)));
               AV105TFContratada_Lote_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFContratada_Lote_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFContratada_Lote_To), 4, 0)));
               AV108TFContratada_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContratada_Ativo_Sel", StringUtil.Str( (decimal)(AV108TFContratada_Ativo_Sel), 1, 0));
               AV82ddo_Contratada_PessoaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ddo_Contratada_PessoaNomTitleControlIdToReplace", AV82ddo_Contratada_PessoaNomTitleControlIdToReplace);
               AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace", AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace);
               AV90ddo_Pessoa_IETitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ddo_Pessoa_IETitleControlIdToReplace", AV90ddo_Pessoa_IETitleControlIdToReplace);
               AV94ddo_Pessoa_TelefoneTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94ddo_Pessoa_TelefoneTitleControlIdToReplace", AV94ddo_Pessoa_TelefoneTitleControlIdToReplace);
               AV98ddo_Contratada_SSTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98ddo_Contratada_SSTitleControlIdToReplace", AV98ddo_Contratada_SSTitleControlIdToReplace);
               AV102ddo_Contratada_OSTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ddo_Contratada_OSTitleControlIdToReplace", AV102ddo_Contratada_OSTitleControlIdToReplace);
               AV106ddo_Contratada_LoteTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106ddo_Contratada_LoteTitleControlIdToReplace", AV106ddo_Contratada_LoteTitleControlIdToReplace);
               AV109ddo_Contratada_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109ddo_Contratada_AtivoTitleControlIdToReplace", AV109ddo_Contratada_AtivoTitleControlIdToReplace);
               AV42Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Contratada_AreaTrabalhoCod), 6, 0)));
               AV76Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76Contratada_Codigo), 6, 0)));
               AV144Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV30DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
               AV29DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
               A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A43Contratada_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Contratada_PessoaNom1, AV73Contratada_PessoaCNPJ1, AV20DynamicFiltersSelector2, AV22Contratada_PessoaNom2, AV74Contratada_PessoaCNPJ2, AV19DynamicFiltersEnabled2, AV80TFContratada_PessoaNom, AV81TFContratada_PessoaNom_Sel, AV84TFContratada_PessoaCNPJ, AV85TFContratada_PessoaCNPJ_Sel, AV88TFPessoa_IE, AV89TFPessoa_IE_Sel, AV92TFPessoa_Telefone, AV93TFPessoa_Telefone_Sel, AV96TFContratada_SS, AV97TFContratada_SS_To, AV100TFContratada_OS, AV101TFContratada_OS_To, AV104TFContratada_Lote, AV105TFContratada_Lote_To, AV108TFContratada_Ativo_Sel, AV82ddo_Contratada_PessoaNomTitleControlIdToReplace, AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV90ddo_Pessoa_IETitleControlIdToReplace, AV94ddo_Pessoa_TelefoneTitleControlIdToReplace, AV98ddo_Contratada_SSTitleControlIdToReplace, AV102ddo_Contratada_OSTitleControlIdToReplace, AV106ddo_Contratada_LoteTitleControlIdToReplace, AV109ddo_Contratada_AtivoTitleControlIdToReplace, AV42Contratada_AreaTrabalhoCod, AV76Contratada_Codigo, AV144Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A39Contratada_Codigo, A43Contratada_Ativo, AV6WWPContext) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA352( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START352( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117325797");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontratada.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADA_PESSOANOM1", StringUtil.RTrim( AV17Contratada_PessoaNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADA_PESSOACNPJ1", AV73Contratada_PessoaCNPJ1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADA_PESSOANOM2", StringUtil.RTrim( AV22Contratada_PessoaNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADA_PESSOACNPJ2", AV74Contratada_PessoaCNPJ2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOANOM", StringUtil.RTrim( AV80TFContratada_PessoaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOANOM_SEL", StringUtil.RTrim( AV81TFContratada_PessoaNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOACNPJ", AV84TFContratada_PessoaCNPJ);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOACNPJ_SEL", AV85TFContratada_PessoaCNPJ_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_IE", StringUtil.RTrim( AV88TFPessoa_IE));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_IE_SEL", StringUtil.RTrim( AV89TFPessoa_IE_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_TELEFONE", StringUtil.RTrim( AV92TFPessoa_Telefone));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOA_TELEFONE_SEL", StringUtil.RTrim( AV93TFPessoa_Telefone_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_SS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV96TFContratada_SS), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_SS_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV97TFContratada_SS_To), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_OS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV100TFContratada_OS), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_OS_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV101TFContratada_OS_To), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_LOTE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV104TFContratada_Lote), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_LOTE_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV105TFContratada_Lote_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV108TFContratada_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_77", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_77), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV112GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV113GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV110DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV110DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_PESSOANOMTITLEFILTERDATA", AV79Contratada_PessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_PESSOANOMTITLEFILTERDATA", AV79Contratada_PessoaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_PESSOACNPJTITLEFILTERDATA", AV83Contratada_PessoaCNPJTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_PESSOACNPJTITLEFILTERDATA", AV83Contratada_PessoaCNPJTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPESSOA_IETITLEFILTERDATA", AV87Pessoa_IETitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPESSOA_IETITLEFILTERDATA", AV87Pessoa_IETitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPESSOA_TELEFONETITLEFILTERDATA", AV91Pessoa_TelefoneTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPESSOA_TELEFONETITLEFILTERDATA", AV91Pessoa_TelefoneTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_SSTITLEFILTERDATA", AV95Contratada_SSTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_SSTITLEFILTERDATA", AV95Contratada_SSTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_OSTITLEFILTERDATA", AV99Contratada_OSTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_OSTITLEFILTERDATA", AV99Contratada_OSTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_LOTETITLEFILTERDATA", AV103Contratada_LoteTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_LOTETITLEFILTERDATA", AV103Contratada_LoteTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_ATIVOTITLEFILTERDATA", AV107Contratada_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_ATIVOTITLEFILTERDATA", AV107Contratada_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV144Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTIFICATIONINFO", AV44NotificationInfo);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTIFICATIONINFO", AV44NotificationInfo);
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Caption", StringUtil.RTrim( Ddo_contratada_pessoanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Tooltip", StringUtil.RTrim( Ddo_contratada_pessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Cls", StringUtil.RTrim( Ddo_contratada_pessoanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_pessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratada_pessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_pessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_pessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortedstatus", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filtertype", StringUtil.RTrim( Ddo_contratada_pessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_contratada_pessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_contratada_pessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratada_pessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortasc", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortdsc", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_contratada_pessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_contratada_pessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_contratada_pessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_pessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Caption", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Tooltip", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Cls", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_set", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Sortedstatus", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includefilter", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filtertype", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalisttype", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalistproc", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Sortasc", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Sortdsc", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Loadingdata", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Cleanfilter", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Noresultsfound", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Caption", StringUtil.RTrim( Ddo_pessoa_ie_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Tooltip", StringUtil.RTrim( Ddo_pessoa_ie_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Cls", StringUtil.RTrim( Ddo_pessoa_ie_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Filteredtext_set", StringUtil.RTrim( Ddo_pessoa_ie_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Selectedvalue_set", StringUtil.RTrim( Ddo_pessoa_ie_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoa_ie_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoa_ie_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Includesortasc", StringUtil.BoolToStr( Ddo_pessoa_ie_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoa_ie_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Sortedstatus", StringUtil.RTrim( Ddo_pessoa_ie_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Includefilter", StringUtil.BoolToStr( Ddo_pessoa_ie_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Filtertype", StringUtil.RTrim( Ddo_pessoa_ie_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Filterisrange", StringUtil.BoolToStr( Ddo_pessoa_ie_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Includedatalist", StringUtil.BoolToStr( Ddo_pessoa_ie_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Datalisttype", StringUtil.RTrim( Ddo_pessoa_ie_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Datalistproc", StringUtil.RTrim( Ddo_pessoa_ie_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_pessoa_ie_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Sortasc", StringUtil.RTrim( Ddo_pessoa_ie_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Sortdsc", StringUtil.RTrim( Ddo_pessoa_ie_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Loadingdata", StringUtil.RTrim( Ddo_pessoa_ie_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Cleanfilter", StringUtil.RTrim( Ddo_pessoa_ie_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Noresultsfound", StringUtil.RTrim( Ddo_pessoa_ie_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Searchbuttontext", StringUtil.RTrim( Ddo_pessoa_ie_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Caption", StringUtil.RTrim( Ddo_pessoa_telefone_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Tooltip", StringUtil.RTrim( Ddo_pessoa_telefone_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Cls", StringUtil.RTrim( Ddo_pessoa_telefone_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Filteredtext_set", StringUtil.RTrim( Ddo_pessoa_telefone_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Selectedvalue_set", StringUtil.RTrim( Ddo_pessoa_telefone_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoa_telefone_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoa_telefone_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Includesortasc", StringUtil.BoolToStr( Ddo_pessoa_telefone_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoa_telefone_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Sortedstatus", StringUtil.RTrim( Ddo_pessoa_telefone_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Includefilter", StringUtil.BoolToStr( Ddo_pessoa_telefone_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Filtertype", StringUtil.RTrim( Ddo_pessoa_telefone_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Filterisrange", StringUtil.BoolToStr( Ddo_pessoa_telefone_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Includedatalist", StringUtil.BoolToStr( Ddo_pessoa_telefone_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Datalisttype", StringUtil.RTrim( Ddo_pessoa_telefone_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Datalistproc", StringUtil.RTrim( Ddo_pessoa_telefone_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_pessoa_telefone_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Sortasc", StringUtil.RTrim( Ddo_pessoa_telefone_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Sortdsc", StringUtil.RTrim( Ddo_pessoa_telefone_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Loadingdata", StringUtil.RTrim( Ddo_pessoa_telefone_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Cleanfilter", StringUtil.RTrim( Ddo_pessoa_telefone_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Noresultsfound", StringUtil.RTrim( Ddo_pessoa_telefone_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Searchbuttontext", StringUtil.RTrim( Ddo_pessoa_telefone_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Caption", StringUtil.RTrim( Ddo_contratada_ss_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Tooltip", StringUtil.RTrim( Ddo_contratada_ss_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Cls", StringUtil.RTrim( Ddo_contratada_ss_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_ss_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Filteredtextto_set", StringUtil.RTrim( Ddo_contratada_ss_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_ss_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_ss_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_ss_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_ss_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Sortedstatus", StringUtil.RTrim( Ddo_contratada_ss_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Includefilter", StringUtil.BoolToStr( Ddo_contratada_ss_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Filtertype", StringUtil.RTrim( Ddo_contratada_ss_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_ss_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_ss_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Sortasc", StringUtil.RTrim( Ddo_contratada_ss_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Sortdsc", StringUtil.RTrim( Ddo_contratada_ss_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Cleanfilter", StringUtil.RTrim( Ddo_contratada_ss_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Rangefilterfrom", StringUtil.RTrim( Ddo_contratada_ss_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Rangefilterto", StringUtil.RTrim( Ddo_contratada_ss_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_ss_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Caption", StringUtil.RTrim( Ddo_contratada_os_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Tooltip", StringUtil.RTrim( Ddo_contratada_os_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Cls", StringUtil.RTrim( Ddo_contratada_os_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_os_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Filteredtextto_set", StringUtil.RTrim( Ddo_contratada_os_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_os_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_os_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_os_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_os_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Sortedstatus", StringUtil.RTrim( Ddo_contratada_os_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Includefilter", StringUtil.BoolToStr( Ddo_contratada_os_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Filtertype", StringUtil.RTrim( Ddo_contratada_os_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_os_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_os_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Sortasc", StringUtil.RTrim( Ddo_contratada_os_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Sortdsc", StringUtil.RTrim( Ddo_contratada_os_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Cleanfilter", StringUtil.RTrim( Ddo_contratada_os_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Rangefilterfrom", StringUtil.RTrim( Ddo_contratada_os_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Rangefilterto", StringUtil.RTrim( Ddo_contratada_os_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_os_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Caption", StringUtil.RTrim( Ddo_contratada_lote_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Tooltip", StringUtil.RTrim( Ddo_contratada_lote_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Cls", StringUtil.RTrim( Ddo_contratada_lote_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_lote_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Filteredtextto_set", StringUtil.RTrim( Ddo_contratada_lote_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_lote_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_lote_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_lote_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_lote_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Sortedstatus", StringUtil.RTrim( Ddo_contratada_lote_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Includefilter", StringUtil.BoolToStr( Ddo_contratada_lote_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Filtertype", StringUtil.RTrim( Ddo_contratada_lote_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_lote_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_lote_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Sortasc", StringUtil.RTrim( Ddo_contratada_lote_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Sortdsc", StringUtil.RTrim( Ddo_contratada_lote_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Cleanfilter", StringUtil.RTrim( Ddo_contratada_lote_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Rangefilterfrom", StringUtil.RTrim( Ddo_contratada_lote_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Rangefilterto", StringUtil.RTrim( Ddo_contratada_lote_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_lote_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Caption", StringUtil.RTrim( Ddo_contratada_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Tooltip", StringUtil.RTrim( Ddo_contratada_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Cls", StringUtil.RTrim( Ddo_contratada_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratada_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_contratada_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_contratada_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_contratada_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratada_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Sortasc", StringUtil.RTrim( Ddo_contratada_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_contratada_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_contratada_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_contratada_pessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_pessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratada_pessoanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Activeeventkey", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_get", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Activeeventkey", StringUtil.RTrim( Ddo_pessoa_ie_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Filteredtext_get", StringUtil.RTrim( Ddo_pessoa_ie_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_IE_Selectedvalue_get", StringUtil.RTrim( Ddo_pessoa_ie_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Activeeventkey", StringUtil.RTrim( Ddo_pessoa_telefone_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Filteredtext_get", StringUtil.RTrim( Ddo_pessoa_telefone_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOA_TELEFONE_Selectedvalue_get", StringUtil.RTrim( Ddo_pessoa_telefone_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Activeeventkey", StringUtil.RTrim( Ddo_contratada_ss_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_ss_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_SS_Filteredtextto_get", StringUtil.RTrim( Ddo_contratada_ss_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Activeeventkey", StringUtil.RTrim( Ddo_contratada_os_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_os_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_OS_Filteredtextto_get", StringUtil.RTrim( Ddo_contratada_os_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Activeeventkey", StringUtil.RTrim( Ddo_contratada_lote_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_lote_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_LOTE_Filteredtextto_get", StringUtil.RTrim( Ddo_contratada_lote_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_contratada_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratada_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE352( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT352( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontratada.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContratada" ;
      }

      public override String GetPgmdesc( )
      {
         return " Contratadas" ;
      }

      protected void WB350( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_352( true) ;
         }
         else
         {
            wb_table1_2_352( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_352e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(96, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoanom_Internalname, StringUtil.RTrim( AV80TFContratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( AV80TFContratada_PessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratada_pessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoanom_sel_Internalname, StringUtil.RTrim( AV81TFContratada_PessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV81TFContratada_PessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratada_pessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoacnpj_Internalname, AV84TFContratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( AV84TFContratada_PessoaCNPJ, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoacnpj_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_pessoacnpj_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoacnpj_sel_Internalname, AV85TFContratada_PessoaCNPJ_Sel, StringUtil.RTrim( context.localUtil.Format( AV85TFContratada_PessoaCNPJ_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoacnpj_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_pessoacnpj_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_ie_Internalname, StringUtil.RTrim( AV88TFPessoa_IE), StringUtil.RTrim( context.localUtil.Format( AV88TFPessoa_IE, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_ie_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_ie_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_ie_sel_Internalname, StringUtil.RTrim( AV89TFPessoa_IE_Sel), StringUtil.RTrim( context.localUtil.Format( AV89TFPessoa_IE_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_ie_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_ie_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_telefone_Internalname, StringUtil.RTrim( AV92TFPessoa_Telefone), StringUtil.RTrim( context.localUtil.Format( AV92TFPessoa_Telefone, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_telefone_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_telefone_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoa_telefone_sel_Internalname, StringUtil.RTrim( AV93TFPessoa_Telefone_Sel), StringUtil.RTrim( context.localUtil.Format( AV93TFPessoa_Telefone_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoa_telefone_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoa_telefone_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_ss_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV96TFContratada_SS), 8, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV96TFContratada_SS), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_ss_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_ss_Visible, 1, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_ss_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV97TFContratada_SS_To), 8, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV97TFContratada_SS_To), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_ss_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_ss_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_os_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV100TFContratada_OS), 8, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV100TFContratada_OS), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_os_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_os_Visible, 1, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_os_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV101TFContratada_OS_To), 8, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV101TFContratada_OS_To), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_os_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_os_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_lote_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV104TFContratada_Lote), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV104TFContratada_Lote), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_lote_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_lote_Visible, 1, 0, "text", "", 45, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_lote_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV105TFContratada_Lote_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV105TFContratada_Lote_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_lote_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_lote_to_Visible, 1, 0, "text", "", 45, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV108TFContratada_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV108TFContratada_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratada.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_PESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname, AV82ddo_Contratada_PessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"", 0, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratada.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_PESSOACNPJContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname, AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", 0, edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratada.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PESSOA_IEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoa_ietitlecontrolidtoreplace_Internalname, AV90ddo_Pessoa_IETitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"", 0, edtavDdo_pessoa_ietitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratada.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PESSOA_TELEFONEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Internalname, AV94ddo_Pessoa_TelefoneTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", 0, edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratada.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_SSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_sstitlecontrolidtoreplace_Internalname, AV98ddo_Contratada_SSTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"", 0, edtavDdo_contratada_sstitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratada.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_OSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_ostitlecontrolidtoreplace_Internalname, AV102ddo_Contratada_OSTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"", 0, edtavDdo_contratada_ostitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratada.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_LOTEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_lotetitlecontrolidtoreplace_Internalname, AV106ddo_Contratada_LoteTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"", 0, edtavDdo_contratada_lotetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratada.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_ativotitlecontrolidtoreplace_Internalname, AV109ddo_Contratada_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"", 0, edtavDdo_contratada_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratada.htm");
         }
         wbLoad = true;
      }

      protected void START352( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Contratadas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP350( ) ;
      }

      protected void WS352( )
      {
         START352( ) ;
         EVT352( ) ;
      }

      protected void EVT352( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11352 */
                              E11352 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12352 */
                              E12352 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_PESSOACNPJ.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13352 */
                              E13352 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOA_IE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14352 */
                              E14352 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOA_TELEFONE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15352 */
                              E15352 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_SS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16352 */
                              E16352 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_OS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17352 */
                              E17352 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_LOTE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18352 */
                              E18352 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19352 */
                              E19352 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20352 */
                              E20352 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21352 */
                              E21352 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22352 */
                              E22352 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23352 */
                              E23352 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24352 */
                              E24352 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25352 */
                              E25352 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26352 */
                              E26352 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E27352 */
                              E27352 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ONMESSAGE_GX1") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E28352 */
                              E28352 ();
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "ONMESSAGE_GX1") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_77_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_77_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_77_idx), 4, 0)), 4, "0");
                              SubsflControlProps_772( ) ;
                              AV31Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV141Update_GXI : context.convertURL( context.PathToRelativeUrl( AV31Update))));
                              AV32Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV142Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV32Delete))));
                              AV75Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV75Display)) ? AV143Display_GXI : context.convertURL( context.PathToRelativeUrl( AV75Display))));
                              A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratada_Codigo_Internalname), ",", "."));
                              A52Contratada_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtContratada_AreaTrabalhoCod_Internalname), ",", "."));
                              A40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratada_PessoaCod_Internalname), ",", "."));
                              A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
                              n41Contratada_PessoaNom = false;
                              A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
                              n42Contratada_PessoaCNPJ = false;
                              A518Pessoa_IE = cgiGet( edtPessoa_IE_Internalname);
                              n518Pessoa_IE = false;
                              A522Pessoa_Telefone = cgiGet( edtPessoa_Telefone_Internalname);
                              n522Pessoa_Telefone = false;
                              A1451Contratada_SS = (int)(context.localUtil.CToN( cgiGet( edtContratada_SS_Internalname), ",", "."));
                              n1451Contratada_SS = false;
                              A524Contratada_OS = (int)(context.localUtil.CToN( cgiGet( edtContratada_OS_Internalname), ",", "."));
                              n524Contratada_OS = false;
                              A530Contratada_Lote = (short)(context.localUtil.CToN( cgiGet( edtContratada_Lote_Internalname), ",", "."));
                              n530Contratada_Lote = false;
                              A43Contratada_Ativo = StringUtil.StrToBool( cgiGet( chkContratada_Ativo_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E29352 */
                                    E29352 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E30352 */
                                    E30352 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E31352 */
                                    E31352 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ONMESSAGE_GX1") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28352 */
                                    E28352 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratada_pessoanom1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM1"), AV17Contratada_PessoaNom1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratada_pessoacnpj1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOACNPJ1"), AV73Contratada_PessoaCNPJ1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratada_pessoanom2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM2"), AV22Contratada_PessoaNom2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratada_pessoacnpj2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOACNPJ2"), AV74Contratada_PessoaCNPJ2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_pessoanom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM"), AV80TFContratada_PessoaNom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_pessoanom_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM_SEL"), AV81TFContratada_PessoaNom_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_pessoacnpj Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ"), AV84TFContratada_PessoaCNPJ) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_pessoacnpj_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ_SEL"), AV85TFContratada_PessoaCNPJ_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_ie Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_IE"), AV88TFPessoa_IE) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_ie_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_IE_SEL"), AV89TFPessoa_IE_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_telefone Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_TELEFONE"), AV92TFPessoa_Telefone) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoa_telefone_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_TELEFONE_SEL"), AV93TFPessoa_Telefone_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_ss Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_SS"), ",", ".") != Convert.ToDecimal( AV96TFContratada_SS )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_ss_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_SS_TO"), ",", ".") != Convert.ToDecimal( AV97TFContratada_SS_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_os Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_OS"), ",", ".") != Convert.ToDecimal( AV100TFContratada_OS )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_os_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_OS_TO"), ",", ".") != Convert.ToDecimal( AV101TFContratada_OS_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_lote Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_LOTE"), ",", ".") != Convert.ToDecimal( AV104TFContratada_Lote )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_lote_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_LOTE_TO"), ",", ".") != Convert.ToDecimal( AV105TFContratada_Lote_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV108TFContratada_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ONMESSAGE_GX1") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28352 */
                                    E28352 ();
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE352( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA352( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            dynavContratada_areatrabalhocod.Name = "vCONTRATADA_AREATRABALHOCOD";
            dynavContratada_areatrabalhocod.WebTags = "";
            dynavContratada_areatrabalhocod.removeAllItems();
            /* Using cursor H00352 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynavContratada_areatrabalhocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00352_A5AreaTrabalho_Codigo[0]), 6, 0)), H00352_A6AreaTrabalho_Descricao[0], 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynavContratada_areatrabalhocod.ItemCount > 0 )
            {
               AV42Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavContratada_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV42Contratada_AreaTrabalhoCod), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Contratada_AreaTrabalhoCod), 6, 0)));
            }
            dynavContratada_codigo.Name = "vCONTRATADA_CODIGO";
            dynavContratada_codigo.WebTags = "";
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATADA_PESSOANOM", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATADA_PESSOACNPJ", "CNPJ", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATADA_PESSOANOM", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATADA_PESSOACNPJ", "CNPJ", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            GXCCtl = "CONTRATADA_ATIVO_" + sGXsfl_77_idx;
            chkContratada_Ativo.Name = GXCCtl;
            chkContratada_Ativo.WebTags = "";
            chkContratada_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratada_Ativo_Internalname, "TitleCaption", chkContratada_Ativo.Caption);
            chkContratada_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTRATADA_AREATRABALHOCOD351( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADA_AREATRABALHOCOD_data351( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADA_AREATRABALHOCOD_html351( )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADA_AREATRABALHOCOD_data351( ) ;
         gxdynajaxindex = 1;
         dynavContratada_areatrabalhocod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratada_areatrabalhocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratada_areatrabalhocod.ItemCount > 0 )
         {
            AV42Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavContratada_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV42Contratada_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Contratada_AreaTrabalhoCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADA_AREATRABALHOCOD_data351( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00353 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00353_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00353_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvCONTRATADA_CODIGO352( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADA_CODIGO_data352( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADA_CODIGO_html352( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADA_CODIGO_data352( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContratada_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratada_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV76Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV76Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76Contratada_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADA_CODIGO_data352( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00354 */
         pr_default.execute(2, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00354_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00354_A41Contratada_PessoaNom[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_772( ) ;
         while ( nGXsfl_77_idx <= nRC_GXsfl_77 )
         {
            sendrow_772( ) ;
            nGXsfl_77_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_77_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_77_idx+1));
            sGXsfl_77_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_77_idx), 4, 0)), 4, "0");
            SubsflControlProps_772( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV17Contratada_PessoaNom1 ,
                                       String AV73Contratada_PessoaCNPJ1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       String AV22Contratada_PessoaNom2 ,
                                       String AV74Contratada_PessoaCNPJ2 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       String AV80TFContratada_PessoaNom ,
                                       String AV81TFContratada_PessoaNom_Sel ,
                                       String AV84TFContratada_PessoaCNPJ ,
                                       String AV85TFContratada_PessoaCNPJ_Sel ,
                                       String AV88TFPessoa_IE ,
                                       String AV89TFPessoa_IE_Sel ,
                                       String AV92TFPessoa_Telefone ,
                                       String AV93TFPessoa_Telefone_Sel ,
                                       int AV96TFContratada_SS ,
                                       int AV97TFContratada_SS_To ,
                                       int AV100TFContratada_OS ,
                                       int AV101TFContratada_OS_To ,
                                       short AV104TFContratada_Lote ,
                                       short AV105TFContratada_Lote_To ,
                                       short AV108TFContratada_Ativo_Sel ,
                                       String AV82ddo_Contratada_PessoaNomTitleControlIdToReplace ,
                                       String AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace ,
                                       String AV90ddo_Pessoa_IETitleControlIdToReplace ,
                                       String AV94ddo_Pessoa_TelefoneTitleControlIdToReplace ,
                                       String AV98ddo_Contratada_SSTitleControlIdToReplace ,
                                       String AV102ddo_Contratada_OSTitleControlIdToReplace ,
                                       String AV106ddo_Contratada_LoteTitleControlIdToReplace ,
                                       String AV109ddo_Contratada_AtivoTitleControlIdToReplace ,
                                       int AV42Contratada_AreaTrabalhoCod ,
                                       int AV76Contratada_Codigo ,
                                       String AV144Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV30DynamicFiltersIgnoreFirst ,
                                       bool AV29DynamicFiltersRemoving ,
                                       int A39Contratada_Codigo ,
                                       bool A43Contratada_Ativo ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF352( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A52Contratada_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_PESSOACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A40Contratada_PessoaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_SS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1451Contratada_SS), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_SS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1451Contratada_SS), 8, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_OS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A524Contratada_OS), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_OS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A524Contratada_OS), 8, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_LOTE", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A530Contratada_Lote), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_LOTE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A530Contratada_Lote), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_ATIVO", GetSecureSignedToken( "", A43Contratada_Ativo));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_ATIVO", StringUtil.BoolToStr( A43Contratada_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( dynavContratada_areatrabalhocod.ItemCount > 0 )
         {
            AV42Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavContratada_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV42Contratada_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Contratada_AreaTrabalhoCod), 6, 0)));
         }
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV76Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV76Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76Contratada_Codigo), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF352( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV144Pgmname = "WWContratada";
         context.Gx_err = 0;
      }

      protected void RF352( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 77;
         /* Execute user event: E30352 */
         E30352 ();
         nGXsfl_77_idx = 1;
         sGXsfl_77_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_77_idx), 4, 0)), 4, "0");
         SubsflControlProps_772( ) ;
         nGXsfl_77_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_772( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(3, new Object[]{ new Object[]{
                                                 AV6WWPContext.gxTpr_Contratada_codigo ,
                                                 AV119WWContratadaDS_3_Dynamicfiltersselector1 ,
                                                 AV120WWContratadaDS_4_Contratada_pessoanom1 ,
                                                 AV121WWContratadaDS_5_Contratada_pessoacnpj1 ,
                                                 AV122WWContratadaDS_6_Dynamicfiltersenabled2 ,
                                                 AV123WWContratadaDS_7_Dynamicfiltersselector2 ,
                                                 AV124WWContratadaDS_8_Contratada_pessoanom2 ,
                                                 AV125WWContratadaDS_9_Contratada_pessoacnpj2 ,
                                                 AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel ,
                                                 AV126WWContratadaDS_10_Tfcontratada_pessoanom ,
                                                 AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel ,
                                                 AV128WWContratadaDS_12_Tfcontratada_pessoacnpj ,
                                                 AV131WWContratadaDS_15_Tfpessoa_ie_sel ,
                                                 AV130WWContratadaDS_14_Tfpessoa_ie ,
                                                 AV133WWContratadaDS_17_Tfpessoa_telefone_sel ,
                                                 AV132WWContratadaDS_16_Tfpessoa_telefone ,
                                                 AV134WWContratadaDS_18_Tfcontratada_ss ,
                                                 AV135WWContratadaDS_19_Tfcontratada_ss_to ,
                                                 AV136WWContratadaDS_20_Tfcontratada_os ,
                                                 AV137WWContratadaDS_21_Tfcontratada_os_to ,
                                                 AV138WWContratadaDS_22_Tfcontratada_lote ,
                                                 AV139WWContratadaDS_23_Tfcontratada_lote_to ,
                                                 AV140WWContratadaDS_24_Tfcontratada_ativo_sel ,
                                                 A39Contratada_Codigo ,
                                                 A41Contratada_PessoaNom ,
                                                 A42Contratada_PessoaCNPJ ,
                                                 A518Pessoa_IE ,
                                                 A522Pessoa_Telefone ,
                                                 A1451Contratada_SS ,
                                                 A524Contratada_OS ,
                                                 A530Contratada_Lote ,
                                                 A43Contratada_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A52Contratada_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV120WWContratadaDS_4_Contratada_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV120WWContratadaDS_4_Contratada_pessoanom1), 100, "%");
            lV121WWContratadaDS_5_Contratada_pessoacnpj1 = StringUtil.Concat( StringUtil.RTrim( AV121WWContratadaDS_5_Contratada_pessoacnpj1), "%", "");
            lV124WWContratadaDS_8_Contratada_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV124WWContratadaDS_8_Contratada_pessoanom2), 100, "%");
            lV125WWContratadaDS_9_Contratada_pessoacnpj2 = StringUtil.Concat( StringUtil.RTrim( AV125WWContratadaDS_9_Contratada_pessoacnpj2), "%", "");
            lV126WWContratadaDS_10_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV126WWContratadaDS_10_Tfcontratada_pessoanom), 100, "%");
            lV128WWContratadaDS_12_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV128WWContratadaDS_12_Tfcontratada_pessoacnpj), "%", "");
            lV130WWContratadaDS_14_Tfpessoa_ie = StringUtil.PadR( StringUtil.RTrim( AV130WWContratadaDS_14_Tfpessoa_ie), 15, "%");
            lV132WWContratadaDS_16_Tfpessoa_telefone = StringUtil.PadR( StringUtil.RTrim( AV132WWContratadaDS_16_Tfpessoa_telefone), 15, "%");
            /* Using cursor H00355 */
            pr_default.execute(3, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, AV6WWPContext.gxTpr_Contratada_codigo, lV120WWContratadaDS_4_Contratada_pessoanom1, lV121WWContratadaDS_5_Contratada_pessoacnpj1, lV124WWContratadaDS_8_Contratada_pessoanom2, lV125WWContratadaDS_9_Contratada_pessoacnpj2, lV126WWContratadaDS_10_Tfcontratada_pessoanom, AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel, lV128WWContratadaDS_12_Tfcontratada_pessoacnpj, AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel, lV130WWContratadaDS_14_Tfpessoa_ie, AV131WWContratadaDS_15_Tfpessoa_ie_sel, lV132WWContratadaDS_16_Tfpessoa_telefone, AV133WWContratadaDS_17_Tfpessoa_telefone_sel, AV134WWContratadaDS_18_Tfcontratada_ss, AV135WWContratadaDS_19_Tfcontratada_ss_to, AV136WWContratadaDS_20_Tfcontratada_os, AV137WWContratadaDS_21_Tfcontratada_os_to, AV138WWContratadaDS_22_Tfcontratada_lote, AV139WWContratadaDS_23_Tfcontratada_lote_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_77_idx = 1;
            while ( ( (pr_default.getStatus(3) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A43Contratada_Ativo = H00355_A43Contratada_Ativo[0];
               A530Contratada_Lote = H00355_A530Contratada_Lote[0];
               n530Contratada_Lote = H00355_n530Contratada_Lote[0];
               A524Contratada_OS = H00355_A524Contratada_OS[0];
               n524Contratada_OS = H00355_n524Contratada_OS[0];
               A1451Contratada_SS = H00355_A1451Contratada_SS[0];
               n1451Contratada_SS = H00355_n1451Contratada_SS[0];
               A522Pessoa_Telefone = H00355_A522Pessoa_Telefone[0];
               n522Pessoa_Telefone = H00355_n522Pessoa_Telefone[0];
               A518Pessoa_IE = H00355_A518Pessoa_IE[0];
               n518Pessoa_IE = H00355_n518Pessoa_IE[0];
               A42Contratada_PessoaCNPJ = H00355_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H00355_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H00355_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00355_n41Contratada_PessoaNom[0];
               A40Contratada_PessoaCod = H00355_A40Contratada_PessoaCod[0];
               A52Contratada_AreaTrabalhoCod = H00355_A52Contratada_AreaTrabalhoCod[0];
               A39Contratada_Codigo = H00355_A39Contratada_Codigo[0];
               A522Pessoa_Telefone = H00355_A522Pessoa_Telefone[0];
               n522Pessoa_Telefone = H00355_n522Pessoa_Telefone[0];
               A518Pessoa_IE = H00355_A518Pessoa_IE[0];
               n518Pessoa_IE = H00355_n518Pessoa_IE[0];
               A42Contratada_PessoaCNPJ = H00355_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H00355_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H00355_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00355_n41Contratada_PessoaNom[0];
               /* Execute user event: E31352 */
               E31352 ();
               pr_default.readNext(3);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(3) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(3);
            wbEnd = 77;
            WB350( ) ;
         }
         nGXsfl_77_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV117WWContratadaDS_1_Contratada_areatrabalhocod = AV42Contratada_AreaTrabalhoCod;
         AV118WWContratadaDS_2_Contratada_codigo = AV76Contratada_Codigo;
         AV119WWContratadaDS_3_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV120WWContratadaDS_4_Contratada_pessoanom1 = AV17Contratada_PessoaNom1;
         AV121WWContratadaDS_5_Contratada_pessoacnpj1 = AV73Contratada_PessoaCNPJ1;
         AV122WWContratadaDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV123WWContratadaDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV124WWContratadaDS_8_Contratada_pessoanom2 = AV22Contratada_PessoaNom2;
         AV125WWContratadaDS_9_Contratada_pessoacnpj2 = AV74Contratada_PessoaCNPJ2;
         AV126WWContratadaDS_10_Tfcontratada_pessoanom = AV80TFContratada_PessoaNom;
         AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel = AV81TFContratada_PessoaNom_Sel;
         AV128WWContratadaDS_12_Tfcontratada_pessoacnpj = AV84TFContratada_PessoaCNPJ;
         AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel = AV85TFContratada_PessoaCNPJ_Sel;
         AV130WWContratadaDS_14_Tfpessoa_ie = AV88TFPessoa_IE;
         AV131WWContratadaDS_15_Tfpessoa_ie_sel = AV89TFPessoa_IE_Sel;
         AV132WWContratadaDS_16_Tfpessoa_telefone = AV92TFPessoa_Telefone;
         AV133WWContratadaDS_17_Tfpessoa_telefone_sel = AV93TFPessoa_Telefone_Sel;
         AV134WWContratadaDS_18_Tfcontratada_ss = AV96TFContratada_SS;
         AV135WWContratadaDS_19_Tfcontratada_ss_to = AV97TFContratada_SS_To;
         AV136WWContratadaDS_20_Tfcontratada_os = AV100TFContratada_OS;
         AV137WWContratadaDS_21_Tfcontratada_os_to = AV101TFContratada_OS_To;
         AV138WWContratadaDS_22_Tfcontratada_lote = AV104TFContratada_Lote;
         AV139WWContratadaDS_23_Tfcontratada_lote_to = AV105TFContratada_Lote_To;
         AV140WWContratadaDS_24_Tfcontratada_ativo_sel = AV108TFContratada_Ativo_Sel;
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              AV6WWPContext.gxTpr_Contratada_codigo ,
                                              AV119WWContratadaDS_3_Dynamicfiltersselector1 ,
                                              AV120WWContratadaDS_4_Contratada_pessoanom1 ,
                                              AV121WWContratadaDS_5_Contratada_pessoacnpj1 ,
                                              AV122WWContratadaDS_6_Dynamicfiltersenabled2 ,
                                              AV123WWContratadaDS_7_Dynamicfiltersselector2 ,
                                              AV124WWContratadaDS_8_Contratada_pessoanom2 ,
                                              AV125WWContratadaDS_9_Contratada_pessoacnpj2 ,
                                              AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel ,
                                              AV126WWContratadaDS_10_Tfcontratada_pessoanom ,
                                              AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel ,
                                              AV128WWContratadaDS_12_Tfcontratada_pessoacnpj ,
                                              AV131WWContratadaDS_15_Tfpessoa_ie_sel ,
                                              AV130WWContratadaDS_14_Tfpessoa_ie ,
                                              AV133WWContratadaDS_17_Tfpessoa_telefone_sel ,
                                              AV132WWContratadaDS_16_Tfpessoa_telefone ,
                                              AV134WWContratadaDS_18_Tfcontratada_ss ,
                                              AV135WWContratadaDS_19_Tfcontratada_ss_to ,
                                              AV136WWContratadaDS_20_Tfcontratada_os ,
                                              AV137WWContratadaDS_21_Tfcontratada_os_to ,
                                              AV138WWContratadaDS_22_Tfcontratada_lote ,
                                              AV139WWContratadaDS_23_Tfcontratada_lote_to ,
                                              AV140WWContratadaDS_24_Tfcontratada_ativo_sel ,
                                              A39Contratada_Codigo ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A518Pessoa_IE ,
                                              A522Pessoa_Telefone ,
                                              A1451Contratada_SS ,
                                              A524Contratada_OS ,
                                              A530Contratada_Lote ,
                                              A43Contratada_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV120WWContratadaDS_4_Contratada_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV120WWContratadaDS_4_Contratada_pessoanom1), 100, "%");
         lV121WWContratadaDS_5_Contratada_pessoacnpj1 = StringUtil.Concat( StringUtil.RTrim( AV121WWContratadaDS_5_Contratada_pessoacnpj1), "%", "");
         lV124WWContratadaDS_8_Contratada_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV124WWContratadaDS_8_Contratada_pessoanom2), 100, "%");
         lV125WWContratadaDS_9_Contratada_pessoacnpj2 = StringUtil.Concat( StringUtil.RTrim( AV125WWContratadaDS_9_Contratada_pessoacnpj2), "%", "");
         lV126WWContratadaDS_10_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV126WWContratadaDS_10_Tfcontratada_pessoanom), 100, "%");
         lV128WWContratadaDS_12_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV128WWContratadaDS_12_Tfcontratada_pessoacnpj), "%", "");
         lV130WWContratadaDS_14_Tfpessoa_ie = StringUtil.PadR( StringUtil.RTrim( AV130WWContratadaDS_14_Tfpessoa_ie), 15, "%");
         lV132WWContratadaDS_16_Tfpessoa_telefone = StringUtil.PadR( StringUtil.RTrim( AV132WWContratadaDS_16_Tfpessoa_telefone), 15, "%");
         /* Using cursor H00356 */
         pr_default.execute(4, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, AV6WWPContext.gxTpr_Contratada_codigo, lV120WWContratadaDS_4_Contratada_pessoanom1, lV121WWContratadaDS_5_Contratada_pessoacnpj1, lV124WWContratadaDS_8_Contratada_pessoanom2, lV125WWContratadaDS_9_Contratada_pessoacnpj2, lV126WWContratadaDS_10_Tfcontratada_pessoanom, AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel, lV128WWContratadaDS_12_Tfcontratada_pessoacnpj, AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel, lV130WWContratadaDS_14_Tfpessoa_ie, AV131WWContratadaDS_15_Tfpessoa_ie_sel, lV132WWContratadaDS_16_Tfpessoa_telefone, AV133WWContratadaDS_17_Tfpessoa_telefone_sel, AV134WWContratadaDS_18_Tfcontratada_ss, AV135WWContratadaDS_19_Tfcontratada_ss_to, AV136WWContratadaDS_20_Tfcontratada_os, AV137WWContratadaDS_21_Tfcontratada_os_to, AV138WWContratadaDS_22_Tfcontratada_lote, AV139WWContratadaDS_23_Tfcontratada_lote_to});
         GRID_nRecordCount = H00356_AGRID_nRecordCount[0];
         pr_default.close(4);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV117WWContratadaDS_1_Contratada_areatrabalhocod = AV42Contratada_AreaTrabalhoCod;
         AV118WWContratadaDS_2_Contratada_codigo = AV76Contratada_Codigo;
         AV119WWContratadaDS_3_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV120WWContratadaDS_4_Contratada_pessoanom1 = AV17Contratada_PessoaNom1;
         AV121WWContratadaDS_5_Contratada_pessoacnpj1 = AV73Contratada_PessoaCNPJ1;
         AV122WWContratadaDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV123WWContratadaDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV124WWContratadaDS_8_Contratada_pessoanom2 = AV22Contratada_PessoaNom2;
         AV125WWContratadaDS_9_Contratada_pessoacnpj2 = AV74Contratada_PessoaCNPJ2;
         AV126WWContratadaDS_10_Tfcontratada_pessoanom = AV80TFContratada_PessoaNom;
         AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel = AV81TFContratada_PessoaNom_Sel;
         AV128WWContratadaDS_12_Tfcontratada_pessoacnpj = AV84TFContratada_PessoaCNPJ;
         AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel = AV85TFContratada_PessoaCNPJ_Sel;
         AV130WWContratadaDS_14_Tfpessoa_ie = AV88TFPessoa_IE;
         AV131WWContratadaDS_15_Tfpessoa_ie_sel = AV89TFPessoa_IE_Sel;
         AV132WWContratadaDS_16_Tfpessoa_telefone = AV92TFPessoa_Telefone;
         AV133WWContratadaDS_17_Tfpessoa_telefone_sel = AV93TFPessoa_Telefone_Sel;
         AV134WWContratadaDS_18_Tfcontratada_ss = AV96TFContratada_SS;
         AV135WWContratadaDS_19_Tfcontratada_ss_to = AV97TFContratada_SS_To;
         AV136WWContratadaDS_20_Tfcontratada_os = AV100TFContratada_OS;
         AV137WWContratadaDS_21_Tfcontratada_os_to = AV101TFContratada_OS_To;
         AV138WWContratadaDS_22_Tfcontratada_lote = AV104TFContratada_Lote;
         AV139WWContratadaDS_23_Tfcontratada_lote_to = AV105TFContratada_Lote_To;
         AV140WWContratadaDS_24_Tfcontratada_ativo_sel = AV108TFContratada_Ativo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Contratada_PessoaNom1, AV73Contratada_PessoaCNPJ1, AV20DynamicFiltersSelector2, AV22Contratada_PessoaNom2, AV74Contratada_PessoaCNPJ2, AV19DynamicFiltersEnabled2, AV80TFContratada_PessoaNom, AV81TFContratada_PessoaNom_Sel, AV84TFContratada_PessoaCNPJ, AV85TFContratada_PessoaCNPJ_Sel, AV88TFPessoa_IE, AV89TFPessoa_IE_Sel, AV92TFPessoa_Telefone, AV93TFPessoa_Telefone_Sel, AV96TFContratada_SS, AV97TFContratada_SS_To, AV100TFContratada_OS, AV101TFContratada_OS_To, AV104TFContratada_Lote, AV105TFContratada_Lote_To, AV108TFContratada_Ativo_Sel, AV82ddo_Contratada_PessoaNomTitleControlIdToReplace, AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV90ddo_Pessoa_IETitleControlIdToReplace, AV94ddo_Pessoa_TelefoneTitleControlIdToReplace, AV98ddo_Contratada_SSTitleControlIdToReplace, AV102ddo_Contratada_OSTitleControlIdToReplace, AV106ddo_Contratada_LoteTitleControlIdToReplace, AV109ddo_Contratada_AtivoTitleControlIdToReplace, AV42Contratada_AreaTrabalhoCod, AV76Contratada_Codigo, AV144Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A39Contratada_Codigo, A43Contratada_Ativo, AV6WWPContext) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV117WWContratadaDS_1_Contratada_areatrabalhocod = AV42Contratada_AreaTrabalhoCod;
         AV118WWContratadaDS_2_Contratada_codigo = AV76Contratada_Codigo;
         AV119WWContratadaDS_3_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV120WWContratadaDS_4_Contratada_pessoanom1 = AV17Contratada_PessoaNom1;
         AV121WWContratadaDS_5_Contratada_pessoacnpj1 = AV73Contratada_PessoaCNPJ1;
         AV122WWContratadaDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV123WWContratadaDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV124WWContratadaDS_8_Contratada_pessoanom2 = AV22Contratada_PessoaNom2;
         AV125WWContratadaDS_9_Contratada_pessoacnpj2 = AV74Contratada_PessoaCNPJ2;
         AV126WWContratadaDS_10_Tfcontratada_pessoanom = AV80TFContratada_PessoaNom;
         AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel = AV81TFContratada_PessoaNom_Sel;
         AV128WWContratadaDS_12_Tfcontratada_pessoacnpj = AV84TFContratada_PessoaCNPJ;
         AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel = AV85TFContratada_PessoaCNPJ_Sel;
         AV130WWContratadaDS_14_Tfpessoa_ie = AV88TFPessoa_IE;
         AV131WWContratadaDS_15_Tfpessoa_ie_sel = AV89TFPessoa_IE_Sel;
         AV132WWContratadaDS_16_Tfpessoa_telefone = AV92TFPessoa_Telefone;
         AV133WWContratadaDS_17_Tfpessoa_telefone_sel = AV93TFPessoa_Telefone_Sel;
         AV134WWContratadaDS_18_Tfcontratada_ss = AV96TFContratada_SS;
         AV135WWContratadaDS_19_Tfcontratada_ss_to = AV97TFContratada_SS_To;
         AV136WWContratadaDS_20_Tfcontratada_os = AV100TFContratada_OS;
         AV137WWContratadaDS_21_Tfcontratada_os_to = AV101TFContratada_OS_To;
         AV138WWContratadaDS_22_Tfcontratada_lote = AV104TFContratada_Lote;
         AV139WWContratadaDS_23_Tfcontratada_lote_to = AV105TFContratada_Lote_To;
         AV140WWContratadaDS_24_Tfcontratada_ativo_sel = AV108TFContratada_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Contratada_PessoaNom1, AV73Contratada_PessoaCNPJ1, AV20DynamicFiltersSelector2, AV22Contratada_PessoaNom2, AV74Contratada_PessoaCNPJ2, AV19DynamicFiltersEnabled2, AV80TFContratada_PessoaNom, AV81TFContratada_PessoaNom_Sel, AV84TFContratada_PessoaCNPJ, AV85TFContratada_PessoaCNPJ_Sel, AV88TFPessoa_IE, AV89TFPessoa_IE_Sel, AV92TFPessoa_Telefone, AV93TFPessoa_Telefone_Sel, AV96TFContratada_SS, AV97TFContratada_SS_To, AV100TFContratada_OS, AV101TFContratada_OS_To, AV104TFContratada_Lote, AV105TFContratada_Lote_To, AV108TFContratada_Ativo_Sel, AV82ddo_Contratada_PessoaNomTitleControlIdToReplace, AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV90ddo_Pessoa_IETitleControlIdToReplace, AV94ddo_Pessoa_TelefoneTitleControlIdToReplace, AV98ddo_Contratada_SSTitleControlIdToReplace, AV102ddo_Contratada_OSTitleControlIdToReplace, AV106ddo_Contratada_LoteTitleControlIdToReplace, AV109ddo_Contratada_AtivoTitleControlIdToReplace, AV42Contratada_AreaTrabalhoCod, AV76Contratada_Codigo, AV144Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A39Contratada_Codigo, A43Contratada_Ativo, AV6WWPContext) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV117WWContratadaDS_1_Contratada_areatrabalhocod = AV42Contratada_AreaTrabalhoCod;
         AV118WWContratadaDS_2_Contratada_codigo = AV76Contratada_Codigo;
         AV119WWContratadaDS_3_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV120WWContratadaDS_4_Contratada_pessoanom1 = AV17Contratada_PessoaNom1;
         AV121WWContratadaDS_5_Contratada_pessoacnpj1 = AV73Contratada_PessoaCNPJ1;
         AV122WWContratadaDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV123WWContratadaDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV124WWContratadaDS_8_Contratada_pessoanom2 = AV22Contratada_PessoaNom2;
         AV125WWContratadaDS_9_Contratada_pessoacnpj2 = AV74Contratada_PessoaCNPJ2;
         AV126WWContratadaDS_10_Tfcontratada_pessoanom = AV80TFContratada_PessoaNom;
         AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel = AV81TFContratada_PessoaNom_Sel;
         AV128WWContratadaDS_12_Tfcontratada_pessoacnpj = AV84TFContratada_PessoaCNPJ;
         AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel = AV85TFContratada_PessoaCNPJ_Sel;
         AV130WWContratadaDS_14_Tfpessoa_ie = AV88TFPessoa_IE;
         AV131WWContratadaDS_15_Tfpessoa_ie_sel = AV89TFPessoa_IE_Sel;
         AV132WWContratadaDS_16_Tfpessoa_telefone = AV92TFPessoa_Telefone;
         AV133WWContratadaDS_17_Tfpessoa_telefone_sel = AV93TFPessoa_Telefone_Sel;
         AV134WWContratadaDS_18_Tfcontratada_ss = AV96TFContratada_SS;
         AV135WWContratadaDS_19_Tfcontratada_ss_to = AV97TFContratada_SS_To;
         AV136WWContratadaDS_20_Tfcontratada_os = AV100TFContratada_OS;
         AV137WWContratadaDS_21_Tfcontratada_os_to = AV101TFContratada_OS_To;
         AV138WWContratadaDS_22_Tfcontratada_lote = AV104TFContratada_Lote;
         AV139WWContratadaDS_23_Tfcontratada_lote_to = AV105TFContratada_Lote_To;
         AV140WWContratadaDS_24_Tfcontratada_ativo_sel = AV108TFContratada_Ativo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Contratada_PessoaNom1, AV73Contratada_PessoaCNPJ1, AV20DynamicFiltersSelector2, AV22Contratada_PessoaNom2, AV74Contratada_PessoaCNPJ2, AV19DynamicFiltersEnabled2, AV80TFContratada_PessoaNom, AV81TFContratada_PessoaNom_Sel, AV84TFContratada_PessoaCNPJ, AV85TFContratada_PessoaCNPJ_Sel, AV88TFPessoa_IE, AV89TFPessoa_IE_Sel, AV92TFPessoa_Telefone, AV93TFPessoa_Telefone_Sel, AV96TFContratada_SS, AV97TFContratada_SS_To, AV100TFContratada_OS, AV101TFContratada_OS_To, AV104TFContratada_Lote, AV105TFContratada_Lote_To, AV108TFContratada_Ativo_Sel, AV82ddo_Contratada_PessoaNomTitleControlIdToReplace, AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV90ddo_Pessoa_IETitleControlIdToReplace, AV94ddo_Pessoa_TelefoneTitleControlIdToReplace, AV98ddo_Contratada_SSTitleControlIdToReplace, AV102ddo_Contratada_OSTitleControlIdToReplace, AV106ddo_Contratada_LoteTitleControlIdToReplace, AV109ddo_Contratada_AtivoTitleControlIdToReplace, AV42Contratada_AreaTrabalhoCod, AV76Contratada_Codigo, AV144Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A39Contratada_Codigo, A43Contratada_Ativo, AV6WWPContext) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV117WWContratadaDS_1_Contratada_areatrabalhocod = AV42Contratada_AreaTrabalhoCod;
         AV118WWContratadaDS_2_Contratada_codigo = AV76Contratada_Codigo;
         AV119WWContratadaDS_3_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV120WWContratadaDS_4_Contratada_pessoanom1 = AV17Contratada_PessoaNom1;
         AV121WWContratadaDS_5_Contratada_pessoacnpj1 = AV73Contratada_PessoaCNPJ1;
         AV122WWContratadaDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV123WWContratadaDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV124WWContratadaDS_8_Contratada_pessoanom2 = AV22Contratada_PessoaNom2;
         AV125WWContratadaDS_9_Contratada_pessoacnpj2 = AV74Contratada_PessoaCNPJ2;
         AV126WWContratadaDS_10_Tfcontratada_pessoanom = AV80TFContratada_PessoaNom;
         AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel = AV81TFContratada_PessoaNom_Sel;
         AV128WWContratadaDS_12_Tfcontratada_pessoacnpj = AV84TFContratada_PessoaCNPJ;
         AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel = AV85TFContratada_PessoaCNPJ_Sel;
         AV130WWContratadaDS_14_Tfpessoa_ie = AV88TFPessoa_IE;
         AV131WWContratadaDS_15_Tfpessoa_ie_sel = AV89TFPessoa_IE_Sel;
         AV132WWContratadaDS_16_Tfpessoa_telefone = AV92TFPessoa_Telefone;
         AV133WWContratadaDS_17_Tfpessoa_telefone_sel = AV93TFPessoa_Telefone_Sel;
         AV134WWContratadaDS_18_Tfcontratada_ss = AV96TFContratada_SS;
         AV135WWContratadaDS_19_Tfcontratada_ss_to = AV97TFContratada_SS_To;
         AV136WWContratadaDS_20_Tfcontratada_os = AV100TFContratada_OS;
         AV137WWContratadaDS_21_Tfcontratada_os_to = AV101TFContratada_OS_To;
         AV138WWContratadaDS_22_Tfcontratada_lote = AV104TFContratada_Lote;
         AV139WWContratadaDS_23_Tfcontratada_lote_to = AV105TFContratada_Lote_To;
         AV140WWContratadaDS_24_Tfcontratada_ativo_sel = AV108TFContratada_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Contratada_PessoaNom1, AV73Contratada_PessoaCNPJ1, AV20DynamicFiltersSelector2, AV22Contratada_PessoaNom2, AV74Contratada_PessoaCNPJ2, AV19DynamicFiltersEnabled2, AV80TFContratada_PessoaNom, AV81TFContratada_PessoaNom_Sel, AV84TFContratada_PessoaCNPJ, AV85TFContratada_PessoaCNPJ_Sel, AV88TFPessoa_IE, AV89TFPessoa_IE_Sel, AV92TFPessoa_Telefone, AV93TFPessoa_Telefone_Sel, AV96TFContratada_SS, AV97TFContratada_SS_To, AV100TFContratada_OS, AV101TFContratada_OS_To, AV104TFContratada_Lote, AV105TFContratada_Lote_To, AV108TFContratada_Ativo_Sel, AV82ddo_Contratada_PessoaNomTitleControlIdToReplace, AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV90ddo_Pessoa_IETitleControlIdToReplace, AV94ddo_Pessoa_TelefoneTitleControlIdToReplace, AV98ddo_Contratada_SSTitleControlIdToReplace, AV102ddo_Contratada_OSTitleControlIdToReplace, AV106ddo_Contratada_LoteTitleControlIdToReplace, AV109ddo_Contratada_AtivoTitleControlIdToReplace, AV42Contratada_AreaTrabalhoCod, AV76Contratada_Codigo, AV144Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A39Contratada_Codigo, A43Contratada_Ativo, AV6WWPContext) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV117WWContratadaDS_1_Contratada_areatrabalhocod = AV42Contratada_AreaTrabalhoCod;
         AV118WWContratadaDS_2_Contratada_codigo = AV76Contratada_Codigo;
         AV119WWContratadaDS_3_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV120WWContratadaDS_4_Contratada_pessoanom1 = AV17Contratada_PessoaNom1;
         AV121WWContratadaDS_5_Contratada_pessoacnpj1 = AV73Contratada_PessoaCNPJ1;
         AV122WWContratadaDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV123WWContratadaDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV124WWContratadaDS_8_Contratada_pessoanom2 = AV22Contratada_PessoaNom2;
         AV125WWContratadaDS_9_Contratada_pessoacnpj2 = AV74Contratada_PessoaCNPJ2;
         AV126WWContratadaDS_10_Tfcontratada_pessoanom = AV80TFContratada_PessoaNom;
         AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel = AV81TFContratada_PessoaNom_Sel;
         AV128WWContratadaDS_12_Tfcontratada_pessoacnpj = AV84TFContratada_PessoaCNPJ;
         AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel = AV85TFContratada_PessoaCNPJ_Sel;
         AV130WWContratadaDS_14_Tfpessoa_ie = AV88TFPessoa_IE;
         AV131WWContratadaDS_15_Tfpessoa_ie_sel = AV89TFPessoa_IE_Sel;
         AV132WWContratadaDS_16_Tfpessoa_telefone = AV92TFPessoa_Telefone;
         AV133WWContratadaDS_17_Tfpessoa_telefone_sel = AV93TFPessoa_Telefone_Sel;
         AV134WWContratadaDS_18_Tfcontratada_ss = AV96TFContratada_SS;
         AV135WWContratadaDS_19_Tfcontratada_ss_to = AV97TFContratada_SS_To;
         AV136WWContratadaDS_20_Tfcontratada_os = AV100TFContratada_OS;
         AV137WWContratadaDS_21_Tfcontratada_os_to = AV101TFContratada_OS_To;
         AV138WWContratadaDS_22_Tfcontratada_lote = AV104TFContratada_Lote;
         AV139WWContratadaDS_23_Tfcontratada_lote_to = AV105TFContratada_Lote_To;
         AV140WWContratadaDS_24_Tfcontratada_ativo_sel = AV108TFContratada_Ativo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Contratada_PessoaNom1, AV73Contratada_PessoaCNPJ1, AV20DynamicFiltersSelector2, AV22Contratada_PessoaNom2, AV74Contratada_PessoaCNPJ2, AV19DynamicFiltersEnabled2, AV80TFContratada_PessoaNom, AV81TFContratada_PessoaNom_Sel, AV84TFContratada_PessoaCNPJ, AV85TFContratada_PessoaCNPJ_Sel, AV88TFPessoa_IE, AV89TFPessoa_IE_Sel, AV92TFPessoa_Telefone, AV93TFPessoa_Telefone_Sel, AV96TFContratada_SS, AV97TFContratada_SS_To, AV100TFContratada_OS, AV101TFContratada_OS_To, AV104TFContratada_Lote, AV105TFContratada_Lote_To, AV108TFContratada_Ativo_Sel, AV82ddo_Contratada_PessoaNomTitleControlIdToReplace, AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV90ddo_Pessoa_IETitleControlIdToReplace, AV94ddo_Pessoa_TelefoneTitleControlIdToReplace, AV98ddo_Contratada_SSTitleControlIdToReplace, AV102ddo_Contratada_OSTitleControlIdToReplace, AV106ddo_Contratada_LoteTitleControlIdToReplace, AV109ddo_Contratada_AtivoTitleControlIdToReplace, AV42Contratada_AreaTrabalhoCod, AV76Contratada_Codigo, AV144Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A39Contratada_Codigo, A43Contratada_Ativo, AV6WWPContext) ;
         }
         return (int)(0) ;
      }

      protected void STRUP350( )
      {
         /* Before Start, stand alone formulas. */
         AV144Pgmname = "WWContratada";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E29352 */
         E29352 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV110DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_PESSOANOMTITLEFILTERDATA"), AV79Contratada_PessoaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_PESSOACNPJTITLEFILTERDATA"), AV83Contratada_PessoaCNPJTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPESSOA_IETITLEFILTERDATA"), AV87Pessoa_IETitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPESSOA_TELEFONETITLEFILTERDATA"), AV91Pessoa_TelefoneTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_SSTITLEFILTERDATA"), AV95Contratada_SSTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_OSTITLEFILTERDATA"), AV99Contratada_OSTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_LOTETITLEFILTERDATA"), AV103Contratada_LoteTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_ATIVOTITLEFILTERDATA"), AV107Contratada_AtivoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vNOTIFICATIONINFO"), AV44NotificationInfo);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            dynavContratada_areatrabalhocod.Name = dynavContratada_areatrabalhocod_Internalname;
            dynavContratada_areatrabalhocod.CurrentValue = cgiGet( dynavContratada_areatrabalhocod_Internalname);
            AV42Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( cgiGet( dynavContratada_areatrabalhocod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Contratada_AreaTrabalhoCod), 6, 0)));
            dynavContratada_codigo.Name = dynavContratada_codigo_Internalname;
            dynavContratada_codigo.CurrentValue = cgiGet( dynavContratada_codigo_Internalname);
            AV76Contratada_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratada_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76Contratada_Codigo), 6, 0)));
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV17Contratada_PessoaNom1 = StringUtil.Upper( cgiGet( edtavContratada_pessoanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_PessoaNom1", AV17Contratada_PessoaNom1);
            AV73Contratada_PessoaCNPJ1 = cgiGet( edtavContratada_pessoacnpj1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Contratada_PessoaCNPJ1", AV73Contratada_PessoaCNPJ1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            AV22Contratada_PessoaNom2 = StringUtil.Upper( cgiGet( edtavContratada_pessoanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratada_PessoaNom2", AV22Contratada_PessoaNom2);
            AV74Contratada_PessoaCNPJ2 = cgiGet( edtavContratada_pessoacnpj2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74Contratada_PessoaCNPJ2", AV74Contratada_PessoaCNPJ2);
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV80TFContratada_PessoaNom = StringUtil.Upper( cgiGet( edtavTfcontratada_pessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratada_PessoaNom", AV80TFContratada_PessoaNom);
            AV81TFContratada_PessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontratada_pessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratada_PessoaNom_Sel", AV81TFContratada_PessoaNom_Sel);
            AV84TFContratada_PessoaCNPJ = cgiGet( edtavTfcontratada_pessoacnpj_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContratada_PessoaCNPJ", AV84TFContratada_PessoaCNPJ);
            AV85TFContratada_PessoaCNPJ_Sel = cgiGet( edtavTfcontratada_pessoacnpj_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratada_PessoaCNPJ_Sel", AV85TFContratada_PessoaCNPJ_Sel);
            AV88TFPessoa_IE = cgiGet( edtavTfpessoa_ie_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFPessoa_IE", AV88TFPessoa_IE);
            AV89TFPessoa_IE_Sel = cgiGet( edtavTfpessoa_ie_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFPessoa_IE_Sel", AV89TFPessoa_IE_Sel);
            AV92TFPessoa_Telefone = cgiGet( edtavTfpessoa_telefone_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFPessoa_Telefone", AV92TFPessoa_Telefone);
            AV93TFPessoa_Telefone_Sel = cgiGet( edtavTfpessoa_telefone_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93TFPessoa_Telefone_Sel", AV93TFPessoa_Telefone_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_ss_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_ss_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADA_SS");
               GX_FocusControl = edtavTfcontratada_ss_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV96TFContratada_SS = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96TFContratada_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96TFContratada_SS), 8, 0)));
            }
            else
            {
               AV96TFContratada_SS = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratada_ss_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96TFContratada_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96TFContratada_SS), 8, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_ss_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_ss_to_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADA_SS_TO");
               GX_FocusControl = edtavTfcontratada_ss_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV97TFContratada_SS_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97TFContratada_SS_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV97TFContratada_SS_To), 8, 0)));
            }
            else
            {
               AV97TFContratada_SS_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratada_ss_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97TFContratada_SS_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV97TFContratada_SS_To), 8, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_os_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_os_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADA_OS");
               GX_FocusControl = edtavTfcontratada_os_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV100TFContratada_OS = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFContratada_OS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100TFContratada_OS), 8, 0)));
            }
            else
            {
               AV100TFContratada_OS = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratada_os_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFContratada_OS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100TFContratada_OS), 8, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_os_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_os_to_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADA_OS_TO");
               GX_FocusControl = edtavTfcontratada_os_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV101TFContratada_OS_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFContratada_OS_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV101TFContratada_OS_To), 8, 0)));
            }
            else
            {
               AV101TFContratada_OS_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratada_os_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFContratada_OS_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV101TFContratada_OS_To), 8, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_lote_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_lote_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADA_LOTE");
               GX_FocusControl = edtavTfcontratada_lote_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV104TFContratada_Lote = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFContratada_Lote", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104TFContratada_Lote), 4, 0)));
            }
            else
            {
               AV104TFContratada_Lote = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratada_lote_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFContratada_Lote", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104TFContratada_Lote), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_lote_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_lote_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADA_LOTE_TO");
               GX_FocusControl = edtavTfcontratada_lote_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV105TFContratada_Lote_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFContratada_Lote_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFContratada_Lote_To), 4, 0)));
            }
            else
            {
               AV105TFContratada_Lote_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratada_lote_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFContratada_Lote_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFContratada_Lote_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADA_ATIVO_SEL");
               GX_FocusControl = edtavTfcontratada_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV108TFContratada_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContratada_Ativo_Sel", StringUtil.Str( (decimal)(AV108TFContratada_Ativo_Sel), 1, 0));
            }
            else
            {
               AV108TFContratada_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratada_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContratada_Ativo_Sel", StringUtil.Str( (decimal)(AV108TFContratada_Ativo_Sel), 1, 0));
            }
            AV82ddo_Contratada_PessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ddo_Contratada_PessoaNomTitleControlIdToReplace", AV82ddo_Contratada_PessoaNomTitleControlIdToReplace);
            AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace = cgiGet( edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace", AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace);
            AV90ddo_Pessoa_IETitleControlIdToReplace = cgiGet( edtavDdo_pessoa_ietitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ddo_Pessoa_IETitleControlIdToReplace", AV90ddo_Pessoa_IETitleControlIdToReplace);
            AV94ddo_Pessoa_TelefoneTitleControlIdToReplace = cgiGet( edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94ddo_Pessoa_TelefoneTitleControlIdToReplace", AV94ddo_Pessoa_TelefoneTitleControlIdToReplace);
            AV98ddo_Contratada_SSTitleControlIdToReplace = cgiGet( edtavDdo_contratada_sstitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98ddo_Contratada_SSTitleControlIdToReplace", AV98ddo_Contratada_SSTitleControlIdToReplace);
            AV102ddo_Contratada_OSTitleControlIdToReplace = cgiGet( edtavDdo_contratada_ostitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ddo_Contratada_OSTitleControlIdToReplace", AV102ddo_Contratada_OSTitleControlIdToReplace);
            AV106ddo_Contratada_LoteTitleControlIdToReplace = cgiGet( edtavDdo_contratada_lotetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106ddo_Contratada_LoteTitleControlIdToReplace", AV106ddo_Contratada_LoteTitleControlIdToReplace);
            AV109ddo_Contratada_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_contratada_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109ddo_Contratada_AtivoTitleControlIdToReplace", AV109ddo_Contratada_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_77 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_77"), ",", "."));
            AV112GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV113GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratada_pessoanom_Caption = cgiGet( "DDO_CONTRATADA_PESSOANOM_Caption");
            Ddo_contratada_pessoanom_Tooltip = cgiGet( "DDO_CONTRATADA_PESSOANOM_Tooltip");
            Ddo_contratada_pessoanom_Cls = cgiGet( "DDO_CONTRATADA_PESSOANOM_Cls");
            Ddo_contratada_pessoanom_Filteredtext_set = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filteredtext_set");
            Ddo_contratada_pessoanom_Selectedvalue_set = cgiGet( "DDO_CONTRATADA_PESSOANOM_Selectedvalue_set");
            Ddo_contratada_pessoanom_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Dropdownoptionstype");
            Ddo_contratada_pessoanom_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_PESSOANOM_Titlecontrolidtoreplace");
            Ddo_contratada_pessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includesortasc"));
            Ddo_contratada_pessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includesortdsc"));
            Ddo_contratada_pessoanom_Sortedstatus = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortedstatus");
            Ddo_contratada_pessoanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includefilter"));
            Ddo_contratada_pessoanom_Filtertype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filtertype");
            Ddo_contratada_pessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Filterisrange"));
            Ddo_contratada_pessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includedatalist"));
            Ddo_contratada_pessoanom_Datalisttype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalisttype");
            Ddo_contratada_pessoanom_Datalistproc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalistproc");
            Ddo_contratada_pessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratada_pessoanom_Sortasc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortasc");
            Ddo_contratada_pessoanom_Sortdsc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortdsc");
            Ddo_contratada_pessoanom_Loadingdata = cgiGet( "DDO_CONTRATADA_PESSOANOM_Loadingdata");
            Ddo_contratada_pessoanom_Cleanfilter = cgiGet( "DDO_CONTRATADA_PESSOANOM_Cleanfilter");
            Ddo_contratada_pessoanom_Noresultsfound = cgiGet( "DDO_CONTRATADA_PESSOANOM_Noresultsfound");
            Ddo_contratada_pessoanom_Searchbuttontext = cgiGet( "DDO_CONTRATADA_PESSOANOM_Searchbuttontext");
            Ddo_contratada_pessoacnpj_Caption = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Caption");
            Ddo_contratada_pessoacnpj_Tooltip = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Tooltip");
            Ddo_contratada_pessoacnpj_Cls = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Cls");
            Ddo_contratada_pessoacnpj_Filteredtext_set = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_set");
            Ddo_contratada_pessoacnpj_Selectedvalue_set = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_set");
            Ddo_contratada_pessoacnpj_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Dropdownoptionstype");
            Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Titlecontrolidtoreplace");
            Ddo_contratada_pessoacnpj_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includesortasc"));
            Ddo_contratada_pessoacnpj_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includesortdsc"));
            Ddo_contratada_pessoacnpj_Sortedstatus = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Sortedstatus");
            Ddo_contratada_pessoacnpj_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includefilter"));
            Ddo_contratada_pessoacnpj_Filtertype = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filtertype");
            Ddo_contratada_pessoacnpj_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filterisrange"));
            Ddo_contratada_pessoacnpj_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includedatalist"));
            Ddo_contratada_pessoacnpj_Datalisttype = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalisttype");
            Ddo_contratada_pessoacnpj_Datalistproc = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalistproc");
            Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratada_pessoacnpj_Sortasc = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Sortasc");
            Ddo_contratada_pessoacnpj_Sortdsc = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Sortdsc");
            Ddo_contratada_pessoacnpj_Loadingdata = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Loadingdata");
            Ddo_contratada_pessoacnpj_Cleanfilter = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Cleanfilter");
            Ddo_contratada_pessoacnpj_Noresultsfound = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Noresultsfound");
            Ddo_contratada_pessoacnpj_Searchbuttontext = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Searchbuttontext");
            Ddo_pessoa_ie_Caption = cgiGet( "DDO_PESSOA_IE_Caption");
            Ddo_pessoa_ie_Tooltip = cgiGet( "DDO_PESSOA_IE_Tooltip");
            Ddo_pessoa_ie_Cls = cgiGet( "DDO_PESSOA_IE_Cls");
            Ddo_pessoa_ie_Filteredtext_set = cgiGet( "DDO_PESSOA_IE_Filteredtext_set");
            Ddo_pessoa_ie_Selectedvalue_set = cgiGet( "DDO_PESSOA_IE_Selectedvalue_set");
            Ddo_pessoa_ie_Dropdownoptionstype = cgiGet( "DDO_PESSOA_IE_Dropdownoptionstype");
            Ddo_pessoa_ie_Titlecontrolidtoreplace = cgiGet( "DDO_PESSOA_IE_Titlecontrolidtoreplace");
            Ddo_pessoa_ie_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_IE_Includesortasc"));
            Ddo_pessoa_ie_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_IE_Includesortdsc"));
            Ddo_pessoa_ie_Sortedstatus = cgiGet( "DDO_PESSOA_IE_Sortedstatus");
            Ddo_pessoa_ie_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_IE_Includefilter"));
            Ddo_pessoa_ie_Filtertype = cgiGet( "DDO_PESSOA_IE_Filtertype");
            Ddo_pessoa_ie_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_IE_Filterisrange"));
            Ddo_pessoa_ie_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_IE_Includedatalist"));
            Ddo_pessoa_ie_Datalisttype = cgiGet( "DDO_PESSOA_IE_Datalisttype");
            Ddo_pessoa_ie_Datalistproc = cgiGet( "DDO_PESSOA_IE_Datalistproc");
            Ddo_pessoa_ie_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PESSOA_IE_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_pessoa_ie_Sortasc = cgiGet( "DDO_PESSOA_IE_Sortasc");
            Ddo_pessoa_ie_Sortdsc = cgiGet( "DDO_PESSOA_IE_Sortdsc");
            Ddo_pessoa_ie_Loadingdata = cgiGet( "DDO_PESSOA_IE_Loadingdata");
            Ddo_pessoa_ie_Cleanfilter = cgiGet( "DDO_PESSOA_IE_Cleanfilter");
            Ddo_pessoa_ie_Noresultsfound = cgiGet( "DDO_PESSOA_IE_Noresultsfound");
            Ddo_pessoa_ie_Searchbuttontext = cgiGet( "DDO_PESSOA_IE_Searchbuttontext");
            Ddo_pessoa_telefone_Caption = cgiGet( "DDO_PESSOA_TELEFONE_Caption");
            Ddo_pessoa_telefone_Tooltip = cgiGet( "DDO_PESSOA_TELEFONE_Tooltip");
            Ddo_pessoa_telefone_Cls = cgiGet( "DDO_PESSOA_TELEFONE_Cls");
            Ddo_pessoa_telefone_Filteredtext_set = cgiGet( "DDO_PESSOA_TELEFONE_Filteredtext_set");
            Ddo_pessoa_telefone_Selectedvalue_set = cgiGet( "DDO_PESSOA_TELEFONE_Selectedvalue_set");
            Ddo_pessoa_telefone_Dropdownoptionstype = cgiGet( "DDO_PESSOA_TELEFONE_Dropdownoptionstype");
            Ddo_pessoa_telefone_Titlecontrolidtoreplace = cgiGet( "DDO_PESSOA_TELEFONE_Titlecontrolidtoreplace");
            Ddo_pessoa_telefone_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_TELEFONE_Includesortasc"));
            Ddo_pessoa_telefone_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_TELEFONE_Includesortdsc"));
            Ddo_pessoa_telefone_Sortedstatus = cgiGet( "DDO_PESSOA_TELEFONE_Sortedstatus");
            Ddo_pessoa_telefone_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_TELEFONE_Includefilter"));
            Ddo_pessoa_telefone_Filtertype = cgiGet( "DDO_PESSOA_TELEFONE_Filtertype");
            Ddo_pessoa_telefone_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_TELEFONE_Filterisrange"));
            Ddo_pessoa_telefone_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PESSOA_TELEFONE_Includedatalist"));
            Ddo_pessoa_telefone_Datalisttype = cgiGet( "DDO_PESSOA_TELEFONE_Datalisttype");
            Ddo_pessoa_telefone_Datalistproc = cgiGet( "DDO_PESSOA_TELEFONE_Datalistproc");
            Ddo_pessoa_telefone_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PESSOA_TELEFONE_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_pessoa_telefone_Sortasc = cgiGet( "DDO_PESSOA_TELEFONE_Sortasc");
            Ddo_pessoa_telefone_Sortdsc = cgiGet( "DDO_PESSOA_TELEFONE_Sortdsc");
            Ddo_pessoa_telefone_Loadingdata = cgiGet( "DDO_PESSOA_TELEFONE_Loadingdata");
            Ddo_pessoa_telefone_Cleanfilter = cgiGet( "DDO_PESSOA_TELEFONE_Cleanfilter");
            Ddo_pessoa_telefone_Noresultsfound = cgiGet( "DDO_PESSOA_TELEFONE_Noresultsfound");
            Ddo_pessoa_telefone_Searchbuttontext = cgiGet( "DDO_PESSOA_TELEFONE_Searchbuttontext");
            Ddo_contratada_ss_Caption = cgiGet( "DDO_CONTRATADA_SS_Caption");
            Ddo_contratada_ss_Tooltip = cgiGet( "DDO_CONTRATADA_SS_Tooltip");
            Ddo_contratada_ss_Cls = cgiGet( "DDO_CONTRATADA_SS_Cls");
            Ddo_contratada_ss_Filteredtext_set = cgiGet( "DDO_CONTRATADA_SS_Filteredtext_set");
            Ddo_contratada_ss_Filteredtextto_set = cgiGet( "DDO_CONTRATADA_SS_Filteredtextto_set");
            Ddo_contratada_ss_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_SS_Dropdownoptionstype");
            Ddo_contratada_ss_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_SS_Titlecontrolidtoreplace");
            Ddo_contratada_ss_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_SS_Includesortasc"));
            Ddo_contratada_ss_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_SS_Includesortdsc"));
            Ddo_contratada_ss_Sortedstatus = cgiGet( "DDO_CONTRATADA_SS_Sortedstatus");
            Ddo_contratada_ss_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_SS_Includefilter"));
            Ddo_contratada_ss_Filtertype = cgiGet( "DDO_CONTRATADA_SS_Filtertype");
            Ddo_contratada_ss_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_SS_Filterisrange"));
            Ddo_contratada_ss_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_SS_Includedatalist"));
            Ddo_contratada_ss_Sortasc = cgiGet( "DDO_CONTRATADA_SS_Sortasc");
            Ddo_contratada_ss_Sortdsc = cgiGet( "DDO_CONTRATADA_SS_Sortdsc");
            Ddo_contratada_ss_Cleanfilter = cgiGet( "DDO_CONTRATADA_SS_Cleanfilter");
            Ddo_contratada_ss_Rangefilterfrom = cgiGet( "DDO_CONTRATADA_SS_Rangefilterfrom");
            Ddo_contratada_ss_Rangefilterto = cgiGet( "DDO_CONTRATADA_SS_Rangefilterto");
            Ddo_contratada_ss_Searchbuttontext = cgiGet( "DDO_CONTRATADA_SS_Searchbuttontext");
            Ddo_contratada_os_Caption = cgiGet( "DDO_CONTRATADA_OS_Caption");
            Ddo_contratada_os_Tooltip = cgiGet( "DDO_CONTRATADA_OS_Tooltip");
            Ddo_contratada_os_Cls = cgiGet( "DDO_CONTRATADA_OS_Cls");
            Ddo_contratada_os_Filteredtext_set = cgiGet( "DDO_CONTRATADA_OS_Filteredtext_set");
            Ddo_contratada_os_Filteredtextto_set = cgiGet( "DDO_CONTRATADA_OS_Filteredtextto_set");
            Ddo_contratada_os_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_OS_Dropdownoptionstype");
            Ddo_contratada_os_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_OS_Titlecontrolidtoreplace");
            Ddo_contratada_os_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_OS_Includesortasc"));
            Ddo_contratada_os_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_OS_Includesortdsc"));
            Ddo_contratada_os_Sortedstatus = cgiGet( "DDO_CONTRATADA_OS_Sortedstatus");
            Ddo_contratada_os_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_OS_Includefilter"));
            Ddo_contratada_os_Filtertype = cgiGet( "DDO_CONTRATADA_OS_Filtertype");
            Ddo_contratada_os_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_OS_Filterisrange"));
            Ddo_contratada_os_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_OS_Includedatalist"));
            Ddo_contratada_os_Sortasc = cgiGet( "DDO_CONTRATADA_OS_Sortasc");
            Ddo_contratada_os_Sortdsc = cgiGet( "DDO_CONTRATADA_OS_Sortdsc");
            Ddo_contratada_os_Cleanfilter = cgiGet( "DDO_CONTRATADA_OS_Cleanfilter");
            Ddo_contratada_os_Rangefilterfrom = cgiGet( "DDO_CONTRATADA_OS_Rangefilterfrom");
            Ddo_contratada_os_Rangefilterto = cgiGet( "DDO_CONTRATADA_OS_Rangefilterto");
            Ddo_contratada_os_Searchbuttontext = cgiGet( "DDO_CONTRATADA_OS_Searchbuttontext");
            Ddo_contratada_lote_Caption = cgiGet( "DDO_CONTRATADA_LOTE_Caption");
            Ddo_contratada_lote_Tooltip = cgiGet( "DDO_CONTRATADA_LOTE_Tooltip");
            Ddo_contratada_lote_Cls = cgiGet( "DDO_CONTRATADA_LOTE_Cls");
            Ddo_contratada_lote_Filteredtext_set = cgiGet( "DDO_CONTRATADA_LOTE_Filteredtext_set");
            Ddo_contratada_lote_Filteredtextto_set = cgiGet( "DDO_CONTRATADA_LOTE_Filteredtextto_set");
            Ddo_contratada_lote_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_LOTE_Dropdownoptionstype");
            Ddo_contratada_lote_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_LOTE_Titlecontrolidtoreplace");
            Ddo_contratada_lote_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_LOTE_Includesortasc"));
            Ddo_contratada_lote_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_LOTE_Includesortdsc"));
            Ddo_contratada_lote_Sortedstatus = cgiGet( "DDO_CONTRATADA_LOTE_Sortedstatus");
            Ddo_contratada_lote_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_LOTE_Includefilter"));
            Ddo_contratada_lote_Filtertype = cgiGet( "DDO_CONTRATADA_LOTE_Filtertype");
            Ddo_contratada_lote_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_LOTE_Filterisrange"));
            Ddo_contratada_lote_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_LOTE_Includedatalist"));
            Ddo_contratada_lote_Sortasc = cgiGet( "DDO_CONTRATADA_LOTE_Sortasc");
            Ddo_contratada_lote_Sortdsc = cgiGet( "DDO_CONTRATADA_LOTE_Sortdsc");
            Ddo_contratada_lote_Cleanfilter = cgiGet( "DDO_CONTRATADA_LOTE_Cleanfilter");
            Ddo_contratada_lote_Rangefilterfrom = cgiGet( "DDO_CONTRATADA_LOTE_Rangefilterfrom");
            Ddo_contratada_lote_Rangefilterto = cgiGet( "DDO_CONTRATADA_LOTE_Rangefilterto");
            Ddo_contratada_lote_Searchbuttontext = cgiGet( "DDO_CONTRATADA_LOTE_Searchbuttontext");
            Ddo_contratada_ativo_Caption = cgiGet( "DDO_CONTRATADA_ATIVO_Caption");
            Ddo_contratada_ativo_Tooltip = cgiGet( "DDO_CONTRATADA_ATIVO_Tooltip");
            Ddo_contratada_ativo_Cls = cgiGet( "DDO_CONTRATADA_ATIVO_Cls");
            Ddo_contratada_ativo_Selectedvalue_set = cgiGet( "DDO_CONTRATADA_ATIVO_Selectedvalue_set");
            Ddo_contratada_ativo_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_ATIVO_Dropdownoptionstype");
            Ddo_contratada_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_ATIVO_Titlecontrolidtoreplace");
            Ddo_contratada_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_ATIVO_Includesortasc"));
            Ddo_contratada_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_ATIVO_Includesortdsc"));
            Ddo_contratada_ativo_Sortedstatus = cgiGet( "DDO_CONTRATADA_ATIVO_Sortedstatus");
            Ddo_contratada_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_ATIVO_Includefilter"));
            Ddo_contratada_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_ATIVO_Includedatalist"));
            Ddo_contratada_ativo_Datalisttype = cgiGet( "DDO_CONTRATADA_ATIVO_Datalisttype");
            Ddo_contratada_ativo_Datalistfixedvalues = cgiGet( "DDO_CONTRATADA_ATIVO_Datalistfixedvalues");
            Ddo_contratada_ativo_Sortasc = cgiGet( "DDO_CONTRATADA_ATIVO_Sortasc");
            Ddo_contratada_ativo_Sortdsc = cgiGet( "DDO_CONTRATADA_ATIVO_Sortdsc");
            Ddo_contratada_ativo_Cleanfilter = cgiGet( "DDO_CONTRATADA_ATIVO_Cleanfilter");
            Ddo_contratada_ativo_Searchbuttontext = cgiGet( "DDO_CONTRATADA_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratada_pessoanom_Activeeventkey = cgiGet( "DDO_CONTRATADA_PESSOANOM_Activeeventkey");
            Ddo_contratada_pessoanom_Filteredtext_get = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filteredtext_get");
            Ddo_contratada_pessoanom_Selectedvalue_get = cgiGet( "DDO_CONTRATADA_PESSOANOM_Selectedvalue_get");
            Ddo_contratada_pessoacnpj_Activeeventkey = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Activeeventkey");
            Ddo_contratada_pessoacnpj_Filteredtext_get = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_get");
            Ddo_contratada_pessoacnpj_Selectedvalue_get = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_get");
            Ddo_pessoa_ie_Activeeventkey = cgiGet( "DDO_PESSOA_IE_Activeeventkey");
            Ddo_pessoa_ie_Filteredtext_get = cgiGet( "DDO_PESSOA_IE_Filteredtext_get");
            Ddo_pessoa_ie_Selectedvalue_get = cgiGet( "DDO_PESSOA_IE_Selectedvalue_get");
            Ddo_pessoa_telefone_Activeeventkey = cgiGet( "DDO_PESSOA_TELEFONE_Activeeventkey");
            Ddo_pessoa_telefone_Filteredtext_get = cgiGet( "DDO_PESSOA_TELEFONE_Filteredtext_get");
            Ddo_pessoa_telefone_Selectedvalue_get = cgiGet( "DDO_PESSOA_TELEFONE_Selectedvalue_get");
            Ddo_contratada_ss_Activeeventkey = cgiGet( "DDO_CONTRATADA_SS_Activeeventkey");
            Ddo_contratada_ss_Filteredtext_get = cgiGet( "DDO_CONTRATADA_SS_Filteredtext_get");
            Ddo_contratada_ss_Filteredtextto_get = cgiGet( "DDO_CONTRATADA_SS_Filteredtextto_get");
            Ddo_contratada_os_Activeeventkey = cgiGet( "DDO_CONTRATADA_OS_Activeeventkey");
            Ddo_contratada_os_Filteredtext_get = cgiGet( "DDO_CONTRATADA_OS_Filteredtext_get");
            Ddo_contratada_os_Filteredtextto_get = cgiGet( "DDO_CONTRATADA_OS_Filteredtextto_get");
            Ddo_contratada_lote_Activeeventkey = cgiGet( "DDO_CONTRATADA_LOTE_Activeeventkey");
            Ddo_contratada_lote_Filteredtext_get = cgiGet( "DDO_CONTRATADA_LOTE_Filteredtext_get");
            Ddo_contratada_lote_Filteredtextto_get = cgiGet( "DDO_CONTRATADA_LOTE_Filteredtextto_get");
            Ddo_contratada_ativo_Activeeventkey = cgiGet( "DDO_CONTRATADA_ATIVO_Activeeventkey");
            Ddo_contratada_ativo_Selectedvalue_get = cgiGet( "DDO_CONTRATADA_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM1"), AV17Contratada_PessoaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOACNPJ1"), AV73Contratada_PessoaCNPJ1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM2"), AV22Contratada_PessoaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOACNPJ2"), AV74Contratada_PessoaCNPJ2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM"), AV80TFContratada_PessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM_SEL"), AV81TFContratada_PessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ"), AV84TFContratada_PessoaCNPJ) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ_SEL"), AV85TFContratada_PessoaCNPJ_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_IE"), AV88TFPessoa_IE) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_IE_SEL"), AV89TFPessoa_IE_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_TELEFONE"), AV92TFPessoa_Telefone) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOA_TELEFONE_SEL"), AV93TFPessoa_Telefone_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_SS"), ",", ".") != Convert.ToDecimal( AV96TFContratada_SS )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_SS_TO"), ",", ".") != Convert.ToDecimal( AV97TFContratada_SS_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_OS"), ",", ".") != Convert.ToDecimal( AV100TFContratada_OS )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_OS_TO"), ",", ".") != Convert.ToDecimal( AV101TFContratada_OS_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_LOTE"), ",", ".") != Convert.ToDecimal( AV104TFContratada_Lote )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_LOTE_TO"), ",", ".") != Convert.ToDecimal( AV105TFContratada_Lote_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV108TFContratada_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E29352 */
         E29352 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E29352( )
      {
         /* Start Routine */
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTRATADA_PESSOANOM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "CONTRATADA_PESSOANOM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTfcontratada_pessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoanom_Visible), 5, 0)));
         edtavTfcontratada_pessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoanom_sel_Visible), 5, 0)));
         edtavTfcontratada_pessoacnpj_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoacnpj_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoacnpj_Visible), 5, 0)));
         edtavTfcontratada_pessoacnpj_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoacnpj_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoacnpj_sel_Visible), 5, 0)));
         edtavTfpessoa_ie_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_ie_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_ie_Visible), 5, 0)));
         edtavTfpessoa_ie_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_ie_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_ie_sel_Visible), 5, 0)));
         edtavTfpessoa_telefone_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_telefone_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_telefone_Visible), 5, 0)));
         edtavTfpessoa_telefone_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoa_telefone_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoa_telefone_sel_Visible), 5, 0)));
         edtavTfcontratada_ss_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_ss_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_ss_Visible), 5, 0)));
         edtavTfcontratada_ss_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_ss_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_ss_to_Visible), 5, 0)));
         edtavTfcontratada_os_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_os_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_os_Visible), 5, 0)));
         edtavTfcontratada_os_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_os_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_os_to_Visible), 5, 0)));
         edtavTfcontratada_lote_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_lote_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_lote_Visible), 5, 0)));
         edtavTfcontratada_lote_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_lote_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_lote_to_Visible), 5, 0)));
         edtavTfcontratada_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_ativo_sel_Visible), 5, 0)));
         Ddo_contratada_pessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_PessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "TitleControlIdToReplace", Ddo_contratada_pessoanom_Titlecontrolidtoreplace);
         AV82ddo_Contratada_PessoaNomTitleControlIdToReplace = Ddo_contratada_pessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ddo_Contratada_PessoaNomTitleControlIdToReplace", AV82ddo_Contratada_PessoaNomTitleControlIdToReplace);
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_PessoaCNPJ";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "TitleControlIdToReplace", Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace);
         AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace = Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace", AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace);
         edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_pessoa_ie_Titlecontrolidtoreplace = subGrid_Internalname+"_Pessoa_IE";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ie_Internalname, "TitleControlIdToReplace", Ddo_pessoa_ie_Titlecontrolidtoreplace);
         AV90ddo_Pessoa_IETitleControlIdToReplace = Ddo_pessoa_ie_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ddo_Pessoa_IETitleControlIdToReplace", AV90ddo_Pessoa_IETitleControlIdToReplace);
         edtavDdo_pessoa_ietitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pessoa_ietitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoa_ietitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_pessoa_telefone_Titlecontrolidtoreplace = subGrid_Internalname+"_Pessoa_Telefone";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_telefone_Internalname, "TitleControlIdToReplace", Ddo_pessoa_telefone_Titlecontrolidtoreplace);
         AV94ddo_Pessoa_TelefoneTitleControlIdToReplace = Ddo_pessoa_telefone_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94ddo_Pessoa_TelefoneTitleControlIdToReplace", AV94ddo_Pessoa_TelefoneTitleControlIdToReplace);
         edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_ss_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_SS";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ss_Internalname, "TitleControlIdToReplace", Ddo_contratada_ss_Titlecontrolidtoreplace);
         AV98ddo_Contratada_SSTitleControlIdToReplace = Ddo_contratada_ss_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98ddo_Contratada_SSTitleControlIdToReplace", AV98ddo_Contratada_SSTitleControlIdToReplace);
         edtavDdo_contratada_sstitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_sstitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_sstitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_os_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_OS";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_os_Internalname, "TitleControlIdToReplace", Ddo_contratada_os_Titlecontrolidtoreplace);
         AV102ddo_Contratada_OSTitleControlIdToReplace = Ddo_contratada_os_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ddo_Contratada_OSTitleControlIdToReplace", AV102ddo_Contratada_OSTitleControlIdToReplace);
         edtavDdo_contratada_ostitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_ostitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_ostitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_lote_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_Lote";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_lote_Internalname, "TitleControlIdToReplace", Ddo_contratada_lote_Titlecontrolidtoreplace);
         AV106ddo_Contratada_LoteTitleControlIdToReplace = Ddo_contratada_lote_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106ddo_Contratada_LoteTitleControlIdToReplace", AV106ddo_Contratada_LoteTitleControlIdToReplace);
         edtavDdo_contratada_lotetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_lotetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_lotetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ativo_Internalname, "TitleControlIdToReplace", Ddo_contratada_ativo_Titlecontrolidtoreplace);
         AV109ddo_Contratada_AtivoTitleControlIdToReplace = Ddo_contratada_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109ddo_Contratada_AtivoTitleControlIdToReplace", AV109ddo_Contratada_AtivoTitleControlIdToReplace);
         edtavDdo_contratada_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Contratadas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "CNPJ", 0);
         cmbavOrderedby.addItem("3", "IE", 0);
         cmbavOrderedby.addItem("4", "Telefone", 0);
         cmbavOrderedby.addItem("5", "�lt.SS", 0);
         cmbavOrderedby.addItem("6", "�lt.OS", 0);
         cmbavOrderedby.addItem("7", "�lt.Lote", 0);
         cmbavOrderedby.addItem("8", "Ativo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV110DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV110DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E30352( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV79Contratada_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV83Contratada_PessoaCNPJTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV87Pessoa_IETitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV91Pessoa_TelefoneTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV95Contratada_SSTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV99Contratada_OSTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV103Contratada_LoteTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV107Contratada_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratada_PessoaNom_Titleformat = 2;
         edtContratada_PessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV82ddo_Contratada_PessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Title", edtContratada_PessoaNom_Title);
         edtContratada_PessoaCNPJ_Titleformat = 2;
         edtContratada_PessoaCNPJ_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "CNPJ", AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCNPJ_Internalname, "Title", edtContratada_PessoaCNPJ_Title);
         edtPessoa_IE_Titleformat = 2;
         edtPessoa_IE_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "IE", AV90ddo_Pessoa_IETitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_IE_Internalname, "Title", edtPessoa_IE_Title);
         edtPessoa_Telefone_Titleformat = 2;
         edtPessoa_Telefone_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Telefone", AV94ddo_Pessoa_TelefoneTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_Telefone_Internalname, "Title", edtPessoa_Telefone_Title);
         edtContratada_SS_Titleformat = 2;
         edtContratada_SS_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "�lt.SS", AV98ddo_Contratada_SSTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_SS_Internalname, "Title", edtContratada_SS_Title);
         edtContratada_OS_Titleformat = 2;
         edtContratada_OS_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "�lt.OS", AV102ddo_Contratada_OSTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_OS_Internalname, "Title", edtContratada_OS_Title);
         edtContratada_Lote_Titleformat = 2;
         edtContratada_Lote_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "�lt.Lote", AV106ddo_Contratada_LoteTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_Lote_Internalname, "Title", edtContratada_Lote_Title);
         chkContratada_Ativo_Titleformat = 2;
         chkContratada_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV109ddo_Contratada_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratada_Ativo_Internalname, "Title", chkContratada_Ativo.Title.Text);
         AV112GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV112GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV112GridCurrentPage), 10, 0)));
         AV113GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV113GridPageCount), 10, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Insert&&(AV6WWPContext.gxTpr_Userehcontratante||AV6WWPContext.gxTpr_Userehadministradorgam) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         GXt_boolean2 = false;
         GXt_int3 = AV6WWPContext.gxTpr_Areatrabalho_codigo;
         new prc_osautomatica(context ).execute( ref  GXt_int3, out  GXt_boolean2) ;
         AV6WWPContext.gxTpr_Areatrabalho_codigo = GXt_int3;
         edtContratada_OS_Visible = (GXt_boolean2 ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_OS_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_OS_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         edtavDisplay_Visible = (AV6WWPContext.gxTpr_Display ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDisplay_Visible), 5, 0)));
         AV117WWContratadaDS_1_Contratada_areatrabalhocod = AV42Contratada_AreaTrabalhoCod;
         AV118WWContratadaDS_2_Contratada_codigo = AV76Contratada_Codigo;
         AV119WWContratadaDS_3_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV120WWContratadaDS_4_Contratada_pessoanom1 = AV17Contratada_PessoaNom1;
         AV121WWContratadaDS_5_Contratada_pessoacnpj1 = AV73Contratada_PessoaCNPJ1;
         AV122WWContratadaDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV123WWContratadaDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV124WWContratadaDS_8_Contratada_pessoanom2 = AV22Contratada_PessoaNom2;
         AV125WWContratadaDS_9_Contratada_pessoacnpj2 = AV74Contratada_PessoaCNPJ2;
         AV126WWContratadaDS_10_Tfcontratada_pessoanom = AV80TFContratada_PessoaNom;
         AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel = AV81TFContratada_PessoaNom_Sel;
         AV128WWContratadaDS_12_Tfcontratada_pessoacnpj = AV84TFContratada_PessoaCNPJ;
         AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel = AV85TFContratada_PessoaCNPJ_Sel;
         AV130WWContratadaDS_14_Tfpessoa_ie = AV88TFPessoa_IE;
         AV131WWContratadaDS_15_Tfpessoa_ie_sel = AV89TFPessoa_IE_Sel;
         AV132WWContratadaDS_16_Tfpessoa_telefone = AV92TFPessoa_Telefone;
         AV133WWContratadaDS_17_Tfpessoa_telefone_sel = AV93TFPessoa_Telefone_Sel;
         AV134WWContratadaDS_18_Tfcontratada_ss = AV96TFContratada_SS;
         AV135WWContratadaDS_19_Tfcontratada_ss_to = AV97TFContratada_SS_To;
         AV136WWContratadaDS_20_Tfcontratada_os = AV100TFContratada_OS;
         AV137WWContratadaDS_21_Tfcontratada_os_to = AV101TFContratada_OS_To;
         AV138WWContratadaDS_22_Tfcontratada_lote = AV104TFContratada_Lote;
         AV139WWContratadaDS_23_Tfcontratada_lote_to = AV105TFContratada_Lote_To;
         AV140WWContratadaDS_24_Tfcontratada_ativo_sel = AV108TFContratada_Ativo_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV79Contratada_PessoaNomTitleFilterData", AV79Contratada_PessoaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV83Contratada_PessoaCNPJTitleFilterData", AV83Contratada_PessoaCNPJTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV87Pessoa_IETitleFilterData", AV87Pessoa_IETitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV91Pessoa_TelefoneTitleFilterData", AV91Pessoa_TelefoneTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV95Contratada_SSTitleFilterData", AV95Contratada_SSTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV99Contratada_OSTitleFilterData", AV99Contratada_OSTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV103Contratada_LoteTitleFilterData", AV103Contratada_LoteTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV107Contratada_AtivoTitleFilterData", AV107Contratada_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11352( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV111PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV111PageToGo) ;
         }
      }

      protected void E12352( )
      {
         /* Ddo_contratada_pessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV80TFContratada_PessoaNom = Ddo_contratada_pessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratada_PessoaNom", AV80TFContratada_PessoaNom);
            AV81TFContratada_PessoaNom_Sel = Ddo_contratada_pessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratada_PessoaNom_Sel", AV81TFContratada_PessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13352( )
      {
         /* Ddo_contratada_pessoacnpj_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_pessoacnpj_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoacnpj_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoacnpj_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoacnpj_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoacnpj_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV84TFContratada_PessoaCNPJ = Ddo_contratada_pessoacnpj_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContratada_PessoaCNPJ", AV84TFContratada_PessoaCNPJ);
            AV85TFContratada_PessoaCNPJ_Sel = Ddo_contratada_pessoacnpj_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratada_PessoaCNPJ_Sel", AV85TFContratada_PessoaCNPJ_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14352( )
      {
         /* Ddo_pessoa_ie_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoa_ie_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_ie_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ie_Internalname, "SortedStatus", Ddo_pessoa_ie_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_ie_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_ie_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ie_Internalname, "SortedStatus", Ddo_pessoa_ie_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_ie_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV88TFPessoa_IE = Ddo_pessoa_ie_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFPessoa_IE", AV88TFPessoa_IE);
            AV89TFPessoa_IE_Sel = Ddo_pessoa_ie_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFPessoa_IE_Sel", AV89TFPessoa_IE_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15352( )
      {
         /* Ddo_pessoa_telefone_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoa_telefone_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_telefone_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_telefone_Internalname, "SortedStatus", Ddo_pessoa_telefone_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_telefone_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoa_telefone_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_telefone_Internalname, "SortedStatus", Ddo_pessoa_telefone_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoa_telefone_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV92TFPessoa_Telefone = Ddo_pessoa_telefone_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFPessoa_Telefone", AV92TFPessoa_Telefone);
            AV93TFPessoa_Telefone_Sel = Ddo_pessoa_telefone_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93TFPessoa_Telefone_Sel", AV93TFPessoa_Telefone_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16352( )
      {
         /* Ddo_contratada_ss_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_ss_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_ss_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ss_Internalname, "SortedStatus", Ddo_contratada_ss_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_ss_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_ss_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ss_Internalname, "SortedStatus", Ddo_contratada_ss_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_ss_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV96TFContratada_SS = (int)(NumberUtil.Val( Ddo_contratada_ss_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96TFContratada_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96TFContratada_SS), 8, 0)));
            AV97TFContratada_SS_To = (int)(NumberUtil.Val( Ddo_contratada_ss_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97TFContratada_SS_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV97TFContratada_SS_To), 8, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17352( )
      {
         /* Ddo_contratada_os_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_os_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_os_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_os_Internalname, "SortedStatus", Ddo_contratada_os_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_os_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_os_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_os_Internalname, "SortedStatus", Ddo_contratada_os_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_os_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV100TFContratada_OS = (int)(NumberUtil.Val( Ddo_contratada_os_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFContratada_OS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100TFContratada_OS), 8, 0)));
            AV101TFContratada_OS_To = (int)(NumberUtil.Val( Ddo_contratada_os_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFContratada_OS_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV101TFContratada_OS_To), 8, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18352( )
      {
         /* Ddo_contratada_lote_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_lote_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_lote_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_lote_Internalname, "SortedStatus", Ddo_contratada_lote_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_lote_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_lote_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_lote_Internalname, "SortedStatus", Ddo_contratada_lote_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_lote_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV104TFContratada_Lote = (short)(NumberUtil.Val( Ddo_contratada_lote_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFContratada_Lote", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104TFContratada_Lote), 4, 0)));
            AV105TFContratada_Lote_To = (short)(NumberUtil.Val( Ddo_contratada_lote_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFContratada_Lote_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFContratada_Lote_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E19352( )
      {
         /* Ddo_contratada_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ativo_Internalname, "SortedStatus", Ddo_contratada_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ativo_Internalname, "SortedStatus", Ddo_contratada_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV108TFContratada_Ativo_Sel = (short)(NumberUtil.Val( Ddo_contratada_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContratada_Ativo_Sel", StringUtil.Str( (decimal)(AV108TFContratada_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E31352( )
      {
         /* Grid_Load Routine */
         AV31Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV31Update);
         AV141Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contratada.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A39Contratada_Codigo);
         AV32Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
         AV142Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contratada.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A39Contratada_Codigo);
         AV75Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV75Display);
         AV143Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Contrato e Usu�rios";
         edtavDisplay_Link = formatLink("viewcontratada.aspx") + "?" + UrlEncode("" +A39Contratada_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         if ( A43Contratada_Ativo )
         {
            AV114Color = GXUtil.RGB( 0, 0, 0);
         }
         else
         {
            AV114Color = GXUtil.RGB( 255, 0, 0);
         }
         edtContratada_PessoaNom_Forecolor = (int)(AV114Color);
         edtContratada_PessoaCNPJ_Forecolor = (int)(AV114Color);
         edtPessoa_IE_Forecolor = (int)(AV114Color);
         edtPessoa_Telefone_Forecolor = (int)(AV114Color);
         edtContratada_SS_Forecolor = (int)(AV114Color);
         edtContratada_OS_Forecolor = (int)(AV114Color);
         edtContratada_Lote_Forecolor = (int)(AV114Color);
         if ( ( NumberUtil.Val( StringUtil.Substring( AV6WWPContext.gxTpr_Validationkey, 3, 2), ".") <= Convert.ToDecimal( subGrid_Recordcount( ) )) && ( StringUtil.StrCmp(StringUtil.Substring( AV6WWPContext.gxTpr_Validationkey, 1, 2), "00") != 0 ) )
         {
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
            imgInsert_Tooltiptext = "Limite da sua vers�o atingido!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Tooltiptext", imgInsert_Tooltiptext);
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 77;
         }
         sendrow_772( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_77_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(77, GridRow);
         }
      }

      protected void E20352( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E25352( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E21352( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Contratada_PessoaNom1, AV73Contratada_PessoaCNPJ1, AV20DynamicFiltersSelector2, AV22Contratada_PessoaNom2, AV74Contratada_PessoaCNPJ2, AV19DynamicFiltersEnabled2, AV80TFContratada_PessoaNom, AV81TFContratada_PessoaNom_Sel, AV84TFContratada_PessoaCNPJ, AV85TFContratada_PessoaCNPJ_Sel, AV88TFPessoa_IE, AV89TFPessoa_IE_Sel, AV92TFPessoa_Telefone, AV93TFPessoa_Telefone_Sel, AV96TFContratada_SS, AV97TFContratada_SS_To, AV100TFContratada_OS, AV101TFContratada_OS_To, AV104TFContratada_Lote, AV105TFContratada_Lote_To, AV108TFContratada_Ativo_Sel, AV82ddo_Contratada_PessoaNomTitleControlIdToReplace, AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV90ddo_Pessoa_IETitleControlIdToReplace, AV94ddo_Pessoa_TelefoneTitleControlIdToReplace, AV98ddo_Contratada_SSTitleControlIdToReplace, AV102ddo_Contratada_OSTitleControlIdToReplace, AV106ddo_Contratada_LoteTitleControlIdToReplace, AV109ddo_Contratada_AtivoTitleControlIdToReplace, AV42Contratada_AreaTrabalhoCod, AV76Contratada_Codigo, AV144Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A39Contratada_Codigo, A43Contratada_Ativo, AV6WWPContext) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E26352( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22352( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Contratada_PessoaNom1, AV73Contratada_PessoaCNPJ1, AV20DynamicFiltersSelector2, AV22Contratada_PessoaNom2, AV74Contratada_PessoaCNPJ2, AV19DynamicFiltersEnabled2, AV80TFContratada_PessoaNom, AV81TFContratada_PessoaNom_Sel, AV84TFContratada_PessoaCNPJ, AV85TFContratada_PessoaCNPJ_Sel, AV88TFPessoa_IE, AV89TFPessoa_IE_Sel, AV92TFPessoa_Telefone, AV93TFPessoa_Telefone_Sel, AV96TFContratada_SS, AV97TFContratada_SS_To, AV100TFContratada_OS, AV101TFContratada_OS_To, AV104TFContratada_Lote, AV105TFContratada_Lote_To, AV108TFContratada_Ativo_Sel, AV82ddo_Contratada_PessoaNomTitleControlIdToReplace, AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV90ddo_Pessoa_IETitleControlIdToReplace, AV94ddo_Pessoa_TelefoneTitleControlIdToReplace, AV98ddo_Contratada_SSTitleControlIdToReplace, AV102ddo_Contratada_OSTitleControlIdToReplace, AV106ddo_Contratada_LoteTitleControlIdToReplace, AV109ddo_Contratada_AtivoTitleControlIdToReplace, AV42Contratada_AreaTrabalhoCod, AV76Contratada_Codigo, AV144Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A39Contratada_Codigo, A43Contratada_Ativo, AV6WWPContext) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E27352( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23352( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         dynavContratada_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV42Contratada_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_areatrabalhocod_Internalname, "Values", dynavContratada_areatrabalhocod.ToJavascriptSource());
         dynavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV76Contratada_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Values", dynavContratada_codigo.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
      }

      protected void E24352( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratada.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratada_pessoanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
         Ddo_contratada_pessoacnpj_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
         Ddo_pessoa_ie_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ie_Internalname, "SortedStatus", Ddo_pessoa_ie_Sortedstatus);
         Ddo_pessoa_telefone_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_telefone_Internalname, "SortedStatus", Ddo_pessoa_telefone_Sortedstatus);
         Ddo_contratada_ss_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ss_Internalname, "SortedStatus", Ddo_contratada_ss_Sortedstatus);
         Ddo_contratada_os_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_os_Internalname, "SortedStatus", Ddo_contratada_os_Sortedstatus);
         Ddo_contratada_lote_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_lote_Internalname, "SortedStatus", Ddo_contratada_lote_Sortedstatus);
         Ddo_contratada_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ativo_Internalname, "SortedStatus", Ddo_contratada_ativo_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_contratada_pessoanom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contratada_pessoacnpj_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_pessoa_ie_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ie_Internalname, "SortedStatus", Ddo_pessoa_ie_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_pessoa_telefone_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_telefone_Internalname, "SortedStatus", Ddo_pessoa_telefone_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contratada_ss_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ss_Internalname, "SortedStatus", Ddo_contratada_ss_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_contratada_os_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_os_Internalname, "SortedStatus", Ddo_contratada_os_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_contratada_lote_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_lote_Internalname, "SortedStatus", Ddo_contratada_lote_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_contratada_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ativo_Internalname, "SortedStatus", Ddo_contratada_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContratada_pessoanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom1_Visible), 5, 0)));
         edtavContratada_pessoacnpj1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoacnpj1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoacnpj1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
         {
            edtavContratada_pessoanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOACNPJ") == 0 )
         {
            edtavContratada_pessoacnpj1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoacnpj1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoacnpj1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContratada_pessoanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom2_Visible), 5, 0)));
         edtavContratada_pessoacnpj2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoacnpj2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoacnpj2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
         {
            edtavContratada_pessoanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOACNPJ") == 0 )
         {
            edtavContratada_pessoacnpj2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoacnpj2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoacnpj2_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "CONTRATADA_PESSOANOM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV22Contratada_PessoaNom2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratada_PessoaNom2", AV22Contratada_PessoaNom2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S212( )
      {
         /* 'CLEANFILTERS' Routine */
         AV42Contratada_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Contratada_AreaTrabalhoCod), 6, 0)));
         AV76Contratada_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76Contratada_Codigo), 6, 0)));
         AV80TFContratada_PessoaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratada_PessoaNom", AV80TFContratada_PessoaNom);
         Ddo_contratada_pessoanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "FilteredText_set", Ddo_contratada_pessoanom_Filteredtext_set);
         AV81TFContratada_PessoaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratada_PessoaNom_Sel", AV81TFContratada_PessoaNom_Sel);
         Ddo_contratada_pessoanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SelectedValue_set", Ddo_contratada_pessoanom_Selectedvalue_set);
         AV84TFContratada_PessoaCNPJ = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContratada_PessoaCNPJ", AV84TFContratada_PessoaCNPJ);
         Ddo_contratada_pessoacnpj_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "FilteredText_set", Ddo_contratada_pessoacnpj_Filteredtext_set);
         AV85TFContratada_PessoaCNPJ_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratada_PessoaCNPJ_Sel", AV85TFContratada_PessoaCNPJ_Sel);
         Ddo_contratada_pessoacnpj_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SelectedValue_set", Ddo_contratada_pessoacnpj_Selectedvalue_set);
         AV88TFPessoa_IE = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFPessoa_IE", AV88TFPessoa_IE);
         Ddo_pessoa_ie_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ie_Internalname, "FilteredText_set", Ddo_pessoa_ie_Filteredtext_set);
         AV89TFPessoa_IE_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFPessoa_IE_Sel", AV89TFPessoa_IE_Sel);
         Ddo_pessoa_ie_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ie_Internalname, "SelectedValue_set", Ddo_pessoa_ie_Selectedvalue_set);
         AV92TFPessoa_Telefone = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFPessoa_Telefone", AV92TFPessoa_Telefone);
         Ddo_pessoa_telefone_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_telefone_Internalname, "FilteredText_set", Ddo_pessoa_telefone_Filteredtext_set);
         AV93TFPessoa_Telefone_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93TFPessoa_Telefone_Sel", AV93TFPessoa_Telefone_Sel);
         Ddo_pessoa_telefone_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_telefone_Internalname, "SelectedValue_set", Ddo_pessoa_telefone_Selectedvalue_set);
         AV96TFContratada_SS = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96TFContratada_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96TFContratada_SS), 8, 0)));
         Ddo_contratada_ss_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ss_Internalname, "FilteredText_set", Ddo_contratada_ss_Filteredtext_set);
         AV97TFContratada_SS_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97TFContratada_SS_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV97TFContratada_SS_To), 8, 0)));
         Ddo_contratada_ss_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ss_Internalname, "FilteredTextTo_set", Ddo_contratada_ss_Filteredtextto_set);
         AV100TFContratada_OS = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFContratada_OS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100TFContratada_OS), 8, 0)));
         Ddo_contratada_os_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_os_Internalname, "FilteredText_set", Ddo_contratada_os_Filteredtext_set);
         AV101TFContratada_OS_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFContratada_OS_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV101TFContratada_OS_To), 8, 0)));
         Ddo_contratada_os_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_os_Internalname, "FilteredTextTo_set", Ddo_contratada_os_Filteredtextto_set);
         AV104TFContratada_Lote = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFContratada_Lote", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104TFContratada_Lote), 4, 0)));
         Ddo_contratada_lote_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_lote_Internalname, "FilteredText_set", Ddo_contratada_lote_Filteredtext_set);
         AV105TFContratada_Lote_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFContratada_Lote_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFContratada_Lote_To), 4, 0)));
         Ddo_contratada_lote_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_lote_Internalname, "FilteredTextTo_set", Ddo_contratada_lote_Filteredtextto_set);
         AV108TFContratada_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContratada_Ativo_Sel", StringUtil.Str( (decimal)(AV108TFContratada_Ativo_Sel), 1, 0));
         Ddo_contratada_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ativo_Internalname, "SelectedValue_set", Ddo_contratada_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "CONTRATADA_PESSOANOM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV17Contratada_PessoaNom1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_PessoaNom1", AV17Contratada_PessoaNom1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get(AV144Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV144Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV35Session.Get(AV144Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S222( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV145GXV1 = 1;
         while ( AV145GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV145GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTRATADA_AREATRABALHOCOD") == 0 )
            {
               AV42Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Contratada_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTRATADA_CODIGO") == 0 )
            {
               AV76Contratada_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76Contratada_Codigo), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM") == 0 )
            {
               AV80TFContratada_PessoaNom = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratada_PessoaNom", AV80TFContratada_PessoaNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80TFContratada_PessoaNom)) )
               {
                  Ddo_contratada_pessoanom_Filteredtext_set = AV80TFContratada_PessoaNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "FilteredText_set", Ddo_contratada_pessoanom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM_SEL") == 0 )
            {
               AV81TFContratada_PessoaNom_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratada_PessoaNom_Sel", AV81TFContratada_PessoaNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81TFContratada_PessoaNom_Sel)) )
               {
                  Ddo_contratada_pessoanom_Selectedvalue_set = AV81TFContratada_PessoaNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SelectedValue_set", Ddo_contratada_pessoanom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ") == 0 )
            {
               AV84TFContratada_PessoaCNPJ = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContratada_PessoaCNPJ", AV84TFContratada_PessoaCNPJ);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84TFContratada_PessoaCNPJ)) )
               {
                  Ddo_contratada_pessoacnpj_Filteredtext_set = AV84TFContratada_PessoaCNPJ;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "FilteredText_set", Ddo_contratada_pessoacnpj_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ_SEL") == 0 )
            {
               AV85TFContratada_PessoaCNPJ_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratada_PessoaCNPJ_Sel", AV85TFContratada_PessoaCNPJ_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85TFContratada_PessoaCNPJ_Sel)) )
               {
                  Ddo_contratada_pessoacnpj_Selectedvalue_set = AV85TFContratada_PessoaCNPJ_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SelectedValue_set", Ddo_contratada_pessoacnpj_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_IE") == 0 )
            {
               AV88TFPessoa_IE = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFPessoa_IE", AV88TFPessoa_IE);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88TFPessoa_IE)) )
               {
                  Ddo_pessoa_ie_Filteredtext_set = AV88TFPessoa_IE;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ie_Internalname, "FilteredText_set", Ddo_pessoa_ie_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_IE_SEL") == 0 )
            {
               AV89TFPessoa_IE_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFPessoa_IE_Sel", AV89TFPessoa_IE_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89TFPessoa_IE_Sel)) )
               {
                  Ddo_pessoa_ie_Selectedvalue_set = AV89TFPessoa_IE_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_ie_Internalname, "SelectedValue_set", Ddo_pessoa_ie_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_TELEFONE") == 0 )
            {
               AV92TFPessoa_Telefone = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92TFPessoa_Telefone", AV92TFPessoa_Telefone);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92TFPessoa_Telefone)) )
               {
                  Ddo_pessoa_telefone_Filteredtext_set = AV92TFPessoa_Telefone;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_telefone_Internalname, "FilteredText_set", Ddo_pessoa_telefone_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOA_TELEFONE_SEL") == 0 )
            {
               AV93TFPessoa_Telefone_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93TFPessoa_Telefone_Sel", AV93TFPessoa_Telefone_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93TFPessoa_Telefone_Sel)) )
               {
                  Ddo_pessoa_telefone_Selectedvalue_set = AV93TFPessoa_Telefone_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoa_telefone_Internalname, "SelectedValue_set", Ddo_pessoa_telefone_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_SS") == 0 )
            {
               AV96TFContratada_SS = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96TFContratada_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96TFContratada_SS), 8, 0)));
               AV97TFContratada_SS_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97TFContratada_SS_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV97TFContratada_SS_To), 8, 0)));
               if ( ! (0==AV96TFContratada_SS) )
               {
                  Ddo_contratada_ss_Filteredtext_set = StringUtil.Str( (decimal)(AV96TFContratada_SS), 8, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ss_Internalname, "FilteredText_set", Ddo_contratada_ss_Filteredtext_set);
               }
               if ( ! (0==AV97TFContratada_SS_To) )
               {
                  Ddo_contratada_ss_Filteredtextto_set = StringUtil.Str( (decimal)(AV97TFContratada_SS_To), 8, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ss_Internalname, "FilteredTextTo_set", Ddo_contratada_ss_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_OS") == 0 )
            {
               AV100TFContratada_OS = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFContratada_OS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100TFContratada_OS), 8, 0)));
               AV101TFContratada_OS_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFContratada_OS_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV101TFContratada_OS_To), 8, 0)));
               if ( ! (0==AV100TFContratada_OS) )
               {
                  Ddo_contratada_os_Filteredtext_set = StringUtil.Str( (decimal)(AV100TFContratada_OS), 8, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_os_Internalname, "FilteredText_set", Ddo_contratada_os_Filteredtext_set);
               }
               if ( ! (0==AV101TFContratada_OS_To) )
               {
                  Ddo_contratada_os_Filteredtextto_set = StringUtil.Str( (decimal)(AV101TFContratada_OS_To), 8, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_os_Internalname, "FilteredTextTo_set", Ddo_contratada_os_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_LOTE") == 0 )
            {
               AV104TFContratada_Lote = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFContratada_Lote", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104TFContratada_Lote), 4, 0)));
               AV105TFContratada_Lote_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFContratada_Lote_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105TFContratada_Lote_To), 4, 0)));
               if ( ! (0==AV104TFContratada_Lote) )
               {
                  Ddo_contratada_lote_Filteredtext_set = StringUtil.Str( (decimal)(AV104TFContratada_Lote), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_lote_Internalname, "FilteredText_set", Ddo_contratada_lote_Filteredtext_set);
               }
               if ( ! (0==AV105TFContratada_Lote_To) )
               {
                  Ddo_contratada_lote_Filteredtextto_set = StringUtil.Str( (decimal)(AV105TFContratada_Lote_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_lote_Internalname, "FilteredTextTo_set", Ddo_contratada_lote_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_ATIVO_SEL") == 0 )
            {
               AV108TFContratada_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContratada_Ativo_Sel", StringUtil.Str( (decimal)(AV108TFContratada_Ativo_Sel), 1, 0));
               if ( ! (0==AV108TFContratada_Ativo_Sel) )
               {
                  Ddo_contratada_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV108TFContratada_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ativo_Internalname, "SelectedValue_set", Ddo_contratada_ativo_Selectedvalue_set);
               }
            }
            AV145GXV1 = (int)(AV145GXV1+1);
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
            {
               AV17Contratada_PessoaNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_PessoaNom1", AV17Contratada_PessoaNom1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOACNPJ") == 0 )
            {
               AV73Contratada_PessoaCNPJ1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Contratada_PessoaCNPJ1", AV73Contratada_PessoaCNPJ1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
               {
                  AV22Contratada_PessoaNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratada_PessoaNom2", AV22Contratada_PessoaNom2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOACNPJ") == 0 )
               {
                  AV74Contratada_PessoaCNPJ2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74Contratada_PessoaCNPJ2", AV74Contratada_PessoaCNPJ2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV35Session.Get(AV144Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV42Contratada_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTRATADA_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV42Contratada_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV76Contratada_Codigo) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTRATADA_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV76Contratada_Codigo), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80TFContratada_PessoaNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOANOM";
            AV11GridStateFilterValue.gxTpr_Value = AV80TFContratada_PessoaNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81TFContratada_PessoaNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOANOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV81TFContratada_PessoaNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84TFContratada_PessoaCNPJ)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOACNPJ";
            AV11GridStateFilterValue.gxTpr_Value = AV84TFContratada_PessoaCNPJ;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85TFContratada_PessoaCNPJ_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOACNPJ_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV85TFContratada_PessoaCNPJ_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88TFPessoa_IE)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_IE";
            AV11GridStateFilterValue.gxTpr_Value = AV88TFPessoa_IE;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89TFPessoa_IE_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_IE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV89TFPessoa_IE_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92TFPessoa_Telefone)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_TELEFONE";
            AV11GridStateFilterValue.gxTpr_Value = AV92TFPessoa_Telefone;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93TFPessoa_Telefone_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOA_TELEFONE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV93TFPessoa_Telefone_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV96TFContratada_SS) && (0==AV97TFContratada_SS_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_SS";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV96TFContratada_SS), 8, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV97TFContratada_SS_To), 8, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV100TFContratada_OS) && (0==AV101TFContratada_OS_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_OS";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV100TFContratada_OS), 8, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV101TFContratada_OS_To), 8, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV104TFContratada_Lote) && (0==AV105TFContratada_Lote_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_LOTE";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV104TFContratada_Lote), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV105TFContratada_Lote_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV108TFContratada_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV108TFContratada_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV144Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Contratada_PessoaNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17Contratada_PessoaNom1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOACNPJ") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV73Contratada_PessoaCNPJ1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV73Contratada_PessoaCNPJ1;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Contratada_PessoaNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22Contratada_PessoaNom2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOACNPJ") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV74Contratada_PessoaCNPJ2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV74Contratada_PessoaCNPJ2;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV144Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Contratada";
         AV35Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void E28352( )
      {
         /* Onmessage_gx1 Routine */
         if ( StringUtil.StrCmp(AV44NotificationInfo.gxTpr_Id, "ALTERAR_AREA_TRABALHO") == 0 )
         {
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
            AV42Contratada_AreaTrabalhoCod = AV6WWPContext.gxTpr_Areatrabalho_codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Contratada_AreaTrabalhoCod), 6, 0)));
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Contratada_PessoaNom1, AV73Contratada_PessoaCNPJ1, AV20DynamicFiltersSelector2, AV22Contratada_PessoaNom2, AV74Contratada_PessoaCNPJ2, AV19DynamicFiltersEnabled2, AV80TFContratada_PessoaNom, AV81TFContratada_PessoaNom_Sel, AV84TFContratada_PessoaCNPJ, AV85TFContratada_PessoaCNPJ_Sel, AV88TFPessoa_IE, AV89TFPessoa_IE_Sel, AV92TFPessoa_Telefone, AV93TFPessoa_Telefone_Sel, AV96TFContratada_SS, AV97TFContratada_SS_To, AV100TFContratada_OS, AV101TFContratada_OS_To, AV104TFContratada_Lote, AV105TFContratada_Lote_To, AV108TFContratada_Ativo_Sel, AV82ddo_Contratada_PessoaNomTitleControlIdToReplace, AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV90ddo_Pessoa_IETitleControlIdToReplace, AV94ddo_Pessoa_TelefoneTitleControlIdToReplace, AV98ddo_Contratada_SSTitleControlIdToReplace, AV102ddo_Contratada_OSTitleControlIdToReplace, AV106ddo_Contratada_LoteTitleControlIdToReplace, AV109ddo_Contratada_AtivoTitleControlIdToReplace, AV42Contratada_AreaTrabalhoCod, AV76Contratada_Codigo, AV144Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A39Contratada_Codigo, A43Contratada_Ativo, AV6WWPContext) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         dynavContratada_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV42Contratada_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_areatrabalhocod_Internalname, "Values", dynavContratada_areatrabalhocod.ToJavascriptSource());
      }

      protected void wb_table1_2_352( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_352( true) ;
         }
         else
         {
            wb_table2_8_352( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_352e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_71_352( true) ;
         }
         else
         {
            wb_table3_71_352( false) ;
         }
         return  ;
      }

      protected void wb_table3_71_352e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_352e( true) ;
         }
         else
         {
            wb_table1_2_352e( false) ;
         }
      }

      protected void wb_table3_71_352( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_74_352( true) ;
         }
         else
         {
            wb_table4_74_352( false) ;
         }
         return  ;
      }

      protected void wb_table4_74_352e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_71_352e( true) ;
         }
         else
         {
            wb_table3_71_352e( false) ;
         }
      }

      protected void wb_table4_74_352( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"77\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "de Trabalho") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "da Pessoa") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaCNPJ_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaCNPJ_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaCNPJ_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoa_IE_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoa_IE_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoa_IE_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoa_Telefone_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoa_Telefone_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoa_Telefone_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(58), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_SS_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_SS_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_SS_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(58), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtContratada_OS_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               if ( edtContratada_OS_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_OS_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_OS_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(50), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_Lote_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_Lote_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_Lote_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkContratada_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkContratada_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkContratada_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV75Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A41Contratada_PessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaNom_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A42Contratada_PessoaCNPJ);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaCNPJ_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaCNPJ_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaCNPJ_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A518Pessoa_IE));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoa_IE_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoa_IE_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoa_IE_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A522Pessoa_Telefone));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoa_Telefone_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoa_Telefone_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoa_Telefone_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1451Contratada_SS), 8, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_SS_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_SS_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_SS_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A524Contratada_OS), 8, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_OS_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_OS_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_OS_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_OS_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A530Contratada_Lote), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_Lote_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_Lote_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_Lote_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A43Contratada_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkContratada_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkContratada_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 77 )
         {
            wbEnd = 0;
            nRC_GXsfl_77 = (short)(nGXsfl_77_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_74_352e( true) ;
         }
         else
         {
            wb_table4_74_352e( false) ;
         }
      }

      protected void wb_table2_8_352( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratadatitle_Internalname, "Contratadas", "", "", lblContratadatitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_13_352( true) ;
         }
         else
         {
            wb_table5_13_352( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_352e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_77_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWContratada.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_352( true) ;
         }
         else
         {
            wb_table6_23_352( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_352e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_352e( true) ;
         }
         else
         {
            wb_table2_8_352e( false) ;
         }
      }

      protected void wb_table6_23_352( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontratada_areatrabalhocod_Internalname, "�rea de Trabalhlho", "", "", lblFiltertextcontratada_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_77_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada_areatrabalhocod, dynavContratada_areatrabalhocod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV42Contratada_AreaTrabalhoCod), 6, 0)), 1, dynavContratada_areatrabalhocod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_WWContratada.htm");
            dynavContratada_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV42Contratada_AreaTrabalhoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_areatrabalhocod_Internalname, "Values", (String)(dynavContratada_areatrabalhocod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontratada_codigo_Internalname, "Contratada", "", "", lblFiltertextcontratada_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_77_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada_codigo, dynavContratada_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV76Contratada_Codigo), 6, 0)), 1, dynavContratada_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", "", true, "HLP_WWContratada.htm");
            dynavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV76Contratada_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Values", (String)(dynavContratada_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_37_352( true) ;
         }
         else
         {
            wb_table7_37_352( false) ;
         }
         return  ;
      }

      protected void wb_table7_37_352e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_352e( true) ;
         }
         else
         {
            wb_table6_23_352e( false) ;
         }
      }

      protected void wb_table7_37_352( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_40_352( true) ;
         }
         else
         {
            wb_table8_40_352( false) ;
         }
         return  ;
      }

      protected void wb_table8_40_352e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_37_352e( true) ;
         }
         else
         {
            wb_table7_37_352e( false) ;
         }
      }

      protected void wb_table8_40_352( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_77_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_WWContratada.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoanom1_Internalname, StringUtil.RTrim( AV17Contratada_PessoaNom1), StringUtil.RTrim( context.localUtil.Format( AV17Contratada_PessoaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_pessoanom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoacnpj1_Internalname, AV73Contratada_PessoaCNPJ1, StringUtil.RTrim( context.localUtil.Format( AV73Contratada_PessoaCNPJ1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoacnpj1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_pessoacnpj1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratada.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_77_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_WWContratada.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoanom2_Internalname, StringUtil.RTrim( AV22Contratada_PessoaNom2), StringUtil.RTrim( context.localUtil.Format( AV22Contratada_PessoaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_pessoanom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoacnpj2_Internalname, AV74Contratada_PessoaCNPJ2, StringUtil.RTrim( context.localUtil.Format( AV74Contratada_PessoaCNPJ2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoacnpj2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_pessoacnpj2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_40_352e( true) ;
         }
         else
         {
            wb_table8_40_352e( false) ;
         }
      }

      protected void wb_table5_13_352( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, imgInsert_Enabled, "", imgInsert_Tooltiptext, 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_352e( true) ;
         }
         else
         {
            wb_table5_13_352e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA352( ) ;
         WS352( ) ;
         WE352( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117331083");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontratada.js", "?20203117331084");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_772( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_77_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_77_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_77_idx;
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO_"+sGXsfl_77_idx;
         edtContratada_AreaTrabalhoCod_Internalname = "CONTRATADA_AREATRABALHOCOD_"+sGXsfl_77_idx;
         edtContratada_PessoaCod_Internalname = "CONTRATADA_PESSOACOD_"+sGXsfl_77_idx;
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM_"+sGXsfl_77_idx;
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ_"+sGXsfl_77_idx;
         edtPessoa_IE_Internalname = "PESSOA_IE_"+sGXsfl_77_idx;
         edtPessoa_Telefone_Internalname = "PESSOA_TELEFONE_"+sGXsfl_77_idx;
         edtContratada_SS_Internalname = "CONTRATADA_SS_"+sGXsfl_77_idx;
         edtContratada_OS_Internalname = "CONTRATADA_OS_"+sGXsfl_77_idx;
         edtContratada_Lote_Internalname = "CONTRATADA_LOTE_"+sGXsfl_77_idx;
         chkContratada_Ativo_Internalname = "CONTRATADA_ATIVO_"+sGXsfl_77_idx;
      }

      protected void SubsflControlProps_fel_772( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_77_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_77_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_77_fel_idx;
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO_"+sGXsfl_77_fel_idx;
         edtContratada_AreaTrabalhoCod_Internalname = "CONTRATADA_AREATRABALHOCOD_"+sGXsfl_77_fel_idx;
         edtContratada_PessoaCod_Internalname = "CONTRATADA_PESSOACOD_"+sGXsfl_77_fel_idx;
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM_"+sGXsfl_77_fel_idx;
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ_"+sGXsfl_77_fel_idx;
         edtPessoa_IE_Internalname = "PESSOA_IE_"+sGXsfl_77_fel_idx;
         edtPessoa_Telefone_Internalname = "PESSOA_TELEFONE_"+sGXsfl_77_fel_idx;
         edtContratada_SS_Internalname = "CONTRATADA_SS_"+sGXsfl_77_fel_idx;
         edtContratada_OS_Internalname = "CONTRATADA_OS_"+sGXsfl_77_fel_idx;
         edtContratada_Lote_Internalname = "CONTRATADA_LOTE_"+sGXsfl_77_fel_idx;
         chkContratada_Ativo_Internalname = "CONTRATADA_ATIVO_"+sGXsfl_77_fel_idx;
      }

      protected void sendrow_772( )
      {
         SubsflControlProps_772( ) ;
         WB350( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_77_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_77_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_77_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV31Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV141Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV141Update_GXI : context.PathToRelativeUrl( AV31Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV31Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV142Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV142Delete_GXI : context.PathToRelativeUrl( AV32Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV75Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV75Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV143Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV75Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV75Display)) ? AV143Display_GXI : context.PathToRelativeUrl( AV75Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDisplay_Visible,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV75Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_AreaTrabalhoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A52Contratada_AreaTrabalhoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_AreaTrabalhoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A40Contratada_PessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaNom_Internalname,StringUtil.RTrim( A41Contratada_PessoaNom),StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContratada_PessoaNom_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaCNPJ_Internalname,(String)A42Contratada_PessoaCNPJ,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaCNPJ_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContratada_PessoaCNPJ_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoa_IE_Internalname,StringUtil.RTrim( A518Pessoa_IE),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoa_IE_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtPessoa_IE_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)-1,(bool)true,(String)"IE",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoa_Telefone_Internalname,StringUtil.RTrim( A522Pessoa_Telefone),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoa_Telefone_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtPessoa_Telefone_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_SS_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1451Contratada_SS), 8, 0, ",", "")),context.localUtil.Format( (decimal)(A1451Contratada_SS), "ZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_SS_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContratada_SS_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)58,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((edtContratada_OS_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_OS_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A524Contratada_OS), 8, 0, ",", "")),context.localUtil.Format( (decimal)(A524Contratada_OS), "ZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_OS_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContratada_OS_Forecolor)+";",(String)ROClassString,(String)"",(int)edtContratada_OS_Visible,(short)0,(short)0,(String)"text",(String)"",(short)58,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_Lote_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A530Contratada_Lote), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A530Contratada_Lote), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_Lote_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContratada_Lote_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)50,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkContratada_Ativo_Internalname,StringUtil.BoolToStr( A43Contratada_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_CODIGO"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_AREATRABALHOCOD"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, context.localUtil.Format( (decimal)(A52Contratada_AreaTrabalhoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_PESSOACOD"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, context.localUtil.Format( (decimal)(A40Contratada_PessoaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_SS"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, context.localUtil.Format( (decimal)(A1451Contratada_SS), "ZZZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_OS"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, context.localUtil.Format( (decimal)(A524Contratada_OS), "ZZZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_LOTE"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, context.localUtil.Format( (decimal)(A530Contratada_Lote), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_ATIVO"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, A43Contratada_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_77_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_77_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_77_idx+1));
            sGXsfl_77_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_77_idx), 4, 0)), 4, "0");
            SubsflControlProps_772( ) ;
         }
         /* End function sendrow_772 */
      }

      protected void init_default_properties( )
      {
         lblContratadatitle_Internalname = "CONTRATADATITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         lblFiltertextcontratada_areatrabalhocod_Internalname = "FILTERTEXTCONTRATADA_AREATRABALHOCOD";
         dynavContratada_areatrabalhocod_Internalname = "vCONTRATADA_AREATRABALHOCOD";
         lblFiltertextcontratada_codigo_Internalname = "FILTERTEXTCONTRATADA_CODIGO";
         dynavContratada_codigo_Internalname = "vCONTRATADA_CODIGO";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavContratada_pessoanom1_Internalname = "vCONTRATADA_PESSOANOM1";
         edtavContratada_pessoacnpj1_Internalname = "vCONTRATADA_PESSOACNPJ1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavContratada_pessoanom2_Internalname = "vCONTRATADA_PESSOANOM2";
         edtavContratada_pessoacnpj2_Internalname = "vCONTRATADA_PESSOACNPJ2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO";
         edtContratada_AreaTrabalhoCod_Internalname = "CONTRATADA_AREATRABALHOCOD";
         edtContratada_PessoaCod_Internalname = "CONTRATADA_PESSOACOD";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ";
         edtPessoa_IE_Internalname = "PESSOA_IE";
         edtPessoa_Telefone_Internalname = "PESSOA_TELEFONE";
         edtContratada_SS_Internalname = "CONTRATADA_SS";
         edtContratada_OS_Internalname = "CONTRATADA_OS";
         edtContratada_Lote_Internalname = "CONTRATADA_LOTE";
         chkContratada_Ativo_Internalname = "CONTRATADA_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         edtavTfcontratada_pessoanom_Internalname = "vTFCONTRATADA_PESSOANOM";
         edtavTfcontratada_pessoanom_sel_Internalname = "vTFCONTRATADA_PESSOANOM_SEL";
         edtavTfcontratada_pessoacnpj_Internalname = "vTFCONTRATADA_PESSOACNPJ";
         edtavTfcontratada_pessoacnpj_sel_Internalname = "vTFCONTRATADA_PESSOACNPJ_SEL";
         edtavTfpessoa_ie_Internalname = "vTFPESSOA_IE";
         edtavTfpessoa_ie_sel_Internalname = "vTFPESSOA_IE_SEL";
         edtavTfpessoa_telefone_Internalname = "vTFPESSOA_TELEFONE";
         edtavTfpessoa_telefone_sel_Internalname = "vTFPESSOA_TELEFONE_SEL";
         edtavTfcontratada_ss_Internalname = "vTFCONTRATADA_SS";
         edtavTfcontratada_ss_to_Internalname = "vTFCONTRATADA_SS_TO";
         edtavTfcontratada_os_Internalname = "vTFCONTRATADA_OS";
         edtavTfcontratada_os_to_Internalname = "vTFCONTRATADA_OS_TO";
         edtavTfcontratada_lote_Internalname = "vTFCONTRATADA_LOTE";
         edtavTfcontratada_lote_to_Internalname = "vTFCONTRATADA_LOTE_TO";
         edtavTfcontratada_ativo_sel_Internalname = "vTFCONTRATADA_ATIVO_SEL";
         Ddo_contratada_pessoanom_Internalname = "DDO_CONTRATADA_PESSOANOM";
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE";
         Ddo_contratada_pessoacnpj_Internalname = "DDO_CONTRATADA_PESSOACNPJ";
         edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE";
         Ddo_pessoa_ie_Internalname = "DDO_PESSOA_IE";
         edtavDdo_pessoa_ietitlecontrolidtoreplace_Internalname = "vDDO_PESSOA_IETITLECONTROLIDTOREPLACE";
         Ddo_pessoa_telefone_Internalname = "DDO_PESSOA_TELEFONE";
         edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Internalname = "vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE";
         Ddo_contratada_ss_Internalname = "DDO_CONTRATADA_SS";
         edtavDdo_contratada_sstitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_SSTITLECONTROLIDTOREPLACE";
         Ddo_contratada_os_Internalname = "DDO_CONTRATADA_OS";
         edtavDdo_contratada_ostitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_OSTITLECONTROLIDTOREPLACE";
         Ddo_contratada_lote_Internalname = "DDO_CONTRATADA_LOTE";
         edtavDdo_contratada_lotetitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_LOTETITLECONTROLIDTOREPLACE";
         Ddo_contratada_ativo_Internalname = "DDO_CONTRATADA_ATIVO";
         edtavDdo_contratada_ativotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratada_Lote_Jsonclick = "";
         edtContratada_OS_Jsonclick = "";
         edtContratada_SS_Jsonclick = "";
         edtPessoa_Telefone_Jsonclick = "";
         edtPessoa_IE_Jsonclick = "";
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratada_PessoaCod_Jsonclick = "";
         edtContratada_AreaTrabalhoCod_Jsonclick = "";
         edtContratada_Codigo_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Visible = 1;
         edtavContratada_pessoacnpj2_Jsonclick = "";
         edtavContratada_pessoanom2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavContratada_pessoacnpj1_Jsonclick = "";
         edtavContratada_pessoanom1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         dynavContratada_codigo_Jsonclick = "";
         dynavContratada_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratada_Lote_Forecolor = (int)(0xFFFFFF);
         edtContratada_OS_Forecolor = (int)(0xFFFFFF);
         edtContratada_SS_Forecolor = (int)(0xFFFFFF);
         edtPessoa_Telefone_Forecolor = (int)(0xFFFFFF);
         edtPessoa_IE_Forecolor = (int)(0xFFFFFF);
         edtContratada_PessoaCNPJ_Forecolor = (int)(0xFFFFFF);
         edtContratada_PessoaNom_Forecolor = (int)(0xFFFFFF);
         edtavDisplay_Tooltiptext = "Contrato e Usu�rios";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         chkContratada_Ativo_Titleformat = 0;
         edtContratada_Lote_Titleformat = 0;
         edtContratada_OS_Titleformat = 0;
         edtContratada_SS_Titleformat = 0;
         edtPessoa_Telefone_Titleformat = 0;
         edtPessoa_IE_Titleformat = 0;
         edtContratada_PessoaCNPJ_Titleformat = 0;
         edtContratada_PessoaNom_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavContratada_pessoacnpj2_Visible = 1;
         edtavContratada_pessoanom2_Visible = 1;
         edtavContratada_pessoacnpj1_Visible = 1;
         edtavContratada_pessoanom1_Visible = 1;
         imgInsert_Tooltiptext = "Inserir";
         edtavDisplay_Visible = -1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         edtContratada_OS_Visible = -1;
         chkContratada_Ativo.Title.Text = "Ativo";
         edtContratada_Lote_Title = "�lt.Lote";
         edtContratada_OS_Title = "�lt.OS";
         edtContratada_SS_Title = "�lt.SS";
         edtPessoa_Telefone_Title = "Telefone";
         edtPessoa_IE_Title = "IE";
         edtContratada_PessoaCNPJ_Title = "CNPJ";
         edtContratada_PessoaNom_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         chkContratada_Ativo.Caption = "";
         edtavDdo_contratada_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_lotetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_ostitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_sstitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pessoa_ietitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratada_ativo_sel_Jsonclick = "";
         edtavTfcontratada_ativo_sel_Visible = 1;
         edtavTfcontratada_lote_to_Jsonclick = "";
         edtavTfcontratada_lote_to_Visible = 1;
         edtavTfcontratada_lote_Jsonclick = "";
         edtavTfcontratada_lote_Visible = 1;
         edtavTfcontratada_os_to_Jsonclick = "";
         edtavTfcontratada_os_to_Visible = 1;
         edtavTfcontratada_os_Jsonclick = "";
         edtavTfcontratada_os_Visible = 1;
         edtavTfcontratada_ss_to_Jsonclick = "";
         edtavTfcontratada_ss_to_Visible = 1;
         edtavTfcontratada_ss_Jsonclick = "";
         edtavTfcontratada_ss_Visible = 1;
         edtavTfpessoa_telefone_sel_Jsonclick = "";
         edtavTfpessoa_telefone_sel_Visible = 1;
         edtavTfpessoa_telefone_Jsonclick = "";
         edtavTfpessoa_telefone_Visible = 1;
         edtavTfpessoa_ie_sel_Jsonclick = "";
         edtavTfpessoa_ie_sel_Visible = 1;
         edtavTfpessoa_ie_Jsonclick = "";
         edtavTfpessoa_ie_Visible = 1;
         edtavTfcontratada_pessoacnpj_sel_Jsonclick = "";
         edtavTfcontratada_pessoacnpj_sel_Visible = 1;
         edtavTfcontratada_pessoacnpj_Jsonclick = "";
         edtavTfcontratada_pessoacnpj_Visible = 1;
         edtavTfcontratada_pessoanom_sel_Jsonclick = "";
         edtavTfcontratada_pessoanom_sel_Visible = 1;
         edtavTfcontratada_pessoanom_Jsonclick = "";
         edtavTfcontratada_pessoanom_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contratada_ativo_Searchbuttontext = "Pesquisar";
         Ddo_contratada_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_contratada_ativo_Datalisttype = "FixedValues";
         Ddo_contratada_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratada_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratada_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_ativo_Titlecontrolidtoreplace = "";
         Ddo_contratada_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_ativo_Cls = "ColumnSettings";
         Ddo_contratada_ativo_Tooltip = "Op��es";
         Ddo_contratada_ativo_Caption = "";
         Ddo_contratada_lote_Searchbuttontext = "Pesquisar";
         Ddo_contratada_lote_Rangefilterto = "At�";
         Ddo_contratada_lote_Rangefilterfrom = "Desde";
         Ddo_contratada_lote_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_lote_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_lote_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_lote_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratada_lote_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratada_lote_Filtertype = "Numeric";
         Ddo_contratada_lote_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_lote_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_lote_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_lote_Titlecontrolidtoreplace = "";
         Ddo_contratada_lote_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_lote_Cls = "ColumnSettings";
         Ddo_contratada_lote_Tooltip = "Op��es";
         Ddo_contratada_lote_Caption = "";
         Ddo_contratada_os_Searchbuttontext = "Pesquisar";
         Ddo_contratada_os_Rangefilterto = "At�";
         Ddo_contratada_os_Rangefilterfrom = "Desde";
         Ddo_contratada_os_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_os_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_os_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_os_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratada_os_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratada_os_Filtertype = "Numeric";
         Ddo_contratada_os_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_os_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_os_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_os_Titlecontrolidtoreplace = "";
         Ddo_contratada_os_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_os_Cls = "ColumnSettings";
         Ddo_contratada_os_Tooltip = "Op��es";
         Ddo_contratada_os_Caption = "";
         Ddo_contratada_ss_Searchbuttontext = "Pesquisar";
         Ddo_contratada_ss_Rangefilterto = "At�";
         Ddo_contratada_ss_Rangefilterfrom = "Desde";
         Ddo_contratada_ss_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_ss_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_ss_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_ss_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratada_ss_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratada_ss_Filtertype = "Numeric";
         Ddo_contratada_ss_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_ss_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_ss_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_ss_Titlecontrolidtoreplace = "";
         Ddo_contratada_ss_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_ss_Cls = "ColumnSettings";
         Ddo_contratada_ss_Tooltip = "Op��es";
         Ddo_contratada_ss_Caption = "";
         Ddo_pessoa_telefone_Searchbuttontext = "Pesquisar";
         Ddo_pessoa_telefone_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_pessoa_telefone_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoa_telefone_Loadingdata = "Carregando dados...";
         Ddo_pessoa_telefone_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoa_telefone_Sortasc = "Ordenar de A � Z";
         Ddo_pessoa_telefone_Datalistupdateminimumcharacters = 0;
         Ddo_pessoa_telefone_Datalistproc = "GetWWContratadaFilterData";
         Ddo_pessoa_telefone_Datalisttype = "Dynamic";
         Ddo_pessoa_telefone_Includedatalist = Convert.ToBoolean( -1);
         Ddo_pessoa_telefone_Filterisrange = Convert.ToBoolean( 0);
         Ddo_pessoa_telefone_Filtertype = "Character";
         Ddo_pessoa_telefone_Includefilter = Convert.ToBoolean( -1);
         Ddo_pessoa_telefone_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoa_telefone_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoa_telefone_Titlecontrolidtoreplace = "";
         Ddo_pessoa_telefone_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoa_telefone_Cls = "ColumnSettings";
         Ddo_pessoa_telefone_Tooltip = "Op��es";
         Ddo_pessoa_telefone_Caption = "";
         Ddo_pessoa_ie_Searchbuttontext = "Pesquisar";
         Ddo_pessoa_ie_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_pessoa_ie_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoa_ie_Loadingdata = "Carregando dados...";
         Ddo_pessoa_ie_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoa_ie_Sortasc = "Ordenar de A � Z";
         Ddo_pessoa_ie_Datalistupdateminimumcharacters = 0;
         Ddo_pessoa_ie_Datalistproc = "GetWWContratadaFilterData";
         Ddo_pessoa_ie_Datalisttype = "Dynamic";
         Ddo_pessoa_ie_Includedatalist = Convert.ToBoolean( -1);
         Ddo_pessoa_ie_Filterisrange = Convert.ToBoolean( 0);
         Ddo_pessoa_ie_Filtertype = "Character";
         Ddo_pessoa_ie_Includefilter = Convert.ToBoolean( -1);
         Ddo_pessoa_ie_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoa_ie_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoa_ie_Titlecontrolidtoreplace = "";
         Ddo_pessoa_ie_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoa_ie_Cls = "ColumnSettings";
         Ddo_pessoa_ie_Tooltip = "Op��es";
         Ddo_pessoa_ie_Caption = "";
         Ddo_contratada_pessoacnpj_Searchbuttontext = "Pesquisar";
         Ddo_contratada_pessoacnpj_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratada_pessoacnpj_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_pessoacnpj_Loadingdata = "Carregando dados...";
         Ddo_contratada_pessoacnpj_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_pessoacnpj_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters = 0;
         Ddo_contratada_pessoacnpj_Datalistproc = "GetWWContratadaFilterData";
         Ddo_contratada_pessoacnpj_Datalisttype = "Dynamic";
         Ddo_contratada_pessoacnpj_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratada_pessoacnpj_Filtertype = "Character";
         Ddo_contratada_pessoacnpj_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace = "";
         Ddo_contratada_pessoacnpj_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_pessoacnpj_Cls = "ColumnSettings";
         Ddo_contratada_pessoacnpj_Tooltip = "Op��es";
         Ddo_contratada_pessoacnpj_Caption = "";
         Ddo_contratada_pessoanom_Searchbuttontext = "Pesquisar";
         Ddo_contratada_pessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratada_pessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_pessoanom_Loadingdata = "Carregando dados...";
         Ddo_contratada_pessoanom_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_pessoanom_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_pessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_contratada_pessoanom_Datalistproc = "GetWWContratadaFilterData";
         Ddo_contratada_pessoanom_Datalisttype = "Dynamic";
         Ddo_contratada_pessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratada_pessoanom_Filtertype = "Character";
         Ddo_contratada_pessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Titlecontrolidtoreplace = "";
         Ddo_contratada_pessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_pessoanom_Cls = "ColumnSettings";
         Ddo_contratada_pessoanom_Tooltip = "Op��es";
         Ddo_contratada_pessoanom_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Contratadas";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV82ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV94ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Contratada_SSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_SSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Contratada_OSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_OSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Contratada_LoteTitleControlIdToReplace',fld:'vDDO_CONTRATADA_LOTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV76Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Contratada_PessoaCNPJ1',fld:'vCONTRATADA_PESSOACNPJ1',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV74Contratada_PessoaCNPJ2',fld:'vCONTRATADA_PESSOACNPJ2',pic:'',nv:''},{av:'AV80TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV81TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV84TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV85TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV88TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV89TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV92TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV93TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV96TFContratada_SS',fld:'vTFCONTRATADA_SS',pic:'ZZZZZZZ9',nv:0},{av:'AV97TFContratada_SS_To',fld:'vTFCONTRATADA_SS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV100TFContratada_OS',fld:'vTFCONTRATADA_OS',pic:'ZZZZZZZ9',nv:0},{av:'AV101TFContratada_OS_To',fld:'vTFCONTRATADA_OS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV104TFContratada_Lote',fld:'vTFCONTRATADA_LOTE',pic:'ZZZ9',nv:0},{av:'AV105TFContratada_Lote_To',fld:'vTFCONTRATADA_LOTE_TO',pic:'ZZZ9',nv:0},{av:'AV108TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV144Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV79Contratada_PessoaNomTitleFilterData',fld:'vCONTRATADA_PESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV83Contratada_PessoaCNPJTitleFilterData',fld:'vCONTRATADA_PESSOACNPJTITLEFILTERDATA',pic:'',nv:null},{av:'AV87Pessoa_IETitleFilterData',fld:'vPESSOA_IETITLEFILTERDATA',pic:'',nv:null},{av:'AV91Pessoa_TelefoneTitleFilterData',fld:'vPESSOA_TELEFONETITLEFILTERDATA',pic:'',nv:null},{av:'AV95Contratada_SSTitleFilterData',fld:'vCONTRATADA_SSTITLEFILTERDATA',pic:'',nv:null},{av:'AV99Contratada_OSTitleFilterData',fld:'vCONTRATADA_OSTITLEFILTERDATA',pic:'',nv:null},{av:'AV103Contratada_LoteTitleFilterData',fld:'vCONTRATADA_LOTETITLEFILTERDATA',pic:'',nv:null},{av:'AV107Contratada_AtivoTitleFilterData',fld:'vCONTRATADA_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContratada_PessoaNom_Titleformat',ctrl:'CONTRATADA_PESSOANOM',prop:'Titleformat'},{av:'edtContratada_PessoaNom_Title',ctrl:'CONTRATADA_PESSOANOM',prop:'Title'},{av:'edtContratada_PessoaCNPJ_Titleformat',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Titleformat'},{av:'edtContratada_PessoaCNPJ_Title',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Title'},{av:'edtPessoa_IE_Titleformat',ctrl:'PESSOA_IE',prop:'Titleformat'},{av:'edtPessoa_IE_Title',ctrl:'PESSOA_IE',prop:'Title'},{av:'edtPessoa_Telefone_Titleformat',ctrl:'PESSOA_TELEFONE',prop:'Titleformat'},{av:'edtPessoa_Telefone_Title',ctrl:'PESSOA_TELEFONE',prop:'Title'},{av:'edtContratada_SS_Titleformat',ctrl:'CONTRATADA_SS',prop:'Titleformat'},{av:'edtContratada_SS_Title',ctrl:'CONTRATADA_SS',prop:'Title'},{av:'edtContratada_OS_Titleformat',ctrl:'CONTRATADA_OS',prop:'Titleformat'},{av:'edtContratada_OS_Title',ctrl:'CONTRATADA_OS',prop:'Title'},{av:'edtContratada_Lote_Titleformat',ctrl:'CONTRATADA_LOTE',prop:'Titleformat'},{av:'edtContratada_Lote_Title',ctrl:'CONTRATADA_LOTE',prop:'Title'},{av:'chkContratada_Ativo_Titleformat',ctrl:'CONTRATADA_ATIVO',prop:'Titleformat'},{av:'chkContratada_Ativo.Title.Text',ctrl:'CONTRATADA_ATIVO',prop:'Title'},{av:'AV112GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV113GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtContratada_OS_Visible',ctrl:'CONTRATADA_OS',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'edtavDisplay_Visible',ctrl:'vDISPLAY',prop:'Visible'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11352',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Contratada_PessoaCNPJ1',fld:'vCONTRATADA_PESSOACNPJ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV74Contratada_PessoaCNPJ2',fld:'vCONTRATADA_PESSOACNPJ2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV80TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV81TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV84TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV85TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV88TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV89TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV92TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV93TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV96TFContratada_SS',fld:'vTFCONTRATADA_SS',pic:'ZZZZZZZ9',nv:0},{av:'AV97TFContratada_SS_To',fld:'vTFCONTRATADA_SS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV100TFContratada_OS',fld:'vTFCONTRATADA_OS',pic:'ZZZZZZZ9',nv:0},{av:'AV101TFContratada_OS_To',fld:'vTFCONTRATADA_OS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV104TFContratada_Lote',fld:'vTFCONTRATADA_LOTE',pic:'ZZZ9',nv:0},{av:'AV105TFContratada_Lote_To',fld:'vTFCONTRATADA_LOTE_TO',pic:'ZZZ9',nv:0},{av:'AV108TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV82ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV94ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Contratada_SSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_SSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Contratada_OSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_OSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Contratada_LoteTitleControlIdToReplace',fld:'vDDO_CONTRATADA_LOTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV76Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV144Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED","{handler:'E12352',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Contratada_PessoaCNPJ1',fld:'vCONTRATADA_PESSOACNPJ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV74Contratada_PessoaCNPJ2',fld:'vCONTRATADA_PESSOACNPJ2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV80TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV81TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV84TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV85TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV88TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV89TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV92TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV93TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV96TFContratada_SS',fld:'vTFCONTRATADA_SS',pic:'ZZZZZZZ9',nv:0},{av:'AV97TFContratada_SS_To',fld:'vTFCONTRATADA_SS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV100TFContratada_OS',fld:'vTFCONTRATADA_OS',pic:'ZZZZZZZ9',nv:0},{av:'AV101TFContratada_OS_To',fld:'vTFCONTRATADA_OS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV104TFContratada_Lote',fld:'vTFCONTRATADA_LOTE',pic:'ZZZ9',nv:0},{av:'AV105TFContratada_Lote_To',fld:'vTFCONTRATADA_LOTE_TO',pic:'ZZZ9',nv:0},{av:'AV108TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV82ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV94ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Contratada_SSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_SSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Contratada_OSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_OSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Contratada_LoteTitleControlIdToReplace',fld:'vDDO_CONTRATADA_LOTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV76Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV144Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'Ddo_contratada_pessoanom_Activeeventkey',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_contratada_pessoanom_Filteredtext_get',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'FilteredText_get'},{av:'Ddo_contratada_pessoanom_Selectedvalue_get',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'AV80TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV81TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'Ddo_contratada_ss_Sortedstatus',ctrl:'DDO_CONTRATADA_SS',prop:'SortedStatus'},{av:'Ddo_contratada_os_Sortedstatus',ctrl:'DDO_CONTRATADA_OS',prop:'SortedStatus'},{av:'Ddo_contratada_lote_Sortedstatus',ctrl:'DDO_CONTRATADA_LOTE',prop:'SortedStatus'},{av:'Ddo_contratada_ativo_Sortedstatus',ctrl:'DDO_CONTRATADA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_PESSOACNPJ.ONOPTIONCLICKED","{handler:'E13352',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Contratada_PessoaCNPJ1',fld:'vCONTRATADA_PESSOACNPJ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV74Contratada_PessoaCNPJ2',fld:'vCONTRATADA_PESSOACNPJ2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV80TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV81TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV84TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV85TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV88TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV89TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV92TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV93TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV96TFContratada_SS',fld:'vTFCONTRATADA_SS',pic:'ZZZZZZZ9',nv:0},{av:'AV97TFContratada_SS_To',fld:'vTFCONTRATADA_SS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV100TFContratada_OS',fld:'vTFCONTRATADA_OS',pic:'ZZZZZZZ9',nv:0},{av:'AV101TFContratada_OS_To',fld:'vTFCONTRATADA_OS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV104TFContratada_Lote',fld:'vTFCONTRATADA_LOTE',pic:'ZZZ9',nv:0},{av:'AV105TFContratada_Lote_To',fld:'vTFCONTRATADA_LOTE_TO',pic:'ZZZ9',nv:0},{av:'AV108TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV82ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV94ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Contratada_SSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_SSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Contratada_OSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_OSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Contratada_LoteTitleControlIdToReplace',fld:'vDDO_CONTRATADA_LOTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV76Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV144Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'Ddo_contratada_pessoacnpj_Activeeventkey',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'ActiveEventKey'},{av:'Ddo_contratada_pessoacnpj_Filteredtext_get',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'FilteredText_get'},{av:'Ddo_contratada_pessoacnpj_Selectedvalue_get',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'AV84TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV85TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'Ddo_contratada_ss_Sortedstatus',ctrl:'DDO_CONTRATADA_SS',prop:'SortedStatus'},{av:'Ddo_contratada_os_Sortedstatus',ctrl:'DDO_CONTRATADA_OS',prop:'SortedStatus'},{av:'Ddo_contratada_lote_Sortedstatus',ctrl:'DDO_CONTRATADA_LOTE',prop:'SortedStatus'},{av:'Ddo_contratada_ativo_Sortedstatus',ctrl:'DDO_CONTRATADA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PESSOA_IE.ONOPTIONCLICKED","{handler:'E14352',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Contratada_PessoaCNPJ1',fld:'vCONTRATADA_PESSOACNPJ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV74Contratada_PessoaCNPJ2',fld:'vCONTRATADA_PESSOACNPJ2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV80TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV81TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV84TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV85TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV88TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV89TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV92TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV93TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV96TFContratada_SS',fld:'vTFCONTRATADA_SS',pic:'ZZZZZZZ9',nv:0},{av:'AV97TFContratada_SS_To',fld:'vTFCONTRATADA_SS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV100TFContratada_OS',fld:'vTFCONTRATADA_OS',pic:'ZZZZZZZ9',nv:0},{av:'AV101TFContratada_OS_To',fld:'vTFCONTRATADA_OS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV104TFContratada_Lote',fld:'vTFCONTRATADA_LOTE',pic:'ZZZ9',nv:0},{av:'AV105TFContratada_Lote_To',fld:'vTFCONTRATADA_LOTE_TO',pic:'ZZZ9',nv:0},{av:'AV108TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV82ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV94ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Contratada_SSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_SSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Contratada_OSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_OSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Contratada_LoteTitleControlIdToReplace',fld:'vDDO_CONTRATADA_LOTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV76Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV144Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'Ddo_pessoa_ie_Activeeventkey',ctrl:'DDO_PESSOA_IE',prop:'ActiveEventKey'},{av:'Ddo_pessoa_ie_Filteredtext_get',ctrl:'DDO_PESSOA_IE',prop:'FilteredText_get'},{av:'Ddo_pessoa_ie_Selectedvalue_get',ctrl:'DDO_PESSOA_IE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'AV88TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV89TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'Ddo_contratada_ss_Sortedstatus',ctrl:'DDO_CONTRATADA_SS',prop:'SortedStatus'},{av:'Ddo_contratada_os_Sortedstatus',ctrl:'DDO_CONTRATADA_OS',prop:'SortedStatus'},{av:'Ddo_contratada_lote_Sortedstatus',ctrl:'DDO_CONTRATADA_LOTE',prop:'SortedStatus'},{av:'Ddo_contratada_ativo_Sortedstatus',ctrl:'DDO_CONTRATADA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PESSOA_TELEFONE.ONOPTIONCLICKED","{handler:'E15352',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Contratada_PessoaCNPJ1',fld:'vCONTRATADA_PESSOACNPJ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV74Contratada_PessoaCNPJ2',fld:'vCONTRATADA_PESSOACNPJ2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV80TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV81TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV84TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV85TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV88TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV89TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV92TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV93TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV96TFContratada_SS',fld:'vTFCONTRATADA_SS',pic:'ZZZZZZZ9',nv:0},{av:'AV97TFContratada_SS_To',fld:'vTFCONTRATADA_SS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV100TFContratada_OS',fld:'vTFCONTRATADA_OS',pic:'ZZZZZZZ9',nv:0},{av:'AV101TFContratada_OS_To',fld:'vTFCONTRATADA_OS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV104TFContratada_Lote',fld:'vTFCONTRATADA_LOTE',pic:'ZZZ9',nv:0},{av:'AV105TFContratada_Lote_To',fld:'vTFCONTRATADA_LOTE_TO',pic:'ZZZ9',nv:0},{av:'AV108TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV82ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV94ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Contratada_SSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_SSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Contratada_OSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_OSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Contratada_LoteTitleControlIdToReplace',fld:'vDDO_CONTRATADA_LOTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV76Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV144Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'Ddo_pessoa_telefone_Activeeventkey',ctrl:'DDO_PESSOA_TELEFONE',prop:'ActiveEventKey'},{av:'Ddo_pessoa_telefone_Filteredtext_get',ctrl:'DDO_PESSOA_TELEFONE',prop:'FilteredText_get'},{av:'Ddo_pessoa_telefone_Selectedvalue_get',ctrl:'DDO_PESSOA_TELEFONE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'AV92TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV93TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'Ddo_contratada_ss_Sortedstatus',ctrl:'DDO_CONTRATADA_SS',prop:'SortedStatus'},{av:'Ddo_contratada_os_Sortedstatus',ctrl:'DDO_CONTRATADA_OS',prop:'SortedStatus'},{av:'Ddo_contratada_lote_Sortedstatus',ctrl:'DDO_CONTRATADA_LOTE',prop:'SortedStatus'},{av:'Ddo_contratada_ativo_Sortedstatus',ctrl:'DDO_CONTRATADA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_SS.ONOPTIONCLICKED","{handler:'E16352',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Contratada_PessoaCNPJ1',fld:'vCONTRATADA_PESSOACNPJ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV74Contratada_PessoaCNPJ2',fld:'vCONTRATADA_PESSOACNPJ2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV80TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV81TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV84TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV85TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV88TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV89TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV92TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV93TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV96TFContratada_SS',fld:'vTFCONTRATADA_SS',pic:'ZZZZZZZ9',nv:0},{av:'AV97TFContratada_SS_To',fld:'vTFCONTRATADA_SS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV100TFContratada_OS',fld:'vTFCONTRATADA_OS',pic:'ZZZZZZZ9',nv:0},{av:'AV101TFContratada_OS_To',fld:'vTFCONTRATADA_OS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV104TFContratada_Lote',fld:'vTFCONTRATADA_LOTE',pic:'ZZZ9',nv:0},{av:'AV105TFContratada_Lote_To',fld:'vTFCONTRATADA_LOTE_TO',pic:'ZZZ9',nv:0},{av:'AV108TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV82ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV94ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Contratada_SSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_SSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Contratada_OSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_OSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Contratada_LoteTitleControlIdToReplace',fld:'vDDO_CONTRATADA_LOTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV76Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV144Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'Ddo_contratada_ss_Activeeventkey',ctrl:'DDO_CONTRATADA_SS',prop:'ActiveEventKey'},{av:'Ddo_contratada_ss_Filteredtext_get',ctrl:'DDO_CONTRATADA_SS',prop:'FilteredText_get'},{av:'Ddo_contratada_ss_Filteredtextto_get',ctrl:'DDO_CONTRATADA_SS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_ss_Sortedstatus',ctrl:'DDO_CONTRATADA_SS',prop:'SortedStatus'},{av:'AV96TFContratada_SS',fld:'vTFCONTRATADA_SS',pic:'ZZZZZZZ9',nv:0},{av:'AV97TFContratada_SS_To',fld:'vTFCONTRATADA_SS_TO',pic:'ZZZZZZZ9',nv:0},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'Ddo_contratada_os_Sortedstatus',ctrl:'DDO_CONTRATADA_OS',prop:'SortedStatus'},{av:'Ddo_contratada_lote_Sortedstatus',ctrl:'DDO_CONTRATADA_LOTE',prop:'SortedStatus'},{av:'Ddo_contratada_ativo_Sortedstatus',ctrl:'DDO_CONTRATADA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_OS.ONOPTIONCLICKED","{handler:'E17352',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Contratada_PessoaCNPJ1',fld:'vCONTRATADA_PESSOACNPJ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV74Contratada_PessoaCNPJ2',fld:'vCONTRATADA_PESSOACNPJ2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV80TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV81TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV84TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV85TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV88TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV89TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV92TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV93TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV96TFContratada_SS',fld:'vTFCONTRATADA_SS',pic:'ZZZZZZZ9',nv:0},{av:'AV97TFContratada_SS_To',fld:'vTFCONTRATADA_SS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV100TFContratada_OS',fld:'vTFCONTRATADA_OS',pic:'ZZZZZZZ9',nv:0},{av:'AV101TFContratada_OS_To',fld:'vTFCONTRATADA_OS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV104TFContratada_Lote',fld:'vTFCONTRATADA_LOTE',pic:'ZZZ9',nv:0},{av:'AV105TFContratada_Lote_To',fld:'vTFCONTRATADA_LOTE_TO',pic:'ZZZ9',nv:0},{av:'AV108TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV82ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV94ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Contratada_SSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_SSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Contratada_OSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_OSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Contratada_LoteTitleControlIdToReplace',fld:'vDDO_CONTRATADA_LOTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV76Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV144Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'Ddo_contratada_os_Activeeventkey',ctrl:'DDO_CONTRATADA_OS',prop:'ActiveEventKey'},{av:'Ddo_contratada_os_Filteredtext_get',ctrl:'DDO_CONTRATADA_OS',prop:'FilteredText_get'},{av:'Ddo_contratada_os_Filteredtextto_get',ctrl:'DDO_CONTRATADA_OS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_os_Sortedstatus',ctrl:'DDO_CONTRATADA_OS',prop:'SortedStatus'},{av:'AV100TFContratada_OS',fld:'vTFCONTRATADA_OS',pic:'ZZZZZZZ9',nv:0},{av:'AV101TFContratada_OS_To',fld:'vTFCONTRATADA_OS_TO',pic:'ZZZZZZZ9',nv:0},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'Ddo_contratada_ss_Sortedstatus',ctrl:'DDO_CONTRATADA_SS',prop:'SortedStatus'},{av:'Ddo_contratada_lote_Sortedstatus',ctrl:'DDO_CONTRATADA_LOTE',prop:'SortedStatus'},{av:'Ddo_contratada_ativo_Sortedstatus',ctrl:'DDO_CONTRATADA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_LOTE.ONOPTIONCLICKED","{handler:'E18352',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Contratada_PessoaCNPJ1',fld:'vCONTRATADA_PESSOACNPJ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV74Contratada_PessoaCNPJ2',fld:'vCONTRATADA_PESSOACNPJ2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV80TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV81TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV84TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV85TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV88TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV89TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV92TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV93TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV96TFContratada_SS',fld:'vTFCONTRATADA_SS',pic:'ZZZZZZZ9',nv:0},{av:'AV97TFContratada_SS_To',fld:'vTFCONTRATADA_SS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV100TFContratada_OS',fld:'vTFCONTRATADA_OS',pic:'ZZZZZZZ9',nv:0},{av:'AV101TFContratada_OS_To',fld:'vTFCONTRATADA_OS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV104TFContratada_Lote',fld:'vTFCONTRATADA_LOTE',pic:'ZZZ9',nv:0},{av:'AV105TFContratada_Lote_To',fld:'vTFCONTRATADA_LOTE_TO',pic:'ZZZ9',nv:0},{av:'AV108TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV82ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV94ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Contratada_SSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_SSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Contratada_OSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_OSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Contratada_LoteTitleControlIdToReplace',fld:'vDDO_CONTRATADA_LOTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV76Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV144Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'Ddo_contratada_lote_Activeeventkey',ctrl:'DDO_CONTRATADA_LOTE',prop:'ActiveEventKey'},{av:'Ddo_contratada_lote_Filteredtext_get',ctrl:'DDO_CONTRATADA_LOTE',prop:'FilteredText_get'},{av:'Ddo_contratada_lote_Filteredtextto_get',ctrl:'DDO_CONTRATADA_LOTE',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_lote_Sortedstatus',ctrl:'DDO_CONTRATADA_LOTE',prop:'SortedStatus'},{av:'AV104TFContratada_Lote',fld:'vTFCONTRATADA_LOTE',pic:'ZZZ9',nv:0},{av:'AV105TFContratada_Lote_To',fld:'vTFCONTRATADA_LOTE_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'Ddo_contratada_ss_Sortedstatus',ctrl:'DDO_CONTRATADA_SS',prop:'SortedStatus'},{av:'Ddo_contratada_os_Sortedstatus',ctrl:'DDO_CONTRATADA_OS',prop:'SortedStatus'},{av:'Ddo_contratada_ativo_Sortedstatus',ctrl:'DDO_CONTRATADA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_ATIVO.ONOPTIONCLICKED","{handler:'E19352',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Contratada_PessoaCNPJ1',fld:'vCONTRATADA_PESSOACNPJ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV74Contratada_PessoaCNPJ2',fld:'vCONTRATADA_PESSOACNPJ2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV80TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV81TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV84TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV85TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV88TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV89TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV92TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV93TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV96TFContratada_SS',fld:'vTFCONTRATADA_SS',pic:'ZZZZZZZ9',nv:0},{av:'AV97TFContratada_SS_To',fld:'vTFCONTRATADA_SS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV100TFContratada_OS',fld:'vTFCONTRATADA_OS',pic:'ZZZZZZZ9',nv:0},{av:'AV101TFContratada_OS_To',fld:'vTFCONTRATADA_OS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV104TFContratada_Lote',fld:'vTFCONTRATADA_LOTE',pic:'ZZZ9',nv:0},{av:'AV105TFContratada_Lote_To',fld:'vTFCONTRATADA_LOTE_TO',pic:'ZZZ9',nv:0},{av:'AV108TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV82ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV94ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Contratada_SSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_SSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Contratada_OSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_OSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Contratada_LoteTitleControlIdToReplace',fld:'vDDO_CONTRATADA_LOTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV76Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV144Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'Ddo_contratada_ativo_Activeeventkey',ctrl:'DDO_CONTRATADA_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_contratada_ativo_Selectedvalue_get',ctrl:'DDO_CONTRATADA_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_ativo_Sortedstatus',ctrl:'DDO_CONTRATADA_ATIVO',prop:'SortedStatus'},{av:'AV108TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_pessoa_ie_Sortedstatus',ctrl:'DDO_PESSOA_IE',prop:'SortedStatus'},{av:'Ddo_pessoa_telefone_Sortedstatus',ctrl:'DDO_PESSOA_TELEFONE',prop:'SortedStatus'},{av:'Ddo_contratada_ss_Sortedstatus',ctrl:'DDO_CONTRATADA_SS',prop:'SortedStatus'},{av:'Ddo_contratada_os_Sortedstatus',ctrl:'DDO_CONTRATADA_OS',prop:'SortedStatus'},{av:'Ddo_contratada_lote_Sortedstatus',ctrl:'DDO_CONTRATADA_LOTE',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E31352',iparms:[{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV31Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV32Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV75Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'edtContratada_PessoaNom_Forecolor',ctrl:'CONTRATADA_PESSOANOM',prop:'Forecolor'},{av:'edtContratada_PessoaCNPJ_Forecolor',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Forecolor'},{av:'edtPessoa_IE_Forecolor',ctrl:'PESSOA_IE',prop:'Forecolor'},{av:'edtPessoa_Telefone_Forecolor',ctrl:'PESSOA_TELEFONE',prop:'Forecolor'},{av:'edtContratada_SS_Forecolor',ctrl:'CONTRATADA_SS',prop:'Forecolor'},{av:'edtContratada_OS_Forecolor',ctrl:'CONTRATADA_OS',prop:'Forecolor'},{av:'edtContratada_Lote_Forecolor',ctrl:'CONTRATADA_LOTE',prop:'Forecolor'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'imgInsert_Tooltiptext',ctrl:'INSERT',prop:'Tooltiptext'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E20352',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Contratada_PessoaCNPJ1',fld:'vCONTRATADA_PESSOACNPJ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV74Contratada_PessoaCNPJ2',fld:'vCONTRATADA_PESSOACNPJ2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV80TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV81TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV84TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV85TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV88TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV89TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV92TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV93TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV96TFContratada_SS',fld:'vTFCONTRATADA_SS',pic:'ZZZZZZZ9',nv:0},{av:'AV97TFContratada_SS_To',fld:'vTFCONTRATADA_SS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV100TFContratada_OS',fld:'vTFCONTRATADA_OS',pic:'ZZZZZZZ9',nv:0},{av:'AV101TFContratada_OS_To',fld:'vTFCONTRATADA_OS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV104TFContratada_Lote',fld:'vTFCONTRATADA_LOTE',pic:'ZZZ9',nv:0},{av:'AV105TFContratada_Lote_To',fld:'vTFCONTRATADA_LOTE_TO',pic:'ZZZ9',nv:0},{av:'AV108TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV82ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV94ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Contratada_SSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_SSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Contratada_OSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_OSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Contratada_LoteTitleControlIdToReplace',fld:'vDDO_CONTRATADA_LOTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV76Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV144Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E25352',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E21352',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Contratada_PessoaCNPJ1',fld:'vCONTRATADA_PESSOACNPJ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV74Contratada_PessoaCNPJ2',fld:'vCONTRATADA_PESSOACNPJ2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV80TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV81TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV84TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV85TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV88TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV89TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV92TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV93TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV96TFContratada_SS',fld:'vTFCONTRATADA_SS',pic:'ZZZZZZZ9',nv:0},{av:'AV97TFContratada_SS_To',fld:'vTFCONTRATADA_SS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV100TFContratada_OS',fld:'vTFCONTRATADA_OS',pic:'ZZZZZZZ9',nv:0},{av:'AV101TFContratada_OS_To',fld:'vTFCONTRATADA_OS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV104TFContratada_Lote',fld:'vTFCONTRATADA_LOTE',pic:'ZZZ9',nv:0},{av:'AV105TFContratada_Lote_To',fld:'vTFCONTRATADA_LOTE_TO',pic:'ZZZ9',nv:0},{av:'AV108TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV82ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV94ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Contratada_SSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_SSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Contratada_OSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_OSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Contratada_LoteTitleControlIdToReplace',fld:'vDDO_CONTRATADA_LOTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV76Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV144Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Contratada_PessoaCNPJ1',fld:'vCONTRATADA_PESSOACNPJ1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV74Contratada_PessoaCNPJ2',fld:'vCONTRATADA_PESSOACNPJ2',pic:'',nv:''},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'edtavContratada_pessoacnpj2_Visible',ctrl:'vCONTRATADA_PESSOACNPJ2',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'edtavContratada_pessoacnpj1_Visible',ctrl:'vCONTRATADA_PESSOACNPJ1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E26352',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'edtavContratada_pessoacnpj1_Visible',ctrl:'vCONTRATADA_PESSOACNPJ1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E22352',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Contratada_PessoaCNPJ1',fld:'vCONTRATADA_PESSOACNPJ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV74Contratada_PessoaCNPJ2',fld:'vCONTRATADA_PESSOACNPJ2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV80TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV81TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV84TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV85TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV88TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV89TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV92TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV93TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV96TFContratada_SS',fld:'vTFCONTRATADA_SS',pic:'ZZZZZZZ9',nv:0},{av:'AV97TFContratada_SS_To',fld:'vTFCONTRATADA_SS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV100TFContratada_OS',fld:'vTFCONTRATADA_OS',pic:'ZZZZZZZ9',nv:0},{av:'AV101TFContratada_OS_To',fld:'vTFCONTRATADA_OS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV104TFContratada_Lote',fld:'vTFCONTRATADA_LOTE',pic:'ZZZ9',nv:0},{av:'AV105TFContratada_Lote_To',fld:'vTFCONTRATADA_LOTE_TO',pic:'ZZZ9',nv:0},{av:'AV108TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV82ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV94ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Contratada_SSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_SSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Contratada_OSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_OSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Contratada_LoteTitleControlIdToReplace',fld:'vDDO_CONTRATADA_LOTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV76Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV144Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Contratada_PessoaCNPJ1',fld:'vCONTRATADA_PESSOACNPJ1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV74Contratada_PessoaCNPJ2',fld:'vCONTRATADA_PESSOACNPJ2',pic:'',nv:''},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'edtavContratada_pessoacnpj2_Visible',ctrl:'vCONTRATADA_PESSOACNPJ2',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'edtavContratada_pessoacnpj1_Visible',ctrl:'vCONTRATADA_PESSOACNPJ1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E27352',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'edtavContratada_pessoacnpj2_Visible',ctrl:'vCONTRATADA_PESSOACNPJ2',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E23352',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Contratada_PessoaCNPJ1',fld:'vCONTRATADA_PESSOACNPJ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV74Contratada_PessoaCNPJ2',fld:'vCONTRATADA_PESSOACNPJ2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV80TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV81TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV84TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV85TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV88TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV89TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV92TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV93TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV96TFContratada_SS',fld:'vTFCONTRATADA_SS',pic:'ZZZZZZZ9',nv:0},{av:'AV97TFContratada_SS_To',fld:'vTFCONTRATADA_SS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV100TFContratada_OS',fld:'vTFCONTRATADA_OS',pic:'ZZZZZZZ9',nv:0},{av:'AV101TFContratada_OS_To',fld:'vTFCONTRATADA_OS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV104TFContratada_Lote',fld:'vTFCONTRATADA_LOTE',pic:'ZZZ9',nv:0},{av:'AV105TFContratada_Lote_To',fld:'vTFCONTRATADA_LOTE_TO',pic:'ZZZ9',nv:0},{av:'AV108TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV82ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV94ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Contratada_SSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_SSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Contratada_OSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_OSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Contratada_LoteTitleControlIdToReplace',fld:'vDDO_CONTRATADA_LOTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV76Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV144Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV42Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV76Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV80TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'Ddo_contratada_pessoanom_Filteredtext_set',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'FilteredText_set'},{av:'AV81TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratada_pessoanom_Selectedvalue_set',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SelectedValue_set'},{av:'AV84TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'Ddo_contratada_pessoacnpj_Filteredtext_set',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'FilteredText_set'},{av:'AV85TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'Ddo_contratada_pessoacnpj_Selectedvalue_set',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SelectedValue_set'},{av:'AV88TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'Ddo_pessoa_ie_Filteredtext_set',ctrl:'DDO_PESSOA_IE',prop:'FilteredText_set'},{av:'AV89TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'Ddo_pessoa_ie_Selectedvalue_set',ctrl:'DDO_PESSOA_IE',prop:'SelectedValue_set'},{av:'AV92TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'Ddo_pessoa_telefone_Filteredtext_set',ctrl:'DDO_PESSOA_TELEFONE',prop:'FilteredText_set'},{av:'AV93TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'Ddo_pessoa_telefone_Selectedvalue_set',ctrl:'DDO_PESSOA_TELEFONE',prop:'SelectedValue_set'},{av:'AV96TFContratada_SS',fld:'vTFCONTRATADA_SS',pic:'ZZZZZZZ9',nv:0},{av:'Ddo_contratada_ss_Filteredtext_set',ctrl:'DDO_CONTRATADA_SS',prop:'FilteredText_set'},{av:'AV97TFContratada_SS_To',fld:'vTFCONTRATADA_SS_TO',pic:'ZZZZZZZ9',nv:0},{av:'Ddo_contratada_ss_Filteredtextto_set',ctrl:'DDO_CONTRATADA_SS',prop:'FilteredTextTo_set'},{av:'AV100TFContratada_OS',fld:'vTFCONTRATADA_OS',pic:'ZZZZZZZ9',nv:0},{av:'Ddo_contratada_os_Filteredtext_set',ctrl:'DDO_CONTRATADA_OS',prop:'FilteredText_set'},{av:'AV101TFContratada_OS_To',fld:'vTFCONTRATADA_OS_TO',pic:'ZZZZZZZ9',nv:0},{av:'Ddo_contratada_os_Filteredtextto_set',ctrl:'DDO_CONTRATADA_OS',prop:'FilteredTextTo_set'},{av:'AV104TFContratada_Lote',fld:'vTFCONTRATADA_LOTE',pic:'ZZZ9',nv:0},{av:'Ddo_contratada_lote_Filteredtext_set',ctrl:'DDO_CONTRATADA_LOTE',prop:'FilteredText_set'},{av:'AV105TFContratada_Lote_To',fld:'vTFCONTRATADA_LOTE_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contratada_lote_Filteredtextto_set',ctrl:'DDO_CONTRATADA_LOTE',prop:'FilteredTextTo_set'},{av:'AV108TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_contratada_ativo_Selectedvalue_set',ctrl:'DDO_CONTRATADA_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'edtavContratada_pessoacnpj1_Visible',ctrl:'vCONTRATADA_PESSOACNPJ1',prop:'Visible'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV73Contratada_PessoaCNPJ1',fld:'vCONTRATADA_PESSOACNPJ1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV74Contratada_PessoaCNPJ2',fld:'vCONTRATADA_PESSOACNPJ2',pic:'',nv:''},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'edtavContratada_pessoacnpj2_Visible',ctrl:'vCONTRATADA_PESSOACNPJ2',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E24352',iparms:[{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("ONMESSAGE_GX1","{handler:'E28352',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV73Contratada_PessoaCNPJ1',fld:'vCONTRATADA_PESSOACNPJ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV74Contratada_PessoaCNPJ2',fld:'vCONTRATADA_PESSOACNPJ2',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV80TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV81TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV84TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV85TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV88TFPessoa_IE',fld:'vTFPESSOA_IE',pic:'',nv:''},{av:'AV89TFPessoa_IE_Sel',fld:'vTFPESSOA_IE_SEL',pic:'',nv:''},{av:'AV92TFPessoa_Telefone',fld:'vTFPESSOA_TELEFONE',pic:'',nv:''},{av:'AV93TFPessoa_Telefone_Sel',fld:'vTFPESSOA_TELEFONE_SEL',pic:'',nv:''},{av:'AV96TFContratada_SS',fld:'vTFCONTRATADA_SS',pic:'ZZZZZZZ9',nv:0},{av:'AV97TFContratada_SS_To',fld:'vTFCONTRATADA_SS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV100TFContratada_OS',fld:'vTFCONTRATADA_OS',pic:'ZZZZZZZ9',nv:0},{av:'AV101TFContratada_OS_To',fld:'vTFCONTRATADA_OS_TO',pic:'ZZZZZZZ9',nv:0},{av:'AV104TFContratada_Lote',fld:'vTFCONTRATADA_LOTE',pic:'ZZZ9',nv:0},{av:'AV105TFContratada_Lote_To',fld:'vTFCONTRATADA_LOTE_TO',pic:'ZZZ9',nv:0},{av:'AV108TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV82ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90ddo_Pessoa_IETitleControlIdToReplace',fld:'vDDO_PESSOA_IETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV94ddo_Pessoa_TelefoneTitleControlIdToReplace',fld:'vDDO_PESSOA_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Contratada_SSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_SSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Contratada_OSTitleControlIdToReplace',fld:'vDDO_CONTRATADA_OSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_Contratada_LoteTitleControlIdToReplace',fld:'vDDO_CONTRATADA_LOTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV109ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV76Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV144Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',hsh:true,nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV44NotificationInfo',fld:'vNOTIFICATIONINFO',pic:'',nv:null}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV42Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratada_pessoanom_Activeeventkey = "";
         Ddo_contratada_pessoanom_Filteredtext_get = "";
         Ddo_contratada_pessoanom_Selectedvalue_get = "";
         Ddo_contratada_pessoacnpj_Activeeventkey = "";
         Ddo_contratada_pessoacnpj_Filteredtext_get = "";
         Ddo_contratada_pessoacnpj_Selectedvalue_get = "";
         Ddo_pessoa_ie_Activeeventkey = "";
         Ddo_pessoa_ie_Filteredtext_get = "";
         Ddo_pessoa_ie_Selectedvalue_get = "";
         Ddo_pessoa_telefone_Activeeventkey = "";
         Ddo_pessoa_telefone_Filteredtext_get = "";
         Ddo_pessoa_telefone_Selectedvalue_get = "";
         Ddo_contratada_ss_Activeeventkey = "";
         Ddo_contratada_ss_Filteredtext_get = "";
         Ddo_contratada_ss_Filteredtextto_get = "";
         Ddo_contratada_os_Activeeventkey = "";
         Ddo_contratada_os_Filteredtext_get = "";
         Ddo_contratada_os_Filteredtextto_get = "";
         Ddo_contratada_lote_Activeeventkey = "";
         Ddo_contratada_lote_Filteredtext_get = "";
         Ddo_contratada_lote_Filteredtextto_get = "";
         Ddo_contratada_ativo_Activeeventkey = "";
         Ddo_contratada_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV15DynamicFiltersSelector1 = "";
         AV17Contratada_PessoaNom1 = "";
         AV73Contratada_PessoaCNPJ1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV22Contratada_PessoaNom2 = "";
         AV74Contratada_PessoaCNPJ2 = "";
         AV80TFContratada_PessoaNom = "";
         AV81TFContratada_PessoaNom_Sel = "";
         AV84TFContratada_PessoaCNPJ = "";
         AV85TFContratada_PessoaCNPJ_Sel = "";
         AV88TFPessoa_IE = "";
         AV89TFPessoa_IE_Sel = "";
         AV92TFPessoa_Telefone = "";
         AV93TFPessoa_Telefone_Sel = "";
         AV82ddo_Contratada_PessoaNomTitleControlIdToReplace = "";
         AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace = "";
         AV90ddo_Pessoa_IETitleControlIdToReplace = "";
         AV94ddo_Pessoa_TelefoneTitleControlIdToReplace = "";
         AV98ddo_Contratada_SSTitleControlIdToReplace = "";
         AV102ddo_Contratada_OSTitleControlIdToReplace = "";
         AV106ddo_Contratada_LoteTitleControlIdToReplace = "";
         AV109ddo_Contratada_AtivoTitleControlIdToReplace = "";
         AV144Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV110DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV79Contratada_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV83Contratada_PessoaCNPJTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV87Pessoa_IETitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV91Pessoa_TelefoneTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV95Contratada_SSTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV99Contratada_OSTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV103Contratada_LoteTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV107Contratada_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44NotificationInfo = new SdtNotificationInfo(context);
         Ddo_contratada_pessoanom_Filteredtext_set = "";
         Ddo_contratada_pessoanom_Selectedvalue_set = "";
         Ddo_contratada_pessoanom_Sortedstatus = "";
         Ddo_contratada_pessoacnpj_Filteredtext_set = "";
         Ddo_contratada_pessoacnpj_Selectedvalue_set = "";
         Ddo_contratada_pessoacnpj_Sortedstatus = "";
         Ddo_pessoa_ie_Filteredtext_set = "";
         Ddo_pessoa_ie_Selectedvalue_set = "";
         Ddo_pessoa_ie_Sortedstatus = "";
         Ddo_pessoa_telefone_Filteredtext_set = "";
         Ddo_pessoa_telefone_Selectedvalue_set = "";
         Ddo_pessoa_telefone_Sortedstatus = "";
         Ddo_contratada_ss_Filteredtext_set = "";
         Ddo_contratada_ss_Filteredtextto_set = "";
         Ddo_contratada_ss_Sortedstatus = "";
         Ddo_contratada_os_Filteredtext_set = "";
         Ddo_contratada_os_Filteredtextto_set = "";
         Ddo_contratada_os_Sortedstatus = "";
         Ddo_contratada_lote_Filteredtext_set = "";
         Ddo_contratada_lote_Filteredtextto_set = "";
         Ddo_contratada_lote_Sortedstatus = "";
         Ddo_contratada_ativo_Selectedvalue_set = "";
         Ddo_contratada_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Update = "";
         AV141Update_GXI = "";
         AV32Delete = "";
         AV142Delete_GXI = "";
         AV75Display = "";
         AV143Display_GXI = "";
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         A518Pessoa_IE = "";
         A522Pessoa_Telefone = "";
         scmdbuf = "";
         H00352_A5AreaTrabalho_Codigo = new int[1] ;
         H00352_A6AreaTrabalho_Descricao = new String[] {""} ;
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H00353_A5AreaTrabalho_Codigo = new int[1] ;
         H00353_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00354_A40Contratada_PessoaCod = new int[1] ;
         H00354_A39Contratada_Codigo = new int[1] ;
         H00354_A41Contratada_PessoaNom = new String[] {""} ;
         H00354_n41Contratada_PessoaNom = new bool[] {false} ;
         H00354_A52Contratada_AreaTrabalhoCod = new int[1] ;
         GridContainer = new GXWebGrid( context);
         lV120WWContratadaDS_4_Contratada_pessoanom1 = "";
         lV121WWContratadaDS_5_Contratada_pessoacnpj1 = "";
         lV124WWContratadaDS_8_Contratada_pessoanom2 = "";
         lV125WWContratadaDS_9_Contratada_pessoacnpj2 = "";
         lV126WWContratadaDS_10_Tfcontratada_pessoanom = "";
         lV128WWContratadaDS_12_Tfcontratada_pessoacnpj = "";
         lV130WWContratadaDS_14_Tfpessoa_ie = "";
         lV132WWContratadaDS_16_Tfpessoa_telefone = "";
         AV119WWContratadaDS_3_Dynamicfiltersselector1 = "";
         AV120WWContratadaDS_4_Contratada_pessoanom1 = "";
         AV121WWContratadaDS_5_Contratada_pessoacnpj1 = "";
         AV123WWContratadaDS_7_Dynamicfiltersselector2 = "";
         AV124WWContratadaDS_8_Contratada_pessoanom2 = "";
         AV125WWContratadaDS_9_Contratada_pessoacnpj2 = "";
         AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel = "";
         AV126WWContratadaDS_10_Tfcontratada_pessoanom = "";
         AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel = "";
         AV128WWContratadaDS_12_Tfcontratada_pessoacnpj = "";
         AV131WWContratadaDS_15_Tfpessoa_ie_sel = "";
         AV130WWContratadaDS_14_Tfpessoa_ie = "";
         AV133WWContratadaDS_17_Tfpessoa_telefone_sel = "";
         AV132WWContratadaDS_16_Tfpessoa_telefone = "";
         H00355_A43Contratada_Ativo = new bool[] {false} ;
         H00355_A530Contratada_Lote = new short[1] ;
         H00355_n530Contratada_Lote = new bool[] {false} ;
         H00355_A524Contratada_OS = new int[1] ;
         H00355_n524Contratada_OS = new bool[] {false} ;
         H00355_A1451Contratada_SS = new int[1] ;
         H00355_n1451Contratada_SS = new bool[] {false} ;
         H00355_A522Pessoa_Telefone = new String[] {""} ;
         H00355_n522Pessoa_Telefone = new bool[] {false} ;
         H00355_A518Pessoa_IE = new String[] {""} ;
         H00355_n518Pessoa_IE = new bool[] {false} ;
         H00355_A42Contratada_PessoaCNPJ = new String[] {""} ;
         H00355_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         H00355_A41Contratada_PessoaNom = new String[] {""} ;
         H00355_n41Contratada_PessoaNom = new bool[] {false} ;
         H00355_A40Contratada_PessoaCod = new int[1] ;
         H00355_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00355_A39Contratada_Codigo = new int[1] ;
         H00356_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV35Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContratadatitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblFiltertextcontratada_areatrabalhocod_Jsonclick = "";
         lblFiltertextcontratada_codigo_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontratada__default(),
            new Object[][] {
                new Object[] {
               H00352_A5AreaTrabalho_Codigo, H00352_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00353_A5AreaTrabalho_Codigo, H00353_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00354_A40Contratada_PessoaCod, H00354_A39Contratada_Codigo, H00354_A41Contratada_PessoaNom, H00354_n41Contratada_PessoaNom, H00354_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00355_A43Contratada_Ativo, H00355_A530Contratada_Lote, H00355_n530Contratada_Lote, H00355_A524Contratada_OS, H00355_n524Contratada_OS, H00355_A1451Contratada_SS, H00355_n1451Contratada_SS, H00355_A522Pessoa_Telefone, H00355_n522Pessoa_Telefone, H00355_A518Pessoa_IE,
               H00355_n518Pessoa_IE, H00355_A42Contratada_PessoaCNPJ, H00355_n42Contratada_PessoaCNPJ, H00355_A41Contratada_PessoaNom, H00355_n41Contratada_PessoaNom, H00355_A40Contratada_PessoaCod, H00355_A52Contratada_AreaTrabalhoCod, H00355_A39Contratada_Codigo
               }
               , new Object[] {
               H00356_AGRID_nRecordCount
               }
            }
         );
         AV144Pgmname = "WWContratada";
         /* GeneXus formulas. */
         AV144Pgmname = "WWContratada";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_77 ;
      private short nGXsfl_77_idx=1 ;
      private short AV13OrderedBy ;
      private short AV104TFContratada_Lote ;
      private short AV105TFContratada_Lote_To ;
      private short AV108TFContratada_Ativo_Sel ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A530Contratada_Lote ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_77_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV138WWContratadaDS_22_Tfcontratada_lote ;
      private short AV139WWContratadaDS_23_Tfcontratada_lote_to ;
      private short AV140WWContratadaDS_24_Tfcontratada_ativo_sel ;
      private short edtContratada_PessoaNom_Titleformat ;
      private short edtContratada_PessoaCNPJ_Titleformat ;
      private short edtPessoa_IE_Titleformat ;
      private short edtPessoa_Telefone_Titleformat ;
      private short edtContratada_SS_Titleformat ;
      private short edtContratada_OS_Titleformat ;
      private short edtContratada_Lote_Titleformat ;
      private short chkContratada_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV96TFContratada_SS ;
      private int AV97TFContratada_SS_To ;
      private int AV100TFContratada_OS ;
      private int AV101TFContratada_OS_To ;
      private int AV42Contratada_AreaTrabalhoCod ;
      private int AV76Contratada_Codigo ;
      private int A39Contratada_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contratada_pessoanom_Datalistupdateminimumcharacters ;
      private int Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters ;
      private int Ddo_pessoa_ie_Datalistupdateminimumcharacters ;
      private int Ddo_pessoa_telefone_Datalistupdateminimumcharacters ;
      private int edtavTfcontratada_pessoanom_Visible ;
      private int edtavTfcontratada_pessoanom_sel_Visible ;
      private int edtavTfcontratada_pessoacnpj_Visible ;
      private int edtavTfcontratada_pessoacnpj_sel_Visible ;
      private int edtavTfpessoa_ie_Visible ;
      private int edtavTfpessoa_ie_sel_Visible ;
      private int edtavTfpessoa_telefone_Visible ;
      private int edtavTfpessoa_telefone_sel_Visible ;
      private int edtavTfcontratada_ss_Visible ;
      private int edtavTfcontratada_ss_to_Visible ;
      private int edtavTfcontratada_os_Visible ;
      private int edtavTfcontratada_os_to_Visible ;
      private int edtavTfcontratada_lote_Visible ;
      private int edtavTfcontratada_lote_to_Visible ;
      private int edtavTfcontratada_ativo_sel_Visible ;
      private int edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_pessoa_ietitlecontrolidtoreplace_Visible ;
      private int edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_sstitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_ostitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_lotetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_ativotitlecontrolidtoreplace_Visible ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A40Contratada_PessoaCod ;
      private int A1451Contratada_SS ;
      private int A524Contratada_OS ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV6WWPContext_gxTpr_Contratada_codigo ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int AV134WWContratadaDS_18_Tfcontratada_ss ;
      private int AV135WWContratadaDS_19_Tfcontratada_ss_to ;
      private int AV136WWContratadaDS_20_Tfcontratada_os ;
      private int AV137WWContratadaDS_21_Tfcontratada_os_to ;
      private int AV117WWContratadaDS_1_Contratada_areatrabalhocod ;
      private int AV118WWContratadaDS_2_Contratada_codigo ;
      private int edtavOrdereddsc_Visible ;
      private int imgInsert_Visible ;
      private int edtContratada_OS_Visible ;
      private int GXt_int3 ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int edtavDisplay_Visible ;
      private int AV111PageToGo ;
      private int edtContratada_PessoaNom_Forecolor ;
      private int edtContratada_PessoaCNPJ_Forecolor ;
      private int edtPessoa_IE_Forecolor ;
      private int edtPessoa_Telefone_Forecolor ;
      private int edtContratada_SS_Forecolor ;
      private int edtContratada_OS_Forecolor ;
      private int edtContratada_Lote_Forecolor ;
      private int imgInsert_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int edtavContratada_pessoanom1_Visible ;
      private int edtavContratada_pessoacnpj1_Visible ;
      private int edtavContratada_pessoanom2_Visible ;
      private int edtavContratada_pessoacnpj2_Visible ;
      private int AV145GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV112GridCurrentPage ;
      private long AV113GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long AV114Color ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratada_pessoanom_Activeeventkey ;
      private String Ddo_contratada_pessoanom_Filteredtext_get ;
      private String Ddo_contratada_pessoanom_Selectedvalue_get ;
      private String Ddo_contratada_pessoacnpj_Activeeventkey ;
      private String Ddo_contratada_pessoacnpj_Filteredtext_get ;
      private String Ddo_contratada_pessoacnpj_Selectedvalue_get ;
      private String Ddo_pessoa_ie_Activeeventkey ;
      private String Ddo_pessoa_ie_Filteredtext_get ;
      private String Ddo_pessoa_ie_Selectedvalue_get ;
      private String Ddo_pessoa_telefone_Activeeventkey ;
      private String Ddo_pessoa_telefone_Filteredtext_get ;
      private String Ddo_pessoa_telefone_Selectedvalue_get ;
      private String Ddo_contratada_ss_Activeeventkey ;
      private String Ddo_contratada_ss_Filteredtext_get ;
      private String Ddo_contratada_ss_Filteredtextto_get ;
      private String Ddo_contratada_os_Activeeventkey ;
      private String Ddo_contratada_os_Filteredtext_get ;
      private String Ddo_contratada_os_Filteredtextto_get ;
      private String Ddo_contratada_lote_Activeeventkey ;
      private String Ddo_contratada_lote_Filteredtext_get ;
      private String Ddo_contratada_lote_Filteredtextto_get ;
      private String Ddo_contratada_ativo_Activeeventkey ;
      private String Ddo_contratada_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_77_idx="0001" ;
      private String AV17Contratada_PessoaNom1 ;
      private String AV22Contratada_PessoaNom2 ;
      private String AV80TFContratada_PessoaNom ;
      private String AV81TFContratada_PessoaNom_Sel ;
      private String AV88TFPessoa_IE ;
      private String AV89TFPessoa_IE_Sel ;
      private String AV92TFPessoa_Telefone ;
      private String AV93TFPessoa_Telefone_Sel ;
      private String AV144Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratada_pessoanom_Caption ;
      private String Ddo_contratada_pessoanom_Tooltip ;
      private String Ddo_contratada_pessoanom_Cls ;
      private String Ddo_contratada_pessoanom_Filteredtext_set ;
      private String Ddo_contratada_pessoanom_Selectedvalue_set ;
      private String Ddo_contratada_pessoanom_Dropdownoptionstype ;
      private String Ddo_contratada_pessoanom_Titlecontrolidtoreplace ;
      private String Ddo_contratada_pessoanom_Sortedstatus ;
      private String Ddo_contratada_pessoanom_Filtertype ;
      private String Ddo_contratada_pessoanom_Datalisttype ;
      private String Ddo_contratada_pessoanom_Datalistproc ;
      private String Ddo_contratada_pessoanom_Sortasc ;
      private String Ddo_contratada_pessoanom_Sortdsc ;
      private String Ddo_contratada_pessoanom_Loadingdata ;
      private String Ddo_contratada_pessoanom_Cleanfilter ;
      private String Ddo_contratada_pessoanom_Noresultsfound ;
      private String Ddo_contratada_pessoanom_Searchbuttontext ;
      private String Ddo_contratada_pessoacnpj_Caption ;
      private String Ddo_contratada_pessoacnpj_Tooltip ;
      private String Ddo_contratada_pessoacnpj_Cls ;
      private String Ddo_contratada_pessoacnpj_Filteredtext_set ;
      private String Ddo_contratada_pessoacnpj_Selectedvalue_set ;
      private String Ddo_contratada_pessoacnpj_Dropdownoptionstype ;
      private String Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace ;
      private String Ddo_contratada_pessoacnpj_Sortedstatus ;
      private String Ddo_contratada_pessoacnpj_Filtertype ;
      private String Ddo_contratada_pessoacnpj_Datalisttype ;
      private String Ddo_contratada_pessoacnpj_Datalistproc ;
      private String Ddo_contratada_pessoacnpj_Sortasc ;
      private String Ddo_contratada_pessoacnpj_Sortdsc ;
      private String Ddo_contratada_pessoacnpj_Loadingdata ;
      private String Ddo_contratada_pessoacnpj_Cleanfilter ;
      private String Ddo_contratada_pessoacnpj_Noresultsfound ;
      private String Ddo_contratada_pessoacnpj_Searchbuttontext ;
      private String Ddo_pessoa_ie_Caption ;
      private String Ddo_pessoa_ie_Tooltip ;
      private String Ddo_pessoa_ie_Cls ;
      private String Ddo_pessoa_ie_Filteredtext_set ;
      private String Ddo_pessoa_ie_Selectedvalue_set ;
      private String Ddo_pessoa_ie_Dropdownoptionstype ;
      private String Ddo_pessoa_ie_Titlecontrolidtoreplace ;
      private String Ddo_pessoa_ie_Sortedstatus ;
      private String Ddo_pessoa_ie_Filtertype ;
      private String Ddo_pessoa_ie_Datalisttype ;
      private String Ddo_pessoa_ie_Datalistproc ;
      private String Ddo_pessoa_ie_Sortasc ;
      private String Ddo_pessoa_ie_Sortdsc ;
      private String Ddo_pessoa_ie_Loadingdata ;
      private String Ddo_pessoa_ie_Cleanfilter ;
      private String Ddo_pessoa_ie_Noresultsfound ;
      private String Ddo_pessoa_ie_Searchbuttontext ;
      private String Ddo_pessoa_telefone_Caption ;
      private String Ddo_pessoa_telefone_Tooltip ;
      private String Ddo_pessoa_telefone_Cls ;
      private String Ddo_pessoa_telefone_Filteredtext_set ;
      private String Ddo_pessoa_telefone_Selectedvalue_set ;
      private String Ddo_pessoa_telefone_Dropdownoptionstype ;
      private String Ddo_pessoa_telefone_Titlecontrolidtoreplace ;
      private String Ddo_pessoa_telefone_Sortedstatus ;
      private String Ddo_pessoa_telefone_Filtertype ;
      private String Ddo_pessoa_telefone_Datalisttype ;
      private String Ddo_pessoa_telefone_Datalistproc ;
      private String Ddo_pessoa_telefone_Sortasc ;
      private String Ddo_pessoa_telefone_Sortdsc ;
      private String Ddo_pessoa_telefone_Loadingdata ;
      private String Ddo_pessoa_telefone_Cleanfilter ;
      private String Ddo_pessoa_telefone_Noresultsfound ;
      private String Ddo_pessoa_telefone_Searchbuttontext ;
      private String Ddo_contratada_ss_Caption ;
      private String Ddo_contratada_ss_Tooltip ;
      private String Ddo_contratada_ss_Cls ;
      private String Ddo_contratada_ss_Filteredtext_set ;
      private String Ddo_contratada_ss_Filteredtextto_set ;
      private String Ddo_contratada_ss_Dropdownoptionstype ;
      private String Ddo_contratada_ss_Titlecontrolidtoreplace ;
      private String Ddo_contratada_ss_Sortedstatus ;
      private String Ddo_contratada_ss_Filtertype ;
      private String Ddo_contratada_ss_Sortasc ;
      private String Ddo_contratada_ss_Sortdsc ;
      private String Ddo_contratada_ss_Cleanfilter ;
      private String Ddo_contratada_ss_Rangefilterfrom ;
      private String Ddo_contratada_ss_Rangefilterto ;
      private String Ddo_contratada_ss_Searchbuttontext ;
      private String Ddo_contratada_os_Caption ;
      private String Ddo_contratada_os_Tooltip ;
      private String Ddo_contratada_os_Cls ;
      private String Ddo_contratada_os_Filteredtext_set ;
      private String Ddo_contratada_os_Filteredtextto_set ;
      private String Ddo_contratada_os_Dropdownoptionstype ;
      private String Ddo_contratada_os_Titlecontrolidtoreplace ;
      private String Ddo_contratada_os_Sortedstatus ;
      private String Ddo_contratada_os_Filtertype ;
      private String Ddo_contratada_os_Sortasc ;
      private String Ddo_contratada_os_Sortdsc ;
      private String Ddo_contratada_os_Cleanfilter ;
      private String Ddo_contratada_os_Rangefilterfrom ;
      private String Ddo_contratada_os_Rangefilterto ;
      private String Ddo_contratada_os_Searchbuttontext ;
      private String Ddo_contratada_lote_Caption ;
      private String Ddo_contratada_lote_Tooltip ;
      private String Ddo_contratada_lote_Cls ;
      private String Ddo_contratada_lote_Filteredtext_set ;
      private String Ddo_contratada_lote_Filteredtextto_set ;
      private String Ddo_contratada_lote_Dropdownoptionstype ;
      private String Ddo_contratada_lote_Titlecontrolidtoreplace ;
      private String Ddo_contratada_lote_Sortedstatus ;
      private String Ddo_contratada_lote_Filtertype ;
      private String Ddo_contratada_lote_Sortasc ;
      private String Ddo_contratada_lote_Sortdsc ;
      private String Ddo_contratada_lote_Cleanfilter ;
      private String Ddo_contratada_lote_Rangefilterfrom ;
      private String Ddo_contratada_lote_Rangefilterto ;
      private String Ddo_contratada_lote_Searchbuttontext ;
      private String Ddo_contratada_ativo_Caption ;
      private String Ddo_contratada_ativo_Tooltip ;
      private String Ddo_contratada_ativo_Cls ;
      private String Ddo_contratada_ativo_Selectedvalue_set ;
      private String Ddo_contratada_ativo_Dropdownoptionstype ;
      private String Ddo_contratada_ativo_Titlecontrolidtoreplace ;
      private String Ddo_contratada_ativo_Sortedstatus ;
      private String Ddo_contratada_ativo_Datalisttype ;
      private String Ddo_contratada_ativo_Datalistfixedvalues ;
      private String Ddo_contratada_ativo_Sortasc ;
      private String Ddo_contratada_ativo_Sortdsc ;
      private String Ddo_contratada_ativo_Cleanfilter ;
      private String Ddo_contratada_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTfcontratada_pessoanom_Internalname ;
      private String edtavTfcontratada_pessoanom_Jsonclick ;
      private String edtavTfcontratada_pessoanom_sel_Internalname ;
      private String edtavTfcontratada_pessoanom_sel_Jsonclick ;
      private String edtavTfcontratada_pessoacnpj_Internalname ;
      private String edtavTfcontratada_pessoacnpj_Jsonclick ;
      private String edtavTfcontratada_pessoacnpj_sel_Internalname ;
      private String edtavTfcontratada_pessoacnpj_sel_Jsonclick ;
      private String edtavTfpessoa_ie_Internalname ;
      private String edtavTfpessoa_ie_Jsonclick ;
      private String edtavTfpessoa_ie_sel_Internalname ;
      private String edtavTfpessoa_ie_sel_Jsonclick ;
      private String edtavTfpessoa_telefone_Internalname ;
      private String edtavTfpessoa_telefone_Jsonclick ;
      private String edtavTfpessoa_telefone_sel_Internalname ;
      private String edtavTfpessoa_telefone_sel_Jsonclick ;
      private String edtavTfcontratada_ss_Internalname ;
      private String edtavTfcontratada_ss_Jsonclick ;
      private String edtavTfcontratada_ss_to_Internalname ;
      private String edtavTfcontratada_ss_to_Jsonclick ;
      private String edtavTfcontratada_os_Internalname ;
      private String edtavTfcontratada_os_Jsonclick ;
      private String edtavTfcontratada_os_to_Internalname ;
      private String edtavTfcontratada_os_to_Jsonclick ;
      private String edtavTfcontratada_lote_Internalname ;
      private String edtavTfcontratada_lote_Jsonclick ;
      private String edtavTfcontratada_lote_to_Internalname ;
      private String edtavTfcontratada_lote_to_Jsonclick ;
      private String edtavTfcontratada_ativo_sel_Internalname ;
      private String edtavTfcontratada_ativo_sel_Jsonclick ;
      private String edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_pessoa_ietitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_pessoa_telefonetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_sstitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_ostitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_lotetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtContratada_Codigo_Internalname ;
      private String edtContratada_AreaTrabalhoCod_Internalname ;
      private String edtContratada_PessoaCod_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Internalname ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String A518Pessoa_IE ;
      private String edtPessoa_IE_Internalname ;
      private String A522Pessoa_Telefone ;
      private String edtPessoa_Telefone_Internalname ;
      private String edtContratada_SS_Internalname ;
      private String edtContratada_OS_Internalname ;
      private String edtContratada_Lote_Internalname ;
      private String chkContratada_Ativo_Internalname ;
      private String scmdbuf ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String lV120WWContratadaDS_4_Contratada_pessoanom1 ;
      private String lV124WWContratadaDS_8_Contratada_pessoanom2 ;
      private String lV126WWContratadaDS_10_Tfcontratada_pessoanom ;
      private String lV130WWContratadaDS_14_Tfpessoa_ie ;
      private String lV132WWContratadaDS_16_Tfpessoa_telefone ;
      private String AV120WWContratadaDS_4_Contratada_pessoanom1 ;
      private String AV124WWContratadaDS_8_Contratada_pessoanom2 ;
      private String AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel ;
      private String AV126WWContratadaDS_10_Tfcontratada_pessoanom ;
      private String AV131WWContratadaDS_15_Tfpessoa_ie_sel ;
      private String AV130WWContratadaDS_14_Tfpessoa_ie ;
      private String AV133WWContratadaDS_17_Tfpessoa_telefone_sel ;
      private String AV132WWContratadaDS_16_Tfpessoa_telefone ;
      private String edtavOrdereddsc_Internalname ;
      private String dynavContratada_areatrabalhocod_Internalname ;
      private String dynavContratada_codigo_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavContratada_pessoanom1_Internalname ;
      private String edtavContratada_pessoacnpj1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavContratada_pessoanom2_Internalname ;
      private String edtavContratada_pessoacnpj2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratada_pessoanom_Internalname ;
      private String Ddo_contratada_pessoacnpj_Internalname ;
      private String Ddo_pessoa_ie_Internalname ;
      private String Ddo_pessoa_telefone_Internalname ;
      private String Ddo_contratada_ss_Internalname ;
      private String Ddo_contratada_os_Internalname ;
      private String Ddo_contratada_lote_Internalname ;
      private String Ddo_contratada_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContratada_PessoaNom_Title ;
      private String edtContratada_PessoaCNPJ_Title ;
      private String edtPessoa_IE_Title ;
      private String edtPessoa_Telefone_Title ;
      private String edtContratada_SS_Title ;
      private String edtContratada_OS_Title ;
      private String edtContratada_Lote_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String imgInsert_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblContratadatitle_Internalname ;
      private String lblContratadatitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextcontratada_areatrabalhocod_Internalname ;
      private String lblFiltertextcontratada_areatrabalhocod_Jsonclick ;
      private String dynavContratada_areatrabalhocod_Jsonclick ;
      private String lblFiltertextcontratada_codigo_Internalname ;
      private String lblFiltertextcontratada_codigo_Jsonclick ;
      private String dynavContratada_codigo_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavContratada_pessoanom1_Jsonclick ;
      private String edtavContratada_pessoacnpj1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavContratada_pessoanom2_Jsonclick ;
      private String edtavContratada_pessoacnpj2_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_77_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratada_Codigo_Jsonclick ;
      private String edtContratada_AreaTrabalhoCod_Jsonclick ;
      private String edtContratada_PessoaCod_Jsonclick ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String edtPessoa_IE_Jsonclick ;
      private String edtPessoa_Telefone_Jsonclick ;
      private String edtContratada_SS_Jsonclick ;
      private String edtContratada_OS_Jsonclick ;
      private String edtContratada_Lote_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool A43Contratada_Ativo ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratada_pessoanom_Includesortasc ;
      private bool Ddo_contratada_pessoanom_Includesortdsc ;
      private bool Ddo_contratada_pessoanom_Includefilter ;
      private bool Ddo_contratada_pessoanom_Filterisrange ;
      private bool Ddo_contratada_pessoanom_Includedatalist ;
      private bool Ddo_contratada_pessoacnpj_Includesortasc ;
      private bool Ddo_contratada_pessoacnpj_Includesortdsc ;
      private bool Ddo_contratada_pessoacnpj_Includefilter ;
      private bool Ddo_contratada_pessoacnpj_Filterisrange ;
      private bool Ddo_contratada_pessoacnpj_Includedatalist ;
      private bool Ddo_pessoa_ie_Includesortasc ;
      private bool Ddo_pessoa_ie_Includesortdsc ;
      private bool Ddo_pessoa_ie_Includefilter ;
      private bool Ddo_pessoa_ie_Filterisrange ;
      private bool Ddo_pessoa_ie_Includedatalist ;
      private bool Ddo_pessoa_telefone_Includesortasc ;
      private bool Ddo_pessoa_telefone_Includesortdsc ;
      private bool Ddo_pessoa_telefone_Includefilter ;
      private bool Ddo_pessoa_telefone_Filterisrange ;
      private bool Ddo_pessoa_telefone_Includedatalist ;
      private bool Ddo_contratada_ss_Includesortasc ;
      private bool Ddo_contratada_ss_Includesortdsc ;
      private bool Ddo_contratada_ss_Includefilter ;
      private bool Ddo_contratada_ss_Filterisrange ;
      private bool Ddo_contratada_ss_Includedatalist ;
      private bool Ddo_contratada_os_Includesortasc ;
      private bool Ddo_contratada_os_Includesortdsc ;
      private bool Ddo_contratada_os_Includefilter ;
      private bool Ddo_contratada_os_Filterisrange ;
      private bool Ddo_contratada_os_Includedatalist ;
      private bool Ddo_contratada_lote_Includesortasc ;
      private bool Ddo_contratada_lote_Includesortdsc ;
      private bool Ddo_contratada_lote_Includefilter ;
      private bool Ddo_contratada_lote_Filterisrange ;
      private bool Ddo_contratada_lote_Includedatalist ;
      private bool Ddo_contratada_ativo_Includesortasc ;
      private bool Ddo_contratada_ativo_Includesortdsc ;
      private bool Ddo_contratada_ativo_Includefilter ;
      private bool Ddo_contratada_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n518Pessoa_IE ;
      private bool n522Pessoa_Telefone ;
      private bool n1451Contratada_SS ;
      private bool n524Contratada_OS ;
      private bool n530Contratada_Lote ;
      private bool AV122WWContratadaDS_6_Dynamicfiltersenabled2 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool GXt_boolean2 ;
      private bool AV31Update_IsBlob ;
      private bool AV32Delete_IsBlob ;
      private bool AV75Display_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV73Contratada_PessoaCNPJ1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV74Contratada_PessoaCNPJ2 ;
      private String AV84TFContratada_PessoaCNPJ ;
      private String AV85TFContratada_PessoaCNPJ_Sel ;
      private String AV82ddo_Contratada_PessoaNomTitleControlIdToReplace ;
      private String AV86ddo_Contratada_PessoaCNPJTitleControlIdToReplace ;
      private String AV90ddo_Pessoa_IETitleControlIdToReplace ;
      private String AV94ddo_Pessoa_TelefoneTitleControlIdToReplace ;
      private String AV98ddo_Contratada_SSTitleControlIdToReplace ;
      private String AV102ddo_Contratada_OSTitleControlIdToReplace ;
      private String AV106ddo_Contratada_LoteTitleControlIdToReplace ;
      private String AV109ddo_Contratada_AtivoTitleControlIdToReplace ;
      private String AV141Update_GXI ;
      private String AV142Delete_GXI ;
      private String AV143Display_GXI ;
      private String A42Contratada_PessoaCNPJ ;
      private String lV121WWContratadaDS_5_Contratada_pessoacnpj1 ;
      private String lV125WWContratadaDS_9_Contratada_pessoacnpj2 ;
      private String lV128WWContratadaDS_12_Tfcontratada_pessoacnpj ;
      private String AV119WWContratadaDS_3_Dynamicfiltersselector1 ;
      private String AV121WWContratadaDS_5_Contratada_pessoacnpj1 ;
      private String AV123WWContratadaDS_7_Dynamicfiltersselector2 ;
      private String AV125WWContratadaDS_9_Contratada_pessoacnpj2 ;
      private String AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel ;
      private String AV128WWContratadaDS_12_Tfcontratada_pessoacnpj ;
      private String AV31Update ;
      private String AV32Delete ;
      private String AV75Display ;
      private IGxSession AV35Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox dynavContratada_areatrabalhocod ;
      private GXCombobox dynavContratada_codigo ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCheckbox chkContratada_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private int[] H00352_A5AreaTrabalho_Codigo ;
      private String[] H00352_A6AreaTrabalho_Descricao ;
      private int[] H00353_A5AreaTrabalho_Codigo ;
      private String[] H00353_A6AreaTrabalho_Descricao ;
      private int[] H00354_A40Contratada_PessoaCod ;
      private int[] H00354_A39Contratada_Codigo ;
      private String[] H00354_A41Contratada_PessoaNom ;
      private bool[] H00354_n41Contratada_PessoaNom ;
      private int[] H00354_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00355_A43Contratada_Ativo ;
      private short[] H00355_A530Contratada_Lote ;
      private bool[] H00355_n530Contratada_Lote ;
      private int[] H00355_A524Contratada_OS ;
      private bool[] H00355_n524Contratada_OS ;
      private int[] H00355_A1451Contratada_SS ;
      private bool[] H00355_n1451Contratada_SS ;
      private String[] H00355_A522Pessoa_Telefone ;
      private bool[] H00355_n522Pessoa_Telefone ;
      private String[] H00355_A518Pessoa_IE ;
      private bool[] H00355_n518Pessoa_IE ;
      private String[] H00355_A42Contratada_PessoaCNPJ ;
      private bool[] H00355_n42Contratada_PessoaCNPJ ;
      private String[] H00355_A41Contratada_PessoaNom ;
      private bool[] H00355_n41Contratada_PessoaNom ;
      private int[] H00355_A40Contratada_PessoaCod ;
      private int[] H00355_A52Contratada_AreaTrabalhoCod ;
      private int[] H00355_A39Contratada_Codigo ;
      private long[] H00356_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV79Contratada_PessoaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV83Contratada_PessoaCNPJTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV87Pessoa_IETitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV91Pessoa_TelefoneTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV95Contratada_SSTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV99Contratada_OSTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV103Contratada_LoteTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV107Contratada_AtivoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private SdtNotificationInfo AV44NotificationInfo ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV110DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwcontratada__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00355( IGxContext context ,
                                             int AV6WWPContext_gxTpr_Contratada_codigo ,
                                             String AV119WWContratadaDS_3_Dynamicfiltersselector1 ,
                                             String AV120WWContratadaDS_4_Contratada_pessoanom1 ,
                                             String AV121WWContratadaDS_5_Contratada_pessoacnpj1 ,
                                             bool AV122WWContratadaDS_6_Dynamicfiltersenabled2 ,
                                             String AV123WWContratadaDS_7_Dynamicfiltersselector2 ,
                                             String AV124WWContratadaDS_8_Contratada_pessoanom2 ,
                                             String AV125WWContratadaDS_9_Contratada_pessoacnpj2 ,
                                             String AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel ,
                                             String AV126WWContratadaDS_10_Tfcontratada_pessoanom ,
                                             String AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel ,
                                             String AV128WWContratadaDS_12_Tfcontratada_pessoacnpj ,
                                             String AV131WWContratadaDS_15_Tfpessoa_ie_sel ,
                                             String AV130WWContratadaDS_14_Tfpessoa_ie ,
                                             String AV133WWContratadaDS_17_Tfpessoa_telefone_sel ,
                                             String AV132WWContratadaDS_16_Tfpessoa_telefone ,
                                             int AV134WWContratadaDS_18_Tfcontratada_ss ,
                                             int AV135WWContratadaDS_19_Tfcontratada_ss_to ,
                                             int AV136WWContratadaDS_20_Tfcontratada_os ,
                                             int AV137WWContratadaDS_21_Tfcontratada_os_to ,
                                             short AV138WWContratadaDS_22_Tfcontratada_lote ,
                                             short AV139WWContratadaDS_23_Tfcontratada_lote_to ,
                                             short AV140WWContratadaDS_24_Tfcontratada_ativo_sel ,
                                             int A39Contratada_Codigo ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             String A518Pessoa_IE ,
                                             String A522Pessoa_Telefone ,
                                             int A1451Contratada_SS ,
                                             int A524Contratada_OS ,
                                             short A530Contratada_Lote ,
                                             bool A43Contratada_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [25] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Contratada_Ativo], T1.[Contratada_Lote], T1.[Contratada_OS], T1.[Contratada_SS], T2.[Pessoa_Telefone], T2.[Pessoa_IE], T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_AreaTrabalhoCod], T1.[Contratada_Codigo]";
         sFromString = " FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ! (0==AV6WWPContext_gxTpr_Contratada_codigo) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV6WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV119WWContratadaDS_3_Dynamicfiltersselector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWContratadaDS_4_Contratada_pessoanom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV120WWContratadaDS_4_Contratada_pessoanom1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV119WWContratadaDS_3_Dynamicfiltersselector1, "CONTRATADA_PESSOACNPJ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContratadaDS_5_Contratada_pessoacnpj1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV121WWContratadaDS_5_Contratada_pessoacnpj1)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV122WWContratadaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV123WWContratadaDS_7_Dynamicfiltersselector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWContratadaDS_8_Contratada_pessoanom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV124WWContratadaDS_8_Contratada_pessoanom2)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV122WWContratadaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV123WWContratadaDS_7_Dynamicfiltersselector2, "CONTRATADA_PESSOACNPJ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV125WWContratadaDS_9_Contratada_pessoacnpj2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV125WWContratadaDS_9_Contratada_pessoacnpj2)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV126WWContratadaDS_10_Tfcontratada_pessoanom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV126WWContratadaDS_10_Tfcontratada_pessoanom)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV128WWContratadaDS_12_Tfcontratada_pessoacnpj)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV128WWContratadaDS_12_Tfcontratada_pessoacnpj)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV131WWContratadaDS_15_Tfpessoa_ie_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContratadaDS_14_Tfpessoa_ie)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_IE] like @lV130WWContratadaDS_14_Tfpessoa_ie)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV131WWContratadaDS_15_Tfpessoa_ie_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_IE] = @AV131WWContratadaDS_15_Tfpessoa_ie_sel)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV133WWContratadaDS_17_Tfpessoa_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV132WWContratadaDS_16_Tfpessoa_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Telefone] like @lV132WWContratadaDS_16_Tfpessoa_telefone)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV133WWContratadaDS_17_Tfpessoa_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Telefone] = @AV133WWContratadaDS_17_Tfpessoa_telefone_sel)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (0==AV134WWContratadaDS_18_Tfcontratada_ss) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_SS] >= @AV134WWContratadaDS_18_Tfcontratada_ss)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (0==AV135WWContratadaDS_19_Tfcontratada_ss_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_SS] <= @AV135WWContratadaDS_19_Tfcontratada_ss_to)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (0==AV136WWContratadaDS_20_Tfcontratada_os) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_OS] >= @AV136WWContratadaDS_20_Tfcontratada_os)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (0==AV137WWContratadaDS_21_Tfcontratada_os_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_OS] <= @AV137WWContratadaDS_21_Tfcontratada_os_to)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! (0==AV138WWContratadaDS_22_Tfcontratada_lote) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Lote] >= @AV138WWContratadaDS_22_Tfcontratada_lote)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! (0==AV139WWContratadaDS_23_Tfcontratada_lote_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Lote] <= @AV139WWContratadaDS_23_Tfcontratada_lote_to)";
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( AV140WWContratadaDS_24_Tfcontratada_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 1)";
         }
         if ( AV140WWContratadaDS_24_Tfcontratada_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 0)";
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Docto]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Docto] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_IE]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_IE] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Telefone]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Telefone] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_SS]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_SS] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_OS]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_OS] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Lote]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Lote] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Ativo]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_H00356( IGxContext context ,
                                             int AV6WWPContext_gxTpr_Contratada_codigo ,
                                             String AV119WWContratadaDS_3_Dynamicfiltersselector1 ,
                                             String AV120WWContratadaDS_4_Contratada_pessoanom1 ,
                                             String AV121WWContratadaDS_5_Contratada_pessoacnpj1 ,
                                             bool AV122WWContratadaDS_6_Dynamicfiltersenabled2 ,
                                             String AV123WWContratadaDS_7_Dynamicfiltersselector2 ,
                                             String AV124WWContratadaDS_8_Contratada_pessoanom2 ,
                                             String AV125WWContratadaDS_9_Contratada_pessoacnpj2 ,
                                             String AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel ,
                                             String AV126WWContratadaDS_10_Tfcontratada_pessoanom ,
                                             String AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel ,
                                             String AV128WWContratadaDS_12_Tfcontratada_pessoacnpj ,
                                             String AV131WWContratadaDS_15_Tfpessoa_ie_sel ,
                                             String AV130WWContratadaDS_14_Tfpessoa_ie ,
                                             String AV133WWContratadaDS_17_Tfpessoa_telefone_sel ,
                                             String AV132WWContratadaDS_16_Tfpessoa_telefone ,
                                             int AV134WWContratadaDS_18_Tfcontratada_ss ,
                                             int AV135WWContratadaDS_19_Tfcontratada_ss_to ,
                                             int AV136WWContratadaDS_20_Tfcontratada_os ,
                                             int AV137WWContratadaDS_21_Tfcontratada_os_to ,
                                             short AV138WWContratadaDS_22_Tfcontratada_lote ,
                                             short AV139WWContratadaDS_23_Tfcontratada_lote_to ,
                                             short AV140WWContratadaDS_24_Tfcontratada_ativo_sel ,
                                             int A39Contratada_Codigo ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             String A518Pessoa_IE ,
                                             String A522Pessoa_Telefone ,
                                             int A1451Contratada_SS ,
                                             int A524Contratada_OS ,
                                             short A530Contratada_Lote ,
                                             bool A43Contratada_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [20] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ! (0==AV6WWPContext_gxTpr_Contratada_codigo) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV6WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int6[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV119WWContratadaDS_3_Dynamicfiltersselector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120WWContratadaDS_4_Contratada_pessoanom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV120WWContratadaDS_4_Contratada_pessoanom1)";
         }
         else
         {
            GXv_int6[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV119WWContratadaDS_3_Dynamicfiltersselector1, "CONTRATADA_PESSOACNPJ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContratadaDS_5_Contratada_pessoacnpj1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV121WWContratadaDS_5_Contratada_pessoacnpj1)";
         }
         else
         {
            GXv_int6[3] = 1;
         }
         if ( AV122WWContratadaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV123WWContratadaDS_7_Dynamicfiltersselector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWContratadaDS_8_Contratada_pessoanom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV124WWContratadaDS_8_Contratada_pessoanom2)";
         }
         else
         {
            GXv_int6[4] = 1;
         }
         if ( AV122WWContratadaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV123WWContratadaDS_7_Dynamicfiltersselector2, "CONTRATADA_PESSOACNPJ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV125WWContratadaDS_9_Contratada_pessoacnpj2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like '%' + @lV125WWContratadaDS_9_Contratada_pessoacnpj2)";
         }
         else
         {
            GXv_int6[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV126WWContratadaDS_10_Tfcontratada_pessoanom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV126WWContratadaDS_10_Tfcontratada_pessoanom)";
         }
         else
         {
            GXv_int6[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel)";
         }
         else
         {
            GXv_int6[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV128WWContratadaDS_12_Tfcontratada_pessoacnpj)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV128WWContratadaDS_12_Tfcontratada_pessoacnpj)";
         }
         else
         {
            GXv_int6[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel)";
         }
         else
         {
            GXv_int6[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV131WWContratadaDS_15_Tfpessoa_ie_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContratadaDS_14_Tfpessoa_ie)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_IE] like @lV130WWContratadaDS_14_Tfpessoa_ie)";
         }
         else
         {
            GXv_int6[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV131WWContratadaDS_15_Tfpessoa_ie_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_IE] = @AV131WWContratadaDS_15_Tfpessoa_ie_sel)";
         }
         else
         {
            GXv_int6[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV133WWContratadaDS_17_Tfpessoa_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV132WWContratadaDS_16_Tfpessoa_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Telefone] like @lV132WWContratadaDS_16_Tfpessoa_telefone)";
         }
         else
         {
            GXv_int6[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV133WWContratadaDS_17_Tfpessoa_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Telefone] = @AV133WWContratadaDS_17_Tfpessoa_telefone_sel)";
         }
         else
         {
            GXv_int6[13] = 1;
         }
         if ( ! (0==AV134WWContratadaDS_18_Tfcontratada_ss) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_SS] >= @AV134WWContratadaDS_18_Tfcontratada_ss)";
         }
         else
         {
            GXv_int6[14] = 1;
         }
         if ( ! (0==AV135WWContratadaDS_19_Tfcontratada_ss_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_SS] <= @AV135WWContratadaDS_19_Tfcontratada_ss_to)";
         }
         else
         {
            GXv_int6[15] = 1;
         }
         if ( ! (0==AV136WWContratadaDS_20_Tfcontratada_os) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_OS] >= @AV136WWContratadaDS_20_Tfcontratada_os)";
         }
         else
         {
            GXv_int6[16] = 1;
         }
         if ( ! (0==AV137WWContratadaDS_21_Tfcontratada_os_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_OS] <= @AV137WWContratadaDS_21_Tfcontratada_os_to)";
         }
         else
         {
            GXv_int6[17] = 1;
         }
         if ( ! (0==AV138WWContratadaDS_22_Tfcontratada_lote) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Lote] >= @AV138WWContratadaDS_22_Tfcontratada_lote)";
         }
         else
         {
            GXv_int6[18] = 1;
         }
         if ( ! (0==AV139WWContratadaDS_23_Tfcontratada_lote_to) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Lote] <= @AV139WWContratadaDS_23_Tfcontratada_lote_to)";
         }
         else
         {
            GXv_int6[19] = 1;
         }
         if ( AV140WWContratadaDS_24_Tfcontratada_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 1)";
         }
         if ( AV140WWContratadaDS_24_Tfcontratada_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 3 :
                     return conditional_H00355(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (short)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (short)dynConstraints[30] , (bool)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] );
               case 4 :
                     return conditional_H00356(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (short)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (short)dynConstraints[30] , (bool)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00352 ;
          prmH00352 = new Object[] {
          } ;
          Object[] prmH00353 ;
          prmH00353 = new Object[] {
          } ;
          Object[] prmH00354 ;
          prmH00354 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00355 ;
          prmH00355 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV120WWContratadaDS_4_Contratada_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV121WWContratadaDS_5_Contratada_pessoacnpj1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV124WWContratadaDS_8_Contratada_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV125WWContratadaDS_9_Contratada_pessoacnpj2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV126WWContratadaDS_10_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV128WWContratadaDS_12_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV130WWContratadaDS_14_Tfpessoa_ie",SqlDbType.Char,15,0} ,
          new Object[] {"@AV131WWContratadaDS_15_Tfpessoa_ie_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV132WWContratadaDS_16_Tfpessoa_telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@AV133WWContratadaDS_17_Tfpessoa_telefone_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV134WWContratadaDS_18_Tfcontratada_ss",SqlDbType.Int,8,0} ,
          new Object[] {"@AV135WWContratadaDS_19_Tfcontratada_ss_to",SqlDbType.Int,8,0} ,
          new Object[] {"@AV136WWContratadaDS_20_Tfcontratada_os",SqlDbType.Int,8,0} ,
          new Object[] {"@AV137WWContratadaDS_21_Tfcontratada_os_to",SqlDbType.Int,8,0} ,
          new Object[] {"@AV138WWContratadaDS_22_Tfcontratada_lote",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV139WWContratadaDS_23_Tfcontratada_lote_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00356 ;
          prmH00356 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV120WWContratadaDS_4_Contratada_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV121WWContratadaDS_5_Contratada_pessoacnpj1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV124WWContratadaDS_8_Contratada_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV125WWContratadaDS_9_Contratada_pessoacnpj2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV126WWContratadaDS_10_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV127WWContratadaDS_11_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV128WWContratadaDS_12_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV129WWContratadaDS_13_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV130WWContratadaDS_14_Tfpessoa_ie",SqlDbType.Char,15,0} ,
          new Object[] {"@AV131WWContratadaDS_15_Tfpessoa_ie_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV132WWContratadaDS_16_Tfpessoa_telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@AV133WWContratadaDS_17_Tfpessoa_telefone_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV134WWContratadaDS_18_Tfcontratada_ss",SqlDbType.Int,8,0} ,
          new Object[] {"@AV135WWContratadaDS_19_Tfcontratada_ss_to",SqlDbType.Int,8,0} ,
          new Object[] {"@AV136WWContratadaDS_20_Tfcontratada_os",SqlDbType.Int,8,0} ,
          new Object[] {"@AV137WWContratadaDS_21_Tfcontratada_os_to",SqlDbType.Int,8,0} ,
          new Object[] {"@AV138WWContratadaDS_22_Tfcontratada_lote",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV139WWContratadaDS_23_Tfcontratada_lote_to",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00352", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00352,0,0,true,false )
             ,new CursorDef("H00353", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00353,0,0,true,false )
             ,new CursorDef("H00354", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00354,0,0,true,false )
             ,new CursorDef("H00355", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00355,11,0,true,false )
             ,new CursorDef("H00356", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00356,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 3 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                return;
             case 4 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                return;
       }
    }

 }

}
