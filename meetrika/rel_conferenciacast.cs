/*
               File: REL_ConferenciaCAST
        Description: Stub for REL_ConferenciaCAST
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:14:33.99
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rel_conferenciacast : GXProcedure
   {
      public rel_conferenciacast( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public rel_conferenciacast( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Arquivo ,
                           String aP1_Aba ,
                           int aP2_Contratada_Codigo ,
                           short aP3_PraLinCAST ,
                           short aP4_ColDmnCASTn ,
                           short aP5_ColDataCASTn ,
                           short aP6_ColPFBFSCASTn ,
                           short aP7_ColPFLFSCASTn ,
                           short aP8_ColPFBFMCASTn ,
                           short aP9_ColPFLFMCASTn ,
                           ref String aP10_FileName )
      {
         this.AV2Arquivo = aP0_Arquivo;
         this.AV3Aba = aP1_Aba;
         this.AV4Contratada_Codigo = aP2_Contratada_Codigo;
         this.AV5PraLinCAST = aP3_PraLinCAST;
         this.AV6ColDmnCASTn = aP4_ColDmnCASTn;
         this.AV7ColDataCASTn = aP5_ColDataCASTn;
         this.AV8ColPFBFSCASTn = aP6_ColPFBFSCASTn;
         this.AV9ColPFLFSCASTn = aP7_ColPFLFSCASTn;
         this.AV10ColPFBFMCASTn = aP8_ColPFBFMCASTn;
         this.AV11ColPFLFMCASTn = aP9_ColPFLFMCASTn;
         this.AV12FileName = aP10_FileName;
         initialize();
         executePrivate();
         aP10_FileName=this.AV12FileName;
      }

      public String executeUdp( String aP0_Arquivo ,
                                String aP1_Aba ,
                                int aP2_Contratada_Codigo ,
                                short aP3_PraLinCAST ,
                                short aP4_ColDmnCASTn ,
                                short aP5_ColDataCASTn ,
                                short aP6_ColPFBFSCASTn ,
                                short aP7_ColPFLFSCASTn ,
                                short aP8_ColPFBFMCASTn ,
                                short aP9_ColPFLFMCASTn )
      {
         this.AV2Arquivo = aP0_Arquivo;
         this.AV3Aba = aP1_Aba;
         this.AV4Contratada_Codigo = aP2_Contratada_Codigo;
         this.AV5PraLinCAST = aP3_PraLinCAST;
         this.AV6ColDmnCASTn = aP4_ColDmnCASTn;
         this.AV7ColDataCASTn = aP5_ColDataCASTn;
         this.AV8ColPFBFSCASTn = aP6_ColPFBFSCASTn;
         this.AV9ColPFLFSCASTn = aP7_ColPFLFSCASTn;
         this.AV10ColPFBFMCASTn = aP8_ColPFBFMCASTn;
         this.AV11ColPFLFMCASTn = aP9_ColPFLFMCASTn;
         this.AV12FileName = aP10_FileName;
         initialize();
         executePrivate();
         aP10_FileName=this.AV12FileName;
         return AV12FileName ;
      }

      public void executeSubmit( String aP0_Arquivo ,
                                 String aP1_Aba ,
                                 int aP2_Contratada_Codigo ,
                                 short aP3_PraLinCAST ,
                                 short aP4_ColDmnCASTn ,
                                 short aP5_ColDataCASTn ,
                                 short aP6_ColPFBFSCASTn ,
                                 short aP7_ColPFLFSCASTn ,
                                 short aP8_ColPFBFMCASTn ,
                                 short aP9_ColPFLFMCASTn ,
                                 ref String aP10_FileName )
      {
         rel_conferenciacast objrel_conferenciacast;
         objrel_conferenciacast = new rel_conferenciacast();
         objrel_conferenciacast.AV2Arquivo = aP0_Arquivo;
         objrel_conferenciacast.AV3Aba = aP1_Aba;
         objrel_conferenciacast.AV4Contratada_Codigo = aP2_Contratada_Codigo;
         objrel_conferenciacast.AV5PraLinCAST = aP3_PraLinCAST;
         objrel_conferenciacast.AV6ColDmnCASTn = aP4_ColDmnCASTn;
         objrel_conferenciacast.AV7ColDataCASTn = aP5_ColDataCASTn;
         objrel_conferenciacast.AV8ColPFBFSCASTn = aP6_ColPFBFSCASTn;
         objrel_conferenciacast.AV9ColPFLFSCASTn = aP7_ColPFLFSCASTn;
         objrel_conferenciacast.AV10ColPFBFMCASTn = aP8_ColPFBFMCASTn;
         objrel_conferenciacast.AV11ColPFLFMCASTn = aP9_ColPFLFMCASTn;
         objrel_conferenciacast.AV12FileName = aP10_FileName;
         objrel_conferenciacast.context.SetSubmitInitialConfig(context);
         objrel_conferenciacast.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrel_conferenciacast);
         aP10_FileName=this.AV12FileName;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rel_conferenciacast)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(String)AV2Arquivo,(String)AV3Aba,(int)AV4Contratada_Codigo,(short)AV5PraLinCAST,(short)AV6ColDmnCASTn,(short)AV7ColDataCASTn,(short)AV8ColPFBFSCASTn,(short)AV9ColPFLFSCASTn,(short)AV10ColPFBFMCASTn,(short)AV11ColPFLFMCASTn,(String)AV12FileName} ;
         ClassLoader.Execute("arel_conferenciacast","GeneXus.Programs.arel_conferenciacast", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 11 ) )
         {
            AV12FileName = (String)(args[10]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV5PraLinCAST ;
      private short AV6ColDmnCASTn ;
      private short AV7ColDataCASTn ;
      private short AV8ColPFBFSCASTn ;
      private short AV9ColPFLFSCASTn ;
      private short AV10ColPFBFMCASTn ;
      private short AV11ColPFLFMCASTn ;
      private int AV4Contratada_Codigo ;
      private String AV2Arquivo ;
      private String AV3Aba ;
      private String AV12FileName ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP10_FileName ;
      private Object[] args ;
   }

}
