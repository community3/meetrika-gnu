/*
               File: PromptContratada
        Description: Selecione Contratada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:32:47.82
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontratada : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontratada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontratada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContratada_Codigo ,
                           ref String aP1_InOutContratada_PessoaNom )
      {
         this.AV7InOutContratada_Codigo = aP0_InOutContratada_Codigo;
         this.AV8InOutContratada_PessoaNom = aP1_InOutContratada_PessoaNom;
         executePrivate();
         aP0_InOutContratada_Codigo=this.AV7InOutContratada_Codigo;
         aP1_InOutContratada_PessoaNom=this.AV8InOutContratada_PessoaNom;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         dynavContratada_areatrabalhocod = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         dynavContratada_codigo1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         dynavContratada_codigo2 = new GXCombobox();
         chkContratada_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADA_CODIGO1") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADA_CODIGO1362( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADA_CODIGO2") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADA_CODIGO2362( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_69 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_69_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_69_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Contratada_PessoaNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_PessoaNom1", AV17Contratada_PessoaNom1);
               AV33Contratada_Codigo1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratada_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Contratada_Codigo1), 6, 0)));
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
               AV22Contratada_PessoaNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratada_PessoaNom2", AV22Contratada_PessoaNom2);
               AV34Contratada_Codigo2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contratada_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratada_Codigo2), 6, 0)));
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV61TFContratada_PessoaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratada_PessoaNom", AV61TFContratada_PessoaNom);
               AV62TFContratada_PessoaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContratada_PessoaNom_Sel", AV62TFContratada_PessoaNom_Sel);
               AV65TFContratada_PessoaCNPJ = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratada_PessoaCNPJ", AV65TFContratada_PessoaCNPJ);
               AV66TFContratada_PessoaCNPJ_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratada_PessoaCNPJ_Sel", AV66TFContratada_PessoaCNPJ_Sel);
               AV69TFContratada_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratada_Ativo_Sel", StringUtil.Str( (decimal)(AV69TFContratada_Ativo_Sel), 1, 0));
               AV63ddo_Contratada_PessoaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_Contratada_PessoaNomTitleControlIdToReplace", AV63ddo_Contratada_PessoaNomTitleControlIdToReplace);
               AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace", AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace);
               AV70ddo_Contratada_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ddo_Contratada_AtivoTitleControlIdToReplace", AV70ddo_Contratada_AtivoTitleControlIdToReplace);
               AV59Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Contratada_AreaTrabalhoCod), 6, 0)));
               AV78Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV30DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
               AV29DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contratada_PessoaNom1, AV33Contratada_Codigo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Contratada_PessoaNom2, AV34Contratada_Codigo2, AV19DynamicFiltersEnabled2, AV61TFContratada_PessoaNom, AV62TFContratada_PessoaNom_Sel, AV65TFContratada_PessoaCNPJ, AV66TFContratada_PessoaCNPJ_Sel, AV69TFContratada_Ativo_Sel, AV63ddo_Contratada_PessoaNomTitleControlIdToReplace, AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV70ddo_Contratada_AtivoTitleControlIdToReplace, AV59Contratada_AreaTrabalhoCod, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContratada_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratada_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutContratada_PessoaNom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratada_PessoaNom", AV8InOutContratada_PessoaNom);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA362( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV78Pgmname = "PromptContratada";
               context.Gx_err = 0;
               WS362( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE362( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311732485");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontratada.aspx") + "?" + UrlEncode("" +AV7InOutContratada_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutContratada_PessoaNom))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADA_PESSOANOM1", StringUtil.RTrim( AV17Contratada_PessoaNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADA_CODIGO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33Contratada_Codigo1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADA_PESSOANOM2", StringUtil.RTrim( AV22Contratada_PessoaNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADA_CODIGO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34Contratada_Codigo2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOANOM", StringUtil.RTrim( AV61TFContratada_PessoaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOANOM_SEL", StringUtil.RTrim( AV62TFContratada_PessoaNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOACNPJ", AV65TFContratada_PessoaCNPJ);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOACNPJ_SEL", AV66TFContratada_PessoaCNPJ_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69TFContratada_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_69", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_69), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV73GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV74GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV71DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV71DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_PESSOANOMTITLEFILTERDATA", AV60Contratada_PessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_PESSOANOMTITLEFILTERDATA", AV60Contratada_PessoaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_PESSOACNPJTITLEFILTERDATA", AV64Contratada_PessoaCNPJTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_PESSOACNPJTITLEFILTERDATA", AV64Contratada_PessoaCNPJTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_ATIVOTITLEFILTERDATA", AV68Contratada_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_ATIVOTITLEFILTERDATA", AV68Contratada_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV78Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATADA_PESSOANOM", StringUtil.RTrim( AV8InOutContratada_PessoaNom));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Caption", StringUtil.RTrim( Ddo_contratada_pessoanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Tooltip", StringUtil.RTrim( Ddo_contratada_pessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Cls", StringUtil.RTrim( Ddo_contratada_pessoanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_pessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratada_pessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_pessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_pessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortedstatus", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filtertype", StringUtil.RTrim( Ddo_contratada_pessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_contratada_pessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_contratada_pessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratada_pessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortasc", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortdsc", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_contratada_pessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_contratada_pessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_contratada_pessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_pessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Caption", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Tooltip", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Cls", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_set", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Sortedstatus", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includefilter", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filtertype", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalisttype", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalistproc", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Sortasc", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Sortdsc", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Loadingdata", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Cleanfilter", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Noresultsfound", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Caption", StringUtil.RTrim( Ddo_contratada_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Tooltip", StringUtil.RTrim( Ddo_contratada_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Cls", StringUtil.RTrim( Ddo_contratada_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratada_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_contratada_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_contratada_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_contratada_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratada_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Sortasc", StringUtil.RTrim( Ddo_contratada_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_contratada_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_contratada_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_contratada_pessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_pessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratada_pessoanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Activeeventkey", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_get", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_contratada_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratada_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm362( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContratada" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Contratada" ;
      }

      protected void WB360( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_362( true) ;
         }
         else
         {
            wb_table1_2_362( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_362e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(81, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoanom_Internalname, StringUtil.RTrim( AV61TFContratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( AV61TFContratada_PessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratada_pessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoanom_sel_Internalname, StringUtil.RTrim( AV62TFContratada_PessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV62TFContratada_PessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,83);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratada_pessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoacnpj_Internalname, AV65TFContratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( AV65TFContratada_PessoaCNPJ, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoacnpj_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_pessoacnpj_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoacnpj_sel_Internalname, AV66TFContratada_PessoaCNPJ_Sel, StringUtil.RTrim( context.localUtil.Format( AV66TFContratada_PessoaCNPJ_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,85);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoacnpj_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_pessoacnpj_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratada.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69TFContratada_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV69TFContratada_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratada.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_PESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname, AV63ddo_Contratada_PessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,88);\"", 0, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratada.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_PESSOACNPJContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname, AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"", 0, edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratada.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_ativotitlecontrolidtoreplace_Internalname, AV70ddo_Contratada_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", 0, edtavDdo_contratada_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratada.htm");
         }
         wbLoad = true;
      }

      protected void START362( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Contratada", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP360( ) ;
      }

      protected void WS362( )
      {
         START362( ) ;
         EVT362( ) ;
      }

      protected void EVT362( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11362 */
                           E11362 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12362 */
                           E12362 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_PESSOACNPJ.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13362 */
                           E13362 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_ATIVO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14362 */
                           E14362 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15362 */
                           E15362 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16362 */
                           E16362 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17362 */
                           E17362 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18362 */
                           E18362 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19362 */
                           E19362 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20362 */
                           E20362 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21362 */
                           E21362 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_69_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_69_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_69_idx), 4, 0)), 4, "0");
                           SubsflControlProps_692( ) ;
                           AV31Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV77Select_GXI : context.convertURL( context.PathToRelativeUrl( AV31Select))));
                           A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratada_Codigo_Internalname), ",", "."));
                           A52Contratada_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtContratada_AreaTrabalhoCod_Internalname), ",", "."));
                           A40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratada_PessoaCod_Internalname), ",", "."));
                           A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
                           n41Contratada_PessoaNom = false;
                           A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
                           n42Contratada_PessoaCNPJ = false;
                           A43Contratada_Ativo = StringUtil.StrToBool( cgiGet( chkContratada_Ativo_Internalname));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E22362 */
                                 E22362 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E23362 */
                                 E23362 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E24362 */
                                 E24362 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratada_pessoanom1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM1"), AV17Contratada_PessoaNom1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratada_codigo1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATADA_CODIGO1"), ",", ".") != Convert.ToDecimal( AV33Contratada_Codigo1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratada_pessoanom2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM2"), AV22Contratada_PessoaNom2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratada_codigo2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATADA_CODIGO2"), ",", ".") != Convert.ToDecimal( AV34Contratada_Codigo2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_pessoanom Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM"), AV61TFContratada_PessoaNom) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_pessoanom_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM_SEL"), AV62TFContratada_PessoaNom_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_pessoacnpj Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ"), AV65TFContratada_PessoaCNPJ) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_pessoacnpj_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ_SEL"), AV66TFContratada_PessoaCNPJ_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_ativo_sel Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV69TFContratada_Ativo_Sel )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E25362 */
                                       E25362 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE362( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm362( ) ;
            }
         }
      }

      protected void PA362( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            dynavContratada_areatrabalhocod.Name = "vCONTRATADA_AREATRABALHOCOD";
            dynavContratada_areatrabalhocod.WebTags = "";
            dynavContratada_areatrabalhocod.removeAllItems();
            /* Using cursor H00362 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynavContratada_areatrabalhocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00362_A5AreaTrabalho_Codigo[0]), 6, 0)), H00362_A6AreaTrabalho_Descricao[0], 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynavContratada_areatrabalhocod.ItemCount > 0 )
            {
               AV59Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavContratada_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV59Contratada_AreaTrabalhoCod), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Contratada_AreaTrabalhoCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATADA_PESSOANOM", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATADA_CODIGO", "Contratada", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            dynavContratada_codigo1.Name = "vCONTRATADA_CODIGO1";
            dynavContratada_codigo1.WebTags = "";
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATADA_PESSOANOM", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATADA_CODIGO", "Contratada", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            dynavContratada_codigo2.Name = "vCONTRATADA_CODIGO2";
            dynavContratada_codigo2.WebTags = "";
            GXCCtl = "CONTRATADA_ATIVO_" + sGXsfl_69_idx;
            chkContratada_Ativo.Name = GXCCtl;
            chkContratada_Ativo.WebTags = "";
            chkContratada_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratada_Ativo_Internalname, "TitleCaption", chkContratada_Ativo.Caption);
            chkContratada_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTRATADA_AREATRABALHOCOD361( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADA_AREATRABALHOCOD_data361( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADA_AREATRABALHOCOD_html361( )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADA_AREATRABALHOCOD_data361( ) ;
         gxdynajaxindex = 1;
         dynavContratada_areatrabalhocod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratada_areatrabalhocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratada_areatrabalhocod.ItemCount > 0 )
         {
            AV59Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavContratada_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV59Contratada_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Contratada_AreaTrabalhoCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADA_AREATRABALHOCOD_data361( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00363 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00363_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00363_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvCONTRATADA_CODIGO1362( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADA_CODIGO1_data362( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADA_CODIGO1_html362( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADA_CODIGO1_data362( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContratada_codigo1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratada_codigo1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratada_codigo1.ItemCount > 0 )
         {
            AV33Contratada_Codigo1 = (int)(NumberUtil.Val( dynavContratada_codigo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV33Contratada_Codigo1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratada_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Contratada_Codigo1), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADA_CODIGO1_data362( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00364 */
         pr_default.execute(2, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00364_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00364_A41Contratada_PessoaNom[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void GXDLVvCONTRATADA_CODIGO2362( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADA_CODIGO2_data362( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADA_CODIGO2_html362( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADA_CODIGO2_data362( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContratada_codigo2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratada_codigo2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratada_codigo2.ItemCount > 0 )
         {
            AV34Contratada_Codigo2 = (int)(NumberUtil.Val( dynavContratada_codigo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV34Contratada_Codigo2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contratada_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratada_Codigo2), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADA_CODIGO2_data362( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00365 */
         pr_default.execute(3, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00365_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00365_A41Contratada_PessoaNom[0]));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_692( ) ;
         while ( nGXsfl_69_idx <= nRC_GXsfl_69 )
         {
            sendrow_692( ) ;
            nGXsfl_69_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_69_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_69_idx+1));
            sGXsfl_69_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_69_idx), 4, 0)), 4, "0");
            SubsflControlProps_692( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17Contratada_PessoaNom1 ,
                                       int AV33Contratada_Codigo1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       String AV22Contratada_PessoaNom2 ,
                                       int AV34Contratada_Codigo2 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       String AV61TFContratada_PessoaNom ,
                                       String AV62TFContratada_PessoaNom_Sel ,
                                       String AV65TFContratada_PessoaCNPJ ,
                                       String AV66TFContratada_PessoaCNPJ_Sel ,
                                       short AV69TFContratada_Ativo_Sel ,
                                       String AV63ddo_Contratada_PessoaNomTitleControlIdToReplace ,
                                       String AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace ,
                                       String AV70ddo_Contratada_AtivoTitleControlIdToReplace ,
                                       int AV59Contratada_AreaTrabalhoCod ,
                                       String AV78Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV30DynamicFiltersIgnoreFirst ,
                                       bool AV29DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF362( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A52Contratada_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_PESSOACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A40Contratada_PessoaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_ATIVO", GetSecureSignedToken( "", A43Contratada_Ativo));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_ATIVO", StringUtil.BoolToStr( A43Contratada_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( dynavContratada_areatrabalhocod.ItemCount > 0 )
         {
            AV59Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavContratada_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV59Contratada_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Contratada_AreaTrabalhoCod), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( dynavContratada_codigo1.ItemCount > 0 )
         {
            AV33Contratada_Codigo1 = (int)(NumberUtil.Val( dynavContratada_codigo1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV33Contratada_Codigo1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratada_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Contratada_Codigo1), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( dynavContratada_codigo2.ItemCount > 0 )
         {
            AV34Contratada_Codigo2 = (int)(NumberUtil.Val( dynavContratada_codigo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV34Contratada_Codigo2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contratada_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratada_Codigo2), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF362( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV78Pgmname = "PromptContratada";
         context.Gx_err = 0;
      }

      protected void RF362( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 69;
         /* Execute user event: E23362 */
         E23362 ();
         nGXsfl_69_idx = 1;
         sGXsfl_69_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_69_idx), 4, 0)), 4, "0");
         SubsflControlProps_692( ) ;
         nGXsfl_69_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_692( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(4, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17Contratada_PessoaNom1 ,
                                                 AV33Contratada_Codigo1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV21DynamicFiltersOperator2 ,
                                                 AV22Contratada_PessoaNom2 ,
                                                 AV34Contratada_Codigo2 ,
                                                 AV62TFContratada_PessoaNom_Sel ,
                                                 AV61TFContratada_PessoaNom ,
                                                 AV66TFContratada_PessoaCNPJ_Sel ,
                                                 AV65TFContratada_PessoaCNPJ ,
                                                 AV69TFContratada_Ativo_Sel ,
                                                 A41Contratada_PessoaNom ,
                                                 A39Contratada_Codigo ,
                                                 A42Contratada_PessoaCNPJ ,
                                                 A43Contratada_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A52Contratada_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV17Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV17Contratada_PessoaNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_PessoaNom1", AV17Contratada_PessoaNom1);
            lV17Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV17Contratada_PessoaNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_PessoaNom1", AV17Contratada_PessoaNom1);
            lV22Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV22Contratada_PessoaNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratada_PessoaNom2", AV22Contratada_PessoaNom2);
            lV22Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV22Contratada_PessoaNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratada_PessoaNom2", AV22Contratada_PessoaNom2);
            lV61TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV61TFContratada_PessoaNom), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratada_PessoaNom", AV61TFContratada_PessoaNom);
            lV65TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV65TFContratada_PessoaCNPJ), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratada_PessoaCNPJ", AV65TFContratada_PessoaCNPJ);
            /* Using cursor H00366 */
            pr_default.execute(4, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV17Contratada_PessoaNom1, lV17Contratada_PessoaNom1, AV33Contratada_Codigo1, lV22Contratada_PessoaNom2, lV22Contratada_PessoaNom2, AV34Contratada_Codigo2, lV61TFContratada_PessoaNom, AV62TFContratada_PessoaNom_Sel, lV65TFContratada_PessoaCNPJ, AV66TFContratada_PessoaCNPJ_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_69_idx = 1;
            while ( ( (pr_default.getStatus(4) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A43Contratada_Ativo = H00366_A43Contratada_Ativo[0];
               A42Contratada_PessoaCNPJ = H00366_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H00366_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H00366_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00366_n41Contratada_PessoaNom[0];
               A40Contratada_PessoaCod = H00366_A40Contratada_PessoaCod[0];
               A52Contratada_AreaTrabalhoCod = H00366_A52Contratada_AreaTrabalhoCod[0];
               A39Contratada_Codigo = H00366_A39Contratada_Codigo[0];
               A42Contratada_PessoaCNPJ = H00366_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H00366_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H00366_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00366_n41Contratada_PessoaNom[0];
               /* Execute user event: E24362 */
               E24362 ();
               pr_default.readNext(4);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(4) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(4);
            wbEnd = 69;
            WB360( ) ;
         }
         nGXsfl_69_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(5, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV17Contratada_PessoaNom1 ,
                                              AV33Contratada_Codigo1 ,
                                              AV19DynamicFiltersEnabled2 ,
                                              AV20DynamicFiltersSelector2 ,
                                              AV21DynamicFiltersOperator2 ,
                                              AV22Contratada_PessoaNom2 ,
                                              AV34Contratada_Codigo2 ,
                                              AV62TFContratada_PessoaNom_Sel ,
                                              AV61TFContratada_PessoaNom ,
                                              AV66TFContratada_PessoaCNPJ_Sel ,
                                              AV65TFContratada_PessoaCNPJ ,
                                              AV69TFContratada_Ativo_Sel ,
                                              A41Contratada_PessoaNom ,
                                              A39Contratada_Codigo ,
                                              A42Contratada_PessoaCNPJ ,
                                              A43Contratada_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV17Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV17Contratada_PessoaNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_PessoaNom1", AV17Contratada_PessoaNom1);
         lV17Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV17Contratada_PessoaNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_PessoaNom1", AV17Contratada_PessoaNom1);
         lV22Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV22Contratada_PessoaNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratada_PessoaNom2", AV22Contratada_PessoaNom2);
         lV22Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV22Contratada_PessoaNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratada_PessoaNom2", AV22Contratada_PessoaNom2);
         lV61TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV61TFContratada_PessoaNom), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratada_PessoaNom", AV61TFContratada_PessoaNom);
         lV65TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV65TFContratada_PessoaCNPJ), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratada_PessoaCNPJ", AV65TFContratada_PessoaCNPJ);
         /* Using cursor H00367 */
         pr_default.execute(5, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV17Contratada_PessoaNom1, lV17Contratada_PessoaNom1, AV33Contratada_Codigo1, lV22Contratada_PessoaNom2, lV22Contratada_PessoaNom2, AV34Contratada_Codigo2, lV61TFContratada_PessoaNom, AV62TFContratada_PessoaNom_Sel, lV65TFContratada_PessoaCNPJ, AV66TFContratada_PessoaCNPJ_Sel});
         GRID_nRecordCount = H00367_AGRID_nRecordCount[0];
         pr_default.close(5);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contratada_PessoaNom1, AV33Contratada_Codigo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Contratada_PessoaNom2, AV34Contratada_Codigo2, AV19DynamicFiltersEnabled2, AV61TFContratada_PessoaNom, AV62TFContratada_PessoaNom_Sel, AV65TFContratada_PessoaCNPJ, AV66TFContratada_PessoaCNPJ_Sel, AV69TFContratada_Ativo_Sel, AV63ddo_Contratada_PessoaNomTitleControlIdToReplace, AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV70ddo_Contratada_AtivoTitleControlIdToReplace, AV59Contratada_AreaTrabalhoCod, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contratada_PessoaNom1, AV33Contratada_Codigo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Contratada_PessoaNom2, AV34Contratada_Codigo2, AV19DynamicFiltersEnabled2, AV61TFContratada_PessoaNom, AV62TFContratada_PessoaNom_Sel, AV65TFContratada_PessoaCNPJ, AV66TFContratada_PessoaCNPJ_Sel, AV69TFContratada_Ativo_Sel, AV63ddo_Contratada_PessoaNomTitleControlIdToReplace, AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV70ddo_Contratada_AtivoTitleControlIdToReplace, AV59Contratada_AreaTrabalhoCod, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contratada_PessoaNom1, AV33Contratada_Codigo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Contratada_PessoaNom2, AV34Contratada_Codigo2, AV19DynamicFiltersEnabled2, AV61TFContratada_PessoaNom, AV62TFContratada_PessoaNom_Sel, AV65TFContratada_PessoaCNPJ, AV66TFContratada_PessoaCNPJ_Sel, AV69TFContratada_Ativo_Sel, AV63ddo_Contratada_PessoaNomTitleControlIdToReplace, AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV70ddo_Contratada_AtivoTitleControlIdToReplace, AV59Contratada_AreaTrabalhoCod, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contratada_PessoaNom1, AV33Contratada_Codigo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Contratada_PessoaNom2, AV34Contratada_Codigo2, AV19DynamicFiltersEnabled2, AV61TFContratada_PessoaNom, AV62TFContratada_PessoaNom_Sel, AV65TFContratada_PessoaCNPJ, AV66TFContratada_PessoaCNPJ_Sel, AV69TFContratada_Ativo_Sel, AV63ddo_Contratada_PessoaNomTitleControlIdToReplace, AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV70ddo_Contratada_AtivoTitleControlIdToReplace, AV59Contratada_AreaTrabalhoCod, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contratada_PessoaNom1, AV33Contratada_Codigo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Contratada_PessoaNom2, AV34Contratada_Codigo2, AV19DynamicFiltersEnabled2, AV61TFContratada_PessoaNom, AV62TFContratada_PessoaNom_Sel, AV65TFContratada_PessoaCNPJ, AV66TFContratada_PessoaCNPJ_Sel, AV69TFContratada_Ativo_Sel, AV63ddo_Contratada_PessoaNomTitleControlIdToReplace, AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV70ddo_Contratada_AtivoTitleControlIdToReplace, AV59Contratada_AreaTrabalhoCod, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUP360( )
      {
         /* Before Start, stand alone formulas. */
         AV78Pgmname = "PromptContratada";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E22362 */
         E22362 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV71DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_PESSOANOMTITLEFILTERDATA"), AV60Contratada_PessoaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_PESSOACNPJTITLEFILTERDATA"), AV64Contratada_PessoaCNPJTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_ATIVOTITLEFILTERDATA"), AV68Contratada_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            dynavContratada_areatrabalhocod.Name = dynavContratada_areatrabalhocod_Internalname;
            dynavContratada_areatrabalhocod.CurrentValue = cgiGet( dynavContratada_areatrabalhocod_Internalname);
            AV59Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( cgiGet( dynavContratada_areatrabalhocod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Contratada_AreaTrabalhoCod), 6, 0)));
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17Contratada_PessoaNom1 = StringUtil.Upper( cgiGet( edtavContratada_pessoanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_PessoaNom1", AV17Contratada_PessoaNom1);
            dynavContratada_codigo1.Name = dynavContratada_codigo1_Internalname;
            dynavContratada_codigo1.CurrentValue = cgiGet( dynavContratada_codigo1_Internalname);
            AV33Contratada_Codigo1 = (int)(NumberUtil.Val( cgiGet( dynavContratada_codigo1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratada_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Contratada_Codigo1), 6, 0)));
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            AV22Contratada_PessoaNom2 = StringUtil.Upper( cgiGet( edtavContratada_pessoanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratada_PessoaNom2", AV22Contratada_PessoaNom2);
            dynavContratada_codigo2.Name = dynavContratada_codigo2_Internalname;
            dynavContratada_codigo2.CurrentValue = cgiGet( dynavContratada_codigo2_Internalname);
            AV34Contratada_Codigo2 = (int)(NumberUtil.Val( cgiGet( dynavContratada_codigo2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contratada_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratada_Codigo2), 6, 0)));
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV61TFContratada_PessoaNom = StringUtil.Upper( cgiGet( edtavTfcontratada_pessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratada_PessoaNom", AV61TFContratada_PessoaNom);
            AV62TFContratada_PessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontratada_pessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContratada_PessoaNom_Sel", AV62TFContratada_PessoaNom_Sel);
            AV65TFContratada_PessoaCNPJ = cgiGet( edtavTfcontratada_pessoacnpj_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratada_PessoaCNPJ", AV65TFContratada_PessoaCNPJ);
            AV66TFContratada_PessoaCNPJ_Sel = cgiGet( edtavTfcontratada_pessoacnpj_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratada_PessoaCNPJ_Sel", AV66TFContratada_PessoaCNPJ_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADA_ATIVO_SEL");
               GX_FocusControl = edtavTfcontratada_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV69TFContratada_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratada_Ativo_Sel", StringUtil.Str( (decimal)(AV69TFContratada_Ativo_Sel), 1, 0));
            }
            else
            {
               AV69TFContratada_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratada_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratada_Ativo_Sel", StringUtil.Str( (decimal)(AV69TFContratada_Ativo_Sel), 1, 0));
            }
            AV63ddo_Contratada_PessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_Contratada_PessoaNomTitleControlIdToReplace", AV63ddo_Contratada_PessoaNomTitleControlIdToReplace);
            AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace = cgiGet( edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace", AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace);
            AV70ddo_Contratada_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_contratada_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ddo_Contratada_AtivoTitleControlIdToReplace", AV70ddo_Contratada_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_69 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_69"), ",", "."));
            AV73GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV74GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratada_pessoanom_Caption = cgiGet( "DDO_CONTRATADA_PESSOANOM_Caption");
            Ddo_contratada_pessoanom_Tooltip = cgiGet( "DDO_CONTRATADA_PESSOANOM_Tooltip");
            Ddo_contratada_pessoanom_Cls = cgiGet( "DDO_CONTRATADA_PESSOANOM_Cls");
            Ddo_contratada_pessoanom_Filteredtext_set = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filteredtext_set");
            Ddo_contratada_pessoanom_Selectedvalue_set = cgiGet( "DDO_CONTRATADA_PESSOANOM_Selectedvalue_set");
            Ddo_contratada_pessoanom_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Dropdownoptionstype");
            Ddo_contratada_pessoanom_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_PESSOANOM_Titlecontrolidtoreplace");
            Ddo_contratada_pessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includesortasc"));
            Ddo_contratada_pessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includesortdsc"));
            Ddo_contratada_pessoanom_Sortedstatus = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortedstatus");
            Ddo_contratada_pessoanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includefilter"));
            Ddo_contratada_pessoanom_Filtertype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filtertype");
            Ddo_contratada_pessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Filterisrange"));
            Ddo_contratada_pessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includedatalist"));
            Ddo_contratada_pessoanom_Datalisttype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalisttype");
            Ddo_contratada_pessoanom_Datalistproc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalistproc");
            Ddo_contratada_pessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratada_pessoanom_Sortasc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortasc");
            Ddo_contratada_pessoanom_Sortdsc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortdsc");
            Ddo_contratada_pessoanom_Loadingdata = cgiGet( "DDO_CONTRATADA_PESSOANOM_Loadingdata");
            Ddo_contratada_pessoanom_Cleanfilter = cgiGet( "DDO_CONTRATADA_PESSOANOM_Cleanfilter");
            Ddo_contratada_pessoanom_Noresultsfound = cgiGet( "DDO_CONTRATADA_PESSOANOM_Noresultsfound");
            Ddo_contratada_pessoanom_Searchbuttontext = cgiGet( "DDO_CONTRATADA_PESSOANOM_Searchbuttontext");
            Ddo_contratada_pessoacnpj_Caption = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Caption");
            Ddo_contratada_pessoacnpj_Tooltip = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Tooltip");
            Ddo_contratada_pessoacnpj_Cls = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Cls");
            Ddo_contratada_pessoacnpj_Filteredtext_set = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_set");
            Ddo_contratada_pessoacnpj_Selectedvalue_set = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_set");
            Ddo_contratada_pessoacnpj_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Dropdownoptionstype");
            Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Titlecontrolidtoreplace");
            Ddo_contratada_pessoacnpj_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includesortasc"));
            Ddo_contratada_pessoacnpj_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includesortdsc"));
            Ddo_contratada_pessoacnpj_Sortedstatus = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Sortedstatus");
            Ddo_contratada_pessoacnpj_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includefilter"));
            Ddo_contratada_pessoacnpj_Filtertype = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filtertype");
            Ddo_contratada_pessoacnpj_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filterisrange"));
            Ddo_contratada_pessoacnpj_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includedatalist"));
            Ddo_contratada_pessoacnpj_Datalisttype = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalisttype");
            Ddo_contratada_pessoacnpj_Datalistproc = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalistproc");
            Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratada_pessoacnpj_Sortasc = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Sortasc");
            Ddo_contratada_pessoacnpj_Sortdsc = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Sortdsc");
            Ddo_contratada_pessoacnpj_Loadingdata = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Loadingdata");
            Ddo_contratada_pessoacnpj_Cleanfilter = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Cleanfilter");
            Ddo_contratada_pessoacnpj_Noresultsfound = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Noresultsfound");
            Ddo_contratada_pessoacnpj_Searchbuttontext = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Searchbuttontext");
            Ddo_contratada_ativo_Caption = cgiGet( "DDO_CONTRATADA_ATIVO_Caption");
            Ddo_contratada_ativo_Tooltip = cgiGet( "DDO_CONTRATADA_ATIVO_Tooltip");
            Ddo_contratada_ativo_Cls = cgiGet( "DDO_CONTRATADA_ATIVO_Cls");
            Ddo_contratada_ativo_Selectedvalue_set = cgiGet( "DDO_CONTRATADA_ATIVO_Selectedvalue_set");
            Ddo_contratada_ativo_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_ATIVO_Dropdownoptionstype");
            Ddo_contratada_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_ATIVO_Titlecontrolidtoreplace");
            Ddo_contratada_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_ATIVO_Includesortasc"));
            Ddo_contratada_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_ATIVO_Includesortdsc"));
            Ddo_contratada_ativo_Sortedstatus = cgiGet( "DDO_CONTRATADA_ATIVO_Sortedstatus");
            Ddo_contratada_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_ATIVO_Includefilter"));
            Ddo_contratada_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_ATIVO_Includedatalist"));
            Ddo_contratada_ativo_Datalisttype = cgiGet( "DDO_CONTRATADA_ATIVO_Datalisttype");
            Ddo_contratada_ativo_Datalistfixedvalues = cgiGet( "DDO_CONTRATADA_ATIVO_Datalistfixedvalues");
            Ddo_contratada_ativo_Sortasc = cgiGet( "DDO_CONTRATADA_ATIVO_Sortasc");
            Ddo_contratada_ativo_Sortdsc = cgiGet( "DDO_CONTRATADA_ATIVO_Sortdsc");
            Ddo_contratada_ativo_Cleanfilter = cgiGet( "DDO_CONTRATADA_ATIVO_Cleanfilter");
            Ddo_contratada_ativo_Searchbuttontext = cgiGet( "DDO_CONTRATADA_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratada_pessoanom_Activeeventkey = cgiGet( "DDO_CONTRATADA_PESSOANOM_Activeeventkey");
            Ddo_contratada_pessoanom_Filteredtext_get = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filteredtext_get");
            Ddo_contratada_pessoanom_Selectedvalue_get = cgiGet( "DDO_CONTRATADA_PESSOANOM_Selectedvalue_get");
            Ddo_contratada_pessoacnpj_Activeeventkey = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Activeeventkey");
            Ddo_contratada_pessoacnpj_Filteredtext_get = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_get");
            Ddo_contratada_pessoacnpj_Selectedvalue_get = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_get");
            Ddo_contratada_ativo_Activeeventkey = cgiGet( "DDO_CONTRATADA_ATIVO_Activeeventkey");
            Ddo_contratada_ativo_Selectedvalue_get = cgiGet( "DDO_CONTRATADA_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM1"), AV17Contratada_PessoaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATADA_CODIGO1"), ",", ".") != Convert.ToDecimal( AV33Contratada_Codigo1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM2"), AV22Contratada_PessoaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATADA_CODIGO2"), ",", ".") != Convert.ToDecimal( AV34Contratada_Codigo2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM"), AV61TFContratada_PessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM_SEL"), AV62TFContratada_PessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ"), AV65TFContratada_PessoaCNPJ) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ_SEL"), AV66TFContratada_PessoaCNPJ_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV69TFContratada_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E22362 */
         E22362 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22362( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTRATADA_PESSOANOM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "CONTRATADA_PESSOANOM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTfcontratada_pessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoanom_Visible), 5, 0)));
         edtavTfcontratada_pessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoanom_sel_Visible), 5, 0)));
         edtavTfcontratada_pessoacnpj_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoacnpj_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoacnpj_Visible), 5, 0)));
         edtavTfcontratada_pessoacnpj_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoacnpj_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoacnpj_sel_Visible), 5, 0)));
         edtavTfcontratada_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_ativo_sel_Visible), 5, 0)));
         Ddo_contratada_pessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_PessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "TitleControlIdToReplace", Ddo_contratada_pessoanom_Titlecontrolidtoreplace);
         AV63ddo_Contratada_PessoaNomTitleControlIdToReplace = Ddo_contratada_pessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_Contratada_PessoaNomTitleControlIdToReplace", AV63ddo_Contratada_PessoaNomTitleControlIdToReplace);
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_PessoaCNPJ";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "TitleControlIdToReplace", Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace);
         AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace = Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace", AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace);
         edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ativo_Internalname, "TitleControlIdToReplace", Ddo_contratada_ativo_Titlecontrolidtoreplace);
         AV70ddo_Contratada_AtivoTitleControlIdToReplace = Ddo_contratada_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ddo_Contratada_AtivoTitleControlIdToReplace", AV70ddo_Contratada_AtivoTitleControlIdToReplace);
         edtavDdo_contratada_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Contratada";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "CNPJ", 0);
         cmbavOrderedby.addItem("3", "Ativo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV71DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV71DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E23362( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV60Contratada_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64Contratada_PessoaCNPJTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68Contratada_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_CODIGO") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "=", 0);
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_CODIGO") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "=", 0);
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratada_PessoaNom_Titleformat = 2;
         edtContratada_PessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV63ddo_Contratada_PessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Title", edtContratada_PessoaNom_Title);
         edtContratada_PessoaCNPJ_Titleformat = 2;
         edtContratada_PessoaCNPJ_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "CNPJ", AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCNPJ_Internalname, "Title", edtContratada_PessoaCNPJ_Title);
         chkContratada_Ativo_Titleformat = 2;
         chkContratada_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV70ddo_Contratada_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratada_Ativo_Internalname, "Title", chkContratada_Ativo.Title.Text);
         AV73GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73GridCurrentPage), 10, 0)));
         AV74GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV74GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV60Contratada_PessoaNomTitleFilterData", AV60Contratada_PessoaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64Contratada_PessoaCNPJTitleFilterData", AV64Contratada_PessoaCNPJTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68Contratada_AtivoTitleFilterData", AV68Contratada_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11362( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV72PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV72PageToGo) ;
         }
      }

      protected void E12362( )
      {
         /* Ddo_contratada_pessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV61TFContratada_PessoaNom = Ddo_contratada_pessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratada_PessoaNom", AV61TFContratada_PessoaNom);
            AV62TFContratada_PessoaNom_Sel = Ddo_contratada_pessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContratada_PessoaNom_Sel", AV62TFContratada_PessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13362( )
      {
         /* Ddo_contratada_pessoacnpj_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_pessoacnpj_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoacnpj_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoacnpj_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoacnpj_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoacnpj_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV65TFContratada_PessoaCNPJ = Ddo_contratada_pessoacnpj_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratada_PessoaCNPJ", AV65TFContratada_PessoaCNPJ);
            AV66TFContratada_PessoaCNPJ_Sel = Ddo_contratada_pessoacnpj_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratada_PessoaCNPJ_Sel", AV66TFContratada_PessoaCNPJ_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14362( )
      {
         /* Ddo_contratada_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ativo_Internalname, "SortedStatus", Ddo_contratada_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ativo_Internalname, "SortedStatus", Ddo_contratada_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV69TFContratada_Ativo_Sel = (short)(NumberUtil.Val( Ddo_contratada_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratada_Ativo_Sel", StringUtil.Str( (decimal)(AV69TFContratada_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E24362( )
      {
         /* Grid_Load Routine */
         AV31Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV31Select);
         AV77Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 69;
         }
         sendrow_692( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_69_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(69, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E25362 */
         E25362 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25362( )
      {
         /* Enter Routine */
         AV7InOutContratada_Codigo = A39Contratada_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratada_Codigo), 6, 0)));
         AV8InOutContratada_PessoaNom = A41Contratada_PessoaNom;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratada_PessoaNom", AV8InOutContratada_PessoaNom);
         context.setWebReturnParms(new Object[] {(int)AV7InOutContratada_Codigo,(String)AV8InOutContratada_PessoaNom});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E15362( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E19362( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contratada_PessoaNom1, AV33Contratada_Codigo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Contratada_PessoaNom2, AV34Contratada_Codigo2, AV19DynamicFiltersEnabled2, AV61TFContratada_PessoaNom, AV62TFContratada_PessoaNom_Sel, AV65TFContratada_PessoaCNPJ, AV66TFContratada_PessoaCNPJ_Sel, AV69TFContratada_Ativo_Sel, AV63ddo_Contratada_PessoaNomTitleControlIdToReplace, AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV70ddo_Contratada_AtivoTitleControlIdToReplace, AV59Contratada_AreaTrabalhoCod, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
      }

      protected void E16362( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contratada_PessoaNom1, AV33Contratada_Codigo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Contratada_PessoaNom2, AV34Contratada_Codigo2, AV19DynamicFiltersEnabled2, AV61TFContratada_PessoaNom, AV62TFContratada_PessoaNom_Sel, AV65TFContratada_PessoaCNPJ, AV66TFContratada_PessoaCNPJ_Sel, AV69TFContratada_Ativo_Sel, AV63ddo_Contratada_PessoaNomTitleControlIdToReplace, AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV70ddo_Contratada_AtivoTitleControlIdToReplace, AV59Contratada_AreaTrabalhoCod, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavContratada_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV33Contratada_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo1_Internalname, "Values", dynavContratada_codigo1.ToJavascriptSource());
         dynavContratada_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34Contratada_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo2_Internalname, "Values", dynavContratada_codigo2.ToJavascriptSource());
      }

      protected void E20362( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E17362( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Contratada_PessoaNom1, AV33Contratada_Codigo1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Contratada_PessoaNom2, AV34Contratada_Codigo2, AV19DynamicFiltersEnabled2, AV61TFContratada_PessoaNom, AV62TFContratada_PessoaNom_Sel, AV65TFContratada_PessoaCNPJ, AV66TFContratada_PessoaCNPJ_Sel, AV69TFContratada_Ativo_Sel, AV63ddo_Contratada_PessoaNomTitleControlIdToReplace, AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV70ddo_Contratada_AtivoTitleControlIdToReplace, AV59Contratada_AreaTrabalhoCod, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavContratada_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV33Contratada_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo1_Internalname, "Values", dynavContratada_codigo1.ToJavascriptSource());
         dynavContratada_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34Contratada_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo2_Internalname, "Values", dynavContratada_codigo2.ToJavascriptSource());
      }

      protected void E21362( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E18362( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         dynavContratada_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV59Contratada_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_areatrabalhocod_Internalname, "Values", dynavContratada_areatrabalhocod.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         dynavContratada_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV33Contratada_Codigo1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo1_Internalname, "Values", dynavContratada_codigo1.ToJavascriptSource());
         dynavContratada_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34Contratada_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo2_Internalname, "Values", dynavContratada_codigo2.ToJavascriptSource());
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratada_pessoanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
         Ddo_contratada_pessoacnpj_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
         Ddo_contratada_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ativo_Internalname, "SortedStatus", Ddo_contratada_ativo_Sortedstatus);
      }

      protected void S142( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_contratada_pessoanom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contratada_pessoacnpj_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratada_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ativo_Internalname, "SortedStatus", Ddo_contratada_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContratada_pessoanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom1_Visible), 5, 0)));
         dynavContratada_codigo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratada_codigo1.Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
         {
            edtavContratada_pessoanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_CODIGO") == 0 )
         {
            dynavContratada_codigo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratada_codigo1.Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContratada_pessoanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom2_Visible), 5, 0)));
         dynavContratada_codigo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratada_codigo2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
         {
            edtavContratada_pessoanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_CODIGO") == 0 )
         {
            dynavContratada_codigo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratada_codigo2.Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "CONTRATADA_PESSOANOM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         AV22Contratada_PessoaNom2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratada_PessoaNom2", AV22Contratada_PessoaNom2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'CLEANFILTERS' Routine */
         AV59Contratada_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59Contratada_AreaTrabalhoCod), 6, 0)));
         AV61TFContratada_PessoaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratada_PessoaNom", AV61TFContratada_PessoaNom);
         Ddo_contratada_pessoanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "FilteredText_set", Ddo_contratada_pessoanom_Filteredtext_set);
         AV62TFContratada_PessoaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContratada_PessoaNom_Sel", AV62TFContratada_PessoaNom_Sel);
         Ddo_contratada_pessoanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SelectedValue_set", Ddo_contratada_pessoanom_Selectedvalue_set);
         AV65TFContratada_PessoaCNPJ = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratada_PessoaCNPJ", AV65TFContratada_PessoaCNPJ);
         Ddo_contratada_pessoacnpj_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "FilteredText_set", Ddo_contratada_pessoacnpj_Filteredtext_set);
         AV66TFContratada_PessoaCNPJ_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratada_PessoaCNPJ_Sel", AV66TFContratada_PessoaCNPJ_Sel);
         Ddo_contratada_pessoacnpj_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SelectedValue_set", Ddo_contratada_pessoacnpj_Selectedvalue_set);
         AV69TFContratada_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratada_Ativo_Sel", StringUtil.Str( (decimal)(AV69TFContratada_Ativo_Sel), 1, 0));
         Ddo_contratada_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_ativo_Internalname, "SelectedValue_set", Ddo_contratada_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "CONTRATADA_PESSOANOM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17Contratada_PessoaNom1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_PessoaNom1", AV17Contratada_PessoaNom1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S132( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Contratada_PessoaNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_PessoaNom1", AV17Contratada_PessoaNom1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_CODIGO") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV33Contratada_Codigo1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratada_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Contratada_Codigo1), 6, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22Contratada_PessoaNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Contratada_PessoaNom2", AV22Contratada_PessoaNom2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_CODIGO") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV34Contratada_Codigo2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contratada_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratada_Codigo2), 6, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV59Contratada_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTRATADA_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV59Contratada_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFContratada_PessoaNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOANOM";
            AV11GridStateFilterValue.gxTpr_Value = AV61TFContratada_PessoaNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFContratada_PessoaNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOANOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV62TFContratada_PessoaNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFContratada_PessoaCNPJ)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOACNPJ";
            AV11GridStateFilterValue.gxTpr_Value = AV65TFContratada_PessoaCNPJ;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFContratada_PessoaCNPJ_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOACNPJ_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV66TFContratada_PessoaCNPJ_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV69TFContratada_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV69TFContratada_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV78Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Contratada_PessoaNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17Contratada_PessoaNom1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_CODIGO") == 0 ) && ! (0==AV33Contratada_Codigo1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV33Contratada_Codigo1), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Contratada_PessoaNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22Contratada_PessoaNom2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_CODIGO") == 0 ) && ! (0==AV34Contratada_Codigo2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV34Contratada_Codigo2), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_362( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_362( true) ;
         }
         else
         {
            wb_table2_5_362( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_362e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_63_362( true) ;
         }
         else
         {
            wb_table3_63_362( false) ;
         }
         return  ;
      }

      protected void wb_table3_63_362e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_362e( true) ;
         }
         else
         {
            wb_table1_2_362e( false) ;
         }
      }

      protected void wb_table3_63_362( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_66_362( true) ;
         }
         else
         {
            wb_table4_66_362( false) ;
         }
         return  ;
      }

      protected void wb_table4_66_362e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_63_362e( true) ;
         }
         else
         {
            wb_table3_63_362e( false) ;
         }
      }

      protected void wb_table4_66_362( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"69\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contratada") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "de Trabalho") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "da Pessoa") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaCNPJ_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaCNPJ_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaCNPJ_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkContratada_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkContratada_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkContratada_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A41Contratada_PessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A42Contratada_PessoaCNPJ);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaCNPJ_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaCNPJ_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A43Contratada_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkContratada_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkContratada_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 69 )
         {
            wbEnd = 0;
            nRC_GXsfl_69 = (short)(nGXsfl_69_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_66_362e( true) ;
         }
         else
         {
            wb_table4_66_362e( false) ;
         }
      }

      protected void wb_table2_5_362( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContratada.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_362( true) ;
         }
         else
         {
            wb_table5_14_362( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_362e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_362e( true) ;
         }
         else
         {
            wb_table2_5_362e( false) ;
         }
      }

      protected void wb_table5_14_362( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontratada_areatrabalhocod_Internalname, "�rea de Trabalhlho", "", "", lblFiltertextcontratada_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_PromptContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada_areatrabalhocod, dynavContratada_areatrabalhocod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV59Contratada_AreaTrabalhoCod), 6, 0)), 1, dynavContratada_areatrabalhocod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "", true, "HLP_PromptContratada.htm");
            dynavContratada_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV59Contratada_AreaTrabalhoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_areatrabalhocod_Internalname, "Values", (String)(dynavContratada_areatrabalhocod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_23_362( true) ;
         }
         else
         {
            wb_table6_23_362( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_362e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_362e( true) ;
         }
         else
         {
            wb_table5_14_362e( false) ;
         }
      }

      protected void wb_table6_23_362( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_PromptContratada.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_32_362( true) ;
         }
         else
         {
            wb_table7_32_362( false) ;
         }
         return  ;
      }

      protected void wb_table7_32_362e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratada.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_PromptContratada.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_50_362( true) ;
         }
         else
         {
            wb_table8_50_362( false) ;
         }
         return  ;
      }

      protected void wb_table8_50_362e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_362e( true) ;
         }
         else
         {
            wb_table6_23_362e( false) ;
         }
      }

      protected void wb_table8_50_362( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", "", true, "HLP_PromptContratada.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoanom2_Internalname, StringUtil.RTrim( AV22Contratada_PessoaNom2), StringUtil.RTrim( context.localUtil.Format( AV22Contratada_PessoaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_pessoanom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratada.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada_codigo2, dynavContratada_codigo2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV34Contratada_Codigo2), 6, 0)), 1, dynavContratada_codigo2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContratada_codigo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_PromptContratada.htm");
            dynavContratada_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34Contratada_Codigo2), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo2_Internalname, "Values", (String)(dynavContratada_codigo2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_50_362e( true) ;
         }
         else
         {
            wb_table8_50_362e( false) ;
         }
      }

      protected void wb_table7_32_362( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "", true, "HLP_PromptContratada.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoanom1_Internalname, StringUtil.RTrim( AV17Contratada_PessoaNom1), StringUtil.RTrim( context.localUtil.Format( AV17Contratada_PessoaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_pessoanom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratada.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada_codigo1, dynavContratada_codigo1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV33Contratada_Codigo1), 6, 0)), 1, dynavContratada_codigo1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContratada_codigo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_PromptContratada.htm");
            dynavContratada_codigo1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV33Contratada_Codigo1), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo1_Internalname, "Values", (String)(dynavContratada_codigo1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_32_362e( true) ;
         }
         else
         {
            wb_table7_32_362e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContratada_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratada_Codigo), 6, 0)));
         AV8InOutContratada_PessoaNom = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratada_PessoaNom", AV8InOutContratada_PessoaNom);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA362( ) ;
         WS362( ) ;
         WE362( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117325272");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontratada.js", "?20203117325273");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_692( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_69_idx;
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO_"+sGXsfl_69_idx;
         edtContratada_AreaTrabalhoCod_Internalname = "CONTRATADA_AREATRABALHOCOD_"+sGXsfl_69_idx;
         edtContratada_PessoaCod_Internalname = "CONTRATADA_PESSOACOD_"+sGXsfl_69_idx;
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM_"+sGXsfl_69_idx;
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ_"+sGXsfl_69_idx;
         chkContratada_Ativo_Internalname = "CONTRATADA_ATIVO_"+sGXsfl_69_idx;
      }

      protected void SubsflControlProps_fel_692( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_69_fel_idx;
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO_"+sGXsfl_69_fel_idx;
         edtContratada_AreaTrabalhoCod_Internalname = "CONTRATADA_AREATRABALHOCOD_"+sGXsfl_69_fel_idx;
         edtContratada_PessoaCod_Internalname = "CONTRATADA_PESSOACOD_"+sGXsfl_69_fel_idx;
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM_"+sGXsfl_69_fel_idx;
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ_"+sGXsfl_69_fel_idx;
         chkContratada_Ativo_Internalname = "CONTRATADA_ATIVO_"+sGXsfl_69_fel_idx;
      }

      protected void sendrow_692( )
      {
         SubsflControlProps_692( ) ;
         WB360( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_69_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_69_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_69_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 70,'',false,'',69)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV31Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV77Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV77Select_GXI : context.PathToRelativeUrl( AV31Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_69_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV31Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)69,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_AreaTrabalhoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A52Contratada_AreaTrabalhoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_AreaTrabalhoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)69,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A40Contratada_PessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)69,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaNom_Internalname,StringUtil.RTrim( A41Contratada_PessoaNom),StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)69,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaCNPJ_Internalname,(String)A42Contratada_PessoaCNPJ,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaCNPJ_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)69,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkContratada_Ativo_Internalname,StringUtil.BoolToStr( A43Contratada_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_CODIGO"+"_"+sGXsfl_69_idx, GetSecureSignedToken( sGXsfl_69_idx, context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_AREATRABALHOCOD"+"_"+sGXsfl_69_idx, GetSecureSignedToken( sGXsfl_69_idx, context.localUtil.Format( (decimal)(A52Contratada_AreaTrabalhoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_PESSOACOD"+"_"+sGXsfl_69_idx, GetSecureSignedToken( sGXsfl_69_idx, context.localUtil.Format( (decimal)(A40Contratada_PessoaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_ATIVO"+"_"+sGXsfl_69_idx, GetSecureSignedToken( sGXsfl_69_idx, A43Contratada_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_69_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_69_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_69_idx+1));
            sGXsfl_69_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_69_idx), 4, 0)), 4, "0");
            SubsflControlProps_692( ) ;
         }
         /* End function sendrow_692 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextcontratada_areatrabalhocod_Internalname = "FILTERTEXTCONTRATADA_AREATRABALHOCOD";
         dynavContratada_areatrabalhocod_Internalname = "vCONTRATADA_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContratada_pessoanom1_Internalname = "vCONTRATADA_PESSOANOM1";
         dynavContratada_codigo1_Internalname = "vCONTRATADA_CODIGO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContratada_pessoanom2_Internalname = "vCONTRATADA_PESSOANOM2";
         dynavContratada_codigo2_Internalname = "vCONTRATADA_CODIGO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO";
         edtContratada_AreaTrabalhoCod_Internalname = "CONTRATADA_AREATRABALHOCOD";
         edtContratada_PessoaCod_Internalname = "CONTRATADA_PESSOACOD";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ";
         chkContratada_Ativo_Internalname = "CONTRATADA_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         edtavTfcontratada_pessoanom_Internalname = "vTFCONTRATADA_PESSOANOM";
         edtavTfcontratada_pessoanom_sel_Internalname = "vTFCONTRATADA_PESSOANOM_SEL";
         edtavTfcontratada_pessoacnpj_Internalname = "vTFCONTRATADA_PESSOACNPJ";
         edtavTfcontratada_pessoacnpj_sel_Internalname = "vTFCONTRATADA_PESSOACNPJ_SEL";
         edtavTfcontratada_ativo_sel_Internalname = "vTFCONTRATADA_ATIVO_SEL";
         Ddo_contratada_pessoanom_Internalname = "DDO_CONTRATADA_PESSOANOM";
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE";
         Ddo_contratada_pessoacnpj_Internalname = "DDO_CONTRATADA_PESSOACNPJ";
         edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE";
         Ddo_contratada_ativo_Internalname = "DDO_CONTRATADA_ATIVO";
         edtavDdo_contratada_ativotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratada_PessoaCod_Jsonclick = "";
         edtContratada_AreaTrabalhoCod_Jsonclick = "";
         edtContratada_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         dynavContratada_codigo1_Jsonclick = "";
         edtavContratada_pessoanom1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         dynavContratada_codigo2_Jsonclick = "";
         edtavContratada_pessoanom2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         dynavContratada_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         chkContratada_Ativo_Titleformat = 0;
         edtContratada_PessoaCNPJ_Titleformat = 0;
         edtContratada_PessoaNom_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator2.Visible = 1;
         dynavContratada_codigo2.Visible = 1;
         edtavContratada_pessoanom2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         dynavContratada_codigo1.Visible = 1;
         edtavContratada_pessoanom1_Visible = 1;
         chkContratada_Ativo.Title.Text = "Ativo";
         edtContratada_PessoaCNPJ_Title = "CNPJ";
         edtContratada_PessoaNom_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         chkContratada_Ativo.Caption = "";
         edtavDdo_contratada_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratada_ativo_sel_Jsonclick = "";
         edtavTfcontratada_ativo_sel_Visible = 1;
         edtavTfcontratada_pessoacnpj_sel_Jsonclick = "";
         edtavTfcontratada_pessoacnpj_sel_Visible = 1;
         edtavTfcontratada_pessoacnpj_Jsonclick = "";
         edtavTfcontratada_pessoacnpj_Visible = 1;
         edtavTfcontratada_pessoanom_sel_Jsonclick = "";
         edtavTfcontratada_pessoanom_sel_Visible = 1;
         edtavTfcontratada_pessoanom_Jsonclick = "";
         edtavTfcontratada_pessoanom_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contratada_ativo_Searchbuttontext = "Pesquisar";
         Ddo_contratada_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_contratada_ativo_Datalisttype = "FixedValues";
         Ddo_contratada_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratada_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratada_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_ativo_Titlecontrolidtoreplace = "";
         Ddo_contratada_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_ativo_Cls = "ColumnSettings";
         Ddo_contratada_ativo_Tooltip = "Op��es";
         Ddo_contratada_ativo_Caption = "";
         Ddo_contratada_pessoacnpj_Searchbuttontext = "Pesquisar";
         Ddo_contratada_pessoacnpj_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratada_pessoacnpj_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_pessoacnpj_Loadingdata = "Carregando dados...";
         Ddo_contratada_pessoacnpj_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_pessoacnpj_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters = 0;
         Ddo_contratada_pessoacnpj_Datalistproc = "GetPromptContratadaFilterData";
         Ddo_contratada_pessoacnpj_Datalisttype = "Dynamic";
         Ddo_contratada_pessoacnpj_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratada_pessoacnpj_Filtertype = "Character";
         Ddo_contratada_pessoacnpj_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace = "";
         Ddo_contratada_pessoacnpj_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_pessoacnpj_Cls = "ColumnSettings";
         Ddo_contratada_pessoacnpj_Tooltip = "Op��es";
         Ddo_contratada_pessoacnpj_Caption = "";
         Ddo_contratada_pessoanom_Searchbuttontext = "Pesquisar";
         Ddo_contratada_pessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratada_pessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_pessoanom_Loadingdata = "Carregando dados...";
         Ddo_contratada_pessoanom_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_pessoanom_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_pessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_contratada_pessoanom_Datalistproc = "GetPromptContratadaFilterData";
         Ddo_contratada_pessoanom_Datalisttype = "Dynamic";
         Ddo_contratada_pessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratada_pessoanom_Filtertype = "Character";
         Ddo_contratada_pessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Titlecontrolidtoreplace = "";
         Ddo_contratada_pessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_pessoanom_Cls = "ColumnSettings";
         Ddo_contratada_pessoanom_Tooltip = "Op��es";
         Ddo_contratada_pessoanom_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Contratada";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV63ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV59Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV61TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV62TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV65TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV66TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV69TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV33Contratada_Codigo1',fld:'vCONTRATADA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV34Contratada_Codigo2',fld:'vCONTRATADA_CODIGO2',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV60Contratada_PessoaNomTitleFilterData',fld:'vCONTRATADA_PESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV64Contratada_PessoaCNPJTitleFilterData',fld:'vCONTRATADA_PESSOACNPJTITLEFILTERDATA',pic:'',nv:null},{av:'AV68Contratada_AtivoTitleFilterData',fld:'vCONTRATADA_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtContratada_PessoaNom_Titleformat',ctrl:'CONTRATADA_PESSOANOM',prop:'Titleformat'},{av:'edtContratada_PessoaNom_Title',ctrl:'CONTRATADA_PESSOANOM',prop:'Title'},{av:'edtContratada_PessoaCNPJ_Titleformat',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Titleformat'},{av:'edtContratada_PessoaCNPJ_Title',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Title'},{av:'chkContratada_Ativo_Titleformat',ctrl:'CONTRATADA_ATIVO',prop:'Titleformat'},{av:'chkContratada_Ativo.Title.Text',ctrl:'CONTRATADA_ATIVO',prop:'Title'},{av:'AV73GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV74GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11362',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV33Contratada_Codigo1',fld:'vCONTRATADA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV34Contratada_Codigo2',fld:'vCONTRATADA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV62TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV65TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV66TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV69TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED","{handler:'E12362',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV33Contratada_Codigo1',fld:'vCONTRATADA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV34Contratada_Codigo2',fld:'vCONTRATADA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV62TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV65TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV66TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV69TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratada_pessoanom_Activeeventkey',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_contratada_pessoanom_Filteredtext_get',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'FilteredText_get'},{av:'Ddo_contratada_pessoanom_Selectedvalue_get',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'AV61TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV62TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratada_ativo_Sortedstatus',ctrl:'DDO_CONTRATADA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_PESSOACNPJ.ONOPTIONCLICKED","{handler:'E13362',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV33Contratada_Codigo1',fld:'vCONTRATADA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV34Contratada_Codigo2',fld:'vCONTRATADA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV62TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV65TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV66TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV69TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratada_pessoacnpj_Activeeventkey',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'ActiveEventKey'},{av:'Ddo_contratada_pessoacnpj_Filteredtext_get',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'FilteredText_get'},{av:'Ddo_contratada_pessoacnpj_Selectedvalue_get',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'AV65TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV66TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_ativo_Sortedstatus',ctrl:'DDO_CONTRATADA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_ATIVO.ONOPTIONCLICKED","{handler:'E14362',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV33Contratada_Codigo1',fld:'vCONTRATADA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV34Contratada_Codigo2',fld:'vCONTRATADA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV62TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV65TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV66TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV69TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratada_ativo_Activeeventkey',ctrl:'DDO_CONTRATADA_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_contratada_ativo_Selectedvalue_get',ctrl:'DDO_CONTRATADA_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_ativo_Sortedstatus',ctrl:'DDO_CONTRATADA_ATIVO',prop:'SortedStatus'},{av:'AV69TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E24362',iparms:[],oparms:[{av:'AV31Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E25362',iparms:[{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A41Contratada_PessoaNom',fld:'CONTRATADA_PESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV7InOutContratada_Codigo',fld:'vINOUTCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutContratada_PessoaNom',fld:'vINOUTCONTRATADA_PESSOANOM',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15362',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV33Contratada_Codigo1',fld:'vCONTRATADA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV34Contratada_Codigo2',fld:'vCONTRATADA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV62TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV65TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV66TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV69TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E19362',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV33Contratada_Codigo1',fld:'vCONTRATADA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV34Contratada_Codigo2',fld:'vCONTRATADA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV62TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV65TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV66TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV69TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E16362',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV33Contratada_Codigo1',fld:'vCONTRATADA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV34Contratada_Codigo2',fld:'vCONTRATADA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV62TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV65TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV66TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV69TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV33Contratada_Codigo1',fld:'vCONTRATADA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV34Contratada_Codigo2',fld:'vCONTRATADA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'dynavContratada_codigo2'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'dynavContratada_codigo1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E20362',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'dynavContratada_codigo1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E17362',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV33Contratada_Codigo1',fld:'vCONTRATADA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV34Contratada_Codigo2',fld:'vCONTRATADA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV62TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV65TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV66TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV69TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV33Contratada_Codigo1',fld:'vCONTRATADA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV34Contratada_Codigo2',fld:'vCONTRATADA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'dynavContratada_codigo2'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'dynavContratada_codigo1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E21362',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'dynavContratada_codigo2'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E18362',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV33Contratada_Codigo1',fld:'vCONTRATADA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV34Contratada_Codigo2',fld:'vCONTRATADA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV61TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV62TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV65TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV66TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV69TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_Contratada_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV59Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV61TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'Ddo_contratada_pessoanom_Filteredtext_set',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'FilteredText_set'},{av:'AV62TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratada_pessoanom_Selectedvalue_set',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SelectedValue_set'},{av:'AV65TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'Ddo_contratada_pessoacnpj_Filteredtext_set',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'FilteredText_set'},{av:'AV66TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'Ddo_contratada_pessoacnpj_Selectedvalue_set',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SelectedValue_set'},{av:'AV69TFContratada_Ativo_Sel',fld:'vTFCONTRATADA_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_contratada_ativo_Selectedvalue_set',ctrl:'DDO_CONTRATADA_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'dynavContratada_codigo1'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV33Contratada_Codigo1',fld:'vCONTRATADA_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV34Contratada_Codigo2',fld:'vCONTRATADA_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'dynavContratada_codigo2'},{av:'cmbavDynamicfiltersoperator2'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutContratada_PessoaNom = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratada_pessoanom_Activeeventkey = "";
         Ddo_contratada_pessoanom_Filteredtext_get = "";
         Ddo_contratada_pessoanom_Selectedvalue_get = "";
         Ddo_contratada_pessoacnpj_Activeeventkey = "";
         Ddo_contratada_pessoacnpj_Filteredtext_get = "";
         Ddo_contratada_pessoacnpj_Selectedvalue_get = "";
         Ddo_contratada_ativo_Activeeventkey = "";
         Ddo_contratada_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV15DynamicFiltersSelector1 = "";
         AV17Contratada_PessoaNom1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV22Contratada_PessoaNom2 = "";
         AV61TFContratada_PessoaNom = "";
         AV62TFContratada_PessoaNom_Sel = "";
         AV65TFContratada_PessoaCNPJ = "";
         AV66TFContratada_PessoaCNPJ_Sel = "";
         AV63ddo_Contratada_PessoaNomTitleControlIdToReplace = "";
         AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace = "";
         AV70ddo_Contratada_AtivoTitleControlIdToReplace = "";
         AV78Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV71DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV60Contratada_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64Contratada_PessoaCNPJTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68Contratada_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratada_pessoanom_Filteredtext_set = "";
         Ddo_contratada_pessoanom_Selectedvalue_set = "";
         Ddo_contratada_pessoanom_Sortedstatus = "";
         Ddo_contratada_pessoacnpj_Filteredtext_set = "";
         Ddo_contratada_pessoacnpj_Selectedvalue_set = "";
         Ddo_contratada_pessoacnpj_Sortedstatus = "";
         Ddo_contratada_ativo_Selectedvalue_set = "";
         Ddo_contratada_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Select = "";
         AV77Select_GXI = "";
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         scmdbuf = "";
         H00362_A5AreaTrabalho_Codigo = new int[1] ;
         H00362_A6AreaTrabalho_Descricao = new String[] {""} ;
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H00363_A5AreaTrabalho_Codigo = new int[1] ;
         H00363_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00364_A40Contratada_PessoaCod = new int[1] ;
         H00364_A39Contratada_Codigo = new int[1] ;
         H00364_A41Contratada_PessoaNom = new String[] {""} ;
         H00364_n41Contratada_PessoaNom = new bool[] {false} ;
         H00364_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00365_A40Contratada_PessoaCod = new int[1] ;
         H00365_A39Contratada_Codigo = new int[1] ;
         H00365_A41Contratada_PessoaNom = new String[] {""} ;
         H00365_n41Contratada_PessoaNom = new bool[] {false} ;
         H00365_A52Contratada_AreaTrabalhoCod = new int[1] ;
         GridContainer = new GXWebGrid( context);
         lV17Contratada_PessoaNom1 = "";
         lV22Contratada_PessoaNom2 = "";
         lV61TFContratada_PessoaNom = "";
         lV65TFContratada_PessoaCNPJ = "";
         H00366_A43Contratada_Ativo = new bool[] {false} ;
         H00366_A42Contratada_PessoaCNPJ = new String[] {""} ;
         H00366_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         H00366_A41Contratada_PessoaNom = new String[] {""} ;
         H00366_n41Contratada_PessoaNom = new bool[] {false} ;
         H00366_A40Contratada_PessoaCod = new int[1] ;
         H00366_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00366_A39Contratada_Codigo = new int[1] ;
         H00367_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblFiltertextcontratada_areatrabalhocod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontratada__default(),
            new Object[][] {
                new Object[] {
               H00362_A5AreaTrabalho_Codigo, H00362_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00363_A5AreaTrabalho_Codigo, H00363_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00364_A40Contratada_PessoaCod, H00364_A39Contratada_Codigo, H00364_A41Contratada_PessoaNom, H00364_n41Contratada_PessoaNom, H00364_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00365_A40Contratada_PessoaCod, H00365_A39Contratada_Codigo, H00365_A41Contratada_PessoaNom, H00365_n41Contratada_PessoaNom, H00365_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00366_A43Contratada_Ativo, H00366_A42Contratada_PessoaCNPJ, H00366_n42Contratada_PessoaCNPJ, H00366_A41Contratada_PessoaNom, H00366_n41Contratada_PessoaNom, H00366_A40Contratada_PessoaCod, H00366_A52Contratada_AreaTrabalhoCod, H00366_A39Contratada_Codigo
               }
               , new Object[] {
               H00367_AGRID_nRecordCount
               }
            }
         );
         AV78Pgmname = "PromptContratada";
         /* GeneXus formulas. */
         AV78Pgmname = "PromptContratada";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_69 ;
      private short nGXsfl_69_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV69TFContratada_Ativo_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_69_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratada_PessoaNom_Titleformat ;
      private short edtContratada_PessoaCNPJ_Titleformat ;
      private short chkContratada_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContratada_Codigo ;
      private int wcpOAV7InOutContratada_Codigo ;
      private int subGrid_Rows ;
      private int AV33Contratada_Codigo1 ;
      private int AV34Contratada_Codigo2 ;
      private int AV59Contratada_AreaTrabalhoCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contratada_pessoanom_Datalistupdateminimumcharacters ;
      private int Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters ;
      private int edtavTfcontratada_pessoanom_Visible ;
      private int edtavTfcontratada_pessoanom_sel_Visible ;
      private int edtavTfcontratada_pessoacnpj_Visible ;
      private int edtavTfcontratada_pessoacnpj_sel_Visible ;
      private int edtavTfcontratada_ativo_sel_Visible ;
      private int edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_ativotitlecontrolidtoreplace_Visible ;
      private int A39Contratada_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A40Contratada_PessoaCod ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int edtavOrdereddsc_Visible ;
      private int AV72PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int edtavContratada_pessoanom1_Visible ;
      private int edtavContratada_pessoanom2_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV73GridCurrentPage ;
      private long AV74GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String AV8InOutContratada_PessoaNom ;
      private String wcpOAV8InOutContratada_PessoaNom ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratada_pessoanom_Activeeventkey ;
      private String Ddo_contratada_pessoanom_Filteredtext_get ;
      private String Ddo_contratada_pessoanom_Selectedvalue_get ;
      private String Ddo_contratada_pessoacnpj_Activeeventkey ;
      private String Ddo_contratada_pessoacnpj_Filteredtext_get ;
      private String Ddo_contratada_pessoacnpj_Selectedvalue_get ;
      private String Ddo_contratada_ativo_Activeeventkey ;
      private String Ddo_contratada_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_69_idx="0001" ;
      private String AV17Contratada_PessoaNom1 ;
      private String AV22Contratada_PessoaNom2 ;
      private String AV61TFContratada_PessoaNom ;
      private String AV62TFContratada_PessoaNom_Sel ;
      private String AV78Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratada_pessoanom_Caption ;
      private String Ddo_contratada_pessoanom_Tooltip ;
      private String Ddo_contratada_pessoanom_Cls ;
      private String Ddo_contratada_pessoanom_Filteredtext_set ;
      private String Ddo_contratada_pessoanom_Selectedvalue_set ;
      private String Ddo_contratada_pessoanom_Dropdownoptionstype ;
      private String Ddo_contratada_pessoanom_Titlecontrolidtoreplace ;
      private String Ddo_contratada_pessoanom_Sortedstatus ;
      private String Ddo_contratada_pessoanom_Filtertype ;
      private String Ddo_contratada_pessoanom_Datalisttype ;
      private String Ddo_contratada_pessoanom_Datalistproc ;
      private String Ddo_contratada_pessoanom_Sortasc ;
      private String Ddo_contratada_pessoanom_Sortdsc ;
      private String Ddo_contratada_pessoanom_Loadingdata ;
      private String Ddo_contratada_pessoanom_Cleanfilter ;
      private String Ddo_contratada_pessoanom_Noresultsfound ;
      private String Ddo_contratada_pessoanom_Searchbuttontext ;
      private String Ddo_contratada_pessoacnpj_Caption ;
      private String Ddo_contratada_pessoacnpj_Tooltip ;
      private String Ddo_contratada_pessoacnpj_Cls ;
      private String Ddo_contratada_pessoacnpj_Filteredtext_set ;
      private String Ddo_contratada_pessoacnpj_Selectedvalue_set ;
      private String Ddo_contratada_pessoacnpj_Dropdownoptionstype ;
      private String Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace ;
      private String Ddo_contratada_pessoacnpj_Sortedstatus ;
      private String Ddo_contratada_pessoacnpj_Filtertype ;
      private String Ddo_contratada_pessoacnpj_Datalisttype ;
      private String Ddo_contratada_pessoacnpj_Datalistproc ;
      private String Ddo_contratada_pessoacnpj_Sortasc ;
      private String Ddo_contratada_pessoacnpj_Sortdsc ;
      private String Ddo_contratada_pessoacnpj_Loadingdata ;
      private String Ddo_contratada_pessoacnpj_Cleanfilter ;
      private String Ddo_contratada_pessoacnpj_Noresultsfound ;
      private String Ddo_contratada_pessoacnpj_Searchbuttontext ;
      private String Ddo_contratada_ativo_Caption ;
      private String Ddo_contratada_ativo_Tooltip ;
      private String Ddo_contratada_ativo_Cls ;
      private String Ddo_contratada_ativo_Selectedvalue_set ;
      private String Ddo_contratada_ativo_Dropdownoptionstype ;
      private String Ddo_contratada_ativo_Titlecontrolidtoreplace ;
      private String Ddo_contratada_ativo_Sortedstatus ;
      private String Ddo_contratada_ativo_Datalisttype ;
      private String Ddo_contratada_ativo_Datalistfixedvalues ;
      private String Ddo_contratada_ativo_Sortasc ;
      private String Ddo_contratada_ativo_Sortdsc ;
      private String Ddo_contratada_ativo_Cleanfilter ;
      private String Ddo_contratada_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTfcontratada_pessoanom_Internalname ;
      private String edtavTfcontratada_pessoanom_Jsonclick ;
      private String edtavTfcontratada_pessoanom_sel_Internalname ;
      private String edtavTfcontratada_pessoanom_sel_Jsonclick ;
      private String edtavTfcontratada_pessoacnpj_Internalname ;
      private String edtavTfcontratada_pessoacnpj_Jsonclick ;
      private String edtavTfcontratada_pessoacnpj_sel_Internalname ;
      private String edtavTfcontratada_pessoacnpj_sel_Jsonclick ;
      private String edtavTfcontratada_ativo_sel_Internalname ;
      private String edtavTfcontratada_ativo_sel_Jsonclick ;
      private String edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContratada_Codigo_Internalname ;
      private String edtContratada_AreaTrabalhoCod_Internalname ;
      private String edtContratada_PessoaCod_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Internalname ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String chkContratada_Ativo_Internalname ;
      private String scmdbuf ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String lV17Contratada_PessoaNom1 ;
      private String lV22Contratada_PessoaNom2 ;
      private String lV61TFContratada_PessoaNom ;
      private String edtavOrdereddsc_Internalname ;
      private String dynavContratada_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContratada_pessoanom1_Internalname ;
      private String dynavContratada_codigo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContratada_pessoanom2_Internalname ;
      private String dynavContratada_codigo2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratada_pessoanom_Internalname ;
      private String Ddo_contratada_pessoacnpj_Internalname ;
      private String Ddo_contratada_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContratada_PessoaNom_Title ;
      private String edtContratada_PessoaCNPJ_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextcontratada_areatrabalhocod_Internalname ;
      private String lblFiltertextcontratada_areatrabalhocod_Jsonclick ;
      private String dynavContratada_areatrabalhocod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContratada_pessoanom2_Jsonclick ;
      private String dynavContratada_codigo2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContratada_pessoanom1_Jsonclick ;
      private String dynavContratada_codigo1_Jsonclick ;
      private String sGXsfl_69_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContratada_Codigo_Jsonclick ;
      private String edtContratada_AreaTrabalhoCod_Jsonclick ;
      private String edtContratada_PessoaCod_Jsonclick ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratada_pessoanom_Includesortasc ;
      private bool Ddo_contratada_pessoanom_Includesortdsc ;
      private bool Ddo_contratada_pessoanom_Includefilter ;
      private bool Ddo_contratada_pessoanom_Filterisrange ;
      private bool Ddo_contratada_pessoanom_Includedatalist ;
      private bool Ddo_contratada_pessoacnpj_Includesortasc ;
      private bool Ddo_contratada_pessoacnpj_Includesortdsc ;
      private bool Ddo_contratada_pessoacnpj_Includefilter ;
      private bool Ddo_contratada_pessoacnpj_Filterisrange ;
      private bool Ddo_contratada_pessoacnpj_Includedatalist ;
      private bool Ddo_contratada_ativo_Includesortasc ;
      private bool Ddo_contratada_ativo_Includesortdsc ;
      private bool Ddo_contratada_ativo_Includefilter ;
      private bool Ddo_contratada_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool A43Contratada_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV65TFContratada_PessoaCNPJ ;
      private String AV66TFContratada_PessoaCNPJ_Sel ;
      private String AV63ddo_Contratada_PessoaNomTitleControlIdToReplace ;
      private String AV67ddo_Contratada_PessoaCNPJTitleControlIdToReplace ;
      private String AV70ddo_Contratada_AtivoTitleControlIdToReplace ;
      private String AV77Select_GXI ;
      private String A42Contratada_PessoaCNPJ ;
      private String lV65TFContratada_PessoaCNPJ ;
      private String AV31Select ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContratada_Codigo ;
      private String aP1_InOutContratada_PessoaNom ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox dynavContratada_areatrabalhocod ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox dynavContratada_codigo1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox dynavContratada_codigo2 ;
      private GXCheckbox chkContratada_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private int[] H00362_A5AreaTrabalho_Codigo ;
      private String[] H00362_A6AreaTrabalho_Descricao ;
      private int[] H00363_A5AreaTrabalho_Codigo ;
      private String[] H00363_A6AreaTrabalho_Descricao ;
      private int[] H00364_A40Contratada_PessoaCod ;
      private int[] H00364_A39Contratada_Codigo ;
      private String[] H00364_A41Contratada_PessoaNom ;
      private bool[] H00364_n41Contratada_PessoaNom ;
      private int[] H00364_A52Contratada_AreaTrabalhoCod ;
      private int[] H00365_A40Contratada_PessoaCod ;
      private int[] H00365_A39Contratada_Codigo ;
      private String[] H00365_A41Contratada_PessoaNom ;
      private bool[] H00365_n41Contratada_PessoaNom ;
      private int[] H00365_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00366_A43Contratada_Ativo ;
      private String[] H00366_A42Contratada_PessoaCNPJ ;
      private bool[] H00366_n42Contratada_PessoaCNPJ ;
      private String[] H00366_A41Contratada_PessoaNom ;
      private bool[] H00366_n41Contratada_PessoaNom ;
      private int[] H00366_A40Contratada_PessoaCod ;
      private int[] H00366_A52Contratada_AreaTrabalhoCod ;
      private int[] H00366_A39Contratada_Codigo ;
      private long[] H00367_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV60Contratada_PessoaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV64Contratada_PessoaCNPJTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV68Contratada_AtivoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV71DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontratada__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00366( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17Contratada_PessoaNom1 ,
                                             int AV33Contratada_Codigo1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV22Contratada_PessoaNom2 ,
                                             int AV34Contratada_Codigo2 ,
                                             String AV62TFContratada_PessoaNom_Sel ,
                                             String AV61TFContratada_PessoaNom ,
                                             String AV66TFContratada_PessoaCNPJ_Sel ,
                                             String AV65TFContratada_PessoaCNPJ ,
                                             short AV69TFContratada_Ativo_Sel ,
                                             String A41Contratada_PessoaNom ,
                                             int A39Contratada_Codigo ,
                                             String A42Contratada_PessoaCNPJ ,
                                             bool A43Contratada_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [16] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Contratada_Ativo], T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_AreaTrabalhoCod], T1.[Contratada_Codigo]";
         sFromString = " FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Contratada_PessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV17Contratada_PessoaNom1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Contratada_PessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV17Contratada_PessoaNom1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_CODIGO") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! (0==AV33Contratada_Codigo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV33Contratada_Codigo1)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Contratada_PessoaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV22Contratada_PessoaNom2)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Contratada_PessoaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV22Contratada_PessoaNom2)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_CODIGO") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! (0==AV34Contratada_Codigo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV34Contratada_Codigo2)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFContratada_PessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV61TFContratada_PessoaNom)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFContratada_PessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV62TFContratada_PessoaNom_Sel)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFContratada_PessoaCNPJ)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV65TFContratada_PessoaCNPJ)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFContratada_PessoaCNPJ_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV66TFContratada_PessoaCNPJ_Sel)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV69TFContratada_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 1)";
         }
         if ( AV69TFContratada_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 0)";
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Docto]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Docto] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Ativo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00367( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17Contratada_PessoaNom1 ,
                                             int AV33Contratada_Codigo1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV22Contratada_PessoaNom2 ,
                                             int AV34Contratada_Codigo2 ,
                                             String AV62TFContratada_PessoaNom_Sel ,
                                             String AV61TFContratada_PessoaNom ,
                                             String AV66TFContratada_PessoaCNPJ_Sel ,
                                             String AV65TFContratada_PessoaCNPJ ,
                                             short AV69TFContratada_Ativo_Sel ,
                                             String A41Contratada_PessoaNom ,
                                             int A39Contratada_Codigo ,
                                             String A42Contratada_PessoaCNPJ ,
                                             bool A43Contratada_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [11] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Contratada_PessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV17Contratada_PessoaNom1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Contratada_PessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV17Contratada_PessoaNom1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_CODIGO") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! (0==AV33Contratada_Codigo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV33Contratada_Codigo1)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Contratada_PessoaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV22Contratada_PessoaNom2)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Contratada_PessoaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV22Contratada_PessoaNom2)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_CODIGO") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! (0==AV34Contratada_Codigo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV34Contratada_Codigo2)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFContratada_PessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV61TFContratada_PessoaNom)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFContratada_PessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV62TFContratada_PessoaNom_Sel)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFContratada_PessoaCNPJ)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV65TFContratada_PessoaCNPJ)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFContratada_PessoaCNPJ_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV66TFContratada_PessoaCNPJ_Sel)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV69TFContratada_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 1)";
         }
         if ( AV69TFContratada_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 4 :
                     return conditional_H00366(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (short)dynConstraints[18] , (bool)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] );
               case 5 :
                     return conditional_H00367(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (short)dynConstraints[18] , (bool)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00362 ;
          prmH00362 = new Object[] {
          } ;
          Object[] prmH00363 ;
          prmH00363 = new Object[] {
          } ;
          Object[] prmH00364 ;
          prmH00364 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00365 ;
          prmH00365 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00366 ;
          prmH00366 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV17Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV17Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV33Contratada_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV22Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV34Contratada_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV62TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV65TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV66TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00367 ;
          prmH00367 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV17Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV17Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV33Contratada_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV22Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV34Contratada_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV62TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV65TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV66TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00362", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00362,0,0,true,false )
             ,new CursorDef("H00363", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00363,0,0,true,false )
             ,new CursorDef("H00364", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00364,0,0,true,false )
             ,new CursorDef("H00365", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00365,0,0,true,false )
             ,new CursorDef("H00366", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00366,11,0,true,false )
             ,new CursorDef("H00367", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00367,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 4 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 5 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                return;
             case 5 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
       }
    }

 }

}
