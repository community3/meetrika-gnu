/**@preserve  GeneXus C# 10_3_14-114418 on 3/24/2020 23:8:27.40
*/
gx.evt.autoSkip = false;
gx.define('servico', false, function () {
   this.ServerClass =  "servico" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("trn");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.A1551Servico_Responsavel=gx.fn.getIntegerValue("SERVICO_RESPONSAVEL",'.') ;
      this.A640Servico_Vinculados=gx.fn.getIntegerValue("SERVICO_VINCULADOS",'.') ;
      this.A2107Servico_Identificacao=gx.fn.getControlValue("SERVICO_IDENTIFICACAO") ;
      this.AV7Servico_Codigo=gx.fn.getIntegerValue("vSERVICO_CODIGO",'.') ;
      this.AV11Insert_ServicoGrupo_Codigo=gx.fn.getIntegerValue("vINSERT_SERVICOGRUPO_CODIGO",'.') ;
      this.AV15Insert_Servico_UO=gx.fn.getIntegerValue("vINSERT_SERVICO_UO",'.') ;
      this.A633Servico_UO=gx.fn.getIntegerValue("SERVICO_UO",'.') ;
      this.AV14Insert_Servico_Vinculado=gx.fn.getIntegerValue("vINSERT_SERVICO_VINCULADO",'.') ;
      this.AV24Insert_Servico_LinNegCod=gx.fn.getIntegerValue("vINSERT_SERVICO_LINNEGCOD",'.') ;
      this.AV21Servico_Responsavel=gx.fn.getIntegerValue("vSERVICO_RESPONSAVEL",'.') ;
      this.Gx_BScreen=gx.fn.getIntegerValue("vGXBSCREEN",'.') ;
      this.A1530Servico_TipoHierarquia=gx.fn.getIntegerValue("SERVICO_TIPOHIERARQUIA",'.') ;
      this.AV16ServicoGrupo_Codigo=gx.fn.getIntegerValue("vSERVICOGRUPO_CODIGO",'.') ;
      this.AV8WWPContext=gx.fn.getControlValue("vWWPCONTEXT") ;
      this.AV19AuditingObject=gx.fn.getControlValue("vAUDITINGOBJECT") ;
      this.A158ServicoGrupo_Descricao=gx.fn.getControlValue("SERVICOGRUPO_DESCRICAO") ;
      this.A2048Servico_LinNegDsc=gx.fn.getControlValue("SERVICO_LINNEGDSC") ;
      this.A641Servico_VincDesc=gx.fn.getControlValue("SERVICO_VINCDESC") ;
      this.A1557Servico_VincSigla=gx.fn.getControlValue("SERVICO_VINCSIGLA") ;
      this.AV25Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.Gx_mode=gx.fn.getControlValue("vMODE") ;
      this.AV9TrnContext=gx.fn.getControlValue("vTRNCONTEXT") ;
   };
   this.Valid_Servico_codigo=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Servico_codigo",["gx.O.A155Servico_Codigo", "gx.O.A1551Servico_Responsavel", "gx.O.A640Servico_Vinculados"],["A1551Servico_Responsavel", "A640Servico_Vinculados"]);
      return true;
   }
   this.Validv_Servico_uo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vSERVICO_UO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Servicogrupo_codigo=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Servicogrupo_codigo",["gx.O.A157ServicoGrupo_Codigo", "gx.O.AV16ServicoGrupo_Codigo", "gx.O.AV7Servico_Codigo", "gx.O.A640Servico_Vinculados", "gx.O.A632Servico_Ativo", "gx.O.A158ServicoGrupo_Descricao", "gx.O.A631Servico_Vinculado"],["A158ServicoGrupo_Descricao", "A631Servico_Vinculado"]);
      return true;
   }
   this.Valid_Servico_nome=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("SERVICO_NOME");
         this.AnyError  = 0;
         if ( ((''==this.A608Servico_Nome)) )
         {
            try {
               gxballoon.setError("Nome é obrigatório.");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Servico_sigla=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("SERVICO_SIGLA");
         this.AnyError  = 0;
         try {
            this.A2107Servico_Identificacao =  gx.text.concat( gx.text.trim( this.A605Servico_Sigla), gx.text.trim( this.A608Servico_Nome), " - ")  ;
         }
         catch(e){}
         if ( ((''==this.A605Servico_Sigla)) )
         {
            try {
               gxballoon.setError("Sigla é obrigatório.");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Servico_vinculado=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Servico_vinculado",["gx.O.A631Servico_Vinculado", "gx.O.A641Servico_VincDesc", "gx.O.A1557Servico_VincSigla"],["A641Servico_VincDesc", "A1557Servico_VincSigla", ["A1534Servico_PercTmp","Visible"], ["SERVICO_PERCTMP_CELL","Class"], ["TEXTBLOCKSERVICO_PERCTMP_CELL","Class"]]);
      return true;
   }
   this.Valid_Servico_tela=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("SERVICO_TELA");
         this.AnyError  = 0;
         if ( ! ( ( this.A1061Servico_Tela == "CNT" ) || ( this.A1061Servico_Tela == "CHK" ) || ((''==this.A1061Servico_Tela)) ) )
         {
            try {
               gxballoon.setError("Campo Tela fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Servico_atende=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("SERVICO_ATENDE");
         this.AnyError  = 0;
         if ( ! ( ( this.A1072Servico_Atende == "S" ) || ( this.A1072Servico_Atende == "H" ) || ((''==this.A1072Servico_Atende)) ) )
         {
            try {
               gxballoon.setError("Campo Atende fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Servico_obrigavalores=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("SERVICO_OBRIGAVALORES");
         this.AnyError  = 0;
         if ( ! ( ( this.A1429Servico_ObrigaValores == "" ) || ( this.A1429Servico_ObrigaValores == "L" ) || ( this.A1429Servico_ObrigaValores == "B" ) || ( this.A1429Servico_ObrigaValores == "A" ) || ((''==this.A1429Servico_ObrigaValores)) ) )
         {
            try {
               gxballoon.setError("Campo Obriga Valores fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Servico_objetocontrole=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("SERVICO_OBJETOCONTROLE");
         this.AnyError  = 0;
         if ( ! ( ( this.A1436Servico_ObjetoControle == "SIS" ) || ( this.A1436Servico_ObjetoControle == "PRC" ) || ( this.A1436Servico_ObjetoControle == "PRD" ) ) )
         {
            try {
               gxballoon.setError("Campo Servico_Objeto Controle fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Servico_ispublico=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Servico_ispublico",["gx.O.A1635Servico_IsPublico", "gx.O.AV21Servico_Responsavel"],[["AV22Responsavel_Codigo","Visible"], ["TEXTBLOCKRESPONSAVEL_CODIGO","Visible"], "AV21Servico_Responsavel"]);
      return true;
   }
   this.Validv_Responsavel_codigo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vRESPONSAVEL_CODIGO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Servico_linnegcod=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Servico_linnegcod",["gx.O.A2047Servico_LinNegCod", "gx.O.A2048Servico_LinNegDsc"],["A2048Servico_LinNegDsc"]);
      return true;
   }
   this.Valid_Servico_ativo=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Servico_ativo",["gx.O.AV16ServicoGrupo_Codigo", "gx.O.AV7Servico_Codigo", "gx.O.A157ServicoGrupo_Codigo", "gx.O.A640Servico_Vinculados", "gx.O.A632Servico_Ativo", "gx.O.A631Servico_Vinculado", "gx.O.A1545Servico_Anterior", "gx.O.A1546Servico_Posterior"],["A631Servico_Vinculado", "A1545Servico_Anterior", "A1546Servico_Posterior"]);
      return true;
   }
   this.Validv_Contratante_codigo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATANTE_CODIGO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.s112_client=function()
   {
      gx.fn.setCtrlProperty("SERVICO_PERCTMP","Visible", false );
      gx.fn.setCtrlProperty("SERVICO_PERCTMP_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("TEXTBLOCKSERVICO_PERCTMP_CELL","Class", "Invisible" );
   };
   this.e110v130_client=function()
   {
      this.clearMessages();
      gx.popup.openUrl(gx.http.formatLink("wp_selecionauo.aspx",[this.AV17Servico_UO]), ["AV17Servico_UO"]);
      this.refreshOutputs([{av:'AV17Servico_UO',fld:'vSERVICO_UO',pic:'ZZZZZ9',nv:0}]);
   };
   this.e130v2_client=function()
   {
      this.executeServerEvent("AFTER TRN", true, null, false, false);
   };
   this.e140v2_client=function()
   {
      this.executeServerEvent("SERVICO_ISPUBLICO.CLICK", true, null, false, true);
   };
   this.e150v130_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e160v130_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,13,16,18,21,23,25,27,32,34,41,43,45,47,49,51,54,56,59,61,63,65,70,72,74,76,81,83,85,87,89,91,94,96,99,101,103,105,108,110,112,114,117,119,122,124,126,128,133,135,137,139,141,143,146,148,155,163,164];
   this.GXLastCtrlId =164;
   this.DVPANEL_TABLEATTRIBUTESContainer = gx.uc.getNew(this, 11, 0, "BootstrapPanel", "DVPANEL_TABLEATTRIBUTESContainer", "Dvpanel_tableattributes");
   var DVPANEL_TABLEATTRIBUTESContainer = this.DVPANEL_TABLEATTRIBUTESContainer;
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Width", "Width", "100%", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Height", "Height", "100", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Title", "Title", "Serviço", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsible", "Collapsible", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Visible", "Visible", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Class", "Class", "", "char");
   DVPANEL_TABLEATTRIBUTESContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_TABLEATTRIBUTESContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[5]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[13]={fld:"TABLEATTRIBUTES",grid:0};
   GXValidFnc[16]={fld:"TEXTBLOCKSERVICO_UO", format:0,grid:0};
   GXValidFnc[18]={fld:"TABLEMERGEDSERVICO_UO",grid:0};
   GXValidFnc[21]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Servico_uo,isvalid:null,rgrid:[],fld:"vSERVICO_UO",gxz:"ZV17Servico_UO",gxold:"OV17Servico_UO",gxvar:"AV17Servico_UO",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV17Servico_UO=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV17Servico_UO=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vSERVICO_UO",gx.O.AV17Servico_UO)},c2v:function(){if(this.val()!==undefined)gx.O.AV17Servico_UO=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vSERVICO_UO",'.')},nac:gx.falseFn};
   GXValidFnc[23]={fld:"SELECTUO",grid:0};
   GXValidFnc[25]={fld:"TEXTBLOCKSERVICO_UORESPEXCLUSIVA", format:0,grid:0};
   GXValidFnc[27]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"SERVICO_UORESPEXCLUSIVA",gxz:"Z1077Servico_UORespExclusiva",gxold:"O1077Servico_UORespExclusiva",gxvar:"A1077Servico_UORespExclusiva",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1077Servico_UORespExclusiva=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1077Servico_UORespExclusiva=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setComboBoxValue("SERVICO_UORESPEXCLUSIVA",gx.O.A1077Servico_UORespExclusiva);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1077Servico_UORespExclusiva=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("SERVICO_UORESPEXCLUSIVA")},nac:gx.falseFn};
   this.declareDomainHdlr( 27 , function() {
   });
   GXValidFnc[32]={fld:"TEXTBLOCKSERVICOGRUPO_CODIGO", format:0,grid:0};
   GXValidFnc[34]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Servicogrupo_codigo,isvalid:null,rgrid:[],fld:"SERVICOGRUPO_CODIGO",gxz:"Z157ServicoGrupo_Codigo",gxold:"O157ServicoGrupo_Codigo",gxvar:"A157ServicoGrupo_Codigo",ucs:[],op:[51],ip:[51,148,34],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.A157ServicoGrupo_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z157ServicoGrupo_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("SERVICOGRUPO_CODIGO",gx.O.A157ServicoGrupo_Codigo);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A157ServicoGrupo_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("SERVICOGRUPO_CODIGO",'.')},nac:function(){return (this.Gx_mode=="INS")&&!((0==this.AV11Insert_ServicoGrupo_Codigo))}};
   this.declareDomainHdlr( 34 , function() {
   });
   GXValidFnc[41]={fld:"TEXTBLOCKSERVICO_NOME", format:0,grid:0};
   GXValidFnc[43]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Servico_nome,isvalid:null,rgrid:[],fld:"SERVICO_NOME",gxz:"Z608Servico_Nome",gxold:"O608Servico_Nome",gxvar:"A608Servico_Nome",ucs:[],op:[43],ip:[43],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A608Servico_Nome=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z608Servico_Nome=Value},v2c:function(){gx.fn.setControlValue("SERVICO_NOME",gx.O.A608Servico_Nome,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A608Servico_Nome=this.val()},val:function(){return gx.fn.getControlValue("SERVICO_NOME")},nac:gx.falseFn};
   this.declareDomainHdlr( 43 , function() {
   });
   GXValidFnc[45]={fld:"TEXTBLOCKSERVICO_SIGLA", format:0,grid:0};
   GXValidFnc[47]={lvl:0,type:"char",len:15,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Servico_sigla,isvalid:null,rgrid:[],fld:"SERVICO_SIGLA",gxz:"Z605Servico_Sigla",gxold:"O605Servico_Sigla",gxvar:"A605Servico_Sigla",ucs:[],op:[47],ip:[43,47],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A605Servico_Sigla=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z605Servico_Sigla=Value},v2c:function(){gx.fn.setControlValue("SERVICO_SIGLA",gx.O.A605Servico_Sigla,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A605Servico_Sigla=this.val()},val:function(){return gx.fn.getControlValue("SERVICO_SIGLA")},nac:gx.falseFn};
   this.declareDomainHdlr( 47 , function() {
   });
   GXValidFnc[49]={fld:"TEXTBLOCKSERVICO_VINCULADO", format:0,grid:0};
   GXValidFnc[51]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Servico_vinculado,isvalid:null,rgrid:[],fld:"SERVICO_VINCULADO",gxz:"Z631Servico_Vinculado",gxold:"O631Servico_Vinculado",gxvar:"A631Servico_Vinculado",ucs:[],op:[],ip:[51],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.A631Servico_Vinculado=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z631Servico_Vinculado=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("SERVICO_VINCULADO",gx.O.A631Servico_Vinculado);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A631Servico_Vinculado=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("SERVICO_VINCULADO",'.')},nac:function(){return (this.Gx_mode=="INS")&&!((0==this.AV14Insert_Servico_Vinculado))}};
   this.declareDomainHdlr( 51 , function() {
   });
   GXValidFnc[54]={fld:"TEXTBLOCKSERVICO_DESCRICAO", format:0,grid:0};
   GXValidFnc[56]={lvl:0,type:"vchar",len:500,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"SERVICO_DESCRICAO",gxz:"Z156Servico_Descricao",gxold:"O156Servico_Descricao",gxvar:"A156Servico_Descricao",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A156Servico_Descricao=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z156Servico_Descricao=Value},v2c:function(){gx.fn.setControlValue("SERVICO_DESCRICAO",gx.O.A156Servico_Descricao,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A156Servico_Descricao=this.val()},val:function(){return gx.fn.getControlValue("SERVICO_DESCRICAO")},nac:gx.falseFn};
   this.declareDomainHdlr( 56 , function() {
   });
   GXValidFnc[59]={fld:"TEXTBLOCKSERVICO_ANTERIOR", format:0,grid:0};
   GXValidFnc[61]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"SERVICO_ANTERIOR",gxz:"Z1545Servico_Anterior",gxold:"O1545Servico_Anterior",gxvar:"A1545Servico_Anterior",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.A1545Servico_Anterior=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1545Servico_Anterior=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("SERVICO_ANTERIOR",gx.O.A1545Servico_Anterior);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1545Servico_Anterior=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("SERVICO_ANTERIOR",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 61 , function() {
   });
   GXValidFnc[63]={fld:"TEXTBLOCKSERVICO_POSTERIOR", format:0,grid:0};
   GXValidFnc[65]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"SERVICO_POSTERIOR",gxz:"Z1546Servico_Posterior",gxold:"O1546Servico_Posterior",gxvar:"A1546Servico_Posterior",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.A1546Servico_Posterior=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1546Servico_Posterior=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("SERVICO_POSTERIOR",gx.O.A1546Servico_Posterior);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1546Servico_Posterior=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("SERVICO_POSTERIOR",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 65 , function() {
   });
   GXValidFnc[70]={fld:"TEXTBLOCKSERVICO_TERCERIZA", format:0,grid:0};
   GXValidFnc[72]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"SERVICO_TERCERIZA",gxz:"Z889Servico_Terceriza",gxold:"O889Servico_Terceriza",gxvar:"A889Servico_Terceriza",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A889Servico_Terceriza=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z889Servico_Terceriza=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setComboBoxValue("SERVICO_TERCERIZA",gx.O.A889Servico_Terceriza)},c2v:function(){if(this.val()!==undefined)gx.O.A889Servico_Terceriza=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("SERVICO_TERCERIZA")},nac:gx.falseFn};
   GXValidFnc[74]={fld:"TEXTBLOCKSERVICO_TELA", format:0,grid:0};
   GXValidFnc[76]={lvl:0,type:"char",len:3,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Valid_Servico_tela,isvalid:null,rgrid:[],fld:"SERVICO_TELA",gxz:"Z1061Servico_Tela",gxold:"O1061Servico_Tela",gxvar:"A1061Servico_Tela",ucs:[],op:[76],ip:[76],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1061Servico_Tela=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1061Servico_Tela=Value},v2c:function(){gx.fn.setComboBoxValue("SERVICO_TELA",gx.O.A1061Servico_Tela);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1061Servico_Tela=this.val()},val:function(){return gx.fn.getControlValue("SERVICO_TELA")},nac:gx.falseFn};
   this.declareDomainHdlr( 76 , function() {
   });
   GXValidFnc[81]={fld:"TEXTBLOCKSERVICO_ATENDE", format:0,grid:0};
   GXValidFnc[83]={lvl:0,type:"char",len:1,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Valid_Servico_atende,isvalid:null,rgrid:[],fld:"SERVICO_ATENDE",gxz:"Z1072Servico_Atende",gxold:"O1072Servico_Atende",gxvar:"A1072Servico_Atende",ucs:[],op:[83],ip:[83],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1072Servico_Atende=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1072Servico_Atende=Value},v2c:function(){gx.fn.setComboBoxValue("SERVICO_ATENDE",gx.O.A1072Servico_Atende);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1072Servico_Atende=this.val()},val:function(){return gx.fn.getControlValue("SERVICO_ATENDE")},nac:gx.falseFn};
   this.declareDomainHdlr( 83 , function() {
   });
   GXValidFnc[85]={fld:"TEXTBLOCKSERVICO_OBRIGAVALORES", format:0,grid:0};
   GXValidFnc[87]={lvl:0,type:"char",len:1,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Valid_Servico_obrigavalores,isvalid:null,rgrid:[],fld:"SERVICO_OBRIGAVALORES",gxz:"Z1429Servico_ObrigaValores",gxold:"O1429Servico_ObrigaValores",gxvar:"A1429Servico_ObrigaValores",ucs:[],op:[87],ip:[87],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1429Servico_ObrigaValores=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1429Servico_ObrigaValores=Value},v2c:function(){gx.fn.setComboBoxValue("SERVICO_OBRIGAVALORES",gx.O.A1429Servico_ObrigaValores);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1429Servico_ObrigaValores=this.val()},val:function(){return gx.fn.getControlValue("SERVICO_OBRIGAVALORES")},nac:gx.falseFn};
   this.declareDomainHdlr( 87 , function() {
   });
   GXValidFnc[89]={fld:"TEXTBLOCKSERVICO_OBJETOCONTROLE", format:0,grid:0};
   GXValidFnc[91]={lvl:0,type:"char",len:3,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Valid_Servico_objetocontrole,isvalid:null,rgrid:[],fld:"SERVICO_OBJETOCONTROLE",gxz:"Z1436Servico_ObjetoControle",gxold:"O1436Servico_ObjetoControle",gxvar:"A1436Servico_ObjetoControle",ucs:[],op:[91],ip:[91],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1436Servico_ObjetoControle=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1436Servico_ObjetoControle=Value},v2c:function(){gx.fn.setComboBoxValue("SERVICO_OBJETOCONTROLE",gx.O.A1436Servico_ObjetoControle);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1436Servico_ObjetoControle=this.val()},val:function(){return gx.fn.getControlValue("SERVICO_OBJETOCONTROLE")},nac:gx.falseFn};
   this.declareDomainHdlr( 91 , function() {
   });
   GXValidFnc[94]={fld:"TEXTBLOCKSERVICO_PERCPGM", format:0,grid:0};
   GXValidFnc[96]={fld:"TABLEMERGEDSERVICO_PERCPGM",grid:0};
   GXValidFnc[99]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"SERVICO_PERCPGM",gxz:"Z1535Servico_PercPgm",gxold:"O1535Servico_PercPgm",gxvar:"A1535Servico_PercPgm",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1535Servico_PercPgm=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1535Servico_PercPgm=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("SERVICO_PERCPGM",gx.O.A1535Servico_PercPgm,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1535Servico_PercPgm=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("SERVICO_PERCPGM",'.')},nac:gx.falseFn};
   GXValidFnc[101]={fld:"SERVICO_PERCPGM_RIGHTTEXT", format:0,grid:0};
   GXValidFnc[103]={fld:"TEXTBLOCKSERVICO_PERCCNC", format:0,grid:0};
   GXValidFnc[105]={fld:"TABLEMERGEDSERVICO_PERCCNC",grid:0};
   GXValidFnc[108]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"SERVICO_PERCCNC",gxz:"Z1536Servico_PercCnc",gxold:"O1536Servico_PercCnc",gxvar:"A1536Servico_PercCnc",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1536Servico_PercCnc=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1536Servico_PercCnc=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("SERVICO_PERCCNC",gx.O.A1536Servico_PercCnc,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1536Servico_PercCnc=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("SERVICO_PERCCNC",'.')},nac:gx.falseFn};
   GXValidFnc[110]={fld:"SERVICO_PERCCNC_RIGHTTEXT", format:0,grid:0};
   GXValidFnc[112]={fld:"TEXTBLOCKSERVICO_PERCTMP", format:0,grid:0};
   GXValidFnc[114]={fld:"TABLEMERGEDSERVICO_PERCTMP",grid:0};
   GXValidFnc[117]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"SERVICO_PERCTMP",gxz:"Z1534Servico_PercTmp",gxold:"O1534Servico_PercTmp",gxvar:"A1534Servico_PercTmp",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1534Servico_PercTmp=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1534Servico_PercTmp=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("SERVICO_PERCTMP",gx.O.A1534Servico_PercTmp,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1534Servico_PercTmp=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("SERVICO_PERCTMP",'.')},nac:gx.falseFn};
   GXValidFnc[119]={fld:"SERVICO_PERCTMP_RIGHTTEXT", format:0,grid:0};
   GXValidFnc[122]={fld:"TEXTBLOCKSERVICO_ISPUBLICO", format:0,grid:0};
   GXValidFnc[124]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Valid_Servico_ispublico,isvalid:null,rgrid:[],fld:"SERVICO_ISPUBLICO",gxz:"Z1635Servico_IsPublico",gxold:"O1635Servico_IsPublico",gxvar:"A1635Servico_IsPublico",ucs:[],op:[],ip:[124],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1635Servico_IsPublico=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1635Servico_IsPublico=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setComboBoxValue("SERVICO_ISPUBLICO",gx.O.A1635Servico_IsPublico);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1635Servico_IsPublico=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("SERVICO_ISPUBLICO")},nac:gx.falseFn};
   this.declareDomainHdlr( 124 , function() {
   });
   GXValidFnc[126]={fld:"TEXTBLOCKRESPONSAVEL_CODIGO", format:0,grid:0};
   GXValidFnc[128]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Responsavel_codigo,isvalid:null,rgrid:[],fld:"vRESPONSAVEL_CODIGO",gxz:"ZV22Responsavel_Codigo",gxold:"OV22Responsavel_Codigo",gxvar:"AV22Responsavel_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV22Responsavel_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV22Responsavel_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vRESPONSAVEL_CODIGO",gx.O.AV22Responsavel_Codigo);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.AV22Responsavel_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vRESPONSAVEL_CODIGO",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 128 , function() {
   });
   GXValidFnc[133]={fld:"TEXTBLOCKSERVICO_LINNEGCOD", format:0,grid:0};
   GXValidFnc[135]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Servico_linnegcod,isvalid:null,rgrid:[],fld:"SERVICO_LINNEGCOD",gxz:"Z2047Servico_LinNegCod",gxold:"O2047Servico_LinNegCod",gxvar:"A2047Servico_LinNegCod",ucs:[],op:[],ip:[135],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.A2047Servico_LinNegCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z2047Servico_LinNegCod=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("SERVICO_LINNEGCOD",gx.O.A2047Servico_LinNegCod);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A2047Servico_LinNegCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("SERVICO_LINNEGCOD",'.')},nac:function(){return (this.Gx_mode=="INS")&&!((0==this.AV24Insert_Servico_LinNegCod))}};
   this.declareDomainHdlr( 135 , function() {
   });
   GXValidFnc[137]={fld:"TEXTBLOCKSERVICO_ISORIGEMREFERENCIA", format:0,grid:0};
   GXValidFnc[139]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"SERVICO_ISORIGEMREFERENCIA",gxz:"Z2092Servico_IsOrigemReferencia",gxold:"O2092Servico_IsOrigemReferencia",gxvar:"A2092Servico_IsOrigemReferencia",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A2092Servico_IsOrigemReferencia=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z2092Servico_IsOrigemReferencia=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setComboBoxValue("SERVICO_ISORIGEMREFERENCIA",gx.O.A2092Servico_IsOrigemReferencia);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A2092Servico_IsOrigemReferencia=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("SERVICO_ISORIGEMREFERENCIA")},nac:gx.falseFn};
   this.declareDomainHdlr( 139 , function() {
   });
   GXValidFnc[141]={fld:"TEXTBLOCKSERVICO_PAUSASLA", format:0,grid:0};
   GXValidFnc[143]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"SERVICO_PAUSASLA",gxz:"Z2131Servico_PausaSLA",gxold:"O2131Servico_PausaSLA",gxvar:"A2131Servico_PausaSLA",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A2131Servico_PausaSLA=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z2131Servico_PausaSLA=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setComboBoxValue("SERVICO_PAUSASLA",gx.O.A2131Servico_PausaSLA);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A2131Servico_PausaSLA=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("SERVICO_PAUSASLA")},nac:gx.falseFn};
   this.declareDomainHdlr( 143 , function() {
   });
   GXValidFnc[146]={fld:"TEXTBLOCKSERVICO_ATIVO", format:0,grid:0};
   GXValidFnc[148]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Valid_Servico_ativo,isvalid:null,rgrid:[],fld:"SERVICO_ATIVO",gxz:"Z632Servico_Ativo",gxold:"O632Servico_Ativo",gxvar:"A632Servico_Ativo",ucs:[],op:[65,61,51],ip:[65,61,51,148,34],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A632Servico_Ativo=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z632Servico_Ativo=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setComboBoxValue("SERVICO_ATIVO",gx.O.A632Servico_Ativo);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A632Servico_Ativo=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("SERVICO_ATIVO")},nac:gx.falseFn};
   this.declareDomainHdlr( 148 , function() {
   });
   GXValidFnc[155]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[163]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Validv_Contratante_codigo,isvalid:null,rgrid:[],fld:"vCONTRATANTE_CODIGO",gxz:"ZV20Contratante_Codigo",gxold:"OV20Contratante_Codigo",gxvar:"AV20Contratante_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV20Contratante_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV20Contratante_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vCONTRATANTE_CODIGO",gx.O.AV20Contratante_Codigo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV20Contratante_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTRATANTE_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[164]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Servico_codigo,isvalid:null,rgrid:[],fld:"SERVICO_CODIGO",gxz:"Z155Servico_Codigo",gxold:"O155Servico_Codigo",gxvar:"A155Servico_Codigo",ucs:[],op:[],ip:[164],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A155Servico_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z155Servico_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("SERVICO_CODIGO",gx.O.A155Servico_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A155Servico_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("SERVICO_CODIGO",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 164 , function() {
   });
   this.AV17Servico_UO = 0 ;
   this.ZV17Servico_UO = 0 ;
   this.OV17Servico_UO = 0 ;
   this.A1077Servico_UORespExclusiva = false ;
   this.Z1077Servico_UORespExclusiva = false ;
   this.O1077Servico_UORespExclusiva = false ;
   this.A157ServicoGrupo_Codigo = 0 ;
   this.Z157ServicoGrupo_Codigo = 0 ;
   this.O157ServicoGrupo_Codigo = 0 ;
   this.A608Servico_Nome = "" ;
   this.Z608Servico_Nome = "" ;
   this.O608Servico_Nome = "" ;
   this.A605Servico_Sigla = "" ;
   this.Z605Servico_Sigla = "" ;
   this.O605Servico_Sigla = "" ;
   this.A631Servico_Vinculado = 0 ;
   this.Z631Servico_Vinculado = 0 ;
   this.O631Servico_Vinculado = 0 ;
   this.A156Servico_Descricao = "" ;
   this.Z156Servico_Descricao = "" ;
   this.O156Servico_Descricao = "" ;
   this.A1545Servico_Anterior = 0 ;
   this.Z1545Servico_Anterior = 0 ;
   this.O1545Servico_Anterior = 0 ;
   this.A1546Servico_Posterior = 0 ;
   this.Z1546Servico_Posterior = 0 ;
   this.O1546Servico_Posterior = 0 ;
   this.A889Servico_Terceriza = false ;
   this.Z889Servico_Terceriza = false ;
   this.O889Servico_Terceriza = false ;
   this.A1061Servico_Tela = "" ;
   this.Z1061Servico_Tela = "" ;
   this.O1061Servico_Tela = "" ;
   this.A1072Servico_Atende = "" ;
   this.Z1072Servico_Atende = "" ;
   this.O1072Servico_Atende = "" ;
   this.A1429Servico_ObrigaValores = "" ;
   this.Z1429Servico_ObrigaValores = "" ;
   this.O1429Servico_ObrigaValores = "" ;
   this.A1436Servico_ObjetoControle = "" ;
   this.Z1436Servico_ObjetoControle = "" ;
   this.O1436Servico_ObjetoControle = "" ;
   this.A1535Servico_PercPgm = 0 ;
   this.Z1535Servico_PercPgm = 0 ;
   this.O1535Servico_PercPgm = 0 ;
   this.A1536Servico_PercCnc = 0 ;
   this.Z1536Servico_PercCnc = 0 ;
   this.O1536Servico_PercCnc = 0 ;
   this.A1534Servico_PercTmp = 0 ;
   this.Z1534Servico_PercTmp = 0 ;
   this.O1534Servico_PercTmp = 0 ;
   this.A1635Servico_IsPublico = false ;
   this.Z1635Servico_IsPublico = false ;
   this.O1635Servico_IsPublico = false ;
   this.AV22Responsavel_Codigo = 0 ;
   this.ZV22Responsavel_Codigo = 0 ;
   this.OV22Responsavel_Codigo = 0 ;
   this.A2047Servico_LinNegCod = 0 ;
   this.Z2047Servico_LinNegCod = 0 ;
   this.O2047Servico_LinNegCod = 0 ;
   this.A2092Servico_IsOrigemReferencia = false ;
   this.Z2092Servico_IsOrigemReferencia = false ;
   this.O2092Servico_IsOrigemReferencia = false ;
   this.A2131Servico_PausaSLA = false ;
   this.Z2131Servico_PausaSLA = false ;
   this.O2131Servico_PausaSLA = false ;
   this.A632Servico_Ativo = false ;
   this.Z632Servico_Ativo = false ;
   this.O632Servico_Ativo = false ;
   this.AV20Contratante_Codigo = 0 ;
   this.ZV20Contratante_Codigo = 0 ;
   this.OV20Contratante_Codigo = 0 ;
   this.A155Servico_Codigo = 0 ;
   this.Z155Servico_Codigo = 0 ;
   this.O155Servico_Codigo = 0 ;
   this.AV8WWPContext = {} ;
   this.AV9TrnContext = {} ;
   this.AV26GXV1 = 0 ;
   this.AV11Insert_ServicoGrupo_Codigo = 0 ;
   this.AV15Insert_Servico_UO = 0 ;
   this.AV14Insert_Servico_Vinculado = 0 ;
   this.AV24Insert_Servico_LinNegCod = 0 ;
   this.AV17Servico_UO = 0 ;
   this.AV22Responsavel_Codigo = 0 ;
   this.AV20Contratante_Codigo = 0 ;
   this.AV12TrnContextAtt = {} ;
   this.AV7Servico_Codigo = 0 ;
   this.AV16ServicoGrupo_Codigo = 0 ;
   this.AV10WebSession = {} ;
   this.A155Servico_Codigo = 0 ;
   this.A157ServicoGrupo_Codigo = 0 ;
   this.A633Servico_UO = 0 ;
   this.A631Servico_Vinculado = 0 ;
   this.A2047Servico_LinNegCod = 0 ;
   this.AV19AuditingObject = {} ;
   this.Gx_BScreen = 0 ;
   this.AV25Pgmname = "" ;
   this.A2107Servico_Identificacao = "" ;
   this.A640Servico_Vinculados = 0 ;
   this.A1551Servico_Responsavel = 0 ;
   this.AV21Servico_Responsavel = 0 ;
   this.A608Servico_Nome = "" ;
   this.A156Servico_Descricao = "" ;
   this.A605Servico_Sigla = "" ;
   this.A158ServicoGrupo_Descricao = "" ;
   this.A1077Servico_UORespExclusiva = false ;
   this.A641Servico_VincDesc = "" ;
   this.A1557Servico_VincSigla = "" ;
   this.A889Servico_Terceriza = false ;
   this.A1061Servico_Tela = "" ;
   this.A1072Servico_Atende = "" ;
   this.A1429Servico_ObrigaValores = "" ;
   this.A1436Servico_ObjetoControle = "" ;
   this.A1530Servico_TipoHierarquia = 0 ;
   this.A1534Servico_PercTmp = 0 ;
   this.A1535Servico_PercPgm = 0 ;
   this.A1536Servico_PercCnc = 0 ;
   this.A1545Servico_Anterior = 0 ;
   this.A1546Servico_Posterior = 0 ;
   this.A632Servico_Ativo = false ;
   this.A1635Servico_IsPublico = false ;
   this.A2048Servico_LinNegDsc = "" ;
   this.A2092Servico_IsOrigemReferencia = false ;
   this.A2131Servico_PausaSLA = false ;
   this.Gx_mode = "" ;
   this.Events = {"e130v2_client": ["AFTER TRN", true] ,"e140v2_client": ["SERVICO_ISPUBLICO.CLICK", true] ,"e150v130_client": ["ENTER", true] ,"e160v130_client": ["CANCEL", true] ,"e110v130_client": ["'DOSELECTUO'", false]};
   this.EvtParms["ENTER"] = [[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],[]];
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["AFTER TRN"] = [[{av:'AV19AuditingObject',fld:'vAUDITINGOBJECT',pic:'',nv:null},{av:'AV25Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],[]];
   this.EvtParms["'DOSELECTUO'"] = [[{av:'AV17Servico_UO',fld:'vSERVICO_UO',pic:'ZZZZZ9',nv:0}],[{av:'AV17Servico_UO',fld:'vSERVICO_UO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["SERVICO_ISPUBLICO.CLICK"] = [[{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV7Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[{av:'AV22Responsavel_Codigo',fld:'vRESPONSAVEL_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EnterCtrl = ["BTN_TRN_ENTER"];
   this.setVCMap("A1551Servico_Responsavel", "SERVICO_RESPONSAVEL", 0, "int");
   this.setVCMap("A640Servico_Vinculados", "SERVICO_VINCULADOS", 0, "int");
   this.setVCMap("A2107Servico_Identificacao", "SERVICO_IDENTIFICACAO", 0, "svchar");
   this.setVCMap("AV7Servico_Codigo", "vSERVICO_CODIGO", 0, "int");
   this.setVCMap("AV11Insert_ServicoGrupo_Codigo", "vINSERT_SERVICOGRUPO_CODIGO", 0, "int");
   this.setVCMap("AV15Insert_Servico_UO", "vINSERT_SERVICO_UO", 0, "int");
   this.setVCMap("A633Servico_UO", "SERVICO_UO", 0, "int");
   this.setVCMap("AV14Insert_Servico_Vinculado", "vINSERT_SERVICO_VINCULADO", 0, "int");
   this.setVCMap("AV24Insert_Servico_LinNegCod", "vINSERT_SERVICO_LINNEGCOD", 0, "int");
   this.setVCMap("AV21Servico_Responsavel", "vSERVICO_RESPONSAVEL", 0, "int");
   this.setVCMap("Gx_BScreen", "vGXBSCREEN", 0, "int");
   this.setVCMap("A1530Servico_TipoHierarquia", "SERVICO_TIPOHIERARQUIA", 0, "int");
   this.setVCMap("AV16ServicoGrupo_Codigo", "vSERVICOGRUPO_CODIGO", 0, "int");
   this.setVCMap("AV8WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("AV19AuditingObject", "vAUDITINGOBJECT", 0, "WWPBaseObjects\AuditingObject");
   this.setVCMap("A158ServicoGrupo_Descricao", "SERVICOGRUPO_DESCRICAO", 0, "svchar");
   this.setVCMap("A2048Servico_LinNegDsc", "SERVICO_LINNEGDSC", 0, "svchar");
   this.setVCMap("A641Servico_VincDesc", "SERVICO_VINCDESC", 0, "vchar");
   this.setVCMap("A1557Servico_VincSigla", "SERVICO_VINCSIGLA", 0, "char");
   this.setVCMap("AV25Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("Gx_mode", "vMODE", 0, "char");
   this.setVCMap("AV9TrnContext", "vTRNCONTEXT", 0, "WWPBaseObjects\WWPTransactionContext");
   this.InitStandaloneVars( );
});
gx.createParentObj(servico);
