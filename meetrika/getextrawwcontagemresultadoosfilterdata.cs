/*
               File: GetExtraWWContagemResultadoOSFilterData
        Description: Get Extra WWContagem Resultado OSFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:5:32.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getextrawwcontagemresultadoosfilterdata : GXProcedure
   {
      public getextrawwcontagemresultadoosfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getextrawwcontagemresultadoosfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV41DDOName = aP0_DDOName;
         this.AV39SearchTxt = aP1_SearchTxt;
         this.AV40SearchTxtTo = aP2_SearchTxtTo;
         this.AV45OptionsJson = "" ;
         this.AV48OptionsDescJson = "" ;
         this.AV50OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV45OptionsJson;
         aP4_OptionsDescJson=this.AV48OptionsDescJson;
         aP5_OptionIndexesJson=this.AV50OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV41DDOName = aP0_DDOName;
         this.AV39SearchTxt = aP1_SearchTxt;
         this.AV40SearchTxtTo = aP2_SearchTxtTo;
         this.AV45OptionsJson = "" ;
         this.AV48OptionsDescJson = "" ;
         this.AV50OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV45OptionsJson;
         aP4_OptionsDescJson=this.AV48OptionsDescJson;
         aP5_OptionIndexesJson=this.AV50OptionIndexesJson;
         return AV50OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getextrawwcontagemresultadoosfilterdata objgetextrawwcontagemresultadoosfilterdata;
         objgetextrawwcontagemresultadoosfilterdata = new getextrawwcontagemresultadoosfilterdata();
         objgetextrawwcontagemresultadoosfilterdata.AV41DDOName = aP0_DDOName;
         objgetextrawwcontagemresultadoosfilterdata.AV39SearchTxt = aP1_SearchTxt;
         objgetextrawwcontagemresultadoosfilterdata.AV40SearchTxtTo = aP2_SearchTxtTo;
         objgetextrawwcontagemresultadoosfilterdata.AV45OptionsJson = "" ;
         objgetextrawwcontagemresultadoosfilterdata.AV48OptionsDescJson = "" ;
         objgetextrawwcontagemresultadoosfilterdata.AV50OptionIndexesJson = "" ;
         objgetextrawwcontagemresultadoosfilterdata.context.SetSubmitInitialConfig(context);
         objgetextrawwcontagemresultadoosfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetextrawwcontagemresultadoosfilterdata);
         aP3_OptionsJson=this.AV45OptionsJson;
         aP4_OptionsDescJson=this.AV48OptionsDescJson;
         aP5_OptionIndexesJson=this.AV50OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getextrawwcontagemresultadoosfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV44Options = (IGxCollection)(new GxSimpleCollection());
         AV47OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV49OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV41DDOName), "DDO_CONTAGEMRESULTADO_OSFSOSFM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_OSFSOSFMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV41DDOName), "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRRESULTADO_SISTEMASIGLAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV41DDOName), "DDO_CONTAGEMRESULTADO_CONTRATADASIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_CONTRATADASIGLAOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV41DDOName), "DDO_CONTAGEMRESULTADO_SERVICOSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_SERVICOSIGLAOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV45OptionsJson = AV44Options.ToJSonString(false);
         AV48OptionsDescJson = AV47OptionsDesc.ToJSonString(false);
         AV50OptionIndexesJson = AV49OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV52Session.Get("ExtraWWContagemResultadoOSGridState"), "") == 0 )
         {
            AV54GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ExtraWWContagemResultadoOSGridState"), "");
         }
         else
         {
            AV54GridState.FromXml(AV52Session.Get("ExtraWWContagemResultadoOSGridState"), "");
         }
         AV111GXV1 = 1;
         while ( AV111GXV1 <= AV54GridState.gxTpr_Filtervalues.Count )
         {
            AV55GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV54GridState.gxTpr_Filtervalues.Item(AV111GXV1));
            if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "CONTRATADA_AREATRABALHOCOD") == 0 )
            {
               AV57Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( AV55GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "CONTRATADA_CODIGO") == 0 )
            {
               AV58Contratada_Codigo = (int)(NumberUtil.Val( AV55GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV10TFContagemResultado_OsFsOsFm = AV55GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_OSFSOSFM_SEL") == 0 )
            {
               AV11TFContagemResultado_OsFsOsFm_Sel = AV55GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV12TFContagemResultado_DataDmn = context.localUtil.CToD( AV55GridStateFilterValue.gxTpr_Value, 2);
               AV13TFContagemResultado_DataDmn_To = context.localUtil.CToD( AV55GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DATAULTCNT") == 0 )
            {
               AV14TFContagemResultado_DataUltCnt = context.localUtil.CToD( AV55GridStateFilterValue.gxTpr_Value, 2);
               AV15TFContagemResultado_DataUltCnt_To = context.localUtil.CToD( AV55GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRRESULTADO_SISTEMASIGLA") == 0 )
            {
               AV16TFContagemrResultado_SistemaSigla = AV55GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL") == 0 )
            {
               AV17TFContagemrResultado_SistemaSigla_Sel = AV55GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_CONTRATADASIGLA") == 0 )
            {
               AV18TFContagemResultado_ContratadaSigla = AV55GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_CONTRATADASIGLA_SEL") == 0 )
            {
               AV19TFContagemResultado_ContratadaSigla_Sel = AV55GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_SERVICOSIGLA") == 0 )
            {
               AV20TFContagemResultado_ServicoSigla = AV55GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_SERVICOSIGLA_SEL") == 0 )
            {
               AV21TFContagemResultado_ServicoSigla_Sel = AV55GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEFLATORCNT") == 0 )
            {
               AV22TFContagemResultado_DeflatorCnt = NumberUtil.Val( AV55GridStateFilterValue.gxTpr_Value, ".");
               AV23TFContagemResultado_DeflatorCnt_To = NumberUtil.Val( AV55GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_STATUSDMN_SEL") == 0 )
            {
               AV24TFContagemResultado_StatusDmn_SelsJson = AV55GridStateFilterValue.gxTpr_Value;
               AV25TFContagemResultado_StatusDmn_Sels.FromJSonString(AV24TFContagemResultado_StatusDmn_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_STATUSULTCNT_SEL") == 0 )
            {
               AV26TFContagemResultado_StatusUltCnt_SelsJson = AV55GridStateFilterValue.gxTpr_Value;
               AV27TFContagemResultado_StatusUltCnt_Sels.FromJSonString(AV26TFContagemResultado_StatusUltCnt_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_BASELINE_SEL") == 0 )
            {
               AV28TFContagemResultado_Baseline_Sel = (short)(NumberUtil.Val( AV55GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_PFBFSULTIMA") == 0 )
            {
               AV29TFContagemResultado_PFBFSUltima = NumberUtil.Val( AV55GridStateFilterValue.gxTpr_Value, ".");
               AV30TFContagemResultado_PFBFSUltima_To = NumberUtil.Val( AV55GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_PFLFSULTIMA") == 0 )
            {
               AV31TFContagemResultado_PFLFSUltima = NumberUtil.Val( AV55GridStateFilterValue.gxTpr_Value, ".");
               AV32TFContagemResultado_PFLFSUltima_To = NumberUtil.Val( AV55GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_PFBFMULTIMA") == 0 )
            {
               AV33TFContagemResultado_PFBFMUltima = NumberUtil.Val( AV55GridStateFilterValue.gxTpr_Value, ".");
               AV34TFContagemResultado_PFBFMUltima_To = NumberUtil.Val( AV55GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_PFLFMULTIMA") == 0 )
            {
               AV35TFContagemResultado_PFLFMUltima = NumberUtil.Val( AV55GridStateFilterValue.gxTpr_Value, ".");
               AV36TFContagemResultado_PFLFMUltima_To = NumberUtil.Val( AV55GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV55GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_PFFINAL") == 0 )
            {
               AV37TFContagemResultado_PFFinal = NumberUtil.Val( AV55GridStateFilterValue.gxTpr_Value, ".");
               AV38TFContagemResultado_PFFinal_To = NumberUtil.Val( AV55GridStateFilterValue.gxTpr_Valueto, ".");
            }
            AV111GXV1 = (int)(AV111GXV1+1);
         }
         if ( AV54GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV56GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV54GridState.gxTpr_Dynamicfilters.Item(1));
            AV59DynamicFiltersSelector1 = AV56GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV59DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV60DynamicFiltersOperator1 = AV56GridStateDynamicFilter.gxTpr_Operator;
               AV61ContagemResultado_OsFsOsFm1 = AV56GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV62ContagemResultado_DataDmn1 = context.localUtil.CToD( AV56GridStateDynamicFilter.gxTpr_Value, 2);
               AV63ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV56GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
            {
               AV64ContagemResultado_DataUltCnt1 = context.localUtil.CToD( AV56GridStateDynamicFilter.gxTpr_Value, 2);
               AV65ContagemResultado_DataUltCnt_To1 = context.localUtil.CToD( AV56GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
            {
               AV66ContagemResultado_ContadorFM1 = (int)(NumberUtil.Val( AV56GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV67ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV56GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV68ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( AV56GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
            {
               AV69ContagemResultado_NaoCnfDmnCod1 = (int)(NumberUtil.Val( AV56GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV70ContagemResultado_StatusDmn1 = AV56GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 )
            {
               AV71ContagemResultado_StatusDmnVnc1 = AV56GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
            {
               AV60DynamicFiltersOperator1 = AV56GridStateDynamicFilter.gxTpr_Operator;
               AV72ContagemResultado_EsforcoSoma1 = (int)(NumberUtil.Val( AV56GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector1, "CONTAGEMRESULTADO_BASELINE") == 0 )
            {
               AV73ContagemResultado_Baseline1 = AV56GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV74ContagemResultado_Servico1 = (int)(NumberUtil.Val( AV56GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV54GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV75DynamicFiltersEnabled2 = true;
               AV56GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV54GridState.gxTpr_Dynamicfilters.Item(2));
               AV76DynamicFiltersSelector2 = AV56GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV76DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
               {
                  AV77DynamicFiltersOperator2 = AV56GridStateDynamicFilter.gxTpr_Operator;
                  AV78ContagemResultado_OsFsOsFm2 = AV56GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV76DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV79ContagemResultado_DataDmn2 = context.localUtil.CToD( AV56GridStateDynamicFilter.gxTpr_Value, 2);
                  AV80ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV56GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV76DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
               {
                  AV81ContagemResultado_DataUltCnt2 = context.localUtil.CToD( AV56GridStateDynamicFilter.gxTpr_Value, 2);
                  AV82ContagemResultado_DataUltCnt_To2 = context.localUtil.CToD( AV56GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV76DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
               {
                  AV83ContagemResultado_ContadorFM2 = (int)(NumberUtil.Val( AV56GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV76DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV84ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV56GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV76DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
               {
                  AV85ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( AV56GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV76DynamicFiltersSelector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
               {
                  AV86ContagemResultado_NaoCnfDmnCod2 = (int)(NumberUtil.Val( AV56GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV76DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV87ContagemResultado_StatusDmn2 = AV56GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV76DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 )
               {
                  AV88ContagemResultado_StatusDmnVnc2 = AV56GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV76DynamicFiltersSelector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
               {
                  AV77DynamicFiltersOperator2 = AV56GridStateDynamicFilter.gxTpr_Operator;
                  AV89ContagemResultado_EsforcoSoma2 = (int)(NumberUtil.Val( AV56GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV76DynamicFiltersSelector2, "CONTAGEMRESULTADO_BASELINE") == 0 )
               {
                  AV90ContagemResultado_Baseline2 = AV56GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV76DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICO") == 0 )
               {
                  AV91ContagemResultado_Servico2 = (int)(NumberUtil.Val( AV56GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV54GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV92DynamicFiltersEnabled3 = true;
                  AV56GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV54GridState.gxTpr_Dynamicfilters.Item(3));
                  AV93DynamicFiltersSelector3 = AV56GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV93DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                  {
                     AV94DynamicFiltersOperator3 = AV56GridStateDynamicFilter.gxTpr_Operator;
                     AV95ContagemResultado_OsFsOsFm3 = AV56GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV96ContagemResultado_DataDmn3 = context.localUtil.CToD( AV56GridStateDynamicFilter.gxTpr_Value, 2);
                     AV97ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV56GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
                  {
                     AV98ContagemResultado_DataUltCnt3 = context.localUtil.CToD( AV56GridStateDynamicFilter.gxTpr_Value, 2);
                     AV99ContagemResultado_DataUltCnt_To3 = context.localUtil.CToD( AV56GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                  {
                     AV100ContagemResultado_ContadorFM3 = (int)(NumberUtil.Val( AV56GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV101ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV56GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                  {
                     AV102ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( AV56GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                  {
                     AV103ContagemResultado_NaoCnfDmnCod3 = (int)(NumberUtil.Val( AV56GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV104ContagemResultado_StatusDmn3 = AV56GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 )
                  {
                     AV105ContagemResultado_StatusDmnVnc3 = AV56GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 )
                  {
                     AV94DynamicFiltersOperator3 = AV56GridStateDynamicFilter.gxTpr_Operator;
                     AV106ContagemResultado_EsforcoSoma3 = (int)(NumberUtil.Val( AV56GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector3, "CONTAGEMRESULTADO_BASELINE") == 0 )
                  {
                     AV107ContagemResultado_Baseline3 = AV56GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICO") == 0 )
                  {
                     AV108ContagemResultado_Servico3 = (int)(NumberUtil.Val( AV56GridStateDynamicFilter.gxTpr_Value, "."));
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEMRESULTADO_OSFSOSFMOPTIONS' Routine */
         AV10TFContagemResultado_OsFsOsFm = AV39SearchTxt;
         AV11TFContagemResultado_OsFsOsFm_Sel = "";
         AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod = AV57Contratada_AreaTrabalhoCod;
         AV114ExtraWWContagemResultadoOSDS_2_Contratada_codigo = AV58Contratada_Codigo;
         AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = AV59DynamicFiltersSelector1;
         AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 = AV60DynamicFiltersOperator1;
         AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = AV61ContagemResultado_OsFsOsFm1;
         AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 = AV62ContagemResultado_DataDmn1;
         AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 = AV63ContagemResultado_DataDmn_To1;
         AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 = AV64ContagemResultado_DataUltCnt1;
         AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 = AV65ContagemResultado_DataUltCnt_To1;
         AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 = AV66ContagemResultado_ContadorFM1;
         AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 = AV67ContagemResultado_SistemaCod1;
         AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 = AV68ContagemResultado_ContratadaCod1;
         AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 = AV69ContagemResultado_NaoCnfDmnCod1;
         AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 = AV70ContagemResultado_StatusDmn1;
         AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 = AV71ContagemResultado_StatusDmnVnc1;
         AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 = AV72ContagemResultado_EsforcoSoma1;
         AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 = AV73ContagemResultado_Baseline1;
         AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 = AV74ContagemResultado_Servico1;
         AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = AV75DynamicFiltersEnabled2;
         AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = AV76DynamicFiltersSelector2;
         AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 = AV77DynamicFiltersOperator2;
         AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = AV78ContagemResultado_OsFsOsFm2;
         AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 = AV79ContagemResultado_DataDmn2;
         AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 = AV80ContagemResultado_DataDmn_To2;
         AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 = AV81ContagemResultado_DataUltCnt2;
         AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 = AV82ContagemResultado_DataUltCnt_To2;
         AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 = AV83ContagemResultado_ContadorFM2;
         AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 = AV84ContagemResultado_SistemaCod2;
         AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 = AV85ContagemResultado_ContratadaCod2;
         AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 = AV86ContagemResultado_NaoCnfDmnCod2;
         AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 = AV87ContagemResultado_StatusDmn2;
         AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 = AV88ContagemResultado_StatusDmnVnc2;
         AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 = AV89ContagemResultado_EsforcoSoma2;
         AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 = AV90ContagemResultado_Baseline2;
         AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 = AV91ContagemResultado_Servico2;
         AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = AV92DynamicFiltersEnabled3;
         AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = AV93DynamicFiltersSelector3;
         AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 = AV94DynamicFiltersOperator3;
         AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = AV95ContagemResultado_OsFsOsFm3;
         AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 = AV96ContagemResultado_DataDmn3;
         AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 = AV97ContagemResultado_DataDmn_To3;
         AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 = AV98ContagemResultado_DataUltCnt3;
         AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 = AV99ContagemResultado_DataUltCnt_To3;
         AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 = AV100ContagemResultado_ContadorFM3;
         AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 = AV101ContagemResultado_SistemaCod3;
         AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 = AV102ContagemResultado_ContratadaCod3;
         AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 = AV103ContagemResultado_NaoCnfDmnCod3;
         AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 = AV104ContagemResultado_StatusDmn3;
         AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 = AV105ContagemResultado_StatusDmnVnc3;
         AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 = AV106ContagemResultado_EsforcoSoma3;
         AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 = AV107ContagemResultado_Baseline3;
         AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 = AV108ContagemResultado_Servico3;
         AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm = AV10TFContagemResultado_OsFsOsFm;
         AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel = AV11TFContagemResultado_OsFsOsFm_Sel;
         AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn = AV12TFContagemResultado_DataDmn;
         AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to = AV13TFContagemResultado_DataDmn_To;
         AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt = AV14TFContagemResultado_DataUltCnt;
         AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to = AV15TFContagemResultado_DataUltCnt_To;
         AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla = AV16TFContagemrResultado_SistemaSigla;
         AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel = AV17TFContagemrResultado_SistemaSigla_Sel;
         AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla = AV18TFContagemResultado_ContratadaSigla;
         AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel = AV19TFContagemResultado_ContratadaSigla_Sel;
         AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla = AV20TFContagemResultado_ServicoSigla;
         AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel = AV21TFContagemResultado_ServicoSigla_Sel;
         AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt = AV22TFContagemResultado_DeflatorCnt;
         AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to = AV23TFContagemResultado_DeflatorCnt_To;
         AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels = AV25TFContagemResultado_StatusDmn_Sels;
         AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels = AV27TFContagemResultado_StatusUltCnt_Sels;
         AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel = AV28TFContagemResultado_Baseline_Sel;
         AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima = AV29TFContagemResultado_PFBFSUltima;
         AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to = AV30TFContagemResultado_PFBFSUltima_To;
         AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima = AV31TFContagemResultado_PFLFSUltima;
         AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to = AV32TFContagemResultado_PFLFSUltima_To;
         AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima = AV33TFContagemResultado_PFBFMUltima;
         AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to = AV34TFContagemResultado_PFBFMUltima_To;
         AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima = AV35TFContagemResultado_PFLFMUltima;
         AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to = AV36TFContagemResultado_PFLFMUltima_To;
         AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal = AV37TFContagemResultado_PFFinal;
         AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to = AV38TFContagemResultado_PFFinal_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A531ContagemResultado_StatusUltCnt ,
                                              AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels ,
                                              AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod ,
                                              AV192Contratada_dousuario ,
                                              AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 ,
                                              AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 ,
                                              AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 ,
                                              AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 ,
                                              AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 ,
                                              AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 ,
                                              AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 ,
                                              AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 ,
                                              AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 ,
                                              AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 ,
                                              AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 ,
                                              AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 ,
                                              AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 ,
                                              AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 ,
                                              AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 ,
                                              AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 ,
                                              AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 ,
                                              AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 ,
                                              AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 ,
                                              AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 ,
                                              AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 ,
                                              AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 ,
                                              AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 ,
                                              AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 ,
                                              AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 ,
                                              AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 ,
                                              AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 ,
                                              AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 ,
                                              AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 ,
                                              AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 ,
                                              AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 ,
                                              AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 ,
                                              AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 ,
                                              AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 ,
                                              AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 ,
                                              AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 ,
                                              AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 ,
                                              AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 ,
                                              AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 ,
                                              AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 ,
                                              AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 ,
                                              AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel ,
                                              AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm ,
                                              AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn ,
                                              AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to ,
                                              AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel ,
                                              AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla ,
                                              AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel ,
                                              AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla ,
                                              AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel ,
                                              AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla ,
                                              AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel ,
                                              AV54GridState.gxTpr_Dynamicfilters.Count ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A489ContagemResultado_SistemaCod ,
                                              A468ContagemResultado_NaoCnfDmnCod ,
                                              A771ContagemResultado_StatusDmnVnc ,
                                              A510ContagemResultado_EsforcoSoma ,
                                              A598ContagemResultado_Baseline ,
                                              A601ContagemResultado_Servico ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A803ContagemResultado_ContratadaSigla ,
                                              A801ContagemResultado_ServicoSigla ,
                                              A456ContagemResultado_Codigo ,
                                              AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 ,
                                              AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 ,
                                              A584ContagemResultado_ContadorFM ,
                                              A508ContagemResultado_Owner ,
                                              AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 ,
                                              AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 ,
                                              AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 ,
                                              AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 ,
                                              AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 ,
                                              AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 ,
                                              AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt ,
                                              AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to ,
                                              AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt ,
                                              A802ContagemResultado_DeflatorCnt ,
                                              AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to ,
                                              AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels.Count ,
                                              AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima ,
                                              A684ContagemResultado_PFBFSUltima ,
                                              AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to ,
                                              AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima ,
                                              A685ContagemResultado_PFLFSUltima ,
                                              AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to ,
                                              AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima ,
                                              A682ContagemResultado_PFBFMUltima ,
                                              AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to ,
                                              AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima ,
                                              A683ContagemResultado_PFLFMUltima ,
                                              AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to ,
                                              AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ,
                                              A574ContagemResultado_PFFinal ,
                                              AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.SHORT
                                              }
         });
         lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm = StringUtil.Concat( StringUtil.RTrim( AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm), "%", "");
         lV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla), 25, "%");
         lV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla = StringUtil.PadR( StringUtil.RTrim( AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla), 15, "%");
         lV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P00S14 */
         pr_default.execute(0, new Object[] {AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1, AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1, AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1, AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1, AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1, AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1, AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1, AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2, AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2, AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2, AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2, AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2, AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2, AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2, AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2, AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2, AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2, AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3, AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3, AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3, AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3, AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3, AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3, AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3, AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3, AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3, AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3, AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt, AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt, AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to, AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to, AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt, AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt, AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to, AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to, AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels.Count, AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima, AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima, AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to, AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to, AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima, AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima, AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to, AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to, AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima, AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima, AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to, AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to, AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima, AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima, AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to, AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to, AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod, AV192Contratada_dousuario, AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1, AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1, AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1, AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1, AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1, AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1, AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1, AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1, AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1, AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1, AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1, AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2, AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2, AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2, AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2, AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2, AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2, AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2, AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2, AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2, AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2, AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2, AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3, AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3, AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3, AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3, AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3, AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3, AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3, AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3, AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3, AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3, AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3, lV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm, AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel, AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn, AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to, lV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla, AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel, lV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla, AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel,
         lV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla, AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00S14_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00S14_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = P00S14_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00S14_n602ContagemResultado_OSVinculada[0];
            A1583ContagemResultado_TipoRegistro = P00S14_A1583ContagemResultado_TipoRegistro[0];
            A801ContagemResultado_ServicoSigla = P00S14_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00S14_n801ContagemResultado_ServicoSigla[0];
            A803ContagemResultado_ContratadaSigla = P00S14_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00S14_n803ContagemResultado_ContratadaSigla[0];
            A509ContagemrResultado_SistemaSigla = P00S14_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00S14_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00S14_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00S14_n601ContagemResultado_Servico[0];
            A598ContagemResultado_Baseline = P00S14_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P00S14_n598ContagemResultado_Baseline[0];
            A771ContagemResultado_StatusDmnVnc = P00S14_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = P00S14_n771ContagemResultado_StatusDmnVnc[0];
            A484ContagemResultado_StatusDmn = P00S14_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00S14_n484ContagemResultado_StatusDmn[0];
            A468ContagemResultado_NaoCnfDmnCod = P00S14_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P00S14_n468ContagemResultado_NaoCnfDmnCod[0];
            A489ContagemResultado_SistemaCod = P00S14_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00S14_n489ContagemResultado_SistemaCod[0];
            A508ContagemResultado_Owner = P00S14_A508ContagemResultado_Owner[0];
            A471ContagemResultado_DataDmn = P00S14_A471ContagemResultado_DataDmn[0];
            A490ContagemResultado_ContratadaCod = P00S14_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00S14_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00S14_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00S14_n52Contratada_AreaTrabalhoCod[0];
            A683ContagemResultado_PFLFMUltima = P00S14_A683ContagemResultado_PFLFMUltima[0];
            A682ContagemResultado_PFBFMUltima = P00S14_A682ContagemResultado_PFBFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P00S14_A685ContagemResultado_PFLFSUltima[0];
            A684ContagemResultado_PFBFSUltima = P00S14_A684ContagemResultado_PFBFSUltima[0];
            A531ContagemResultado_StatusUltCnt = P00S14_A531ContagemResultado_StatusUltCnt[0];
            A802ContagemResultado_DeflatorCnt = P00S14_A802ContagemResultado_DeflatorCnt[0];
            A510ContagemResultado_EsforcoSoma = P00S14_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P00S14_n510ContagemResultado_EsforcoSoma[0];
            A584ContagemResultado_ContadorFM = P00S14_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P00S14_A566ContagemResultado_DataUltCnt[0];
            A493ContagemResultado_DemandaFM = P00S14_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00S14_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00S14_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00S14_n457ContagemResultado_Demanda[0];
            A456ContagemResultado_Codigo = P00S14_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = P00S14_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00S14_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00S14_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00S14_n801ContagemResultado_ServicoSigla[0];
            A771ContagemResultado_StatusDmnVnc = P00S14_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = P00S14_n771ContagemResultado_StatusDmnVnc[0];
            A509ContagemrResultado_SistemaSigla = P00S14_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00S14_n509ContagemrResultado_SistemaSigla[0];
            A803ContagemResultado_ContratadaSigla = P00S14_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00S14_n803ContagemResultado_ContratadaSigla[0];
            A52Contratada_AreaTrabalhoCod = P00S14_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00S14_n52Contratada_AreaTrabalhoCod[0];
            A683ContagemResultado_PFLFMUltima = P00S14_A683ContagemResultado_PFLFMUltima[0];
            A682ContagemResultado_PFBFMUltima = P00S14_A682ContagemResultado_PFBFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P00S14_A685ContagemResultado_PFLFSUltima[0];
            A684ContagemResultado_PFBFSUltima = P00S14_A684ContagemResultado_PFBFSUltima[0];
            A531ContagemResultado_StatusUltCnt = P00S14_A531ContagemResultado_StatusUltCnt[0];
            A802ContagemResultado_DeflatorCnt = P00S14_A802ContagemResultado_DeflatorCnt[0];
            A584ContagemResultado_ContadorFM = P00S14_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P00S14_A566ContagemResultado_DataUltCnt[0];
            A510ContagemResultado_EsforcoSoma = P00S14_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P00S14_n510ContagemResultado_EsforcoSoma[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            if ( (Convert.ToDecimal(0)==AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal) || ( ( A574ContagemResultado_PFFinal >= AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ) ) )
            {
               if ( (Convert.ToDecimal(0)==AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to) || ( ( A574ContagemResultado_PFFinal <= AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ) ) )
               {
                  A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A501ContagemResultado_OsFsOsFm)) )
                  {
                     AV43Option = A501ContagemResultado_OsFsOsFm;
                     AV42InsertIndex = 1;
                     while ( ( AV42InsertIndex <= AV44Options.Count ) && ( StringUtil.StrCmp(((String)AV44Options.Item(AV42InsertIndex)), AV43Option) < 0 ) )
                     {
                        AV42InsertIndex = (int)(AV42InsertIndex+1);
                     }
                     if ( ( AV42InsertIndex <= AV44Options.Count ) && ( StringUtil.StrCmp(((String)AV44Options.Item(AV42InsertIndex)), AV43Option) == 0 ) )
                     {
                        AV51count = (long)(NumberUtil.Val( ((String)AV49OptionIndexes.Item(AV42InsertIndex)), "."));
                        AV51count = (long)(AV51count+1);
                        AV49OptionIndexes.RemoveItem(AV42InsertIndex);
                        AV49OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV51count), "Z,ZZZ,ZZZ,ZZ9")), AV42InsertIndex);
                     }
                     else
                     {
                        AV44Options.Add(AV43Option, AV42InsertIndex);
                        AV49OptionIndexes.Add("1", AV42InsertIndex);
                     }
                  }
                  if ( AV44Options.Count == 50 )
                  {
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTAGEMRRESULTADO_SISTEMASIGLAOPTIONS' Routine */
         AV16TFContagemrResultado_SistemaSigla = AV39SearchTxt;
         AV17TFContagemrResultado_SistemaSigla_Sel = "";
         AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod = AV57Contratada_AreaTrabalhoCod;
         AV114ExtraWWContagemResultadoOSDS_2_Contratada_codigo = AV58Contratada_Codigo;
         AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = AV59DynamicFiltersSelector1;
         AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 = AV60DynamicFiltersOperator1;
         AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = AV61ContagemResultado_OsFsOsFm1;
         AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 = AV62ContagemResultado_DataDmn1;
         AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 = AV63ContagemResultado_DataDmn_To1;
         AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 = AV64ContagemResultado_DataUltCnt1;
         AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 = AV65ContagemResultado_DataUltCnt_To1;
         AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 = AV66ContagemResultado_ContadorFM1;
         AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 = AV67ContagemResultado_SistemaCod1;
         AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 = AV68ContagemResultado_ContratadaCod1;
         AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 = AV69ContagemResultado_NaoCnfDmnCod1;
         AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 = AV70ContagemResultado_StatusDmn1;
         AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 = AV71ContagemResultado_StatusDmnVnc1;
         AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 = AV72ContagemResultado_EsforcoSoma1;
         AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 = AV73ContagemResultado_Baseline1;
         AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 = AV74ContagemResultado_Servico1;
         AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = AV75DynamicFiltersEnabled2;
         AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = AV76DynamicFiltersSelector2;
         AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 = AV77DynamicFiltersOperator2;
         AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = AV78ContagemResultado_OsFsOsFm2;
         AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 = AV79ContagemResultado_DataDmn2;
         AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 = AV80ContagemResultado_DataDmn_To2;
         AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 = AV81ContagemResultado_DataUltCnt2;
         AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 = AV82ContagemResultado_DataUltCnt_To2;
         AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 = AV83ContagemResultado_ContadorFM2;
         AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 = AV84ContagemResultado_SistemaCod2;
         AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 = AV85ContagemResultado_ContratadaCod2;
         AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 = AV86ContagemResultado_NaoCnfDmnCod2;
         AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 = AV87ContagemResultado_StatusDmn2;
         AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 = AV88ContagemResultado_StatusDmnVnc2;
         AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 = AV89ContagemResultado_EsforcoSoma2;
         AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 = AV90ContagemResultado_Baseline2;
         AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 = AV91ContagemResultado_Servico2;
         AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = AV92DynamicFiltersEnabled3;
         AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = AV93DynamicFiltersSelector3;
         AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 = AV94DynamicFiltersOperator3;
         AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = AV95ContagemResultado_OsFsOsFm3;
         AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 = AV96ContagemResultado_DataDmn3;
         AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 = AV97ContagemResultado_DataDmn_To3;
         AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 = AV98ContagemResultado_DataUltCnt3;
         AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 = AV99ContagemResultado_DataUltCnt_To3;
         AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 = AV100ContagemResultado_ContadorFM3;
         AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 = AV101ContagemResultado_SistemaCod3;
         AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 = AV102ContagemResultado_ContratadaCod3;
         AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 = AV103ContagemResultado_NaoCnfDmnCod3;
         AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 = AV104ContagemResultado_StatusDmn3;
         AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 = AV105ContagemResultado_StatusDmnVnc3;
         AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 = AV106ContagemResultado_EsforcoSoma3;
         AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 = AV107ContagemResultado_Baseline3;
         AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 = AV108ContagemResultado_Servico3;
         AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm = AV10TFContagemResultado_OsFsOsFm;
         AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel = AV11TFContagemResultado_OsFsOsFm_Sel;
         AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn = AV12TFContagemResultado_DataDmn;
         AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to = AV13TFContagemResultado_DataDmn_To;
         AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt = AV14TFContagemResultado_DataUltCnt;
         AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to = AV15TFContagemResultado_DataUltCnt_To;
         AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla = AV16TFContagemrResultado_SistemaSigla;
         AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel = AV17TFContagemrResultado_SistemaSigla_Sel;
         AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla = AV18TFContagemResultado_ContratadaSigla;
         AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel = AV19TFContagemResultado_ContratadaSigla_Sel;
         AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla = AV20TFContagemResultado_ServicoSigla;
         AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel = AV21TFContagemResultado_ServicoSigla_Sel;
         AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt = AV22TFContagemResultado_DeflatorCnt;
         AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to = AV23TFContagemResultado_DeflatorCnt_To;
         AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels = AV25TFContagemResultado_StatusDmn_Sels;
         AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels = AV27TFContagemResultado_StatusUltCnt_Sels;
         AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel = AV28TFContagemResultado_Baseline_Sel;
         AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima = AV29TFContagemResultado_PFBFSUltima;
         AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to = AV30TFContagemResultado_PFBFSUltima_To;
         AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima = AV31TFContagemResultado_PFLFSUltima;
         AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to = AV32TFContagemResultado_PFLFSUltima_To;
         AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima = AV33TFContagemResultado_PFBFMUltima;
         AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to = AV34TFContagemResultado_PFBFMUltima_To;
         AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima = AV35TFContagemResultado_PFLFMUltima;
         AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to = AV36TFContagemResultado_PFLFMUltima_To;
         AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal = AV37TFContagemResultado_PFFinal;
         AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to = AV38TFContagemResultado_PFFinal_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A531ContagemResultado_StatusUltCnt ,
                                              AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels ,
                                              AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod ,
                                              AV192Contratada_dousuario ,
                                              AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 ,
                                              AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 ,
                                              AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 ,
                                              AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 ,
                                              AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 ,
                                              AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 ,
                                              AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 ,
                                              AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 ,
                                              AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 ,
                                              AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 ,
                                              AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 ,
                                              AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 ,
                                              AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 ,
                                              AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 ,
                                              AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 ,
                                              AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 ,
                                              AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 ,
                                              AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 ,
                                              AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 ,
                                              AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 ,
                                              AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 ,
                                              AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 ,
                                              AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 ,
                                              AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 ,
                                              AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 ,
                                              AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 ,
                                              AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 ,
                                              AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 ,
                                              AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 ,
                                              AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 ,
                                              AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 ,
                                              AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 ,
                                              AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 ,
                                              AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 ,
                                              AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 ,
                                              AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 ,
                                              AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 ,
                                              AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 ,
                                              AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 ,
                                              AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 ,
                                              AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 ,
                                              AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel ,
                                              AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm ,
                                              AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn ,
                                              AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to ,
                                              AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel ,
                                              AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla ,
                                              AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel ,
                                              AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla ,
                                              AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel ,
                                              AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla ,
                                              AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel ,
                                              AV54GridState.gxTpr_Dynamicfilters.Count ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A489ContagemResultado_SistemaCod ,
                                              A468ContagemResultado_NaoCnfDmnCod ,
                                              A771ContagemResultado_StatusDmnVnc ,
                                              A510ContagemResultado_EsforcoSoma ,
                                              A598ContagemResultado_Baseline ,
                                              A601ContagemResultado_Servico ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A803ContagemResultado_ContratadaSigla ,
                                              A801ContagemResultado_ServicoSigla ,
                                              A456ContagemResultado_Codigo ,
                                              AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 ,
                                              AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 ,
                                              A584ContagemResultado_ContadorFM ,
                                              A508ContagemResultado_Owner ,
                                              AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 ,
                                              AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 ,
                                              AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 ,
                                              AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 ,
                                              AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 ,
                                              AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 ,
                                              AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt ,
                                              AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to ,
                                              AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt ,
                                              A802ContagemResultado_DeflatorCnt ,
                                              AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to ,
                                              AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels.Count ,
                                              AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima ,
                                              A684ContagemResultado_PFBFSUltima ,
                                              AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to ,
                                              AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima ,
                                              A685ContagemResultado_PFLFSUltima ,
                                              AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to ,
                                              AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima ,
                                              A682ContagemResultado_PFBFMUltima ,
                                              AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to ,
                                              AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima ,
                                              A683ContagemResultado_PFLFMUltima ,
                                              AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to ,
                                              AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ,
                                              A574ContagemResultado_PFFinal ,
                                              AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.SHORT
                                              }
         });
         lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm = StringUtil.Concat( StringUtil.RTrim( AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm), "%", "");
         lV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla), 25, "%");
         lV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla = StringUtil.PadR( StringUtil.RTrim( AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla), 15, "%");
         lV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P00S17 */
         pr_default.execute(1, new Object[] {AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1, AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1, AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1, AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1, AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1, AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1, AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1, AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2, AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2, AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2, AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2, AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2, AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2, AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2, AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2, AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2, AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2, AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3, AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3, AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3, AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3, AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3, AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3, AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3, AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3, AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3, AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3, AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt, AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt, AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to, AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to, AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt, AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt, AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to, AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to, AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels.Count, AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima, AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima, AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to, AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to, AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima, AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima, AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to, AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to, AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima, AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima, AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to, AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to, AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima, AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima, AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to, AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to, AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod, AV192Contratada_dousuario, AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1, AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1, AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1, AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1, AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1, AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1, AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1, AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1, AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1, AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1, AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1, AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2, AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2, AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2, AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2, AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2, AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2, AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2, AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2, AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2, AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2, AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2, AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3, AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3, AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3, AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3, AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3, AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3, AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3, AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3, AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3, AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3, AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3, lV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm, AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel, AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn, AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to, lV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla, AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel, lV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla, AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel,
         lV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla, AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKS13 = false;
            A1553ContagemResultado_CntSrvCod = P00S17_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00S17_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = P00S17_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00S17_n602ContagemResultado_OSVinculada[0];
            A1583ContagemResultado_TipoRegistro = P00S17_A1583ContagemResultado_TipoRegistro[0];
            A509ContagemrResultado_SistemaSigla = P00S17_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00S17_n509ContagemrResultado_SistemaSigla[0];
            A801ContagemResultado_ServicoSigla = P00S17_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00S17_n801ContagemResultado_ServicoSigla[0];
            A803ContagemResultado_ContratadaSigla = P00S17_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00S17_n803ContagemResultado_ContratadaSigla[0];
            A601ContagemResultado_Servico = P00S17_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00S17_n601ContagemResultado_Servico[0];
            A598ContagemResultado_Baseline = P00S17_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P00S17_n598ContagemResultado_Baseline[0];
            A771ContagemResultado_StatusDmnVnc = P00S17_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = P00S17_n771ContagemResultado_StatusDmnVnc[0];
            A484ContagemResultado_StatusDmn = P00S17_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00S17_n484ContagemResultado_StatusDmn[0];
            A468ContagemResultado_NaoCnfDmnCod = P00S17_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P00S17_n468ContagemResultado_NaoCnfDmnCod[0];
            A489ContagemResultado_SistemaCod = P00S17_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00S17_n489ContagemResultado_SistemaCod[0];
            A508ContagemResultado_Owner = P00S17_A508ContagemResultado_Owner[0];
            A471ContagemResultado_DataDmn = P00S17_A471ContagemResultado_DataDmn[0];
            A490ContagemResultado_ContratadaCod = P00S17_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00S17_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00S17_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00S17_n52Contratada_AreaTrabalhoCod[0];
            A683ContagemResultado_PFLFMUltima = P00S17_A683ContagemResultado_PFLFMUltima[0];
            A682ContagemResultado_PFBFMUltima = P00S17_A682ContagemResultado_PFBFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P00S17_A685ContagemResultado_PFLFSUltima[0];
            A684ContagemResultado_PFBFSUltima = P00S17_A684ContagemResultado_PFBFSUltima[0];
            A531ContagemResultado_StatusUltCnt = P00S17_A531ContagemResultado_StatusUltCnt[0];
            A802ContagemResultado_DeflatorCnt = P00S17_A802ContagemResultado_DeflatorCnt[0];
            A510ContagemResultado_EsforcoSoma = P00S17_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P00S17_n510ContagemResultado_EsforcoSoma[0];
            A584ContagemResultado_ContadorFM = P00S17_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P00S17_A566ContagemResultado_DataUltCnt[0];
            A493ContagemResultado_DemandaFM = P00S17_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00S17_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00S17_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00S17_n457ContagemResultado_Demanda[0];
            A456ContagemResultado_Codigo = P00S17_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = P00S17_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00S17_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00S17_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00S17_n801ContagemResultado_ServicoSigla[0];
            A771ContagemResultado_StatusDmnVnc = P00S17_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = P00S17_n771ContagemResultado_StatusDmnVnc[0];
            A509ContagemrResultado_SistemaSigla = P00S17_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00S17_n509ContagemrResultado_SistemaSigla[0];
            A803ContagemResultado_ContratadaSigla = P00S17_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00S17_n803ContagemResultado_ContratadaSigla[0];
            A52Contratada_AreaTrabalhoCod = P00S17_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00S17_n52Contratada_AreaTrabalhoCod[0];
            A683ContagemResultado_PFLFMUltima = P00S17_A683ContagemResultado_PFLFMUltima[0];
            A682ContagemResultado_PFBFMUltima = P00S17_A682ContagemResultado_PFBFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P00S17_A685ContagemResultado_PFLFSUltima[0];
            A684ContagemResultado_PFBFSUltima = P00S17_A684ContagemResultado_PFBFSUltima[0];
            A531ContagemResultado_StatusUltCnt = P00S17_A531ContagemResultado_StatusUltCnt[0];
            A802ContagemResultado_DeflatorCnt = P00S17_A802ContagemResultado_DeflatorCnt[0];
            A584ContagemResultado_ContadorFM = P00S17_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P00S17_A566ContagemResultado_DataUltCnt[0];
            A510ContagemResultado_EsforcoSoma = P00S17_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P00S17_n510ContagemResultado_EsforcoSoma[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            if ( (Convert.ToDecimal(0)==AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal) || ( ( A574ContagemResultado_PFFinal >= AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ) ) )
            {
               if ( (Convert.ToDecimal(0)==AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to) || ( ( A574ContagemResultado_PFFinal <= AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ) ) )
               {
                  A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
                  AV51count = 0;
                  while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00S17_A509ContagemrResultado_SistemaSigla[0], A509ContagemrResultado_SistemaSigla) == 0 ) )
                  {
                     BRKS13 = false;
                     A489ContagemResultado_SistemaCod = P00S17_A489ContagemResultado_SistemaCod[0];
                     n489ContagemResultado_SistemaCod = P00S17_n489ContagemResultado_SistemaCod[0];
                     A456ContagemResultado_Codigo = P00S17_A456ContagemResultado_Codigo[0];
                     AV51count = (long)(AV51count+1);
                     BRKS13 = true;
                     pr_default.readNext(1);
                  }
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A509ContagemrResultado_SistemaSigla)) )
                  {
                     AV43Option = A509ContagemrResultado_SistemaSigla;
                     AV44Options.Add(AV43Option, 0);
                     AV49OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV51count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                  }
                  if ( AV44Options.Count == 50 )
                  {
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
               }
            }
            if ( ! BRKS13 )
            {
               BRKS13 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTAGEMRESULTADO_CONTRATADASIGLAOPTIONS' Routine */
         AV18TFContagemResultado_ContratadaSigla = AV39SearchTxt;
         AV19TFContagemResultado_ContratadaSigla_Sel = "";
         AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod = AV57Contratada_AreaTrabalhoCod;
         AV114ExtraWWContagemResultadoOSDS_2_Contratada_codigo = AV58Contratada_Codigo;
         AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = AV59DynamicFiltersSelector1;
         AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 = AV60DynamicFiltersOperator1;
         AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = AV61ContagemResultado_OsFsOsFm1;
         AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 = AV62ContagemResultado_DataDmn1;
         AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 = AV63ContagemResultado_DataDmn_To1;
         AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 = AV64ContagemResultado_DataUltCnt1;
         AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 = AV65ContagemResultado_DataUltCnt_To1;
         AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 = AV66ContagemResultado_ContadorFM1;
         AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 = AV67ContagemResultado_SistemaCod1;
         AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 = AV68ContagemResultado_ContratadaCod1;
         AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 = AV69ContagemResultado_NaoCnfDmnCod1;
         AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 = AV70ContagemResultado_StatusDmn1;
         AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 = AV71ContagemResultado_StatusDmnVnc1;
         AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 = AV72ContagemResultado_EsforcoSoma1;
         AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 = AV73ContagemResultado_Baseline1;
         AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 = AV74ContagemResultado_Servico1;
         AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = AV75DynamicFiltersEnabled2;
         AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = AV76DynamicFiltersSelector2;
         AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 = AV77DynamicFiltersOperator2;
         AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = AV78ContagemResultado_OsFsOsFm2;
         AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 = AV79ContagemResultado_DataDmn2;
         AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 = AV80ContagemResultado_DataDmn_To2;
         AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 = AV81ContagemResultado_DataUltCnt2;
         AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 = AV82ContagemResultado_DataUltCnt_To2;
         AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 = AV83ContagemResultado_ContadorFM2;
         AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 = AV84ContagemResultado_SistemaCod2;
         AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 = AV85ContagemResultado_ContratadaCod2;
         AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 = AV86ContagemResultado_NaoCnfDmnCod2;
         AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 = AV87ContagemResultado_StatusDmn2;
         AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 = AV88ContagemResultado_StatusDmnVnc2;
         AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 = AV89ContagemResultado_EsforcoSoma2;
         AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 = AV90ContagemResultado_Baseline2;
         AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 = AV91ContagemResultado_Servico2;
         AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = AV92DynamicFiltersEnabled3;
         AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = AV93DynamicFiltersSelector3;
         AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 = AV94DynamicFiltersOperator3;
         AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = AV95ContagemResultado_OsFsOsFm3;
         AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 = AV96ContagemResultado_DataDmn3;
         AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 = AV97ContagemResultado_DataDmn_To3;
         AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 = AV98ContagemResultado_DataUltCnt3;
         AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 = AV99ContagemResultado_DataUltCnt_To3;
         AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 = AV100ContagemResultado_ContadorFM3;
         AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 = AV101ContagemResultado_SistemaCod3;
         AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 = AV102ContagemResultado_ContratadaCod3;
         AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 = AV103ContagemResultado_NaoCnfDmnCod3;
         AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 = AV104ContagemResultado_StatusDmn3;
         AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 = AV105ContagemResultado_StatusDmnVnc3;
         AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 = AV106ContagemResultado_EsforcoSoma3;
         AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 = AV107ContagemResultado_Baseline3;
         AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 = AV108ContagemResultado_Servico3;
         AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm = AV10TFContagemResultado_OsFsOsFm;
         AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel = AV11TFContagemResultado_OsFsOsFm_Sel;
         AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn = AV12TFContagemResultado_DataDmn;
         AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to = AV13TFContagemResultado_DataDmn_To;
         AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt = AV14TFContagemResultado_DataUltCnt;
         AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to = AV15TFContagemResultado_DataUltCnt_To;
         AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla = AV16TFContagemrResultado_SistemaSigla;
         AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel = AV17TFContagemrResultado_SistemaSigla_Sel;
         AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla = AV18TFContagemResultado_ContratadaSigla;
         AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel = AV19TFContagemResultado_ContratadaSigla_Sel;
         AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla = AV20TFContagemResultado_ServicoSigla;
         AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel = AV21TFContagemResultado_ServicoSigla_Sel;
         AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt = AV22TFContagemResultado_DeflatorCnt;
         AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to = AV23TFContagemResultado_DeflatorCnt_To;
         AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels = AV25TFContagemResultado_StatusDmn_Sels;
         AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels = AV27TFContagemResultado_StatusUltCnt_Sels;
         AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel = AV28TFContagemResultado_Baseline_Sel;
         AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima = AV29TFContagemResultado_PFBFSUltima;
         AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to = AV30TFContagemResultado_PFBFSUltima_To;
         AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima = AV31TFContagemResultado_PFLFSUltima;
         AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to = AV32TFContagemResultado_PFLFSUltima_To;
         AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima = AV33TFContagemResultado_PFBFMUltima;
         AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to = AV34TFContagemResultado_PFBFMUltima_To;
         AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima = AV35TFContagemResultado_PFLFMUltima;
         AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to = AV36TFContagemResultado_PFLFMUltima_To;
         AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal = AV37TFContagemResultado_PFFinal;
         AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to = AV38TFContagemResultado_PFFinal_To;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A531ContagemResultado_StatusUltCnt ,
                                              AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels ,
                                              AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod ,
                                              AV192Contratada_dousuario ,
                                              AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 ,
                                              AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 ,
                                              AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 ,
                                              AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 ,
                                              AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 ,
                                              AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 ,
                                              AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 ,
                                              AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 ,
                                              AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 ,
                                              AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 ,
                                              AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 ,
                                              AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 ,
                                              AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 ,
                                              AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 ,
                                              AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 ,
                                              AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 ,
                                              AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 ,
                                              AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 ,
                                              AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 ,
                                              AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 ,
                                              AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 ,
                                              AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 ,
                                              AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 ,
                                              AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 ,
                                              AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 ,
                                              AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 ,
                                              AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 ,
                                              AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 ,
                                              AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 ,
                                              AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 ,
                                              AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 ,
                                              AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 ,
                                              AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 ,
                                              AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 ,
                                              AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 ,
                                              AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 ,
                                              AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 ,
                                              AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 ,
                                              AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 ,
                                              AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 ,
                                              AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 ,
                                              AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel ,
                                              AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm ,
                                              AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn ,
                                              AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to ,
                                              AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel ,
                                              AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla ,
                                              AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel ,
                                              AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla ,
                                              AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel ,
                                              AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla ,
                                              AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel ,
                                              AV54GridState.gxTpr_Dynamicfilters.Count ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A489ContagemResultado_SistemaCod ,
                                              A468ContagemResultado_NaoCnfDmnCod ,
                                              A771ContagemResultado_StatusDmnVnc ,
                                              A510ContagemResultado_EsforcoSoma ,
                                              A598ContagemResultado_Baseline ,
                                              A601ContagemResultado_Servico ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A803ContagemResultado_ContratadaSigla ,
                                              A801ContagemResultado_ServicoSigla ,
                                              A456ContagemResultado_Codigo ,
                                              AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 ,
                                              AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 ,
                                              A584ContagemResultado_ContadorFM ,
                                              A508ContagemResultado_Owner ,
                                              AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 ,
                                              AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 ,
                                              AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 ,
                                              AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 ,
                                              AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 ,
                                              AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 ,
                                              AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt ,
                                              AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to ,
                                              AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt ,
                                              A802ContagemResultado_DeflatorCnt ,
                                              AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to ,
                                              AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels.Count ,
                                              AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima ,
                                              A684ContagemResultado_PFBFSUltima ,
                                              AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to ,
                                              AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima ,
                                              A685ContagemResultado_PFLFSUltima ,
                                              AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to ,
                                              AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima ,
                                              A682ContagemResultado_PFBFMUltima ,
                                              AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to ,
                                              AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima ,
                                              A683ContagemResultado_PFLFMUltima ,
                                              AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to ,
                                              AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ,
                                              A574ContagemResultado_PFFinal ,
                                              AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.SHORT
                                              }
         });
         lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm = StringUtil.Concat( StringUtil.RTrim( AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm), "%", "");
         lV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla), 25, "%");
         lV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla = StringUtil.PadR( StringUtil.RTrim( AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla), 15, "%");
         lV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P00S110 */
         pr_default.execute(2, new Object[] {AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1, AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1, AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1, AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1, AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1, AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1, AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1, AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2, AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2, AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2, AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2, AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2, AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2, AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2, AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2, AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2, AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2, AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3, AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3, AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3, AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3, AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3, AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3, AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3, AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3, AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3, AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3, AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt, AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt, AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to, AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to, AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt, AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt, AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to, AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to, AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels.Count, AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima, AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima, AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to, AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to, AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima, AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima, AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to, AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to, AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima, AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima, AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to, AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to, AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima, AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima, AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to, AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to, AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod, AV192Contratada_dousuario, AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1, AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1, AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1, AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1, AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1, AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1, AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1, AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1, AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1, AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1, AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1, AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2, AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2, AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2, AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2, AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2, AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2, AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2, AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2, AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2, AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2, AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2, AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3, AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3, AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3, AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3, AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3, AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3, AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3, AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3, AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3, AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3, AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3, lV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm, AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel, AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn, AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to, lV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla, AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel, lV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla, AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel,
         lV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla, AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKS15 = false;
            A1553ContagemResultado_CntSrvCod = P00S110_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00S110_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = P00S110_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00S110_n602ContagemResultado_OSVinculada[0];
            A1583ContagemResultado_TipoRegistro = P00S110_A1583ContagemResultado_TipoRegistro[0];
            A803ContagemResultado_ContratadaSigla = P00S110_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00S110_n803ContagemResultado_ContratadaSigla[0];
            A801ContagemResultado_ServicoSigla = P00S110_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00S110_n801ContagemResultado_ServicoSigla[0];
            A509ContagemrResultado_SistemaSigla = P00S110_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00S110_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00S110_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00S110_n601ContagemResultado_Servico[0];
            A598ContagemResultado_Baseline = P00S110_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P00S110_n598ContagemResultado_Baseline[0];
            A771ContagemResultado_StatusDmnVnc = P00S110_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = P00S110_n771ContagemResultado_StatusDmnVnc[0];
            A484ContagemResultado_StatusDmn = P00S110_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00S110_n484ContagemResultado_StatusDmn[0];
            A468ContagemResultado_NaoCnfDmnCod = P00S110_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P00S110_n468ContagemResultado_NaoCnfDmnCod[0];
            A489ContagemResultado_SistemaCod = P00S110_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00S110_n489ContagemResultado_SistemaCod[0];
            A508ContagemResultado_Owner = P00S110_A508ContagemResultado_Owner[0];
            A471ContagemResultado_DataDmn = P00S110_A471ContagemResultado_DataDmn[0];
            A490ContagemResultado_ContratadaCod = P00S110_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00S110_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00S110_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00S110_n52Contratada_AreaTrabalhoCod[0];
            A683ContagemResultado_PFLFMUltima = P00S110_A683ContagemResultado_PFLFMUltima[0];
            A682ContagemResultado_PFBFMUltima = P00S110_A682ContagemResultado_PFBFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P00S110_A685ContagemResultado_PFLFSUltima[0];
            A684ContagemResultado_PFBFSUltima = P00S110_A684ContagemResultado_PFBFSUltima[0];
            A531ContagemResultado_StatusUltCnt = P00S110_A531ContagemResultado_StatusUltCnt[0];
            A802ContagemResultado_DeflatorCnt = P00S110_A802ContagemResultado_DeflatorCnt[0];
            A510ContagemResultado_EsforcoSoma = P00S110_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P00S110_n510ContagemResultado_EsforcoSoma[0];
            A584ContagemResultado_ContadorFM = P00S110_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P00S110_A566ContagemResultado_DataUltCnt[0];
            A493ContagemResultado_DemandaFM = P00S110_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00S110_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00S110_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00S110_n457ContagemResultado_Demanda[0];
            A456ContagemResultado_Codigo = P00S110_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = P00S110_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00S110_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00S110_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00S110_n801ContagemResultado_ServicoSigla[0];
            A771ContagemResultado_StatusDmnVnc = P00S110_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = P00S110_n771ContagemResultado_StatusDmnVnc[0];
            A509ContagemrResultado_SistemaSigla = P00S110_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00S110_n509ContagemrResultado_SistemaSigla[0];
            A803ContagemResultado_ContratadaSigla = P00S110_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00S110_n803ContagemResultado_ContratadaSigla[0];
            A52Contratada_AreaTrabalhoCod = P00S110_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00S110_n52Contratada_AreaTrabalhoCod[0];
            A683ContagemResultado_PFLFMUltima = P00S110_A683ContagemResultado_PFLFMUltima[0];
            A682ContagemResultado_PFBFMUltima = P00S110_A682ContagemResultado_PFBFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P00S110_A685ContagemResultado_PFLFSUltima[0];
            A684ContagemResultado_PFBFSUltima = P00S110_A684ContagemResultado_PFBFSUltima[0];
            A531ContagemResultado_StatusUltCnt = P00S110_A531ContagemResultado_StatusUltCnt[0];
            A802ContagemResultado_DeflatorCnt = P00S110_A802ContagemResultado_DeflatorCnt[0];
            A584ContagemResultado_ContadorFM = P00S110_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P00S110_A566ContagemResultado_DataUltCnt[0];
            A510ContagemResultado_EsforcoSoma = P00S110_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P00S110_n510ContagemResultado_EsforcoSoma[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            if ( (Convert.ToDecimal(0)==AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal) || ( ( A574ContagemResultado_PFFinal >= AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ) ) )
            {
               if ( (Convert.ToDecimal(0)==AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to) || ( ( A574ContagemResultado_PFFinal <= AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ) ) )
               {
                  A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
                  AV51count = 0;
                  while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00S110_A803ContagemResultado_ContratadaSigla[0], A803ContagemResultado_ContratadaSigla) == 0 ) )
                  {
                     BRKS15 = false;
                     A490ContagemResultado_ContratadaCod = P00S110_A490ContagemResultado_ContratadaCod[0];
                     n490ContagemResultado_ContratadaCod = P00S110_n490ContagemResultado_ContratadaCod[0];
                     A456ContagemResultado_Codigo = P00S110_A456ContagemResultado_Codigo[0];
                     AV51count = (long)(AV51count+1);
                     BRKS15 = true;
                     pr_default.readNext(2);
                  }
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A803ContagemResultado_ContratadaSigla)) )
                  {
                     AV43Option = A803ContagemResultado_ContratadaSigla;
                     AV44Options.Add(AV43Option, 0);
                     AV49OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV51count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                  }
                  if ( AV44Options.Count == 50 )
                  {
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
               }
            }
            if ( ! BRKS15 )
            {
               BRKS15 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTAGEMRESULTADO_SERVICOSIGLAOPTIONS' Routine */
         AV20TFContagemResultado_ServicoSigla = AV39SearchTxt;
         AV21TFContagemResultado_ServicoSigla_Sel = "";
         AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod = AV57Contratada_AreaTrabalhoCod;
         AV114ExtraWWContagemResultadoOSDS_2_Contratada_codigo = AV58Contratada_Codigo;
         AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = AV59DynamicFiltersSelector1;
         AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 = AV60DynamicFiltersOperator1;
         AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = AV61ContagemResultado_OsFsOsFm1;
         AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 = AV62ContagemResultado_DataDmn1;
         AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 = AV63ContagemResultado_DataDmn_To1;
         AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 = AV64ContagemResultado_DataUltCnt1;
         AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 = AV65ContagemResultado_DataUltCnt_To1;
         AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 = AV66ContagemResultado_ContadorFM1;
         AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 = AV67ContagemResultado_SistemaCod1;
         AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 = AV68ContagemResultado_ContratadaCod1;
         AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 = AV69ContagemResultado_NaoCnfDmnCod1;
         AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 = AV70ContagemResultado_StatusDmn1;
         AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 = AV71ContagemResultado_StatusDmnVnc1;
         AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 = AV72ContagemResultado_EsforcoSoma1;
         AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 = AV73ContagemResultado_Baseline1;
         AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 = AV74ContagemResultado_Servico1;
         AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = AV75DynamicFiltersEnabled2;
         AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = AV76DynamicFiltersSelector2;
         AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 = AV77DynamicFiltersOperator2;
         AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = AV78ContagemResultado_OsFsOsFm2;
         AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 = AV79ContagemResultado_DataDmn2;
         AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 = AV80ContagemResultado_DataDmn_To2;
         AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 = AV81ContagemResultado_DataUltCnt2;
         AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 = AV82ContagemResultado_DataUltCnt_To2;
         AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 = AV83ContagemResultado_ContadorFM2;
         AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 = AV84ContagemResultado_SistemaCod2;
         AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 = AV85ContagemResultado_ContratadaCod2;
         AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 = AV86ContagemResultado_NaoCnfDmnCod2;
         AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 = AV87ContagemResultado_StatusDmn2;
         AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 = AV88ContagemResultado_StatusDmnVnc2;
         AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 = AV89ContagemResultado_EsforcoSoma2;
         AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 = AV90ContagemResultado_Baseline2;
         AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 = AV91ContagemResultado_Servico2;
         AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = AV92DynamicFiltersEnabled3;
         AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = AV93DynamicFiltersSelector3;
         AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 = AV94DynamicFiltersOperator3;
         AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = AV95ContagemResultado_OsFsOsFm3;
         AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 = AV96ContagemResultado_DataDmn3;
         AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 = AV97ContagemResultado_DataDmn_To3;
         AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 = AV98ContagemResultado_DataUltCnt3;
         AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 = AV99ContagemResultado_DataUltCnt_To3;
         AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 = AV100ContagemResultado_ContadorFM3;
         AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 = AV101ContagemResultado_SistemaCod3;
         AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 = AV102ContagemResultado_ContratadaCod3;
         AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 = AV103ContagemResultado_NaoCnfDmnCod3;
         AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 = AV104ContagemResultado_StatusDmn3;
         AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 = AV105ContagemResultado_StatusDmnVnc3;
         AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 = AV106ContagemResultado_EsforcoSoma3;
         AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 = AV107ContagemResultado_Baseline3;
         AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 = AV108ContagemResultado_Servico3;
         AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm = AV10TFContagemResultado_OsFsOsFm;
         AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel = AV11TFContagemResultado_OsFsOsFm_Sel;
         AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn = AV12TFContagemResultado_DataDmn;
         AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to = AV13TFContagemResultado_DataDmn_To;
         AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt = AV14TFContagemResultado_DataUltCnt;
         AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to = AV15TFContagemResultado_DataUltCnt_To;
         AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla = AV16TFContagemrResultado_SistemaSigla;
         AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel = AV17TFContagemrResultado_SistemaSigla_Sel;
         AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla = AV18TFContagemResultado_ContratadaSigla;
         AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel = AV19TFContagemResultado_ContratadaSigla_Sel;
         AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla = AV20TFContagemResultado_ServicoSigla;
         AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel = AV21TFContagemResultado_ServicoSigla_Sel;
         AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt = AV22TFContagemResultado_DeflatorCnt;
         AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to = AV23TFContagemResultado_DeflatorCnt_To;
         AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels = AV25TFContagemResultado_StatusDmn_Sels;
         AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels = AV27TFContagemResultado_StatusUltCnt_Sels;
         AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel = AV28TFContagemResultado_Baseline_Sel;
         AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima = AV29TFContagemResultado_PFBFSUltima;
         AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to = AV30TFContagemResultado_PFBFSUltima_To;
         AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima = AV31TFContagemResultado_PFLFSUltima;
         AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to = AV32TFContagemResultado_PFLFSUltima_To;
         AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima = AV33TFContagemResultado_PFBFMUltima;
         AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to = AV34TFContagemResultado_PFBFMUltima_To;
         AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima = AV35TFContagemResultado_PFLFMUltima;
         AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to = AV36TFContagemResultado_PFLFMUltima_To;
         AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal = AV37TFContagemResultado_PFFinal;
         AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to = AV38TFContagemResultado_PFFinal_To;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A531ContagemResultado_StatusUltCnt ,
                                              AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels ,
                                              AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod ,
                                              AV192Contratada_dousuario ,
                                              AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 ,
                                              AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 ,
                                              AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 ,
                                              AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 ,
                                              AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 ,
                                              AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 ,
                                              AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 ,
                                              AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 ,
                                              AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 ,
                                              AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 ,
                                              AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 ,
                                              AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 ,
                                              AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 ,
                                              AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 ,
                                              AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 ,
                                              AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 ,
                                              AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 ,
                                              AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 ,
                                              AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 ,
                                              AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 ,
                                              AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 ,
                                              AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 ,
                                              AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 ,
                                              AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 ,
                                              AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 ,
                                              AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 ,
                                              AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 ,
                                              AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 ,
                                              AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 ,
                                              AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 ,
                                              AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 ,
                                              AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 ,
                                              AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 ,
                                              AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 ,
                                              AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 ,
                                              AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 ,
                                              AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 ,
                                              AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 ,
                                              AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 ,
                                              AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 ,
                                              AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 ,
                                              AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel ,
                                              AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm ,
                                              AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn ,
                                              AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to ,
                                              AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel ,
                                              AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla ,
                                              AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel ,
                                              AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla ,
                                              AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel ,
                                              AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla ,
                                              AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel ,
                                              AV54GridState.gxTpr_Dynamicfilters.Count ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A489ContagemResultado_SistemaCod ,
                                              A468ContagemResultado_NaoCnfDmnCod ,
                                              A771ContagemResultado_StatusDmnVnc ,
                                              A510ContagemResultado_EsforcoSoma ,
                                              A598ContagemResultado_Baseline ,
                                              A601ContagemResultado_Servico ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A803ContagemResultado_ContratadaSigla ,
                                              A801ContagemResultado_ServicoSigla ,
                                              A456ContagemResultado_Codigo ,
                                              AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 ,
                                              AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 ,
                                              A584ContagemResultado_ContadorFM ,
                                              A508ContagemResultado_Owner ,
                                              AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 ,
                                              AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 ,
                                              AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 ,
                                              AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 ,
                                              AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 ,
                                              AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 ,
                                              AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt ,
                                              AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to ,
                                              AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt ,
                                              A802ContagemResultado_DeflatorCnt ,
                                              AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to ,
                                              AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels.Count ,
                                              AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima ,
                                              A684ContagemResultado_PFBFSUltima ,
                                              AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to ,
                                              AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima ,
                                              A685ContagemResultado_PFLFSUltima ,
                                              AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to ,
                                              AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima ,
                                              A682ContagemResultado_PFBFMUltima ,
                                              AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to ,
                                              AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima ,
                                              A683ContagemResultado_PFLFMUltima ,
                                              AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to ,
                                              AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ,
                                              A574ContagemResultado_PFFinal ,
                                              AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.SHORT
                                              }
         });
         lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1), "%", "");
         lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2), "%", "");
         lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3), "%", "");
         lV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm = StringUtil.Concat( StringUtil.RTrim( AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm), "%", "");
         lV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla), 25, "%");
         lV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla = StringUtil.PadR( StringUtil.RTrim( AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla), 15, "%");
         lV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P00S113 */
         pr_default.execute(3, new Object[] {AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1, AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1, AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1, AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1, AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1, AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1, AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1, AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2, AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2, AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2, AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2, AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2, AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2, AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2, AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2, AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2, AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2, AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3, AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3, AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3, AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3, AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3, AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3, AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3, AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3, AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3, AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3, AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt, AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt, AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to, AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to, AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt, AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt, AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to, AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to, AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels.Count, AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima, AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima, AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to, AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to, AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima, AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima, AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to, AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to, AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima, AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima, AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to, AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to, AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima, AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima, AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to, AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to, AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod, AV192Contratada_dousuario, AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1, AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1, AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1, AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1, AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1, AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1, AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1, AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1, AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1, AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1, AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1, AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1, AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2, AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2, AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2, AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2, AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2, AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2, AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2, AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2, AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2, AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2, AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2, AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2, AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3, AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3, AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3, AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3, AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3, AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3, AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3, AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3, AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3, AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3, AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3, AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3, lV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm, AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel, AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn, AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to, lV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla, AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel, lV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla, AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel,
         lV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla, AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKS17 = false;
            A1553ContagemResultado_CntSrvCod = P00S113_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00S113_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = P00S113_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00S113_n602ContagemResultado_OSVinculada[0];
            A1583ContagemResultado_TipoRegistro = P00S113_A1583ContagemResultado_TipoRegistro[0];
            A801ContagemResultado_ServicoSigla = P00S113_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00S113_n801ContagemResultado_ServicoSigla[0];
            A803ContagemResultado_ContratadaSigla = P00S113_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00S113_n803ContagemResultado_ContratadaSigla[0];
            A509ContagemrResultado_SistemaSigla = P00S113_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00S113_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00S113_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00S113_n601ContagemResultado_Servico[0];
            A598ContagemResultado_Baseline = P00S113_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P00S113_n598ContagemResultado_Baseline[0];
            A771ContagemResultado_StatusDmnVnc = P00S113_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = P00S113_n771ContagemResultado_StatusDmnVnc[0];
            A484ContagemResultado_StatusDmn = P00S113_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00S113_n484ContagemResultado_StatusDmn[0];
            A468ContagemResultado_NaoCnfDmnCod = P00S113_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P00S113_n468ContagemResultado_NaoCnfDmnCod[0];
            A489ContagemResultado_SistemaCod = P00S113_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00S113_n489ContagemResultado_SistemaCod[0];
            A508ContagemResultado_Owner = P00S113_A508ContagemResultado_Owner[0];
            A471ContagemResultado_DataDmn = P00S113_A471ContagemResultado_DataDmn[0];
            A490ContagemResultado_ContratadaCod = P00S113_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00S113_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00S113_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00S113_n52Contratada_AreaTrabalhoCod[0];
            A683ContagemResultado_PFLFMUltima = P00S113_A683ContagemResultado_PFLFMUltima[0];
            A682ContagemResultado_PFBFMUltima = P00S113_A682ContagemResultado_PFBFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P00S113_A685ContagemResultado_PFLFSUltima[0];
            A684ContagemResultado_PFBFSUltima = P00S113_A684ContagemResultado_PFBFSUltima[0];
            A531ContagemResultado_StatusUltCnt = P00S113_A531ContagemResultado_StatusUltCnt[0];
            A802ContagemResultado_DeflatorCnt = P00S113_A802ContagemResultado_DeflatorCnt[0];
            A510ContagemResultado_EsforcoSoma = P00S113_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P00S113_n510ContagemResultado_EsforcoSoma[0];
            A584ContagemResultado_ContadorFM = P00S113_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P00S113_A566ContagemResultado_DataUltCnt[0];
            A493ContagemResultado_DemandaFM = P00S113_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00S113_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00S113_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00S113_n457ContagemResultado_Demanda[0];
            A456ContagemResultado_Codigo = P00S113_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = P00S113_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00S113_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00S113_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00S113_n801ContagemResultado_ServicoSigla[0];
            A771ContagemResultado_StatusDmnVnc = P00S113_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = P00S113_n771ContagemResultado_StatusDmnVnc[0];
            A509ContagemrResultado_SistemaSigla = P00S113_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00S113_n509ContagemrResultado_SistemaSigla[0];
            A803ContagemResultado_ContratadaSigla = P00S113_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00S113_n803ContagemResultado_ContratadaSigla[0];
            A52Contratada_AreaTrabalhoCod = P00S113_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00S113_n52Contratada_AreaTrabalhoCod[0];
            A683ContagemResultado_PFLFMUltima = P00S113_A683ContagemResultado_PFLFMUltima[0];
            A682ContagemResultado_PFBFMUltima = P00S113_A682ContagemResultado_PFBFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P00S113_A685ContagemResultado_PFLFSUltima[0];
            A684ContagemResultado_PFBFSUltima = P00S113_A684ContagemResultado_PFBFSUltima[0];
            A531ContagemResultado_StatusUltCnt = P00S113_A531ContagemResultado_StatusUltCnt[0];
            A802ContagemResultado_DeflatorCnt = P00S113_A802ContagemResultado_DeflatorCnt[0];
            A584ContagemResultado_ContadorFM = P00S113_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P00S113_A566ContagemResultado_DataUltCnt[0];
            A510ContagemResultado_EsforcoSoma = P00S113_A510ContagemResultado_EsforcoSoma[0];
            n510ContagemResultado_EsforcoSoma = P00S113_n510ContagemResultado_EsforcoSoma[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            if ( (Convert.ToDecimal(0)==AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal) || ( ( A574ContagemResultado_PFFinal >= AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ) ) )
            {
               if ( (Convert.ToDecimal(0)==AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to) || ( ( A574ContagemResultado_PFFinal <= AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ) ) )
               {
                  A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
                  AV51count = 0;
                  while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00S113_A801ContagemResultado_ServicoSigla[0], A801ContagemResultado_ServicoSigla) == 0 ) )
                  {
                     BRKS17 = false;
                     A1553ContagemResultado_CntSrvCod = P00S113_A1553ContagemResultado_CntSrvCod[0];
                     n1553ContagemResultado_CntSrvCod = P00S113_n1553ContagemResultado_CntSrvCod[0];
                     A601ContagemResultado_Servico = P00S113_A601ContagemResultado_Servico[0];
                     n601ContagemResultado_Servico = P00S113_n601ContagemResultado_Servico[0];
                     A456ContagemResultado_Codigo = P00S113_A456ContagemResultado_Codigo[0];
                     A601ContagemResultado_Servico = P00S113_A601ContagemResultado_Servico[0];
                     n601ContagemResultado_Servico = P00S113_n601ContagemResultado_Servico[0];
                     AV51count = (long)(AV51count+1);
                     BRKS17 = true;
                     pr_default.readNext(3);
                  }
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A801ContagemResultado_ServicoSigla)) )
                  {
                     AV43Option = A801ContagemResultado_ServicoSigla;
                     AV44Options.Add(AV43Option, 0);
                     AV49OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV51count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                  }
                  if ( AV44Options.Count == 50 )
                  {
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
               }
            }
            if ( ! BRKS17 )
            {
               BRKS17 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV44Options = new GxSimpleCollection();
         AV47OptionsDesc = new GxSimpleCollection();
         AV49OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV52Session = context.GetSession();
         AV54GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV55GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContagemResultado_OsFsOsFm = "";
         AV11TFContagemResultado_OsFsOsFm_Sel = "";
         AV12TFContagemResultado_DataDmn = DateTime.MinValue;
         AV13TFContagemResultado_DataDmn_To = DateTime.MinValue;
         AV14TFContagemResultado_DataUltCnt = DateTime.MinValue;
         AV15TFContagemResultado_DataUltCnt_To = DateTime.MinValue;
         AV16TFContagemrResultado_SistemaSigla = "";
         AV17TFContagemrResultado_SistemaSigla_Sel = "";
         AV18TFContagemResultado_ContratadaSigla = "";
         AV19TFContagemResultado_ContratadaSigla_Sel = "";
         AV20TFContagemResultado_ServicoSigla = "";
         AV21TFContagemResultado_ServicoSigla_Sel = "";
         AV24TFContagemResultado_StatusDmn_SelsJson = "";
         AV25TFContagemResultado_StatusDmn_Sels = new GxSimpleCollection();
         AV26TFContagemResultado_StatusUltCnt_SelsJson = "";
         AV27TFContagemResultado_StatusUltCnt_Sels = new GxSimpleCollection();
         AV56GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV59DynamicFiltersSelector1 = "";
         AV61ContagemResultado_OsFsOsFm1 = "";
         AV62ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV63ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV64ContagemResultado_DataUltCnt1 = DateTime.MinValue;
         AV65ContagemResultado_DataUltCnt_To1 = DateTime.MinValue;
         AV70ContagemResultado_StatusDmn1 = "";
         AV71ContagemResultado_StatusDmnVnc1 = "";
         AV73ContagemResultado_Baseline1 = "";
         AV76DynamicFiltersSelector2 = "";
         AV78ContagemResultado_OsFsOsFm2 = "";
         AV79ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV80ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV81ContagemResultado_DataUltCnt2 = DateTime.MinValue;
         AV82ContagemResultado_DataUltCnt_To2 = DateTime.MinValue;
         AV87ContagemResultado_StatusDmn2 = "";
         AV88ContagemResultado_StatusDmnVnc2 = "";
         AV90ContagemResultado_Baseline2 = "";
         AV93DynamicFiltersSelector3 = "";
         AV95ContagemResultado_OsFsOsFm3 = "";
         AV96ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV97ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV98ContagemResultado_DataUltCnt3 = DateTime.MinValue;
         AV99ContagemResultado_DataUltCnt_To3 = DateTime.MinValue;
         AV104ContagemResultado_StatusDmn3 = "";
         AV105ContagemResultado_StatusDmnVnc3 = "";
         AV107ContagemResultado_Baseline3 = "";
         AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = "";
         AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = "";
         AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 = DateTime.MinValue;
         AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 = DateTime.MinValue;
         AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 = "";
         AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 = "";
         AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 = "";
         AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = "";
         AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = "";
         AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 = DateTime.MinValue;
         AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 = DateTime.MinValue;
         AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 = "";
         AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 = "";
         AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 = "";
         AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = "";
         AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = "";
         AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 = DateTime.MinValue;
         AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 = DateTime.MinValue;
         AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 = "";
         AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 = "";
         AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 = "";
         AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm = "";
         AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel = "";
         AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn = DateTime.MinValue;
         AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to = DateTime.MinValue;
         AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt = DateTime.MinValue;
         AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to = DateTime.MinValue;
         AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla = "";
         AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel = "";
         AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla = "";
         AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel = "";
         AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla = "";
         AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel = "";
         AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels = new GxSimpleCollection();
         AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 = "";
         lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 = "";
         lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 = "";
         lV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm = "";
         lV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla = "";
         lV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla = "";
         lV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla = "";
         A484ContagemResultado_StatusDmn = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A771ContagemResultado_StatusDmnVnc = "";
         A509ContagemrResultado_SistemaSigla = "";
         A803ContagemResultado_ContratadaSigla = "";
         A801ContagemResultado_ServicoSigla = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         P00S14_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00S14_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00S14_A602ContagemResultado_OSVinculada = new int[1] ;
         P00S14_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00S14_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P00S14_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00S14_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00S14_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00S14_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00S14_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00S14_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00S14_A601ContagemResultado_Servico = new int[1] ;
         P00S14_n601ContagemResultado_Servico = new bool[] {false} ;
         P00S14_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00S14_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00S14_A771ContagemResultado_StatusDmnVnc = new String[] {""} ;
         P00S14_n771ContagemResultado_StatusDmnVnc = new bool[] {false} ;
         P00S14_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00S14_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00S14_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P00S14_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P00S14_A489ContagemResultado_SistemaCod = new int[1] ;
         P00S14_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00S14_A508ContagemResultado_Owner = new int[1] ;
         P00S14_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00S14_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00S14_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00S14_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00S14_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00S14_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         P00S14_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00S14_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         P00S14_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00S14_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00S14_A802ContagemResultado_DeflatorCnt = new decimal[1] ;
         P00S14_A510ContagemResultado_EsforcoSoma = new int[1] ;
         P00S14_n510ContagemResultado_EsforcoSoma = new bool[] {false} ;
         P00S14_A584ContagemResultado_ContadorFM = new int[1] ;
         P00S14_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00S14_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00S14_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00S14_A457ContagemResultado_Demanda = new String[] {""} ;
         P00S14_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00S14_A456ContagemResultado_Codigo = new int[1] ;
         A501ContagemResultado_OsFsOsFm = "";
         AV43Option = "";
         P00S17_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00S17_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00S17_A602ContagemResultado_OSVinculada = new int[1] ;
         P00S17_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00S17_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P00S17_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00S17_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00S17_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00S17_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00S17_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00S17_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00S17_A601ContagemResultado_Servico = new int[1] ;
         P00S17_n601ContagemResultado_Servico = new bool[] {false} ;
         P00S17_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00S17_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00S17_A771ContagemResultado_StatusDmnVnc = new String[] {""} ;
         P00S17_n771ContagemResultado_StatusDmnVnc = new bool[] {false} ;
         P00S17_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00S17_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00S17_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P00S17_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P00S17_A489ContagemResultado_SistemaCod = new int[1] ;
         P00S17_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00S17_A508ContagemResultado_Owner = new int[1] ;
         P00S17_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00S17_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00S17_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00S17_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00S17_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00S17_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         P00S17_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00S17_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         P00S17_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00S17_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00S17_A802ContagemResultado_DeflatorCnt = new decimal[1] ;
         P00S17_A510ContagemResultado_EsforcoSoma = new int[1] ;
         P00S17_n510ContagemResultado_EsforcoSoma = new bool[] {false} ;
         P00S17_A584ContagemResultado_ContadorFM = new int[1] ;
         P00S17_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00S17_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00S17_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00S17_A457ContagemResultado_Demanda = new String[] {""} ;
         P00S17_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00S17_A456ContagemResultado_Codigo = new int[1] ;
         P00S110_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00S110_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00S110_A602ContagemResultado_OSVinculada = new int[1] ;
         P00S110_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00S110_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P00S110_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00S110_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00S110_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00S110_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00S110_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00S110_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00S110_A601ContagemResultado_Servico = new int[1] ;
         P00S110_n601ContagemResultado_Servico = new bool[] {false} ;
         P00S110_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00S110_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00S110_A771ContagemResultado_StatusDmnVnc = new String[] {""} ;
         P00S110_n771ContagemResultado_StatusDmnVnc = new bool[] {false} ;
         P00S110_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00S110_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00S110_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P00S110_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P00S110_A489ContagemResultado_SistemaCod = new int[1] ;
         P00S110_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00S110_A508ContagemResultado_Owner = new int[1] ;
         P00S110_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00S110_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00S110_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00S110_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00S110_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00S110_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         P00S110_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00S110_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         P00S110_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00S110_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00S110_A802ContagemResultado_DeflatorCnt = new decimal[1] ;
         P00S110_A510ContagemResultado_EsforcoSoma = new int[1] ;
         P00S110_n510ContagemResultado_EsforcoSoma = new bool[] {false} ;
         P00S110_A584ContagemResultado_ContadorFM = new int[1] ;
         P00S110_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00S110_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00S110_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00S110_A457ContagemResultado_Demanda = new String[] {""} ;
         P00S110_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00S110_A456ContagemResultado_Codigo = new int[1] ;
         P00S113_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00S113_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00S113_A602ContagemResultado_OSVinculada = new int[1] ;
         P00S113_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00S113_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P00S113_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00S113_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00S113_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00S113_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00S113_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00S113_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00S113_A601ContagemResultado_Servico = new int[1] ;
         P00S113_n601ContagemResultado_Servico = new bool[] {false} ;
         P00S113_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00S113_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00S113_A771ContagemResultado_StatusDmnVnc = new String[] {""} ;
         P00S113_n771ContagemResultado_StatusDmnVnc = new bool[] {false} ;
         P00S113_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00S113_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00S113_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P00S113_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P00S113_A489ContagemResultado_SistemaCod = new int[1] ;
         P00S113_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00S113_A508ContagemResultado_Owner = new int[1] ;
         P00S113_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00S113_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00S113_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00S113_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00S113_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00S113_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         P00S113_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00S113_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         P00S113_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00S113_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00S113_A802ContagemResultado_DeflatorCnt = new decimal[1] ;
         P00S113_A510ContagemResultado_EsforcoSoma = new int[1] ;
         P00S113_n510ContagemResultado_EsforcoSoma = new bool[] {false} ;
         P00S113_A584ContagemResultado_ContadorFM = new int[1] ;
         P00S113_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00S113_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00S113_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00S113_A457ContagemResultado_Demanda = new String[] {""} ;
         P00S113_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00S113_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getextrawwcontagemresultadoosfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00S14_A1553ContagemResultado_CntSrvCod, P00S14_n1553ContagemResultado_CntSrvCod, P00S14_A602ContagemResultado_OSVinculada, P00S14_n602ContagemResultado_OSVinculada, P00S14_A1583ContagemResultado_TipoRegistro, P00S14_A801ContagemResultado_ServicoSigla, P00S14_n801ContagemResultado_ServicoSigla, P00S14_A803ContagemResultado_ContratadaSigla, P00S14_n803ContagemResultado_ContratadaSigla, P00S14_A509ContagemrResultado_SistemaSigla,
               P00S14_n509ContagemrResultado_SistemaSigla, P00S14_A601ContagemResultado_Servico, P00S14_n601ContagemResultado_Servico, P00S14_A598ContagemResultado_Baseline, P00S14_n598ContagemResultado_Baseline, P00S14_A771ContagemResultado_StatusDmnVnc, P00S14_n771ContagemResultado_StatusDmnVnc, P00S14_A484ContagemResultado_StatusDmn, P00S14_n484ContagemResultado_StatusDmn, P00S14_A468ContagemResultado_NaoCnfDmnCod,
               P00S14_n468ContagemResultado_NaoCnfDmnCod, P00S14_A489ContagemResultado_SistemaCod, P00S14_n489ContagemResultado_SistemaCod, P00S14_A508ContagemResultado_Owner, P00S14_A471ContagemResultado_DataDmn, P00S14_A490ContagemResultado_ContratadaCod, P00S14_n490ContagemResultado_ContratadaCod, P00S14_A52Contratada_AreaTrabalhoCod, P00S14_n52Contratada_AreaTrabalhoCod, P00S14_A683ContagemResultado_PFLFMUltima,
               P00S14_A682ContagemResultado_PFBFMUltima, P00S14_A685ContagemResultado_PFLFSUltima, P00S14_A684ContagemResultado_PFBFSUltima, P00S14_A531ContagemResultado_StatusUltCnt, P00S14_A802ContagemResultado_DeflatorCnt, P00S14_A510ContagemResultado_EsforcoSoma, P00S14_n510ContagemResultado_EsforcoSoma, P00S14_A584ContagemResultado_ContadorFM, P00S14_A566ContagemResultado_DataUltCnt, P00S14_A493ContagemResultado_DemandaFM,
               P00S14_n493ContagemResultado_DemandaFM, P00S14_A457ContagemResultado_Demanda, P00S14_n457ContagemResultado_Demanda, P00S14_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00S17_A1553ContagemResultado_CntSrvCod, P00S17_n1553ContagemResultado_CntSrvCod, P00S17_A602ContagemResultado_OSVinculada, P00S17_n602ContagemResultado_OSVinculada, P00S17_A1583ContagemResultado_TipoRegistro, P00S17_A509ContagemrResultado_SistemaSigla, P00S17_n509ContagemrResultado_SistemaSigla, P00S17_A801ContagemResultado_ServicoSigla, P00S17_n801ContagemResultado_ServicoSigla, P00S17_A803ContagemResultado_ContratadaSigla,
               P00S17_n803ContagemResultado_ContratadaSigla, P00S17_A601ContagemResultado_Servico, P00S17_n601ContagemResultado_Servico, P00S17_A598ContagemResultado_Baseline, P00S17_n598ContagemResultado_Baseline, P00S17_A771ContagemResultado_StatusDmnVnc, P00S17_n771ContagemResultado_StatusDmnVnc, P00S17_A484ContagemResultado_StatusDmn, P00S17_n484ContagemResultado_StatusDmn, P00S17_A468ContagemResultado_NaoCnfDmnCod,
               P00S17_n468ContagemResultado_NaoCnfDmnCod, P00S17_A489ContagemResultado_SistemaCod, P00S17_n489ContagemResultado_SistemaCod, P00S17_A508ContagemResultado_Owner, P00S17_A471ContagemResultado_DataDmn, P00S17_A490ContagemResultado_ContratadaCod, P00S17_n490ContagemResultado_ContratadaCod, P00S17_A52Contratada_AreaTrabalhoCod, P00S17_n52Contratada_AreaTrabalhoCod, P00S17_A683ContagemResultado_PFLFMUltima,
               P00S17_A682ContagemResultado_PFBFMUltima, P00S17_A685ContagemResultado_PFLFSUltima, P00S17_A684ContagemResultado_PFBFSUltima, P00S17_A531ContagemResultado_StatusUltCnt, P00S17_A802ContagemResultado_DeflatorCnt, P00S17_A510ContagemResultado_EsforcoSoma, P00S17_n510ContagemResultado_EsforcoSoma, P00S17_A584ContagemResultado_ContadorFM, P00S17_A566ContagemResultado_DataUltCnt, P00S17_A493ContagemResultado_DemandaFM,
               P00S17_n493ContagemResultado_DemandaFM, P00S17_A457ContagemResultado_Demanda, P00S17_n457ContagemResultado_Demanda, P00S17_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00S110_A1553ContagemResultado_CntSrvCod, P00S110_n1553ContagemResultado_CntSrvCod, P00S110_A602ContagemResultado_OSVinculada, P00S110_n602ContagemResultado_OSVinculada, P00S110_A1583ContagemResultado_TipoRegistro, P00S110_A803ContagemResultado_ContratadaSigla, P00S110_n803ContagemResultado_ContratadaSigla, P00S110_A801ContagemResultado_ServicoSigla, P00S110_n801ContagemResultado_ServicoSigla, P00S110_A509ContagemrResultado_SistemaSigla,
               P00S110_n509ContagemrResultado_SistemaSigla, P00S110_A601ContagemResultado_Servico, P00S110_n601ContagemResultado_Servico, P00S110_A598ContagemResultado_Baseline, P00S110_n598ContagemResultado_Baseline, P00S110_A771ContagemResultado_StatusDmnVnc, P00S110_n771ContagemResultado_StatusDmnVnc, P00S110_A484ContagemResultado_StatusDmn, P00S110_n484ContagemResultado_StatusDmn, P00S110_A468ContagemResultado_NaoCnfDmnCod,
               P00S110_n468ContagemResultado_NaoCnfDmnCod, P00S110_A489ContagemResultado_SistemaCod, P00S110_n489ContagemResultado_SistemaCod, P00S110_A508ContagemResultado_Owner, P00S110_A471ContagemResultado_DataDmn, P00S110_A490ContagemResultado_ContratadaCod, P00S110_n490ContagemResultado_ContratadaCod, P00S110_A52Contratada_AreaTrabalhoCod, P00S110_n52Contratada_AreaTrabalhoCod, P00S110_A683ContagemResultado_PFLFMUltima,
               P00S110_A682ContagemResultado_PFBFMUltima, P00S110_A685ContagemResultado_PFLFSUltima, P00S110_A684ContagemResultado_PFBFSUltima, P00S110_A531ContagemResultado_StatusUltCnt, P00S110_A802ContagemResultado_DeflatorCnt, P00S110_A510ContagemResultado_EsforcoSoma, P00S110_n510ContagemResultado_EsforcoSoma, P00S110_A584ContagemResultado_ContadorFM, P00S110_A566ContagemResultado_DataUltCnt, P00S110_A493ContagemResultado_DemandaFM,
               P00S110_n493ContagemResultado_DemandaFM, P00S110_A457ContagemResultado_Demanda, P00S110_n457ContagemResultado_Demanda, P00S110_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00S113_A1553ContagemResultado_CntSrvCod, P00S113_n1553ContagemResultado_CntSrvCod, P00S113_A602ContagemResultado_OSVinculada, P00S113_n602ContagemResultado_OSVinculada, P00S113_A1583ContagemResultado_TipoRegistro, P00S113_A801ContagemResultado_ServicoSigla, P00S113_n801ContagemResultado_ServicoSigla, P00S113_A803ContagemResultado_ContratadaSigla, P00S113_n803ContagemResultado_ContratadaSigla, P00S113_A509ContagemrResultado_SistemaSigla,
               P00S113_n509ContagemrResultado_SistemaSigla, P00S113_A601ContagemResultado_Servico, P00S113_n601ContagemResultado_Servico, P00S113_A598ContagemResultado_Baseline, P00S113_n598ContagemResultado_Baseline, P00S113_A771ContagemResultado_StatusDmnVnc, P00S113_n771ContagemResultado_StatusDmnVnc, P00S113_A484ContagemResultado_StatusDmn, P00S113_n484ContagemResultado_StatusDmn, P00S113_A468ContagemResultado_NaoCnfDmnCod,
               P00S113_n468ContagemResultado_NaoCnfDmnCod, P00S113_A489ContagemResultado_SistemaCod, P00S113_n489ContagemResultado_SistemaCod, P00S113_A508ContagemResultado_Owner, P00S113_A471ContagemResultado_DataDmn, P00S113_A490ContagemResultado_ContratadaCod, P00S113_n490ContagemResultado_ContratadaCod, P00S113_A52Contratada_AreaTrabalhoCod, P00S113_n52Contratada_AreaTrabalhoCod, P00S113_A683ContagemResultado_PFLFMUltima,
               P00S113_A682ContagemResultado_PFBFMUltima, P00S113_A685ContagemResultado_PFLFSUltima, P00S113_A684ContagemResultado_PFBFSUltima, P00S113_A531ContagemResultado_StatusUltCnt, P00S113_A802ContagemResultado_DeflatorCnt, P00S113_A510ContagemResultado_EsforcoSoma, P00S113_n510ContagemResultado_EsforcoSoma, P00S113_A584ContagemResultado_ContadorFM, P00S113_A566ContagemResultado_DataUltCnt, P00S113_A493ContagemResultado_DemandaFM,
               P00S113_n493ContagemResultado_DemandaFM, P00S113_A457ContagemResultado_Demanda, P00S113_n457ContagemResultado_Demanda, P00S113_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV28TFContagemResultado_Baseline_Sel ;
      private short AV60DynamicFiltersOperator1 ;
      private short AV77DynamicFiltersOperator2 ;
      private short AV94DynamicFiltersOperator3 ;
      private short AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 ;
      private short AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 ;
      private short AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 ;
      private short AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short A1583ContagemResultado_TipoRegistro ;
      private int AV111GXV1 ;
      private int AV57Contratada_AreaTrabalhoCod ;
      private int AV58Contratada_Codigo ;
      private int AV66ContagemResultado_ContadorFM1 ;
      private int AV67ContagemResultado_SistemaCod1 ;
      private int AV68ContagemResultado_ContratadaCod1 ;
      private int AV69ContagemResultado_NaoCnfDmnCod1 ;
      private int AV72ContagemResultado_EsforcoSoma1 ;
      private int AV74ContagemResultado_Servico1 ;
      private int AV83ContagemResultado_ContadorFM2 ;
      private int AV84ContagemResultado_SistemaCod2 ;
      private int AV85ContagemResultado_ContratadaCod2 ;
      private int AV86ContagemResultado_NaoCnfDmnCod2 ;
      private int AV89ContagemResultado_EsforcoSoma2 ;
      private int AV91ContagemResultado_Servico2 ;
      private int AV100ContagemResultado_ContadorFM3 ;
      private int AV101ContagemResultado_SistemaCod3 ;
      private int AV102ContagemResultado_ContratadaCod3 ;
      private int AV103ContagemResultado_NaoCnfDmnCod3 ;
      private int AV106ContagemResultado_EsforcoSoma3 ;
      private int AV108ContagemResultado_Servico3 ;
      private int AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod ;
      private int AV114ExtraWWContagemResultadoOSDS_2_Contratada_codigo ;
      private int AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 ;
      private int AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 ;
      private int AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 ;
      private int AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 ;
      private int AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 ;
      private int AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 ;
      private int AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 ;
      private int AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 ;
      private int AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 ;
      private int AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 ;
      private int AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 ;
      private int AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 ;
      private int AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 ;
      private int AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 ;
      private int AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 ;
      private int AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 ;
      private int AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 ;
      private int AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 ;
      private int AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels_Count ;
      private int AV54GridState_gxTpr_Dynamicfilters_Count ;
      private int AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels_Count ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A510ContagemResultado_EsforcoSoma ;
      private int A601ContagemResultado_Servico ;
      private int A456ContagemResultado_Codigo ;
      private int A584ContagemResultado_ContadorFM ;
      private int A508ContagemResultado_Owner ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int AV42InsertIndex ;
      private long AV51count ;
      private decimal AV22TFContagemResultado_DeflatorCnt ;
      private decimal AV23TFContagemResultado_DeflatorCnt_To ;
      private decimal AV29TFContagemResultado_PFBFSUltima ;
      private decimal AV30TFContagemResultado_PFBFSUltima_To ;
      private decimal AV31TFContagemResultado_PFLFSUltima ;
      private decimal AV32TFContagemResultado_PFLFSUltima_To ;
      private decimal AV33TFContagemResultado_PFBFMUltima ;
      private decimal AV34TFContagemResultado_PFBFMUltima_To ;
      private decimal AV35TFContagemResultado_PFLFMUltima ;
      private decimal AV36TFContagemResultado_PFLFMUltima_To ;
      private decimal AV37TFContagemResultado_PFFinal ;
      private decimal AV38TFContagemResultado_PFFinal_To ;
      private decimal AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt ;
      private decimal AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to ;
      private decimal AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima ;
      private decimal AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to ;
      private decimal AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima ;
      private decimal AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to ;
      private decimal AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima ;
      private decimal AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to ;
      private decimal AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima ;
      private decimal AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to ;
      private decimal AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ;
      private decimal AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ;
      private decimal AV192Contratada_dousuario ;
      private decimal A802ContagemResultado_DeflatorCnt ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A683ContagemResultado_PFLFMUltima ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private String AV16TFContagemrResultado_SistemaSigla ;
      private String AV17TFContagemrResultado_SistemaSigla_Sel ;
      private String AV18TFContagemResultado_ContratadaSigla ;
      private String AV19TFContagemResultado_ContratadaSigla_Sel ;
      private String AV20TFContagemResultado_ServicoSigla ;
      private String AV21TFContagemResultado_ServicoSigla_Sel ;
      private String AV70ContagemResultado_StatusDmn1 ;
      private String AV71ContagemResultado_StatusDmnVnc1 ;
      private String AV73ContagemResultado_Baseline1 ;
      private String AV87ContagemResultado_StatusDmn2 ;
      private String AV88ContagemResultado_StatusDmnVnc2 ;
      private String AV90ContagemResultado_Baseline2 ;
      private String AV104ContagemResultado_StatusDmn3 ;
      private String AV105ContagemResultado_StatusDmnVnc3 ;
      private String AV107ContagemResultado_Baseline3 ;
      private String AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 ;
      private String AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 ;
      private String AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 ;
      private String AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 ;
      private String AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 ;
      private String AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 ;
      private String AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 ;
      private String AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 ;
      private String AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 ;
      private String AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla ;
      private String AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel ;
      private String AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla ;
      private String AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel ;
      private String AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla ;
      private String AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel ;
      private String scmdbuf ;
      private String lV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla ;
      private String lV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla ;
      private String lV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String A771ContagemResultado_StatusDmnVnc ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A801ContagemResultado_ServicoSigla ;
      private DateTime AV12TFContagemResultado_DataDmn ;
      private DateTime AV13TFContagemResultado_DataDmn_To ;
      private DateTime AV14TFContagemResultado_DataUltCnt ;
      private DateTime AV15TFContagemResultado_DataUltCnt_To ;
      private DateTime AV62ContagemResultado_DataDmn1 ;
      private DateTime AV63ContagemResultado_DataDmn_To1 ;
      private DateTime AV64ContagemResultado_DataUltCnt1 ;
      private DateTime AV65ContagemResultado_DataUltCnt_To1 ;
      private DateTime AV79ContagemResultado_DataDmn2 ;
      private DateTime AV80ContagemResultado_DataDmn_To2 ;
      private DateTime AV81ContagemResultado_DataUltCnt2 ;
      private DateTime AV82ContagemResultado_DataUltCnt_To2 ;
      private DateTime AV96ContagemResultado_DataDmn3 ;
      private DateTime AV97ContagemResultado_DataDmn_To3 ;
      private DateTime AV98ContagemResultado_DataUltCnt3 ;
      private DateTime AV99ContagemResultado_DataUltCnt_To3 ;
      private DateTime AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 ;
      private DateTime AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 ;
      private DateTime AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 ;
      private DateTime AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 ;
      private DateTime AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 ;
      private DateTime AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 ;
      private DateTime AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 ;
      private DateTime AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 ;
      private DateTime AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 ;
      private DateTime AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 ;
      private DateTime AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 ;
      private DateTime AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 ;
      private DateTime AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn ;
      private DateTime AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to ;
      private DateTime AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt ;
      private DateTime AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private bool returnInSub ;
      private bool AV75DynamicFiltersEnabled2 ;
      private bool AV92DynamicFiltersEnabled3 ;
      private bool AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 ;
      private bool AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 ;
      private bool A598ContagemResultado_Baseline ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n601ContagemResultado_Servico ;
      private bool n598ContagemResultado_Baseline ;
      private bool n771ContagemResultado_StatusDmnVnc ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n510ContagemResultado_EsforcoSoma ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool BRKS13 ;
      private bool BRKS15 ;
      private bool BRKS17 ;
      private String AV50OptionIndexesJson ;
      private String AV45OptionsJson ;
      private String AV48OptionsDescJson ;
      private String AV24TFContagemResultado_StatusDmn_SelsJson ;
      private String AV26TFContagemResultado_StatusUltCnt_SelsJson ;
      private String AV41DDOName ;
      private String AV39SearchTxt ;
      private String AV40SearchTxtTo ;
      private String AV10TFContagemResultado_OsFsOsFm ;
      private String AV11TFContagemResultado_OsFsOsFm_Sel ;
      private String AV59DynamicFiltersSelector1 ;
      private String AV61ContagemResultado_OsFsOsFm1 ;
      private String AV76DynamicFiltersSelector2 ;
      private String AV78ContagemResultado_OsFsOsFm2 ;
      private String AV93DynamicFiltersSelector3 ;
      private String AV95ContagemResultado_OsFsOsFm3 ;
      private String AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 ;
      private String AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 ;
      private String AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 ;
      private String AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 ;
      private String AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 ;
      private String AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 ;
      private String AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm ;
      private String AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel ;
      private String lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 ;
      private String lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 ;
      private String lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 ;
      private String lV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String AV43Option ;
      private IGxSession AV52Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00S14_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00S14_n1553ContagemResultado_CntSrvCod ;
      private int[] P00S14_A602ContagemResultado_OSVinculada ;
      private bool[] P00S14_n602ContagemResultado_OSVinculada ;
      private short[] P00S14_A1583ContagemResultado_TipoRegistro ;
      private String[] P00S14_A801ContagemResultado_ServicoSigla ;
      private bool[] P00S14_n801ContagemResultado_ServicoSigla ;
      private String[] P00S14_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00S14_n803ContagemResultado_ContratadaSigla ;
      private String[] P00S14_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00S14_n509ContagemrResultado_SistemaSigla ;
      private int[] P00S14_A601ContagemResultado_Servico ;
      private bool[] P00S14_n601ContagemResultado_Servico ;
      private bool[] P00S14_A598ContagemResultado_Baseline ;
      private bool[] P00S14_n598ContagemResultado_Baseline ;
      private String[] P00S14_A771ContagemResultado_StatusDmnVnc ;
      private bool[] P00S14_n771ContagemResultado_StatusDmnVnc ;
      private String[] P00S14_A484ContagemResultado_StatusDmn ;
      private bool[] P00S14_n484ContagemResultado_StatusDmn ;
      private int[] P00S14_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P00S14_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] P00S14_A489ContagemResultado_SistemaCod ;
      private bool[] P00S14_n489ContagemResultado_SistemaCod ;
      private int[] P00S14_A508ContagemResultado_Owner ;
      private DateTime[] P00S14_A471ContagemResultado_DataDmn ;
      private int[] P00S14_A490ContagemResultado_ContratadaCod ;
      private bool[] P00S14_n490ContagemResultado_ContratadaCod ;
      private int[] P00S14_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00S14_n52Contratada_AreaTrabalhoCod ;
      private decimal[] P00S14_A683ContagemResultado_PFLFMUltima ;
      private decimal[] P00S14_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P00S14_A685ContagemResultado_PFLFSUltima ;
      private decimal[] P00S14_A684ContagemResultado_PFBFSUltima ;
      private short[] P00S14_A531ContagemResultado_StatusUltCnt ;
      private decimal[] P00S14_A802ContagemResultado_DeflatorCnt ;
      private int[] P00S14_A510ContagemResultado_EsforcoSoma ;
      private bool[] P00S14_n510ContagemResultado_EsforcoSoma ;
      private int[] P00S14_A584ContagemResultado_ContadorFM ;
      private DateTime[] P00S14_A566ContagemResultado_DataUltCnt ;
      private String[] P00S14_A493ContagemResultado_DemandaFM ;
      private bool[] P00S14_n493ContagemResultado_DemandaFM ;
      private String[] P00S14_A457ContagemResultado_Demanda ;
      private bool[] P00S14_n457ContagemResultado_Demanda ;
      private int[] P00S14_A456ContagemResultado_Codigo ;
      private int[] P00S17_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00S17_n1553ContagemResultado_CntSrvCod ;
      private int[] P00S17_A602ContagemResultado_OSVinculada ;
      private bool[] P00S17_n602ContagemResultado_OSVinculada ;
      private short[] P00S17_A1583ContagemResultado_TipoRegistro ;
      private String[] P00S17_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00S17_n509ContagemrResultado_SistemaSigla ;
      private String[] P00S17_A801ContagemResultado_ServicoSigla ;
      private bool[] P00S17_n801ContagemResultado_ServicoSigla ;
      private String[] P00S17_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00S17_n803ContagemResultado_ContratadaSigla ;
      private int[] P00S17_A601ContagemResultado_Servico ;
      private bool[] P00S17_n601ContagemResultado_Servico ;
      private bool[] P00S17_A598ContagemResultado_Baseline ;
      private bool[] P00S17_n598ContagemResultado_Baseline ;
      private String[] P00S17_A771ContagemResultado_StatusDmnVnc ;
      private bool[] P00S17_n771ContagemResultado_StatusDmnVnc ;
      private String[] P00S17_A484ContagemResultado_StatusDmn ;
      private bool[] P00S17_n484ContagemResultado_StatusDmn ;
      private int[] P00S17_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P00S17_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] P00S17_A489ContagemResultado_SistemaCod ;
      private bool[] P00S17_n489ContagemResultado_SistemaCod ;
      private int[] P00S17_A508ContagemResultado_Owner ;
      private DateTime[] P00S17_A471ContagemResultado_DataDmn ;
      private int[] P00S17_A490ContagemResultado_ContratadaCod ;
      private bool[] P00S17_n490ContagemResultado_ContratadaCod ;
      private int[] P00S17_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00S17_n52Contratada_AreaTrabalhoCod ;
      private decimal[] P00S17_A683ContagemResultado_PFLFMUltima ;
      private decimal[] P00S17_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P00S17_A685ContagemResultado_PFLFSUltima ;
      private decimal[] P00S17_A684ContagemResultado_PFBFSUltima ;
      private short[] P00S17_A531ContagemResultado_StatusUltCnt ;
      private decimal[] P00S17_A802ContagemResultado_DeflatorCnt ;
      private int[] P00S17_A510ContagemResultado_EsforcoSoma ;
      private bool[] P00S17_n510ContagemResultado_EsforcoSoma ;
      private int[] P00S17_A584ContagemResultado_ContadorFM ;
      private DateTime[] P00S17_A566ContagemResultado_DataUltCnt ;
      private String[] P00S17_A493ContagemResultado_DemandaFM ;
      private bool[] P00S17_n493ContagemResultado_DemandaFM ;
      private String[] P00S17_A457ContagemResultado_Demanda ;
      private bool[] P00S17_n457ContagemResultado_Demanda ;
      private int[] P00S17_A456ContagemResultado_Codigo ;
      private int[] P00S110_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00S110_n1553ContagemResultado_CntSrvCod ;
      private int[] P00S110_A602ContagemResultado_OSVinculada ;
      private bool[] P00S110_n602ContagemResultado_OSVinculada ;
      private short[] P00S110_A1583ContagemResultado_TipoRegistro ;
      private String[] P00S110_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00S110_n803ContagemResultado_ContratadaSigla ;
      private String[] P00S110_A801ContagemResultado_ServicoSigla ;
      private bool[] P00S110_n801ContagemResultado_ServicoSigla ;
      private String[] P00S110_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00S110_n509ContagemrResultado_SistemaSigla ;
      private int[] P00S110_A601ContagemResultado_Servico ;
      private bool[] P00S110_n601ContagemResultado_Servico ;
      private bool[] P00S110_A598ContagemResultado_Baseline ;
      private bool[] P00S110_n598ContagemResultado_Baseline ;
      private String[] P00S110_A771ContagemResultado_StatusDmnVnc ;
      private bool[] P00S110_n771ContagemResultado_StatusDmnVnc ;
      private String[] P00S110_A484ContagemResultado_StatusDmn ;
      private bool[] P00S110_n484ContagemResultado_StatusDmn ;
      private int[] P00S110_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P00S110_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] P00S110_A489ContagemResultado_SistemaCod ;
      private bool[] P00S110_n489ContagemResultado_SistemaCod ;
      private int[] P00S110_A508ContagemResultado_Owner ;
      private DateTime[] P00S110_A471ContagemResultado_DataDmn ;
      private int[] P00S110_A490ContagemResultado_ContratadaCod ;
      private bool[] P00S110_n490ContagemResultado_ContratadaCod ;
      private int[] P00S110_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00S110_n52Contratada_AreaTrabalhoCod ;
      private decimal[] P00S110_A683ContagemResultado_PFLFMUltima ;
      private decimal[] P00S110_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P00S110_A685ContagemResultado_PFLFSUltima ;
      private decimal[] P00S110_A684ContagemResultado_PFBFSUltima ;
      private short[] P00S110_A531ContagemResultado_StatusUltCnt ;
      private decimal[] P00S110_A802ContagemResultado_DeflatorCnt ;
      private int[] P00S110_A510ContagemResultado_EsforcoSoma ;
      private bool[] P00S110_n510ContagemResultado_EsforcoSoma ;
      private int[] P00S110_A584ContagemResultado_ContadorFM ;
      private DateTime[] P00S110_A566ContagemResultado_DataUltCnt ;
      private String[] P00S110_A493ContagemResultado_DemandaFM ;
      private bool[] P00S110_n493ContagemResultado_DemandaFM ;
      private String[] P00S110_A457ContagemResultado_Demanda ;
      private bool[] P00S110_n457ContagemResultado_Demanda ;
      private int[] P00S110_A456ContagemResultado_Codigo ;
      private int[] P00S113_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00S113_n1553ContagemResultado_CntSrvCod ;
      private int[] P00S113_A602ContagemResultado_OSVinculada ;
      private bool[] P00S113_n602ContagemResultado_OSVinculada ;
      private short[] P00S113_A1583ContagemResultado_TipoRegistro ;
      private String[] P00S113_A801ContagemResultado_ServicoSigla ;
      private bool[] P00S113_n801ContagemResultado_ServicoSigla ;
      private String[] P00S113_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00S113_n803ContagemResultado_ContratadaSigla ;
      private String[] P00S113_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00S113_n509ContagemrResultado_SistemaSigla ;
      private int[] P00S113_A601ContagemResultado_Servico ;
      private bool[] P00S113_n601ContagemResultado_Servico ;
      private bool[] P00S113_A598ContagemResultado_Baseline ;
      private bool[] P00S113_n598ContagemResultado_Baseline ;
      private String[] P00S113_A771ContagemResultado_StatusDmnVnc ;
      private bool[] P00S113_n771ContagemResultado_StatusDmnVnc ;
      private String[] P00S113_A484ContagemResultado_StatusDmn ;
      private bool[] P00S113_n484ContagemResultado_StatusDmn ;
      private int[] P00S113_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P00S113_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] P00S113_A489ContagemResultado_SistemaCod ;
      private bool[] P00S113_n489ContagemResultado_SistemaCod ;
      private int[] P00S113_A508ContagemResultado_Owner ;
      private DateTime[] P00S113_A471ContagemResultado_DataDmn ;
      private int[] P00S113_A490ContagemResultado_ContratadaCod ;
      private bool[] P00S113_n490ContagemResultado_ContratadaCod ;
      private int[] P00S113_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00S113_n52Contratada_AreaTrabalhoCod ;
      private decimal[] P00S113_A683ContagemResultado_PFLFMUltima ;
      private decimal[] P00S113_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P00S113_A685ContagemResultado_PFLFSUltima ;
      private decimal[] P00S113_A684ContagemResultado_PFBFSUltima ;
      private short[] P00S113_A531ContagemResultado_StatusUltCnt ;
      private decimal[] P00S113_A802ContagemResultado_DeflatorCnt ;
      private int[] P00S113_A510ContagemResultado_EsforcoSoma ;
      private bool[] P00S113_n510ContagemResultado_EsforcoSoma ;
      private int[] P00S113_A584ContagemResultado_ContadorFM ;
      private DateTime[] P00S113_A566ContagemResultado_DataUltCnt ;
      private String[] P00S113_A493ContagemResultado_DemandaFM ;
      private bool[] P00S113_n493ContagemResultado_DemandaFM ;
      private String[] P00S113_A457ContagemResultado_Demanda ;
      private bool[] P00S113_n457ContagemResultado_Demanda ;
      private int[] P00S113_A456ContagemResultado_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV27TFContagemResultado_StatusUltCnt_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25TFContagemResultado_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV44Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV47OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV49OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV54GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV55GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV56GridStateDynamicFilter ;
   }

   public class getextrawwcontagemresultadoosfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00S14( IGxContext context ,
                                             short A531ContagemResultado_StatusUltCnt ,
                                             IGxCollection AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels ,
                                             int AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod ,
                                             decimal AV192Contratada_dousuario ,
                                             String AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 ,
                                             short AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 ,
                                             String AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 ,
                                             DateTime AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 ,
                                             DateTime AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 ,
                                             int AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 ,
                                             int AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 ,
                                             int AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 ,
                                             String AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 ,
                                             String AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 ,
                                             int AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 ,
                                             String AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 ,
                                             int AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 ,
                                             bool AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 ,
                                             String AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 ,
                                             short AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 ,
                                             String AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 ,
                                             DateTime AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 ,
                                             DateTime AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 ,
                                             int AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 ,
                                             int AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 ,
                                             int AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 ,
                                             String AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 ,
                                             String AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 ,
                                             int AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 ,
                                             String AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 ,
                                             int AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 ,
                                             bool AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 ,
                                             String AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 ,
                                             short AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 ,
                                             String AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 ,
                                             DateTime AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 ,
                                             DateTime AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 ,
                                             int AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 ,
                                             int AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 ,
                                             int AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 ,
                                             String AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 ,
                                             String AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 ,
                                             int AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 ,
                                             String AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 ,
                                             int AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 ,
                                             String AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel ,
                                             String AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm ,
                                             DateTime AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn ,
                                             DateTime AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to ,
                                             String AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel ,
                                             String AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla ,
                                             String AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel ,
                                             String AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla ,
                                             String AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel ,
                                             String AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla ,
                                             int AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels_Count ,
                                             short AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel ,
                                             int AV54GridState_gxTpr_Dynamicfilters_Count ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             int A489ContagemResultado_SistemaCod ,
                                             int A468ContagemResultado_NaoCnfDmnCod ,
                                             String A771ContagemResultado_StatusDmnVnc ,
                                             int A510ContagemResultado_EsforcoSoma ,
                                             bool A598ContagemResultado_Baseline ,
                                             int A601ContagemResultado_Servico ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             String A803ContagemResultado_ContratadaSigla ,
                                             String A801ContagemResultado_ServicoSigla ,
                                             int A456ContagemResultado_Codigo ,
                                             DateTime AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             DateTime AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 ,
                                             int AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 ,
                                             int A584ContagemResultado_ContadorFM ,
                                             int A508ContagemResultado_Owner ,
                                             DateTime AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 ,
                                             DateTime AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 ,
                                             int AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 ,
                                             DateTime AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 ,
                                             DateTime AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 ,
                                             int AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 ,
                                             DateTime AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt ,
                                             DateTime AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to ,
                                             decimal AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt ,
                                             decimal A802ContagemResultado_DeflatorCnt ,
                                             decimal AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to ,
                                             int AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels_Count ,
                                             decimal AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima ,
                                             decimal A684ContagemResultado_PFBFSUltima ,
                                             decimal AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to ,
                                             decimal AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima ,
                                             decimal A685ContagemResultado_PFLFSUltima ,
                                             decimal AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to ,
                                             decimal AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima ,
                                             decimal A682ContagemResultado_PFBFMUltima ,
                                             decimal AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to ,
                                             decimal AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima ,
                                             decimal A683ContagemResultado_PFLFMUltima ,
                                             decimal AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to ,
                                             decimal AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ,
                                             decimal A574ContagemResultado_PFFinal ,
                                             decimal AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ,
                                             short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [124] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T1.[ContagemResultado_TipoRegistro], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T6.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T5.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Baseline], T4.[ContagemResultado_StatusDmn] AS ContagemResultado_StatusDmnVnc, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_NaoCnfDmnCod], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_Owner], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T6.[Contratada_AreaTrabalhoCod], COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T7.[ContagemResultado_DeflatorCnt], 0) AS ContagemResultado_DeflatorCnt, COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) AS ContagemResultado_EsforcoSoma, COALESCE( T7.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_Codigo] FROM ((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo]";
         scmdbuf = scmdbuf + " = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContagemResultado] T4 WITH (NOLOCK) ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada]) LEFT JOIN [Sistema] T5 WITH (NOLOCK) ON T5.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, MIN([ContagemResultado_Deflator]) AS ContagemResultado_DeflatorCnt, MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T7 ON T7.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultadoContagens_Esforco]) AS ContagemResultado_EsforcoSoma, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (Not ( @AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = 1 and @AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = 1 and @AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = 1 and @AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = 1 and @AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = 1 and @AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = 1 and @AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3)))";
         scmdbuf = scmdbuf + " and ((@AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and ((@AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_DeflatorCnt], 0) >= @AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt))";
         scmdbuf = scmdbuf + " and ((@AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_DeflatorCnt], 0) <= @AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to))";
         scmdbuf = scmdbuf + " and (@AV180ExtCount <= 0 or ( " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels, "COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) IN (", ")") + "))";
         scmdbuf = scmdbuf + " and ((@AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) >= @AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima))";
         scmdbuf = scmdbuf + " and ((@AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <= @AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to))";
         scmdbuf = scmdbuf + " and ((@AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) >= @AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima))";
         scmdbuf = scmdbuf + " and ((@AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <= @AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to))";
         scmdbuf = scmdbuf + " and ((@AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) >= @AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima))";
         scmdbuf = scmdbuf + " and ((@AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) <= @AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to))";
         scmdbuf = scmdbuf + " and ((@AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) >= @AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima))";
         scmdbuf = scmdbuf + " and ((@AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) <= @AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to))";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod > 0 )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_AreaTrabalhoCod] = @AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod)";
         }
         else
         {
            GXv_int2[61] = 1;
         }
         if ( ( AV192Contratada_dousuario > Convert.ToDecimal( 0 )) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV192Contratada_dousuario)";
         }
         else
         {
            GXv_int2[62] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] = @AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int2[63] = 1;
            GXv_int2[64] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like @lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int2[65] = 1;
            GXv_int2[66] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int2[67] = 1;
            GXv_int2[68] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int2[69] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int2[70] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int2[71] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1) && ! (0==AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int2[72] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1)";
         }
         else
         {
            GXv_int2[73] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int2[74] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_StatusDmn] = @AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1)";
         }
         else
         {
            GXv_int2[75] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) > @AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int2[76] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) < @AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int2[77] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) = @AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int2[78] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int2[79] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] = @AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int2[80] = 1;
            GXv_int2[81] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like @lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int2[82] = 1;
            GXv_int2[83] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int2[84] = 1;
            GXv_int2[85] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int2[86] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int2[87] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int2[88] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2) && ! (0==AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int2[89] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2)";
         }
         else
         {
            GXv_int2[90] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int2[91] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_StatusDmn] = @AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2)";
         }
         else
         {
            GXv_int2[92] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) > @AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int2[93] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) < @AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int2[94] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) = @AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int2[95] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int2[96] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] = @AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int2[97] = 1;
            GXv_int2[98] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like @lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int2[99] = 1;
            GXv_int2[100] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int2[101] = 1;
            GXv_int2[102] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int2[103] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int2[104] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int2[105] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3) && ! (0==AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int2[106] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3)";
         }
         else
         {
            GXv_int2[107] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int2[108] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_StatusDmn] = @AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3)";
         }
         else
         {
            GXv_int2[109] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) > @AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int2[110] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) < @AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int2[111] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) = @AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int2[112] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int2[113] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm)) ) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T1.[ContagemResultado_Demanda])) + CASE  WHEN (T1.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T1.[ContagemResultado_DemandaFM])) END) like @lV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm)";
         }
         else
         {
            GXv_int2[114] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel)) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T1.[ContagemResultado_Demanda])) + CASE  WHEN (T1.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T1.[ContagemResultado_DemandaFM])) END) = @AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel)";
         }
         else
         {
            GXv_int2[115] = 1;
         }
         if ( ! (DateTime.MinValue==AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int2[116] = 1;
         }
         if ( ! (DateTime.MinValue==AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int2[117] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] like @lV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla)";
         }
         else
         {
            GXv_int2[118] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] = @AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel)";
         }
         else
         {
            GXv_int2[119] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] like @lV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla)";
         }
         else
         {
            GXv_int2[120] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] = @AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel)";
         }
         else
         {
            GXv_int2[121] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int2[122] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int2[123] = 1;
         }
         if ( AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 0)";
         }
         if ( AV54GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_P00S17( IGxContext context ,
                                             short A531ContagemResultado_StatusUltCnt ,
                                             IGxCollection AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels ,
                                             int AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod ,
                                             decimal AV192Contratada_dousuario ,
                                             String AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 ,
                                             short AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 ,
                                             String AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 ,
                                             DateTime AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 ,
                                             DateTime AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 ,
                                             int AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 ,
                                             int AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 ,
                                             int AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 ,
                                             String AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 ,
                                             String AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 ,
                                             int AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 ,
                                             String AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 ,
                                             int AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 ,
                                             bool AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 ,
                                             String AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 ,
                                             short AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 ,
                                             String AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 ,
                                             DateTime AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 ,
                                             DateTime AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 ,
                                             int AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 ,
                                             int AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 ,
                                             int AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 ,
                                             String AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 ,
                                             String AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 ,
                                             int AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 ,
                                             String AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 ,
                                             int AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 ,
                                             bool AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 ,
                                             String AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 ,
                                             short AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 ,
                                             String AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 ,
                                             DateTime AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 ,
                                             DateTime AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 ,
                                             int AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 ,
                                             int AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 ,
                                             int AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 ,
                                             String AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 ,
                                             String AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 ,
                                             int AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 ,
                                             String AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 ,
                                             int AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 ,
                                             String AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel ,
                                             String AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm ,
                                             DateTime AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn ,
                                             DateTime AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to ,
                                             String AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel ,
                                             String AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla ,
                                             String AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel ,
                                             String AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla ,
                                             String AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel ,
                                             String AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla ,
                                             int AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels_Count ,
                                             short AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel ,
                                             int AV54GridState_gxTpr_Dynamicfilters_Count ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             int A489ContagemResultado_SistemaCod ,
                                             int A468ContagemResultado_NaoCnfDmnCod ,
                                             String A771ContagemResultado_StatusDmnVnc ,
                                             int A510ContagemResultado_EsforcoSoma ,
                                             bool A598ContagemResultado_Baseline ,
                                             int A601ContagemResultado_Servico ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             String A803ContagemResultado_ContratadaSigla ,
                                             String A801ContagemResultado_ServicoSigla ,
                                             int A456ContagemResultado_Codigo ,
                                             DateTime AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             DateTime AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 ,
                                             int AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 ,
                                             int A584ContagemResultado_ContadorFM ,
                                             int A508ContagemResultado_Owner ,
                                             DateTime AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 ,
                                             DateTime AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 ,
                                             int AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 ,
                                             DateTime AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 ,
                                             DateTime AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 ,
                                             int AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 ,
                                             DateTime AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt ,
                                             DateTime AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to ,
                                             decimal AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt ,
                                             decimal A802ContagemResultado_DeflatorCnt ,
                                             decimal AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to ,
                                             int AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels_Count ,
                                             decimal AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima ,
                                             decimal A684ContagemResultado_PFBFSUltima ,
                                             decimal AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to ,
                                             decimal AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima ,
                                             decimal A685ContagemResultado_PFLFSUltima ,
                                             decimal AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to ,
                                             decimal AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima ,
                                             decimal A682ContagemResultado_PFBFMUltima ,
                                             decimal AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to ,
                                             decimal AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima ,
                                             decimal A683ContagemResultado_PFLFMUltima ,
                                             decimal AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to ,
                                             decimal AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ,
                                             decimal A574ContagemResultado_PFFinal ,
                                             decimal AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ,
                                             short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [124] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T1.[ContagemResultado_TipoRegistro], T5.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T6.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Baseline], T4.[ContagemResultado_StatusDmn] AS ContagemResultado_StatusDmnVnc, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_NaoCnfDmnCod], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_Owner], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T6.[Contratada_AreaTrabalhoCod], COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T7.[ContagemResultado_DeflatorCnt], 0) AS ContagemResultado_DeflatorCnt, COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) AS ContagemResultado_EsforcoSoma, COALESCE( T7.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_Codigo] FROM ((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo]";
         scmdbuf = scmdbuf + " = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContagemResultado] T4 WITH (NOLOCK) ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada]) LEFT JOIN [Sistema] T5 WITH (NOLOCK) ON T5.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, MIN([ContagemResultado_Deflator]) AS ContagemResultado_DeflatorCnt, MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T7 ON T7.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultadoContagens_Esforco]) AS ContagemResultado_EsforcoSoma, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (Not ( @AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = 1 and @AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = 1 and @AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = 1 and @AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = 1 and @AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = 1 and @AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = 1 and @AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3)))";
         scmdbuf = scmdbuf + " and ((@AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and ((@AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_DeflatorCnt], 0) >= @AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt))";
         scmdbuf = scmdbuf + " and ((@AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_DeflatorCnt], 0) <= @AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to))";
         scmdbuf = scmdbuf + " and (@AV180ExtCount <= 0 or ( " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels, "COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) IN (", ")") + "))";
         scmdbuf = scmdbuf + " and ((@AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) >= @AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima))";
         scmdbuf = scmdbuf + " and ((@AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <= @AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to))";
         scmdbuf = scmdbuf + " and ((@AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) >= @AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima))";
         scmdbuf = scmdbuf + " and ((@AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <= @AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to))";
         scmdbuf = scmdbuf + " and ((@AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) >= @AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima))";
         scmdbuf = scmdbuf + " and ((@AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) <= @AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to))";
         scmdbuf = scmdbuf + " and ((@AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) >= @AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima))";
         scmdbuf = scmdbuf + " and ((@AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) <= @AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to))";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod > 0 )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_AreaTrabalhoCod] = @AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod)";
         }
         else
         {
            GXv_int4[61] = 1;
         }
         if ( ( AV192Contratada_dousuario > Convert.ToDecimal( 0 )) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV192Contratada_dousuario)";
         }
         else
         {
            GXv_int4[62] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] = @AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int4[63] = 1;
            GXv_int4[64] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like @lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int4[65] = 1;
            GXv_int4[66] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int4[67] = 1;
            GXv_int4[68] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int4[69] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int4[70] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int4[71] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1) && ! (0==AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int4[72] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1)";
         }
         else
         {
            GXv_int4[73] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int4[74] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_StatusDmn] = @AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1)";
         }
         else
         {
            GXv_int4[75] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) > @AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int4[76] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) < @AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int4[77] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) = @AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int4[78] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int4[79] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] = @AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int4[80] = 1;
            GXv_int4[81] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like @lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int4[82] = 1;
            GXv_int4[83] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int4[84] = 1;
            GXv_int4[85] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int4[86] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int4[87] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int4[88] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2) && ! (0==AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int4[89] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2)";
         }
         else
         {
            GXv_int4[90] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int4[91] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_StatusDmn] = @AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2)";
         }
         else
         {
            GXv_int4[92] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) > @AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int4[93] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) < @AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int4[94] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) = @AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int4[95] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int4[96] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] = @AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int4[97] = 1;
            GXv_int4[98] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like @lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int4[99] = 1;
            GXv_int4[100] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int4[101] = 1;
            GXv_int4[102] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int4[103] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int4[104] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int4[105] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3) && ! (0==AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int4[106] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3)";
         }
         else
         {
            GXv_int4[107] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int4[108] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_StatusDmn] = @AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3)";
         }
         else
         {
            GXv_int4[109] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) > @AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int4[110] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) < @AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int4[111] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) = @AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int4[112] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int4[113] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm)) ) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T1.[ContagemResultado_Demanda])) + CASE  WHEN (T1.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T1.[ContagemResultado_DemandaFM])) END) like @lV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm)";
         }
         else
         {
            GXv_int4[114] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel)) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T1.[ContagemResultado_Demanda])) + CASE  WHEN (T1.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T1.[ContagemResultado_DemandaFM])) END) = @AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel)";
         }
         else
         {
            GXv_int4[115] = 1;
         }
         if ( ! (DateTime.MinValue==AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int4[116] = 1;
         }
         if ( ! (DateTime.MinValue==AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int4[117] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] like @lV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla)";
         }
         else
         {
            GXv_int4[118] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] = @AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel)";
         }
         else
         {
            GXv_int4[119] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] like @lV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla)";
         }
         else
         {
            GXv_int4[120] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] = @AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel)";
         }
         else
         {
            GXv_int4[121] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int4[122] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int4[123] = 1;
         }
         if ( AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 0)";
         }
         if ( AV54GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T5.[Sistema_Sigla]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_P00S110( IGxContext context ,
                                              short A531ContagemResultado_StatusUltCnt ,
                                              IGxCollection AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels ,
                                              String A484ContagemResultado_StatusDmn ,
                                              IGxCollection AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels ,
                                              int AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod ,
                                              decimal AV192Contratada_dousuario ,
                                              String AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 ,
                                              short AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 ,
                                              String AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 ,
                                              DateTime AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 ,
                                              DateTime AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 ,
                                              int AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 ,
                                              int AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 ,
                                              int AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 ,
                                              String AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 ,
                                              String AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 ,
                                              int AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 ,
                                              String AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 ,
                                              int AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 ,
                                              bool AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 ,
                                              String AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 ,
                                              short AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 ,
                                              String AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 ,
                                              DateTime AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 ,
                                              DateTime AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 ,
                                              int AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 ,
                                              int AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 ,
                                              int AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 ,
                                              String AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 ,
                                              String AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 ,
                                              int AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 ,
                                              String AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 ,
                                              int AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 ,
                                              bool AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 ,
                                              String AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 ,
                                              short AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 ,
                                              String AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 ,
                                              DateTime AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 ,
                                              DateTime AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 ,
                                              int AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 ,
                                              int AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 ,
                                              int AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 ,
                                              String AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 ,
                                              String AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 ,
                                              int AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 ,
                                              String AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 ,
                                              int AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 ,
                                              String AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel ,
                                              String AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm ,
                                              DateTime AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn ,
                                              DateTime AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to ,
                                              String AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel ,
                                              String AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla ,
                                              String AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel ,
                                              String AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla ,
                                              String AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel ,
                                              String AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla ,
                                              int AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels_Count ,
                                              short AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel ,
                                              int AV54GridState_gxTpr_Dynamicfilters_Count ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              String A457ContagemResultado_Demanda ,
                                              String A493ContagemResultado_DemandaFM ,
                                              DateTime A471ContagemResultado_DataDmn ,
                                              int A489ContagemResultado_SistemaCod ,
                                              int A468ContagemResultado_NaoCnfDmnCod ,
                                              String A771ContagemResultado_StatusDmnVnc ,
                                              int A510ContagemResultado_EsforcoSoma ,
                                              bool A598ContagemResultado_Baseline ,
                                              int A601ContagemResultado_Servico ,
                                              String A509ContagemrResultado_SistemaSigla ,
                                              String A803ContagemResultado_ContratadaSigla ,
                                              String A801ContagemResultado_ServicoSigla ,
                                              int A456ContagemResultado_Codigo ,
                                              DateTime AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 ,
                                              DateTime A566ContagemResultado_DataUltCnt ,
                                              DateTime AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 ,
                                              int AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 ,
                                              int A584ContagemResultado_ContadorFM ,
                                              int A508ContagemResultado_Owner ,
                                              DateTime AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 ,
                                              DateTime AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 ,
                                              int AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 ,
                                              DateTime AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 ,
                                              DateTime AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 ,
                                              int AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 ,
                                              DateTime AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt ,
                                              DateTime AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to ,
                                              decimal AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt ,
                                              decimal A802ContagemResultado_DeflatorCnt ,
                                              decimal AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to ,
                                              int AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels_Count ,
                                              decimal AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima ,
                                              decimal A684ContagemResultado_PFBFSUltima ,
                                              decimal AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to ,
                                              decimal AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima ,
                                              decimal A685ContagemResultado_PFLFSUltima ,
                                              decimal AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to ,
                                              decimal AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima ,
                                              decimal A682ContagemResultado_PFBFMUltima ,
                                              decimal AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to ,
                                              decimal AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima ,
                                              decimal A683ContagemResultado_PFLFMUltima ,
                                              decimal AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to ,
                                              decimal AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ,
                                              decimal A574ContagemResultado_PFFinal ,
                                              decimal AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ,
                                              short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [124] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T1.[ContagemResultado_TipoRegistro], T6.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T5.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Baseline], T4.[ContagemResultado_StatusDmn] AS ContagemResultado_StatusDmnVnc, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_NaoCnfDmnCod], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_Owner], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T6.[Contratada_AreaTrabalhoCod], COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T7.[ContagemResultado_DeflatorCnt], 0) AS ContagemResultado_DeflatorCnt, COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) AS ContagemResultado_EsforcoSoma, COALESCE( T7.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_Codigo] FROM ((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo]";
         scmdbuf = scmdbuf + " = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContagemResultado] T4 WITH (NOLOCK) ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada]) LEFT JOIN [Sistema] T5 WITH (NOLOCK) ON T5.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, MIN([ContagemResultado_Deflator]) AS ContagemResultado_DeflatorCnt, MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T7 ON T7.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultadoContagens_Esforco]) AS ContagemResultado_EsforcoSoma, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (Not ( @AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = 1 and @AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = 1 and @AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = 1 and @AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = 1 and @AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = 1 and @AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = 1 and @AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3)))";
         scmdbuf = scmdbuf + " and ((@AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and ((@AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_DeflatorCnt], 0) >= @AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt))";
         scmdbuf = scmdbuf + " and ((@AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_DeflatorCnt], 0) <= @AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to))";
         scmdbuf = scmdbuf + " and (@AV180ExtCount <= 0 or ( " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels, "COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) IN (", ")") + "))";
         scmdbuf = scmdbuf + " and ((@AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) >= @AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima))";
         scmdbuf = scmdbuf + " and ((@AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <= @AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to))";
         scmdbuf = scmdbuf + " and ((@AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) >= @AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima))";
         scmdbuf = scmdbuf + " and ((@AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <= @AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to))";
         scmdbuf = scmdbuf + " and ((@AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) >= @AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima))";
         scmdbuf = scmdbuf + " and ((@AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) <= @AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to))";
         scmdbuf = scmdbuf + " and ((@AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) >= @AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima))";
         scmdbuf = scmdbuf + " and ((@AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) <= @AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to))";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod > 0 )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_AreaTrabalhoCod] = @AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod)";
         }
         else
         {
            GXv_int6[61] = 1;
         }
         if ( ( AV192Contratada_dousuario > Convert.ToDecimal( 0 )) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV192Contratada_dousuario)";
         }
         else
         {
            GXv_int6[62] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] = @AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int6[63] = 1;
            GXv_int6[64] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like @lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int6[65] = 1;
            GXv_int6[66] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int6[67] = 1;
            GXv_int6[68] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int6[69] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int6[70] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int6[71] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1) && ! (0==AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int6[72] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1)";
         }
         else
         {
            GXv_int6[73] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int6[74] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_StatusDmn] = @AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1)";
         }
         else
         {
            GXv_int6[75] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) > @AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int6[76] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) < @AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int6[77] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) = @AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int6[78] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int6[79] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] = @AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int6[80] = 1;
            GXv_int6[81] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like @lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int6[82] = 1;
            GXv_int6[83] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int6[84] = 1;
            GXv_int6[85] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int6[86] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int6[87] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int6[88] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2) && ! (0==AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int6[89] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2)";
         }
         else
         {
            GXv_int6[90] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int6[91] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_StatusDmn] = @AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2)";
         }
         else
         {
            GXv_int6[92] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) > @AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int6[93] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) < @AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int6[94] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) = @AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int6[95] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int6[96] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] = @AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int6[97] = 1;
            GXv_int6[98] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like @lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int6[99] = 1;
            GXv_int6[100] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int6[101] = 1;
            GXv_int6[102] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int6[103] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int6[104] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int6[105] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3) && ! (0==AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int6[106] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3)";
         }
         else
         {
            GXv_int6[107] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int6[108] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_StatusDmn] = @AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3)";
         }
         else
         {
            GXv_int6[109] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) > @AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int6[110] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) < @AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int6[111] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) = @AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int6[112] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int6[113] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm)) ) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T1.[ContagemResultado_Demanda])) + CASE  WHEN (T1.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T1.[ContagemResultado_DemandaFM])) END) like @lV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm)";
         }
         else
         {
            GXv_int6[114] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel)) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T1.[ContagemResultado_Demanda])) + CASE  WHEN (T1.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T1.[ContagemResultado_DemandaFM])) END) = @AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel)";
         }
         else
         {
            GXv_int6[115] = 1;
         }
         if ( ! (DateTime.MinValue==AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int6[116] = 1;
         }
         if ( ! (DateTime.MinValue==AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int6[117] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] like @lV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla)";
         }
         else
         {
            GXv_int6[118] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] = @AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel)";
         }
         else
         {
            GXv_int6[119] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] like @lV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla)";
         }
         else
         {
            GXv_int6[120] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] = @AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel)";
         }
         else
         {
            GXv_int6[121] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int6[122] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int6[123] = 1;
         }
         if ( AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 0)";
         }
         if ( AV54GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T6.[Contratada_Sigla]";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      protected Object[] conditional_P00S113( IGxContext context ,
                                              short A531ContagemResultado_StatusUltCnt ,
                                              IGxCollection AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels ,
                                              String A484ContagemResultado_StatusDmn ,
                                              IGxCollection AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels ,
                                              int AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod ,
                                              decimal AV192Contratada_dousuario ,
                                              String AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 ,
                                              short AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 ,
                                              String AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 ,
                                              DateTime AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1 ,
                                              DateTime AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1 ,
                                              int AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1 ,
                                              int AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1 ,
                                              int AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1 ,
                                              String AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1 ,
                                              String AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1 ,
                                              int AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1 ,
                                              String AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1 ,
                                              int AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1 ,
                                              bool AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 ,
                                              String AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 ,
                                              short AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 ,
                                              String AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 ,
                                              DateTime AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2 ,
                                              DateTime AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2 ,
                                              int AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2 ,
                                              int AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2 ,
                                              int AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2 ,
                                              String AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2 ,
                                              String AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2 ,
                                              int AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2 ,
                                              String AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2 ,
                                              int AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2 ,
                                              bool AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 ,
                                              String AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 ,
                                              short AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 ,
                                              String AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 ,
                                              DateTime AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3 ,
                                              DateTime AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3 ,
                                              int AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3 ,
                                              int AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3 ,
                                              int AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3 ,
                                              String AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3 ,
                                              String AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3 ,
                                              int AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3 ,
                                              String AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3 ,
                                              int AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3 ,
                                              String AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel ,
                                              String AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm ,
                                              DateTime AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn ,
                                              DateTime AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to ,
                                              String AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel ,
                                              String AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla ,
                                              String AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel ,
                                              String AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla ,
                                              String AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel ,
                                              String AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla ,
                                              int AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels_Count ,
                                              short AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel ,
                                              int AV54GridState_gxTpr_Dynamicfilters_Count ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              String A457ContagemResultado_Demanda ,
                                              String A493ContagemResultado_DemandaFM ,
                                              DateTime A471ContagemResultado_DataDmn ,
                                              int A489ContagemResultado_SistemaCod ,
                                              int A468ContagemResultado_NaoCnfDmnCod ,
                                              String A771ContagemResultado_StatusDmnVnc ,
                                              int A510ContagemResultado_EsforcoSoma ,
                                              bool A598ContagemResultado_Baseline ,
                                              int A601ContagemResultado_Servico ,
                                              String A509ContagemrResultado_SistemaSigla ,
                                              String A803ContagemResultado_ContratadaSigla ,
                                              String A801ContagemResultado_ServicoSigla ,
                                              int A456ContagemResultado_Codigo ,
                                              DateTime AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 ,
                                              DateTime A566ContagemResultado_DataUltCnt ,
                                              DateTime AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 ,
                                              int AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 ,
                                              int A584ContagemResultado_ContadorFM ,
                                              int A508ContagemResultado_Owner ,
                                              DateTime AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 ,
                                              DateTime AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 ,
                                              int AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 ,
                                              DateTime AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 ,
                                              DateTime AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 ,
                                              int AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 ,
                                              DateTime AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt ,
                                              DateTime AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to ,
                                              decimal AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt ,
                                              decimal A802ContagemResultado_DeflatorCnt ,
                                              decimal AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to ,
                                              int AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels_Count ,
                                              decimal AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima ,
                                              decimal A684ContagemResultado_PFBFSUltima ,
                                              decimal AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to ,
                                              decimal AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima ,
                                              decimal A685ContagemResultado_PFLFSUltima ,
                                              decimal AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to ,
                                              decimal AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima ,
                                              decimal A682ContagemResultado_PFBFMUltima ,
                                              decimal AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to ,
                                              decimal AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima ,
                                              decimal A683ContagemResultado_PFLFMUltima ,
                                              decimal AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to ,
                                              decimal AV190ExtraWWContagemResultadoOSDS_78_Tfcontagemresultado_pffinal ,
                                              decimal A574ContagemResultado_PFFinal ,
                                              decimal AV191ExtraWWContagemResultadoOSDS_79_Tfcontagemresultado_pffinal_to ,
                                              short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int8 ;
         GXv_int8 = new short [124] ;
         Object[] GXv_Object9 ;
         GXv_Object9 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T1.[ContagemResultado_TipoRegistro], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T6.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T5.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Baseline], T4.[ContagemResultado_StatusDmn] AS ContagemResultado_StatusDmnVnc, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_NaoCnfDmnCod], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_Owner], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T6.[Contratada_AreaTrabalhoCod], COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T7.[ContagemResultado_DeflatorCnt], 0) AS ContagemResultado_DeflatorCnt, COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) AS ContagemResultado_EsforcoSoma, COALESCE( T7.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_Codigo] FROM ((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo]";
         scmdbuf = scmdbuf + " = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContagemResultado] T4 WITH (NOLOCK) ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada]) LEFT JOIN [Sistema] T5 WITH (NOLOCK) ON T5.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, MIN([ContagemResultado_Deflator]) AS ContagemResultado_DeflatorCnt, MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T7 ON T7.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultadoContagens_Esforco]) AS ContagemResultado_EsforcoSoma, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (Not ( @AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = 1 and @AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = 1 and @AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 = 1 and @AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = 1 and @AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = 1 and @AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 = 1 and @AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3)))";
         scmdbuf = scmdbuf + " and ((@AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and ((@AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_DeflatorCnt], 0) >= @AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt))";
         scmdbuf = scmdbuf + " and ((@AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_DeflatorCnt], 0) <= @AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to))";
         scmdbuf = scmdbuf + " and (@AV180ExtCount <= 0 or ( " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV180ExtraWWContagemResultadoOSDS_68_Tfcontagemresultado_statusultcnt_sels, "COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) IN (", ")") + "))";
         scmdbuf = scmdbuf + " and ((@AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) >= @AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima))";
         scmdbuf = scmdbuf + " and ((@AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <= @AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to))";
         scmdbuf = scmdbuf + " and ((@AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) >= @AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima))";
         scmdbuf = scmdbuf + " and ((@AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <= @AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to))";
         scmdbuf = scmdbuf + " and ((@AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) >= @AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima))";
         scmdbuf = scmdbuf + " and ((@AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) <= @AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to))";
         scmdbuf = scmdbuf + " and ((@AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) >= @AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima))";
         scmdbuf = scmdbuf + " and ((@AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to = convert(int, 0)) or ( COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) <= @AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to))";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod > 0 )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_AreaTrabalhoCod] = @AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod)";
         }
         else
         {
            GXv_int8[61] = 1;
         }
         if ( ( AV192Contratada_dousuario > Convert.ToDecimal( 0 )) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV192Contratada_dousuario)";
         }
         else
         {
            GXv_int8[62] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] = @AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int8[63] = 1;
            GXv_int8[64] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like @lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int8[65] = 1;
            GXv_int8[66] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int8[67] = 1;
            GXv_int8[68] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int8[69] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int8[70] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int8[71] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1) && ! (0==AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int8[72] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1)";
         }
         else
         {
            GXv_int8[73] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int8[74] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_StatusDmn] = @AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1)";
         }
         else
         {
            GXv_int8[75] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) > @AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int8[76] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) < @AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int8[77] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV116ExtraWWContagemResultadoOSDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) = @AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1)";
         }
         else
         {
            GXv_int8[78] = 1;
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV129ExtraWWContagemResultadoOSDS_17_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( ( StringUtil.StrCmp(AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int8[79] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] = @AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int8[80] = 1;
            GXv_int8[81] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like @lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int8[82] = 1;
            GXv_int8[83] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int8[84] = 1;
            GXv_int8[85] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int8[86] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int8[87] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int8[88] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2) && ! (0==AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int8[89] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2)";
         }
         else
         {
            GXv_int8[90] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int8[91] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_StatusDmn] = @AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2)";
         }
         else
         {
            GXv_int8[92] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) > @AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int8[93] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) < @AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int8[94] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV133ExtraWWContagemResultadoOSDS_21_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) = @AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2)";
         }
         else
         {
            GXv_int8[95] = 1;
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV146ExtraWWContagemResultadoOSDS_34_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int8[96] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] = @AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int8[97] = 1;
            GXv_int8[98] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like @lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int8[99] = 1;
            GXv_int8[100] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int8[101] = 1;
            GXv_int8[102] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int8[103] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int8[104] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int8[105] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ! (0==AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3) && ! (0==AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int8[106] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3)";
         }
         else
         {
            GXv_int8[107] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int8[108] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMNVNC") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_StatusDmn] = @AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3)";
         }
         else
         {
            GXv_int8[109] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) > @AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int8[110] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) < @AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int8[111] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_ESFORCOSOMA") == 0 ) && ( AV150ExtraWWContagemResultadoOSDS_38_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T8.[ContagemResultado_EsforcoSoma], 0) = @AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3)";
         }
         else
         {
            GXv_int8[112] = 1;
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV163ExtraWWContagemResultadoOSDS_51_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int8[113] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm)) ) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T1.[ContagemResultado_Demanda])) + CASE  WHEN (T1.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T1.[ContagemResultado_DemandaFM])) END) like @lV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm)";
         }
         else
         {
            GXv_int8[114] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel)) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T1.[ContagemResultado_Demanda])) + CASE  WHEN (T1.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T1.[ContagemResultado_DemandaFM])) END) = @AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel)";
         }
         else
         {
            GXv_int8[115] = 1;
         }
         if ( ! (DateTime.MinValue==AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int8[116] = 1;
         }
         if ( ! (DateTime.MinValue==AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int8[117] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] like @lV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla)";
         }
         else
         {
            GXv_int8[118] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] = @AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel)";
         }
         else
         {
            GXv_int8[119] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] like @lV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla)";
         }
         else
         {
            GXv_int8[120] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] = @AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel)";
         }
         else
         {
            GXv_int8[121] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int8[122] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int8[123] = 1;
         }
         if ( AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV179ExtraWWContagemResultadoOSDS_67_Tfcontagemresultado_statusdmn_sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV181ExtraWWContagemResultadoOSDS_69_Tfcontagemresultado_baseline_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 0)";
         }
         if ( AV54GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Servico_Sigla]";
         GXv_Object9[0] = scmdbuf;
         GXv_Object9[1] = GXv_int8;
         return GXv_Object9 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00S14(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (decimal)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (bool)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (String)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (bool)dynConstraints[33] , (String)dynConstraints[34] , (short)dynConstraints[35] , (String)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (String)dynConstraints[45] , (int)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (DateTime)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (String)dynConstraints[53] , (String)dynConstraints[54] , (String)dynConstraints[55] , (String)dynConstraints[56] , (int)dynConstraints[57] , (short)dynConstraints[58] , (int)dynConstraints[59] , (int)dynConstraints[60] , (int)dynConstraints[61] , (String)dynConstraints[62] , (String)dynConstraints[63] , (DateTime)dynConstraints[64] , (int)dynConstraints[65] , (int)dynConstraints[66] , (String)dynConstraints[67] , (int)dynConstraints[68] , (bool)dynConstraints[69] , (int)dynConstraints[70] , (String)dynConstraints[71] , (String)dynConstraints[72] , (String)dynConstraints[73] , (int)dynConstraints[74] , (DateTime)dynConstraints[75] , (DateTime)dynConstraints[76] , (DateTime)dynConstraints[77] , (int)dynConstraints[78] , (int)dynConstraints[79] , (int)dynConstraints[80] , (DateTime)dynConstraints[81] , (DateTime)dynConstraints[82] , (int)dynConstraints[83] , (DateTime)dynConstraints[84] , (DateTime)dynConstraints[85] , (int)dynConstraints[86] , (DateTime)dynConstraints[87] , (DateTime)dynConstraints[88] , (decimal)dynConstraints[89] , (decimal)dynConstraints[90] , (decimal)dynConstraints[91] , (int)dynConstraints[92] , (decimal)dynConstraints[93] , (decimal)dynConstraints[94] , (decimal)dynConstraints[95] , (decimal)dynConstraints[96] , (decimal)dynConstraints[97] , (decimal)dynConstraints[98] , (decimal)dynConstraints[99] , (decimal)dynConstraints[100] , (decimal)dynConstraints[101] , (decimal)dynConstraints[102] , (decimal)dynConstraints[103] , (decimal)dynConstraints[104] , (decimal)dynConstraints[105] , (decimal)dynConstraints[106] , (decimal)dynConstraints[107] , (short)dynConstraints[108] );
               case 1 :
                     return conditional_P00S17(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (decimal)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (bool)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (String)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (bool)dynConstraints[33] , (String)dynConstraints[34] , (short)dynConstraints[35] , (String)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (String)dynConstraints[45] , (int)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (DateTime)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (String)dynConstraints[53] , (String)dynConstraints[54] , (String)dynConstraints[55] , (String)dynConstraints[56] , (int)dynConstraints[57] , (short)dynConstraints[58] , (int)dynConstraints[59] , (int)dynConstraints[60] , (int)dynConstraints[61] , (String)dynConstraints[62] , (String)dynConstraints[63] , (DateTime)dynConstraints[64] , (int)dynConstraints[65] , (int)dynConstraints[66] , (String)dynConstraints[67] , (int)dynConstraints[68] , (bool)dynConstraints[69] , (int)dynConstraints[70] , (String)dynConstraints[71] , (String)dynConstraints[72] , (String)dynConstraints[73] , (int)dynConstraints[74] , (DateTime)dynConstraints[75] , (DateTime)dynConstraints[76] , (DateTime)dynConstraints[77] , (int)dynConstraints[78] , (int)dynConstraints[79] , (int)dynConstraints[80] , (DateTime)dynConstraints[81] , (DateTime)dynConstraints[82] , (int)dynConstraints[83] , (DateTime)dynConstraints[84] , (DateTime)dynConstraints[85] , (int)dynConstraints[86] , (DateTime)dynConstraints[87] , (DateTime)dynConstraints[88] , (decimal)dynConstraints[89] , (decimal)dynConstraints[90] , (decimal)dynConstraints[91] , (int)dynConstraints[92] , (decimal)dynConstraints[93] , (decimal)dynConstraints[94] , (decimal)dynConstraints[95] , (decimal)dynConstraints[96] , (decimal)dynConstraints[97] , (decimal)dynConstraints[98] , (decimal)dynConstraints[99] , (decimal)dynConstraints[100] , (decimal)dynConstraints[101] , (decimal)dynConstraints[102] , (decimal)dynConstraints[103] , (decimal)dynConstraints[104] , (decimal)dynConstraints[105] , (decimal)dynConstraints[106] , (decimal)dynConstraints[107] , (short)dynConstraints[108] );
               case 2 :
                     return conditional_P00S110(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (decimal)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (bool)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (String)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (bool)dynConstraints[33] , (String)dynConstraints[34] , (short)dynConstraints[35] , (String)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (String)dynConstraints[45] , (int)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (DateTime)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (String)dynConstraints[53] , (String)dynConstraints[54] , (String)dynConstraints[55] , (String)dynConstraints[56] , (int)dynConstraints[57] , (short)dynConstraints[58] , (int)dynConstraints[59] , (int)dynConstraints[60] , (int)dynConstraints[61] , (String)dynConstraints[62] , (String)dynConstraints[63] , (DateTime)dynConstraints[64] , (int)dynConstraints[65] , (int)dynConstraints[66] , (String)dynConstraints[67] , (int)dynConstraints[68] , (bool)dynConstraints[69] , (int)dynConstraints[70] , (String)dynConstraints[71] , (String)dynConstraints[72] , (String)dynConstraints[73] , (int)dynConstraints[74] , (DateTime)dynConstraints[75] , (DateTime)dynConstraints[76] , (DateTime)dynConstraints[77] , (int)dynConstraints[78] , (int)dynConstraints[79] , (int)dynConstraints[80] , (DateTime)dynConstraints[81] , (DateTime)dynConstraints[82] , (int)dynConstraints[83] , (DateTime)dynConstraints[84] , (DateTime)dynConstraints[85] , (int)dynConstraints[86] , (DateTime)dynConstraints[87] , (DateTime)dynConstraints[88] , (decimal)dynConstraints[89] , (decimal)dynConstraints[90] , (decimal)dynConstraints[91] , (int)dynConstraints[92] , (decimal)dynConstraints[93] , (decimal)dynConstraints[94] , (decimal)dynConstraints[95] , (decimal)dynConstraints[96] , (decimal)dynConstraints[97] , (decimal)dynConstraints[98] , (decimal)dynConstraints[99] , (decimal)dynConstraints[100] , (decimal)dynConstraints[101] , (decimal)dynConstraints[102] , (decimal)dynConstraints[103] , (decimal)dynConstraints[104] , (decimal)dynConstraints[105] , (decimal)dynConstraints[106] , (decimal)dynConstraints[107] , (short)dynConstraints[108] );
               case 3 :
                     return conditional_P00S113(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (decimal)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (bool)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (String)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (bool)dynConstraints[33] , (String)dynConstraints[34] , (short)dynConstraints[35] , (String)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (String)dynConstraints[45] , (int)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (DateTime)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (String)dynConstraints[53] , (String)dynConstraints[54] , (String)dynConstraints[55] , (String)dynConstraints[56] , (int)dynConstraints[57] , (short)dynConstraints[58] , (int)dynConstraints[59] , (int)dynConstraints[60] , (int)dynConstraints[61] , (String)dynConstraints[62] , (String)dynConstraints[63] , (DateTime)dynConstraints[64] , (int)dynConstraints[65] , (int)dynConstraints[66] , (String)dynConstraints[67] , (int)dynConstraints[68] , (bool)dynConstraints[69] , (int)dynConstraints[70] , (String)dynConstraints[71] , (String)dynConstraints[72] , (String)dynConstraints[73] , (int)dynConstraints[74] , (DateTime)dynConstraints[75] , (DateTime)dynConstraints[76] , (DateTime)dynConstraints[77] , (int)dynConstraints[78] , (int)dynConstraints[79] , (int)dynConstraints[80] , (DateTime)dynConstraints[81] , (DateTime)dynConstraints[82] , (int)dynConstraints[83] , (DateTime)dynConstraints[84] , (DateTime)dynConstraints[85] , (int)dynConstraints[86] , (DateTime)dynConstraints[87] , (DateTime)dynConstraints[88] , (decimal)dynConstraints[89] , (decimal)dynConstraints[90] , (decimal)dynConstraints[91] , (int)dynConstraints[92] , (decimal)dynConstraints[93] , (decimal)dynConstraints[94] , (decimal)dynConstraints[95] , (decimal)dynConstraints[96] , (decimal)dynConstraints[97] , (decimal)dynConstraints[98] , (decimal)dynConstraints[99] , (decimal)dynConstraints[100] , (decimal)dynConstraints[101] , (decimal)dynConstraints[102] , (decimal)dynConstraints[103] , (decimal)dynConstraints[104] , (decimal)dynConstraints[105] , (decimal)dynConstraints[106] , (decimal)dynConstraints[107] , (short)dynConstraints[108] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00S14 ;
          prmP00S14 = new Object[] {
          new Object[] {"@AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV180ExtCount",SqlDbType.Int,9,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV192Contratada_dousuario",SqlDbType.Decimal,10,2} ,
          new Object[] {"@AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0}
          } ;
          Object[] prmP00S17 ;
          prmP00S17 = new Object[] {
          new Object[] {"@AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV180ExtCount",SqlDbType.Int,9,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV192Contratada_dousuario",SqlDbType.Decimal,10,2} ,
          new Object[] {"@AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0}
          } ;
          Object[] prmP00S110 ;
          prmP00S110 = new Object[] {
          new Object[] {"@AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV180ExtCount",SqlDbType.Int,9,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV192Contratada_dousuario",SqlDbType.Decimal,10,2} ,
          new Object[] {"@AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0}
          } ;
          Object[] prmP00S113 ;
          prmP00S113 = new Object[] {
          new Object[] {"@AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV120ExtraWWContagemResultadoOSDS_8_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV121ExtraWWContagemResultadoOSDS_9_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV115ExtraWWContagemResultadoOSDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122ExtraWWContagemResultadoOSDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV137ExtraWWContagemResultadoOSDS_25_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV138ExtraWWContagemResultadoOSDS_26_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV131ExtraWWContagemResultadoOSDS_19_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV132ExtraWWContagemResultadoOSDS_20_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV139ExtraWWContagemResultadoOSDS_27_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV154ExtraWWContagemResultadoOSDS_42_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV155ExtraWWContagemResultadoOSDS_43_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV148ExtraWWContagemResultadoOSDS_36_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV149ExtraWWContagemResultadoOSDS_37_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV156ExtraWWContagemResultadoOSDS_44_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV169ExtraWWContagemResultadoOSDS_57_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV170ExtraWWContagemResultadoOSDS_58_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV177ExtraWWContagemResultadoOSDS_65_Tfcontagemresultado_deflatorcnt",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV178ExtraWWContagemResultadoOSDS_66_Tfcontagemresultado_deflatorcnt_to",SqlDbType.Decimal,6,3} ,
          new Object[] {"@AV180ExtCount",SqlDbType.Int,9,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV182ExtraWWContagemResultadoOSDS_70_Tfcontagemresultado_pfbfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV183ExtraWWContagemResultadoOSDS_71_Tfcontagemresultado_pfbfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV184ExtraWWContagemResultadoOSDS_72_Tfcontagemresultado_pflfsultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV185ExtraWWContagemResultadoOSDS_73_Tfcontagemresultado_pflfsultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV186ExtraWWContagemResultadoOSDS_74_Tfcontagemresultado_pfbfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV187ExtraWWContagemResultadoOSDS_75_Tfcontagemresultado_pfbfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV188ExtraWWContagemResultadoOSDS_76_Tfcontagemresultado_pflfmultima",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV189ExtraWWContagemResultadoOSDS_77_Tfcontagemresultado_pflfmultima_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV113ExtraWWContagemResultadoOSDS_1_Contratada_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV192Contratada_dousuario",SqlDbType.Decimal,10,2} ,
          new Object[] {"@AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV117ExtraWWContagemResultadoOSDS_5_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV118ExtraWWContagemResultadoOSDS_6_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV119ExtraWWContagemResultadoOSDS_7_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV123ExtraWWContagemResultadoOSDS_11_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV124ExtraWWContagemResultadoOSDS_12_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV125ExtraWWContagemResultadoOSDS_13_Contagemresultado_naocnfdmncod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV126ExtraWWContagemResultadoOSDS_14_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV127ExtraWWContagemResultadoOSDS_15_Contagemresultado_statusdmnvnc1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV128ExtraWWContagemResultadoOSDS_16_Contagemresultado_esforcosoma1",SqlDbType.Int,8,0} ,
          new Object[] {"@AV130ExtraWWContagemResultadoOSDS_18_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV134ExtraWWContagemResultadoOSDS_22_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV135ExtraWWContagemResultadoOSDS_23_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV136ExtraWWContagemResultadoOSDS_24_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV140ExtraWWContagemResultadoOSDS_28_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV141ExtraWWContagemResultadoOSDS_29_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV142ExtraWWContagemResultadoOSDS_30_Contagemresultado_naocnfdmncod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV143ExtraWWContagemResultadoOSDS_31_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV144ExtraWWContagemResultadoOSDS_32_Contagemresultado_statusdmnvnc2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV145ExtraWWContagemResultadoOSDS_33_Contagemresultado_esforcosoma2",SqlDbType.Int,8,0} ,
          new Object[] {"@AV147ExtraWWContagemResultadoOSDS_35_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV151ExtraWWContagemResultadoOSDS_39_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV152ExtraWWContagemResultadoOSDS_40_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV153ExtraWWContagemResultadoOSDS_41_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV157ExtraWWContagemResultadoOSDS_45_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV158ExtraWWContagemResultadoOSDS_46_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV159ExtraWWContagemResultadoOSDS_47_Contagemresultado_naocnfdmncod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV160ExtraWWContagemResultadoOSDS_48_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV161ExtraWWContagemResultadoOSDS_49_Contagemresultado_statusdmnvnc3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV162ExtraWWContagemResultadoOSDS_50_Contagemresultado_esforcosoma3",SqlDbType.Int,8,0} ,
          new Object[] {"@AV164ExtraWWContagemResultadoOSDS_52_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV165ExtraWWContagemResultadoOSDS_53_Tfcontagemresultado_osfsosfm",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV166ExtraWWContagemResultadoOSDS_54_Tfcontagemresultado_osfsosfm_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV167ExtraWWContagemResultadoOSDS_55_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV168ExtraWWContagemResultadoOSDS_56_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV171ExtraWWContagemResultadoOSDS_59_Tfcontagemrresultado_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV172ExtraWWContagemResultadoOSDS_60_Tfcontagemrresultado_sistemasigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV173ExtraWWContagemResultadoOSDS_61_Tfcontagemresultado_contratadasigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV174ExtraWWContagemResultadoOSDS_62_Tfcontagemresultado_contratadasigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV175ExtraWWContagemResultadoOSDS_63_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV176ExtraWWContagemResultadoOSDS_64_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00S14", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00S14,100,0,true,false )
             ,new CursorDef("P00S17", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00S17,100,0,true,false )
             ,new CursorDef("P00S110", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00S110,100,0,true,false )
             ,new CursorDef("P00S113", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00S113,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 25) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((bool[]) buf[13])[0] = rslt.getBool(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(14) ;
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((decimal[]) buf[29])[0] = rslt.getDecimal(17) ;
                ((decimal[]) buf[30])[0] = rslt.getDecimal(18) ;
                ((decimal[]) buf[31])[0] = rslt.getDecimal(19) ;
                ((decimal[]) buf[32])[0] = rslt.getDecimal(20) ;
                ((short[]) buf[33])[0] = rslt.getShort(21) ;
                ((decimal[]) buf[34])[0] = rslt.getDecimal(22) ;
                ((int[]) buf[35])[0] = rslt.getInt(23) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((int[]) buf[37])[0] = rslt.getInt(24) ;
                ((DateTime[]) buf[38])[0] = rslt.getGXDate(25) ;
                ((String[]) buf[39])[0] = rslt.getVarchar(26) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(26);
                ((String[]) buf[41])[0] = rslt.getVarchar(27) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(27);
                ((int[]) buf[43])[0] = rslt.getInt(28) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 25) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((bool[]) buf[13])[0] = rslt.getBool(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(14) ;
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((decimal[]) buf[29])[0] = rslt.getDecimal(17) ;
                ((decimal[]) buf[30])[0] = rslt.getDecimal(18) ;
                ((decimal[]) buf[31])[0] = rslt.getDecimal(19) ;
                ((decimal[]) buf[32])[0] = rslt.getDecimal(20) ;
                ((short[]) buf[33])[0] = rslt.getShort(21) ;
                ((decimal[]) buf[34])[0] = rslt.getDecimal(22) ;
                ((int[]) buf[35])[0] = rslt.getInt(23) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((int[]) buf[37])[0] = rslt.getInt(24) ;
                ((DateTime[]) buf[38])[0] = rslt.getGXDate(25) ;
                ((String[]) buf[39])[0] = rslt.getVarchar(26) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(26);
                ((String[]) buf[41])[0] = rslt.getVarchar(27) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(27);
                ((int[]) buf[43])[0] = rslt.getInt(28) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 25) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((bool[]) buf[13])[0] = rslt.getBool(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(14) ;
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((decimal[]) buf[29])[0] = rslt.getDecimal(17) ;
                ((decimal[]) buf[30])[0] = rslt.getDecimal(18) ;
                ((decimal[]) buf[31])[0] = rslt.getDecimal(19) ;
                ((decimal[]) buf[32])[0] = rslt.getDecimal(20) ;
                ((short[]) buf[33])[0] = rslt.getShort(21) ;
                ((decimal[]) buf[34])[0] = rslt.getDecimal(22) ;
                ((int[]) buf[35])[0] = rslt.getInt(23) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((int[]) buf[37])[0] = rslt.getInt(24) ;
                ((DateTime[]) buf[38])[0] = rslt.getGXDate(25) ;
                ((String[]) buf[39])[0] = rslt.getVarchar(26) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(26);
                ((String[]) buf[41])[0] = rslt.getVarchar(27) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(27);
                ((int[]) buf[43])[0] = rslt.getInt(28) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 25) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((bool[]) buf[13])[0] = rslt.getBool(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(14) ;
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((decimal[]) buf[29])[0] = rslt.getDecimal(17) ;
                ((decimal[]) buf[30])[0] = rslt.getDecimal(18) ;
                ((decimal[]) buf[31])[0] = rslt.getDecimal(19) ;
                ((decimal[]) buf[32])[0] = rslt.getDecimal(20) ;
                ((short[]) buf[33])[0] = rslt.getShort(21) ;
                ((decimal[]) buf[34])[0] = rslt.getDecimal(22) ;
                ((int[]) buf[35])[0] = rslt.getInt(23) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((int[]) buf[37])[0] = rslt.getInt(24) ;
                ((DateTime[]) buf[38])[0] = rslt.getGXDate(25) ;
                ((String[]) buf[39])[0] = rslt.getVarchar(26) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(26);
                ((String[]) buf[41])[0] = rslt.getVarchar(27) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(27);
                ((int[]) buf[43])[0] = rslt.getInt(28) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[124]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[125]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[126]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[127]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[128]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[129]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[130]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[131]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[132]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[133]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[134]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[135]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[136]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[137]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[138]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[139]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[140]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[141]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[142]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[143]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[144]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[145]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[146]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[147]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[148]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[149]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[150]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[151]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[152]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[153]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[154]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[155]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[156]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[157]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[158]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[159]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[160]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[161]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[162]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[163]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[164]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[165]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[166]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[167]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[168]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[169]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[170]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[171]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[172]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[173]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[174]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[175]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[176]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[177]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[178]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[179]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[180]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[181]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[182]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[183]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[184]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[185]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[186]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[187]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[188]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[189]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[190]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[191]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[192]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[193]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[194]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[195]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[196]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[197]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[198]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[199]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[200]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[201]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[202]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[203]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[204]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[205]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[206]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[207]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[208]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[209]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[210]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[211]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[212]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[213]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[214]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[215]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[216]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[217]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[218]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[219]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[220]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[221]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[222]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[223]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[224]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[225]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[226]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[227]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[228]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[229]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[230]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[231]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[232]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[233]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[234]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[235]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[236]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[237]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[238]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[239]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[240]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[241]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[242]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[243]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[244]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[245]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[246]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[247]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[124]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[125]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[126]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[127]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[128]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[129]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[130]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[131]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[132]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[133]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[134]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[135]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[136]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[137]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[138]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[139]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[140]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[141]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[142]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[143]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[144]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[145]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[146]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[147]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[148]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[149]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[150]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[151]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[152]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[153]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[154]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[155]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[156]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[157]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[158]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[159]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[160]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[161]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[162]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[163]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[164]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[165]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[166]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[167]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[168]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[169]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[170]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[171]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[172]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[173]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[174]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[175]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[176]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[177]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[178]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[179]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[180]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[181]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[182]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[183]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[184]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[185]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[186]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[187]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[188]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[189]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[190]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[191]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[192]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[193]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[194]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[195]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[196]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[197]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[198]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[199]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[200]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[201]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[202]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[203]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[204]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[205]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[206]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[207]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[208]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[209]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[210]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[211]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[212]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[213]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[214]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[215]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[216]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[217]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[218]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[219]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[220]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[221]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[222]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[223]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[224]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[225]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[226]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[227]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[228]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[229]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[230]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[231]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[232]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[233]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[234]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[235]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[236]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[237]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[238]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[239]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[240]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[241]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[242]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[243]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[244]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[245]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[246]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[247]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[124]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[125]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[126]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[127]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[128]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[129]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[130]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[131]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[132]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[133]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[134]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[135]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[136]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[137]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[138]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[139]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[140]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[141]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[142]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[143]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[144]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[145]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[146]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[147]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[148]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[149]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[150]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[151]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[152]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[153]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[154]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[155]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[156]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[157]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[158]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[159]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[160]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[161]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[162]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[163]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[164]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[165]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[166]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[167]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[168]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[169]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[170]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[171]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[172]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[173]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[174]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[175]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[176]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[177]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[178]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[179]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[180]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[181]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[182]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[183]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[184]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[185]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[186]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[187]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[188]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[189]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[190]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[191]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[192]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[193]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[194]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[195]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[196]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[197]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[198]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[199]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[200]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[201]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[202]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[203]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[204]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[205]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[206]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[207]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[208]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[209]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[210]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[211]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[212]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[213]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[214]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[215]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[216]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[217]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[218]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[219]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[220]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[221]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[222]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[223]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[224]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[225]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[226]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[227]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[228]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[229]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[230]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[231]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[232]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[233]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[234]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[235]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[236]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[237]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[238]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[239]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[240]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[241]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[242]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[243]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[244]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[245]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[246]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[247]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[124]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[125]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[126]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[127]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[128]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[129]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[130]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[131]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[132]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[133]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[134]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[135]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[136]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[137]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[138]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[139]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[140]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[141]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[142]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[143]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[144]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[145]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[146]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[147]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[148]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[149]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[150]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[151]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[152]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[153]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[154]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[155]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[156]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[157]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[158]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[159]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[160]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[161]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[162]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[163]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[164]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[165]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[166]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[167]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[168]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[169]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[170]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[171]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[172]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[173]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[174]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[175]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[176]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[177]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[178]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[179]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[180]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[181]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[182]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[183]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[184]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[185]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[186]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[187]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[188]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[189]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[190]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[191]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[192]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[193]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[194]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[195]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[196]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[197]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[198]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[199]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[200]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[201]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[202]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[203]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[204]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[205]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[206]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[207]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[208]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[209]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[210]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[211]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[212]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[213]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[214]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[215]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[216]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[217]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[218]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[219]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[220]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[221]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[222]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[223]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[224]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[225]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[226]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[227]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[228]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[229]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[230]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[231]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[232]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[233]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[234]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[235]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[236]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[237]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[238]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[239]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[240]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[241]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[242]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[243]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[244]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[245]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[246]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[247]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getextrawwcontagemresultadoosfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getextrawwcontagemresultadoosfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getextrawwcontagemresultadoosfilterdata") )
          {
             return  ;
          }
          getextrawwcontagemresultadoosfilterdata worker = new getextrawwcontagemresultadoosfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
