/*
               File: type_SdtGpoObjCtrlResponsavel
        Description: Gpo Obj Ctrl Responsavel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:30:1.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "GpoObjCtrlResponsavel" )]
   [XmlType(TypeName =  "GpoObjCtrlResponsavel" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtGpoObjCtrlResponsavel : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtGpoObjCtrlResponsavel( )
      {
         /* Constructor for serialization */
         gxTv_SdtGpoObjCtrlResponsavel_Mode = "";
      }

      public SdtGpoObjCtrlResponsavel( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV1837GpoObjCtrlResponsavel_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV1837GpoObjCtrlResponsavel_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"GpoObjCtrlResponsavel_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "GpoObjCtrlResponsavel");
         metadata.Set("BT", "GpoObjCtrlResponsavel");
         metadata.Set("PK", "[ \"GpoObjCtrlResponsavel_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"GpoObjCtrlResponsavel_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"ContratanteUsuario_ContratanteCod\",\"ContratanteUsuario_UsuarioCod\" ],\"FKMap\":[ \"GpoObjCtrlResponsavel_CteCteCod-ContratanteUsuario_ContratanteCod\",\"GpoObjCtrlResponsavel_CteUsrCod-ContratanteUsuario_UsuarioCod\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Gpoobjctrlresponsavel_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Gpoobjctrlresponsavel_gpocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Gpoobjctrlresponsavel_ctectecod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Gpoobjctrlresponsavel_cteusrcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Gpoobjctrlresponsavel_cteareacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Gpoobjctrlresponsavel_gpocod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Gpoobjctrlresponsavel_ctectecod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Gpoobjctrlresponsavel_cteusrcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Gpoobjctrlresponsavel_cteareacod_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtGpoObjCtrlResponsavel deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtGpoObjCtrlResponsavel)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtGpoObjCtrlResponsavel obj ;
         obj = this;
         obj.gxTpr_Gpoobjctrlresponsavel_codigo = deserialized.gxTpr_Gpoobjctrlresponsavel_codigo;
         obj.gxTpr_Gpoobjctrlresponsavel_gpocod = deserialized.gxTpr_Gpoobjctrlresponsavel_gpocod;
         obj.gxTpr_Gpoobjctrlresponsavel_ctectecod = deserialized.gxTpr_Gpoobjctrlresponsavel_ctectecod;
         obj.gxTpr_Gpoobjctrlresponsavel_cteusrcod = deserialized.gxTpr_Gpoobjctrlresponsavel_cteusrcod;
         obj.gxTpr_Gpoobjctrlresponsavel_cteareacod = deserialized.gxTpr_Gpoobjctrlresponsavel_cteareacod;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Gpoobjctrlresponsavel_codigo_Z = deserialized.gxTpr_Gpoobjctrlresponsavel_codigo_Z;
         obj.gxTpr_Gpoobjctrlresponsavel_gpocod_Z = deserialized.gxTpr_Gpoobjctrlresponsavel_gpocod_Z;
         obj.gxTpr_Gpoobjctrlresponsavel_ctectecod_Z = deserialized.gxTpr_Gpoobjctrlresponsavel_ctectecod_Z;
         obj.gxTpr_Gpoobjctrlresponsavel_cteusrcod_Z = deserialized.gxTpr_Gpoobjctrlresponsavel_cteusrcod_Z;
         obj.gxTpr_Gpoobjctrlresponsavel_cteareacod_Z = deserialized.gxTpr_Gpoobjctrlresponsavel_cteareacod_Z;
         obj.gxTpr_Gpoobjctrlresponsavel_gpocod_N = deserialized.gxTpr_Gpoobjctrlresponsavel_gpocod_N;
         obj.gxTpr_Gpoobjctrlresponsavel_ctectecod_N = deserialized.gxTpr_Gpoobjctrlresponsavel_ctectecod_N;
         obj.gxTpr_Gpoobjctrlresponsavel_cteusrcod_N = deserialized.gxTpr_Gpoobjctrlresponsavel_cteusrcod_N;
         obj.gxTpr_Gpoobjctrlresponsavel_cteareacod_N = deserialized.gxTpr_Gpoobjctrlresponsavel_cteareacod_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "GpoObjCtrlResponsavel_Codigo") )
               {
                  gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "GpoObjCtrlResponsavel_GpoCod") )
               {
                  gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "GpoObjCtrlResponsavel_CteCteCod") )
               {
                  gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "GpoObjCtrlResponsavel_CteUsrCod") )
               {
                  gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "GpoObjCtrlResponsavel_CteAreaCod") )
               {
                  gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtGpoObjCtrlResponsavel_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtGpoObjCtrlResponsavel_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "GpoObjCtrlResponsavel_Codigo_Z") )
               {
                  gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "GpoObjCtrlResponsavel_GpoCod_Z") )
               {
                  gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "GpoObjCtrlResponsavel_CteCteCod_Z") )
               {
                  gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "GpoObjCtrlResponsavel_CteUsrCod_Z") )
               {
                  gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "GpoObjCtrlResponsavel_CteAreaCod_Z") )
               {
                  gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "GpoObjCtrlResponsavel_GpoCod_N") )
               {
                  gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "GpoObjCtrlResponsavel_CteCteCod_N") )
               {
                  gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "GpoObjCtrlResponsavel_CteUsrCod_N") )
               {
                  gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "GpoObjCtrlResponsavel_CteAreaCod_N") )
               {
                  gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "GpoObjCtrlResponsavel";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("GpoObjCtrlResponsavel_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("GpoObjCtrlResponsavel_GpoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("GpoObjCtrlResponsavel_CteCteCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("GpoObjCtrlResponsavel_CteUsrCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("GpoObjCtrlResponsavel_CteAreaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtGpoObjCtrlResponsavel_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGpoObjCtrlResponsavel_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("GpoObjCtrlResponsavel_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("GpoObjCtrlResponsavel_GpoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("GpoObjCtrlResponsavel_CteCteCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("GpoObjCtrlResponsavel_CteUsrCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("GpoObjCtrlResponsavel_CteAreaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("GpoObjCtrlResponsavel_GpoCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("GpoObjCtrlResponsavel_CteCteCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("GpoObjCtrlResponsavel_CteUsrCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("GpoObjCtrlResponsavel_CteAreaCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("GpoObjCtrlResponsavel_Codigo", gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_codigo, false);
         AddObjectProperty("GpoObjCtrlResponsavel_GpoCod", gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod, false);
         AddObjectProperty("GpoObjCtrlResponsavel_CteCteCod", gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod, false);
         AddObjectProperty("GpoObjCtrlResponsavel_CteUsrCod", gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod, false);
         AddObjectProperty("GpoObjCtrlResponsavel_CteAreaCod", gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtGpoObjCtrlResponsavel_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtGpoObjCtrlResponsavel_Initialized, false);
            AddObjectProperty("GpoObjCtrlResponsavel_Codigo_Z", gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_codigo_Z, false);
            AddObjectProperty("GpoObjCtrlResponsavel_GpoCod_Z", gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_Z, false);
            AddObjectProperty("GpoObjCtrlResponsavel_CteCteCod_Z", gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_Z, false);
            AddObjectProperty("GpoObjCtrlResponsavel_CteUsrCod_Z", gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_Z, false);
            AddObjectProperty("GpoObjCtrlResponsavel_CteAreaCod_Z", gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_Z, false);
            AddObjectProperty("GpoObjCtrlResponsavel_GpoCod_N", gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_N, false);
            AddObjectProperty("GpoObjCtrlResponsavel_CteCteCod_N", gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_N, false);
            AddObjectProperty("GpoObjCtrlResponsavel_CteUsrCod_N", gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_N, false);
            AddObjectProperty("GpoObjCtrlResponsavel_CteAreaCod_N", gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "GpoObjCtrlResponsavel_Codigo" )]
      [  XmlElement( ElementName = "GpoObjCtrlResponsavel_Codigo"   )]
      public int gxTpr_Gpoobjctrlresponsavel_codigo
      {
         get {
            return gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_codigo ;
         }

         set {
            if ( gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_codigo != value )
            {
               gxTv_SdtGpoObjCtrlResponsavel_Mode = "INS";
               this.gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_codigo_Z_SetNull( );
               this.gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_Z_SetNull( );
               this.gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_Z_SetNull( );
               this.gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_Z_SetNull( );
               this.gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_Z_SetNull( );
            }
            gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "GpoObjCtrlResponsavel_GpoCod" )]
      [  XmlElement( ElementName = "GpoObjCtrlResponsavel_GpoCod"   )]
      public short gxTpr_Gpoobjctrlresponsavel_gpocod
      {
         get {
            return gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod ;
         }

         set {
            gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_N = 0;
            gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod = (short)(value);
         }

      }

      public void gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_SetNull( )
      {
         gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_N = 1;
         gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod = 0;
         return  ;
      }

      public bool gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "GpoObjCtrlResponsavel_CteCteCod" )]
      [  XmlElement( ElementName = "GpoObjCtrlResponsavel_CteCteCod"   )]
      public int gxTpr_Gpoobjctrlresponsavel_ctectecod
      {
         get {
            return gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod ;
         }

         set {
            gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_N = 0;
            gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod = (int)(value);
         }

      }

      public void gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_SetNull( )
      {
         gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_N = 1;
         gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod = 0;
         return  ;
      }

      public bool gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "GpoObjCtrlResponsavel_CteUsrCod" )]
      [  XmlElement( ElementName = "GpoObjCtrlResponsavel_CteUsrCod"   )]
      public int gxTpr_Gpoobjctrlresponsavel_cteusrcod
      {
         get {
            return gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod ;
         }

         set {
            gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_N = 0;
            gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod = (int)(value);
         }

      }

      public void gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_SetNull( )
      {
         gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_N = 1;
         gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod = 0;
         return  ;
      }

      public bool gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "GpoObjCtrlResponsavel_CteAreaCod" )]
      [  XmlElement( ElementName = "GpoObjCtrlResponsavel_CteAreaCod"   )]
      public int gxTpr_Gpoobjctrlresponsavel_cteareacod
      {
         get {
            return gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod ;
         }

         set {
            gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_N = 0;
            gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod = (int)(value);
         }

      }

      public void gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_SetNull( )
      {
         gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_N = 1;
         gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod = 0;
         return  ;
      }

      public bool gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtGpoObjCtrlResponsavel_Mode ;
         }

         set {
            gxTv_SdtGpoObjCtrlResponsavel_Mode = (String)(value);
         }

      }

      public void gxTv_SdtGpoObjCtrlResponsavel_Mode_SetNull( )
      {
         gxTv_SdtGpoObjCtrlResponsavel_Mode = "";
         return  ;
      }

      public bool gxTv_SdtGpoObjCtrlResponsavel_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtGpoObjCtrlResponsavel_Initialized ;
         }

         set {
            gxTv_SdtGpoObjCtrlResponsavel_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtGpoObjCtrlResponsavel_Initialized_SetNull( )
      {
         gxTv_SdtGpoObjCtrlResponsavel_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtGpoObjCtrlResponsavel_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "GpoObjCtrlResponsavel_Codigo_Z" )]
      [  XmlElement( ElementName = "GpoObjCtrlResponsavel_Codigo_Z"   )]
      public int gxTpr_Gpoobjctrlresponsavel_codigo_Z
      {
         get {
            return gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_codigo_Z ;
         }

         set {
            gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_codigo_Z_SetNull( )
      {
         gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "GpoObjCtrlResponsavel_GpoCod_Z" )]
      [  XmlElement( ElementName = "GpoObjCtrlResponsavel_GpoCod_Z"   )]
      public short gxTpr_Gpoobjctrlresponsavel_gpocod_Z
      {
         get {
            return gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_Z ;
         }

         set {
            gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_Z = (short)(value);
         }

      }

      public void gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_Z_SetNull( )
      {
         gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "GpoObjCtrlResponsavel_CteCteCod_Z" )]
      [  XmlElement( ElementName = "GpoObjCtrlResponsavel_CteCteCod_Z"   )]
      public int gxTpr_Gpoobjctrlresponsavel_ctectecod_Z
      {
         get {
            return gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_Z ;
         }

         set {
            gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_Z = (int)(value);
         }

      }

      public void gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_Z_SetNull( )
      {
         gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "GpoObjCtrlResponsavel_CteUsrCod_Z" )]
      [  XmlElement( ElementName = "GpoObjCtrlResponsavel_CteUsrCod_Z"   )]
      public int gxTpr_Gpoobjctrlresponsavel_cteusrcod_Z
      {
         get {
            return gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_Z ;
         }

         set {
            gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_Z_SetNull( )
      {
         gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "GpoObjCtrlResponsavel_CteAreaCod_Z" )]
      [  XmlElement( ElementName = "GpoObjCtrlResponsavel_CteAreaCod_Z"   )]
      public int gxTpr_Gpoobjctrlresponsavel_cteareacod_Z
      {
         get {
            return gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_Z ;
         }

         set {
            gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_Z_SetNull( )
      {
         gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "GpoObjCtrlResponsavel_GpoCod_N" )]
      [  XmlElement( ElementName = "GpoObjCtrlResponsavel_GpoCod_N"   )]
      public short gxTpr_Gpoobjctrlresponsavel_gpocod_N
      {
         get {
            return gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_N ;
         }

         set {
            gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_N = (short)(value);
         }

      }

      public void gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_N_SetNull( )
      {
         gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "GpoObjCtrlResponsavel_CteCteCod_N" )]
      [  XmlElement( ElementName = "GpoObjCtrlResponsavel_CteCteCod_N"   )]
      public short gxTpr_Gpoobjctrlresponsavel_ctectecod_N
      {
         get {
            return gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_N ;
         }

         set {
            gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_N = (short)(value);
         }

      }

      public void gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_N_SetNull( )
      {
         gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_N = 0;
         return  ;
      }

      public bool gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "GpoObjCtrlResponsavel_CteUsrCod_N" )]
      [  XmlElement( ElementName = "GpoObjCtrlResponsavel_CteUsrCod_N"   )]
      public short gxTpr_Gpoobjctrlresponsavel_cteusrcod_N
      {
         get {
            return gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_N ;
         }

         set {
            gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_N = (short)(value);
         }

      }

      public void gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_N_SetNull( )
      {
         gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "GpoObjCtrlResponsavel_CteAreaCod_N" )]
      [  XmlElement( ElementName = "GpoObjCtrlResponsavel_CteAreaCod_N"   )]
      public short gxTpr_Gpoobjctrlresponsavel_cteareacod_N
      {
         get {
            return gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_N ;
         }

         set {
            gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_N = (short)(value);
         }

      }

      public void gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_N_SetNull( )
      {
         gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_N = 0;
         return  ;
      }

      public bool gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtGpoObjCtrlResponsavel_Mode = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "gpoobjctrlresponsavel", "GeneXus.Programs.gpoobjctrlresponsavel_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod ;
      private short gxTv_SdtGpoObjCtrlResponsavel_Initialized ;
      private short gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_Z ;
      private short gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_gpocod_N ;
      private short gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_N ;
      private short gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_N ;
      private short gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_codigo ;
      private int gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod ;
      private int gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod ;
      private int gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod ;
      private int gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_codigo_Z ;
      private int gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_ctectecod_Z ;
      private int gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteusrcod_Z ;
      private int gxTv_SdtGpoObjCtrlResponsavel_Gpoobjctrlresponsavel_cteareacod_Z ;
      private String gxTv_SdtGpoObjCtrlResponsavel_Mode ;
      private String sTagName ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"GpoObjCtrlResponsavel", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtGpoObjCtrlResponsavel_RESTInterface : GxGenericCollectionItem<SdtGpoObjCtrlResponsavel>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtGpoObjCtrlResponsavel_RESTInterface( ) : base()
      {
      }

      public SdtGpoObjCtrlResponsavel_RESTInterface( SdtGpoObjCtrlResponsavel psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "GpoObjCtrlResponsavel_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Gpoobjctrlresponsavel_codigo
      {
         get {
            return sdt.gxTpr_Gpoobjctrlresponsavel_codigo ;
         }

         set {
            sdt.gxTpr_Gpoobjctrlresponsavel_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "GpoObjCtrlResponsavel_GpoCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Gpoobjctrlresponsavel_gpocod
      {
         get {
            return sdt.gxTpr_Gpoobjctrlresponsavel_gpocod ;
         }

         set {
            sdt.gxTpr_Gpoobjctrlresponsavel_gpocod = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "GpoObjCtrlResponsavel_CteCteCod" , Order = 2 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Gpoobjctrlresponsavel_ctectecod
      {
         get {
            return sdt.gxTpr_Gpoobjctrlresponsavel_ctectecod ;
         }

         set {
            sdt.gxTpr_Gpoobjctrlresponsavel_ctectecod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "GpoObjCtrlResponsavel_CteUsrCod" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Gpoobjctrlresponsavel_cteusrcod
      {
         get {
            return sdt.gxTpr_Gpoobjctrlresponsavel_cteusrcod ;
         }

         set {
            sdt.gxTpr_Gpoobjctrlresponsavel_cteusrcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "GpoObjCtrlResponsavel_CteAreaCod" , Order = 4 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Gpoobjctrlresponsavel_cteareacod
      {
         get {
            return sdt.gxTpr_Gpoobjctrlresponsavel_cteareacod ;
         }

         set {
            sdt.gxTpr_Gpoobjctrlresponsavel_cteareacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtGpoObjCtrlResponsavel sdt
      {
         get {
            return (SdtGpoObjCtrlResponsavel)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtGpoObjCtrlResponsavel() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 16 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
