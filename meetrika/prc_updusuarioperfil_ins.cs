/*
               File: PRC_UpdUsuarioPerfil_Ins
        Description: Upd Usuario Perfil_Ins
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:17.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updusuarioperfil_ins : GXProcedure
   {
      public prc_updusuarioperfil_ins( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updusuarioperfil_ins( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Usuario_Codigo ,
                           ref int aP1_Perfil_Codigo ,
                           bool aP2_UsuarioPerfil_Insert )
      {
         this.A1Usuario_Codigo = aP0_Usuario_Codigo;
         this.A3Perfil_Codigo = aP1_Perfil_Codigo;
         this.AV9UsuarioPerfil_Insert = aP2_UsuarioPerfil_Insert;
         initialize();
         executePrivate();
         aP0_Usuario_Codigo=this.A1Usuario_Codigo;
         aP1_Perfil_Codigo=this.A3Perfil_Codigo;
      }

      public void executeSubmit( ref int aP0_Usuario_Codigo ,
                                 ref int aP1_Perfil_Codigo ,
                                 bool aP2_UsuarioPerfil_Insert )
      {
         prc_updusuarioperfil_ins objprc_updusuarioperfil_ins;
         objprc_updusuarioperfil_ins = new prc_updusuarioperfil_ins();
         objprc_updusuarioperfil_ins.A1Usuario_Codigo = aP0_Usuario_Codigo;
         objprc_updusuarioperfil_ins.A3Perfil_Codigo = aP1_Perfil_Codigo;
         objprc_updusuarioperfil_ins.AV9UsuarioPerfil_Insert = aP2_UsuarioPerfil_Insert;
         objprc_updusuarioperfil_ins.context.SetSubmitInitialConfig(context);
         objprc_updusuarioperfil_ins.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updusuarioperfil_ins);
         aP0_Usuario_Codigo=this.A1Usuario_Codigo;
         aP1_Perfil_Codigo=this.A3Perfil_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updusuarioperfil_ins)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized UPDATE. */
         /* Using cursor P004S2 */
         pr_default.execute(0, new Object[] {n544UsuarioPerfil_Insert, AV9UsuarioPerfil_Insert, A1Usuario_Codigo, A3Perfil_Codigo});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("UsuarioPerfil") ;
         dsDefault.SmartCacheProvider.SetUpdated("UsuarioPerfil") ;
         /* End optimized UPDATE. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_UpdUsuarioPerfil_Ins");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updusuarioperfil_ins__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1Usuario_Codigo ;
      private int A3Perfil_Codigo ;
      private bool AV9UsuarioPerfil_Insert ;
      private bool n544UsuarioPerfil_Insert ;
      private bool A544UsuarioPerfil_Insert ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Usuario_Codigo ;
      private int aP1_Perfil_Codigo ;
      private IDataStoreProvider pr_default ;
   }

   public class prc_updusuarioperfil_ins__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004S2 ;
          prmP004S2 = new Object[] {
          new Object[] {"@UsuarioPerfil_Insert",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P004S2", "UPDATE [UsuarioPerfil] SET [UsuarioPerfil_Insert]=@UsuarioPerfil_Insert  WHERE [Usuario_Codigo] = @Usuario_Codigo and [Perfil_Codigo] = @Perfil_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004S2)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
       }
    }

 }

}
