/*
               File: PRC_DataHoraPorExtenso
        Description: PRC_DataHoraPorExtenso
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:48.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_datahoraporextenso : GXProcedure
   {
      public prc_datahoraporextenso( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_datahoraporextenso( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( short aP0_Tipo ,
                           out String aP1_DataExtenso )
      {
         this.AV9Tipo = aP0_Tipo;
         this.AV8DataExtenso = "" ;
         initialize();
         executePrivate();
         aP1_DataExtenso=this.AV8DataExtenso;
      }

      public String executeUdp( short aP0_Tipo )
      {
         this.AV9Tipo = aP0_Tipo;
         this.AV8DataExtenso = "" ;
         initialize();
         executePrivate();
         aP1_DataExtenso=this.AV8DataExtenso;
         return AV8DataExtenso ;
      }

      public void executeSubmit( short aP0_Tipo ,
                                 out String aP1_DataExtenso )
      {
         prc_datahoraporextenso objprc_datahoraporextenso;
         objprc_datahoraporextenso = new prc_datahoraporextenso();
         objprc_datahoraporextenso.AV9Tipo = aP0_Tipo;
         objprc_datahoraporextenso.AV8DataExtenso = "" ;
         objprc_datahoraporextenso.context.SetSubmitInitialConfig(context);
         objprc_datahoraporextenso.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_datahoraporextenso);
         aP1_DataExtenso=this.AV8DataExtenso;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_datahoraporextenso)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( AV9Tipo == 1 )
         {
            AV8DataExtenso = "Impresso em: " + StringUtil.PadL( StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( Gx_date)), 10, 0)), 2, "0") + " de " + StringUtil.Trim( DateTimeUtil.CMonth( Gx_date, "por")) + " de " + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( Gx_date)), 10, 0)) + " �s " + Gx_time;
         }
         else if ( AV9Tipo == 2 )
         {
            AV8DataExtenso = "Data: " + StringUtil.PadL( StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( Gx_date)), 10, 0)), 2, "0") + "/" + StringUtil.PadL( StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( Gx_date)), 10, 0)), 2, "0") + "/" + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( Gx_date)), 10, 0)) + " | Hora: " + StringUtil.Substring( Gx_time, 1, 5) + " |";
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gx_date = DateTime.MinValue;
         Gx_time = "";
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short AV9Tipo ;
      private String Gx_time ;
      private DateTime Gx_date ;
      private String AV8DataExtenso ;
      private String aP1_DataExtenso ;
   }

}
