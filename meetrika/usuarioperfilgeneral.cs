/*
               File: UsuarioPerfilGeneral
        Description: Usuario Perfil General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:57.61
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class usuarioperfilgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public usuarioperfilgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public usuarioperfilgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Usuario_Codigo ,
                           int aP1_Perfil_Codigo )
      {
         this.A1Usuario_Codigo = aP0_Usuario_Codigo;
         this.A3Perfil_Codigo = aP1_Perfil_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbPerfil_Tipo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
                  A3Perfil_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1Usuario_Codigo,(int)A3Perfil_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA902( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "UsuarioPerfilGeneral";
               context.Gx_err = 0;
               /* Using cursor H00902 */
               pr_default.execute(0, new Object[] {A1Usuario_Codigo});
               A57Usuario_PessoaCod = H00902_A57Usuario_PessoaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
               A2Usuario_Nome = H00902_A2Usuario_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2Usuario_Nome", A2Usuario_Nome);
               n2Usuario_Nome = H00902_n2Usuario_Nome[0];
               pr_default.close(0);
               /* Using cursor H00903 */
               pr_default.execute(1, new Object[] {A57Usuario_PessoaCod});
               A58Usuario_PessoaNom = H00903_A58Usuario_PessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
               n58Usuario_PessoaNom = H00903_n58Usuario_PessoaNom[0];
               pr_default.close(1);
               /* Using cursor H00904 */
               pr_default.execute(2, new Object[] {A3Perfil_Codigo});
               A329Perfil_GamId = H00904_A329Perfil_GamId[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A329Perfil_GamId", StringUtil.LTrim( StringUtil.Str( (decimal)(A329Perfil_GamId), 12, 0)));
               A275Perfil_Tipo = H00904_A275Perfil_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A275Perfil_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)));
               A4Perfil_Nome = H00904_A4Perfil_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A4Perfil_Nome", A4Perfil_Nome);
               pr_default.close(2);
               WS902( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Usuario Perfil General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117135774");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("usuarioperfilgeneral.aspx") + "?" + UrlEncode("" +A1Usuario_Codigo) + "," + UrlEncode("" +A3Perfil_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1Usuario_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA3Perfil_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA3Perfil_Codigo), 6, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm902( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("usuarioperfilgeneral.js", "?20203117135775");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "UsuarioPerfilGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Usuario Perfil General" ;
      }

      protected void WB900( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "usuarioperfilgeneral.aspx");
            }
            wb_table1_2_902( true) ;
         }
         else
         {
            wb_table1_2_902( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_902e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtUsuario_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_UsuarioPerfilGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPerfil_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPerfil_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtPerfil_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_UsuarioPerfilGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_PessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A57Usuario_PessoaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_PessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtUsuario_PessoaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_UsuarioPerfilGeneral.htm");
         }
         wbLoad = true;
      }

      protected void START902( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Usuario Perfil General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP900( ) ;
            }
         }
      }

      protected void WS902( )
      {
         START902( ) ;
         EVT902( ) ;
      }

      protected void EVT902( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP900( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP900( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11902 */
                                    E11902 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP900( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12902 */
                                    E12902 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP900( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13902 */
                                    E13902 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP900( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14902 */
                                    E14902 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP900( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP900( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE902( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm902( ) ;
            }
         }
      }

      protected void PA902( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbPerfil_Tipo.Name = "PERFIL_TIPO";
            cmbPerfil_Tipo.WebTags = "";
            cmbPerfil_Tipo.addItem("0", "Desconhecido", 0);
            cmbPerfil_Tipo.addItem("1", "Auditor", 0);
            cmbPerfil_Tipo.addItem("2", "Contador", 0);
            cmbPerfil_Tipo.addItem("3", "Contratada", 0);
            cmbPerfil_Tipo.addItem("4", "Contratante", 0);
            cmbPerfil_Tipo.addItem("5", "Financeiro", 0);
            cmbPerfil_Tipo.addItem("6", "Usuario", 0);
            cmbPerfil_Tipo.addItem("10", "Licensiado", 0);
            cmbPerfil_Tipo.addItem("99", "Administrador GAM", 0);
            if ( cmbPerfil_Tipo.ItemCount > 0 )
            {
               A275Perfil_Tipo = (short)(NumberUtil.Val( cmbPerfil_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A275Perfil_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbPerfil_Tipo.ItemCount > 0 )
         {
            A275Perfil_Tipo = (short)(NumberUtil.Val( cmbPerfil_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A275Perfil_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF902( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "UsuarioPerfilGeneral";
         context.Gx_err = 0;
      }

      protected void RF902( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00905 */
            pr_default.execute(3, new Object[] {A1Usuario_Codigo, A3Perfil_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               /* Execute user event: E12902 */
               E12902 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
            WB900( ) ;
         }
      }

      protected void STRUP900( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "UsuarioPerfilGeneral";
         context.Gx_err = 0;
         /* Using cursor H00906 */
         pr_default.execute(4, new Object[] {A1Usuario_Codigo});
         A57Usuario_PessoaCod = H00906_A57Usuario_PessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
         A2Usuario_Nome = H00906_A2Usuario_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2Usuario_Nome", A2Usuario_Nome);
         n2Usuario_Nome = H00906_n2Usuario_Nome[0];
         pr_default.close(4);
         /* Using cursor H00907 */
         pr_default.execute(5, new Object[] {A57Usuario_PessoaCod});
         A58Usuario_PessoaNom = H00907_A58Usuario_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
         n58Usuario_PessoaNom = H00907_n58Usuario_PessoaNom[0];
         pr_default.close(5);
         /* Using cursor H00908 */
         pr_default.execute(6, new Object[] {A3Perfil_Codigo});
         A329Perfil_GamId = H00908_A329Perfil_GamId[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A329Perfil_GamId", StringUtil.LTrim( StringUtil.Str( (decimal)(A329Perfil_GamId), 12, 0)));
         A275Perfil_Tipo = H00908_A275Perfil_Tipo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A275Perfil_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)));
         A4Perfil_Nome = H00908_A4Perfil_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A4Perfil_Nome", A4Perfil_Nome);
         pr_default.close(6);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11902 */
         E11902 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A2Usuario_Nome = StringUtil.Upper( cgiGet( edtUsuario_Nome_Internalname));
            n2Usuario_Nome = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2Usuario_Nome", A2Usuario_Nome);
            A58Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtUsuario_PessoaNom_Internalname));
            n58Usuario_PessoaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
            A4Perfil_Nome = StringUtil.Upper( cgiGet( edtPerfil_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A4Perfil_Nome", A4Perfil_Nome);
            cmbPerfil_Tipo.CurrentValue = cgiGet( cmbPerfil_Tipo_Internalname);
            A275Perfil_Tipo = (short)(NumberUtil.Val( cgiGet( cmbPerfil_Tipo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A275Perfil_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)));
            A329Perfil_GamId = (long)(context.localUtil.CToN( cgiGet( edtPerfil_GamId_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A329Perfil_GamId", StringUtil.LTrim( StringUtil.Str( (decimal)(A329Perfil_GamId), 12, 0)));
            A57Usuario_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtUsuario_PessoaCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A57Usuario_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A57Usuario_PessoaCod), 6, 0)));
            /* Read saved values. */
            wcpOA1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1Usuario_Codigo"), ",", "."));
            wcpOA3Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA3Perfil_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11902 */
         E11902 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11902( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12902( )
      {
         /* Load Routine */
         edtUsuario_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtUsuario_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Codigo_Visible), 5, 0)));
         edtPerfil_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPerfil_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPerfil_Codigo_Visible), 5, 0)));
         edtUsuario_PessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtUsuario_PessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_PessoaCod_Visible), 5, 0)));
      }

      protected void E13902( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("usuarioperfil.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1Usuario_Codigo) + "," + UrlEncode("" +A3Perfil_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14902( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("usuarioperfil.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1Usuario_Codigo) + "," + UrlEncode("" +A3Perfil_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = false;
         AV9TrnContext.gxTpr_Callerurl = AV12HTTPRequest.ScriptName+"?"+AV12HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "UsuarioPerfil";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Usuario_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Usuario_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Perfil_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV8Perfil_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV11Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_902( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_902( true) ;
         }
         else
         {
            wb_table2_8_902( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_902e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_38_902( true) ;
         }
         else
         {
            wb_table3_38_902( false) ;
         }
         return  ;
      }

      protected void wb_table3_38_902e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_902e( true) ;
         }
         else
         {
            wb_table1_2_902e( false) ;
         }
      }

      protected void wb_table3_38_902( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_UsuarioPerfilGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_UsuarioPerfilGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_38_902e( true) ;
         }
         else
         {
            wb_table3_38_902e( false) ;
         }
      }

      protected void wb_table2_8_902( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_nome_Internalname, "Usu�rio", "", "", lblTextblockusuario_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioPerfilGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_Nome_Internalname, StringUtil.RTrim( A2Usuario_Nome), StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_UsuarioPerfilGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_pessoanom_Internalname, "Pessoa", "", "", lblTextblockusuario_pessoanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioPerfilGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_PessoaNom_Internalname, StringUtil.RTrim( A58Usuario_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A58Usuario_PessoaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_PessoaNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_UsuarioPerfilGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockperfil_nome_Internalname, "Perfil", "", "", lblTextblockperfil_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioPerfilGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPerfil_Nome_Internalname, StringUtil.RTrim( A4Perfil_Nome), StringUtil.RTrim( context.localUtil.Format( A4Perfil_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPerfil_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_UsuarioPerfilGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockperfil_tipo_Internalname, "Tipo do Perfil", "", "", lblTextblockperfil_tipo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioPerfilGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbPerfil_Tipo, cmbPerfil_Tipo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)), 1, cmbPerfil_Tipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 1, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_UsuarioPerfilGeneral.htm");
            cmbPerfil_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbPerfil_Tipo_Internalname, "Values", (String)(cmbPerfil_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockperfil_gamid_Internalname, "Gam Id", "", "", lblTextblockperfil_gamid_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_UsuarioPerfilGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPerfil_GamId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A329Perfil_GamId), 12, 0, ",", "")), context.localUtil.Format( (decimal)(A329Perfil_GamId), "ZZZZZZZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPerfil_GamId_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "GAMKeyNumLong", "right", false, "HLP_UsuarioPerfilGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_902e( true) ;
         }
         else
         {
            wb_table2_8_902e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1Usuario_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         A3Perfil_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA902( ) ;
         WS902( ) ;
         WE902( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1Usuario_Codigo = (String)((String)getParm(obj,0));
         sCtrlA3Perfil_Codigo = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA902( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "usuarioperfilgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA902( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1Usuario_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
            A3Perfil_Codigo = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
         }
         wcpOA1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1Usuario_Codigo"), ",", "."));
         wcpOA3Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA3Perfil_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1Usuario_Codigo != wcpOA1Usuario_Codigo ) || ( A3Perfil_Codigo != wcpOA3Perfil_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA1Usuario_Codigo = A1Usuario_Codigo;
         wcpOA3Perfil_Codigo = A3Perfil_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1Usuario_Codigo = cgiGet( sPrefix+"A1Usuario_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA1Usuario_Codigo) > 0 )
         {
            A1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA1Usuario_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
         }
         else
         {
            A1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1Usuario_Codigo_PARM"), ",", "."));
         }
         sCtrlA3Perfil_Codigo = cgiGet( sPrefix+"A3Perfil_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA3Perfil_Codigo) > 0 )
         {
            A3Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA3Perfil_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
         }
         else
         {
            A3Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A3Perfil_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA902( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS902( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS902( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1Usuario_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1Usuario_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1Usuario_Codigo_CTRL", StringUtil.RTrim( sCtrlA1Usuario_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A3Perfil_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA3Perfil_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A3Perfil_Codigo_CTRL", StringUtil.RTrim( sCtrlA3Perfil_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE902( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117135818");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("usuarioperfilgeneral.js", "?20203117135818");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockusuario_nome_Internalname = sPrefix+"TEXTBLOCKUSUARIO_NOME";
         edtUsuario_Nome_Internalname = sPrefix+"USUARIO_NOME";
         lblTextblockusuario_pessoanom_Internalname = sPrefix+"TEXTBLOCKUSUARIO_PESSOANOM";
         edtUsuario_PessoaNom_Internalname = sPrefix+"USUARIO_PESSOANOM";
         lblTextblockperfil_nome_Internalname = sPrefix+"TEXTBLOCKPERFIL_NOME";
         edtPerfil_Nome_Internalname = sPrefix+"PERFIL_NOME";
         lblTextblockperfil_tipo_Internalname = sPrefix+"TEXTBLOCKPERFIL_TIPO";
         cmbPerfil_Tipo_Internalname = sPrefix+"PERFIL_TIPO";
         lblTextblockperfil_gamid_Internalname = sPrefix+"TEXTBLOCKPERFIL_GAMID";
         edtPerfil_GamId_Internalname = sPrefix+"PERFIL_GAMID";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtUsuario_Codigo_Internalname = sPrefix+"USUARIO_CODIGO";
         edtPerfil_Codigo_Internalname = sPrefix+"PERFIL_CODIGO";
         edtUsuario_PessoaCod_Internalname = sPrefix+"USUARIO_PESSOACOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtPerfil_GamId_Jsonclick = "";
         cmbPerfil_Tipo_Jsonclick = "";
         edtPerfil_Nome_Jsonclick = "";
         edtUsuario_PessoaNom_Jsonclick = "";
         edtUsuario_Nome_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         edtUsuario_PessoaCod_Jsonclick = "";
         edtUsuario_PessoaCod_Visible = 1;
         edtPerfil_Codigo_Jsonclick = "";
         edtPerfil_Codigo_Visible = 1;
         edtUsuario_Codigo_Jsonclick = "";
         edtUsuario_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13902',iparms:[{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14902',iparms:[{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         scmdbuf = "";
         H00902_A57Usuario_PessoaCod = new int[1] ;
         H00902_A2Usuario_Nome = new String[] {""} ;
         H00902_n2Usuario_Nome = new bool[] {false} ;
         A2Usuario_Nome = "";
         H00903_A58Usuario_PessoaNom = new String[] {""} ;
         H00903_n58Usuario_PessoaNom = new bool[] {false} ;
         A58Usuario_PessoaNom = "";
         H00904_A329Perfil_GamId = new long[1] ;
         H00904_A275Perfil_Tipo = new short[1] ;
         H00904_A4Perfil_Nome = new String[] {""} ;
         A4Perfil_Nome = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         H00905_A1Usuario_Codigo = new int[1] ;
         H00905_A3Perfil_Codigo = new int[1] ;
         H00905_A57Usuario_PessoaCod = new int[1] ;
         H00905_A329Perfil_GamId = new long[1] ;
         H00905_A275Perfil_Tipo = new short[1] ;
         H00905_A4Perfil_Nome = new String[] {""} ;
         H00905_A58Usuario_PessoaNom = new String[] {""} ;
         H00905_n58Usuario_PessoaNom = new bool[] {false} ;
         H00905_A2Usuario_Nome = new String[] {""} ;
         H00905_n2Usuario_Nome = new bool[] {false} ;
         H00906_A57Usuario_PessoaCod = new int[1] ;
         H00906_A2Usuario_Nome = new String[] {""} ;
         H00906_n2Usuario_Nome = new bool[] {false} ;
         H00907_A58Usuario_PessoaNom = new String[] {""} ;
         H00907_n58Usuario_PessoaNom = new bool[] {false} ;
         H00908_A329Perfil_GamId = new long[1] ;
         H00908_A275Perfil_Tipo = new short[1] ;
         H00908_A4Perfil_Nome = new String[] {""} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV12HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV11Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockusuario_nome_Jsonclick = "";
         lblTextblockusuario_pessoanom_Jsonclick = "";
         lblTextblockperfil_nome_Jsonclick = "";
         lblTextblockperfil_tipo_Jsonclick = "";
         lblTextblockperfil_gamid_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1Usuario_Codigo = "";
         sCtrlA3Perfil_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.usuarioperfilgeneral__default(),
            new Object[][] {
                new Object[] {
               H00902_A57Usuario_PessoaCod, H00902_A2Usuario_Nome, H00902_n2Usuario_Nome
               }
               , new Object[] {
               H00903_A58Usuario_PessoaNom, H00903_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00904_A329Perfil_GamId, H00904_A275Perfil_Tipo, H00904_A4Perfil_Nome
               }
               , new Object[] {
               H00905_A1Usuario_Codigo, H00905_A3Perfil_Codigo, H00905_A57Usuario_PessoaCod, H00905_A329Perfil_GamId, H00905_A275Perfil_Tipo, H00905_A4Perfil_Nome, H00905_A58Usuario_PessoaNom, H00905_n58Usuario_PessoaNom, H00905_A2Usuario_Nome, H00905_n2Usuario_Nome
               }
               , new Object[] {
               H00906_A57Usuario_PessoaCod, H00906_A2Usuario_Nome, H00906_n2Usuario_Nome
               }
               , new Object[] {
               H00907_A58Usuario_PessoaNom, H00907_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00908_A329Perfil_GamId, H00908_A275Perfil_Tipo, H00908_A4Perfil_Nome
               }
            }
         );
         AV15Pgmname = "UsuarioPerfilGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "UsuarioPerfilGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A275Perfil_Tipo ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A1Usuario_Codigo ;
      private int A3Perfil_Codigo ;
      private int wcpOA1Usuario_Codigo ;
      private int wcpOA3Perfil_Codigo ;
      private int A57Usuario_PessoaCod ;
      private int edtUsuario_Codigo_Visible ;
      private int edtPerfil_Codigo_Visible ;
      private int edtUsuario_PessoaCod_Visible ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7Usuario_Codigo ;
      private int AV8Perfil_Codigo ;
      private int idxLst ;
      private long A329Perfil_GamId ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String scmdbuf ;
      private String A2Usuario_Nome ;
      private String A58Usuario_PessoaNom ;
      private String A4Perfil_Nome ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtUsuario_Codigo_Internalname ;
      private String edtUsuario_Codigo_Jsonclick ;
      private String edtPerfil_Codigo_Internalname ;
      private String edtPerfil_Codigo_Jsonclick ;
      private String edtUsuario_PessoaCod_Internalname ;
      private String edtUsuario_PessoaCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtUsuario_Nome_Internalname ;
      private String edtUsuario_PessoaNom_Internalname ;
      private String edtPerfil_Nome_Internalname ;
      private String cmbPerfil_Tipo_Internalname ;
      private String edtPerfil_GamId_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockusuario_nome_Internalname ;
      private String lblTextblockusuario_nome_Jsonclick ;
      private String edtUsuario_Nome_Jsonclick ;
      private String lblTextblockusuario_pessoanom_Internalname ;
      private String lblTextblockusuario_pessoanom_Jsonclick ;
      private String edtUsuario_PessoaNom_Jsonclick ;
      private String lblTextblockperfil_nome_Internalname ;
      private String lblTextblockperfil_nome_Jsonclick ;
      private String edtPerfil_Nome_Jsonclick ;
      private String lblTextblockperfil_tipo_Internalname ;
      private String lblTextblockperfil_tipo_Jsonclick ;
      private String cmbPerfil_Tipo_Jsonclick ;
      private String lblTextblockperfil_gamid_Internalname ;
      private String lblTextblockperfil_gamid_Jsonclick ;
      private String edtPerfil_GamId_Jsonclick ;
      private String sCtrlA1Usuario_Codigo ;
      private String sCtrlA3Perfil_Codigo ;
      private bool entryPointCalled ;
      private bool n2Usuario_Nome ;
      private bool n58Usuario_PessoaNom ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbPerfil_Tipo ;
      private IDataStoreProvider pr_default ;
      private int[] H00902_A57Usuario_PessoaCod ;
      private String[] H00902_A2Usuario_Nome ;
      private bool[] H00902_n2Usuario_Nome ;
      private String[] H00903_A58Usuario_PessoaNom ;
      private bool[] H00903_n58Usuario_PessoaNom ;
      private long[] H00904_A329Perfil_GamId ;
      private short[] H00904_A275Perfil_Tipo ;
      private String[] H00904_A4Perfil_Nome ;
      private int[] H00905_A1Usuario_Codigo ;
      private int[] H00905_A3Perfil_Codigo ;
      private int[] H00905_A57Usuario_PessoaCod ;
      private long[] H00905_A329Perfil_GamId ;
      private short[] H00905_A275Perfil_Tipo ;
      private String[] H00905_A4Perfil_Nome ;
      private String[] H00905_A58Usuario_PessoaNom ;
      private bool[] H00905_n58Usuario_PessoaNom ;
      private String[] H00905_A2Usuario_Nome ;
      private bool[] H00905_n2Usuario_Nome ;
      private int[] H00906_A57Usuario_PessoaCod ;
      private String[] H00906_A2Usuario_Nome ;
      private bool[] H00906_n2Usuario_Nome ;
      private String[] H00907_A58Usuario_PessoaNom ;
      private bool[] H00907_n58Usuario_PessoaNom ;
      private long[] H00908_A329Perfil_GamId ;
      private short[] H00908_A275Perfil_Tipo ;
      private String[] H00908_A4Perfil_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV12HTTPRequest ;
      private IGxSession AV11Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
   }

   public class usuarioperfilgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00902 ;
          prmH00902 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00903 ;
          prmH00903 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00904 ;
          prmH00904 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00905 ;
          prmH00905 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00906 ;
          prmH00906 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00907 ;
          prmH00907 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00908 ;
          prmH00908 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00902", "SELECT [Usuario_PessoaCod] AS Usuario_PessoaCod, [Usuario_Nome] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00902,1,0,true,true )
             ,new CursorDef("H00903", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00903,1,0,true,true )
             ,new CursorDef("H00904", "SELECT [Perfil_GamId], [Perfil_Tipo], [Perfil_Nome] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00904,1,0,true,true )
             ,new CursorDef("H00905", "SELECT T1.[Usuario_Codigo], T1.[Perfil_Codigo], T2.[Usuario_PessoaCod] AS Usuario_PessoaCod, T4.[Perfil_GamId], T4.[Perfil_Tipo], T4.[Perfil_Nome], T3.[Pessoa_Nome] AS Usuario_PessoaNom, T2.[Usuario_Nome] FROM ((([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Usuario_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Perfil] T4 WITH (NOLOCK) ON T4.[Perfil_Codigo] = T1.[Perfil_Codigo]) WHERE T1.[Usuario_Codigo] = @Usuario_Codigo and T1.[Perfil_Codigo] = @Perfil_Codigo ORDER BY T1.[Usuario_Codigo], T1.[Perfil_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00905,1,0,true,true )
             ,new CursorDef("H00906", "SELECT [Usuario_PessoaCod] AS Usuario_PessoaCod, [Usuario_Nome] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00906,1,0,true,true )
             ,new CursorDef("H00907", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00907,1,0,true,true )
             ,new CursorDef("H00908", "SELECT [Perfil_GamId], [Perfil_Tipo], [Perfil_Nome] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00908,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((long[]) buf[3])[0] = rslt.getLong(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                ((String[]) buf[8])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
