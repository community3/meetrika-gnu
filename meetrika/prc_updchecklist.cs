/*
               File: PRC_UpdCheckList
        Description: Update Check List
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:14:37.25
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updchecklist : GXProcedure
   {
      public prc_updchecklist( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updchecklist( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           int aP1_ContagemResultado_OSVinculada ,
                           int aP2_ContagemResultado_NaoCnfDmnCod ,
                           int aP3_ContagemResultado_NaoCnfCntCod ,
                           int aP4_User_Codigo ,
                           int aP5_Contratada_Codigo ,
                           int aP6_Servico_Codigo ,
                           ref short aP7_Etapa )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV11ContagemResultado_OSVinculada = aP1_ContagemResultado_OSVinculada;
         this.AV10ContagemResultado_NaoCnfDmnCod = aP2_ContagemResultado_NaoCnfDmnCod;
         this.AV9ContagemResultado_NaoCnfCntCod = aP3_ContagemResultado_NaoCnfCntCod;
         this.AV23User_Codigo = aP4_User_Codigo;
         this.AV16Contratada_Codigo = aP5_Contratada_Codigo;
         this.AV20Servico_Codigo = aP6_Servico_Codigo;
         this.AV17Etapa = aP7_Etapa;
         initialize();
         executePrivate();
         aP7_Etapa=this.AV17Etapa;
      }

      public short executeUdp( int aP0_ContagemResultado_Codigo ,
                               int aP1_ContagemResultado_OSVinculada ,
                               int aP2_ContagemResultado_NaoCnfDmnCod ,
                               int aP3_ContagemResultado_NaoCnfCntCod ,
                               int aP4_User_Codigo ,
                               int aP5_Contratada_Codigo ,
                               int aP6_Servico_Codigo )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV11ContagemResultado_OSVinculada = aP1_ContagemResultado_OSVinculada;
         this.AV10ContagemResultado_NaoCnfDmnCod = aP2_ContagemResultado_NaoCnfDmnCod;
         this.AV9ContagemResultado_NaoCnfCntCod = aP3_ContagemResultado_NaoCnfCntCod;
         this.AV23User_Codigo = aP4_User_Codigo;
         this.AV16Contratada_Codigo = aP5_Contratada_Codigo;
         this.AV20Servico_Codigo = aP6_Servico_Codigo;
         this.AV17Etapa = aP7_Etapa;
         initialize();
         executePrivate();
         aP7_Etapa=this.AV17Etapa;
         return AV17Etapa ;
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 int aP1_ContagemResultado_OSVinculada ,
                                 int aP2_ContagemResultado_NaoCnfDmnCod ,
                                 int aP3_ContagemResultado_NaoCnfCntCod ,
                                 int aP4_User_Codigo ,
                                 int aP5_Contratada_Codigo ,
                                 int aP6_Servico_Codigo ,
                                 ref short aP7_Etapa )
      {
         prc_updchecklist objprc_updchecklist;
         objprc_updchecklist = new prc_updchecklist();
         objprc_updchecklist.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_updchecklist.AV11ContagemResultado_OSVinculada = aP1_ContagemResultado_OSVinculada;
         objprc_updchecklist.AV10ContagemResultado_NaoCnfDmnCod = aP2_ContagemResultado_NaoCnfDmnCod;
         objprc_updchecklist.AV9ContagemResultado_NaoCnfCntCod = aP3_ContagemResultado_NaoCnfCntCod;
         objprc_updchecklist.AV23User_Codigo = aP4_User_Codigo;
         objprc_updchecklist.AV16Contratada_Codigo = aP5_Contratada_Codigo;
         objprc_updchecklist.AV20Servico_Codigo = aP6_Servico_Codigo;
         objprc_updchecklist.AV17Etapa = aP7_Etapa;
         objprc_updchecklist.context.SetSubmitInitialConfig(context);
         objprc_updchecklist.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updchecklist);
         aP7_Etapa=this.AV17Etapa;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updchecklist)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV18SDT_CheckList.FromXml(AV24WebSession.Get("CheckList"), "");
         AV24WebSession.Remove("CheckList");
         AV25ChckLst_Completo = true;
         AV31GXV1 = 1;
         while ( AV31GXV1 <= AV18SDT_CheckList.Count )
         {
            AV19SDT_Item = ((SdtSDT_CheckList_Item)AV18SDT_CheckList.Item(AV31GXV1));
            if ( StringUtil.StrCmp(StringUtil.Substring( AV19SDT_Item.gxTpr_Descricao, 1, 1), StringUtil.Trim( StringUtil.Str( (decimal)(AV17Etapa), 4, 0))) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19SDT_Item.gxTpr_Cumpre)) || ( StringUtil.StrCmp(AV19SDT_Item.gxTpr_Cumpre, "N") == 0 ) )
               {
                  AV25ChckLst_Completo = false;
                  if ( StringUtil.StrCmp(AV19SDT_Item.gxTpr_Cumpre, "N") == 0 )
                  {
                     /* Execute user subroutine: 'REGISTRALOG' */
                     S121 ();
                     if ( returnInSub )
                     {
                        this.cleanup();
                        if (true) return;
                     }
                  }
               }
               /* Using cursor P005H2 */
               pr_default.execute(0, new Object[] {AV8ContagemResultado_Codigo, AV19SDT_Item.gxTpr_Codigo});
               while ( (pr_default.getStatus(0) != 101) )
               {
                  A758CheckList_Codigo = P005H2_A758CheckList_Codigo[0];
                  A1868ContagemResultadoChckLst_OSCod = P005H2_A1868ContagemResultadoChckLst_OSCod[0];
                  A762ContagemResultadoChckLst_Cumpre = P005H2_A762ContagemResultadoChckLst_Cumpre[0];
                  A761ContagemResultadoChckLst_Codigo = P005H2_A761ContagemResultadoChckLst_Codigo[0];
                  A762ContagemResultadoChckLst_Cumpre = AV19SDT_Item.gxTpr_Cumpre;
                  BatchSize = 50;
                  pr_default.initializeBatch( 1, BatchSize, this, "Executebatchp005h3");
                  /* Using cursor P005H3 */
                  pr_default.addRecord(1, new Object[] {A762ContagemResultadoChckLst_Cumpre, A761ContagemResultadoChckLst_Codigo});
                  if ( pr_default.recordCount(1) == pr_default.getBatchSize(1) )
                  {
                     Executebatchp005h3( ) ;
                  }
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoChckLst") ;
                  pr_default.readNext(0);
               }
               if ( pr_default.getBatchSize(1) > 0 )
               {
                  Executebatchp005h3( ) ;
               }
               pr_default.close(0);
            }
            AV31GXV1 = (int)(AV31GXV1+1);
         }
         if ( AV25ChckLst_Completo )
         {
            AV22StatusDmnVnc = "";
            if ( AV17Etapa == 1 )
            {
               /* Using cursor P005H4 */
               pr_default.execute(2, new Object[] {AV8ContagemResultado_Codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A456ContagemResultado_Codigo = P005H4_A456ContagemResultado_Codigo[0];
                  A484ContagemResultado_StatusDmn = P005H4_A484ContagemResultado_StatusDmn[0];
                  n484ContagemResultado_StatusDmn = P005H4_n484ContagemResultado_StatusDmn[0];
                  A468ContagemResultado_NaoCnfDmnCod = P005H4_A468ContagemResultado_NaoCnfDmnCod[0];
                  n468ContagemResultado_NaoCnfDmnCod = P005H4_n468ContagemResultado_NaoCnfDmnCod[0];
                  A484ContagemResultado_StatusDmn = "A";
                  n484ContagemResultado_StatusDmn = false;
                  if ( (0==AV10ContagemResultado_NaoCnfDmnCod) )
                  {
                     AV22StatusDmnVnc = "S";
                  }
                  else
                  {
                     A468ContagemResultado_NaoCnfDmnCod = AV10ContagemResultado_NaoCnfDmnCod;
                     n468ContagemResultado_NaoCnfDmnCod = false;
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  /* Using cursor P005H5 */
                  pr_default.execute(3, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, A456ContagemResultado_Codigo});
                  pr_default.close(3);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                  if (true) break;
                  /* Using cursor P005H6 */
                  pr_default.execute(4, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, A456ContagemResultado_Codigo});
                  pr_default.close(4);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
               AV28Observacao = "Check list 1� etapa.";
            }
            if ( AV17Etapa == 2 )
            {
               if ( (0==AV9ContagemResultado_NaoCnfCntCod) )
               {
                  /* Using cursor P005H7 */
                  pr_default.execute(5, new Object[] {AV8ContagemResultado_Codigo});
                  while ( (pr_default.getStatus(5) != 101) )
                  {
                     A1553ContagemResultado_CntSrvCod = P005H7_A1553ContagemResultado_CntSrvCod[0];
                     n1553ContagemResultado_CntSrvCod = P005H7_n1553ContagemResultado_CntSrvCod[0];
                     A456ContagemResultado_Codigo = P005H7_A456ContagemResultado_Codigo[0];
                     A484ContagemResultado_StatusDmn = P005H7_A484ContagemResultado_StatusDmn[0];
                     n484ContagemResultado_StatusDmn = P005H7_n484ContagemResultado_StatusDmn[0];
                     A1596ContagemResultado_CntSrvPrc = P005H7_A1596ContagemResultado_CntSrvPrc[0];
                     n1596ContagemResultado_CntSrvPrc = P005H7_n1596ContagemResultado_CntSrvPrc[0];
                     A1596ContagemResultado_CntSrvPrc = P005H7_A1596ContagemResultado_CntSrvPrc[0];
                     n1596ContagemResultado_CntSrvPrc = P005H7_n1596ContagemResultado_CntSrvPrc[0];
                     A484ContagemResultado_StatusDmn = "R";
                     n484ContagemResultado_StatusDmn = false;
                     AV21Servico_Percentual = A1596ContagemResultado_CntSrvPrc;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     /* Using cursor P005H8 */
                     pr_default.execute(6, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                     if (true) break;
                     /* Using cursor P005H9 */
                     pr_default.execute(7, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  pr_default.close(5);
                  AV22StatusDmnVnc = "R";
               }
               /* Using cursor P005H10 */
               pr_default.execute(8, new Object[] {AV11ContagemResultado_OSVinculada});
               while ( (pr_default.getStatus(8) != 101) )
               {
                  A517ContagemResultado_Ultima = P005H10_A517ContagemResultado_Ultima[0];
                  A456ContagemResultado_Codigo = P005H10_A456ContagemResultado_Codigo[0];
                  A458ContagemResultado_PFBFS = P005H10_A458ContagemResultado_PFBFS[0];
                  n458ContagemResultado_PFBFS = P005H10_n458ContagemResultado_PFBFS[0];
                  A459ContagemResultado_PFLFS = P005H10_A459ContagemResultado_PFLFS[0];
                  n459ContagemResultado_PFLFS = P005H10_n459ContagemResultado_PFLFS[0];
                  A460ContagemResultado_PFBFM = P005H10_A460ContagemResultado_PFBFM[0];
                  n460ContagemResultado_PFBFM = P005H10_n460ContagemResultado_PFBFM[0];
                  A473ContagemResultado_DataCnt = P005H10_A473ContagemResultado_DataCnt[0];
                  A511ContagemResultado_HoraCnt = P005H10_A511ContagemResultado_HoraCnt[0];
                  AV13ContagemResultado_PFBFS = A458ContagemResultado_PFBFS;
                  AV15ContagemResultado_PFLFS = A459ContagemResultado_PFLFS;
                  AV12ContagemResultado_PFBFM = A460ContagemResultado_PFBFM;
                  AV14ContagemResultado_PFLFM = (decimal)(A460ContagemResultado_PFBFM*AV21Servico_Percentual/ (decimal)(100));
                  A517ContagemResultado_Ultima = false;
                  /* Execute user subroutine: 'CONTAGEMDOSRV' */
                  S111 ();
                  if ( returnInSub )
                  {
                     pr_default.close(8);
                     this.cleanup();
                     if (true) return;
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  /* Using cursor P005H11 */
                  pr_default.execute(9, new Object[] {A517ContagemResultado_Ultima, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                  pr_default.close(9);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                  if (true) break;
                  /* Using cursor P005H12 */
                  pr_default.execute(10, new Object[] {A517ContagemResultado_Ultima, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                  pr_default.readNext(8);
               }
               pr_default.close(8);
               /*
                  INSERT RECORD ON TABLE ContagemResultadoChckLstLog

               */
               A814ContagemResultadoChckLstLog_DataHora = DateTimeUtil.ServerNow( context, "DEFAULT");
               A811ContagemResultadoChckLstLog_ChckLstCod = 0;
               n811ContagemResultadoChckLstLog_ChckLstCod = false;
               n811ContagemResultadoChckLstLog_ChckLstCod = true;
               A822ContagemResultadoChckLstLog_UsuarioCod = AV23User_Codigo;
               A824ContagemResultadoChckLstLog_Etapa = AV17Etapa;
               n824ContagemResultadoChckLstLog_Etapa = false;
               /* Using cursor P005H13 */
               pr_default.execute(11, new Object[] {A814ContagemResultadoChckLstLog_DataHora, n811ContagemResultadoChckLstLog_ChckLstCod, A811ContagemResultadoChckLstLog_ChckLstCod, A822ContagemResultadoChckLstLog_UsuarioCod, n824ContagemResultadoChckLstLog_Etapa, A824ContagemResultadoChckLstLog_Etapa});
               A820ContagemResultadoChckLstLog_Codigo = P005H13_A820ContagemResultadoChckLstLog_Codigo[0];
               pr_default.close(11);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoChckLstLog") ;
               if ( (pr_default.getStatus(11) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
               AV28Observacao = "Check list 2� etapa.";
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22StatusDmnVnc)) )
            {
               /* Using cursor P005H14 */
               pr_default.execute(12, new Object[] {AV11ContagemResultado_OSVinculada});
               while ( (pr_default.getStatus(12) != 101) )
               {
                  A456ContagemResultado_Codigo = P005H14_A456ContagemResultado_Codigo[0];
                  A472ContagemResultado_DataEntrega = P005H14_A472ContagemResultado_DataEntrega[0];
                  n472ContagemResultado_DataEntrega = P005H14_n472ContagemResultado_DataEntrega[0];
                  A912ContagemResultado_HoraEntrega = P005H14_A912ContagemResultado_HoraEntrega[0];
                  n912ContagemResultado_HoraEntrega = P005H14_n912ContagemResultado_HoraEntrega[0];
                  A484ContagemResultado_StatusDmn = P005H14_A484ContagemResultado_StatusDmn[0];
                  n484ContagemResultado_StatusDmn = P005H14_n484ContagemResultado_StatusDmn[0];
                  AV27PrazoEntrega = DateTimeUtil.ResetTime( A472ContagemResultado_DataEntrega ) ;
                  AV27PrazoEntrega = DateTimeUtil.TAdd( AV27PrazoEntrega, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
                  AV27PrazoEntrega = DateTimeUtil.TAdd( AV27PrazoEntrega, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
                  AV26StatusDmnAnt = A484ContagemResultado_StatusDmn;
                  A484ContagemResultado_StatusDmn = AV22StatusDmnVnc;
                  n484ContagemResultado_StatusDmn = false;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  /* Using cursor P005H15 */
                  pr_default.execute(13, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
                  pr_default.close(13);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                  if (true) break;
                  /* Using cursor P005H16 */
                  pr_default.execute(14, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
                  pr_default.close(14);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(12);
               new prc_inslogresponsavel(context ).execute( ref  AV11ContagemResultado_OSVinculada,  0,  "F",  "D",  AV23User_Codigo,  0,  AV26StatusDmnAnt,  AV22StatusDmnVnc,  AV28Observacao,  AV27PrazoEntrega,  true) ;
            }
         }
         else
         {
            if ( AV17Etapa == 1 )
            {
               /* Optimized UPDATE. */
               /* Using cursor P005H17 */
               pr_default.execute(15, new Object[] {AV8ContagemResultado_Codigo});
               pr_default.close(15);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               /* End optimized UPDATE. */
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'CONTAGEMDOSRV' Routine */
         /*
            INSERT RECORD ON TABLE ContagemResultadoContagens

         */
         A456ContagemResultado_Codigo = AV8ContagemResultado_Codigo;
         A473ContagemResultado_DataCnt = DateTimeUtil.ServerDate( context, "DEFAULT");
         A511ContagemResultado_HoraCnt = Gx_time;
         A458ContagemResultado_PFBFS = AV13ContagemResultado_PFBFS;
         n458ContagemResultado_PFBFS = false;
         A459ContagemResultado_PFLFS = AV15ContagemResultado_PFLFS;
         n459ContagemResultado_PFLFS = false;
         A460ContagemResultado_PFBFM = AV12ContagemResultado_PFBFM;
         n460ContagemResultado_PFBFM = false;
         A461ContagemResultado_PFLFM = AV14ContagemResultado_PFLFM;
         n461ContagemResultado_PFLFM = false;
         A800ContagemResultado_Deflator = AV21Servico_Percentual;
         n800ContagemResultado_Deflator = false;
         A470ContagemResultado_ContadorFMCod = AV23User_Codigo;
         A517ContagemResultado_Ultima = true;
         if ( (0==AV9ContagemResultado_NaoCnfCntCod) )
         {
            A469ContagemResultado_NaoCnfCntCod = 0;
            n469ContagemResultado_NaoCnfCntCod = false;
            n469ContagemResultado_NaoCnfCntCod = true;
            A483ContagemResultado_StatusCnt = 5;
         }
         else
         {
            A469ContagemResultado_NaoCnfCntCod = AV9ContagemResultado_NaoCnfCntCod;
            n469ContagemResultado_NaoCnfCntCod = false;
            A483ContagemResultado_StatusCnt = 6;
         }
         A482ContagemResultadoContagens_Esforco = 0;
         /* Using cursor P005H18 */
         pr_default.execute(16, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator});
         pr_default.close(16);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
         if ( (pr_default.getStatus(16) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
      }

      protected void S121( )
      {
         /* 'REGISTRALOG' Routine */
         /* Using cursor P005H19 */
         pr_default.execute(17, new Object[] {AV19SDT_Item.gxTpr_Codigo});
         while ( (pr_default.getStatus(17) != 101) )
         {
            A758CheckList_Codigo = P005H19_A758CheckList_Codigo[0];
            A1846CheckList_NaoCnfCod = P005H19_A1846CheckList_NaoCnfCod[0];
            n1846CheckList_NaoCnfCod = P005H19_n1846CheckList_NaoCnfCod[0];
            AV19SDT_Item.gxTpr_Naoconformidade = A1846CheckList_NaoCnfCod;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(17);
      }

      protected void Executebatchp005h3( )
      {
         /* Using cursor P005H3 */
         pr_default.executeBatch(1);
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_UpdCheckList");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         AV18SDT_CheckList = new GxObjectCollection( context, "SDT_CheckList.Item", "GxEv3Up14_Meetrika", "SdtSDT_CheckList_Item", "GeneXus.Programs");
         AV24WebSession = context.GetSession();
         AV19SDT_Item = new SdtSDT_CheckList_Item(context);
         scmdbuf = "";
         P005H2_A758CheckList_Codigo = new int[1] ;
         P005H2_A1868ContagemResultadoChckLst_OSCod = new int[1] ;
         P005H2_A762ContagemResultadoChckLst_Cumpre = new String[] {""} ;
         P005H2_A761ContagemResultadoChckLst_Codigo = new short[1] ;
         A762ContagemResultadoChckLst_Cumpre = "";
         P005H3_A762ContagemResultadoChckLst_Cumpre = new String[] {""} ;
         P005H3_A761ContagemResultadoChckLst_Codigo = new short[1] ;
         AV22StatusDmnVnc = "";
         P005H4_A456ContagemResultado_Codigo = new int[1] ;
         P005H4_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P005H4_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P005H4_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P005H4_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         A484ContagemResultado_StatusDmn = "";
         AV28Observacao = "";
         P005H7_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P005H7_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P005H7_A456ContagemResultado_Codigo = new int[1] ;
         P005H7_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P005H7_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P005H7_A1596ContagemResultado_CntSrvPrc = new decimal[1] ;
         P005H7_n1596ContagemResultado_CntSrvPrc = new bool[] {false} ;
         P005H10_A517ContagemResultado_Ultima = new bool[] {false} ;
         P005H10_A456ContagemResultado_Codigo = new int[1] ;
         P005H10_A458ContagemResultado_PFBFS = new decimal[1] ;
         P005H10_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P005H10_A459ContagemResultado_PFLFS = new decimal[1] ;
         P005H10_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P005H10_A460ContagemResultado_PFBFM = new decimal[1] ;
         P005H10_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P005H10_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P005H10_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         A814ContagemResultadoChckLstLog_DataHora = (DateTime)(DateTime.MinValue);
         P005H13_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         Gx_emsg = "";
         P005H14_A456ContagemResultado_Codigo = new int[1] ;
         P005H14_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P005H14_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P005H14_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P005H14_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P005H14_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P005H14_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         AV27PrazoEntrega = (DateTime)(DateTime.MinValue);
         AV26StatusDmnAnt = "";
         Gx_time = "";
         P005H19_A758CheckList_Codigo = new int[1] ;
         P005H19_A1846CheckList_NaoCnfCod = new int[1] ;
         P005H19_n1846CheckList_NaoCnfCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updchecklist__default(),
            new Object[][] {
                new Object[] {
               P005H2_A758CheckList_Codigo, P005H2_A1868ContagemResultadoChckLst_OSCod, P005H2_A762ContagemResultadoChckLst_Cumpre, P005H2_A761ContagemResultadoChckLst_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               P005H4_A456ContagemResultado_Codigo, P005H4_A484ContagemResultado_StatusDmn, P005H4_n484ContagemResultado_StatusDmn, P005H4_A468ContagemResultado_NaoCnfDmnCod, P005H4_n468ContagemResultado_NaoCnfDmnCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P005H7_A1553ContagemResultado_CntSrvCod, P005H7_n1553ContagemResultado_CntSrvCod, P005H7_A456ContagemResultado_Codigo, P005H7_A484ContagemResultado_StatusDmn, P005H7_n484ContagemResultado_StatusDmn, P005H7_A1596ContagemResultado_CntSrvPrc, P005H7_n1596ContagemResultado_CntSrvPrc
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P005H10_A517ContagemResultado_Ultima, P005H10_A456ContagemResultado_Codigo, P005H10_A458ContagemResultado_PFBFS, P005H10_n458ContagemResultado_PFBFS, P005H10_A459ContagemResultado_PFLFS, P005H10_n459ContagemResultado_PFLFS, P005H10_A460ContagemResultado_PFBFM, P005H10_n460ContagemResultado_PFBFM, P005H10_A473ContagemResultado_DataCnt, P005H10_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P005H13_A820ContagemResultadoChckLstLog_Codigo
               }
               , new Object[] {
               P005H14_A456ContagemResultado_Codigo, P005H14_A472ContagemResultado_DataEntrega, P005H14_n472ContagemResultado_DataEntrega, P005H14_A912ContagemResultado_HoraEntrega, P005H14_n912ContagemResultado_HoraEntrega, P005H14_A484ContagemResultado_StatusDmn, P005H14_n484ContagemResultado_StatusDmn
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P005H19_A758CheckList_Codigo, P005H19_A1846CheckList_NaoCnfCod, P005H19_n1846CheckList_NaoCnfCod
               }
            }
         );
         Gx_time = context.localUtil.Time( );
         /* GeneXus formulas. */
         Gx_time = context.localUtil.Time( );
         context.Gx_err = 0;
      }

      private short AV17Etapa ;
      private short A761ContagemResultadoChckLst_Codigo ;
      private short A824ContagemResultadoChckLstLog_Etapa ;
      private short A483ContagemResultado_StatusCnt ;
      private short A482ContagemResultadoContagens_Esforco ;
      private int AV8ContagemResultado_Codigo ;
      private int AV11ContagemResultado_OSVinculada ;
      private int AV10ContagemResultado_NaoCnfDmnCod ;
      private int AV9ContagemResultado_NaoCnfCntCod ;
      private int AV23User_Codigo ;
      private int AV16Contratada_Codigo ;
      private int AV20Servico_Codigo ;
      private int AV31GXV1 ;
      private int A758CheckList_Codigo ;
      private int A1868ContagemResultadoChckLst_OSCod ;
      private int BatchSize ;
      private int A456ContagemResultado_Codigo ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int GX_INS207 ;
      private int A811ContagemResultadoChckLstLog_ChckLstCod ;
      private int A822ContagemResultadoChckLstLog_UsuarioCod ;
      private int A820ContagemResultadoChckLstLog_Codigo ;
      private int GX_INS72 ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int A1846CheckList_NaoCnfCod ;
      private decimal A1596ContagemResultado_CntSrvPrc ;
      private decimal AV21Servico_Percentual ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal AV13ContagemResultado_PFBFS ;
      private decimal AV15ContagemResultado_PFLFS ;
      private decimal AV12ContagemResultado_PFBFM ;
      private decimal AV14ContagemResultado_PFLFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A800ContagemResultado_Deflator ;
      private String scmdbuf ;
      private String A762ContagemResultadoChckLst_Cumpre ;
      private String AV22StatusDmnVnc ;
      private String A484ContagemResultado_StatusDmn ;
      private String A511ContagemResultado_HoraCnt ;
      private String Gx_emsg ;
      private String AV26StatusDmnAnt ;
      private String Gx_time ;
      private DateTime A814ContagemResultadoChckLstLog_DataHora ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime AV27PrazoEntrega ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool AV25ChckLst_Completo ;
      private bool returnInSub ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1596ContagemResultado_CntSrvPrc ;
      private bool A517ContagemResultado_Ultima ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n811ContagemResultadoChckLstLog_ChckLstCod ;
      private bool n824ContagemResultadoChckLstLog_Etapa ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n800ContagemResultado_Deflator ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool n1846CheckList_NaoCnfCod ;
      private String AV28Observacao ;
      private IGxSession AV24WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private short aP7_Etapa ;
      private IDataStoreProvider pr_default ;
      private int[] P005H2_A758CheckList_Codigo ;
      private int[] P005H2_A1868ContagemResultadoChckLst_OSCod ;
      private String[] P005H2_A762ContagemResultadoChckLst_Cumpre ;
      private short[] P005H2_A761ContagemResultadoChckLst_Codigo ;
      private String[] P005H3_A762ContagemResultadoChckLst_Cumpre ;
      private short[] P005H3_A761ContagemResultadoChckLst_Codigo ;
      private int[] P005H4_A456ContagemResultado_Codigo ;
      private String[] P005H4_A484ContagemResultado_StatusDmn ;
      private bool[] P005H4_n484ContagemResultado_StatusDmn ;
      private int[] P005H4_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P005H4_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] P005H7_A1553ContagemResultado_CntSrvCod ;
      private bool[] P005H7_n1553ContagemResultado_CntSrvCod ;
      private int[] P005H7_A456ContagemResultado_Codigo ;
      private String[] P005H7_A484ContagemResultado_StatusDmn ;
      private bool[] P005H7_n484ContagemResultado_StatusDmn ;
      private decimal[] P005H7_A1596ContagemResultado_CntSrvPrc ;
      private bool[] P005H7_n1596ContagemResultado_CntSrvPrc ;
      private bool[] P005H10_A517ContagemResultado_Ultima ;
      private int[] P005H10_A456ContagemResultado_Codigo ;
      private decimal[] P005H10_A458ContagemResultado_PFBFS ;
      private bool[] P005H10_n458ContagemResultado_PFBFS ;
      private decimal[] P005H10_A459ContagemResultado_PFLFS ;
      private bool[] P005H10_n459ContagemResultado_PFLFS ;
      private decimal[] P005H10_A460ContagemResultado_PFBFM ;
      private bool[] P005H10_n460ContagemResultado_PFBFM ;
      private DateTime[] P005H10_A473ContagemResultado_DataCnt ;
      private String[] P005H10_A511ContagemResultado_HoraCnt ;
      private int[] P005H13_A820ContagemResultadoChckLstLog_Codigo ;
      private int[] P005H14_A456ContagemResultado_Codigo ;
      private DateTime[] P005H14_A472ContagemResultado_DataEntrega ;
      private bool[] P005H14_n472ContagemResultado_DataEntrega ;
      private DateTime[] P005H14_A912ContagemResultado_HoraEntrega ;
      private bool[] P005H14_n912ContagemResultado_HoraEntrega ;
      private String[] P005H14_A484ContagemResultado_StatusDmn ;
      private bool[] P005H14_n484ContagemResultado_StatusDmn ;
      private int[] P005H19_A758CheckList_Codigo ;
      private int[] P005H19_A1846CheckList_NaoCnfCod ;
      private bool[] P005H19_n1846CheckList_NaoCnfCod ;
      [ObjectCollection(ItemType=typeof( SdtSDT_CheckList_Item ))]
      private IGxCollection AV18SDT_CheckList ;
      private SdtSDT_CheckList_Item AV19SDT_Item ;
   }

   public class prc_updchecklist__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new BatchUpdateCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new ForEachCursor(def[17])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP005H2 ;
          prmP005H2 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19SDT_Item__Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005H3 ;
          prmP005H3 = new Object[] {
          new Object[] {"@ContagemResultadoChckLst_Cumpre",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultadoChckLst_Codigo",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP005H4 ;
          prmP005H4 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005H5 ;
          prmP005H5 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005H6 ;
          prmP005H6 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005H7 ;
          prmP005H7 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005H8 ;
          prmP005H8 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005H9 ;
          prmP005H9 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005H10 ;
          prmP005H10 = new Object[] {
          new Object[] {"@AV11ContagemResultado_OSVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005H11 ;
          prmP005H11 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP005H12 ;
          prmP005H12 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP005H13 ;
          prmP005H13 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoChckLstLog_ChckLstCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLstLog_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLstLog_Etapa",SqlDbType.SmallInt,1,0}
          } ;
          Object[] prmP005H14 ;
          prmP005H14 = new Object[] {
          new Object[] {"@AV11ContagemResultado_OSVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005H15 ;
          prmP005H15 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005H16 ;
          prmP005H16 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005H17 ;
          prmP005H17 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005H18 ;
          prmP005H18 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3}
          } ;
          Object[] prmP005H19 ;
          prmP005H19 = new Object[] {
          new Object[] {"@AV19SDT_Item__Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P005H2", "SELECT [CheckList_Codigo], [ContagemResultadoChckLst_OSCod], [ContagemResultadoChckLst_Cumpre], [ContagemResultadoChckLst_Codigo] FROM [ContagemResultadoChckLst] WITH (UPDLOCK) WHERE ([ContagemResultadoChckLst_OSCod] = @AV8ContagemResultado_Codigo) AND ([CheckList_Codigo] = @AV19SDT_Item__Codigo) ORDER BY [ContagemResultadoChckLst_OSCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005H2,1,0,true,false )
             ,new CursorDef("P005H3", "UPDATE [ContagemResultadoChckLst] SET [ContagemResultadoChckLst_Cumpre]=@ContagemResultadoChckLst_Cumpre  WHERE [ContagemResultadoChckLst_Codigo] = @ContagemResultadoChckLst_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005H3)
             ,new CursorDef("P005H4", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_StatusDmn], [ContagemResultado_NaoCnfDmnCod] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @AV8ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005H4,1,0,true,true )
             ,new CursorDef("P005H5", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_NaoCnfDmnCod]=@ContagemResultado_NaoCnfDmnCod  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005H5)
             ,new CursorDef("P005H6", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_NaoCnfDmnCod]=@ContagemResultado_NaoCnfDmnCod  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005H6)
             ,new CursorDef("P005H7", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_StatusDmn], T2.[Servico_Percentual] AS ContagemResultado_CntSrvPrc FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @AV8ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005H7,1,0,true,true )
             ,new CursorDef("P005H8", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005H8)
             ,new CursorDef("P005H9", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005H9)
             ,new CursorDef("P005H10", "SELECT TOP 1 [ContagemResultado_Ultima], [ContagemResultado_Codigo], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE ([ContagemResultado_Codigo] = @AV11ContagemResultado_OSVinculada) AND ([ContagemResultado_Ultima] = 1) ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005H10,1,0,true,true )
             ,new CursorDef("P005H11", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005H11)
             ,new CursorDef("P005H12", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005H12)
             ,new CursorDef("P005H13", "INSERT INTO [ContagemResultadoChckLstLog]([ContagemResultadoChckLstLog_DataHora], [ContagemResultadoChckLstLog_ChckLstCod], [ContagemResultadoChckLstLog_UsuarioCod], [ContagemResultadoChckLstLog_Etapa], [ContagemResultadoChckLstLog_OSCodigo], [NaoConformidade_Codigo]) VALUES(@ContagemResultadoChckLstLog_DataHora, @ContagemResultadoChckLstLog_ChckLstCod, @ContagemResultadoChckLstLog_UsuarioCod, @ContagemResultadoChckLstLog_Etapa, convert(int, 0), convert(int, 0)); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP005H13)
             ,new CursorDef("P005H14", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_DataEntrega], [ContagemResultado_HoraEntrega], [ContagemResultado_StatusDmn] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @AV11ContagemResultado_OSVinculada ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005H14,1,0,true,true )
             ,new CursorDef("P005H15", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005H15)
             ,new CursorDef("P005H16", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005H16)
             ,new CursorDef("P005H17", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]='A'  WHERE [ContagemResultado_Codigo] = @AV8ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005H17)
             ,new CursorDef("P005H18", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_TimeCnt], [ContagemResultado_Divergencia], [ContagemResultado_ParecerTcn], [ContagemResultado_CstUntPrd], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, convert( DATETIME, '17530101', 112 ), convert(int, 0), '', convert(int, 0), CONVERT(varbinary(1), ''), '', '', convert( DATETIME, '17530101', 112 ), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP005H18)
             ,new CursorDef("P005H19", "SELECT TOP 1 [CheckList_Codigo], [CheckList_NaoCnfCod] FROM [CheckList] WITH (NOLOCK) WHERE [CheckList_Codigo] = @AV19SDT_Item__Codigo ORDER BY [CheckList_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005H19,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 8 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(6) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 5) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 10 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 11 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                stmt.SetParameter(3, (int)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[5]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                stmt.SetParameter(8, (int)parms[11]);
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[13]);
                }
                stmt.SetParameter(10, (short)parms[14]);
                stmt.SetParameter(11, (short)parms[15]);
                stmt.SetParameter(12, (bool)parms[16]);
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 13 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(13, (decimal)parms[18]);
                }
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
