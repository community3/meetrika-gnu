/*
               File: TipoRequisitoGeneral
        Description: Tipo Requisito General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:30:22.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tiporequisitogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public tiporequisitogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public tiporequisitogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_TipoRequisito_Codigo )
      {
         this.A2041TipoRequisito_Codigo = aP0_TipoRequisito_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbTipoRequisito_Classificacao = new GXCombobox();
         dynTipoRequisito_LinNegCod = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A2041TipoRequisito_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2041TipoRequisito_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A2041TipoRequisito_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"TIPOREQUISITO_LINNEGCOD") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLATIPOREQUISITO_LINNEGCODRT2( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PART2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "TipoRequisitoGeneral";
               context.Gx_err = 0;
               GXATIPOREQUISITO_LINNEGCOD_htmlRT2( ) ;
               /* Using cursor H00RT2 */
               pr_default.execute(0, new Object[] {n2039TipoRequisito_LinNegCod, A2039TipoRequisito_LinNegCod});
               A2040TipoRequisito_LinNegDsc = H00RT2_A2040TipoRequisito_LinNegDsc[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2040TipoRequisito_LinNegDsc", A2040TipoRequisito_LinNegDsc);
               n2040TipoRequisito_LinNegDsc = H00RT2_n2040TipoRequisito_LinNegDsc[0];
               pr_default.close(0);
               WSRT2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Tipo Requisito General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117302261");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("tiporequisitogeneral.aspx") + "?" + UrlEncode("" +A2041TipoRequisito_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA2041TipoRequisito_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TIPOREQUISITO_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2043TipoRequisito_Descricao, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TIPOREQUISITO_CLASSIFICACAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2044TipoRequisito_Classificacao, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TIPOREQUISITO_LINNEGCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2039TipoRequisito_LinNegCod), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormRT2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("tiporequisitogeneral.js", "?20203117302263");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "TipoRequisitoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Tipo Requisito General" ;
      }

      protected void WBRT0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "tiporequisitogeneral.aspx");
            }
            wb_table1_2_RT2( true) ;
         }
         else
         {
            wb_table1_2_RT2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_RT2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTipoRequisito_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2041TipoRequisito_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A2041TipoRequisito_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipoRequisito_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtTipoRequisito_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_TipoRequisitoGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTRT2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Tipo Requisito General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPRT0( ) ;
            }
         }
      }

      protected void WSRT2( )
      {
         STARTRT2( ) ;
         EVTRT2( ) ;
      }

      protected void EVTRT2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRT0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRT0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11RT2 */
                                    E11RT2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRT0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12RT2 */
                                    E12RT2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRT0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13RT2 */
                                    E13RT2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRT0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14RT2 */
                                    E14RT2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRT0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15RT2 */
                                    E15RT2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRT0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPRT0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WERT2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormRT2( ) ;
            }
         }
      }

      protected void PART2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbTipoRequisito_Classificacao.Name = "TIPOREQUISITO_CLASSIFICACAO";
            cmbTipoRequisito_Classificacao.WebTags = "";
            cmbTipoRequisito_Classificacao.addItem("", "(Nenhum)", 0);
            cmbTipoRequisito_Classificacao.addItem("M", "Must", 0);
            cmbTipoRequisito_Classificacao.addItem("S", "Should", 0);
            cmbTipoRequisito_Classificacao.addItem("C", "Could", 0);
            cmbTipoRequisito_Classificacao.addItem("W", "Would", 0);
            if ( cmbTipoRequisito_Classificacao.ItemCount > 0 )
            {
               A2044TipoRequisito_Classificacao = cmbTipoRequisito_Classificacao.getValidValue(A2044TipoRequisito_Classificacao);
               n2044TipoRequisito_Classificacao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2044TipoRequisito_Classificacao", A2044TipoRequisito_Classificacao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPOREQUISITO_CLASSIFICACAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2044TipoRequisito_Classificacao, ""))));
            }
            dynTipoRequisito_LinNegCod.Name = "TIPOREQUISITO_LINNEGCOD";
            dynTipoRequisito_LinNegCod.WebTags = "";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLATIPOREQUISITO_LINNEGCODRT2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLATIPOREQUISITO_LINNEGCOD_dataRT2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXATIPOREQUISITO_LINNEGCOD_htmlRT2( )
      {
         int gxdynajaxvalue ;
         GXDLATIPOREQUISITO_LINNEGCOD_dataRT2( ) ;
         gxdynajaxindex = 1;
         dynTipoRequisito_LinNegCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynTipoRequisito_LinNegCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynTipoRequisito_LinNegCod.ItemCount > 0 )
         {
            A2039TipoRequisito_LinNegCod = (int)(NumberUtil.Val( dynTipoRequisito_LinNegCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0))), "."));
            n2039TipoRequisito_LinNegCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2039TipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPOREQUISITO_LINNEGCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2039TipoRequisito_LinNegCod), "ZZZZZ9")));
         }
      }

      protected void GXDLATIPOREQUISITO_LINNEGCOD_dataRT2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor H00RT3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00RT3_A2039TipoRequisito_LinNegCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00RT3_A2040TipoRequisito_LinNegDsc[0]);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbTipoRequisito_Classificacao.ItemCount > 0 )
         {
            A2044TipoRequisito_Classificacao = cmbTipoRequisito_Classificacao.getValidValue(A2044TipoRequisito_Classificacao);
            n2044TipoRequisito_Classificacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2044TipoRequisito_Classificacao", A2044TipoRequisito_Classificacao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPOREQUISITO_CLASSIFICACAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2044TipoRequisito_Classificacao, ""))));
         }
         if ( dynTipoRequisito_LinNegCod.ItemCount > 0 )
         {
            A2039TipoRequisito_LinNegCod = (int)(NumberUtil.Val( dynTipoRequisito_LinNegCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0))), "."));
            n2039TipoRequisito_LinNegCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2039TipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPOREQUISITO_LINNEGCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2039TipoRequisito_LinNegCod), "ZZZZZ9")));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFRT2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "TipoRequisitoGeneral";
         context.Gx_err = 0;
      }

      protected void RFRT2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00RT4 */
            pr_default.execute(2, new Object[] {A2041TipoRequisito_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A2040TipoRequisito_LinNegDsc = H00RT4_A2040TipoRequisito_LinNegDsc[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2040TipoRequisito_LinNegDsc", A2040TipoRequisito_LinNegDsc);
               n2040TipoRequisito_LinNegDsc = H00RT4_n2040TipoRequisito_LinNegDsc[0];
               A2039TipoRequisito_LinNegCod = H00RT4_A2039TipoRequisito_LinNegCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2039TipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPOREQUISITO_LINNEGCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2039TipoRequisito_LinNegCod), "ZZZZZ9")));
               n2039TipoRequisito_LinNegCod = H00RT4_n2039TipoRequisito_LinNegCod[0];
               A2044TipoRequisito_Classificacao = H00RT4_A2044TipoRequisito_Classificacao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2044TipoRequisito_Classificacao", A2044TipoRequisito_Classificacao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPOREQUISITO_CLASSIFICACAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2044TipoRequisito_Classificacao, ""))));
               n2044TipoRequisito_Classificacao = H00RT4_n2044TipoRequisito_Classificacao[0];
               A2043TipoRequisito_Descricao = H00RT4_A2043TipoRequisito_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2043TipoRequisito_Descricao", A2043TipoRequisito_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPOREQUISITO_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2043TipoRequisito_Descricao, ""))));
               n2043TipoRequisito_Descricao = H00RT4_n2043TipoRequisito_Descricao[0];
               A2040TipoRequisito_LinNegDsc = H00RT4_A2040TipoRequisito_LinNegDsc[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2040TipoRequisito_LinNegDsc", A2040TipoRequisito_LinNegDsc);
               n2040TipoRequisito_LinNegDsc = H00RT4_n2040TipoRequisito_LinNegDsc[0];
               GXATIPOREQUISITO_LINNEGCOD_htmlRT2( ) ;
               /* Execute user event: E12RT2 */
               E12RT2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
            WBRT0( ) ;
         }
      }

      protected void STRUPRT0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "TipoRequisitoGeneral";
         context.Gx_err = 0;
         GXATIPOREQUISITO_LINNEGCOD_htmlRT2( ) ;
         /* Using cursor H00RT5 */
         pr_default.execute(3, new Object[] {n2039TipoRequisito_LinNegCod, A2039TipoRequisito_LinNegCod});
         A2040TipoRequisito_LinNegDsc = H00RT5_A2040TipoRequisito_LinNegDsc[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2040TipoRequisito_LinNegDsc", A2040TipoRequisito_LinNegDsc);
         n2040TipoRequisito_LinNegDsc = H00RT5_n2040TipoRequisito_LinNegDsc[0];
         pr_default.close(3);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11RT2 */
         E11RT2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A2043TipoRequisito_Descricao = cgiGet( edtTipoRequisito_Descricao_Internalname);
            n2043TipoRequisito_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2043TipoRequisito_Descricao", A2043TipoRequisito_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPOREQUISITO_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2043TipoRequisito_Descricao, ""))));
            cmbTipoRequisito_Classificacao.CurrentValue = cgiGet( cmbTipoRequisito_Classificacao_Internalname);
            A2044TipoRequisito_Classificacao = cgiGet( cmbTipoRequisito_Classificacao_Internalname);
            n2044TipoRequisito_Classificacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2044TipoRequisito_Classificacao", A2044TipoRequisito_Classificacao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPOREQUISITO_CLASSIFICACAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2044TipoRequisito_Classificacao, ""))));
            dynTipoRequisito_LinNegCod.CurrentValue = cgiGet( dynTipoRequisito_LinNegCod_Internalname);
            A2039TipoRequisito_LinNegCod = (int)(NumberUtil.Val( cgiGet( dynTipoRequisito_LinNegCod_Internalname), "."));
            n2039TipoRequisito_LinNegCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2039TipoRequisito_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPOREQUISITO_LINNEGCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2039TipoRequisito_LinNegCod), "ZZZZZ9")));
            A2040TipoRequisito_LinNegDsc = cgiGet( edtTipoRequisito_LinNegDsc_Internalname);
            n2040TipoRequisito_LinNegDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2040TipoRequisito_LinNegDsc", A2040TipoRequisito_LinNegDsc);
            /* Read saved values. */
            wcpOA2041TipoRequisito_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA2041TipoRequisito_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXATIPOREQUISITO_LINNEGCOD_htmlRT2( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11RT2 */
         E11RT2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11RT2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12RT2( )
      {
         /* Load Routine */
         edtTipoRequisito_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTipoRequisito_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoRequisito_Codigo_Visible), 5, 0)));
         if ( ! ( ( AV6WWPContext.gxTpr_Update ) ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Delete ) ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
      }

      protected void E13RT2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("tiporequisito.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A2041TipoRequisito_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14RT2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("tiporequisito.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A2041TipoRequisito_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E15RT2( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("wwtiporequisito.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "TipoRequisito";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "TipoRequisito_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7TipoRequisito_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_RT2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_RT2( true) ;
         }
         else
         {
            wb_table2_8_RT2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_RT2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_30_RT2( true) ;
         }
         else
         {
            wb_table3_30_RT2( false) ;
         }
         return  ;
      }

      protected void wb_table3_30_RT2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_RT2e( true) ;
         }
         else
         {
            wb_table1_2_RT2e( false) ;
         }
      }

      protected void wb_table3_30_RT2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipoRequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipoRequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipoRequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_30_RT2e( true) ;
         }
         else
         {
            wb_table3_30_RT2e( false) ;
         }
      }

      protected void wb_table2_8_RT2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktiporequisito_descricao_Internalname, "Descri��o", "", "", lblTextblocktiporequisito_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipoRequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtTipoRequisito_Descricao_Internalname, A2043TipoRequisito_Descricao, "", "", 0, 1, 0, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "", "HLP_TipoRequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktiporequisito_classificacao_Internalname, "Classifica��o", "", "", lblTextblocktiporequisito_classificacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipoRequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbTipoRequisito_Classificacao, cmbTipoRequisito_Classificacao_Internalname, StringUtil.RTrim( A2044TipoRequisito_Classificacao), 1, cmbTipoRequisito_Classificacao_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_TipoRequisitoGeneral.htm");
            cmbTipoRequisito_Classificacao.CurrentValue = StringUtil.RTrim( A2044TipoRequisito_Classificacao);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbTipoRequisito_Classificacao_Internalname, "Values", (String)(cmbTipoRequisito_Classificacao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktiporequisito_linnegcod_Internalname, "Linha de Neg�cio", "", "", lblTextblocktiporequisito_linnegcod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipoRequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynTipoRequisito_LinNegCod, dynTipoRequisito_LinNegCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0)), 1, dynTipoRequisito_LinNegCod_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_TipoRequisitoGeneral.htm");
            dynTipoRequisito_LinNegCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2039TipoRequisito_LinNegCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynTipoRequisito_LinNegCod_Internalname, "Values", (String)(dynTipoRequisito_LinNegCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtTipoRequisito_LinNegDsc_Internalname, A2040TipoRequisito_LinNegDsc, "", "", 0, 1, 0, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "", "HLP_TipoRequisitoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_RT2e( true) ;
         }
         else
         {
            wb_table2_8_RT2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A2041TipoRequisito_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2041TipoRequisito_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PART2( ) ;
         WSRT2( ) ;
         WERT2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA2041TipoRequisito_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PART2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "tiporequisitogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PART2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A2041TipoRequisito_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2041TipoRequisito_Codigo), 6, 0)));
         }
         wcpOA2041TipoRequisito_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA2041TipoRequisito_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A2041TipoRequisito_Codigo != wcpOA2041TipoRequisito_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA2041TipoRequisito_Codigo = A2041TipoRequisito_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA2041TipoRequisito_Codigo = cgiGet( sPrefix+"A2041TipoRequisito_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA2041TipoRequisito_Codigo) > 0 )
         {
            A2041TipoRequisito_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA2041TipoRequisito_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2041TipoRequisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2041TipoRequisito_Codigo), 6, 0)));
         }
         else
         {
            A2041TipoRequisito_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A2041TipoRequisito_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PART2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSRT2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSRT2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A2041TipoRequisito_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2041TipoRequisito_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA2041TipoRequisito_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A2041TipoRequisito_Codigo_CTRL", StringUtil.RTrim( sCtrlA2041TipoRequisito_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WERT2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117302287");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("tiporequisitogeneral.js", "?20203117302287");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocktiporequisito_descricao_Internalname = sPrefix+"TEXTBLOCKTIPOREQUISITO_DESCRICAO";
         edtTipoRequisito_Descricao_Internalname = sPrefix+"TIPOREQUISITO_DESCRICAO";
         lblTextblocktiporequisito_classificacao_Internalname = sPrefix+"TEXTBLOCKTIPOREQUISITO_CLASSIFICACAO";
         cmbTipoRequisito_Classificacao_Internalname = sPrefix+"TIPOREQUISITO_CLASSIFICACAO";
         lblTextblocktiporequisito_linnegcod_Internalname = sPrefix+"TEXTBLOCKTIPOREQUISITO_LINNEGCOD";
         dynTipoRequisito_LinNegCod_Internalname = sPrefix+"TIPOREQUISITO_LINNEGCOD";
         edtTipoRequisito_LinNegDsc_Internalname = sPrefix+"TIPOREQUISITO_LINNEGDSC";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtTipoRequisito_Codigo_Internalname = sPrefix+"TIPOREQUISITO_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         dynTipoRequisito_LinNegCod_Jsonclick = "";
         cmbTipoRequisito_Classificacao_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtnupdate_Enabled = 1;
         edtTipoRequisito_Codigo_Jsonclick = "";
         edtTipoRequisito_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13RT2',iparms:[{av:'A2041TipoRequisito_Codigo',fld:'TIPOREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14RT2',iparms:[{av:'A2041TipoRequisito_Codigo',fld:'TIPOREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15RT2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         scmdbuf = "";
         H00RT2_A2040TipoRequisito_LinNegDsc = new String[] {""} ;
         H00RT2_n2040TipoRequisito_LinNegDsc = new bool[] {false} ;
         A2040TipoRequisito_LinNegDsc = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A2043TipoRequisito_Descricao = "";
         A2044TipoRequisito_Classificacao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H00RT3_A2039TipoRequisito_LinNegCod = new int[1] ;
         H00RT3_n2039TipoRequisito_LinNegCod = new bool[] {false} ;
         H00RT3_A2040TipoRequisito_LinNegDsc = new String[] {""} ;
         H00RT3_n2040TipoRequisito_LinNegDsc = new bool[] {false} ;
         H00RT4_A2041TipoRequisito_Codigo = new int[1] ;
         H00RT4_A2040TipoRequisito_LinNegDsc = new String[] {""} ;
         H00RT4_n2040TipoRequisito_LinNegDsc = new bool[] {false} ;
         H00RT4_A2039TipoRequisito_LinNegCod = new int[1] ;
         H00RT4_n2039TipoRequisito_LinNegCod = new bool[] {false} ;
         H00RT4_A2044TipoRequisito_Classificacao = new String[] {""} ;
         H00RT4_n2044TipoRequisito_Classificacao = new bool[] {false} ;
         H00RT4_A2043TipoRequisito_Descricao = new String[] {""} ;
         H00RT4_n2043TipoRequisito_Descricao = new bool[] {false} ;
         H00RT5_A2040TipoRequisito_LinNegDsc = new String[] {""} ;
         H00RT5_n2040TipoRequisito_LinNegDsc = new bool[] {false} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblocktiporequisito_descricao_Jsonclick = "";
         lblTextblocktiporequisito_classificacao_Jsonclick = "";
         lblTextblocktiporequisito_linnegcod_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA2041TipoRequisito_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.tiporequisitogeneral__default(),
            new Object[][] {
                new Object[] {
               H00RT2_A2040TipoRequisito_LinNegDsc, H00RT2_n2040TipoRequisito_LinNegDsc
               }
               , new Object[] {
               H00RT3_A2039TipoRequisito_LinNegCod, H00RT3_A2040TipoRequisito_LinNegDsc, H00RT3_n2040TipoRequisito_LinNegDsc
               }
               , new Object[] {
               H00RT4_A2041TipoRequisito_Codigo, H00RT4_A2040TipoRequisito_LinNegDsc, H00RT4_n2040TipoRequisito_LinNegDsc, H00RT4_A2039TipoRequisito_LinNegCod, H00RT4_n2039TipoRequisito_LinNegCod, H00RT4_A2044TipoRequisito_Classificacao, H00RT4_n2044TipoRequisito_Classificacao, H00RT4_A2043TipoRequisito_Descricao, H00RT4_n2043TipoRequisito_Descricao
               }
               , new Object[] {
               H00RT5_A2040TipoRequisito_LinNegDsc, H00RT5_n2040TipoRequisito_LinNegDsc
               }
            }
         );
         AV14Pgmname = "TipoRequisitoGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "TipoRequisitoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A2041TipoRequisito_Codigo ;
      private int wcpOA2041TipoRequisito_Codigo ;
      private int A2039TipoRequisito_LinNegCod ;
      private int edtTipoRequisito_Codigo_Visible ;
      private int gxdynajaxindex ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int AV7TipoRequisito_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String scmdbuf ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A2044TipoRequisito_Classificacao ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtTipoRequisito_Codigo_Internalname ;
      private String edtTipoRequisito_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String gxwrpcisep ;
      private String edtTipoRequisito_Descricao_Internalname ;
      private String cmbTipoRequisito_Classificacao_Internalname ;
      private String dynTipoRequisito_LinNegCod_Internalname ;
      private String edtTipoRequisito_LinNegDsc_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocktiporequisito_descricao_Internalname ;
      private String lblTextblocktiporequisito_descricao_Jsonclick ;
      private String lblTextblocktiporequisito_classificacao_Internalname ;
      private String lblTextblocktiporequisito_classificacao_Jsonclick ;
      private String cmbTipoRequisito_Classificacao_Jsonclick ;
      private String lblTextblocktiporequisito_linnegcod_Internalname ;
      private String lblTextblocktiporequisito_linnegcod_Jsonclick ;
      private String dynTipoRequisito_LinNegCod_Jsonclick ;
      private String sCtrlA2041TipoRequisito_Codigo ;
      private bool entryPointCalled ;
      private bool n2039TipoRequisito_LinNegCod ;
      private bool n2040TipoRequisito_LinNegDsc ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n2044TipoRequisito_Classificacao ;
      private bool n2043TipoRequisito_Descricao ;
      private bool returnInSub ;
      private String A2040TipoRequisito_LinNegDsc ;
      private String A2043TipoRequisito_Descricao ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbTipoRequisito_Classificacao ;
      private GXCombobox dynTipoRequisito_LinNegCod ;
      private IDataStoreProvider pr_default ;
      private String[] H00RT2_A2040TipoRequisito_LinNegDsc ;
      private bool[] H00RT2_n2040TipoRequisito_LinNegDsc ;
      private int[] H00RT3_A2039TipoRequisito_LinNegCod ;
      private bool[] H00RT3_n2039TipoRequisito_LinNegCod ;
      private String[] H00RT3_A2040TipoRequisito_LinNegDsc ;
      private bool[] H00RT3_n2040TipoRequisito_LinNegDsc ;
      private int[] H00RT4_A2041TipoRequisito_Codigo ;
      private String[] H00RT4_A2040TipoRequisito_LinNegDsc ;
      private bool[] H00RT4_n2040TipoRequisito_LinNegDsc ;
      private int[] H00RT4_A2039TipoRequisito_LinNegCod ;
      private bool[] H00RT4_n2039TipoRequisito_LinNegCod ;
      private String[] H00RT4_A2044TipoRequisito_Classificacao ;
      private bool[] H00RT4_n2044TipoRequisito_Classificacao ;
      private String[] H00RT4_A2043TipoRequisito_Descricao ;
      private bool[] H00RT4_n2043TipoRequisito_Descricao ;
      private String[] H00RT5_A2040TipoRequisito_LinNegDsc ;
      private bool[] H00RT5_n2040TipoRequisito_LinNegDsc ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class tiporequisitogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00RT2 ;
          prmH00RT2 = new Object[] {
          new Object[] {"@TipoRequisito_LinNegCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00RT3 ;
          prmH00RT3 = new Object[] {
          } ;
          Object[] prmH00RT4 ;
          prmH00RT4 = new Object[] {
          new Object[] {"@TipoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00RT5 ;
          prmH00RT5 = new Object[] {
          new Object[] {"@TipoRequisito_LinNegCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00RT2", "SELECT [LinhaNegocio_Descricao] AS TipoRequisito_LinNegDsc FROM [LinhaNegocio] WITH (NOLOCK) WHERE [LinhadeNegocio_Codigo] = @TipoRequisito_LinNegCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RT2,1,0,true,true )
             ,new CursorDef("H00RT3", "SELECT [LinhadeNegocio_Codigo] AS TipoRequisito_LinNegCod, [LinhaNegocio_Descricao] AS TipoRequisito_LinNegDsc FROM [LinhaNegocio] WITH (NOLOCK) ORDER BY [LinhaNegocio_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RT3,0,0,true,false )
             ,new CursorDef("H00RT4", "SELECT T1.[TipoRequisito_Codigo], T2.[LinhaNegocio_Descricao] AS TipoRequisito_LinNegDsc, T1.[TipoRequisito_LinNegCod] AS TipoRequisito_LinNegCod, T1.[TipoRequisito_Classificacao], T1.[TipoRequisito_Descricao] FROM ([TipoRequisito] T1 WITH (NOLOCK) LEFT JOIN [LinhaNegocio] T2 WITH (NOLOCK) ON T2.[LinhadeNegocio_Codigo] = T1.[TipoRequisito_LinNegCod]) WHERE T1.[TipoRequisito_Codigo] = @TipoRequisito_Codigo ORDER BY T1.[TipoRequisito_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RT4,1,0,true,true )
             ,new CursorDef("H00RT5", "SELECT [LinhaNegocio_Descricao] AS TipoRequisito_LinNegDsc FROM [LinhaNegocio] WITH (NOLOCK) WHERE [LinhadeNegocio_Codigo] = @TipoRequisito_LinNegCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RT5,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
