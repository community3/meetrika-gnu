/*
               File: BancoAgenciaGeneral
        Description: Banco Agencia General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:15:32.7
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class bancoagenciageneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public bancoagenciageneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public bancoagenciageneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_BancoAgencia_Codigo )
      {
         this.A17BancoAgencia_Codigo = aP0_BancoAgencia_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A17BancoAgencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A17BancoAgencia_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A17BancoAgencia_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA1O2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "BancoAgenciaGeneral";
               context.Gx_err = 0;
               WS1O2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Banco Agencia General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117153214");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("bancoagenciageneral.aspx") + "?" + UrlEncode("" +A17BancoAgencia_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA17BancoAgencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_BANCOAGENCIA_AGENCIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A27BancoAgencia_Agencia, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_BANCOAGENCIA_NOMEAGENCIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A28BancoAgencia_NomeAgencia, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_BANCO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A18Banco_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_MUNICIPIO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "BancoAgenciaGeneral";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A18Banco_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("bancoagenciageneral:[SendSecurityCheck value for]"+"Banco_Codigo:"+context.localUtil.Format( (decimal)(A18Banco_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("bancoagenciageneral:[SendSecurityCheck value for]"+"Municipio_Codigo:"+context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseForm1O2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("bancoagenciageneral.js", "?20203117153215");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "BancoAgenciaGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Banco Agencia General" ;
      }

      protected void WB1O0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "bancoagenciageneral.aspx");
            }
            wb_table1_2_1O2( true) ;
         }
         else
         {
            wb_table1_2_1O2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_1O2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START1O2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Banco Agencia General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP1O0( ) ;
            }
         }
      }

      protected void WS1O2( )
      {
         START1O2( ) ;
         EVT1O2( ) ;
      }

      protected void EVT1O2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP1O0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP1O0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E111O2 */
                                    E111O2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP1O0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E121O2 */
                                    E121O2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP1O0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E131O2 */
                                    E131O2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP1O0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E141O2 */
                                    E141O2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP1O0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP1O0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE1O2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm1O2( ) ;
            }
         }
      }

      protected void PA1O2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF1O2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "BancoAgenciaGeneral";
         context.Gx_err = 0;
      }

      protected void RF1O2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H001O2 */
            pr_default.execute(0, new Object[] {A17BancoAgencia_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A26Municipio_Nome = H001O2_A26Municipio_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A26Municipio_Nome", A26Municipio_Nome);
               A25Municipio_Codigo = H001O2_A25Municipio_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MUNICIPIO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9")));
               A20Banco_Nome = H001O2_A20Banco_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A20Banco_Nome", A20Banco_Nome);
               A18Banco_Codigo = H001O2_A18Banco_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A18Banco_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Banco_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_BANCO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A18Banco_Codigo), "ZZZZZ9")));
               A28BancoAgencia_NomeAgencia = H001O2_A28BancoAgencia_NomeAgencia[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A28BancoAgencia_NomeAgencia", A28BancoAgencia_NomeAgencia);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_BANCOAGENCIA_NOMEAGENCIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A28BancoAgencia_NomeAgencia, "@!"))));
               A27BancoAgencia_Agencia = H001O2_A27BancoAgencia_Agencia[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A27BancoAgencia_Agencia", A27BancoAgencia_Agencia);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_BANCOAGENCIA_AGENCIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A27BancoAgencia_Agencia, ""))));
               A26Municipio_Nome = H001O2_A26Municipio_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A26Municipio_Nome", A26Municipio_Nome);
               A20Banco_Nome = H001O2_A20Banco_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A20Banco_Nome", A20Banco_Nome);
               /* Execute user event: E121O2 */
               E121O2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB1O0( ) ;
         }
      }

      protected void STRUP1O0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "BancoAgenciaGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E111O2 */
         E111O2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A27BancoAgencia_Agencia = cgiGet( edtBancoAgencia_Agencia_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A27BancoAgencia_Agencia", A27BancoAgencia_Agencia);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_BANCOAGENCIA_AGENCIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A27BancoAgencia_Agencia, ""))));
            A28BancoAgencia_NomeAgencia = StringUtil.Upper( cgiGet( edtBancoAgencia_NomeAgencia_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A28BancoAgencia_NomeAgencia", A28BancoAgencia_NomeAgencia);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_BANCOAGENCIA_NOMEAGENCIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A28BancoAgencia_NomeAgencia, "@!"))));
            A18Banco_Codigo = (int)(context.localUtil.CToN( cgiGet( edtBanco_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A18Banco_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Banco_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_BANCO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A18Banco_Codigo), "ZZZZZ9")));
            A20Banco_Nome = StringUtil.Upper( cgiGet( edtBanco_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A20Banco_Nome", A20Banco_Nome);
            A25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMunicipio_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MUNICIPIO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9")));
            A26Municipio_Nome = StringUtil.Upper( cgiGet( edtMunicipio_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A26Municipio_Nome", A26Municipio_Nome);
            /* Read saved values. */
            wcpOA17BancoAgencia_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA17BancoAgencia_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "BancoAgenciaGeneral";
            A18Banco_Codigo = (int)(context.localUtil.CToN( cgiGet( edtBanco_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A18Banco_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Banco_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_BANCO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A18Banco_Codigo), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A18Banco_Codigo), "ZZZZZ9");
            A25Municipio_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMunicipio_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_MUNICIPIO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("bancoagenciageneral:[SecurityCheckFailed value for]"+"Banco_Codigo:"+context.localUtil.Format( (decimal)(A18Banco_Codigo), "ZZZZZ9"));
               GXUtil.WriteLog("bancoagenciageneral:[SecurityCheckFailed value for]"+"Municipio_Codigo:"+context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E111O2 */
         E111O2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E111O2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E121O2( )
      {
         /* Load Routine */
         edtBanco_Nome_Link = formatLink("viewbanco.aspx") + "?" + UrlEncode("" +A18Banco_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtBanco_Nome_Internalname, "Link", edtBanco_Nome_Link);
         edtMunicipio_Nome_Link = formatLink("viewmunicipio.aspx") + "?" + UrlEncode("" +A25Municipio_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtMunicipio_Nome_Internalname, "Link", edtMunicipio_Nome_Link);
         if ( ! ( ( AV6WWPContext.gxTpr_Update ) ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Delete ) ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
      }

      protected void E131O2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("bancoagencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A17BancoAgencia_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E141O2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("bancoagencia.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A17BancoAgencia_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "BancoAgencia";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "BancoAgencia_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7BancoAgencia_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_1O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_1O2( true) ;
         }
         else
         {
            wb_table2_8_1O2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_1O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_46_1O2( true) ;
         }
         else
         {
            wb_table3_46_1O2( false) ;
         }
         return  ;
      }

      protected void wb_table3_46_1O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1O2e( true) ;
         }
         else
         {
            wb_table1_2_1O2e( false) ;
         }
      }

      protected void wb_table3_46_1O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_BancoAgenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_BancoAgenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_46_1O2e( true) ;
         }
         else
         {
            wb_table3_46_1O2e( false) ;
         }
      }

      protected void wb_table2_8_1O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockbancoagencia_codigo_Internalname, "C�digo", "", "", lblTextblockbancoagencia_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_BancoAgenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtBancoAgencia_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A17BancoAgencia_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A17BancoAgencia_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtBancoAgencia_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_BancoAgenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockbancoagencia_agencia_Internalname, "N�mero da Ag�ncia", "", "", lblTextblockbancoagencia_agencia_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_BancoAgenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtBancoAgencia_Agencia_Internalname, StringUtil.RTrim( A27BancoAgencia_Agencia), StringUtil.RTrim( context.localUtil.Format( A27BancoAgencia_Agencia, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtBancoAgencia_Agencia_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "Agencia", "left", true, "HLP_BancoAgenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockbancoagencia_nomeagencia_Internalname, "Nome da Ag�ncia", "", "", lblTextblockbancoagencia_nomeagencia_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_BancoAgenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtBancoAgencia_NomeAgencia_Internalname, StringUtil.RTrim( A28BancoAgencia_NomeAgencia), StringUtil.RTrim( context.localUtil.Format( A28BancoAgencia_NomeAgencia, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtBancoAgencia_NomeAgencia_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_BancoAgenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockbanco_codigo_Internalname, "C�digo", "", "", lblTextblockbanco_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_BancoAgenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtBanco_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A18Banco_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A18Banco_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtBanco_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_BancoAgenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockbanco_nome_Internalname, "Banco", "", "", lblTextblockbanco_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_BancoAgenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtBanco_Nome_Internalname, StringUtil.RTrim( A20Banco_Nome), StringUtil.RTrim( context.localUtil.Format( A20Banco_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtBanco_Nome_Link, "", "", "", edtBanco_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_BancoAgenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmunicipio_codigo_Internalname, "C�digo", "", "", lblTextblockmunicipio_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_BancoAgenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMunicipio_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A25Municipio_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMunicipio_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_BancoAgenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmunicipio_nome_Internalname, "Nome do Munic�pio", "", "", lblTextblockmunicipio_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_BancoAgenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMunicipio_Nome_Internalname, StringUtil.RTrim( A26Municipio_Nome), StringUtil.RTrim( context.localUtil.Format( A26Municipio_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtMunicipio_Nome_Link, "", "", "", edtMunicipio_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_BancoAgenciaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_1O2e( true) ;
         }
         else
         {
            wb_table2_8_1O2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A17BancoAgencia_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A17BancoAgencia_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA1O2( ) ;
         WS1O2( ) ;
         WE1O2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA17BancoAgencia_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA1O2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "bancoagenciageneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA1O2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A17BancoAgencia_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A17BancoAgencia_Codigo), 6, 0)));
         }
         wcpOA17BancoAgencia_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA17BancoAgencia_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A17BancoAgencia_Codigo != wcpOA17BancoAgencia_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA17BancoAgencia_Codigo = A17BancoAgencia_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA17BancoAgencia_Codigo = cgiGet( sPrefix+"A17BancoAgencia_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA17BancoAgencia_Codigo) > 0 )
         {
            A17BancoAgencia_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA17BancoAgencia_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A17BancoAgencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A17BancoAgencia_Codigo), 6, 0)));
         }
         else
         {
            A17BancoAgencia_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A17BancoAgencia_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA1O2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS1O2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS1O2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A17BancoAgencia_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A17BancoAgencia_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA17BancoAgencia_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A17BancoAgencia_Codigo_CTRL", StringUtil.RTrim( sCtrlA17BancoAgencia_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE1O2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117153250");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("bancoagenciageneral.js", "?20203117153250");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockbancoagencia_codigo_Internalname = sPrefix+"TEXTBLOCKBANCOAGENCIA_CODIGO";
         edtBancoAgencia_Codigo_Internalname = sPrefix+"BANCOAGENCIA_CODIGO";
         lblTextblockbancoagencia_agencia_Internalname = sPrefix+"TEXTBLOCKBANCOAGENCIA_AGENCIA";
         edtBancoAgencia_Agencia_Internalname = sPrefix+"BANCOAGENCIA_AGENCIA";
         lblTextblockbancoagencia_nomeagencia_Internalname = sPrefix+"TEXTBLOCKBANCOAGENCIA_NOMEAGENCIA";
         edtBancoAgencia_NomeAgencia_Internalname = sPrefix+"BANCOAGENCIA_NOMEAGENCIA";
         lblTextblockbanco_codigo_Internalname = sPrefix+"TEXTBLOCKBANCO_CODIGO";
         edtBanco_Codigo_Internalname = sPrefix+"BANCO_CODIGO";
         lblTextblockbanco_nome_Internalname = sPrefix+"TEXTBLOCKBANCO_NOME";
         edtBanco_Nome_Internalname = sPrefix+"BANCO_NOME";
         lblTextblockmunicipio_codigo_Internalname = sPrefix+"TEXTBLOCKMUNICIPIO_CODIGO";
         edtMunicipio_Codigo_Internalname = sPrefix+"MUNICIPIO_CODIGO";
         lblTextblockmunicipio_nome_Internalname = sPrefix+"TEXTBLOCKMUNICIPIO_NOME";
         edtMunicipio_Nome_Internalname = sPrefix+"MUNICIPIO_NOME";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtMunicipio_Nome_Jsonclick = "";
         edtMunicipio_Codigo_Jsonclick = "";
         edtBanco_Nome_Jsonclick = "";
         edtBanco_Codigo_Jsonclick = "";
         edtBancoAgencia_NomeAgencia_Jsonclick = "";
         edtBancoAgencia_Agencia_Jsonclick = "";
         edtBancoAgencia_Codigo_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtnupdate_Enabled = 1;
         edtMunicipio_Nome_Link = "";
         edtBanco_Nome_Link = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E131O2',iparms:[{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E141O2',iparms:[{av:'A17BancoAgencia_Codigo',fld:'BANCOAGENCIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A27BancoAgencia_Agencia = "";
         A28BancoAgencia_NomeAgencia = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H001O2_A17BancoAgencia_Codigo = new int[1] ;
         H001O2_A26Municipio_Nome = new String[] {""} ;
         H001O2_A25Municipio_Codigo = new int[1] ;
         H001O2_A20Banco_Nome = new String[] {""} ;
         H001O2_A18Banco_Codigo = new int[1] ;
         H001O2_A28BancoAgencia_NomeAgencia = new String[] {""} ;
         H001O2_A27BancoAgencia_Agencia = new String[] {""} ;
         A26Municipio_Nome = "";
         A20Banco_Nome = "";
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockbancoagencia_codigo_Jsonclick = "";
         lblTextblockbancoagencia_agencia_Jsonclick = "";
         lblTextblockbancoagencia_nomeagencia_Jsonclick = "";
         lblTextblockbanco_codigo_Jsonclick = "";
         lblTextblockbanco_nome_Jsonclick = "";
         lblTextblockmunicipio_codigo_Jsonclick = "";
         lblTextblockmunicipio_nome_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA17BancoAgencia_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.bancoagenciageneral__default(),
            new Object[][] {
                new Object[] {
               H001O2_A17BancoAgencia_Codigo, H001O2_A26Municipio_Nome, H001O2_A25Municipio_Codigo, H001O2_A20Banco_Nome, H001O2_A18Banco_Codigo, H001O2_A28BancoAgencia_NomeAgencia, H001O2_A27BancoAgencia_Agencia
               }
            }
         );
         AV14Pgmname = "BancoAgenciaGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "BancoAgenciaGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A17BancoAgencia_Codigo ;
      private int wcpOA17BancoAgencia_Codigo ;
      private int A18Banco_Codigo ;
      private int A25Municipio_Codigo ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int AV7BancoAgencia_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A27BancoAgencia_Agencia ;
      private String A28BancoAgencia_NomeAgencia ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String A26Municipio_Nome ;
      private String A20Banco_Nome ;
      private String edtBancoAgencia_Agencia_Internalname ;
      private String edtBancoAgencia_NomeAgencia_Internalname ;
      private String edtBanco_Codigo_Internalname ;
      private String edtBanco_Nome_Internalname ;
      private String edtMunicipio_Codigo_Internalname ;
      private String edtMunicipio_Nome_Internalname ;
      private String hsh ;
      private String edtBanco_Nome_Link ;
      private String edtMunicipio_Nome_Link ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockbancoagencia_codigo_Internalname ;
      private String lblTextblockbancoagencia_codigo_Jsonclick ;
      private String edtBancoAgencia_Codigo_Internalname ;
      private String edtBancoAgencia_Codigo_Jsonclick ;
      private String lblTextblockbancoagencia_agencia_Internalname ;
      private String lblTextblockbancoagencia_agencia_Jsonclick ;
      private String edtBancoAgencia_Agencia_Jsonclick ;
      private String lblTextblockbancoagencia_nomeagencia_Internalname ;
      private String lblTextblockbancoagencia_nomeagencia_Jsonclick ;
      private String edtBancoAgencia_NomeAgencia_Jsonclick ;
      private String lblTextblockbanco_codigo_Internalname ;
      private String lblTextblockbanco_codigo_Jsonclick ;
      private String edtBanco_Codigo_Jsonclick ;
      private String lblTextblockbanco_nome_Internalname ;
      private String lblTextblockbanco_nome_Jsonclick ;
      private String edtBanco_Nome_Jsonclick ;
      private String lblTextblockmunicipio_codigo_Internalname ;
      private String lblTextblockmunicipio_codigo_Jsonclick ;
      private String edtMunicipio_Codigo_Jsonclick ;
      private String lblTextblockmunicipio_nome_Internalname ;
      private String lblTextblockmunicipio_nome_Jsonclick ;
      private String edtMunicipio_Nome_Jsonclick ;
      private String sCtrlA17BancoAgencia_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H001O2_A17BancoAgencia_Codigo ;
      private String[] H001O2_A26Municipio_Nome ;
      private int[] H001O2_A25Municipio_Codigo ;
      private String[] H001O2_A20Banco_Nome ;
      private int[] H001O2_A18Banco_Codigo ;
      private String[] H001O2_A28BancoAgencia_NomeAgencia ;
      private String[] H001O2_A27BancoAgencia_Agencia ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class bancoagenciageneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH001O2 ;
          prmH001O2 = new Object[] {
          new Object[] {"@BancoAgencia_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H001O2", "SELECT T1.[BancoAgencia_Codigo], T2.[Municipio_Nome], T1.[Municipio_Codigo], T3.[Banco_Nome], T1.[Banco_Codigo], T1.[BancoAgencia_NomeAgencia], T1.[BancoAgencia_Agencia] FROM (([BancoAgencia] T1 WITH (NOLOCK) INNER JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = T1.[Municipio_Codigo]) INNER JOIN [Banco] T3 WITH (NOLOCK) ON T3.[Banco_Codigo] = T1.[Banco_Codigo]) WHERE T1.[BancoAgencia_Codigo] = @BancoAgencia_Codigo ORDER BY T1.[BancoAgencia_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH001O2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
