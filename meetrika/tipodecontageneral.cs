/*
               File: TipodeContaGeneral
        Description: Tipode Conta General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:23:48.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tipodecontageneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public tipodecontageneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public tipodecontageneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_TipodeConta_Codigo )
      {
         this.A870TipodeConta_Codigo = aP0_TipodeConta_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbTipodeConta_Tipo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A870TipodeConta_Codigo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A870TipodeConta_Codigo", A870TipodeConta_Codigo);
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(String)A870TipodeConta_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAFJ2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "TipodeContaGeneral";
               context.Gx_err = 0;
               WSFJ2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Tipode Conta General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117234836");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("tipodecontageneral.aspx") + "?" + UrlEncode(StringUtil.RTrim(A870TipodeConta_Codigo))+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA870TipodeConta_Codigo", StringUtil.RTrim( wcpOA870TipodeConta_Codigo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TIPODECONTA_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A871TipodeConta_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TIPODECONTA_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A872TipodeConta_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TIPODECONTA_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A873TipodeConta_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TIPODECONTA_CONTAPAICOD", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A885TipoDeConta_ContaPaiCod, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormFJ2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("tipodecontageneral.js", "?20203117234838");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "TipodeContaGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Tipode Conta General" ;
      }

      protected void WBFJ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "tipodecontageneral.aspx");
            }
            wb_table1_2_FJ2( true) ;
         }
         else
         {
            wb_table1_2_FJ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_FJ2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTFJ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Tipode Conta General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPFJ0( ) ;
            }
         }
      }

      protected void WSFJ2( )
      {
         STARTFJ2( ) ;
         EVTFJ2( ) ;
      }

      protected void EVTFJ2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFJ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFJ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11FJ2 */
                                    E11FJ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFJ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12FJ2 */
                                    E12FJ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFJ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13FJ2 */
                                    E13FJ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFJ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14FJ2 */
                                    E14FJ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFJ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFJ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEFJ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormFJ2( ) ;
            }
         }
      }

      protected void PAFJ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbTipodeConta_Tipo.Name = "TIPODECONTA_TIPO";
            cmbTipodeConta_Tipo.WebTags = "";
            cmbTipodeConta_Tipo.addItem("", "(Nenhum)", 0);
            cmbTipodeConta_Tipo.addItem("D", "D�bito", 0);
            cmbTipodeConta_Tipo.addItem("C", "Cr�dito", 0);
            if ( cmbTipodeConta_Tipo.ItemCount > 0 )
            {
               A873TipodeConta_Tipo = cmbTipodeConta_Tipo.getValidValue(A873TipodeConta_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A873TipodeConta_Tipo", A873TipodeConta_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPODECONTA_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A873TipodeConta_Tipo, ""))));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbTipodeConta_Tipo.ItemCount > 0 )
         {
            A873TipodeConta_Tipo = cmbTipodeConta_Tipo.getValidValue(A873TipodeConta_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A873TipodeConta_Tipo", A873TipodeConta_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPODECONTA_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A873TipodeConta_Tipo, ""))));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFFJ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "TipodeContaGeneral";
         context.Gx_err = 0;
      }

      protected void RFFJ2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00FJ2 */
            pr_default.execute(0, new Object[] {A870TipodeConta_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A885TipoDeConta_ContaPaiCod = H00FJ2_A885TipoDeConta_ContaPaiCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A885TipoDeConta_ContaPaiCod", A885TipoDeConta_ContaPaiCod);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPODECONTA_CONTAPAICOD", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A885TipoDeConta_ContaPaiCod, ""))));
               n885TipoDeConta_ContaPaiCod = H00FJ2_n885TipoDeConta_ContaPaiCod[0];
               A873TipodeConta_Tipo = H00FJ2_A873TipodeConta_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A873TipodeConta_Tipo", A873TipodeConta_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPODECONTA_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A873TipodeConta_Tipo, ""))));
               A872TipodeConta_Descricao = H00FJ2_A872TipodeConta_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A872TipodeConta_Descricao", A872TipodeConta_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPODECONTA_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A872TipodeConta_Descricao, "@!"))));
               A871TipodeConta_Nome = H00FJ2_A871TipodeConta_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A871TipodeConta_Nome", A871TipodeConta_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPODECONTA_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A871TipodeConta_Nome, "@!"))));
               /* Execute user event: E12FJ2 */
               E12FJ2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBFJ0( ) ;
         }
      }

      protected void STRUPFJ0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "TipodeContaGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11FJ2 */
         E11FJ2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A871TipodeConta_Nome = StringUtil.Upper( cgiGet( edtTipodeConta_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A871TipodeConta_Nome", A871TipodeConta_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPODECONTA_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A871TipodeConta_Nome, "@!"))));
            A872TipodeConta_Descricao = StringUtil.Upper( cgiGet( edtTipodeConta_Descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A872TipodeConta_Descricao", A872TipodeConta_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPODECONTA_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A872TipodeConta_Descricao, "@!"))));
            cmbTipodeConta_Tipo.CurrentValue = cgiGet( cmbTipodeConta_Tipo_Internalname);
            A873TipodeConta_Tipo = cgiGet( cmbTipodeConta_Tipo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A873TipodeConta_Tipo", A873TipodeConta_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPODECONTA_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A873TipodeConta_Tipo, ""))));
            A885TipoDeConta_ContaPaiCod = cgiGet( edtTipoDeConta_ContaPaiCod_Internalname);
            n885TipoDeConta_ContaPaiCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A885TipoDeConta_ContaPaiCod", A885TipoDeConta_ContaPaiCod);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_TIPODECONTA_CONTAPAICOD", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A885TipoDeConta_ContaPaiCod, ""))));
            /* Read saved values. */
            wcpOA870TipodeConta_Codigo = cgiGet( sPrefix+"wcpOA870TipodeConta_Codigo");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11FJ2 */
         E11FJ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11FJ2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12FJ2( )
      {
         /* Load Routine */
      }

      protected void E13FJ2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("tipodeconta.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode(StringUtil.RTrim(A870TipodeConta_Codigo));
         context.wjLocDisableFrm = 1;
      }

      protected void E14FJ2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("tipodeconta.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode(StringUtil.RTrim(A870TipodeConta_Codigo));
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "TipodeConta";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "TipodeConta_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = AV7TipodeConta_Codigo;
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_FJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_FJ2( true) ;
         }
         else
         {
            wb_table2_8_FJ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_FJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_36_FJ2( true) ;
         }
         else
         {
            wb_table3_36_FJ2( false) ;
         }
         return  ;
      }

      protected void wb_table3_36_FJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_FJ2e( true) ;
         }
         else
         {
            wb_table1_2_FJ2e( false) ;
         }
      }

      protected void wb_table3_36_FJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipodeContaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipodeContaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_36_FJ2e( true) ;
         }
         else
         {
            wb_table3_36_FJ2e( false) ;
         }
      }

      protected void wb_table2_8_FJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipodeconta_codigo_Internalname, "Conta", "", "", lblTextblocktipodeconta_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipodeContaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTipodeConta_Codigo_Internalname, StringUtil.RTrim( A870TipodeConta_Codigo), StringUtil.RTrim( context.localUtil.Format( A870TipodeConta_Codigo, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipodeConta_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TipodeContaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipodeconta_nome_Internalname, "Nome", "", "", lblTextblocktipodeconta_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipodeContaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTipodeConta_Nome_Internalname, StringUtil.RTrim( A871TipodeConta_Nome), StringUtil.RTrim( context.localUtil.Format( A871TipodeConta_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipodeConta_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_TipodeContaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipodeconta_descricao_Internalname, "Descri��o", "", "", lblTextblocktipodeconta_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipodeContaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTipodeConta_Descricao_Internalname, A872TipodeConta_Descricao, StringUtil.RTrim( context.localUtil.Format( A872TipodeConta_Descricao, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipodeConta_Descricao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_TipodeContaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipodeconta_tipo_Internalname, "Tipo", "", "", lblTextblocktipodeconta_tipo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipodeContaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbTipodeConta_Tipo, cmbTipodeConta_Tipo_Internalname, StringUtil.RTrim( A873TipodeConta_Tipo), 1, cmbTipodeConta_Tipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_TipodeContaGeneral.htm");
            cmbTipodeConta_Tipo.CurrentValue = StringUtil.RTrim( A873TipodeConta_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbTipodeConta_Tipo_Internalname, "Values", (String)(cmbTipodeConta_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipodeconta_contapaicod_Internalname, "Conta Pai", "", "", lblTextblocktipodeconta_contapaicod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_TipodeContaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTipoDeConta_ContaPaiCod_Internalname, StringUtil.RTrim( A885TipoDeConta_ContaPaiCod), StringUtil.RTrim( context.localUtil.Format( A885TipoDeConta_ContaPaiCod, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipoDeConta_ContaPaiCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_TipodeContaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_FJ2e( true) ;
         }
         else
         {
            wb_table2_8_FJ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A870TipodeConta_Codigo = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A870TipodeConta_Codigo", A870TipodeConta_Codigo);
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAFJ2( ) ;
         WSFJ2( ) ;
         WEFJ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA870TipodeConta_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAFJ2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "tipodecontageneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAFJ2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A870TipodeConta_Codigo = (String)getParm(obj,2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A870TipodeConta_Codigo", A870TipodeConta_Codigo);
         }
         wcpOA870TipodeConta_Codigo = cgiGet( sPrefix+"wcpOA870TipodeConta_Codigo");
         if ( ! GetJustCreated( ) && ( ( StringUtil.StrCmp(A870TipodeConta_Codigo, wcpOA870TipodeConta_Codigo) != 0 ) ) )
         {
            setjustcreated();
         }
         wcpOA870TipodeConta_Codigo = A870TipodeConta_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA870TipodeConta_Codigo = cgiGet( sPrefix+"A870TipodeConta_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA870TipodeConta_Codigo) > 0 )
         {
            A870TipodeConta_Codigo = cgiGet( sCtrlA870TipodeConta_Codigo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A870TipodeConta_Codigo", A870TipodeConta_Codigo);
         }
         else
         {
            A870TipodeConta_Codigo = cgiGet( sPrefix+"A870TipodeConta_Codigo_PARM");
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAFJ2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSFJ2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSFJ2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A870TipodeConta_Codigo_PARM", StringUtil.RTrim( A870TipodeConta_Codigo));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA870TipodeConta_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A870TipodeConta_Codigo_CTRL", StringUtil.RTrim( sCtrlA870TipodeConta_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEFJ2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117234869");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("tipodecontageneral.js", "?20203117234869");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocktipodeconta_codigo_Internalname = sPrefix+"TEXTBLOCKTIPODECONTA_CODIGO";
         edtTipodeConta_Codigo_Internalname = sPrefix+"TIPODECONTA_CODIGO";
         lblTextblocktipodeconta_nome_Internalname = sPrefix+"TEXTBLOCKTIPODECONTA_NOME";
         edtTipodeConta_Nome_Internalname = sPrefix+"TIPODECONTA_NOME";
         lblTextblocktipodeconta_descricao_Internalname = sPrefix+"TEXTBLOCKTIPODECONTA_DESCRICAO";
         edtTipodeConta_Descricao_Internalname = sPrefix+"TIPODECONTA_DESCRICAO";
         lblTextblocktipodeconta_tipo_Internalname = sPrefix+"TEXTBLOCKTIPODECONTA_TIPO";
         cmbTipodeConta_Tipo_Internalname = sPrefix+"TIPODECONTA_TIPO";
         lblTextblocktipodeconta_contapaicod_Internalname = sPrefix+"TEXTBLOCKTIPODECONTA_CONTAPAICOD";
         edtTipoDeConta_ContaPaiCod_Internalname = sPrefix+"TIPODECONTA_CONTAPAICOD";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtTipoDeConta_ContaPaiCod_Jsonclick = "";
         cmbTipodeConta_Tipo_Jsonclick = "";
         edtTipodeConta_Descricao_Jsonclick = "";
         edtTipodeConta_Nome_Jsonclick = "";
         edtTipodeConta_Codigo_Jsonclick = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13FJ2',iparms:[{av:'A870TipodeConta_Codigo',fld:'TIPODECONTA_CODIGO',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14FJ2',iparms:[{av:'A870TipodeConta_Codigo',fld:'TIPODECONTA_CODIGO',pic:'',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOA870TipodeConta_Codigo = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A871TipodeConta_Nome = "";
         A872TipodeConta_Descricao = "";
         A873TipodeConta_Tipo = "";
         A885TipoDeConta_ContaPaiCod = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00FJ2_A870TipodeConta_Codigo = new String[] {""} ;
         H00FJ2_A885TipoDeConta_ContaPaiCod = new String[] {""} ;
         H00FJ2_n885TipoDeConta_ContaPaiCod = new bool[] {false} ;
         H00FJ2_A873TipodeConta_Tipo = new String[] {""} ;
         H00FJ2_A872TipodeConta_Descricao = new String[] {""} ;
         H00FJ2_A871TipodeConta_Nome = new String[] {""} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV7TipodeConta_Codigo = "";
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblocktipodeconta_codigo_Jsonclick = "";
         lblTextblocktipodeconta_nome_Jsonclick = "";
         lblTextblocktipodeconta_descricao_Jsonclick = "";
         lblTextblocktipodeconta_tipo_Jsonclick = "";
         lblTextblocktipodeconta_contapaicod_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA870TipodeConta_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.tipodecontageneral__default(),
            new Object[][] {
                new Object[] {
               H00FJ2_A870TipodeConta_Codigo, H00FJ2_A885TipoDeConta_ContaPaiCod, H00FJ2_n885TipoDeConta_ContaPaiCod, H00FJ2_A873TipodeConta_Tipo, H00FJ2_A872TipodeConta_Descricao, H00FJ2_A871TipodeConta_Nome
               }
            }
         );
         AV14Pgmname = "TipodeContaGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "TipodeContaGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int idxLst ;
      private String A870TipodeConta_Codigo ;
      private String wcpOA870TipodeConta_Codigo ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A871TipodeConta_Nome ;
      private String A873TipodeConta_Tipo ;
      private String A885TipoDeConta_ContaPaiCod ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String edtTipodeConta_Nome_Internalname ;
      private String edtTipodeConta_Descricao_Internalname ;
      private String cmbTipodeConta_Tipo_Internalname ;
      private String edtTipoDeConta_ContaPaiCod_Internalname ;
      private String AV7TipodeConta_Codigo ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocktipodeconta_codigo_Internalname ;
      private String lblTextblocktipodeconta_codigo_Jsonclick ;
      private String edtTipodeConta_Codigo_Internalname ;
      private String edtTipodeConta_Codigo_Jsonclick ;
      private String lblTextblocktipodeconta_nome_Internalname ;
      private String lblTextblocktipodeconta_nome_Jsonclick ;
      private String edtTipodeConta_Nome_Jsonclick ;
      private String lblTextblocktipodeconta_descricao_Internalname ;
      private String lblTextblocktipodeconta_descricao_Jsonclick ;
      private String edtTipodeConta_Descricao_Jsonclick ;
      private String lblTextblocktipodeconta_tipo_Internalname ;
      private String lblTextblocktipodeconta_tipo_Jsonclick ;
      private String cmbTipodeConta_Tipo_Jsonclick ;
      private String lblTextblocktipodeconta_contapaicod_Internalname ;
      private String lblTextblocktipodeconta_contapaicod_Jsonclick ;
      private String edtTipoDeConta_ContaPaiCod_Jsonclick ;
      private String sCtrlA870TipodeConta_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n885TipoDeConta_ContaPaiCod ;
      private bool returnInSub ;
      private String A872TipodeConta_Descricao ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbTipodeConta_Tipo ;
      private IDataStoreProvider pr_default ;
      private String[] H00FJ2_A870TipodeConta_Codigo ;
      private String[] H00FJ2_A885TipoDeConta_ContaPaiCod ;
      private bool[] H00FJ2_n885TipoDeConta_ContaPaiCod ;
      private String[] H00FJ2_A873TipodeConta_Tipo ;
      private String[] H00FJ2_A872TipodeConta_Descricao ;
      private String[] H00FJ2_A871TipodeConta_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class tipodecontageneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00FJ2 ;
          prmH00FJ2 = new Object[] {
          new Object[] {"@TipodeConta_Codigo",SqlDbType.Char,20,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00FJ2", "SELECT [TipodeConta_Codigo], [TipoDeConta_ContaPaiCod], [TipodeConta_Tipo], [TipodeConta_Descricao], [TipodeConta_Nome] FROM [TipodeConta] WITH (NOLOCK) WHERE [TipodeConta_Codigo] = @TipodeConta_Codigo ORDER BY [TipodeConta_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FJ2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
       }
    }

 }

}
