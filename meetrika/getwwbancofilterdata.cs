/*
               File: GetWWBancoFilterData
        Description: Get WWBanco Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:16.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwbancofilterdata : GXProcedure
   {
      public getwwbancofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwbancofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwbancofilterdata objgetwwbancofilterdata;
         objgetwwbancofilterdata = new getwwbancofilterdata();
         objgetwwbancofilterdata.AV18DDOName = aP0_DDOName;
         objgetwwbancofilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetwwbancofilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetwwbancofilterdata.AV22OptionsJson = "" ;
         objgetwwbancofilterdata.AV25OptionsDescJson = "" ;
         objgetwwbancofilterdata.AV27OptionIndexesJson = "" ;
         objgetwwbancofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwbancofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwbancofilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwbancofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_BANCO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADBANCO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_BANCO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADBANCO_NUMEROOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("WWBancoGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWBancoGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("WWBancoGridState"), "");
         }
         AV50GXV1 = 1;
         while ( AV50GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV50GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFBANCO_CODIGO") == 0 )
            {
               AV10TFBanco_Codigo = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
               AV11TFBanco_Codigo_To = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFBANCO_NOME") == 0 )
            {
               AV12TFBanco_Nome = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFBANCO_NOME_SEL") == 0 )
            {
               AV13TFBanco_Nome_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFBANCO_NUMERO") == 0 )
            {
               AV14TFBanco_Numero = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFBANCO_NUMERO_SEL") == 0 )
            {
               AV15TFBanco_Numero_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            AV50GXV1 = (int)(AV50GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "BANCO_NOME") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV45Banco_Nome1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV37DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV38DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "BANCO_NOME") == 0 )
               {
                  AV39DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV46Banco_Nome2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV41DynamicFiltersEnabled3 = true;
                  AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(3));
                  AV42DynamicFiltersSelector3 = AV33GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "BANCO_NOME") == 0 )
                  {
                     AV43DynamicFiltersOperator3 = AV33GridStateDynamicFilter.gxTpr_Operator;
                     AV47Banco_Nome3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADBANCO_NOMEOPTIONS' Routine */
         AV12TFBanco_Nome = AV16SearchTxt;
         AV13TFBanco_Nome_Sel = "";
         AV52WWBancoDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV53WWBancoDS_2_Dynamicfiltersoperator1 = AV35DynamicFiltersOperator1;
         AV54WWBancoDS_3_Banco_nome1 = AV45Banco_Nome1;
         AV55WWBancoDS_4_Dynamicfiltersenabled2 = AV37DynamicFiltersEnabled2;
         AV56WWBancoDS_5_Dynamicfiltersselector2 = AV38DynamicFiltersSelector2;
         AV57WWBancoDS_6_Dynamicfiltersoperator2 = AV39DynamicFiltersOperator2;
         AV58WWBancoDS_7_Banco_nome2 = AV46Banco_Nome2;
         AV59WWBancoDS_8_Dynamicfiltersenabled3 = AV41DynamicFiltersEnabled3;
         AV60WWBancoDS_9_Dynamicfiltersselector3 = AV42DynamicFiltersSelector3;
         AV61WWBancoDS_10_Dynamicfiltersoperator3 = AV43DynamicFiltersOperator3;
         AV62WWBancoDS_11_Banco_nome3 = AV47Banco_Nome3;
         AV63WWBancoDS_12_Tfbanco_codigo = AV10TFBanco_Codigo;
         AV64WWBancoDS_13_Tfbanco_codigo_to = AV11TFBanco_Codigo_To;
         AV65WWBancoDS_14_Tfbanco_nome = AV12TFBanco_Nome;
         AV66WWBancoDS_15_Tfbanco_nome_sel = AV13TFBanco_Nome_Sel;
         AV67WWBancoDS_16_Tfbanco_numero = AV14TFBanco_Numero;
         AV68WWBancoDS_17_Tfbanco_numero_sel = AV15TFBanco_Numero_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV52WWBancoDS_1_Dynamicfiltersselector1 ,
                                              AV53WWBancoDS_2_Dynamicfiltersoperator1 ,
                                              AV54WWBancoDS_3_Banco_nome1 ,
                                              AV55WWBancoDS_4_Dynamicfiltersenabled2 ,
                                              AV56WWBancoDS_5_Dynamicfiltersselector2 ,
                                              AV57WWBancoDS_6_Dynamicfiltersoperator2 ,
                                              AV58WWBancoDS_7_Banco_nome2 ,
                                              AV59WWBancoDS_8_Dynamicfiltersenabled3 ,
                                              AV60WWBancoDS_9_Dynamicfiltersselector3 ,
                                              AV61WWBancoDS_10_Dynamicfiltersoperator3 ,
                                              AV62WWBancoDS_11_Banco_nome3 ,
                                              AV63WWBancoDS_12_Tfbanco_codigo ,
                                              AV64WWBancoDS_13_Tfbanco_codigo_to ,
                                              AV66WWBancoDS_15_Tfbanco_nome_sel ,
                                              AV65WWBancoDS_14_Tfbanco_nome ,
                                              AV68WWBancoDS_17_Tfbanco_numero_sel ,
                                              AV67WWBancoDS_16_Tfbanco_numero ,
                                              A20Banco_Nome ,
                                              A18Banco_Codigo ,
                                              A19Banco_Numero },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING
                                              }
         });
         lV54WWBancoDS_3_Banco_nome1 = StringUtil.PadR( StringUtil.RTrim( AV54WWBancoDS_3_Banco_nome1), 50, "%");
         lV54WWBancoDS_3_Banco_nome1 = StringUtil.PadR( StringUtil.RTrim( AV54WWBancoDS_3_Banco_nome1), 50, "%");
         lV58WWBancoDS_7_Banco_nome2 = StringUtil.PadR( StringUtil.RTrim( AV58WWBancoDS_7_Banco_nome2), 50, "%");
         lV58WWBancoDS_7_Banco_nome2 = StringUtil.PadR( StringUtil.RTrim( AV58WWBancoDS_7_Banco_nome2), 50, "%");
         lV62WWBancoDS_11_Banco_nome3 = StringUtil.PadR( StringUtil.RTrim( AV62WWBancoDS_11_Banco_nome3), 50, "%");
         lV62WWBancoDS_11_Banco_nome3 = StringUtil.PadR( StringUtil.RTrim( AV62WWBancoDS_11_Banco_nome3), 50, "%");
         lV65WWBancoDS_14_Tfbanco_nome = StringUtil.PadR( StringUtil.RTrim( AV65WWBancoDS_14_Tfbanco_nome), 50, "%");
         lV67WWBancoDS_16_Tfbanco_numero = StringUtil.PadR( StringUtil.RTrim( AV67WWBancoDS_16_Tfbanco_numero), 6, "%");
         /* Using cursor P00EV2 */
         pr_default.execute(0, new Object[] {lV54WWBancoDS_3_Banco_nome1, lV54WWBancoDS_3_Banco_nome1, lV58WWBancoDS_7_Banco_nome2, lV58WWBancoDS_7_Banco_nome2, lV62WWBancoDS_11_Banco_nome3, lV62WWBancoDS_11_Banco_nome3, AV63WWBancoDS_12_Tfbanco_codigo, AV64WWBancoDS_13_Tfbanco_codigo_to, lV65WWBancoDS_14_Tfbanco_nome, AV66WWBancoDS_15_Tfbanco_nome_sel, lV67WWBancoDS_16_Tfbanco_numero, AV68WWBancoDS_17_Tfbanco_numero_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKEV2 = false;
            A20Banco_Nome = P00EV2_A20Banco_Nome[0];
            A19Banco_Numero = P00EV2_A19Banco_Numero[0];
            A18Banco_Codigo = P00EV2_A18Banco_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00EV2_A20Banco_Nome[0], A20Banco_Nome) == 0 ) )
            {
               BRKEV2 = false;
               A18Banco_Codigo = P00EV2_A18Banco_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKEV2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A20Banco_Nome)) )
            {
               AV20Option = A20Banco_Nome;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKEV2 )
            {
               BRKEV2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADBANCO_NUMEROOPTIONS' Routine */
         AV14TFBanco_Numero = AV16SearchTxt;
         AV15TFBanco_Numero_Sel = "";
         AV52WWBancoDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV53WWBancoDS_2_Dynamicfiltersoperator1 = AV35DynamicFiltersOperator1;
         AV54WWBancoDS_3_Banco_nome1 = AV45Banco_Nome1;
         AV55WWBancoDS_4_Dynamicfiltersenabled2 = AV37DynamicFiltersEnabled2;
         AV56WWBancoDS_5_Dynamicfiltersselector2 = AV38DynamicFiltersSelector2;
         AV57WWBancoDS_6_Dynamicfiltersoperator2 = AV39DynamicFiltersOperator2;
         AV58WWBancoDS_7_Banco_nome2 = AV46Banco_Nome2;
         AV59WWBancoDS_8_Dynamicfiltersenabled3 = AV41DynamicFiltersEnabled3;
         AV60WWBancoDS_9_Dynamicfiltersselector3 = AV42DynamicFiltersSelector3;
         AV61WWBancoDS_10_Dynamicfiltersoperator3 = AV43DynamicFiltersOperator3;
         AV62WWBancoDS_11_Banco_nome3 = AV47Banco_Nome3;
         AV63WWBancoDS_12_Tfbanco_codigo = AV10TFBanco_Codigo;
         AV64WWBancoDS_13_Tfbanco_codigo_to = AV11TFBanco_Codigo_To;
         AV65WWBancoDS_14_Tfbanco_nome = AV12TFBanco_Nome;
         AV66WWBancoDS_15_Tfbanco_nome_sel = AV13TFBanco_Nome_Sel;
         AV67WWBancoDS_16_Tfbanco_numero = AV14TFBanco_Numero;
         AV68WWBancoDS_17_Tfbanco_numero_sel = AV15TFBanco_Numero_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV52WWBancoDS_1_Dynamicfiltersselector1 ,
                                              AV53WWBancoDS_2_Dynamicfiltersoperator1 ,
                                              AV54WWBancoDS_3_Banco_nome1 ,
                                              AV55WWBancoDS_4_Dynamicfiltersenabled2 ,
                                              AV56WWBancoDS_5_Dynamicfiltersselector2 ,
                                              AV57WWBancoDS_6_Dynamicfiltersoperator2 ,
                                              AV58WWBancoDS_7_Banco_nome2 ,
                                              AV59WWBancoDS_8_Dynamicfiltersenabled3 ,
                                              AV60WWBancoDS_9_Dynamicfiltersselector3 ,
                                              AV61WWBancoDS_10_Dynamicfiltersoperator3 ,
                                              AV62WWBancoDS_11_Banco_nome3 ,
                                              AV63WWBancoDS_12_Tfbanco_codigo ,
                                              AV64WWBancoDS_13_Tfbanco_codigo_to ,
                                              AV66WWBancoDS_15_Tfbanco_nome_sel ,
                                              AV65WWBancoDS_14_Tfbanco_nome ,
                                              AV68WWBancoDS_17_Tfbanco_numero_sel ,
                                              AV67WWBancoDS_16_Tfbanco_numero ,
                                              A20Banco_Nome ,
                                              A18Banco_Codigo ,
                                              A19Banco_Numero },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING
                                              }
         });
         lV54WWBancoDS_3_Banco_nome1 = StringUtil.PadR( StringUtil.RTrim( AV54WWBancoDS_3_Banco_nome1), 50, "%");
         lV54WWBancoDS_3_Banco_nome1 = StringUtil.PadR( StringUtil.RTrim( AV54WWBancoDS_3_Banco_nome1), 50, "%");
         lV58WWBancoDS_7_Banco_nome2 = StringUtil.PadR( StringUtil.RTrim( AV58WWBancoDS_7_Banco_nome2), 50, "%");
         lV58WWBancoDS_7_Banco_nome2 = StringUtil.PadR( StringUtil.RTrim( AV58WWBancoDS_7_Banco_nome2), 50, "%");
         lV62WWBancoDS_11_Banco_nome3 = StringUtil.PadR( StringUtil.RTrim( AV62WWBancoDS_11_Banco_nome3), 50, "%");
         lV62WWBancoDS_11_Banco_nome3 = StringUtil.PadR( StringUtil.RTrim( AV62WWBancoDS_11_Banco_nome3), 50, "%");
         lV65WWBancoDS_14_Tfbanco_nome = StringUtil.PadR( StringUtil.RTrim( AV65WWBancoDS_14_Tfbanco_nome), 50, "%");
         lV67WWBancoDS_16_Tfbanco_numero = StringUtil.PadR( StringUtil.RTrim( AV67WWBancoDS_16_Tfbanco_numero), 6, "%");
         /* Using cursor P00EV3 */
         pr_default.execute(1, new Object[] {lV54WWBancoDS_3_Banco_nome1, lV54WWBancoDS_3_Banco_nome1, lV58WWBancoDS_7_Banco_nome2, lV58WWBancoDS_7_Banco_nome2, lV62WWBancoDS_11_Banco_nome3, lV62WWBancoDS_11_Banco_nome3, AV63WWBancoDS_12_Tfbanco_codigo, AV64WWBancoDS_13_Tfbanco_codigo_to, lV65WWBancoDS_14_Tfbanco_nome, AV66WWBancoDS_15_Tfbanco_nome_sel, lV67WWBancoDS_16_Tfbanco_numero, AV68WWBancoDS_17_Tfbanco_numero_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKEV4 = false;
            A19Banco_Numero = P00EV3_A19Banco_Numero[0];
            A18Banco_Codigo = P00EV3_A18Banco_Codigo[0];
            A20Banco_Nome = P00EV3_A20Banco_Nome[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00EV3_A19Banco_Numero[0], A19Banco_Numero) == 0 ) )
            {
               BRKEV4 = false;
               A18Banco_Codigo = P00EV3_A18Banco_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKEV4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A19Banco_Numero)) )
            {
               AV20Option = A19Banco_Numero;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKEV4 )
            {
               BRKEV4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFBanco_Nome = "";
         AV13TFBanco_Nome_Sel = "";
         AV14TFBanco_Numero = "";
         AV15TFBanco_Numero_Sel = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV45Banco_Nome1 = "";
         AV38DynamicFiltersSelector2 = "";
         AV46Banco_Nome2 = "";
         AV42DynamicFiltersSelector3 = "";
         AV47Banco_Nome3 = "";
         AV52WWBancoDS_1_Dynamicfiltersselector1 = "";
         AV54WWBancoDS_3_Banco_nome1 = "";
         AV56WWBancoDS_5_Dynamicfiltersselector2 = "";
         AV58WWBancoDS_7_Banco_nome2 = "";
         AV60WWBancoDS_9_Dynamicfiltersselector3 = "";
         AV62WWBancoDS_11_Banco_nome3 = "";
         AV65WWBancoDS_14_Tfbanco_nome = "";
         AV66WWBancoDS_15_Tfbanco_nome_sel = "";
         AV67WWBancoDS_16_Tfbanco_numero = "";
         AV68WWBancoDS_17_Tfbanco_numero_sel = "";
         scmdbuf = "";
         lV54WWBancoDS_3_Banco_nome1 = "";
         lV58WWBancoDS_7_Banco_nome2 = "";
         lV62WWBancoDS_11_Banco_nome3 = "";
         lV65WWBancoDS_14_Tfbanco_nome = "";
         lV67WWBancoDS_16_Tfbanco_numero = "";
         A20Banco_Nome = "";
         A19Banco_Numero = "";
         P00EV2_A20Banco_Nome = new String[] {""} ;
         P00EV2_A19Banco_Numero = new String[] {""} ;
         P00EV2_A18Banco_Codigo = new int[1] ;
         AV20Option = "";
         P00EV3_A19Banco_Numero = new String[] {""} ;
         P00EV3_A18Banco_Codigo = new int[1] ;
         P00EV3_A20Banco_Nome = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwbancofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00EV2_A20Banco_Nome, P00EV2_A19Banco_Numero, P00EV2_A18Banco_Codigo
               }
               , new Object[] {
               P00EV3_A19Banco_Numero, P00EV3_A18Banco_Codigo, P00EV3_A20Banco_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV35DynamicFiltersOperator1 ;
      private short AV39DynamicFiltersOperator2 ;
      private short AV43DynamicFiltersOperator3 ;
      private short AV53WWBancoDS_2_Dynamicfiltersoperator1 ;
      private short AV57WWBancoDS_6_Dynamicfiltersoperator2 ;
      private short AV61WWBancoDS_10_Dynamicfiltersoperator3 ;
      private int AV50GXV1 ;
      private int AV10TFBanco_Codigo ;
      private int AV11TFBanco_Codigo_To ;
      private int AV63WWBancoDS_12_Tfbanco_codigo ;
      private int AV64WWBancoDS_13_Tfbanco_codigo_to ;
      private int A18Banco_Codigo ;
      private long AV28count ;
      private String AV12TFBanco_Nome ;
      private String AV13TFBanco_Nome_Sel ;
      private String AV14TFBanco_Numero ;
      private String AV15TFBanco_Numero_Sel ;
      private String AV45Banco_Nome1 ;
      private String AV46Banco_Nome2 ;
      private String AV47Banco_Nome3 ;
      private String AV54WWBancoDS_3_Banco_nome1 ;
      private String AV58WWBancoDS_7_Banco_nome2 ;
      private String AV62WWBancoDS_11_Banco_nome3 ;
      private String AV65WWBancoDS_14_Tfbanco_nome ;
      private String AV66WWBancoDS_15_Tfbanco_nome_sel ;
      private String AV67WWBancoDS_16_Tfbanco_numero ;
      private String AV68WWBancoDS_17_Tfbanco_numero_sel ;
      private String scmdbuf ;
      private String lV54WWBancoDS_3_Banco_nome1 ;
      private String lV58WWBancoDS_7_Banco_nome2 ;
      private String lV62WWBancoDS_11_Banco_nome3 ;
      private String lV65WWBancoDS_14_Tfbanco_nome ;
      private String lV67WWBancoDS_16_Tfbanco_numero ;
      private String A20Banco_Nome ;
      private String A19Banco_Numero ;
      private bool returnInSub ;
      private bool AV37DynamicFiltersEnabled2 ;
      private bool AV41DynamicFiltersEnabled3 ;
      private bool AV55WWBancoDS_4_Dynamicfiltersenabled2 ;
      private bool AV59WWBancoDS_8_Dynamicfiltersenabled3 ;
      private bool BRKEV2 ;
      private bool BRKEV4 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV38DynamicFiltersSelector2 ;
      private String AV42DynamicFiltersSelector3 ;
      private String AV52WWBancoDS_1_Dynamicfiltersselector1 ;
      private String AV56WWBancoDS_5_Dynamicfiltersselector2 ;
      private String AV60WWBancoDS_9_Dynamicfiltersselector3 ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00EV2_A20Banco_Nome ;
      private String[] P00EV2_A19Banco_Numero ;
      private int[] P00EV2_A18Banco_Codigo ;
      private String[] P00EV3_A19Banco_Numero ;
      private int[] P00EV3_A18Banco_Codigo ;
      private String[] P00EV3_A20Banco_Nome ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getwwbancofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00EV2( IGxContext context ,
                                             String AV52WWBancoDS_1_Dynamicfiltersselector1 ,
                                             short AV53WWBancoDS_2_Dynamicfiltersoperator1 ,
                                             String AV54WWBancoDS_3_Banco_nome1 ,
                                             bool AV55WWBancoDS_4_Dynamicfiltersenabled2 ,
                                             String AV56WWBancoDS_5_Dynamicfiltersselector2 ,
                                             short AV57WWBancoDS_6_Dynamicfiltersoperator2 ,
                                             String AV58WWBancoDS_7_Banco_nome2 ,
                                             bool AV59WWBancoDS_8_Dynamicfiltersenabled3 ,
                                             String AV60WWBancoDS_9_Dynamicfiltersselector3 ,
                                             short AV61WWBancoDS_10_Dynamicfiltersoperator3 ,
                                             String AV62WWBancoDS_11_Banco_nome3 ,
                                             int AV63WWBancoDS_12_Tfbanco_codigo ,
                                             int AV64WWBancoDS_13_Tfbanco_codigo_to ,
                                             String AV66WWBancoDS_15_Tfbanco_nome_sel ,
                                             String AV65WWBancoDS_14_Tfbanco_nome ,
                                             String AV68WWBancoDS_17_Tfbanco_numero_sel ,
                                             String AV67WWBancoDS_16_Tfbanco_numero ,
                                             String A20Banco_Nome ,
                                             int A18Banco_Codigo ,
                                             String A19Banco_Numero )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [12] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Banco_Nome], [Banco_Numero], [Banco_Codigo] FROM [Banco] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV52WWBancoDS_1_Dynamicfiltersselector1, "BANCO_NOME") == 0 ) && ( AV53WWBancoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWBancoDS_3_Banco_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Nome] like @lV54WWBancoDS_3_Banco_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Nome] like @lV54WWBancoDS_3_Banco_nome1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWBancoDS_1_Dynamicfiltersselector1, "BANCO_NOME") == 0 ) && ( AV53WWBancoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWBancoDS_3_Banco_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Nome] like '%' + @lV54WWBancoDS_3_Banco_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Nome] like '%' + @lV54WWBancoDS_3_Banco_nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV55WWBancoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV56WWBancoDS_5_Dynamicfiltersselector2, "BANCO_NOME") == 0 ) && ( AV57WWBancoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWBancoDS_7_Banco_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Nome] like @lV58WWBancoDS_7_Banco_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Nome] like @lV58WWBancoDS_7_Banco_nome2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV55WWBancoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV56WWBancoDS_5_Dynamicfiltersselector2, "BANCO_NOME") == 0 ) && ( AV57WWBancoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWBancoDS_7_Banco_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Nome] like '%' + @lV58WWBancoDS_7_Banco_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Nome] like '%' + @lV58WWBancoDS_7_Banco_nome2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV59WWBancoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWBancoDS_9_Dynamicfiltersselector3, "BANCO_NOME") == 0 ) && ( AV61WWBancoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWBancoDS_11_Banco_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Nome] like @lV62WWBancoDS_11_Banco_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Nome] like @lV62WWBancoDS_11_Banco_nome3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV59WWBancoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWBancoDS_9_Dynamicfiltersselector3, "BANCO_NOME") == 0 ) && ( AV61WWBancoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWBancoDS_11_Banco_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Nome] like '%' + @lV62WWBancoDS_11_Banco_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Nome] like '%' + @lV62WWBancoDS_11_Banco_nome3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV63WWBancoDS_12_Tfbanco_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Codigo] >= @AV63WWBancoDS_12_Tfbanco_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Codigo] >= @AV63WWBancoDS_12_Tfbanco_codigo)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV64WWBancoDS_13_Tfbanco_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Codigo] <= @AV64WWBancoDS_13_Tfbanco_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Codigo] <= @AV64WWBancoDS_13_Tfbanco_codigo_to)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWBancoDS_15_Tfbanco_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWBancoDS_14_Tfbanco_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Nome] like @lV65WWBancoDS_14_Tfbanco_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Nome] like @lV65WWBancoDS_14_Tfbanco_nome)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWBancoDS_15_Tfbanco_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Nome] = @AV66WWBancoDS_15_Tfbanco_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Nome] = @AV66WWBancoDS_15_Tfbanco_nome_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWBancoDS_17_Tfbanco_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWBancoDS_16_Tfbanco_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Numero] like @lV67WWBancoDS_16_Tfbanco_numero)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Numero] like @lV67WWBancoDS_16_Tfbanco_numero)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWBancoDS_17_Tfbanco_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Numero] = @AV68WWBancoDS_17_Tfbanco_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Numero] = @AV68WWBancoDS_17_Tfbanco_numero_sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Banco_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00EV3( IGxContext context ,
                                             String AV52WWBancoDS_1_Dynamicfiltersselector1 ,
                                             short AV53WWBancoDS_2_Dynamicfiltersoperator1 ,
                                             String AV54WWBancoDS_3_Banco_nome1 ,
                                             bool AV55WWBancoDS_4_Dynamicfiltersenabled2 ,
                                             String AV56WWBancoDS_5_Dynamicfiltersselector2 ,
                                             short AV57WWBancoDS_6_Dynamicfiltersoperator2 ,
                                             String AV58WWBancoDS_7_Banco_nome2 ,
                                             bool AV59WWBancoDS_8_Dynamicfiltersenabled3 ,
                                             String AV60WWBancoDS_9_Dynamicfiltersselector3 ,
                                             short AV61WWBancoDS_10_Dynamicfiltersoperator3 ,
                                             String AV62WWBancoDS_11_Banco_nome3 ,
                                             int AV63WWBancoDS_12_Tfbanco_codigo ,
                                             int AV64WWBancoDS_13_Tfbanco_codigo_to ,
                                             String AV66WWBancoDS_15_Tfbanco_nome_sel ,
                                             String AV65WWBancoDS_14_Tfbanco_nome ,
                                             String AV68WWBancoDS_17_Tfbanco_numero_sel ,
                                             String AV67WWBancoDS_16_Tfbanco_numero ,
                                             String A20Banco_Nome ,
                                             int A18Banco_Codigo ,
                                             String A19Banco_Numero )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [12] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [Banco_Numero], [Banco_Codigo], [Banco_Nome] FROM [Banco] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV52WWBancoDS_1_Dynamicfiltersselector1, "BANCO_NOME") == 0 ) && ( AV53WWBancoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWBancoDS_3_Banco_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Nome] like @lV54WWBancoDS_3_Banco_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Nome] like @lV54WWBancoDS_3_Banco_nome1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWBancoDS_1_Dynamicfiltersselector1, "BANCO_NOME") == 0 ) && ( AV53WWBancoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWBancoDS_3_Banco_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Nome] like '%' + @lV54WWBancoDS_3_Banco_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Nome] like '%' + @lV54WWBancoDS_3_Banco_nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV55WWBancoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV56WWBancoDS_5_Dynamicfiltersselector2, "BANCO_NOME") == 0 ) && ( AV57WWBancoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWBancoDS_7_Banco_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Nome] like @lV58WWBancoDS_7_Banco_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Nome] like @lV58WWBancoDS_7_Banco_nome2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV55WWBancoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV56WWBancoDS_5_Dynamicfiltersselector2, "BANCO_NOME") == 0 ) && ( AV57WWBancoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWBancoDS_7_Banco_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Nome] like '%' + @lV58WWBancoDS_7_Banco_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Nome] like '%' + @lV58WWBancoDS_7_Banco_nome2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV59WWBancoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWBancoDS_9_Dynamicfiltersselector3, "BANCO_NOME") == 0 ) && ( AV61WWBancoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWBancoDS_11_Banco_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Nome] like @lV62WWBancoDS_11_Banco_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Nome] like @lV62WWBancoDS_11_Banco_nome3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV59WWBancoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWBancoDS_9_Dynamicfiltersselector3, "BANCO_NOME") == 0 ) && ( AV61WWBancoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWBancoDS_11_Banco_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Nome] like '%' + @lV62WWBancoDS_11_Banco_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Nome] like '%' + @lV62WWBancoDS_11_Banco_nome3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV63WWBancoDS_12_Tfbanco_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Codigo] >= @AV63WWBancoDS_12_Tfbanco_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Codigo] >= @AV63WWBancoDS_12_Tfbanco_codigo)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (0==AV64WWBancoDS_13_Tfbanco_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Codigo] <= @AV64WWBancoDS_13_Tfbanco_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Codigo] <= @AV64WWBancoDS_13_Tfbanco_codigo_to)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWBancoDS_15_Tfbanco_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWBancoDS_14_Tfbanco_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Nome] like @lV65WWBancoDS_14_Tfbanco_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Nome] like @lV65WWBancoDS_14_Tfbanco_nome)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWBancoDS_15_Tfbanco_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Nome] = @AV66WWBancoDS_15_Tfbanco_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Nome] = @AV66WWBancoDS_15_Tfbanco_nome_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWBancoDS_17_Tfbanco_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWBancoDS_16_Tfbanco_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Numero] like @lV67WWBancoDS_16_Tfbanco_numero)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Numero] like @lV67WWBancoDS_16_Tfbanco_numero)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWBancoDS_17_Tfbanco_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Banco_Numero] = @AV68WWBancoDS_17_Tfbanco_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Banco_Numero] = @AV68WWBancoDS_17_Tfbanco_numero_sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Banco_Numero]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00EV2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] );
               case 1 :
                     return conditional_P00EV3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00EV2 ;
          prmP00EV2 = new Object[] {
          new Object[] {"@lV54WWBancoDS_3_Banco_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWBancoDS_3_Banco_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV58WWBancoDS_7_Banco_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV58WWBancoDS_7_Banco_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62WWBancoDS_11_Banco_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62WWBancoDS_11_Banco_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV63WWBancoDS_12_Tfbanco_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV64WWBancoDS_13_Tfbanco_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV65WWBancoDS_14_Tfbanco_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV66WWBancoDS_15_Tfbanco_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWBancoDS_16_Tfbanco_numero",SqlDbType.Char,6,0} ,
          new Object[] {"@AV68WWBancoDS_17_Tfbanco_numero_sel",SqlDbType.Char,6,0}
          } ;
          Object[] prmP00EV3 ;
          prmP00EV3 = new Object[] {
          new Object[] {"@lV54WWBancoDS_3_Banco_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWBancoDS_3_Banco_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV58WWBancoDS_7_Banco_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV58WWBancoDS_7_Banco_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62WWBancoDS_11_Banco_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62WWBancoDS_11_Banco_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV63WWBancoDS_12_Tfbanco_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV64WWBancoDS_13_Tfbanco_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV65WWBancoDS_14_Tfbanco_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV66WWBancoDS_15_Tfbanco_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWBancoDS_16_Tfbanco_numero",SqlDbType.Char,6,0} ,
          new Object[] {"@AV68WWBancoDS_17_Tfbanco_numero_sel",SqlDbType.Char,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00EV2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00EV2,100,0,true,false )
             ,new CursorDef("P00EV3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00EV3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 6) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 6) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwbancofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwbancofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwbancofilterdata") )
          {
             return  ;
          }
          getwwbancofilterdata worker = new getwwbancofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
