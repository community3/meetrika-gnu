/*
               File: Guia
        Description: Guia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:17:7.37
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class guia : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A230Guia_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A230Guia_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A230Guia_AreaTrabalhoCod), 6, 0)));
            A93Guia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A230Guia_AreaTrabalhoCod, A93Guia_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_9") == 0 )
         {
            A230Guia_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A230Guia_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A230Guia_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_9( A230Guia_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Guia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Guia_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGUIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Guia_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Guia", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtGuia_AreaTrabalhoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public guia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public guia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Guia_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Guia_Codigo = aP1_Guia_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0H18( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0H18e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0H18( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_0H18( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_0H18e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_45_0H18( true) ;
         }
         return  ;
      }

      protected void wb_table3_45_0H18e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0H18e( true) ;
         }
         else
         {
            wb_table1_2_0H18e( false) ;
         }
      }

      protected void wb_table3_45_0H18( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Guia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Guia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Guia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_45_0H18e( true) ;
         }
         else
         {
            wb_table3_45_0H18e( false) ;
         }
      }

      protected void wb_table2_5_0H18( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_0H18( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_0H18e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0H18e( true) ;
         }
         else
         {
            wb_table2_5_0H18e( false) ;
         }
      }

      protected void wb_table4_13_0H18( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockguia_codigo_Internalname, "C�digo", "", "", lblTextblockguia_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Guia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtGuia_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A93Guia_Codigo), 6, 0, ",", "")), ((edtGuia_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGuia_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtGuia_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Guia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockguia_areatrabalhocod_Internalname, "�rea de Trabalho", "", "", lblTextblockguia_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Guia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGuia_AreaTrabalhoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A230Guia_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A230Guia_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGuia_AreaTrabalhoCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtGuia_AreaTrabalhoCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Guia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtGuia_AreaTrabalhoDes_Internalname, A231Guia_AreaTrabalhoDes, StringUtil.RTrim( context.localUtil.Format( A231Guia_AreaTrabalhoDes, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGuia_AreaTrabalhoDes_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtGuia_AreaTrabalhoDes_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_Guia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockguia_nome_Internalname, "Guia", "", "", lblTextblockguia_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Guia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGuia_Nome_Internalname, StringUtil.RTrim( A94Guia_Nome), StringUtil.RTrim( context.localUtil.Format( A94Guia_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGuia_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtGuia_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Guia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockguia_versao_Internalname, "Vers�o", "", "", lblTextblockguia_versao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Guia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGuia_Versao_Internalname, StringUtil.RTrim( A95Guia_Versao), StringUtil.RTrim( context.localUtil.Format( A95Guia_Versao, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGuia_Versao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtGuia_Versao_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "VersaoGuia", "left", true, "HLP_Guia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockguia_ano_Internalname, "Ano", "", "", lblTextblockguia_ano_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Guia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGuia_Ano_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A96Guia_Ano), 4, 0, ",", "")), ((edtGuia_Ano_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A96Guia_Ano), "ZZZ9")) : context.localUtil.Format( (decimal)(A96Guia_Ano), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGuia_Ano_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtGuia_Ano_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_Guia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_0H18e( true) ;
         }
         else
         {
            wb_table4_13_0H18e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E110H2 */
         E110H2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A93Guia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGuia_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtGuia_AreaTrabalhoCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtGuia_AreaTrabalhoCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "GUIA_AREATRABALHOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtGuia_AreaTrabalhoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A230Guia_AreaTrabalhoCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A230Guia_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A230Guia_AreaTrabalhoCod), 6, 0)));
               }
               else
               {
                  A230Guia_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtGuia_AreaTrabalhoCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A230Guia_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A230Guia_AreaTrabalhoCod), 6, 0)));
               }
               A231Guia_AreaTrabalhoDes = StringUtil.Upper( cgiGet( edtGuia_AreaTrabalhoDes_Internalname));
               n231Guia_AreaTrabalhoDes = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A231Guia_AreaTrabalhoDes", A231Guia_AreaTrabalhoDes);
               A94Guia_Nome = StringUtil.Upper( cgiGet( edtGuia_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94Guia_Nome", A94Guia_Nome);
               A95Guia_Versao = cgiGet( edtGuia_Versao_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A95Guia_Versao", A95Guia_Versao);
               if ( ( ( context.localUtil.CToN( cgiGet( edtGuia_Ano_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtGuia_Ano_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "GUIA_ANO");
                  AnyError = 1;
                  GX_FocusControl = edtGuia_Ano_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A96Guia_Ano = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A96Guia_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A96Guia_Ano), 4, 0)));
               }
               else
               {
                  A96Guia_Ano = (short)(context.localUtil.CToN( cgiGet( edtGuia_Ano_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A96Guia_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A96Guia_Ano), 4, 0)));
               }
               /* Read saved values. */
               Z93Guia_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z93Guia_Codigo"), ",", "."));
               Z94Guia_Nome = cgiGet( "Z94Guia_Nome");
               Z95Guia_Versao = cgiGet( "Z95Guia_Versao");
               Z96Guia_Ano = (short)(context.localUtil.CToN( cgiGet( "Z96Guia_Ano"), ",", "."));
               Z230Guia_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z230Guia_AreaTrabalhoCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N230Guia_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "N230Guia_AreaTrabalhoCod"), ",", "."));
               AV7Guia_Codigo = (int)(context.localUtil.CToN( cgiGet( "vGUIA_CODIGO"), ",", "."));
               AV11Insert_Guia_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_GUIA_AREATRABALHOCOD"), ",", "."));
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Guia";
               A93Guia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGuia_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A93Guia_Codigo != Z93Guia_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("guia:[SecurityCheckFailed value for]"+"Guia_Codigo:"+context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("guia:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A93Guia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode18 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode18;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound18 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0H0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "GUIA_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtGuia_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110H2 */
                           E110H2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120H2 */
                           E120H2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E120H2 */
            E120H2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0H18( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0H18( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0H0( )
      {
         BeforeValidate0H18( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0H18( ) ;
            }
            else
            {
               CheckExtendedTable0H18( ) ;
               CloseExtendedTableCursors0H18( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0H0( )
      {
      }

      protected void E110H2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Guia_AreaTrabalhoCod") == 0 )
               {
                  AV11Insert_Guia_AreaTrabalhoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Guia_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Guia_AreaTrabalhoCod), 6, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
      }

      protected void E120H2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwguia.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM0H18( short GX_JID )
      {
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z94Guia_Nome = T000H3_A94Guia_Nome[0];
               Z95Guia_Versao = T000H3_A95Guia_Versao[0];
               Z96Guia_Ano = T000H3_A96Guia_Ano[0];
               Z230Guia_AreaTrabalhoCod = T000H3_A230Guia_AreaTrabalhoCod[0];
            }
            else
            {
               Z94Guia_Nome = A94Guia_Nome;
               Z95Guia_Versao = A95Guia_Versao;
               Z96Guia_Ano = A96Guia_Ano;
               Z230Guia_AreaTrabalhoCod = A230Guia_AreaTrabalhoCod;
            }
         }
         if ( GX_JID == -8 )
         {
            Z94Guia_Nome = A94Guia_Nome;
            Z95Guia_Versao = A95Guia_Versao;
            Z96Guia_Ano = A96Guia_Ano;
            Z230Guia_AreaTrabalhoCod = A230Guia_AreaTrabalhoCod;
            Z93Guia_Codigo = A93Guia_Codigo;
            Z231Guia_AreaTrabalhoDes = A231Guia_AreaTrabalhoDes;
         }
      }

      protected void standaloneNotModal( )
      {
         edtGuia_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGuia_Codigo_Enabled), 5, 0)));
         AV13Pgmname = "Guia";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         edtGuia_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGuia_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Guia_Codigo) )
         {
            A93Guia_Codigo = AV7Guia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Guia_AreaTrabalhoCod) )
         {
            edtGuia_AreaTrabalhoCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGuia_AreaTrabalhoCod_Enabled), 5, 0)));
         }
         else
         {
            edtGuia_AreaTrabalhoCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGuia_AreaTrabalhoCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Guia_AreaTrabalhoCod) )
         {
            A230Guia_AreaTrabalhoCod = AV11Insert_Guia_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A230Guia_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A230Guia_AreaTrabalhoCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T000H4 */
            pr_default.execute(2, new Object[] {A230Guia_AreaTrabalhoCod});
            A231Guia_AreaTrabalhoDes = T000H4_A231Guia_AreaTrabalhoDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A231Guia_AreaTrabalhoDes", A231Guia_AreaTrabalhoDes);
            n231Guia_AreaTrabalhoDes = T000H4_n231Guia_AreaTrabalhoDes[0];
            pr_default.close(2);
         }
      }

      protected void Load0H18( )
      {
         /* Using cursor T000H6 */
         pr_default.execute(4, new Object[] {A93Guia_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound18 = 1;
            A94Guia_Nome = T000H6_A94Guia_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94Guia_Nome", A94Guia_Nome);
            A231Guia_AreaTrabalhoDes = T000H6_A231Guia_AreaTrabalhoDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A231Guia_AreaTrabalhoDes", A231Guia_AreaTrabalhoDes);
            n231Guia_AreaTrabalhoDes = T000H6_n231Guia_AreaTrabalhoDes[0];
            A95Guia_Versao = T000H6_A95Guia_Versao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A95Guia_Versao", A95Guia_Versao);
            A96Guia_Ano = T000H6_A96Guia_Ano[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A96Guia_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A96Guia_Ano), 4, 0)));
            A230Guia_AreaTrabalhoCod = T000H6_A230Guia_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A230Guia_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A230Guia_AreaTrabalhoCod), 6, 0)));
            ZM0H18( -8) ;
         }
         pr_default.close(4);
         OnLoadActions0H18( ) ;
      }

      protected void OnLoadActions0H18( )
      {
      }

      protected void CheckExtendedTable0H18( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T000H5 */
         pr_default.execute(3, new Object[] {A230Guia_AreaTrabalhoCod, A93Guia_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area Trabalho_Guias'.", "ForeignKeyNotFound", 1, "GUIA_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtGuia_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(3);
         /* Using cursor T000H4 */
         pr_default.execute(2, new Object[] {A230Guia_AreaTrabalhoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Guia_Area Trabalho'.", "ForeignKeyNotFound", 1, "GUIA_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtGuia_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A231Guia_AreaTrabalhoDes = T000H4_A231Guia_AreaTrabalhoDes[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A231Guia_AreaTrabalhoDes", A231Guia_AreaTrabalhoDes);
         n231Guia_AreaTrabalhoDes = T000H4_n231Guia_AreaTrabalhoDes[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors0H18( )
      {
         pr_default.close(3);
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_10( int A230Guia_AreaTrabalhoCod ,
                                int A93Guia_Codigo )
      {
         /* Using cursor T000H7 */
         pr_default.execute(5, new Object[] {A230Guia_AreaTrabalhoCod, A93Guia_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area Trabalho_Guias'.", "ForeignKeyNotFound", 1, "GUIA_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtGuia_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_9( int A230Guia_AreaTrabalhoCod )
      {
         /* Using cursor T000H8 */
         pr_default.execute(6, new Object[] {A230Guia_AreaTrabalhoCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Guia_Area Trabalho'.", "ForeignKeyNotFound", 1, "GUIA_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtGuia_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A231Guia_AreaTrabalhoDes = T000H8_A231Guia_AreaTrabalhoDes[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A231Guia_AreaTrabalhoDes", A231Guia_AreaTrabalhoDes);
         n231Guia_AreaTrabalhoDes = T000H8_n231Guia_AreaTrabalhoDes[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A231Guia_AreaTrabalhoDes)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey0H18( )
      {
         /* Using cursor T000H9 */
         pr_default.execute(7, new Object[] {A93Guia_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound18 = 1;
         }
         else
         {
            RcdFound18 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000H3 */
         pr_default.execute(1, new Object[] {A93Guia_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0H18( 8) ;
            RcdFound18 = 1;
            A94Guia_Nome = T000H3_A94Guia_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94Guia_Nome", A94Guia_Nome);
            A95Guia_Versao = T000H3_A95Guia_Versao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A95Guia_Versao", A95Guia_Versao);
            A96Guia_Ano = T000H3_A96Guia_Ano[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A96Guia_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A96Guia_Ano), 4, 0)));
            A230Guia_AreaTrabalhoCod = T000H3_A230Guia_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A230Guia_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A230Guia_AreaTrabalhoCod), 6, 0)));
            A93Guia_Codigo = T000H3_A93Guia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
            Z93Guia_Codigo = A93Guia_Codigo;
            sMode18 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0H18( ) ;
            if ( AnyError == 1 )
            {
               RcdFound18 = 0;
               InitializeNonKey0H18( ) ;
            }
            Gx_mode = sMode18;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound18 = 0;
            InitializeNonKey0H18( ) ;
            sMode18 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode18;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0H18( ) ;
         if ( RcdFound18 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound18 = 0;
         /* Using cursor T000H10 */
         pr_default.execute(8, new Object[] {A93Guia_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T000H10_A93Guia_Codigo[0] < A93Guia_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T000H10_A93Guia_Codigo[0] > A93Guia_Codigo ) ) )
            {
               A93Guia_Codigo = T000H10_A93Guia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
               RcdFound18 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound18 = 0;
         /* Using cursor T000H11 */
         pr_default.execute(9, new Object[] {A93Guia_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T000H11_A93Guia_Codigo[0] > A93Guia_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T000H11_A93Guia_Codigo[0] < A93Guia_Codigo ) ) )
            {
               A93Guia_Codigo = T000H11_A93Guia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
               RcdFound18 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0H18( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtGuia_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0H18( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound18 == 1 )
            {
               if ( A93Guia_Codigo != Z93Guia_Codigo )
               {
                  A93Guia_Codigo = Z93Guia_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "GUIA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtGuia_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtGuia_AreaTrabalhoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0H18( ) ;
                  GX_FocusControl = edtGuia_AreaTrabalhoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A93Guia_Codigo != Z93Guia_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtGuia_AreaTrabalhoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0H18( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "GUIA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtGuia_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtGuia_AreaTrabalhoCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0H18( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A93Guia_Codigo != Z93Guia_Codigo )
         {
            A93Guia_Codigo = Z93Guia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "GUIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtGuia_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtGuia_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0H18( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000H2 */
            pr_default.execute(0, new Object[] {A93Guia_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Guia"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z94Guia_Nome, T000H2_A94Guia_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z95Guia_Versao, T000H2_A95Guia_Versao[0]) != 0 ) || ( Z96Guia_Ano != T000H2_A96Guia_Ano[0] ) || ( Z230Guia_AreaTrabalhoCod != T000H2_A230Guia_AreaTrabalhoCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z94Guia_Nome, T000H2_A94Guia_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("guia:[seudo value changed for attri]"+"Guia_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z94Guia_Nome);
                  GXUtil.WriteLogRaw("Current: ",T000H2_A94Guia_Nome[0]);
               }
               if ( StringUtil.StrCmp(Z95Guia_Versao, T000H2_A95Guia_Versao[0]) != 0 )
               {
                  GXUtil.WriteLog("guia:[seudo value changed for attri]"+"Guia_Versao");
                  GXUtil.WriteLogRaw("Old: ",Z95Guia_Versao);
                  GXUtil.WriteLogRaw("Current: ",T000H2_A95Guia_Versao[0]);
               }
               if ( Z96Guia_Ano != T000H2_A96Guia_Ano[0] )
               {
                  GXUtil.WriteLog("guia:[seudo value changed for attri]"+"Guia_Ano");
                  GXUtil.WriteLogRaw("Old: ",Z96Guia_Ano);
                  GXUtil.WriteLogRaw("Current: ",T000H2_A96Guia_Ano[0]);
               }
               if ( Z230Guia_AreaTrabalhoCod != T000H2_A230Guia_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("guia:[seudo value changed for attri]"+"Guia_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z230Guia_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T000H2_A230Guia_AreaTrabalhoCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Guia"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0H18( )
      {
         BeforeValidate0H18( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0H18( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0H18( 0) ;
            CheckOptimisticConcurrency0H18( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0H18( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0H18( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000H12 */
                     pr_default.execute(10, new Object[] {A94Guia_Nome, A95Guia_Versao, A96Guia_Ano, A230Guia_AreaTrabalhoCod});
                     A93Guia_Codigo = T000H12_A93Guia_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("Guia") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption0H0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0H18( ) ;
            }
            EndLevel0H18( ) ;
         }
         CloseExtendedTableCursors0H18( ) ;
      }

      protected void Update0H18( )
      {
         BeforeValidate0H18( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0H18( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0H18( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0H18( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0H18( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000H13 */
                     pr_default.execute(11, new Object[] {A94Guia_Nome, A95Guia_Versao, A96Guia_Ano, A230Guia_AreaTrabalhoCod, A93Guia_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("Guia") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Guia"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0H18( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0H18( ) ;
         }
         CloseExtendedTableCursors0H18( ) ;
      }

      protected void DeferredUpdate0H18( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0H18( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0H18( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0H18( ) ;
            AfterConfirm0H18( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0H18( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000H14 */
                  pr_default.execute(12, new Object[] {A93Guia_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("Guia") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode18 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0H18( ) ;
         Gx_mode = sMode18;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0H18( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000H15 */
            pr_default.execute(13, new Object[] {A230Guia_AreaTrabalhoCod});
            A231Guia_AreaTrabalhoDes = T000H15_A231Guia_AreaTrabalhoDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A231Guia_AreaTrabalhoDes", A231Guia_AreaTrabalhoDes);
            n231Guia_AreaTrabalhoDes = T000H15_n231Guia_AreaTrabalhoDes[0];
            pr_default.close(13);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000H16 */
            pr_default.execute(14, new Object[] {A93Guia_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Refer�ncia T�cnica"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
            /* Using cursor T000H17 */
            pr_default.execute(15, new Object[] {A93Guia_Codigo});
            if ( (pr_default.getStatus(15) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Area Trabalho_Guias"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(15);
         }
      }

      protected void EndLevel0H18( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0H18( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(13);
            context.CommitDataStores( "Guia");
            if ( AnyError == 0 )
            {
               ConfirmValues0H0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(13);
            context.RollbackDataStores( "Guia");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0H18( )
      {
         /* Scan By routine */
         /* Using cursor T000H18 */
         pr_default.execute(16);
         RcdFound18 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound18 = 1;
            A93Guia_Codigo = T000H18_A93Guia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0H18( )
      {
         /* Scan next routine */
         pr_default.readNext(16);
         RcdFound18 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound18 = 1;
            A93Guia_Codigo = T000H18_A93Guia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd0H18( )
      {
         pr_default.close(16);
      }

      protected void AfterConfirm0H18( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0H18( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0H18( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0H18( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0H18( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0H18( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0H18( )
      {
         edtGuia_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGuia_Codigo_Enabled), 5, 0)));
         edtGuia_AreaTrabalhoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGuia_AreaTrabalhoCod_Enabled), 5, 0)));
         edtGuia_AreaTrabalhoDes_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_AreaTrabalhoDes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGuia_AreaTrabalhoDes_Enabled), 5, 0)));
         edtGuia_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGuia_Nome_Enabled), 5, 0)));
         edtGuia_Versao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Versao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGuia_Versao_Enabled), 5, 0)));
         edtGuia_Ano_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGuia_Ano_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGuia_Ano_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0H0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311717884");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("guia.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Guia_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z93Guia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z93Guia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z94Guia_Nome", StringUtil.RTrim( Z94Guia_Nome));
         GxWebStd.gx_hidden_field( context, "Z95Guia_Versao", StringUtil.RTrim( Z95Guia_Versao));
         GxWebStd.gx_hidden_field( context, "Z96Guia_Ano", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z96Guia_Ano), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z230Guia_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z230Guia_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N230Guia_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A230Guia_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vGUIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Guia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_GUIA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Guia_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vGUIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Guia_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Guia";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("guia:[SendSecurityCheck value for]"+"Guia_Codigo:"+context.localUtil.Format( (decimal)(A93Guia_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("guia:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("guia.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Guia_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Guia" ;
      }

      public override String GetPgmdesc( )
      {
         return "Guia" ;
      }

      protected void InitializeNonKey0H18( )
      {
         A5AreaTrabalho_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A5AreaTrabalho_Codigo), 6, 0)));
         A230Guia_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A230Guia_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A230Guia_AreaTrabalhoCod), 6, 0)));
         A94Guia_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94Guia_Nome", A94Guia_Nome);
         A231Guia_AreaTrabalhoDes = "";
         n231Guia_AreaTrabalhoDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A231Guia_AreaTrabalhoDes", A231Guia_AreaTrabalhoDes);
         A95Guia_Versao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A95Guia_Versao", A95Guia_Versao);
         A96Guia_Ano = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A96Guia_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A96Guia_Ano), 4, 0)));
         Z94Guia_Nome = "";
         Z95Guia_Versao = "";
         Z96Guia_Ano = 0;
         Z230Guia_AreaTrabalhoCod = 0;
      }

      protected void InitAll0H18( )
      {
         A93Guia_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93Guia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A93Guia_Codigo), 6, 0)));
         InitializeNonKey0H18( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311717910");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("guia.js", "?2020311717910");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockguia_codigo_Internalname = "TEXTBLOCKGUIA_CODIGO";
         edtGuia_Codigo_Internalname = "GUIA_CODIGO";
         lblTextblockguia_areatrabalhocod_Internalname = "TEXTBLOCKGUIA_AREATRABALHOCOD";
         edtGuia_AreaTrabalhoCod_Internalname = "GUIA_AREATRABALHOCOD";
         edtGuia_AreaTrabalhoDes_Internalname = "GUIA_AREATRABALHODES";
         lblTextblockguia_nome_Internalname = "TEXTBLOCKGUIA_NOME";
         edtGuia_Nome_Internalname = "GUIA_NOME";
         lblTextblockguia_versao_Internalname = "TEXTBLOCKGUIA_VERSAO";
         edtGuia_Versao_Internalname = "GUIA_VERSAO";
         lblTextblockguia_ano_Internalname = "TEXTBLOCKGUIA_ANO";
         edtGuia_Ano_Internalname = "GUIA_ANO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Manuten��o";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Guia";
         edtGuia_Ano_Jsonclick = "";
         edtGuia_Ano_Enabled = 1;
         edtGuia_Versao_Jsonclick = "";
         edtGuia_Versao_Enabled = 1;
         edtGuia_Nome_Jsonclick = "";
         edtGuia_Nome_Enabled = 1;
         edtGuia_AreaTrabalhoDes_Jsonclick = "";
         edtGuia_AreaTrabalhoDes_Enabled = 0;
         edtGuia_AreaTrabalhoCod_Jsonclick = "";
         edtGuia_AreaTrabalhoCod_Enabled = 1;
         edtGuia_Codigo_Jsonclick = "";
         edtGuia_Codigo_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Guia_areatrabalhocod( int GX_Parm1 ,
                                              int GX_Parm2 ,
                                              String GX_Parm3 )
      {
         A230Guia_AreaTrabalhoCod = GX_Parm1;
         A93Guia_Codigo = GX_Parm2;
         A231Guia_AreaTrabalhoDes = GX_Parm3;
         n231Guia_AreaTrabalhoDes = false;
         /* Using cursor T000H15 */
         pr_default.execute(13, new Object[] {A230Guia_AreaTrabalhoCod});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Guia_Area Trabalho'.", "ForeignKeyNotFound", 1, "GUIA_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtGuia_AreaTrabalhoCod_Internalname;
         }
         A231Guia_AreaTrabalhoDes = T000H15_A231Guia_AreaTrabalhoDes[0];
         n231Guia_AreaTrabalhoDes = T000H15_n231Guia_AreaTrabalhoDes[0];
         pr_default.close(13);
         /* Using cursor T000H19 */
         pr_default.execute(17, new Object[] {A230Guia_AreaTrabalhoCod, A93Guia_Codigo});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area Trabalho_Guias'.", "ForeignKeyNotFound", 1, "GUIA_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtGuia_AreaTrabalhoCod_Internalname;
         }
         pr_default.close(17);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A231Guia_AreaTrabalhoDes = "";
            n231Guia_AreaTrabalhoDes = false;
         }
         isValidOutput.Add(A231Guia_AreaTrabalhoDes);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Guia_Codigo',fld:'vGUIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120H2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(17);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z94Guia_Nome = "";
         Z95Guia_Versao = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockguia_codigo_Jsonclick = "";
         lblTextblockguia_areatrabalhocod_Jsonclick = "";
         A231Guia_AreaTrabalhoDes = "";
         lblTextblockguia_nome_Jsonclick = "";
         A94Guia_Nome = "";
         lblTextblockguia_versao_Jsonclick = "";
         A95Guia_Versao = "";
         lblTextblockguia_ano_Jsonclick = "";
         AV13Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode18 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z231Guia_AreaTrabalhoDes = "";
         T000H4_A231Guia_AreaTrabalhoDes = new String[] {""} ;
         T000H4_n231Guia_AreaTrabalhoDes = new bool[] {false} ;
         T000H6_A94Guia_Nome = new String[] {""} ;
         T000H6_A231Guia_AreaTrabalhoDes = new String[] {""} ;
         T000H6_n231Guia_AreaTrabalhoDes = new bool[] {false} ;
         T000H6_A95Guia_Versao = new String[] {""} ;
         T000H6_A96Guia_Ano = new short[1] ;
         T000H6_A230Guia_AreaTrabalhoCod = new int[1] ;
         T000H6_A93Guia_Codigo = new int[1] ;
         T000H5_A5AreaTrabalho_Codigo = new int[1] ;
         T000H7_A5AreaTrabalho_Codigo = new int[1] ;
         T000H8_A231Guia_AreaTrabalhoDes = new String[] {""} ;
         T000H8_n231Guia_AreaTrabalhoDes = new bool[] {false} ;
         T000H9_A93Guia_Codigo = new int[1] ;
         T000H3_A94Guia_Nome = new String[] {""} ;
         T000H3_A95Guia_Versao = new String[] {""} ;
         T000H3_A96Guia_Ano = new short[1] ;
         T000H3_A230Guia_AreaTrabalhoCod = new int[1] ;
         T000H3_A93Guia_Codigo = new int[1] ;
         T000H10_A93Guia_Codigo = new int[1] ;
         T000H11_A93Guia_Codigo = new int[1] ;
         T000H2_A94Guia_Nome = new String[] {""} ;
         T000H2_A95Guia_Versao = new String[] {""} ;
         T000H2_A96Guia_Ano = new short[1] ;
         T000H2_A230Guia_AreaTrabalhoCod = new int[1] ;
         T000H2_A93Guia_Codigo = new int[1] ;
         T000H12_A93Guia_Codigo = new int[1] ;
         T000H15_A231Guia_AreaTrabalhoDes = new String[] {""} ;
         T000H15_n231Guia_AreaTrabalhoDes = new bool[] {false} ;
         T000H16_A97ReferenciaTecnica_Codigo = new int[1] ;
         T000H17_A5AreaTrabalho_Codigo = new int[1] ;
         T000H17_A93Guia_Codigo = new int[1] ;
         T000H18_A93Guia_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T000H19_A5AreaTrabalho_Codigo = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.guia__default(),
            new Object[][] {
                new Object[] {
               T000H2_A94Guia_Nome, T000H2_A95Guia_Versao, T000H2_A96Guia_Ano, T000H2_A230Guia_AreaTrabalhoCod, T000H2_A93Guia_Codigo
               }
               , new Object[] {
               T000H3_A94Guia_Nome, T000H3_A95Guia_Versao, T000H3_A96Guia_Ano, T000H3_A230Guia_AreaTrabalhoCod, T000H3_A93Guia_Codigo
               }
               , new Object[] {
               T000H4_A231Guia_AreaTrabalhoDes, T000H4_n231Guia_AreaTrabalhoDes
               }
               , new Object[] {
               T000H5_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               T000H6_A94Guia_Nome, T000H6_A231Guia_AreaTrabalhoDes, T000H6_n231Guia_AreaTrabalhoDes, T000H6_A95Guia_Versao, T000H6_A96Guia_Ano, T000H6_A230Guia_AreaTrabalhoCod, T000H6_A93Guia_Codigo
               }
               , new Object[] {
               T000H7_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               T000H8_A231Guia_AreaTrabalhoDes, T000H8_n231Guia_AreaTrabalhoDes
               }
               , new Object[] {
               T000H9_A93Guia_Codigo
               }
               , new Object[] {
               T000H10_A93Guia_Codigo
               }
               , new Object[] {
               T000H11_A93Guia_Codigo
               }
               , new Object[] {
               T000H12_A93Guia_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000H15_A231Guia_AreaTrabalhoDes, T000H15_n231Guia_AreaTrabalhoDes
               }
               , new Object[] {
               T000H16_A97ReferenciaTecnica_Codigo
               }
               , new Object[] {
               T000H17_A5AreaTrabalho_Codigo, T000H17_A93Guia_Codigo
               }
               , new Object[] {
               T000H18_A93Guia_Codigo
               }
               , new Object[] {
               T000H19_A5AreaTrabalho_Codigo
               }
            }
         );
         AV13Pgmname = "Guia";
      }

      private short Z96Guia_Ano ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A96Guia_Ano ;
      private short RcdFound18 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Guia_Codigo ;
      private int Z93Guia_Codigo ;
      private int Z230Guia_AreaTrabalhoCod ;
      private int N230Guia_AreaTrabalhoCod ;
      private int A230Guia_AreaTrabalhoCod ;
      private int A93Guia_Codigo ;
      private int AV7Guia_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtGuia_Codigo_Enabled ;
      private int edtGuia_AreaTrabalhoCod_Enabled ;
      private int edtGuia_AreaTrabalhoDes_Enabled ;
      private int edtGuia_Nome_Enabled ;
      private int edtGuia_Versao_Enabled ;
      private int edtGuia_Ano_Enabled ;
      private int AV11Insert_Guia_AreaTrabalhoCod ;
      private int AV14GXV1 ;
      private int A5AreaTrabalho_Codigo ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z94Guia_Nome ;
      private String Z95Guia_Versao ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtGuia_AreaTrabalhoCod_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockguia_codigo_Internalname ;
      private String lblTextblockguia_codigo_Jsonclick ;
      private String edtGuia_Codigo_Internalname ;
      private String edtGuia_Codigo_Jsonclick ;
      private String lblTextblockguia_areatrabalhocod_Internalname ;
      private String lblTextblockguia_areatrabalhocod_Jsonclick ;
      private String edtGuia_AreaTrabalhoCod_Jsonclick ;
      private String edtGuia_AreaTrabalhoDes_Internalname ;
      private String edtGuia_AreaTrabalhoDes_Jsonclick ;
      private String lblTextblockguia_nome_Internalname ;
      private String lblTextblockguia_nome_Jsonclick ;
      private String edtGuia_Nome_Internalname ;
      private String A94Guia_Nome ;
      private String edtGuia_Nome_Jsonclick ;
      private String lblTextblockguia_versao_Internalname ;
      private String lblTextblockguia_versao_Jsonclick ;
      private String edtGuia_Versao_Internalname ;
      private String A95Guia_Versao ;
      private String edtGuia_Versao_Jsonclick ;
      private String lblTextblockguia_ano_Internalname ;
      private String lblTextblockguia_ano_Jsonclick ;
      private String edtGuia_Ano_Internalname ;
      private String edtGuia_Ano_Jsonclick ;
      private String AV13Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode18 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n231Guia_AreaTrabalhoDes ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String A231Guia_AreaTrabalhoDes ;
      private String Z231Guia_AreaTrabalhoDes ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] T000H4_A231Guia_AreaTrabalhoDes ;
      private bool[] T000H4_n231Guia_AreaTrabalhoDes ;
      private String[] T000H6_A94Guia_Nome ;
      private String[] T000H6_A231Guia_AreaTrabalhoDes ;
      private bool[] T000H6_n231Guia_AreaTrabalhoDes ;
      private String[] T000H6_A95Guia_Versao ;
      private short[] T000H6_A96Guia_Ano ;
      private int[] T000H6_A230Guia_AreaTrabalhoCod ;
      private int[] T000H6_A93Guia_Codigo ;
      private int[] T000H5_A5AreaTrabalho_Codigo ;
      private int[] T000H7_A5AreaTrabalho_Codigo ;
      private String[] T000H8_A231Guia_AreaTrabalhoDes ;
      private bool[] T000H8_n231Guia_AreaTrabalhoDes ;
      private int[] T000H9_A93Guia_Codigo ;
      private String[] T000H3_A94Guia_Nome ;
      private String[] T000H3_A95Guia_Versao ;
      private short[] T000H3_A96Guia_Ano ;
      private int[] T000H3_A230Guia_AreaTrabalhoCod ;
      private int[] T000H3_A93Guia_Codigo ;
      private int[] T000H10_A93Guia_Codigo ;
      private int[] T000H11_A93Guia_Codigo ;
      private String[] T000H2_A94Guia_Nome ;
      private String[] T000H2_A95Guia_Versao ;
      private short[] T000H2_A96Guia_Ano ;
      private int[] T000H2_A230Guia_AreaTrabalhoCod ;
      private int[] T000H2_A93Guia_Codigo ;
      private int[] T000H12_A93Guia_Codigo ;
      private String[] T000H15_A231Guia_AreaTrabalhoDes ;
      private bool[] T000H15_n231Guia_AreaTrabalhoDes ;
      private int[] T000H16_A97ReferenciaTecnica_Codigo ;
      private int[] T000H17_A5AreaTrabalho_Codigo ;
      private int[] T000H17_A93Guia_Codigo ;
      private int[] T000H18_A93Guia_Codigo ;
      private int[] T000H19_A5AreaTrabalho_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class guia__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000H6 ;
          prmT000H6 = new Object[] {
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000H5 ;
          prmT000H5 = new Object[] {
          new Object[] {"@Guia_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000H4 ;
          prmT000H4 = new Object[] {
          new Object[] {"@Guia_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000H7 ;
          prmT000H7 = new Object[] {
          new Object[] {"@Guia_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000H8 ;
          prmT000H8 = new Object[] {
          new Object[] {"@Guia_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000H9 ;
          prmT000H9 = new Object[] {
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000H3 ;
          prmT000H3 = new Object[] {
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000H10 ;
          prmT000H10 = new Object[] {
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000H11 ;
          prmT000H11 = new Object[] {
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000H2 ;
          prmT000H2 = new Object[] {
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000H12 ;
          prmT000H12 = new Object[] {
          new Object[] {"@Guia_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Guia_Versao",SqlDbType.Char,15,0} ,
          new Object[] {"@Guia_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Guia_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000H13 ;
          prmT000H13 = new Object[] {
          new Object[] {"@Guia_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Guia_Versao",SqlDbType.Char,15,0} ,
          new Object[] {"@Guia_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Guia_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000H14 ;
          prmT000H14 = new Object[] {
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000H16 ;
          prmT000H16 = new Object[] {
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000H17 ;
          prmT000H17 = new Object[] {
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000H18 ;
          prmT000H18 = new Object[] {
          } ;
          Object[] prmT000H15 ;
          prmT000H15 = new Object[] {
          new Object[] {"@Guia_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000H19 ;
          prmT000H19 = new Object[] {
          new Object[] {"@Guia_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000H2", "SELECT [Guia_Nome], [Guia_Versao], [Guia_Ano], [Guia_AreaTrabalhoCod] AS Guia_AreaTrabalhoCod, [Guia_Codigo] FROM [Guia] WITH (UPDLOCK) WHERE [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000H2,1,0,true,false )
             ,new CursorDef("T000H3", "SELECT [Guia_Nome], [Guia_Versao], [Guia_Ano], [Guia_AreaTrabalhoCod] AS Guia_AreaTrabalhoCod, [Guia_Codigo] FROM [Guia] WITH (NOLOCK) WHERE [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000H3,1,0,true,false )
             ,new CursorDef("T000H4", "SELECT [AreaTrabalho_Descricao] AS Guia_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Guia_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000H4,1,0,true,false )
             ,new CursorDef("T000H5", "SELECT [AreaTrabalho_Codigo] FROM [AreaTrabalho_Guias] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Guia_AreaTrabalhoCod AND [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000H5,1,0,true,false )
             ,new CursorDef("T000H6", "SELECT TM1.[Guia_Nome], T2.[AreaTrabalho_Descricao] AS Guia_AreaTrabalhoDes, TM1.[Guia_Versao], TM1.[Guia_Ano], TM1.[Guia_AreaTrabalhoCod] AS Guia_AreaTrabalhoCod, TM1.[Guia_Codigo] FROM ([Guia] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[Guia_AreaTrabalhoCod]) WHERE TM1.[Guia_Codigo] = @Guia_Codigo ORDER BY TM1.[Guia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000H6,100,0,true,false )
             ,new CursorDef("T000H7", "SELECT [AreaTrabalho_Codigo] FROM [AreaTrabalho_Guias] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Guia_AreaTrabalhoCod AND [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000H7,1,0,true,false )
             ,new CursorDef("T000H8", "SELECT [AreaTrabalho_Descricao] AS Guia_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Guia_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000H8,1,0,true,false )
             ,new CursorDef("T000H9", "SELECT [Guia_Codigo] FROM [Guia] WITH (NOLOCK) WHERE [Guia_Codigo] = @Guia_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000H9,1,0,true,false )
             ,new CursorDef("T000H10", "SELECT TOP 1 [Guia_Codigo] FROM [Guia] WITH (NOLOCK) WHERE ( [Guia_Codigo] > @Guia_Codigo) ORDER BY [Guia_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000H10,1,0,true,true )
             ,new CursorDef("T000H11", "SELECT TOP 1 [Guia_Codigo] FROM [Guia] WITH (NOLOCK) WHERE ( [Guia_Codigo] < @Guia_Codigo) ORDER BY [Guia_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000H11,1,0,true,true )
             ,new CursorDef("T000H12", "INSERT INTO [Guia]([Guia_Nome], [Guia_Versao], [Guia_Ano], [Guia_AreaTrabalhoCod]) VALUES(@Guia_Nome, @Guia_Versao, @Guia_Ano, @Guia_AreaTrabalhoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000H12)
             ,new CursorDef("T000H13", "UPDATE [Guia] SET [Guia_Nome]=@Guia_Nome, [Guia_Versao]=@Guia_Versao, [Guia_Ano]=@Guia_Ano, [Guia_AreaTrabalhoCod]=@Guia_AreaTrabalhoCod  WHERE [Guia_Codigo] = @Guia_Codigo", GxErrorMask.GX_NOMASK,prmT000H13)
             ,new CursorDef("T000H14", "DELETE FROM [Guia]  WHERE [Guia_Codigo] = @Guia_Codigo", GxErrorMask.GX_NOMASK,prmT000H14)
             ,new CursorDef("T000H15", "SELECT [AreaTrabalho_Descricao] AS Guia_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Guia_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000H15,1,0,true,false )
             ,new CursorDef("T000H16", "SELECT TOP 1 [ReferenciaTecnica_Codigo] FROM [ReferenciaTecnica] WITH (NOLOCK) WHERE [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000H16,1,0,true,true )
             ,new CursorDef("T000H17", "SELECT TOP 1 [AreaTrabalho_Codigo], [Guia_Codigo] FROM [AreaTrabalho_Guias] WITH (NOLOCK) WHERE [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000H17,1,0,true,true )
             ,new CursorDef("T000H18", "SELECT [Guia_Codigo] FROM [Guia] WITH (NOLOCK) ORDER BY [Guia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000H18,100,0,true,false )
             ,new CursorDef("T000H19", "SELECT [AreaTrabalho_Codigo] FROM [AreaTrabalho_Guias] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Guia_AreaTrabalhoCod AND [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000H19,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
