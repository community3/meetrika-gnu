/*
               File: Projeto
        Description: Projeto
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:21:5.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class projeto : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"PROJETO_TIPOPROJETOCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAPROJETO_TIPOPROJETOCOD2986( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel3"+"_"+"PROJETO_SISTEMACOD") == 0 )
         {
            A648Projeto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX3ASAPROJETO_SISTEMACOD2986( A648Projeto_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_17") == 0 )
         {
            A648Projeto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_17( A648Projeto_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_16") == 0 )
         {
            A983Projeto_TipoProjetoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n983Projeto_TipoProjetoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_16( A983Projeto_TipoProjetoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Projeto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Projeto_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynProjeto_TipoProjetoCod.Name = "PROJETO_TIPOPROJETOCOD";
         dynProjeto_TipoProjetoCod.WebTags = "";
         cmbProjeto_Status.Name = "PROJETO_STATUS";
         cmbProjeto_Status.WebTags = "";
         cmbProjeto_Status.addItem("A", "Aberto", 0);
         cmbProjeto_Status.addItem("E", "Em Contagem", 0);
         cmbProjeto_Status.addItem("C", "Contado", 0);
         if ( cmbProjeto_Status.ItemCount > 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A658Projeto_Status)) )
            {
               A658Projeto_Status = "A";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
            }
            A658Projeto_Status = cmbProjeto_Status.getValidValue(A658Projeto_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
         }
         chkProjeto_Incremental.Name = "PROJETO_INCREMENTAL";
         chkProjeto_Incremental.WebTags = "";
         chkProjeto_Incremental.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkProjeto_Incremental_Internalname, "TitleCaption", chkProjeto_Incremental.Caption);
         chkProjeto_Incremental.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Projeto", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtProjeto_Sigla_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public projeto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public projeto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Projeto_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Projeto_Codigo = aP1_Projeto_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynProjeto_TipoProjetoCod = new GXCombobox();
         cmbProjeto_Status = new GXCombobox();
         chkProjeto_Incremental = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynProjeto_TipoProjetoCod.ItemCount > 0 )
         {
            A983Projeto_TipoProjetoCod = (int)(NumberUtil.Val( dynProjeto_TipoProjetoCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0))), "."));
            n983Projeto_TipoProjetoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)));
         }
         if ( cmbProjeto_Status.ItemCount > 0 )
         {
            A658Projeto_Status = cmbProjeto_Status.getValidValue(A658Projeto_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2986( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ",", "")), ((edtProjeto_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtProjeto_Codigo_Visible, edtProjeto_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Projeto.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2986( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_75_2986( true) ;
         }
         return  ;
      }

      protected void wb_table3_75_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2986e( true) ;
         }
         else
         {
            wb_table1_2_2986e( false) ;
         }
      }

      protected void wb_table3_75_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_75_2986e( true) ;
         }
         else
         {
            wb_table3_75_2986e( false) ;
         }
      }

      protected void wb_table2_5_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2986( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2986e( true) ;
         }
         else
         {
            wb_table2_5_2986e( false) ;
         }
      }

      protected void wb_table4_13_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_sigla_Internalname, "Sigla", "", "", lblTextblockprojeto_sigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProjeto_Sigla_Internalname, StringUtil.RTrim( A650Projeto_Sigla), StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_Sigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProjeto_Sigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_nome_Internalname, "Nome", "", "", lblTextblockprojeto_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProjeto_Nome_Internalname, StringUtil.RTrim( A649Projeto_Nome), StringUtil.RTrim( context.localUtil.Format( A649Projeto_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProjeto_Nome_Enabled, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "Nome", "left", true, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_tipoprojetocod_Internalname, "Tipo de Projeto", "", "", lblTextblockprojeto_tipoprojetocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynProjeto_TipoProjetoCod, dynProjeto_TipoProjetoCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)), 1, dynProjeto_TipoProjetoCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynProjeto_TipoProjetoCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_Projeto.htm");
            dynProjeto_TipoProjetoCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_TipoProjetoCod_Internalname, "Values", (String)(dynProjeto_TipoProjetoCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_escopo_Internalname, "Escopo", "", "", lblTextblockprojeto_escopo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtProjeto_Escopo_Internalname, A653Projeto_Escopo, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", 0, 1, edtProjeto_Escopo_Enabled, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "", "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_status_Internalname, "Status", "", "", lblTextblockprojeto_status_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbProjeto_Status, cmbProjeto_Status_Internalname, StringUtil.RTrim( A658Projeto_Status), 1, cmbProjeto_Status_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbProjeto_Status.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_Projeto.htm");
            cmbProjeto_Status.CurrentValue = StringUtil.RTrim( A658Projeto_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbProjeto_Status_Internalname, "Values", (String)(cmbProjeto_Status.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_fatorescala_Internalname, "Fator de Escala", "", "", lblTextblockprojeto_fatorescala_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_51_2986( true) ;
         }
         return  ;
      }

      protected void wb_table5_51_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_incremental_Internalname, "Incremental", "", "", lblTextblockprojeto_incremental_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkProjeto_Incremental_Internalname, StringUtil.BoolToStr( A1232Projeto_Incremental), "", "", 1, chkProjeto_Incremental.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(70, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,70);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2986e( true) ;
         }
         else
         {
            wb_table4_13_2986e( false) ;
         }
      }

      protected void wb_table5_51_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedprojeto_fatorescala_Internalname, tblTablemergedprojeto_fatorescala_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_FatorEscala_Internalname, StringUtil.LTrim( StringUtil.NToC( A985Projeto_FatorEscala, 6, 2, ",", "")), ((edtProjeto_FatorEscala_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A985Projeto_FatorEscala, "ZZ9.99")) : context.localUtil.Format( A985Projeto_FatorEscala, "ZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_FatorEscala_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProjeto_FatorEscala_Enabled, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfatorescala_Internalname, "", "Calcular", bttBtnfatorescala_Jsonclick, 7, "Fator de Escala", "", StyleString, ClassString, bttBtnfatorescala_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"e112986_client"+"'", TempTags, "", 2, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_fatormultiplicador_Internalname, "Fator Multiplicador", "", "", lblTextblockprojeto_fatormultiplicador_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_FatorMultiplicador_Internalname, StringUtil.LTrim( StringUtil.NToC( A989Projeto_FatorMultiplicador, 6, 2, ",", "")), ((edtProjeto_FatorMultiplicador_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A989Projeto_FatorMultiplicador, "ZZ9.99")) : context.localUtil.Format( A989Projeto_FatorMultiplicador, "ZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_FatorMultiplicador_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProjeto_FatorMultiplicador_Enabled, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfatormultiplicador_Internalname, "", "Calcular", bttBtnfatormultiplicador_Jsonclick, 7, "Fator Multiplicador", "", StyleString, ClassString, bttBtnfatormultiplicador_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"e122986_client"+"'", TempTags, "", 2, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_constacocomo_Internalname, "Constante A (Cocomo)", "", "", lblTextblockprojeto_constacocomo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProjeto_ConstACocomo_Internalname, StringUtil.LTrim( StringUtil.NToC( A990Projeto_ConstACocomo, 6, 2, ",", "")), ((edtProjeto_ConstACocomo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A990Projeto_ConstACocomo, "ZZ9.99")) : context.localUtil.Format( A990Projeto_ConstACocomo, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_ConstACocomo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProjeto_ConstACocomo_Enabled, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_51_2986e( true) ;
         }
         else
         {
            wb_table5_51_2986e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13292 */
         E13292 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               ajax_req_read_hidden_sdt(cgiGet( "vWWPCONTEXT"), AV8WWPContext);
               /* Read variables values. */
               A650Projeto_Sigla = StringUtil.Upper( cgiGet( edtProjeto_Sigla_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A650Projeto_Sigla", A650Projeto_Sigla);
               A649Projeto_Nome = StringUtil.Upper( cgiGet( edtProjeto_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A649Projeto_Nome", A649Projeto_Nome);
               dynProjeto_TipoProjetoCod.CurrentValue = cgiGet( dynProjeto_TipoProjetoCod_Internalname);
               A983Projeto_TipoProjetoCod = (int)(NumberUtil.Val( cgiGet( dynProjeto_TipoProjetoCod_Internalname), "."));
               n983Projeto_TipoProjetoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)));
               n983Projeto_TipoProjetoCod = ((0==A983Projeto_TipoProjetoCod) ? true : false);
               A653Projeto_Escopo = cgiGet( edtProjeto_Escopo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A653Projeto_Escopo", A653Projeto_Escopo);
               cmbProjeto_Status.CurrentValue = cgiGet( cmbProjeto_Status_Internalname);
               A658Projeto_Status = cgiGet( cmbProjeto_Status_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
               A985Projeto_FatorEscala = context.localUtil.CToN( cgiGet( edtProjeto_FatorEscala_Internalname), ",", ".");
               n985Projeto_FatorEscala = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A985Projeto_FatorEscala", StringUtil.LTrim( StringUtil.Str( A985Projeto_FatorEscala, 6, 2)));
               A989Projeto_FatorMultiplicador = context.localUtil.CToN( cgiGet( edtProjeto_FatorMultiplicador_Internalname), ",", ".");
               n989Projeto_FatorMultiplicador = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A989Projeto_FatorMultiplicador", StringUtil.LTrim( StringUtil.Str( A989Projeto_FatorMultiplicador, 6, 2)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtProjeto_ConstACocomo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProjeto_ConstACocomo_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROJETO_CONSTACOCOMO");
                  AnyError = 1;
                  GX_FocusControl = edtProjeto_ConstACocomo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A990Projeto_ConstACocomo = 0;
                  n990Projeto_ConstACocomo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A990Projeto_ConstACocomo", StringUtil.LTrim( StringUtil.Str( A990Projeto_ConstACocomo, 6, 2)));
               }
               else
               {
                  A990Projeto_ConstACocomo = context.localUtil.CToN( cgiGet( edtProjeto_ConstACocomo_Internalname), ",", ".");
                  n990Projeto_ConstACocomo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A990Projeto_ConstACocomo", StringUtil.LTrim( StringUtil.Str( A990Projeto_ConstACocomo, 6, 2)));
               }
               n990Projeto_ConstACocomo = ((Convert.ToDecimal(0)==A990Projeto_ConstACocomo) ? true : false);
               A1232Projeto_Incremental = StringUtil.StrToBool( cgiGet( chkProjeto_Incremental_Internalname));
               n1232Projeto_Incremental = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1232Projeto_Incremental", A1232Projeto_Incremental);
               n1232Projeto_Incremental = ((false==A1232Projeto_Incremental) ? true : false);
               A648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( edtProjeto_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
               /* Read saved values. */
               Z648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z648Projeto_Codigo"), ",", "."));
               Z649Projeto_Nome = cgiGet( "Z649Projeto_Nome");
               Z650Projeto_Sigla = cgiGet( "Z650Projeto_Sigla");
               Z651Projeto_TipoContagem = cgiGet( "Z651Projeto_TipoContagem");
               Z652Projeto_TecnicaContagem = cgiGet( "Z652Projeto_TecnicaContagem");
               Z1541Projeto_Previsao = context.localUtil.CToD( cgiGet( "Z1541Projeto_Previsao"), 0);
               n1541Projeto_Previsao = ((DateTime.MinValue==A1541Projeto_Previsao) ? true : false);
               Z1542Projeto_GerenteCod = (int)(context.localUtil.CToN( cgiGet( "Z1542Projeto_GerenteCod"), ",", "."));
               n1542Projeto_GerenteCod = ((0==A1542Projeto_GerenteCod) ? true : false);
               Z1543Projeto_ServicoCod = (int)(context.localUtil.CToN( cgiGet( "Z1543Projeto_ServicoCod"), ",", "."));
               n1543Projeto_ServicoCod = ((0==A1543Projeto_ServicoCod) ? true : false);
               Z658Projeto_Status = cgiGet( "Z658Projeto_Status");
               Z985Projeto_FatorEscala = context.localUtil.CToN( cgiGet( "Z985Projeto_FatorEscala"), ",", ".");
               n985Projeto_FatorEscala = ((Convert.ToDecimal(0)==A985Projeto_FatorEscala) ? true : false);
               Z989Projeto_FatorMultiplicador = context.localUtil.CToN( cgiGet( "Z989Projeto_FatorMultiplicador"), ",", ".");
               n989Projeto_FatorMultiplicador = ((Convert.ToDecimal(0)==A989Projeto_FatorMultiplicador) ? true : false);
               Z990Projeto_ConstACocomo = context.localUtil.CToN( cgiGet( "Z990Projeto_ConstACocomo"), ",", ".");
               n990Projeto_ConstACocomo = ((Convert.ToDecimal(0)==A990Projeto_ConstACocomo) ? true : false);
               Z1232Projeto_Incremental = StringUtil.StrToBool( cgiGet( "Z1232Projeto_Incremental"));
               n1232Projeto_Incremental = ((false==A1232Projeto_Incremental) ? true : false);
               Z983Projeto_TipoProjetoCod = (int)(context.localUtil.CToN( cgiGet( "Z983Projeto_TipoProjetoCod"), ",", "."));
               n983Projeto_TipoProjetoCod = ((0==A983Projeto_TipoProjetoCod) ? true : false);
               A651Projeto_TipoContagem = cgiGet( "Z651Projeto_TipoContagem");
               A652Projeto_TecnicaContagem = cgiGet( "Z652Projeto_TecnicaContagem");
               A1541Projeto_Previsao = context.localUtil.CToD( cgiGet( "Z1541Projeto_Previsao"), 0);
               n1541Projeto_Previsao = false;
               n1541Projeto_Previsao = ((DateTime.MinValue==A1541Projeto_Previsao) ? true : false);
               A1542Projeto_GerenteCod = (int)(context.localUtil.CToN( cgiGet( "Z1542Projeto_GerenteCod"), ",", "."));
               n1542Projeto_GerenteCod = false;
               n1542Projeto_GerenteCod = ((0==A1542Projeto_GerenteCod) ? true : false);
               A1543Projeto_ServicoCod = (int)(context.localUtil.CToN( cgiGet( "Z1543Projeto_ServicoCod"), ",", "."));
               n1543Projeto_ServicoCod = false;
               n1543Projeto_ServicoCod = ((0==A1543Projeto_ServicoCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N983Projeto_TipoProjetoCod = (int)(context.localUtil.CToN( cgiGet( "N983Projeto_TipoProjetoCod"), ",", "."));
               n983Projeto_TipoProjetoCod = ((0==A983Projeto_TipoProjetoCod) ? true : false);
               A740Projeto_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "PROJETO_SISTEMACOD"), ",", "."));
               AV7Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( "vPROJETO_CODIGO"), ",", "."));
               AV12Insert_Projeto_TipoProjetoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PROJETO_TIPOPROJETOCOD"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A651Projeto_TipoContagem = cgiGet( "PROJETO_TIPOCONTAGEM");
               A652Projeto_TecnicaContagem = cgiGet( "PROJETO_TECNICACONTAGEM");
               A1540Projeto_Introducao = cgiGet( "PROJETO_INTRODUCAO");
               n1540Projeto_Introducao = false;
               n1540Projeto_Introducao = (String.IsNullOrEmpty(StringUtil.RTrim( A1540Projeto_Introducao)) ? true : false);
               A1541Projeto_Previsao = context.localUtil.CToD( cgiGet( "PROJETO_PREVISAO"), 0);
               n1541Projeto_Previsao = ((DateTime.MinValue==A1541Projeto_Previsao) ? true : false);
               A1542Projeto_GerenteCod = (int)(context.localUtil.CToN( cgiGet( "PROJETO_GERENTECOD"), ",", "."));
               n1542Projeto_GerenteCod = ((0==A1542Projeto_GerenteCod) ? true : false);
               A1543Projeto_ServicoCod = (int)(context.localUtil.CToN( cgiGet( "PROJETO_SERVICOCOD"), ",", "."));
               n1543Projeto_ServicoCod = ((0==A1543Projeto_ServicoCod) ? true : false);
               A655Projeto_Custo = context.localUtil.CToN( cgiGet( "PROJETO_CUSTO"), ",", ".");
               A656Projeto_Prazo = (short)(context.localUtil.CToN( cgiGet( "PROJETO_PRAZO"), ",", "."));
               A657Projeto_Esforco = (short)(context.localUtil.CToN( cgiGet( "PROJETO_ESFORCO"), ",", "."));
               AV19Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Projeto";
               A658Projeto_Status = cgiGet( cmbProjeto_Status_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""));
               A648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( edtProjeto_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A651Projeto_TipoContagem, ""));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A652Projeto_TecnicaContagem, ""));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1541Projeto_Previsao, "99/99/99");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1542Projeto_GerenteCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1543Projeto_ServicoCod), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A648Projeto_Codigo != Z648Projeto_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("projeto:[SecurityCheckFailed value for]"+"Projeto_Status:"+StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, "")));
                  GXUtil.WriteLog("projeto:[SecurityCheckFailed value for]"+"Projeto_Codigo:"+context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("projeto:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("projeto:[SecurityCheckFailed value for]"+"Projeto_TipoContagem:"+StringUtil.RTrim( context.localUtil.Format( A651Projeto_TipoContagem, "")));
                  GXUtil.WriteLog("projeto:[SecurityCheckFailed value for]"+"Projeto_TecnicaContagem:"+StringUtil.RTrim( context.localUtil.Format( A652Projeto_TecnicaContagem, "")));
                  GXUtil.WriteLog("projeto:[SecurityCheckFailed value for]"+"Projeto_Previsao:"+context.localUtil.Format(A1541Projeto_Previsao, "99/99/99"));
                  GXUtil.WriteLog("projeto:[SecurityCheckFailed value for]"+"Projeto_GerenteCod:"+context.localUtil.Format( (decimal)(A1542Projeto_GerenteCod), "ZZZZZ9"));
                  GXUtil.WriteLog("projeto:[SecurityCheckFailed value for]"+"Projeto_ServicoCod:"+context.localUtil.Format( (decimal)(A1543Projeto_ServicoCod), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A648Projeto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode86 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode86;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound86 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_290( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "PROJETO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtProjeto_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13292 */
                           E13292 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14292 */
                           E14292 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E14292 */
            E14292 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2986( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2986( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_290( )
      {
         BeforeValidate2986( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2986( ) ;
            }
            else
            {
               CheckExtendedTable2986( ) ;
               CloseExtendedTableCursors2986( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption290( )
      {
      }

      protected void E13292( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV19Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV20GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20GXV1), 8, 0)));
            while ( AV20GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV20GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Projeto_TipoProjetoCod") == 0 )
               {
                  AV12Insert_Projeto_TipoProjetoCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Projeto_TipoProjetoCod), 6, 0)));
               }
               AV20GXV1 = (int)(AV20GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20GXV1), 8, 0)));
            }
         }
         edtProjeto_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_Codigo_Visible), 5, 0)));
         AV16FatorEscala = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16FatorEscala", StringUtil.LTrim( StringUtil.Str( AV16FatorEscala, 6, 2)));
         AV15FatorMultiplicador = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15FatorMultiplicador", StringUtil.LTrim( StringUtil.Str( AV15FatorMultiplicador, 6, 2)));
      }

      protected void E14292( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwprojeto.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2986( short GX_JID )
      {
         if ( ( GX_JID == 15 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z649Projeto_Nome = T00293_A649Projeto_Nome[0];
               Z650Projeto_Sigla = T00293_A650Projeto_Sigla[0];
               Z651Projeto_TipoContagem = T00293_A651Projeto_TipoContagem[0];
               Z652Projeto_TecnicaContagem = T00293_A652Projeto_TecnicaContagem[0];
               Z1541Projeto_Previsao = T00293_A1541Projeto_Previsao[0];
               Z1542Projeto_GerenteCod = T00293_A1542Projeto_GerenteCod[0];
               Z1543Projeto_ServicoCod = T00293_A1543Projeto_ServicoCod[0];
               Z658Projeto_Status = T00293_A658Projeto_Status[0];
               Z985Projeto_FatorEscala = T00293_A985Projeto_FatorEscala[0];
               Z989Projeto_FatorMultiplicador = T00293_A989Projeto_FatorMultiplicador[0];
               Z990Projeto_ConstACocomo = T00293_A990Projeto_ConstACocomo[0];
               Z1232Projeto_Incremental = T00293_A1232Projeto_Incremental[0];
               Z983Projeto_TipoProjetoCod = T00293_A983Projeto_TipoProjetoCod[0];
            }
            else
            {
               Z649Projeto_Nome = A649Projeto_Nome;
               Z650Projeto_Sigla = A650Projeto_Sigla;
               Z651Projeto_TipoContagem = A651Projeto_TipoContagem;
               Z652Projeto_TecnicaContagem = A652Projeto_TecnicaContagem;
               Z1541Projeto_Previsao = A1541Projeto_Previsao;
               Z1542Projeto_GerenteCod = A1542Projeto_GerenteCod;
               Z1543Projeto_ServicoCod = A1543Projeto_ServicoCod;
               Z658Projeto_Status = A658Projeto_Status;
               Z985Projeto_FatorEscala = A985Projeto_FatorEscala;
               Z989Projeto_FatorMultiplicador = A989Projeto_FatorMultiplicador;
               Z990Projeto_ConstACocomo = A990Projeto_ConstACocomo;
               Z1232Projeto_Incremental = A1232Projeto_Incremental;
               Z983Projeto_TipoProjetoCod = A983Projeto_TipoProjetoCod;
            }
         }
         if ( GX_JID == -15 )
         {
            Z648Projeto_Codigo = A648Projeto_Codigo;
            Z649Projeto_Nome = A649Projeto_Nome;
            Z650Projeto_Sigla = A650Projeto_Sigla;
            Z651Projeto_TipoContagem = A651Projeto_TipoContagem;
            Z652Projeto_TecnicaContagem = A652Projeto_TecnicaContagem;
            Z1540Projeto_Introducao = A1540Projeto_Introducao;
            Z653Projeto_Escopo = A653Projeto_Escopo;
            Z1541Projeto_Previsao = A1541Projeto_Previsao;
            Z1542Projeto_GerenteCod = A1542Projeto_GerenteCod;
            Z1543Projeto_ServicoCod = A1543Projeto_ServicoCod;
            Z658Projeto_Status = A658Projeto_Status;
            Z985Projeto_FatorEscala = A985Projeto_FatorEscala;
            Z989Projeto_FatorMultiplicador = A989Projeto_FatorMultiplicador;
            Z990Projeto_ConstACocomo = A990Projeto_ConstACocomo;
            Z1232Projeto_Incremental = A1232Projeto_Incremental;
            Z983Projeto_TipoProjetoCod = A983Projeto_TipoProjetoCod;
            Z655Projeto_Custo = A655Projeto_Custo;
            Z656Projeto_Prazo = A656Projeto_Prazo;
            Z657Projeto_Esforco = A657Projeto_Esforco;
         }
      }

      protected void standaloneNotModal( )
      {
         GXAPROJETO_TIPOPROJETOCOD_html2986( ) ;
         cmbProjeto_Status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbProjeto_Status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbProjeto_Status.Enabled), 5, 0)));
         edtProjeto_FatorEscala_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_FatorEscala_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_FatorEscala_Enabled), 5, 0)));
         edtProjeto_FatorMultiplicador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_FatorMultiplicador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_FatorMultiplicador_Enabled), 5, 0)));
         edtProjeto_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV19Pgmname = "Projeto";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Pgmname", AV19Pgmname);
         cmbProjeto_Status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbProjeto_Status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbProjeto_Status.Enabled), 5, 0)));
         edtProjeto_FatorEscala_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_FatorEscala_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_FatorEscala_Enabled), 5, 0)));
         edtProjeto_FatorMultiplicador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_FatorMultiplicador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_FatorMultiplicador_Enabled), 5, 0)));
         edtProjeto_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Projeto_Codigo) )
         {
            A648Projeto_Codigo = AV7Projeto_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Projeto_TipoProjetoCod) )
         {
            dynProjeto_TipoProjetoCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_TipoProjetoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynProjeto_TipoProjetoCod.Enabled), 5, 0)));
         }
         else
         {
            dynProjeto_TipoProjetoCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_TipoProjetoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynProjeto_TipoProjetoCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Projeto_TipoProjetoCod) )
         {
            A983Projeto_TipoProjetoCod = AV12Insert_Projeto_TipoProjetoCod;
            n983Projeto_TipoProjetoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A658Projeto_Status)) && ( Gx_BScreen == 0 ) )
         {
            A658Projeto_Status = "A";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00296 */
            pr_default.execute(3, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(3) != 101) )
            {
               A655Projeto_Custo = T00296_A655Projeto_Custo[0];
               A656Projeto_Prazo = T00296_A656Projeto_Prazo[0];
               A657Projeto_Esforco = T00296_A657Projeto_Esforco[0];
            }
            else
            {
               A655Projeto_Custo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A655Projeto_Custo", StringUtil.LTrim( StringUtil.Str( A655Projeto_Custo, 12, 2)));
               A656Projeto_Prazo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A656Projeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A656Projeto_Prazo), 4, 0)));
               A657Projeto_Esforco = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A657Projeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A657Projeto_Esforco), 4, 0)));
            }
            pr_default.close(3);
            GXt_int1 = A740Projeto_SistemaCod;
            new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
            A740Projeto_SistemaCod = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A740Projeto_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A740Projeto_SistemaCod), 6, 0)));
         }
      }

      protected void Load2986( )
      {
         /* Using cursor T00298 */
         pr_default.execute(4, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound86 = 1;
            A649Projeto_Nome = T00298_A649Projeto_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A649Projeto_Nome", A649Projeto_Nome);
            A650Projeto_Sigla = T00298_A650Projeto_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A650Projeto_Sigla", A650Projeto_Sigla);
            A651Projeto_TipoContagem = T00298_A651Projeto_TipoContagem[0];
            A652Projeto_TecnicaContagem = T00298_A652Projeto_TecnicaContagem[0];
            A1540Projeto_Introducao = T00298_A1540Projeto_Introducao[0];
            n1540Projeto_Introducao = T00298_n1540Projeto_Introducao[0];
            A653Projeto_Escopo = T00298_A653Projeto_Escopo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A653Projeto_Escopo", A653Projeto_Escopo);
            A1541Projeto_Previsao = T00298_A1541Projeto_Previsao[0];
            n1541Projeto_Previsao = T00298_n1541Projeto_Previsao[0];
            A1542Projeto_GerenteCod = T00298_A1542Projeto_GerenteCod[0];
            n1542Projeto_GerenteCod = T00298_n1542Projeto_GerenteCod[0];
            A1543Projeto_ServicoCod = T00298_A1543Projeto_ServicoCod[0];
            n1543Projeto_ServicoCod = T00298_n1543Projeto_ServicoCod[0];
            A658Projeto_Status = T00298_A658Projeto_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
            A985Projeto_FatorEscala = T00298_A985Projeto_FatorEscala[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A985Projeto_FatorEscala", StringUtil.LTrim( StringUtil.Str( A985Projeto_FatorEscala, 6, 2)));
            n985Projeto_FatorEscala = T00298_n985Projeto_FatorEscala[0];
            A989Projeto_FatorMultiplicador = T00298_A989Projeto_FatorMultiplicador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A989Projeto_FatorMultiplicador", StringUtil.LTrim( StringUtil.Str( A989Projeto_FatorMultiplicador, 6, 2)));
            n989Projeto_FatorMultiplicador = T00298_n989Projeto_FatorMultiplicador[0];
            A990Projeto_ConstACocomo = T00298_A990Projeto_ConstACocomo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A990Projeto_ConstACocomo", StringUtil.LTrim( StringUtil.Str( A990Projeto_ConstACocomo, 6, 2)));
            n990Projeto_ConstACocomo = T00298_n990Projeto_ConstACocomo[0];
            A1232Projeto_Incremental = T00298_A1232Projeto_Incremental[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1232Projeto_Incremental", A1232Projeto_Incremental);
            n1232Projeto_Incremental = T00298_n1232Projeto_Incremental[0];
            A983Projeto_TipoProjetoCod = T00298_A983Projeto_TipoProjetoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)));
            n983Projeto_TipoProjetoCod = T00298_n983Projeto_TipoProjetoCod[0];
            A655Projeto_Custo = T00298_A655Projeto_Custo[0];
            A656Projeto_Prazo = T00298_A656Projeto_Prazo[0];
            A657Projeto_Esforco = T00298_A657Projeto_Esforco[0];
            ZM2986( -15) ;
         }
         pr_default.close(4);
         OnLoadActions2986( ) ;
      }

      protected void OnLoadActions2986( )
      {
         GXt_int1 = A740Projeto_SistemaCod;
         new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         A740Projeto_SistemaCod = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A740Projeto_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A740Projeto_SistemaCod), 6, 0)));
      }

      protected void CheckExtendedTable2986( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T00296 */
         pr_default.execute(3, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            A655Projeto_Custo = T00296_A655Projeto_Custo[0];
            A656Projeto_Prazo = T00296_A656Projeto_Prazo[0];
            A657Projeto_Esforco = T00296_A657Projeto_Esforco[0];
         }
         else
         {
            A655Projeto_Custo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A655Projeto_Custo", StringUtil.LTrim( StringUtil.Str( A655Projeto_Custo, 12, 2)));
            A656Projeto_Prazo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A656Projeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A656Projeto_Prazo), 4, 0)));
            A657Projeto_Esforco = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A657Projeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A657Projeto_Esforco), 4, 0)));
         }
         pr_default.close(3);
         GXt_int1 = A740Projeto_SistemaCod;
         new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         A740Projeto_SistemaCod = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A740Projeto_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A740Projeto_SistemaCod), 6, 0)));
         /* Using cursor T00294 */
         pr_default.execute(2, new Object[] {n983Projeto_TipoProjetoCod, A983Projeto_TipoProjetoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A983Projeto_TipoProjetoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Projeto_Tipo Projeto'.", "ForeignKeyNotFound", 1, "PROJETO_TIPOPROJETOCOD");
               AnyError = 1;
               GX_FocusControl = dynProjeto_TipoProjetoCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(2);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A651Projeto_TipoContagem)) )
         {
            GX_msglist.addItem("Tipo de Contagem � obrigat�rio!", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors2986( )
      {
         pr_default.close(3);
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_17( int A648Projeto_Codigo )
      {
         /* Using cursor T002910 */
         pr_default.execute(5, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            A655Projeto_Custo = T002910_A655Projeto_Custo[0];
            A656Projeto_Prazo = T002910_A656Projeto_Prazo[0];
            A657Projeto_Esforco = T002910_A657Projeto_Esforco[0];
         }
         else
         {
            A655Projeto_Custo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A655Projeto_Custo", StringUtil.LTrim( StringUtil.Str( A655Projeto_Custo, 12, 2)));
            A656Projeto_Prazo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A656Projeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A656Projeto_Prazo), 4, 0)));
            A657Projeto_Esforco = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A657Projeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A657Projeto_Esforco), 4, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( A655Projeto_Custo, 12, 2, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A656Projeto_Prazo), 4, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A657Projeto_Esforco), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_16( int A983Projeto_TipoProjetoCod )
      {
         /* Using cursor T002911 */
         pr_default.execute(6, new Object[] {n983Projeto_TipoProjetoCod, A983Projeto_TipoProjetoCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A983Projeto_TipoProjetoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Projeto_Tipo Projeto'.", "ForeignKeyNotFound", 1, "PROJETO_TIPOPROJETOCOD");
               AnyError = 1;
               GX_FocusControl = dynProjeto_TipoProjetoCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey2986( )
      {
         /* Using cursor T002912 */
         pr_default.execute(7, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound86 = 1;
         }
         else
         {
            RcdFound86 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00293 */
         pr_default.execute(1, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2986( 15) ;
            RcdFound86 = 1;
            A648Projeto_Codigo = T00293_A648Projeto_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
            A649Projeto_Nome = T00293_A649Projeto_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A649Projeto_Nome", A649Projeto_Nome);
            A650Projeto_Sigla = T00293_A650Projeto_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A650Projeto_Sigla", A650Projeto_Sigla);
            A651Projeto_TipoContagem = T00293_A651Projeto_TipoContagem[0];
            A652Projeto_TecnicaContagem = T00293_A652Projeto_TecnicaContagem[0];
            A1540Projeto_Introducao = T00293_A1540Projeto_Introducao[0];
            n1540Projeto_Introducao = T00293_n1540Projeto_Introducao[0];
            A653Projeto_Escopo = T00293_A653Projeto_Escopo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A653Projeto_Escopo", A653Projeto_Escopo);
            A1541Projeto_Previsao = T00293_A1541Projeto_Previsao[0];
            n1541Projeto_Previsao = T00293_n1541Projeto_Previsao[0];
            A1542Projeto_GerenteCod = T00293_A1542Projeto_GerenteCod[0];
            n1542Projeto_GerenteCod = T00293_n1542Projeto_GerenteCod[0];
            A1543Projeto_ServicoCod = T00293_A1543Projeto_ServicoCod[0];
            n1543Projeto_ServicoCod = T00293_n1543Projeto_ServicoCod[0];
            A658Projeto_Status = T00293_A658Projeto_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
            A985Projeto_FatorEscala = T00293_A985Projeto_FatorEscala[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A985Projeto_FatorEscala", StringUtil.LTrim( StringUtil.Str( A985Projeto_FatorEscala, 6, 2)));
            n985Projeto_FatorEscala = T00293_n985Projeto_FatorEscala[0];
            A989Projeto_FatorMultiplicador = T00293_A989Projeto_FatorMultiplicador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A989Projeto_FatorMultiplicador", StringUtil.LTrim( StringUtil.Str( A989Projeto_FatorMultiplicador, 6, 2)));
            n989Projeto_FatorMultiplicador = T00293_n989Projeto_FatorMultiplicador[0];
            A990Projeto_ConstACocomo = T00293_A990Projeto_ConstACocomo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A990Projeto_ConstACocomo", StringUtil.LTrim( StringUtil.Str( A990Projeto_ConstACocomo, 6, 2)));
            n990Projeto_ConstACocomo = T00293_n990Projeto_ConstACocomo[0];
            A1232Projeto_Incremental = T00293_A1232Projeto_Incremental[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1232Projeto_Incremental", A1232Projeto_Incremental);
            n1232Projeto_Incremental = T00293_n1232Projeto_Incremental[0];
            A983Projeto_TipoProjetoCod = T00293_A983Projeto_TipoProjetoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)));
            n983Projeto_TipoProjetoCod = T00293_n983Projeto_TipoProjetoCod[0];
            Z648Projeto_Codigo = A648Projeto_Codigo;
            sMode86 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2986( ) ;
            if ( AnyError == 1 )
            {
               RcdFound86 = 0;
               InitializeNonKey2986( ) ;
            }
            Gx_mode = sMode86;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound86 = 0;
            InitializeNonKey2986( ) ;
            sMode86 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode86;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2986( ) ;
         if ( RcdFound86 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound86 = 0;
         /* Using cursor T002913 */
         pr_default.execute(8, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T002913_A648Projeto_Codigo[0] < A648Projeto_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T002913_A648Projeto_Codigo[0] > A648Projeto_Codigo ) ) )
            {
               A648Projeto_Codigo = T002913_A648Projeto_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
               RcdFound86 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound86 = 0;
         /* Using cursor T002914 */
         pr_default.execute(9, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T002914_A648Projeto_Codigo[0] > A648Projeto_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T002914_A648Projeto_Codigo[0] < A648Projeto_Codigo ) ) )
            {
               A648Projeto_Codigo = T002914_A648Projeto_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
               RcdFound86 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2986( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtProjeto_Sigla_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2986( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound86 == 1 )
            {
               if ( A648Projeto_Codigo != Z648Projeto_Codigo )
               {
                  A648Projeto_Codigo = Z648Projeto_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "PROJETO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtProjeto_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtProjeto_Sigla_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2986( ) ;
                  GX_FocusControl = edtProjeto_Sigla_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A648Projeto_Codigo != Z648Projeto_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtProjeto_Sigla_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2986( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "PROJETO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtProjeto_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtProjeto_Sigla_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2986( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A648Projeto_Codigo != Z648Projeto_Codigo )
         {
            A648Projeto_Codigo = Z648Projeto_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "PROJETO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtProjeto_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtProjeto_Sigla_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2986( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00292 */
            pr_default.execute(0, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Projeto"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z649Projeto_Nome, T00292_A649Projeto_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z650Projeto_Sigla, T00292_A650Projeto_Sigla[0]) != 0 ) || ( StringUtil.StrCmp(Z651Projeto_TipoContagem, T00292_A651Projeto_TipoContagem[0]) != 0 ) || ( StringUtil.StrCmp(Z652Projeto_TecnicaContagem, T00292_A652Projeto_TecnicaContagem[0]) != 0 ) || ( Z1541Projeto_Previsao != T00292_A1541Projeto_Previsao[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1542Projeto_GerenteCod != T00292_A1542Projeto_GerenteCod[0] ) || ( Z1543Projeto_ServicoCod != T00292_A1543Projeto_ServicoCod[0] ) || ( StringUtil.StrCmp(Z658Projeto_Status, T00292_A658Projeto_Status[0]) != 0 ) || ( Z985Projeto_FatorEscala != T00292_A985Projeto_FatorEscala[0] ) || ( Z989Projeto_FatorMultiplicador != T00292_A989Projeto_FatorMultiplicador[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z990Projeto_ConstACocomo != T00292_A990Projeto_ConstACocomo[0] ) || ( Z1232Projeto_Incremental != T00292_A1232Projeto_Incremental[0] ) || ( Z983Projeto_TipoProjetoCod != T00292_A983Projeto_TipoProjetoCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z649Projeto_Nome, T00292_A649Projeto_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z649Projeto_Nome);
                  GXUtil.WriteLogRaw("Current: ",T00292_A649Projeto_Nome[0]);
               }
               if ( StringUtil.StrCmp(Z650Projeto_Sigla, T00292_A650Projeto_Sigla[0]) != 0 )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_Sigla");
                  GXUtil.WriteLogRaw("Old: ",Z650Projeto_Sigla);
                  GXUtil.WriteLogRaw("Current: ",T00292_A650Projeto_Sigla[0]);
               }
               if ( StringUtil.StrCmp(Z651Projeto_TipoContagem, T00292_A651Projeto_TipoContagem[0]) != 0 )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_TipoContagem");
                  GXUtil.WriteLogRaw("Old: ",Z651Projeto_TipoContagem);
                  GXUtil.WriteLogRaw("Current: ",T00292_A651Projeto_TipoContagem[0]);
               }
               if ( StringUtil.StrCmp(Z652Projeto_TecnicaContagem, T00292_A652Projeto_TecnicaContagem[0]) != 0 )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_TecnicaContagem");
                  GXUtil.WriteLogRaw("Old: ",Z652Projeto_TecnicaContagem);
                  GXUtil.WriteLogRaw("Current: ",T00292_A652Projeto_TecnicaContagem[0]);
               }
               if ( Z1541Projeto_Previsao != T00292_A1541Projeto_Previsao[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_Previsao");
                  GXUtil.WriteLogRaw("Old: ",Z1541Projeto_Previsao);
                  GXUtil.WriteLogRaw("Current: ",T00292_A1541Projeto_Previsao[0]);
               }
               if ( Z1542Projeto_GerenteCod != T00292_A1542Projeto_GerenteCod[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_GerenteCod");
                  GXUtil.WriteLogRaw("Old: ",Z1542Projeto_GerenteCod);
                  GXUtil.WriteLogRaw("Current: ",T00292_A1542Projeto_GerenteCod[0]);
               }
               if ( Z1543Projeto_ServicoCod != T00292_A1543Projeto_ServicoCod[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_ServicoCod");
                  GXUtil.WriteLogRaw("Old: ",Z1543Projeto_ServicoCod);
                  GXUtil.WriteLogRaw("Current: ",T00292_A1543Projeto_ServicoCod[0]);
               }
               if ( StringUtil.StrCmp(Z658Projeto_Status, T00292_A658Projeto_Status[0]) != 0 )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_Status");
                  GXUtil.WriteLogRaw("Old: ",Z658Projeto_Status);
                  GXUtil.WriteLogRaw("Current: ",T00292_A658Projeto_Status[0]);
               }
               if ( Z985Projeto_FatorEscala != T00292_A985Projeto_FatorEscala[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_FatorEscala");
                  GXUtil.WriteLogRaw("Old: ",Z985Projeto_FatorEscala);
                  GXUtil.WriteLogRaw("Current: ",T00292_A985Projeto_FatorEscala[0]);
               }
               if ( Z989Projeto_FatorMultiplicador != T00292_A989Projeto_FatorMultiplicador[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_FatorMultiplicador");
                  GXUtil.WriteLogRaw("Old: ",Z989Projeto_FatorMultiplicador);
                  GXUtil.WriteLogRaw("Current: ",T00292_A989Projeto_FatorMultiplicador[0]);
               }
               if ( Z990Projeto_ConstACocomo != T00292_A990Projeto_ConstACocomo[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_ConstACocomo");
                  GXUtil.WriteLogRaw("Old: ",Z990Projeto_ConstACocomo);
                  GXUtil.WriteLogRaw("Current: ",T00292_A990Projeto_ConstACocomo[0]);
               }
               if ( Z1232Projeto_Incremental != T00292_A1232Projeto_Incremental[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_Incremental");
                  GXUtil.WriteLogRaw("Old: ",Z1232Projeto_Incremental);
                  GXUtil.WriteLogRaw("Current: ",T00292_A1232Projeto_Incremental[0]);
               }
               if ( Z983Projeto_TipoProjetoCod != T00292_A983Projeto_TipoProjetoCod[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_TipoProjetoCod");
                  GXUtil.WriteLogRaw("Old: ",Z983Projeto_TipoProjetoCod);
                  GXUtil.WriteLogRaw("Current: ",T00292_A983Projeto_TipoProjetoCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Projeto"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2986( )
      {
         BeforeValidate2986( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2986( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2986( 0) ;
            CheckOptimisticConcurrency2986( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2986( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2986( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002915 */
                     pr_default.execute(10, new Object[] {A649Projeto_Nome, A650Projeto_Sigla, A651Projeto_TipoContagem, A652Projeto_TecnicaContagem, n1540Projeto_Introducao, A1540Projeto_Introducao, A653Projeto_Escopo, n1541Projeto_Previsao, A1541Projeto_Previsao, n1542Projeto_GerenteCod, A1542Projeto_GerenteCod, n1543Projeto_ServicoCod, A1543Projeto_ServicoCod, A658Projeto_Status, n985Projeto_FatorEscala, A985Projeto_FatorEscala, n989Projeto_FatorMultiplicador, A989Projeto_FatorMultiplicador, n990Projeto_ConstACocomo, A990Projeto_ConstACocomo, n1232Projeto_Incremental, A1232Projeto_Incremental, n983Projeto_TipoProjetoCod, A983Projeto_TipoProjetoCod});
                     A648Projeto_Codigo = T002915_A648Projeto_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("Projeto") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption290( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2986( ) ;
            }
            EndLevel2986( ) ;
         }
         CloseExtendedTableCursors2986( ) ;
      }

      protected void Update2986( )
      {
         BeforeValidate2986( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2986( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2986( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2986( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2986( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002916 */
                     pr_default.execute(11, new Object[] {A649Projeto_Nome, A650Projeto_Sigla, A651Projeto_TipoContagem, A652Projeto_TecnicaContagem, n1540Projeto_Introducao, A1540Projeto_Introducao, A653Projeto_Escopo, n1541Projeto_Previsao, A1541Projeto_Previsao, n1542Projeto_GerenteCod, A1542Projeto_GerenteCod, n1543Projeto_ServicoCod, A1543Projeto_ServicoCod, A658Projeto_Status, n985Projeto_FatorEscala, A985Projeto_FatorEscala, n989Projeto_FatorMultiplicador, A989Projeto_FatorMultiplicador, n990Projeto_ConstACocomo, A990Projeto_ConstACocomo, n1232Projeto_Incremental, A1232Projeto_Incremental, n983Projeto_TipoProjetoCod, A983Projeto_TipoProjetoCod, A648Projeto_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("Projeto") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Projeto"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2986( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2986( ) ;
         }
         CloseExtendedTableCursors2986( ) ;
      }

      protected void DeferredUpdate2986( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2986( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2986( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2986( ) ;
            AfterConfirm2986( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2986( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002917 */
                  pr_default.execute(12, new Object[] {A648Projeto_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("Projeto") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode86 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2986( ) ;
         Gx_mode = sMode86;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2986( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002919 */
            pr_default.execute(13, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               A655Projeto_Custo = T002919_A655Projeto_Custo[0];
               A656Projeto_Prazo = T002919_A656Projeto_Prazo[0];
               A657Projeto_Esforco = T002919_A657Projeto_Esforco[0];
            }
            else
            {
               A655Projeto_Custo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A655Projeto_Custo", StringUtil.LTrim( StringUtil.Str( A655Projeto_Custo, 12, 2)));
               A656Projeto_Prazo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A656Projeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A656Projeto_Prazo), 4, 0)));
               A657Projeto_Esforco = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A657Projeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A657Projeto_Esforco), 4, 0)));
            }
            pr_default.close(13);
            GXt_int1 = A740Projeto_SistemaCod;
            new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
            A740Projeto_SistemaCod = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A740Projeto_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A740Projeto_SistemaCod), 6, 0)));
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T002920 */
            pr_default.execute(14, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Vari�vel Cocomo"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
            /* Using cursor T002921 */
            pr_default.execute(15, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(15) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(15);
            /* Using cursor T002922 */
            pr_default.execute(16, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(16) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Projeto Melhoria"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(16);
            /* Using cursor T002923 */
            pr_default.execute(17, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(17) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistema"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(17);
            /* Using cursor T002924 */
            pr_default.execute(18, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(18) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Projeto Desenvolvimento"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(18);
            /* Using cursor T002925 */
            pr_default.execute(19, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(19) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Proposta"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(19);
         }
      }

      protected void EndLevel2986( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2986( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(13);
            context.CommitDataStores( "Projeto");
            if ( AnyError == 0 )
            {
               ConfirmValues290( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(13);
            context.RollbackDataStores( "Projeto");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2986( )
      {
         /* Scan By routine */
         /* Using cursor T002926 */
         pr_default.execute(20);
         RcdFound86 = 0;
         if ( (pr_default.getStatus(20) != 101) )
         {
            RcdFound86 = 1;
            A648Projeto_Codigo = T002926_A648Projeto_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2986( )
      {
         /* Scan next routine */
         pr_default.readNext(20);
         RcdFound86 = 0;
         if ( (pr_default.getStatus(20) != 101) )
         {
            RcdFound86 = 1;
            A648Projeto_Codigo = T002926_A648Projeto_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2986( )
      {
         pr_default.close(20);
      }

      protected void AfterConfirm2986( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2986( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2986( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2986( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2986( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2986( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2986( )
      {
         edtProjeto_Sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_Sigla_Enabled), 5, 0)));
         edtProjeto_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_Nome_Enabled), 5, 0)));
         dynProjeto_TipoProjetoCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_TipoProjetoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynProjeto_TipoProjetoCod.Enabled), 5, 0)));
         edtProjeto_Escopo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Escopo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_Escopo_Enabled), 5, 0)));
         cmbProjeto_Status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbProjeto_Status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbProjeto_Status.Enabled), 5, 0)));
         edtProjeto_FatorEscala_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_FatorEscala_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_FatorEscala_Enabled), 5, 0)));
         edtProjeto_FatorMultiplicador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_FatorMultiplicador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_FatorMultiplicador_Enabled), 5, 0)));
         edtProjeto_ConstACocomo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_ConstACocomo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_ConstACocomo_Enabled), 5, 0)));
         chkProjeto_Incremental.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkProjeto_Incremental_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkProjeto_Incremental.Enabled), 5, 0)));
         edtProjeto_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues290( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020312021729");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("projeto.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Projeto_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z648Projeto_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z648Projeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z649Projeto_Nome", StringUtil.RTrim( Z649Projeto_Nome));
         GxWebStd.gx_hidden_field( context, "Z650Projeto_Sigla", StringUtil.RTrim( Z650Projeto_Sigla));
         GxWebStd.gx_hidden_field( context, "Z651Projeto_TipoContagem", StringUtil.RTrim( Z651Projeto_TipoContagem));
         GxWebStd.gx_hidden_field( context, "Z652Projeto_TecnicaContagem", StringUtil.RTrim( Z652Projeto_TecnicaContagem));
         GxWebStd.gx_hidden_field( context, "Z1541Projeto_Previsao", context.localUtil.DToC( Z1541Projeto_Previsao, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1542Projeto_GerenteCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1542Projeto_GerenteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1543Projeto_ServicoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1543Projeto_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z658Projeto_Status", StringUtil.RTrim( Z658Projeto_Status));
         GxWebStd.gx_hidden_field( context, "Z985Projeto_FatorEscala", StringUtil.LTrim( StringUtil.NToC( Z985Projeto_FatorEscala, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z989Projeto_FatorMultiplicador", StringUtil.LTrim( StringUtil.NToC( Z989Projeto_FatorMultiplicador, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z990Projeto_ConstACocomo", StringUtil.LTrim( StringUtil.NToC( Z990Projeto_ConstACocomo, 6, 2, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1232Projeto_Incremental", Z1232Projeto_Incremental);
         GxWebStd.gx_hidden_field( context, "Z983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z983Projeto_TipoProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A983Projeto_TipoProjetoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV8WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV8WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "PROJETO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A740Projeto_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPROJETO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Projeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PROJETO_TIPOPROJETOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_Projeto_TipoProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROJETO_TIPOCONTAGEM", StringUtil.RTrim( A651Projeto_TipoContagem));
         GxWebStd.gx_hidden_field( context, "PROJETO_TECNICACONTAGEM", StringUtil.RTrim( A652Projeto_TecnicaContagem));
         GxWebStd.gx_hidden_field( context, "PROJETO_INTRODUCAO", A1540Projeto_Introducao);
         GxWebStd.gx_hidden_field( context, "PROJETO_PREVISAO", context.localUtil.DToC( A1541Projeto_Previsao, 0, "/"));
         GxWebStd.gx_hidden_field( context, "PROJETO_GERENTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1542Projeto_GerenteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROJETO_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1543Projeto_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROJETO_CUSTO", StringUtil.LTrim( StringUtil.NToC( A655Projeto_Custo, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROJETO_PRAZO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A656Projeto_Prazo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROJETO_ESFORCO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A657Projeto_Esforco), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV19Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Projeto";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A651Projeto_TipoContagem, ""));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A652Projeto_TecnicaContagem, ""));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1541Projeto_Previsao, "99/99/99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1542Projeto_GerenteCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1543Projeto_ServicoCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("projeto:[SendSecurityCheck value for]"+"Projeto_Status:"+StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, "")));
         GXUtil.WriteLog("projeto:[SendSecurityCheck value for]"+"Projeto_Codigo:"+context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("projeto:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("projeto:[SendSecurityCheck value for]"+"Projeto_TipoContagem:"+StringUtil.RTrim( context.localUtil.Format( A651Projeto_TipoContagem, "")));
         GXUtil.WriteLog("projeto:[SendSecurityCheck value for]"+"Projeto_TecnicaContagem:"+StringUtil.RTrim( context.localUtil.Format( A652Projeto_TecnicaContagem, "")));
         GXUtil.WriteLog("projeto:[SendSecurityCheck value for]"+"Projeto_Previsao:"+context.localUtil.Format(A1541Projeto_Previsao, "99/99/99"));
         GXUtil.WriteLog("projeto:[SendSecurityCheck value for]"+"Projeto_GerenteCod:"+context.localUtil.Format( (decimal)(A1542Projeto_GerenteCod), "ZZZZZ9"));
         GXUtil.WriteLog("projeto:[SendSecurityCheck value for]"+"Projeto_ServicoCod:"+context.localUtil.Format( (decimal)(A1543Projeto_ServicoCod), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("projeto.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Projeto_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Projeto" ;
      }

      public override String GetPgmdesc( )
      {
         return "Projeto" ;
      }

      protected void InitializeNonKey2986( )
      {
         A983Projeto_TipoProjetoCod = 0;
         n983Projeto_TipoProjetoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)));
         n983Projeto_TipoProjetoCod = ((0==A983Projeto_TipoProjetoCod) ? true : false);
         A740Projeto_SistemaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A740Projeto_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A740Projeto_SistemaCod), 6, 0)));
         A649Projeto_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A649Projeto_Nome", A649Projeto_Nome);
         A650Projeto_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A650Projeto_Sigla", A650Projeto_Sigla);
         A651Projeto_TipoContagem = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A651Projeto_TipoContagem", A651Projeto_TipoContagem);
         A652Projeto_TecnicaContagem = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A652Projeto_TecnicaContagem", A652Projeto_TecnicaContagem);
         A1540Projeto_Introducao = "";
         n1540Projeto_Introducao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1540Projeto_Introducao", A1540Projeto_Introducao);
         A653Projeto_Escopo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A653Projeto_Escopo", A653Projeto_Escopo);
         A1541Projeto_Previsao = DateTime.MinValue;
         n1541Projeto_Previsao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1541Projeto_Previsao", context.localUtil.Format(A1541Projeto_Previsao, "99/99/99"));
         A1542Projeto_GerenteCod = 0;
         n1542Projeto_GerenteCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1542Projeto_GerenteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1542Projeto_GerenteCod), 6, 0)));
         A1543Projeto_ServicoCod = 0;
         n1543Projeto_ServicoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1543Projeto_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1543Projeto_ServicoCod), 6, 0)));
         A655Projeto_Custo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A655Projeto_Custo", StringUtil.LTrim( StringUtil.Str( A655Projeto_Custo, 12, 2)));
         A656Projeto_Prazo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A656Projeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A656Projeto_Prazo), 4, 0)));
         A657Projeto_Esforco = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A657Projeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A657Projeto_Esforco), 4, 0)));
         A985Projeto_FatorEscala = 0;
         n985Projeto_FatorEscala = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A985Projeto_FatorEscala", StringUtil.LTrim( StringUtil.Str( A985Projeto_FatorEscala, 6, 2)));
         n985Projeto_FatorEscala = ((Convert.ToDecimal(0)==A985Projeto_FatorEscala) ? true : false);
         A989Projeto_FatorMultiplicador = 0;
         n989Projeto_FatorMultiplicador = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A989Projeto_FatorMultiplicador", StringUtil.LTrim( StringUtil.Str( A989Projeto_FatorMultiplicador, 6, 2)));
         n989Projeto_FatorMultiplicador = ((Convert.ToDecimal(0)==A989Projeto_FatorMultiplicador) ? true : false);
         A990Projeto_ConstACocomo = 0;
         n990Projeto_ConstACocomo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A990Projeto_ConstACocomo", StringUtil.LTrim( StringUtil.Str( A990Projeto_ConstACocomo, 6, 2)));
         n990Projeto_ConstACocomo = ((Convert.ToDecimal(0)==A990Projeto_ConstACocomo) ? true : false);
         A1232Projeto_Incremental = false;
         n1232Projeto_Incremental = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1232Projeto_Incremental", A1232Projeto_Incremental);
         n1232Projeto_Incremental = ((false==A1232Projeto_Incremental) ? true : false);
         A658Projeto_Status = "A";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
         Z649Projeto_Nome = "";
         Z650Projeto_Sigla = "";
         Z651Projeto_TipoContagem = "";
         Z652Projeto_TecnicaContagem = "";
         Z1541Projeto_Previsao = DateTime.MinValue;
         Z1542Projeto_GerenteCod = 0;
         Z1543Projeto_ServicoCod = 0;
         Z658Projeto_Status = "";
         Z985Projeto_FatorEscala = 0;
         Z989Projeto_FatorMultiplicador = 0;
         Z990Projeto_ConstACocomo = 0;
         Z1232Projeto_Incremental = false;
         Z983Projeto_TipoProjetoCod = 0;
      }

      protected void InitAll2986( )
      {
         A648Projeto_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         InitializeNonKey2986( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A658Projeto_Status = i658Projeto_Status;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020312021763");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("projeto.js", "?2020312021764");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockprojeto_sigla_Internalname = "TEXTBLOCKPROJETO_SIGLA";
         edtProjeto_Sigla_Internalname = "PROJETO_SIGLA";
         lblTextblockprojeto_nome_Internalname = "TEXTBLOCKPROJETO_NOME";
         edtProjeto_Nome_Internalname = "PROJETO_NOME";
         lblTextblockprojeto_tipoprojetocod_Internalname = "TEXTBLOCKPROJETO_TIPOPROJETOCOD";
         dynProjeto_TipoProjetoCod_Internalname = "PROJETO_TIPOPROJETOCOD";
         lblTextblockprojeto_escopo_Internalname = "TEXTBLOCKPROJETO_ESCOPO";
         edtProjeto_Escopo_Internalname = "PROJETO_ESCOPO";
         lblTextblockprojeto_status_Internalname = "TEXTBLOCKPROJETO_STATUS";
         cmbProjeto_Status_Internalname = "PROJETO_STATUS";
         lblTextblockprojeto_fatorescala_Internalname = "TEXTBLOCKPROJETO_FATORESCALA";
         edtProjeto_FatorEscala_Internalname = "PROJETO_FATORESCALA";
         bttBtnfatorescala_Internalname = "BTNFATORESCALA";
         lblTextblockprojeto_fatormultiplicador_Internalname = "TEXTBLOCKPROJETO_FATORMULTIPLICADOR";
         edtProjeto_FatorMultiplicador_Internalname = "PROJETO_FATORMULTIPLICADOR";
         bttBtnfatormultiplicador_Internalname = "BTNFATORMULTIPLICADOR";
         lblTextblockprojeto_constacocomo_Internalname = "TEXTBLOCKPROJETO_CONSTACOCOMO";
         edtProjeto_ConstACocomo_Internalname = "PROJETO_CONSTACOCOMO";
         tblTablemergedprojeto_fatorescala_Internalname = "TABLEMERGEDPROJETO_FATORESCALA";
         lblTextblockprojeto_incremental_Internalname = "TEXTBLOCKPROJETO_INCREMENTAL";
         chkProjeto_Incremental_Internalname = "PROJETO_INCREMENTAL";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtProjeto_Codigo_Internalname = "PROJETO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Projeto";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Projeto";
         edtProjeto_ConstACocomo_Jsonclick = "";
         edtProjeto_ConstACocomo_Enabled = 1;
         bttBtnfatormultiplicador_Visible = 1;
         edtProjeto_FatorMultiplicador_Jsonclick = "";
         edtProjeto_FatorMultiplicador_Enabled = 0;
         bttBtnfatorescala_Visible = 1;
         edtProjeto_FatorEscala_Jsonclick = "";
         edtProjeto_FatorEscala_Enabled = 0;
         chkProjeto_Incremental.Enabled = 1;
         cmbProjeto_Status_Jsonclick = "";
         cmbProjeto_Status.Enabled = 0;
         edtProjeto_Escopo_Enabled = 1;
         dynProjeto_TipoProjetoCod_Jsonclick = "";
         dynProjeto_TipoProjetoCod.Enabled = 1;
         edtProjeto_Nome_Jsonclick = "";
         edtProjeto_Nome_Enabled = 1;
         edtProjeto_Sigla_Jsonclick = "";
         edtProjeto_Sigla_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtProjeto_Codigo_Jsonclick = "";
         edtProjeto_Codigo_Enabled = 0;
         edtProjeto_Codigo_Visible = 1;
         chkProjeto_Incremental.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAPROJETO_TIPOPROJETOCOD2986( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAPROJETO_TIPOPROJETOCOD_data2986( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAPROJETO_TIPOPROJETOCOD_html2986( )
      {
         int gxdynajaxvalue ;
         GXDLAPROJETO_TIPOPROJETOCOD_data2986( ) ;
         gxdynajaxindex = 1;
         dynProjeto_TipoProjetoCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynProjeto_TipoProjetoCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAPROJETO_TIPOPROJETOCOD_data2986( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T002927 */
         pr_default.execute(21);
         while ( (pr_default.getStatus(21) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002927_A983Projeto_TipoProjetoCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002927_A980TipoProjeto_Nome[0]));
            pr_default.readNext(21);
         }
         pr_default.close(21);
      }

      protected void GX3ASAPROJETO_SISTEMACOD2986( int A648Projeto_Codigo )
      {
         GXt_int1 = A740Projeto_SistemaCod;
         new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         A740Projeto_SistemaCod = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A740Projeto_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A740Projeto_SistemaCod), 6, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A740Projeto_SistemaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Projeto_codigo( int GX_Parm1 ,
                                        decimal GX_Parm2 ,
                                        short GX_Parm3 ,
                                        short GX_Parm4 ,
                                        int GX_Parm5 )
      {
         A648Projeto_Codigo = GX_Parm1;
         A655Projeto_Custo = GX_Parm2;
         A656Projeto_Prazo = GX_Parm3;
         A657Projeto_Esforco = GX_Parm4;
         A740Projeto_SistemaCod = GX_Parm5;
         /* Using cursor T002929 */
         pr_default.execute(22, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(22) != 101) )
         {
            A655Projeto_Custo = T002929_A655Projeto_Custo[0];
            A656Projeto_Prazo = T002929_A656Projeto_Prazo[0];
            A657Projeto_Esforco = T002929_A657Projeto_Esforco[0];
         }
         else
         {
            A655Projeto_Custo = 0;
            A656Projeto_Prazo = 0;
            A657Projeto_Esforco = 0;
         }
         pr_default.close(22);
         GXt_int1 = A740Projeto_SistemaCod;
         new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
         A740Projeto_SistemaCod = GXt_int1;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A655Projeto_Custo = 0;
            A656Projeto_Prazo = 0;
            A657Projeto_Esforco = 0;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A655Projeto_Custo, 12, 2, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A656Projeto_Prazo), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A657Projeto_Esforco), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A740Projeto_SistemaCod), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Projeto_tipoprojetocod( GXCombobox dynGX_Parm1 )
      {
         dynProjeto_TipoProjetoCod = dynGX_Parm1;
         A983Projeto_TipoProjetoCod = (int)(NumberUtil.Val( dynProjeto_TipoProjetoCod.CurrentValue, "."));
         n983Projeto_TipoProjetoCod = false;
         /* Using cursor T002930 */
         pr_default.execute(23, new Object[] {n983Projeto_TipoProjetoCod, A983Projeto_TipoProjetoCod});
         if ( (pr_default.getStatus(23) == 101) )
         {
            if ( ! ( (0==A983Projeto_TipoProjetoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Projeto_Tipo Projeto'.", "ForeignKeyNotFound", 1, "PROJETO_TIPOPROJETOCOD");
               AnyError = 1;
               GX_FocusControl = dynProjeto_TipoProjetoCod_Internalname;
            }
         }
         pr_default.close(23);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E14292',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOFATORESCALA'","{handler:'E112986',iparms:[{av:'AV8WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV7Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A985Projeto_FatorEscala',fld:'PROJETO_FATORESCALA',pic:'ZZ9.99',nv:0.0},{av:'AV7Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOFATORMULTIPLICADOR'","{handler:'E122986',iparms:[{av:'AV8WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV7Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A989Projeto_FatorMultiplicador',fld:'PROJETO_FATORMULTIPLICADOR',pic:'ZZ9.99',nv:0.0},{av:'AV7Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(23);
         pr_default.close(22);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z649Projeto_Nome = "";
         Z650Projeto_Sigla = "";
         Z651Projeto_TipoContagem = "";
         Z652Projeto_TecnicaContagem = "";
         Z1541Projeto_Previsao = DateTime.MinValue;
         Z658Projeto_Status = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A658Projeto_Status = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockprojeto_sigla_Jsonclick = "";
         A650Projeto_Sigla = "";
         lblTextblockprojeto_nome_Jsonclick = "";
         A649Projeto_Nome = "";
         lblTextblockprojeto_tipoprojetocod_Jsonclick = "";
         lblTextblockprojeto_escopo_Jsonclick = "";
         A653Projeto_Escopo = "";
         lblTextblockprojeto_status_Jsonclick = "";
         lblTextblockprojeto_fatorescala_Jsonclick = "";
         lblTextblockprojeto_incremental_Jsonclick = "";
         bttBtnfatorescala_Jsonclick = "";
         lblTextblockprojeto_fatormultiplicador_Jsonclick = "";
         bttBtnfatormultiplicador_Jsonclick = "";
         lblTextblockprojeto_constacocomo_Jsonclick = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         A1541Projeto_Previsao = DateTime.MinValue;
         A651Projeto_TipoContagem = "";
         A652Projeto_TecnicaContagem = "";
         A1540Projeto_Introducao = "";
         AV19Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode86 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1540Projeto_Introducao = "";
         Z653Projeto_Escopo = "";
         T00296_A655Projeto_Custo = new decimal[1] ;
         T00296_A656Projeto_Prazo = new short[1] ;
         T00296_A657Projeto_Esforco = new short[1] ;
         T00298_A648Projeto_Codigo = new int[1] ;
         T00298_A649Projeto_Nome = new String[] {""} ;
         T00298_A650Projeto_Sigla = new String[] {""} ;
         T00298_A651Projeto_TipoContagem = new String[] {""} ;
         T00298_A652Projeto_TecnicaContagem = new String[] {""} ;
         T00298_A1540Projeto_Introducao = new String[] {""} ;
         T00298_n1540Projeto_Introducao = new bool[] {false} ;
         T00298_A653Projeto_Escopo = new String[] {""} ;
         T00298_A1541Projeto_Previsao = new DateTime[] {DateTime.MinValue} ;
         T00298_n1541Projeto_Previsao = new bool[] {false} ;
         T00298_A1542Projeto_GerenteCod = new int[1] ;
         T00298_n1542Projeto_GerenteCod = new bool[] {false} ;
         T00298_A1543Projeto_ServicoCod = new int[1] ;
         T00298_n1543Projeto_ServicoCod = new bool[] {false} ;
         T00298_A658Projeto_Status = new String[] {""} ;
         T00298_A985Projeto_FatorEscala = new decimal[1] ;
         T00298_n985Projeto_FatorEscala = new bool[] {false} ;
         T00298_A989Projeto_FatorMultiplicador = new decimal[1] ;
         T00298_n989Projeto_FatorMultiplicador = new bool[] {false} ;
         T00298_A990Projeto_ConstACocomo = new decimal[1] ;
         T00298_n990Projeto_ConstACocomo = new bool[] {false} ;
         T00298_A1232Projeto_Incremental = new bool[] {false} ;
         T00298_n1232Projeto_Incremental = new bool[] {false} ;
         T00298_A983Projeto_TipoProjetoCod = new int[1] ;
         T00298_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         T00298_A655Projeto_Custo = new decimal[1] ;
         T00298_A656Projeto_Prazo = new short[1] ;
         T00298_A657Projeto_Esforco = new short[1] ;
         T00294_A983Projeto_TipoProjetoCod = new int[1] ;
         T00294_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         T002910_A655Projeto_Custo = new decimal[1] ;
         T002910_A656Projeto_Prazo = new short[1] ;
         T002910_A657Projeto_Esforco = new short[1] ;
         T002911_A983Projeto_TipoProjetoCod = new int[1] ;
         T002911_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         T002912_A648Projeto_Codigo = new int[1] ;
         T00293_A648Projeto_Codigo = new int[1] ;
         T00293_A649Projeto_Nome = new String[] {""} ;
         T00293_A650Projeto_Sigla = new String[] {""} ;
         T00293_A651Projeto_TipoContagem = new String[] {""} ;
         T00293_A652Projeto_TecnicaContagem = new String[] {""} ;
         T00293_A1540Projeto_Introducao = new String[] {""} ;
         T00293_n1540Projeto_Introducao = new bool[] {false} ;
         T00293_A653Projeto_Escopo = new String[] {""} ;
         T00293_A1541Projeto_Previsao = new DateTime[] {DateTime.MinValue} ;
         T00293_n1541Projeto_Previsao = new bool[] {false} ;
         T00293_A1542Projeto_GerenteCod = new int[1] ;
         T00293_n1542Projeto_GerenteCod = new bool[] {false} ;
         T00293_A1543Projeto_ServicoCod = new int[1] ;
         T00293_n1543Projeto_ServicoCod = new bool[] {false} ;
         T00293_A658Projeto_Status = new String[] {""} ;
         T00293_A985Projeto_FatorEscala = new decimal[1] ;
         T00293_n985Projeto_FatorEscala = new bool[] {false} ;
         T00293_A989Projeto_FatorMultiplicador = new decimal[1] ;
         T00293_n989Projeto_FatorMultiplicador = new bool[] {false} ;
         T00293_A990Projeto_ConstACocomo = new decimal[1] ;
         T00293_n990Projeto_ConstACocomo = new bool[] {false} ;
         T00293_A1232Projeto_Incremental = new bool[] {false} ;
         T00293_n1232Projeto_Incremental = new bool[] {false} ;
         T00293_A983Projeto_TipoProjetoCod = new int[1] ;
         T00293_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         T002913_A648Projeto_Codigo = new int[1] ;
         T002914_A648Projeto_Codigo = new int[1] ;
         T00292_A648Projeto_Codigo = new int[1] ;
         T00292_A649Projeto_Nome = new String[] {""} ;
         T00292_A650Projeto_Sigla = new String[] {""} ;
         T00292_A651Projeto_TipoContagem = new String[] {""} ;
         T00292_A652Projeto_TecnicaContagem = new String[] {""} ;
         T00292_A1540Projeto_Introducao = new String[] {""} ;
         T00292_n1540Projeto_Introducao = new bool[] {false} ;
         T00292_A653Projeto_Escopo = new String[] {""} ;
         T00292_A1541Projeto_Previsao = new DateTime[] {DateTime.MinValue} ;
         T00292_n1541Projeto_Previsao = new bool[] {false} ;
         T00292_A1542Projeto_GerenteCod = new int[1] ;
         T00292_n1542Projeto_GerenteCod = new bool[] {false} ;
         T00292_A1543Projeto_ServicoCod = new int[1] ;
         T00292_n1543Projeto_ServicoCod = new bool[] {false} ;
         T00292_A658Projeto_Status = new String[] {""} ;
         T00292_A985Projeto_FatorEscala = new decimal[1] ;
         T00292_n985Projeto_FatorEscala = new bool[] {false} ;
         T00292_A989Projeto_FatorMultiplicador = new decimal[1] ;
         T00292_n989Projeto_FatorMultiplicador = new bool[] {false} ;
         T00292_A990Projeto_ConstACocomo = new decimal[1] ;
         T00292_n990Projeto_ConstACocomo = new bool[] {false} ;
         T00292_A1232Projeto_Incremental = new bool[] {false} ;
         T00292_n1232Projeto_Incremental = new bool[] {false} ;
         T00292_A983Projeto_TipoProjetoCod = new int[1] ;
         T00292_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         T002915_A648Projeto_Codigo = new int[1] ;
         T002919_A655Projeto_Custo = new decimal[1] ;
         T002919_A656Projeto_Prazo = new short[1] ;
         T002919_A657Projeto_Esforco = new short[1] ;
         T002920_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         T002920_A962VariavelCocomo_Sigla = new String[] {""} ;
         T002920_A964VariavelCocomo_Tipo = new String[] {""} ;
         T002920_A992VariavelCocomo_Sequencial = new short[1] ;
         T002921_A192Contagem_Codigo = new int[1] ;
         T002922_A736ProjetoMelhoria_Codigo = new int[1] ;
         T002923_A127Sistema_Codigo = new int[1] ;
         T002924_A669ProjetoDesenvolvimento_ProjetoCod = new int[1] ;
         T002924_A670ProjetoDesenvolvimento_SistemaCod = new int[1] ;
         T002925_A1685Proposta_Codigo = new int[1] ;
         T002926_A648Projeto_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i658Projeto_Status = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T002927_A983Projeto_TipoProjetoCod = new int[1] ;
         T002927_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         T002927_A980TipoProjeto_Nome = new String[] {""} ;
         T002927_n980TipoProjeto_Nome = new bool[] {false} ;
         T002929_A655Projeto_Custo = new decimal[1] ;
         T002929_A656Projeto_Prazo = new short[1] ;
         T002929_A657Projeto_Esforco = new short[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         T002930_A983Projeto_TipoProjetoCod = new int[1] ;
         T002930_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.projeto__default(),
            new Object[][] {
                new Object[] {
               T00292_A648Projeto_Codigo, T00292_A649Projeto_Nome, T00292_A650Projeto_Sigla, T00292_A651Projeto_TipoContagem, T00292_A652Projeto_TecnicaContagem, T00292_A1540Projeto_Introducao, T00292_n1540Projeto_Introducao, T00292_A653Projeto_Escopo, T00292_A1541Projeto_Previsao, T00292_n1541Projeto_Previsao,
               T00292_A1542Projeto_GerenteCod, T00292_n1542Projeto_GerenteCod, T00292_A1543Projeto_ServicoCod, T00292_n1543Projeto_ServicoCod, T00292_A658Projeto_Status, T00292_A985Projeto_FatorEscala, T00292_n985Projeto_FatorEscala, T00292_A989Projeto_FatorMultiplicador, T00292_n989Projeto_FatorMultiplicador, T00292_A990Projeto_ConstACocomo,
               T00292_n990Projeto_ConstACocomo, T00292_A1232Projeto_Incremental, T00292_n1232Projeto_Incremental, T00292_A983Projeto_TipoProjetoCod, T00292_n983Projeto_TipoProjetoCod
               }
               , new Object[] {
               T00293_A648Projeto_Codigo, T00293_A649Projeto_Nome, T00293_A650Projeto_Sigla, T00293_A651Projeto_TipoContagem, T00293_A652Projeto_TecnicaContagem, T00293_A1540Projeto_Introducao, T00293_n1540Projeto_Introducao, T00293_A653Projeto_Escopo, T00293_A1541Projeto_Previsao, T00293_n1541Projeto_Previsao,
               T00293_A1542Projeto_GerenteCod, T00293_n1542Projeto_GerenteCod, T00293_A1543Projeto_ServicoCod, T00293_n1543Projeto_ServicoCod, T00293_A658Projeto_Status, T00293_A985Projeto_FatorEscala, T00293_n985Projeto_FatorEscala, T00293_A989Projeto_FatorMultiplicador, T00293_n989Projeto_FatorMultiplicador, T00293_A990Projeto_ConstACocomo,
               T00293_n990Projeto_ConstACocomo, T00293_A1232Projeto_Incremental, T00293_n1232Projeto_Incremental, T00293_A983Projeto_TipoProjetoCod, T00293_n983Projeto_TipoProjetoCod
               }
               , new Object[] {
               T00294_A983Projeto_TipoProjetoCod
               }
               , new Object[] {
               T00296_A655Projeto_Custo, T00296_A656Projeto_Prazo, T00296_A657Projeto_Esforco
               }
               , new Object[] {
               T00298_A648Projeto_Codigo, T00298_A649Projeto_Nome, T00298_A650Projeto_Sigla, T00298_A651Projeto_TipoContagem, T00298_A652Projeto_TecnicaContagem, T00298_A1540Projeto_Introducao, T00298_n1540Projeto_Introducao, T00298_A653Projeto_Escopo, T00298_A1541Projeto_Previsao, T00298_n1541Projeto_Previsao,
               T00298_A1542Projeto_GerenteCod, T00298_n1542Projeto_GerenteCod, T00298_A1543Projeto_ServicoCod, T00298_n1543Projeto_ServicoCod, T00298_A658Projeto_Status, T00298_A985Projeto_FatorEscala, T00298_n985Projeto_FatorEscala, T00298_A989Projeto_FatorMultiplicador, T00298_n989Projeto_FatorMultiplicador, T00298_A990Projeto_ConstACocomo,
               T00298_n990Projeto_ConstACocomo, T00298_A1232Projeto_Incremental, T00298_n1232Projeto_Incremental, T00298_A983Projeto_TipoProjetoCod, T00298_n983Projeto_TipoProjetoCod, T00298_A655Projeto_Custo, T00298_A656Projeto_Prazo, T00298_A657Projeto_Esforco
               }
               , new Object[] {
               T002910_A655Projeto_Custo, T002910_A656Projeto_Prazo, T002910_A657Projeto_Esforco
               }
               , new Object[] {
               T002911_A983Projeto_TipoProjetoCod
               }
               , new Object[] {
               T002912_A648Projeto_Codigo
               }
               , new Object[] {
               T002913_A648Projeto_Codigo
               }
               , new Object[] {
               T002914_A648Projeto_Codigo
               }
               , new Object[] {
               T002915_A648Projeto_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002919_A655Projeto_Custo, T002919_A656Projeto_Prazo, T002919_A657Projeto_Esforco
               }
               , new Object[] {
               T002920_A961VariavelCocomo_AreaTrabalhoCod, T002920_A962VariavelCocomo_Sigla, T002920_A964VariavelCocomo_Tipo, T002920_A992VariavelCocomo_Sequencial
               }
               , new Object[] {
               T002921_A192Contagem_Codigo
               }
               , new Object[] {
               T002922_A736ProjetoMelhoria_Codigo
               }
               , new Object[] {
               T002923_A127Sistema_Codigo
               }
               , new Object[] {
               T002924_A669ProjetoDesenvolvimento_ProjetoCod, T002924_A670ProjetoDesenvolvimento_SistemaCod
               }
               , new Object[] {
               T002925_A1685Proposta_Codigo
               }
               , new Object[] {
               T002926_A648Projeto_Codigo
               }
               , new Object[] {
               T002927_A983Projeto_TipoProjetoCod, T002927_A980TipoProjeto_Nome, T002927_n980TipoProjeto_Nome
               }
               , new Object[] {
               T002929_A655Projeto_Custo, T002929_A656Projeto_Prazo, T002929_A657Projeto_Esforco
               }
               , new Object[] {
               T002930_A983Projeto_TipoProjetoCod
               }
            }
         );
         Z658Projeto_Status = "A";
         A658Projeto_Status = "A";
         i658Projeto_Status = "A";
         AV19Pgmname = "Projeto";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short A656Projeto_Prazo ;
      private short A657Projeto_Esforco ;
      private short RcdFound86 ;
      private short GX_JID ;
      private short Z656Projeto_Prazo ;
      private short Z657Projeto_Esforco ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Projeto_Codigo ;
      private int Z648Projeto_Codigo ;
      private int Z1542Projeto_GerenteCod ;
      private int Z1543Projeto_ServicoCod ;
      private int Z983Projeto_TipoProjetoCod ;
      private int N983Projeto_TipoProjetoCod ;
      private int A648Projeto_Codigo ;
      private int A983Projeto_TipoProjetoCod ;
      private int AV7Projeto_Codigo ;
      private int trnEnded ;
      private int edtProjeto_Codigo_Enabled ;
      private int edtProjeto_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtProjeto_Sigla_Enabled ;
      private int edtProjeto_Nome_Enabled ;
      private int edtProjeto_Escopo_Enabled ;
      private int edtProjeto_FatorEscala_Enabled ;
      private int bttBtnfatorescala_Visible ;
      private int edtProjeto_FatorMultiplicador_Enabled ;
      private int bttBtnfatormultiplicador_Visible ;
      private int edtProjeto_ConstACocomo_Enabled ;
      private int A1542Projeto_GerenteCod ;
      private int A1543Projeto_ServicoCod ;
      private int A740Projeto_SistemaCod ;
      private int AV12Insert_Projeto_TipoProjetoCod ;
      private int AV20GXV1 ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private int GXt_int1 ;
      private decimal Z985Projeto_FatorEscala ;
      private decimal Z989Projeto_FatorMultiplicador ;
      private decimal Z990Projeto_ConstACocomo ;
      private decimal A985Projeto_FatorEscala ;
      private decimal A989Projeto_FatorMultiplicador ;
      private decimal A990Projeto_ConstACocomo ;
      private decimal A655Projeto_Custo ;
      private decimal AV16FatorEscala ;
      private decimal AV15FatorMultiplicador ;
      private decimal Z655Projeto_Custo ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z649Projeto_Nome ;
      private String Z650Projeto_Sigla ;
      private String Z651Projeto_TipoContagem ;
      private String Z652Projeto_TecnicaContagem ;
      private String Z658Projeto_Status ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String A658Projeto_Status ;
      private String chkProjeto_Incremental_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtProjeto_Sigla_Internalname ;
      private String edtProjeto_Codigo_Internalname ;
      private String edtProjeto_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockprojeto_sigla_Internalname ;
      private String lblTextblockprojeto_sigla_Jsonclick ;
      private String A650Projeto_Sigla ;
      private String edtProjeto_Sigla_Jsonclick ;
      private String lblTextblockprojeto_nome_Internalname ;
      private String lblTextblockprojeto_nome_Jsonclick ;
      private String edtProjeto_Nome_Internalname ;
      private String A649Projeto_Nome ;
      private String edtProjeto_Nome_Jsonclick ;
      private String lblTextblockprojeto_tipoprojetocod_Internalname ;
      private String lblTextblockprojeto_tipoprojetocod_Jsonclick ;
      private String dynProjeto_TipoProjetoCod_Internalname ;
      private String dynProjeto_TipoProjetoCod_Jsonclick ;
      private String lblTextblockprojeto_escopo_Internalname ;
      private String lblTextblockprojeto_escopo_Jsonclick ;
      private String edtProjeto_Escopo_Internalname ;
      private String lblTextblockprojeto_status_Internalname ;
      private String lblTextblockprojeto_status_Jsonclick ;
      private String cmbProjeto_Status_Internalname ;
      private String cmbProjeto_Status_Jsonclick ;
      private String lblTextblockprojeto_fatorescala_Internalname ;
      private String lblTextblockprojeto_fatorescala_Jsonclick ;
      private String lblTextblockprojeto_incremental_Internalname ;
      private String lblTextblockprojeto_incremental_Jsonclick ;
      private String tblTablemergedprojeto_fatorescala_Internalname ;
      private String edtProjeto_FatorEscala_Internalname ;
      private String edtProjeto_FatorEscala_Jsonclick ;
      private String bttBtnfatorescala_Internalname ;
      private String bttBtnfatorescala_Jsonclick ;
      private String lblTextblockprojeto_fatormultiplicador_Internalname ;
      private String lblTextblockprojeto_fatormultiplicador_Jsonclick ;
      private String edtProjeto_FatorMultiplicador_Internalname ;
      private String edtProjeto_FatorMultiplicador_Jsonclick ;
      private String bttBtnfatormultiplicador_Internalname ;
      private String bttBtnfatormultiplicador_Jsonclick ;
      private String lblTextblockprojeto_constacocomo_Internalname ;
      private String lblTextblockprojeto_constacocomo_Jsonclick ;
      private String edtProjeto_ConstACocomo_Internalname ;
      private String edtProjeto_ConstACocomo_Jsonclick ;
      private String A651Projeto_TipoContagem ;
      private String A652Projeto_TecnicaContagem ;
      private String AV19Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode86 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String i658Projeto_Status ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private DateTime Z1541Projeto_Previsao ;
      private DateTime A1541Projeto_Previsao ;
      private bool Z1232Projeto_Incremental ;
      private bool entryPointCalled ;
      private bool n983Projeto_TipoProjetoCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A1232Projeto_Incremental ;
      private bool n985Projeto_FatorEscala ;
      private bool n989Projeto_FatorMultiplicador ;
      private bool n990Projeto_ConstACocomo ;
      private bool n1232Projeto_Incremental ;
      private bool n1541Projeto_Previsao ;
      private bool n1542Projeto_GerenteCod ;
      private bool n1543Projeto_ServicoCod ;
      private bool n1540Projeto_Introducao ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String A653Projeto_Escopo ;
      private String A1540Projeto_Introducao ;
      private String Z1540Projeto_Introducao ;
      private String Z653Projeto_Escopo ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynProjeto_TipoProjetoCod ;
      private GXCombobox cmbProjeto_Status ;
      private GXCheckbox chkProjeto_Incremental ;
      private IDataStoreProvider pr_default ;
      private decimal[] T00296_A655Projeto_Custo ;
      private short[] T00296_A656Projeto_Prazo ;
      private short[] T00296_A657Projeto_Esforco ;
      private int[] T00298_A648Projeto_Codigo ;
      private String[] T00298_A649Projeto_Nome ;
      private String[] T00298_A650Projeto_Sigla ;
      private String[] T00298_A651Projeto_TipoContagem ;
      private String[] T00298_A652Projeto_TecnicaContagem ;
      private String[] T00298_A1540Projeto_Introducao ;
      private bool[] T00298_n1540Projeto_Introducao ;
      private String[] T00298_A653Projeto_Escopo ;
      private DateTime[] T00298_A1541Projeto_Previsao ;
      private bool[] T00298_n1541Projeto_Previsao ;
      private int[] T00298_A1542Projeto_GerenteCod ;
      private bool[] T00298_n1542Projeto_GerenteCod ;
      private int[] T00298_A1543Projeto_ServicoCod ;
      private bool[] T00298_n1543Projeto_ServicoCod ;
      private String[] T00298_A658Projeto_Status ;
      private decimal[] T00298_A985Projeto_FatorEscala ;
      private bool[] T00298_n985Projeto_FatorEscala ;
      private decimal[] T00298_A989Projeto_FatorMultiplicador ;
      private bool[] T00298_n989Projeto_FatorMultiplicador ;
      private decimal[] T00298_A990Projeto_ConstACocomo ;
      private bool[] T00298_n990Projeto_ConstACocomo ;
      private bool[] T00298_A1232Projeto_Incremental ;
      private bool[] T00298_n1232Projeto_Incremental ;
      private int[] T00298_A983Projeto_TipoProjetoCod ;
      private bool[] T00298_n983Projeto_TipoProjetoCod ;
      private decimal[] T00298_A655Projeto_Custo ;
      private short[] T00298_A656Projeto_Prazo ;
      private short[] T00298_A657Projeto_Esforco ;
      private int[] T00294_A983Projeto_TipoProjetoCod ;
      private bool[] T00294_n983Projeto_TipoProjetoCod ;
      private decimal[] T002910_A655Projeto_Custo ;
      private short[] T002910_A656Projeto_Prazo ;
      private short[] T002910_A657Projeto_Esforco ;
      private int[] T002911_A983Projeto_TipoProjetoCod ;
      private bool[] T002911_n983Projeto_TipoProjetoCod ;
      private int[] T002912_A648Projeto_Codigo ;
      private int[] T00293_A648Projeto_Codigo ;
      private String[] T00293_A649Projeto_Nome ;
      private String[] T00293_A650Projeto_Sigla ;
      private String[] T00293_A651Projeto_TipoContagem ;
      private String[] T00293_A652Projeto_TecnicaContagem ;
      private String[] T00293_A1540Projeto_Introducao ;
      private bool[] T00293_n1540Projeto_Introducao ;
      private String[] T00293_A653Projeto_Escopo ;
      private DateTime[] T00293_A1541Projeto_Previsao ;
      private bool[] T00293_n1541Projeto_Previsao ;
      private int[] T00293_A1542Projeto_GerenteCod ;
      private bool[] T00293_n1542Projeto_GerenteCod ;
      private int[] T00293_A1543Projeto_ServicoCod ;
      private bool[] T00293_n1543Projeto_ServicoCod ;
      private String[] T00293_A658Projeto_Status ;
      private decimal[] T00293_A985Projeto_FatorEscala ;
      private bool[] T00293_n985Projeto_FatorEscala ;
      private decimal[] T00293_A989Projeto_FatorMultiplicador ;
      private bool[] T00293_n989Projeto_FatorMultiplicador ;
      private decimal[] T00293_A990Projeto_ConstACocomo ;
      private bool[] T00293_n990Projeto_ConstACocomo ;
      private bool[] T00293_A1232Projeto_Incremental ;
      private bool[] T00293_n1232Projeto_Incremental ;
      private int[] T00293_A983Projeto_TipoProjetoCod ;
      private bool[] T00293_n983Projeto_TipoProjetoCod ;
      private int[] T002913_A648Projeto_Codigo ;
      private int[] T002914_A648Projeto_Codigo ;
      private int[] T00292_A648Projeto_Codigo ;
      private String[] T00292_A649Projeto_Nome ;
      private String[] T00292_A650Projeto_Sigla ;
      private String[] T00292_A651Projeto_TipoContagem ;
      private String[] T00292_A652Projeto_TecnicaContagem ;
      private String[] T00292_A1540Projeto_Introducao ;
      private bool[] T00292_n1540Projeto_Introducao ;
      private String[] T00292_A653Projeto_Escopo ;
      private DateTime[] T00292_A1541Projeto_Previsao ;
      private bool[] T00292_n1541Projeto_Previsao ;
      private int[] T00292_A1542Projeto_GerenteCod ;
      private bool[] T00292_n1542Projeto_GerenteCod ;
      private int[] T00292_A1543Projeto_ServicoCod ;
      private bool[] T00292_n1543Projeto_ServicoCod ;
      private String[] T00292_A658Projeto_Status ;
      private decimal[] T00292_A985Projeto_FatorEscala ;
      private bool[] T00292_n985Projeto_FatorEscala ;
      private decimal[] T00292_A989Projeto_FatorMultiplicador ;
      private bool[] T00292_n989Projeto_FatorMultiplicador ;
      private decimal[] T00292_A990Projeto_ConstACocomo ;
      private bool[] T00292_n990Projeto_ConstACocomo ;
      private bool[] T00292_A1232Projeto_Incremental ;
      private bool[] T00292_n1232Projeto_Incremental ;
      private int[] T00292_A983Projeto_TipoProjetoCod ;
      private bool[] T00292_n983Projeto_TipoProjetoCod ;
      private int[] T002915_A648Projeto_Codigo ;
      private decimal[] T002919_A655Projeto_Custo ;
      private short[] T002919_A656Projeto_Prazo ;
      private short[] T002919_A657Projeto_Esforco ;
      private int[] T002920_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] T002920_A962VariavelCocomo_Sigla ;
      private String[] T002920_A964VariavelCocomo_Tipo ;
      private short[] T002920_A992VariavelCocomo_Sequencial ;
      private int[] T002921_A192Contagem_Codigo ;
      private int[] T002922_A736ProjetoMelhoria_Codigo ;
      private int[] T002923_A127Sistema_Codigo ;
      private int[] T002924_A669ProjetoDesenvolvimento_ProjetoCod ;
      private int[] T002924_A670ProjetoDesenvolvimento_SistemaCod ;
      private int[] T002925_A1685Proposta_Codigo ;
      private int[] T002926_A648Projeto_Codigo ;
      private int[] T002927_A983Projeto_TipoProjetoCod ;
      private bool[] T002927_n983Projeto_TipoProjetoCod ;
      private String[] T002927_A980TipoProjeto_Nome ;
      private bool[] T002927_n980TipoProjeto_Nome ;
      private decimal[] T002929_A655Projeto_Custo ;
      private short[] T002929_A656Projeto_Prazo ;
      private short[] T002929_A657Projeto_Esforco ;
      private int[] T002930_A983Projeto_TipoProjetoCod ;
      private bool[] T002930_n983Projeto_TipoProjetoCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class projeto__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00298 ;
          prmT00298 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00296 ;
          prmT00296 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00294 ;
          prmT00294 = new Object[] {
          new Object[] {"@Projeto_TipoProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002910 ;
          prmT002910 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002911 ;
          prmT002911 = new Object[] {
          new Object[] {"@Projeto_TipoProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002912 ;
          prmT002912 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00293 ;
          prmT00293 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002913 ;
          prmT002913 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002914 ;
          prmT002914 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00292 ;
          prmT00292 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002915 ;
          prmT002915 = new Object[] {
          new Object[] {"@Projeto_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Projeto_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Projeto_TipoContagem",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_TecnicaContagem",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_Introducao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_Escopo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_Previsao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Projeto_GerenteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_FatorEscala",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_FatorMultiplicador",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_ConstACocomo",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_Incremental",SqlDbType.Bit,4,0} ,
          new Object[] {"@Projeto_TipoProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002916 ;
          prmT002916 = new Object[] {
          new Object[] {"@Projeto_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Projeto_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Projeto_TipoContagem",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_TecnicaContagem",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_Introducao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_Escopo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_Previsao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Projeto_GerenteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_FatorEscala",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_FatorMultiplicador",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_ConstACocomo",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_Incremental",SqlDbType.Bit,4,0} ,
          new Object[] {"@Projeto_TipoProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002917 ;
          prmT002917 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002919 ;
          prmT002919 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002920 ;
          prmT002920 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002921 ;
          prmT002921 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002922 ;
          prmT002922 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002923 ;
          prmT002923 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002924 ;
          prmT002924 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002925 ;
          prmT002925 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002926 ;
          prmT002926 = new Object[] {
          } ;
          Object[] prmT002927 ;
          prmT002927 = new Object[] {
          } ;
          Object[] prmT002929 ;
          prmT002929 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002930 ;
          prmT002930 = new Object[] {
          new Object[] {"@Projeto_TipoProjetoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00292", "SELECT [Projeto_Codigo], [Projeto_Nome], [Projeto_Sigla], [Projeto_TipoContagem], [Projeto_TecnicaContagem], [Projeto_Introducao], [Projeto_Escopo], [Projeto_Previsao], [Projeto_GerenteCod], [Projeto_ServicoCod], [Projeto_Status], [Projeto_FatorEscala], [Projeto_FatorMultiplicador], [Projeto_ConstACocomo], [Projeto_Incremental], [Projeto_TipoProjetoCod] AS Projeto_TipoProjetoCod FROM [Projeto] WITH (UPDLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00292,1,0,true,false )
             ,new CursorDef("T00293", "SELECT [Projeto_Codigo], [Projeto_Nome], [Projeto_Sigla], [Projeto_TipoContagem], [Projeto_TecnicaContagem], [Projeto_Introducao], [Projeto_Escopo], [Projeto_Previsao], [Projeto_GerenteCod], [Projeto_ServicoCod], [Projeto_Status], [Projeto_FatorEscala], [Projeto_FatorMultiplicador], [Projeto_ConstACocomo], [Projeto_Incremental], [Projeto_TipoProjetoCod] AS Projeto_TipoProjetoCod FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00293,1,0,true,false )
             ,new CursorDef("T00294", "SELECT [TipoProjeto_Codigo] AS Projeto_TipoProjetoCod FROM [TipoProjeto] WITH (NOLOCK) WHERE [TipoProjeto_Codigo] = @Projeto_TipoProjetoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00294,1,0,true,false )
             ,new CursorDef("T00296", "SELECT COALESCE( T1.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T1.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T1.[Projeto_Esforco], 0) AS Projeto_Esforco FROM (SELECT SUM([Sistema_Custo]) AS Projeto_Custo, [Sistema_ProjetoCod], SUM([Sistema_Prazo]) AS Projeto_Prazo, SUM([Sistema_Esforco]) AS Projeto_Esforco FROM [Sistema] WITH (NOLOCK) GROUP BY [Sistema_ProjetoCod] ) T1 WHERE T1.[Sistema_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00296,1,0,true,false )
             ,new CursorDef("T00298", "SELECT TM1.[Projeto_Codigo], TM1.[Projeto_Nome], TM1.[Projeto_Sigla], TM1.[Projeto_TipoContagem], TM1.[Projeto_TecnicaContagem], TM1.[Projeto_Introducao], TM1.[Projeto_Escopo], TM1.[Projeto_Previsao], TM1.[Projeto_GerenteCod], TM1.[Projeto_ServicoCod], TM1.[Projeto_Status], TM1.[Projeto_FatorEscala], TM1.[Projeto_FatorMultiplicador], TM1.[Projeto_ConstACocomo], TM1.[Projeto_Incremental], TM1.[Projeto_TipoProjetoCod] AS Projeto_TipoProjetoCod, COALESCE( T2.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T2.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T2.[Projeto_Esforco], 0) AS Projeto_Esforco FROM ([Projeto] TM1 WITH (NOLOCK) LEFT JOIN (SELECT SUM([Sistema_Custo]) AS Projeto_Custo, [Sistema_ProjetoCod], SUM([Sistema_Prazo]) AS Projeto_Prazo, SUM([Sistema_Esforco]) AS Projeto_Esforco FROM [Sistema] WITH (NOLOCK) GROUP BY [Sistema_ProjetoCod] ) T2 ON T2.[Sistema_ProjetoCod] = TM1.[Projeto_Codigo]) WHERE TM1.[Projeto_Codigo] = @Projeto_Codigo ORDER BY TM1.[Projeto_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00298,100,0,true,false )
             ,new CursorDef("T002910", "SELECT COALESCE( T1.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T1.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T1.[Projeto_Esforco], 0) AS Projeto_Esforco FROM (SELECT SUM([Sistema_Custo]) AS Projeto_Custo, [Sistema_ProjetoCod], SUM([Sistema_Prazo]) AS Projeto_Prazo, SUM([Sistema_Esforco]) AS Projeto_Esforco FROM [Sistema] WITH (NOLOCK) GROUP BY [Sistema_ProjetoCod] ) T1 WHERE T1.[Sistema_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002910,1,0,true,false )
             ,new CursorDef("T002911", "SELECT [TipoProjeto_Codigo] AS Projeto_TipoProjetoCod FROM [TipoProjeto] WITH (NOLOCK) WHERE [TipoProjeto_Codigo] = @Projeto_TipoProjetoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002911,1,0,true,false )
             ,new CursorDef("T002912", "SELECT [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002912,1,0,true,false )
             ,new CursorDef("T002913", "SELECT TOP 1 [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE ( [Projeto_Codigo] > @Projeto_Codigo) ORDER BY [Projeto_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002913,1,0,true,true )
             ,new CursorDef("T002914", "SELECT TOP 1 [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE ( [Projeto_Codigo] < @Projeto_Codigo) ORDER BY [Projeto_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002914,1,0,true,true )
             ,new CursorDef("T002915", "INSERT INTO [Projeto]([Projeto_Nome], [Projeto_Sigla], [Projeto_TipoContagem], [Projeto_TecnicaContagem], [Projeto_Introducao], [Projeto_Escopo], [Projeto_Previsao], [Projeto_GerenteCod], [Projeto_ServicoCod], [Projeto_Status], [Projeto_FatorEscala], [Projeto_FatorMultiplicador], [Projeto_ConstACocomo], [Projeto_Incremental], [Projeto_TipoProjetoCod]) VALUES(@Projeto_Nome, @Projeto_Sigla, @Projeto_TipoContagem, @Projeto_TecnicaContagem, @Projeto_Introducao, @Projeto_Escopo, @Projeto_Previsao, @Projeto_GerenteCod, @Projeto_ServicoCod, @Projeto_Status, @Projeto_FatorEscala, @Projeto_FatorMultiplicador, @Projeto_ConstACocomo, @Projeto_Incremental, @Projeto_TipoProjetoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002915)
             ,new CursorDef("T002916", "UPDATE [Projeto] SET [Projeto_Nome]=@Projeto_Nome, [Projeto_Sigla]=@Projeto_Sigla, [Projeto_TipoContagem]=@Projeto_TipoContagem, [Projeto_TecnicaContagem]=@Projeto_TecnicaContagem, [Projeto_Introducao]=@Projeto_Introducao, [Projeto_Escopo]=@Projeto_Escopo, [Projeto_Previsao]=@Projeto_Previsao, [Projeto_GerenteCod]=@Projeto_GerenteCod, [Projeto_ServicoCod]=@Projeto_ServicoCod, [Projeto_Status]=@Projeto_Status, [Projeto_FatorEscala]=@Projeto_FatorEscala, [Projeto_FatorMultiplicador]=@Projeto_FatorMultiplicador, [Projeto_ConstACocomo]=@Projeto_ConstACocomo, [Projeto_Incremental]=@Projeto_Incremental, [Projeto_TipoProjetoCod]=@Projeto_TipoProjetoCod  WHERE [Projeto_Codigo] = @Projeto_Codigo", GxErrorMask.GX_NOMASK,prmT002916)
             ,new CursorDef("T002917", "DELETE FROM [Projeto]  WHERE [Projeto_Codigo] = @Projeto_Codigo", GxErrorMask.GX_NOMASK,prmT002917)
             ,new CursorDef("T002919", "SELECT COALESCE( T1.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T1.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T1.[Projeto_Esforco], 0) AS Projeto_Esforco FROM (SELECT SUM([Sistema_Custo]) AS Projeto_Custo, [Sistema_ProjetoCod], SUM([Sistema_Prazo]) AS Projeto_Prazo, SUM([Sistema_Esforco]) AS Projeto_Esforco FROM [Sistema] WITH (NOLOCK) GROUP BY [Sistema_ProjetoCod] ) T1 WHERE T1.[Sistema_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002919,1,0,true,false )
             ,new CursorDef("T002920", "SELECT TOP 1 [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial] FROM [VariaveisCocomo] WITH (NOLOCK) WHERE [VariavelCocomo_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002920,1,0,true,true )
             ,new CursorDef("T002921", "SELECT TOP 1 [Contagem_Codigo] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002921,1,0,true,true )
             ,new CursorDef("T002922", "SELECT TOP 1 [ProjetoMelhoria_Codigo] FROM [ProjetoMelhoria] WITH (NOLOCK) WHERE [ProjetoMelhoria_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002922,1,0,true,true )
             ,new CursorDef("T002923", "SELECT TOP 1 [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002923,1,0,true,true )
             ,new CursorDef("T002924", "SELECT TOP 1 [ProjetoDesenvolvimento_ProjetoCod], [ProjetoDesenvolvimento_SistemaCod] FROM [ProjetoDesenvolvimento] WITH (NOLOCK) WHERE [ProjetoDesenvolvimento_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002924,1,0,true,true )
             ,new CursorDef("T002925", "SELECT TOP 1 [Proposta_Codigo] FROM dbo.[Proposta] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002925,1,0,true,true )
             ,new CursorDef("T002926", "SELECT [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) ORDER BY [Projeto_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002926,100,0,true,false )
             ,new CursorDef("T002927", "SELECT [TipoProjeto_Codigo] AS Projeto_TipoProjetoCod, [TipoProjeto_Nome] FROM [TipoProjeto] WITH (NOLOCK) ORDER BY [TipoProjeto_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002927,0,0,true,false )
             ,new CursorDef("T002929", "SELECT COALESCE( T1.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T1.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T1.[Projeto_Esforco], 0) AS Projeto_Esforco FROM (SELECT SUM([Sistema_Custo]) AS Projeto_Custo, [Sistema_ProjetoCod], SUM([Sistema_Prazo]) AS Projeto_Prazo, SUM([Sistema_Esforco]) AS Projeto_Esforco FROM [Sistema] WITH (NOLOCK) GROUP BY [Sistema_ProjetoCod] ) T1 WHERE T1.[Sistema_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002929,1,0,true,false )
             ,new CursorDef("T002930", "SELECT [TipoProjeto_Codigo] AS Projeto_TipoProjetoCod FROM [TipoProjeto] WITH (NOLOCK) WHERE [TipoProjeto_Codigo] = @Projeto_TipoProjetoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002930,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 1) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((String[]) buf[14])[0] = rslt.getString(11, 1) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(12);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(13);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                ((bool[]) buf[21])[0] = rslt.getBool(15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((int[]) buf[23])[0] = rslt.getInt(16) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 1) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((String[]) buf[14])[0] = rslt.getString(11, 1) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(12);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(13);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                ((bool[]) buf[21])[0] = rslt.getBool(15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((int[]) buf[23])[0] = rslt.getInt(16) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 1) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((String[]) buf[14])[0] = rslt.getString(11, 1) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(12);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(13);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                ((bool[]) buf[21])[0] = rslt.getBool(15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((int[]) buf[23])[0] = rslt.getInt(16) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                ((decimal[]) buf[25])[0] = rslt.getDecimal(17) ;
                ((short[]) buf[26])[0] = rslt.getShort(18) ;
                ((short[]) buf[27])[0] = rslt.getShort(19) ;
                return;
             case 5 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 22 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[5]);
                }
                stmt.SetParameter(6, (String)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(7, (DateTime)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[12]);
                }
                stmt.SetParameter(10, (String)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 13 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(13, (decimal)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 14 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(14, (bool)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[23]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[5]);
                }
                stmt.SetParameter(6, (String)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(7, (DateTime)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[12]);
                }
                stmt.SetParameter(10, (String)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 13 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(13, (decimal)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 14 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(14, (bool)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[23]);
                }
                stmt.SetParameter(16, (int)parms[24]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
