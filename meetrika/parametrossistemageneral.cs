/*
               File: ParametrosSistemaGeneral
        Description: Parametros Sistema General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:19:58.41
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class parametrossistemageneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public parametrossistemageneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public parametrossistemageneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ParametrosSistema_Codigo )
      {
         this.A330ParametrosSistema_Codigo = aP0_ParametrosSistema_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbParametrosSistema_LicensiadoCadastrado = new GXCombobox();
         cmbParametrosSistema_PadronizarStrings = new GXCombobox();
         cmbParametrosSistema_EmailSdaAut = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A330ParametrosSistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A330ParametrosSistema_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A330ParametrosSistema_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA8J2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "ParametrosSistemaGeneral";
               context.Gx_err = 0;
               WS8J2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Parametros Sistema General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117195846");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("parametrossistemageneral.aspx") + "?" + UrlEncode("" +A330ParametrosSistema_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA330ParametrosSistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"PARAMETROSSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A330ParametrosSistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PARAMETROSSISTEMA_NOMESISTEMA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A331ParametrosSistema_NomeSistema, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PARAMETROSSISTEMA_LICENSIADOCADASTRADO", GetSecureSignedToken( sPrefix, A399ParametrosSistema_LicensiadoCadastrado));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PARAMETROSSISTEMA_PADRONIZARSTRINGS", GetSecureSignedToken( sPrefix, A417ParametrosSistema_PadronizarStrings));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PARAMETROSSISTEMA_FATORAJUSTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A708ParametrosSistema_FatorAjuste, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PARAMETROSSISTEMA_PATHCRTF", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1021ParametrosSistema_PathCrtf, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PARAMETROSSISTEMA_EMAILSDAHOST", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A532ParametrosSistema_EmailSdaHost, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PARAMETROSSISTEMA_EMAILSDAAUT", GetSecureSignedToken( sPrefix, A535ParametrosSistema_EmailSdaAut));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PARAMETROSSISTEMA_EMAILSDAUSER", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A533ParametrosSistema_EmailSdaUser, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PARAMETROSSISTEMA_EMAILSDAPORT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A536ParametrosSistema_EmailSdaPort), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PARAMETROSSISTEMA_APPID", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A334ParametrosSistema_AppID), "ZZZZZZZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm8J2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("parametrossistemageneral.js", "?20203117195849");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ParametrosSistemaGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Parametros Sistema General" ;
      }

      protected void WB8J0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "parametrossistemageneral.aspx");
            }
            wb_table1_2_8J2( true) ;
         }
         else
         {
            wb_table1_2_8J2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_8J2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START8J2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Parametros Sistema General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP8J0( ) ;
            }
         }
      }

      protected void WS8J2( )
      {
         START8J2( ) ;
         EVT8J2( ) ;
      }

      protected void EVT8J2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8J0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8J0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E118J2 */
                                    E118J2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8J0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E128J2 */
                                    E128J2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8J0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E138J2 */
                                    E138J2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8J0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E148J2 */
                                    E148J2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8J0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E158J2 */
                                    E158J2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8J0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP8J0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE8J2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm8J2( ) ;
            }
         }
      }

      protected void PA8J2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbParametrosSistema_LicensiadoCadastrado.Name = "PARAMETROSSISTEMA_LICENSIADOCADASTRADO";
            cmbParametrosSistema_LicensiadoCadastrado.WebTags = "";
            cmbParametrosSistema_LicensiadoCadastrado.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbParametrosSistema_LicensiadoCadastrado.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbParametrosSistema_LicensiadoCadastrado.ItemCount > 0 )
            {
               A399ParametrosSistema_LicensiadoCadastrado = StringUtil.StrToBool( cmbParametrosSistema_LicensiadoCadastrado.getValidValue(StringUtil.BoolToStr( A399ParametrosSistema_LicensiadoCadastrado)));
               n399ParametrosSistema_LicensiadoCadastrado = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A399ParametrosSistema_LicensiadoCadastrado", A399ParametrosSistema_LicensiadoCadastrado);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_LICENSIADOCADASTRADO", GetSecureSignedToken( sPrefix, A399ParametrosSistema_LicensiadoCadastrado));
            }
            cmbParametrosSistema_PadronizarStrings.Name = "PARAMETROSSISTEMA_PADRONIZARSTRINGS";
            cmbParametrosSistema_PadronizarStrings.WebTags = "";
            cmbParametrosSistema_PadronizarStrings.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbParametrosSistema_PadronizarStrings.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbParametrosSistema_PadronizarStrings.ItemCount > 0 )
            {
               A417ParametrosSistema_PadronizarStrings = StringUtil.StrToBool( cmbParametrosSistema_PadronizarStrings.getValidValue(StringUtil.BoolToStr( A417ParametrosSistema_PadronizarStrings)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A417ParametrosSistema_PadronizarStrings", A417ParametrosSistema_PadronizarStrings);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_PADRONIZARSTRINGS", GetSecureSignedToken( sPrefix, A417ParametrosSistema_PadronizarStrings));
            }
            cmbParametrosSistema_EmailSdaAut.Name = "PARAMETROSSISTEMA_EMAILSDAAUT";
            cmbParametrosSistema_EmailSdaAut.WebTags = "";
            cmbParametrosSistema_EmailSdaAut.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbParametrosSistema_EmailSdaAut.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbParametrosSistema_EmailSdaAut.ItemCount > 0 )
            {
               A535ParametrosSistema_EmailSdaAut = StringUtil.StrToBool( cmbParametrosSistema_EmailSdaAut.getValidValue(StringUtil.BoolToStr( A535ParametrosSistema_EmailSdaAut)));
               n535ParametrosSistema_EmailSdaAut = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A535ParametrosSistema_EmailSdaAut", A535ParametrosSistema_EmailSdaAut);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_EMAILSDAAUT", GetSecureSignedToken( sPrefix, A535ParametrosSistema_EmailSdaAut));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbParametrosSistema_LicensiadoCadastrado.ItemCount > 0 )
         {
            A399ParametrosSistema_LicensiadoCadastrado = StringUtil.StrToBool( cmbParametrosSistema_LicensiadoCadastrado.getValidValue(StringUtil.BoolToStr( A399ParametrosSistema_LicensiadoCadastrado)));
            n399ParametrosSistema_LicensiadoCadastrado = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A399ParametrosSistema_LicensiadoCadastrado", A399ParametrosSistema_LicensiadoCadastrado);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_LICENSIADOCADASTRADO", GetSecureSignedToken( sPrefix, A399ParametrosSistema_LicensiadoCadastrado));
         }
         if ( cmbParametrosSistema_PadronizarStrings.ItemCount > 0 )
         {
            A417ParametrosSistema_PadronizarStrings = StringUtil.StrToBool( cmbParametrosSistema_PadronizarStrings.getValidValue(StringUtil.BoolToStr( A417ParametrosSistema_PadronizarStrings)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A417ParametrosSistema_PadronizarStrings", A417ParametrosSistema_PadronizarStrings);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_PADRONIZARSTRINGS", GetSecureSignedToken( sPrefix, A417ParametrosSistema_PadronizarStrings));
         }
         if ( cmbParametrosSistema_EmailSdaAut.ItemCount > 0 )
         {
            A535ParametrosSistema_EmailSdaAut = StringUtil.StrToBool( cmbParametrosSistema_EmailSdaAut.getValidValue(StringUtil.BoolToStr( A535ParametrosSistema_EmailSdaAut)));
            n535ParametrosSistema_EmailSdaAut = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A535ParametrosSistema_EmailSdaAut", A535ParametrosSistema_EmailSdaAut);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_EMAILSDAAUT", GetSecureSignedToken( sPrefix, A535ParametrosSistema_EmailSdaAut));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF8J2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "ParametrosSistemaGeneral";
         context.Gx_err = 0;
      }

      protected void RF8J2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H008J2 */
            pr_default.execute(0, new Object[] {A330ParametrosSistema_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A334ParametrosSistema_AppID = H008J2_A334ParametrosSistema_AppID[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A334ParametrosSistema_AppID", StringUtil.LTrim( StringUtil.Str( (decimal)(A334ParametrosSistema_AppID), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_APPID", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A334ParametrosSistema_AppID), "ZZZZZZZZZZZ9")));
               A536ParametrosSistema_EmailSdaPort = H008J2_A536ParametrosSistema_EmailSdaPort[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A536ParametrosSistema_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(A536ParametrosSistema_EmailSdaPort), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_EMAILSDAPORT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A536ParametrosSistema_EmailSdaPort), "ZZZ9")));
               n536ParametrosSistema_EmailSdaPort = H008J2_n536ParametrosSistema_EmailSdaPort[0];
               A533ParametrosSistema_EmailSdaUser = H008J2_A533ParametrosSistema_EmailSdaUser[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A533ParametrosSistema_EmailSdaUser", A533ParametrosSistema_EmailSdaUser);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_EMAILSDAUSER", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A533ParametrosSistema_EmailSdaUser, ""))));
               n533ParametrosSistema_EmailSdaUser = H008J2_n533ParametrosSistema_EmailSdaUser[0];
               A535ParametrosSistema_EmailSdaAut = H008J2_A535ParametrosSistema_EmailSdaAut[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A535ParametrosSistema_EmailSdaAut", A535ParametrosSistema_EmailSdaAut);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_EMAILSDAAUT", GetSecureSignedToken( sPrefix, A535ParametrosSistema_EmailSdaAut));
               n535ParametrosSistema_EmailSdaAut = H008J2_n535ParametrosSistema_EmailSdaAut[0];
               A532ParametrosSistema_EmailSdaHost = H008J2_A532ParametrosSistema_EmailSdaHost[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A532ParametrosSistema_EmailSdaHost", A532ParametrosSistema_EmailSdaHost);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_EMAILSDAHOST", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A532ParametrosSistema_EmailSdaHost, ""))));
               n532ParametrosSistema_EmailSdaHost = H008J2_n532ParametrosSistema_EmailSdaHost[0];
               A1021ParametrosSistema_PathCrtf = H008J2_A1021ParametrosSistema_PathCrtf[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1021ParametrosSistema_PathCrtf", A1021ParametrosSistema_PathCrtf);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_PATHCRTF", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1021ParametrosSistema_PathCrtf, ""))));
               n1021ParametrosSistema_PathCrtf = H008J2_n1021ParametrosSistema_PathCrtf[0];
               A708ParametrosSistema_FatorAjuste = H008J2_A708ParametrosSistema_FatorAjuste[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A708ParametrosSistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A708ParametrosSistema_FatorAjuste, 6, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_FATORAJUSTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A708ParametrosSistema_FatorAjuste, "ZZ9.99")));
               n708ParametrosSistema_FatorAjuste = H008J2_n708ParametrosSistema_FatorAjuste[0];
               A417ParametrosSistema_PadronizarStrings = H008J2_A417ParametrosSistema_PadronizarStrings[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A417ParametrosSistema_PadronizarStrings", A417ParametrosSistema_PadronizarStrings);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_PADRONIZARSTRINGS", GetSecureSignedToken( sPrefix, A417ParametrosSistema_PadronizarStrings));
               A399ParametrosSistema_LicensiadoCadastrado = H008J2_A399ParametrosSistema_LicensiadoCadastrado[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A399ParametrosSistema_LicensiadoCadastrado", A399ParametrosSistema_LicensiadoCadastrado);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_LICENSIADOCADASTRADO", GetSecureSignedToken( sPrefix, A399ParametrosSistema_LicensiadoCadastrado));
               n399ParametrosSistema_LicensiadoCadastrado = H008J2_n399ParametrosSistema_LicensiadoCadastrado[0];
               A331ParametrosSistema_NomeSistema = H008J2_A331ParametrosSistema_NomeSistema[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A331ParametrosSistema_NomeSistema", A331ParametrosSistema_NomeSistema);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_NOMESISTEMA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A331ParametrosSistema_NomeSistema, "@!"))));
               /* Execute user event: E128J2 */
               E128J2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB8J0( ) ;
         }
      }

      protected void STRUP8J0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "ParametrosSistemaGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E118J2 */
         E118J2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A331ParametrosSistema_NomeSistema = StringUtil.Upper( cgiGet( edtParametrosSistema_NomeSistema_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A331ParametrosSistema_NomeSistema", A331ParametrosSistema_NomeSistema);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_NOMESISTEMA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A331ParametrosSistema_NomeSistema, "@!"))));
            cmbParametrosSistema_LicensiadoCadastrado.CurrentValue = cgiGet( cmbParametrosSistema_LicensiadoCadastrado_Internalname);
            A399ParametrosSistema_LicensiadoCadastrado = StringUtil.StrToBool( cgiGet( cmbParametrosSistema_LicensiadoCadastrado_Internalname));
            n399ParametrosSistema_LicensiadoCadastrado = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A399ParametrosSistema_LicensiadoCadastrado", A399ParametrosSistema_LicensiadoCadastrado);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_LICENSIADOCADASTRADO", GetSecureSignedToken( sPrefix, A399ParametrosSistema_LicensiadoCadastrado));
            cmbParametrosSistema_PadronizarStrings.CurrentValue = cgiGet( cmbParametrosSistema_PadronizarStrings_Internalname);
            A417ParametrosSistema_PadronizarStrings = StringUtil.StrToBool( cgiGet( cmbParametrosSistema_PadronizarStrings_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A417ParametrosSistema_PadronizarStrings", A417ParametrosSistema_PadronizarStrings);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_PADRONIZARSTRINGS", GetSecureSignedToken( sPrefix, A417ParametrosSistema_PadronizarStrings));
            A708ParametrosSistema_FatorAjuste = context.localUtil.CToN( cgiGet( edtParametrosSistema_FatorAjuste_Internalname), ",", ".");
            n708ParametrosSistema_FatorAjuste = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A708ParametrosSistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A708ParametrosSistema_FatorAjuste, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_FATORAJUSTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A708ParametrosSistema_FatorAjuste, "ZZ9.99")));
            A1021ParametrosSistema_PathCrtf = cgiGet( edtParametrosSistema_PathCrtf_Internalname);
            n1021ParametrosSistema_PathCrtf = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1021ParametrosSistema_PathCrtf", A1021ParametrosSistema_PathCrtf);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_PATHCRTF", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1021ParametrosSistema_PathCrtf, ""))));
            A532ParametrosSistema_EmailSdaHost = cgiGet( edtParametrosSistema_EmailSdaHost_Internalname);
            n532ParametrosSistema_EmailSdaHost = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A532ParametrosSistema_EmailSdaHost", A532ParametrosSistema_EmailSdaHost);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_EMAILSDAHOST", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A532ParametrosSistema_EmailSdaHost, ""))));
            cmbParametrosSistema_EmailSdaAut.CurrentValue = cgiGet( cmbParametrosSistema_EmailSdaAut_Internalname);
            A535ParametrosSistema_EmailSdaAut = StringUtil.StrToBool( cgiGet( cmbParametrosSistema_EmailSdaAut_Internalname));
            n535ParametrosSistema_EmailSdaAut = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A535ParametrosSistema_EmailSdaAut", A535ParametrosSistema_EmailSdaAut);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_EMAILSDAAUT", GetSecureSignedToken( sPrefix, A535ParametrosSistema_EmailSdaAut));
            A533ParametrosSistema_EmailSdaUser = cgiGet( edtParametrosSistema_EmailSdaUser_Internalname);
            n533ParametrosSistema_EmailSdaUser = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A533ParametrosSistema_EmailSdaUser", A533ParametrosSistema_EmailSdaUser);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_EMAILSDAUSER", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A533ParametrosSistema_EmailSdaUser, ""))));
            A536ParametrosSistema_EmailSdaPort = (short)(context.localUtil.CToN( cgiGet( edtParametrosSistema_EmailSdaPort_Internalname), ",", "."));
            n536ParametrosSistema_EmailSdaPort = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A536ParametrosSistema_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(A536ParametrosSistema_EmailSdaPort), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_EMAILSDAPORT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A536ParametrosSistema_EmailSdaPort), "ZZZ9")));
            A334ParametrosSistema_AppID = (long)(context.localUtil.CToN( cgiGet( edtParametrosSistema_AppID_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A334ParametrosSistema_AppID", StringUtil.LTrim( StringUtil.Str( (decimal)(A334ParametrosSistema_AppID), 12, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSSISTEMA_APPID", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A334ParametrosSistema_AppID), "ZZZZZZZZZZZ9")));
            /* Read saved values. */
            wcpOA330ParametrosSistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA330ParametrosSistema_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E118J2 */
         E118J2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E118J2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E128J2( )
      {
         /* Load Routine */
      }

      protected void E138J2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("parametrossistema.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A330ParametrosSistema_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E148J2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("parametrossistema.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A330ParametrosSistema_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E158J2( )
      {
         /* 'DoFechar' Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ParametrosSistema";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ParametrosSistema_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ParametrosSistema_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_8J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_8J2( true) ;
         }
         else
         {
            wb_table2_8_8J2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_8J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_76_8J2( true) ;
         }
         else
         {
            wb_table3_76_8J2( false) ;
         }
         return  ;
      }

      protected void wb_table3_76_8J2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_8J2e( true) ;
         }
         else
         {
            wb_table1_2_8J2e( false) ;
         }
      }

      protected void wb_table3_76_8J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_76_8J2e( true) ;
         }
         else
         {
            wb_table3_76_8J2e( false) ;
         }
      }

      protected void wb_table2_8_8J2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_nomesistema_Internalname, "Nome do Sistema", "", "", lblTextblockparametrossistema_nomesistema_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_NomeSistema_Internalname, StringUtil.RTrim( A331ParametrosSistema_NomeSistema), StringUtil.RTrim( context.localUtil.Format( A331ParametrosSistema_NomeSistema, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_NomeSistema_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_licensiadocadastrado_Internalname, "Licenciado cadastrado?", "", "", lblTextblockparametrossistema_licensiadocadastrado_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbParametrosSistema_LicensiadoCadastrado, cmbParametrosSistema_LicensiadoCadastrado_Internalname, StringUtil.BoolToStr( A399ParametrosSistema_LicensiadoCadastrado), 1, cmbParametrosSistema_LicensiadoCadastrado_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ParametrosSistemaGeneral.htm");
            cmbParametrosSistema_LicensiadoCadastrado.CurrentValue = StringUtil.BoolToStr( A399ParametrosSistema_LicensiadoCadastrado);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbParametrosSistema_LicensiadoCadastrado_Internalname, "Values", (String)(cmbParametrosSistema_LicensiadoCadastrado.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_padronizarstrings_Internalname, "Padronizar Strings", "", "", lblTextblockparametrossistema_padronizarstrings_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbParametrosSistema_PadronizarStrings, cmbParametrosSistema_PadronizarStrings_Internalname, StringUtil.BoolToStr( A417ParametrosSistema_PadronizarStrings), 1, cmbParametrosSistema_PadronizarStrings_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ParametrosSistemaGeneral.htm");
            cmbParametrosSistema_PadronizarStrings.CurrentValue = StringUtil.BoolToStr( A417ParametrosSistema_PadronizarStrings);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbParametrosSistema_PadronizarStrings_Internalname, "Values", (String)(cmbParametrosSistema_PadronizarStrings.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_fatorajuste_Internalname, "Fator de Ajuste padr�o", "", "", lblTextblockparametrossistema_fatorajuste_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_FatorAjuste_Internalname, StringUtil.LTrim( StringUtil.NToC( A708ParametrosSistema_FatorAjuste, 6, 2, ",", "")), context.localUtil.Format( A708ParametrosSistema_FatorAjuste, "ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_FatorAjuste_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_pathcrtf_Internalname, "Caminho certifica��o digital", "", "", lblTextblockparametrossistema_pathcrtf_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_PathCrtf_Internalname, A1021ParametrosSistema_PathCrtf, StringUtil.RTrim( context.localUtil.Format( A1021ParametrosSistema_PathCrtf, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_PathCrtf_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_emailsdahost_Internalname, "SMTP", "", "", lblTextblockparametrossistema_emailsdahost_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_EmailSdaHost_Internalname, A532ParametrosSistema_EmailSdaHost, StringUtil.RTrim( context.localUtil.Format( A532ParametrosSistema_EmailSdaHost, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_EmailSdaHost_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 300, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_emailsdaaut_Internalname, "Autentica��o", "", "", lblTextblockparametrossistema_emailsdaaut_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbParametrosSistema_EmailSdaAut, cmbParametrosSistema_EmailSdaAut_Internalname, StringUtil.BoolToStr( A535ParametrosSistema_EmailSdaAut), 1, cmbParametrosSistema_EmailSdaAut_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ParametrosSistemaGeneral.htm");
            cmbParametrosSistema_EmailSdaAut.CurrentValue = StringUtil.BoolToStr( A535ParametrosSistema_EmailSdaAut);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbParametrosSistema_EmailSdaAut_Internalname, "Values", (String)(cmbParametrosSistema_EmailSdaAut.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_emailsdauser_Internalname, "Usu�rio", "", "", lblTextblockparametrossistema_emailsdauser_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_EmailSdaUser_Internalname, A533ParametrosSistema_EmailSdaUser, StringUtil.RTrim( context.localUtil.Format( A533ParametrosSistema_EmailSdaUser, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_EmailSdaUser_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_emailsdaport_Internalname, "Porta", "", "", lblTextblockparametrossistema_emailsdaport_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_EmailSdaPort_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A536ParametrosSistema_EmailSdaPort), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A536ParametrosSistema_EmailSdaPort), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_EmailSdaPort_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 45, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_appid_Internalname, "Aplication ID", "", "", lblTextblockparametrossistema_appid_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_AppID_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A334ParametrosSistema_AppID), 12, 0, ",", "")), context.localUtil.Format( (decimal)(A334ParametrosSistema_AppID), "ZZZZZZZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_AppID_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "GAMKeyNumLong", "right", false, "HLP_ParametrosSistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_8J2e( true) ;
         }
         else
         {
            wb_table2_8_8J2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A330ParametrosSistema_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A330ParametrosSistema_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA8J2( ) ;
         WS8J2( ) ;
         WE8J2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA330ParametrosSistema_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA8J2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "parametrossistemageneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA8J2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A330ParametrosSistema_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A330ParametrosSistema_Codigo), 6, 0)));
         }
         wcpOA330ParametrosSistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA330ParametrosSistema_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A330ParametrosSistema_Codigo != wcpOA330ParametrosSistema_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA330ParametrosSistema_Codigo = A330ParametrosSistema_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA330ParametrosSistema_Codigo = cgiGet( sPrefix+"A330ParametrosSistema_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA330ParametrosSistema_Codigo) > 0 )
         {
            A330ParametrosSistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA330ParametrosSistema_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A330ParametrosSistema_Codigo), 6, 0)));
         }
         else
         {
            A330ParametrosSistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A330ParametrosSistema_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA8J2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS8J2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS8J2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A330ParametrosSistema_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A330ParametrosSistema_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA330ParametrosSistema_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A330ParametrosSistema_Codigo_CTRL", StringUtil.RTrim( sCtrlA330ParametrosSistema_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE8J2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311719597");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("parametrossistemageneral.js", "?2020311719598");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockparametrossistema_nomesistema_Internalname = sPrefix+"TEXTBLOCKPARAMETROSSISTEMA_NOMESISTEMA";
         edtParametrosSistema_NomeSistema_Internalname = sPrefix+"PARAMETROSSISTEMA_NOMESISTEMA";
         lblTextblockparametrossistema_licensiadocadastrado_Internalname = sPrefix+"TEXTBLOCKPARAMETROSSISTEMA_LICENSIADOCADASTRADO";
         cmbParametrosSistema_LicensiadoCadastrado_Internalname = sPrefix+"PARAMETROSSISTEMA_LICENSIADOCADASTRADO";
         lblTextblockparametrossistema_padronizarstrings_Internalname = sPrefix+"TEXTBLOCKPARAMETROSSISTEMA_PADRONIZARSTRINGS";
         cmbParametrosSistema_PadronizarStrings_Internalname = sPrefix+"PARAMETROSSISTEMA_PADRONIZARSTRINGS";
         lblTextblockparametrossistema_fatorajuste_Internalname = sPrefix+"TEXTBLOCKPARAMETROSSISTEMA_FATORAJUSTE";
         edtParametrosSistema_FatorAjuste_Internalname = sPrefix+"PARAMETROSSISTEMA_FATORAJUSTE";
         lblTextblockparametrossistema_pathcrtf_Internalname = sPrefix+"TEXTBLOCKPARAMETROSSISTEMA_PATHCRTF";
         edtParametrosSistema_PathCrtf_Internalname = sPrefix+"PARAMETROSSISTEMA_PATHCRTF";
         lblTextblockparametrossistema_emailsdahost_Internalname = sPrefix+"TEXTBLOCKPARAMETROSSISTEMA_EMAILSDAHOST";
         edtParametrosSistema_EmailSdaHost_Internalname = sPrefix+"PARAMETROSSISTEMA_EMAILSDAHOST";
         lblTextblockparametrossistema_emailsdaaut_Internalname = sPrefix+"TEXTBLOCKPARAMETROSSISTEMA_EMAILSDAAUT";
         cmbParametrosSistema_EmailSdaAut_Internalname = sPrefix+"PARAMETROSSISTEMA_EMAILSDAAUT";
         lblTextblockparametrossistema_emailsdauser_Internalname = sPrefix+"TEXTBLOCKPARAMETROSSISTEMA_EMAILSDAUSER";
         edtParametrosSistema_EmailSdaUser_Internalname = sPrefix+"PARAMETROSSISTEMA_EMAILSDAUSER";
         lblTextblockparametrossistema_emailsdaport_Internalname = sPrefix+"TEXTBLOCKPARAMETROSSISTEMA_EMAILSDAPORT";
         edtParametrosSistema_EmailSdaPort_Internalname = sPrefix+"PARAMETROSSISTEMA_EMAILSDAPORT";
         lblTextblockparametrossistema_appid_Internalname = sPrefix+"TEXTBLOCKPARAMETROSSISTEMA_APPID";
         edtParametrosSistema_AppID_Internalname = sPrefix+"PARAMETROSSISTEMA_APPID";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtParametrosSistema_AppID_Jsonclick = "";
         edtParametrosSistema_EmailSdaPort_Jsonclick = "";
         edtParametrosSistema_EmailSdaUser_Jsonclick = "";
         cmbParametrosSistema_EmailSdaAut_Jsonclick = "";
         edtParametrosSistema_EmailSdaHost_Jsonclick = "";
         edtParametrosSistema_PathCrtf_Jsonclick = "";
         edtParametrosSistema_FatorAjuste_Jsonclick = "";
         cmbParametrosSistema_PadronizarStrings_Jsonclick = "";
         cmbParametrosSistema_LicensiadoCadastrado_Jsonclick = "";
         edtParametrosSistema_NomeSistema_Jsonclick = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E138J2',iparms:[{av:'A330ParametrosSistema_Codigo',fld:'PARAMETROSSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E148J2',iparms:[{av:'A330ParametrosSistema_Codigo',fld:'PARAMETROSSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E158J2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A331ParametrosSistema_NomeSistema = "";
         A1021ParametrosSistema_PathCrtf = "";
         A532ParametrosSistema_EmailSdaHost = "";
         A533ParametrosSistema_EmailSdaUser = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H008J2_A330ParametrosSistema_Codigo = new int[1] ;
         H008J2_A334ParametrosSistema_AppID = new long[1] ;
         H008J2_A536ParametrosSistema_EmailSdaPort = new short[1] ;
         H008J2_n536ParametrosSistema_EmailSdaPort = new bool[] {false} ;
         H008J2_A533ParametrosSistema_EmailSdaUser = new String[] {""} ;
         H008J2_n533ParametrosSistema_EmailSdaUser = new bool[] {false} ;
         H008J2_A535ParametrosSistema_EmailSdaAut = new bool[] {false} ;
         H008J2_n535ParametrosSistema_EmailSdaAut = new bool[] {false} ;
         H008J2_A532ParametrosSistema_EmailSdaHost = new String[] {""} ;
         H008J2_n532ParametrosSistema_EmailSdaHost = new bool[] {false} ;
         H008J2_A1021ParametrosSistema_PathCrtf = new String[] {""} ;
         H008J2_n1021ParametrosSistema_PathCrtf = new bool[] {false} ;
         H008J2_A708ParametrosSistema_FatorAjuste = new decimal[1] ;
         H008J2_n708ParametrosSistema_FatorAjuste = new bool[] {false} ;
         H008J2_A417ParametrosSistema_PadronizarStrings = new bool[] {false} ;
         H008J2_A399ParametrosSistema_LicensiadoCadastrado = new bool[] {false} ;
         H008J2_n399ParametrosSistema_LicensiadoCadastrado = new bool[] {false} ;
         H008J2_A331ParametrosSistema_NomeSistema = new String[] {""} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockparametrossistema_nomesistema_Jsonclick = "";
         lblTextblockparametrossistema_licensiadocadastrado_Jsonclick = "";
         lblTextblockparametrossistema_padronizarstrings_Jsonclick = "";
         lblTextblockparametrossistema_fatorajuste_Jsonclick = "";
         lblTextblockparametrossistema_pathcrtf_Jsonclick = "";
         lblTextblockparametrossistema_emailsdahost_Jsonclick = "";
         lblTextblockparametrossistema_emailsdaaut_Jsonclick = "";
         lblTextblockparametrossistema_emailsdauser_Jsonclick = "";
         lblTextblockparametrossistema_emailsdaport_Jsonclick = "";
         lblTextblockparametrossistema_appid_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA330ParametrosSistema_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.parametrossistemageneral__default(),
            new Object[][] {
                new Object[] {
               H008J2_A330ParametrosSistema_Codigo, H008J2_A334ParametrosSistema_AppID, H008J2_A536ParametrosSistema_EmailSdaPort, H008J2_n536ParametrosSistema_EmailSdaPort, H008J2_A533ParametrosSistema_EmailSdaUser, H008J2_n533ParametrosSistema_EmailSdaUser, H008J2_A535ParametrosSistema_EmailSdaAut, H008J2_n535ParametrosSistema_EmailSdaAut, H008J2_A532ParametrosSistema_EmailSdaHost, H008J2_n532ParametrosSistema_EmailSdaHost,
               H008J2_A1021ParametrosSistema_PathCrtf, H008J2_n1021ParametrosSistema_PathCrtf, H008J2_A708ParametrosSistema_FatorAjuste, H008J2_n708ParametrosSistema_FatorAjuste, H008J2_A417ParametrosSistema_PadronizarStrings, H008J2_A399ParametrosSistema_LicensiadoCadastrado, H008J2_n399ParametrosSistema_LicensiadoCadastrado, H008J2_A331ParametrosSistema_NomeSistema
               }
            }
         );
         AV14Pgmname = "ParametrosSistemaGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "ParametrosSistemaGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A536ParametrosSistema_EmailSdaPort ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A330ParametrosSistema_Codigo ;
      private int wcpOA330ParametrosSistema_Codigo ;
      private int AV7ParametrosSistema_Codigo ;
      private int idxLst ;
      private long A334ParametrosSistema_AppID ;
      private decimal A708ParametrosSistema_FatorAjuste ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A331ParametrosSistema_NomeSistema ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String edtParametrosSistema_NomeSistema_Internalname ;
      private String cmbParametrosSistema_LicensiadoCadastrado_Internalname ;
      private String cmbParametrosSistema_PadronizarStrings_Internalname ;
      private String edtParametrosSistema_FatorAjuste_Internalname ;
      private String edtParametrosSistema_PathCrtf_Internalname ;
      private String edtParametrosSistema_EmailSdaHost_Internalname ;
      private String cmbParametrosSistema_EmailSdaAut_Internalname ;
      private String edtParametrosSistema_EmailSdaUser_Internalname ;
      private String edtParametrosSistema_EmailSdaPort_Internalname ;
      private String edtParametrosSistema_AppID_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockparametrossistema_nomesistema_Internalname ;
      private String lblTextblockparametrossistema_nomesistema_Jsonclick ;
      private String edtParametrosSistema_NomeSistema_Jsonclick ;
      private String lblTextblockparametrossistema_licensiadocadastrado_Internalname ;
      private String lblTextblockparametrossistema_licensiadocadastrado_Jsonclick ;
      private String cmbParametrosSistema_LicensiadoCadastrado_Jsonclick ;
      private String lblTextblockparametrossistema_padronizarstrings_Internalname ;
      private String lblTextblockparametrossistema_padronizarstrings_Jsonclick ;
      private String cmbParametrosSistema_PadronizarStrings_Jsonclick ;
      private String lblTextblockparametrossistema_fatorajuste_Internalname ;
      private String lblTextblockparametrossistema_fatorajuste_Jsonclick ;
      private String edtParametrosSistema_FatorAjuste_Jsonclick ;
      private String lblTextblockparametrossistema_pathcrtf_Internalname ;
      private String lblTextblockparametrossistema_pathcrtf_Jsonclick ;
      private String edtParametrosSistema_PathCrtf_Jsonclick ;
      private String lblTextblockparametrossistema_emailsdahost_Internalname ;
      private String lblTextblockparametrossistema_emailsdahost_Jsonclick ;
      private String edtParametrosSistema_EmailSdaHost_Jsonclick ;
      private String lblTextblockparametrossistema_emailsdaaut_Internalname ;
      private String lblTextblockparametrossistema_emailsdaaut_Jsonclick ;
      private String cmbParametrosSistema_EmailSdaAut_Jsonclick ;
      private String lblTextblockparametrossistema_emailsdauser_Internalname ;
      private String lblTextblockparametrossistema_emailsdauser_Jsonclick ;
      private String edtParametrosSistema_EmailSdaUser_Jsonclick ;
      private String lblTextblockparametrossistema_emailsdaport_Internalname ;
      private String lblTextblockparametrossistema_emailsdaport_Jsonclick ;
      private String edtParametrosSistema_EmailSdaPort_Jsonclick ;
      private String lblTextblockparametrossistema_appid_Internalname ;
      private String lblTextblockparametrossistema_appid_Jsonclick ;
      private String edtParametrosSistema_AppID_Jsonclick ;
      private String sCtrlA330ParametrosSistema_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A399ParametrosSistema_LicensiadoCadastrado ;
      private bool A417ParametrosSistema_PadronizarStrings ;
      private bool A535ParametrosSistema_EmailSdaAut ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n399ParametrosSistema_LicensiadoCadastrado ;
      private bool n535ParametrosSistema_EmailSdaAut ;
      private bool n536ParametrosSistema_EmailSdaPort ;
      private bool n533ParametrosSistema_EmailSdaUser ;
      private bool n532ParametrosSistema_EmailSdaHost ;
      private bool n1021ParametrosSistema_PathCrtf ;
      private bool n708ParametrosSistema_FatorAjuste ;
      private bool returnInSub ;
      private String A1021ParametrosSistema_PathCrtf ;
      private String A532ParametrosSistema_EmailSdaHost ;
      private String A533ParametrosSistema_EmailSdaUser ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbParametrosSistema_LicensiadoCadastrado ;
      private GXCombobox cmbParametrosSistema_PadronizarStrings ;
      private GXCombobox cmbParametrosSistema_EmailSdaAut ;
      private IDataStoreProvider pr_default ;
      private int[] H008J2_A330ParametrosSistema_Codigo ;
      private long[] H008J2_A334ParametrosSistema_AppID ;
      private short[] H008J2_A536ParametrosSistema_EmailSdaPort ;
      private bool[] H008J2_n536ParametrosSistema_EmailSdaPort ;
      private String[] H008J2_A533ParametrosSistema_EmailSdaUser ;
      private bool[] H008J2_n533ParametrosSistema_EmailSdaUser ;
      private bool[] H008J2_A535ParametrosSistema_EmailSdaAut ;
      private bool[] H008J2_n535ParametrosSistema_EmailSdaAut ;
      private String[] H008J2_A532ParametrosSistema_EmailSdaHost ;
      private bool[] H008J2_n532ParametrosSistema_EmailSdaHost ;
      private String[] H008J2_A1021ParametrosSistema_PathCrtf ;
      private bool[] H008J2_n1021ParametrosSistema_PathCrtf ;
      private decimal[] H008J2_A708ParametrosSistema_FatorAjuste ;
      private bool[] H008J2_n708ParametrosSistema_FatorAjuste ;
      private bool[] H008J2_A417ParametrosSistema_PadronizarStrings ;
      private bool[] H008J2_A399ParametrosSistema_LicensiadoCadastrado ;
      private bool[] H008J2_n399ParametrosSistema_LicensiadoCadastrado ;
      private String[] H008J2_A331ParametrosSistema_NomeSistema ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class parametrossistemageneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH008J2 ;
          prmH008J2 = new Object[] {
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H008J2", "SELECT [ParametrosSistema_Codigo], [ParametrosSistema_AppID], [ParametrosSistema_EmailSdaPort], [ParametrosSistema_EmailSdaUser], [ParametrosSistema_EmailSdaAut], [ParametrosSistema_EmailSdaHost], [ParametrosSistema_PathCrtf], [ParametrosSistema_FatorAjuste], [ParametrosSistema_PadronizarStrings], [ParametrosSistema_LicensiadoCadastrado], [ParametrosSistema_NomeSistema] FROM [ParametrosSistema] WITH (NOLOCK) WHERE [ParametrosSistema_Codigo] = @ParametrosSistema_Codigo ORDER BY [ParametrosSistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008J2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((long[]) buf[1])[0] = rslt.getLong(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((bool[]) buf[14])[0] = rslt.getBool(9) ;
                ((bool[]) buf[15])[0] = rslt.getBool(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
