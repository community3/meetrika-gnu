/*
               File: WP_UpdOSSistema
        Description: Alterar Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:49:5.68
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_updossistema : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_updossistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_updossistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbContagemResultado_StatusDmn = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_25 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_25_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_25_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               AV5OldSistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5OldSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5OldSistema_Codigo), 6, 0)));
               AV7Qtde = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Qtde), 4, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( AV5OldSistema_Codigo, AV7Qtde) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "wp_updossistema_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAE12( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTE12( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020312049570");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_updossistema.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vOLDSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5OldSistema_Codigo), 6, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_25", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_25), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A489ContagemResultado_SistemaCod), 6, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEE12( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTE12( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_updossistema.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_UpdOSSistema" ;
      }

      public override String GetPgmdesc( )
      {
         return "Alterar Sistema" ;
      }

      protected void WBE10( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_E12( true) ;
         }
         else
         {
            wb_table1_2_E12( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_E12e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table2_19_E12( true) ;
         }
         else
         {
            wb_table2_19_E12( false) ;
         }
         return  ;
      }

      protected void wb_table2_19_E12e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'" + sGXsfl_25_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAtual_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Atual), 4, 0, ",", "")), ((edtavAtual_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9Atual), "ZZZ9")) : context.localUtil.Format( (decimal)(AV9Atual), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAtual_Jsonclick, 0, "Attribute", "", "", "", edtavAtual_Visible, edtavAtual_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_UpdOSSistema.htm");
         }
         wbLoad = true;
      }

      protected void STARTE12( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Alterar Sistema", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPE10( ) ;
      }

      protected void WSE12( )
      {
         STARTE12( ) ;
         EVTE12( ) ;
      }

      protected void EVTE12( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VNEWSISTEMA_CODIGO.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11E12 */
                              E11E12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 /* Set Refresh If Oldsistema_codigo Changed */
                                 if ( ( context.localUtil.CToN( cgiGet( "GXH_vOLDSISTEMA_CODIGO"), ",", ".") != Convert.ToDecimal( AV5OldSistema_Codigo )) )
                                 {
                                    Rfr0gs = true;
                                 }
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12E12 */
                                    E12E12 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_25_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_25_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_25_idx), 4, 0)), 4, "0");
                              SubsflControlProps_252( ) ;
                              AV7Qtde = (short)(context.localUtil.CToN( cgiGet( edtavQtde_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Qtde), 4, 0)));
                              A457ContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtContagemResultado_Demanda_Internalname));
                              n457ContagemResultado_Demanda = false;
                              A509ContagemrResultado_SistemaSigla = StringUtil.Upper( cgiGet( edtContagemrResultado_SistemaSigla_Internalname));
                              n509ContagemrResultado_SistemaSigla = false;
                              cmbContagemResultado_StatusDmn.Name = cmbContagemResultado_StatusDmn_Internalname;
                              cmbContagemResultado_StatusDmn.CurrentValue = cgiGet( cmbContagemResultado_StatusDmn_Internalname);
                              A484ContagemResultado_StatusDmn = cgiGet( cmbContagemResultado_StatusDmn_Internalname);
                              n484ContagemResultado_StatusDmn = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13E12 */
                                    E13E12 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14E12 */
                                    E14E12 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15E12 */
                                    E15E12 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEE12( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAE12( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            GXCCtl = "CONTAGEMRESULTADO_STATUSDMN_" + sGXsfl_25_idx;
            cmbContagemResultado_StatusDmn.Name = GXCCtl;
            cmbContagemResultado_StatusDmn.WebTags = "";
            cmbContagemResultado_StatusDmn.addItem("B", "Stand by", 0);
            cmbContagemResultado_StatusDmn.addItem("S", "Solicitada", 0);
            cmbContagemResultado_StatusDmn.addItem("E", "Em An�lise", 0);
            cmbContagemResultado_StatusDmn.addItem("A", "Em execu��o", 0);
            cmbContagemResultado_StatusDmn.addItem("R", "Resolvida", 0);
            cmbContagemResultado_StatusDmn.addItem("C", "Conferida", 0);
            cmbContagemResultado_StatusDmn.addItem("D", "Rejeitada", 0);
            cmbContagemResultado_StatusDmn.addItem("H", "Homologada", 0);
            cmbContagemResultado_StatusDmn.addItem("O", "Aceite", 0);
            cmbContagemResultado_StatusDmn.addItem("P", "A Pagar", 0);
            cmbContagemResultado_StatusDmn.addItem("L", "Liquidada", 0);
            cmbContagemResultado_StatusDmn.addItem("X", "Cancelada", 0);
            cmbContagemResultado_StatusDmn.addItem("N", "N�o Faturada", 0);
            cmbContagemResultado_StatusDmn.addItem("J", "Planejamento", 0);
            cmbContagemResultado_StatusDmn.addItem("I", "An�lise Planejamento", 0);
            cmbContagemResultado_StatusDmn.addItem("T", "Validacao T�cnica", 0);
            cmbContagemResultado_StatusDmn.addItem("Q", "Validacao Qualidade", 0);
            cmbContagemResultado_StatusDmn.addItem("G", "Em Homologa��o", 0);
            cmbContagemResultado_StatusDmn.addItem("M", "Valida��o Mensura��o", 0);
            cmbContagemResultado_StatusDmn.addItem("U", "Rascunho", 0);
            if ( cmbContagemResultado_StatusDmn.ItemCount > 0 )
            {
               A484ContagemResultado_StatusDmn = cmbContagemResultado_StatusDmn.getValidValue(A484ContagemResultado_StatusDmn);
               n484ContagemResultado_StatusDmn = false;
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOldsistema_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_252( ) ;
         while ( nGXsfl_25_idx <= nRC_GXsfl_25 )
         {
            sendrow_252( ) ;
            nGXsfl_25_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_25_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_25_idx+1));
            sGXsfl_25_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_25_idx), 4, 0)), 4, "0");
            SubsflControlProps_252( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int AV5OldSistema_Codigo ,
                                       short AV7Qtde )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFE12( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DEMANDA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_STATUSDMN", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A484ContagemResultado_StatusDmn, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFE12( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavAtual_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAtual_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtual_Enabled), 5, 0)));
      }

      protected void RFE12( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 25;
         /* Execute user event: E14E12 */
         E14E12 ();
         nGXsfl_25_idx = 1;
         sGXsfl_25_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_25_idx), 4, 0)), 4, "0");
         SubsflControlProps_252( ) ;
         nGXsfl_25_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_252( ) ;
            /* Using cursor H00E12 */
            pr_default.execute(0, new Object[] {AV5OldSistema_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A489ContagemResultado_SistemaCod = H00E12_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00E12_n489ContagemResultado_SistemaCod[0];
               A484ContagemResultado_StatusDmn = H00E12_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00E12_n484ContagemResultado_StatusDmn[0];
               A509ContagemrResultado_SistemaSigla = H00E12_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = H00E12_n509ContagemrResultado_SistemaSigla[0];
               A457ContagemResultado_Demanda = H00E12_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00E12_n457ContagemResultado_Demanda[0];
               A509ContagemrResultado_SistemaSigla = H00E12_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = H00E12_n509ContagemrResultado_SistemaSigla[0];
               /* Execute user event: E15E12 */
               E15E12 ();
               pr_default.readNext(0);
            }
            pr_default.close(0);
            wbEnd = 25;
            WBE10( ) ;
         }
         nGXsfl_25_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPE10( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavAtual_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAtual_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtual_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13E12 */
         E13E12 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOldsistema_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOldsistema_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vOLDSISTEMA_CODIGO");
               GX_FocusControl = edtavOldsistema_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV5OldSistema_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5OldSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5OldSistema_Codigo), 6, 0)));
            }
            else
            {
               AV5OldSistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavOldsistema_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5OldSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5OldSistema_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavNewsistema_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavNewsistema_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vNEWSISTEMA_CODIGO");
               GX_FocusControl = edtavNewsistema_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV6NewSistema_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6NewSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6NewSistema_Codigo), 6, 0)));
            }
            else
            {
               AV6NewSistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavNewsistema_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6NewSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6NewSistema_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAtual_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAtual_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vATUAL");
               GX_FocusControl = edtavAtual_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9Atual = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Atual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Atual), 4, 0)));
            }
            else
            {
               AV9Atual = (short)(context.localUtil.CToN( cgiGet( edtavAtual_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Atual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Atual), 4, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_25 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_25"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13E12 */
         E13E12 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13E12( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         edtavAtual_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAtual_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAtual_Visible), 5, 0)));
      }

      protected void E14E12( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV7Qtde = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Qtde), 4, 0)));
      }

      private void E15E12( )
      {
         /* Grid_Load Routine */
         AV7Qtde = (short)(AV7Qtde+1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Qtde), 4, 0)));
         bttBtnenter_Caption = "Alterar "+StringUtil.Trim( StringUtil.Str( (decimal)(AV7Qtde), 4, 0))+" OS n�o faturadas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 25;
         }
         sendrow_252( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_25_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(25, GridRow);
         }
      }

      protected void E11E12( )
      {
         /* Newsistema_codigo_Isvalid Routine */
         /* Execute user subroutine: 'QTDATUAL' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E12E12 */
         E12E12 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E12E12( )
      {
         /* Enter Routine */
         if ( (0==AV5OldSistema_Codigo) )
         {
            GX_msglist.addItem("Preencha o Id do Sistema que deseja alterar!");
            GX_FocusControl = edtavOldsistema_codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( (0==AV6NewSistema_Codigo) )
         {
            GX_msglist.addItem("Preencha o novo Id do Sistema que deseja alterar!");
            GX_FocusControl = edtavNewsistema_codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( AV7Qtde > 0 )
         {
            new prc_updossistema(context ).execute( ref  AV5OldSistema_Codigo, ref  AV6NewSistema_Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5OldSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5OldSistema_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6NewSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6NewSistema_Codigo), 6, 0)));
            AV6NewSistema_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6NewSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6NewSistema_Codigo), 6, 0)));
            AV9Atual = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Atual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Atual), 4, 0)));
            lblLblatualmente_Caption = "atualmente com ";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblLblatualmente_Internalname, "Caption", lblLblatualmente_Caption);
            bttBtnenter_Caption = "Alterar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Caption", bttBtnenter_Caption);
            gxgrGrid_refresh( AV5OldSistema_Codigo, AV7Qtde) ;
         }
         else
         {
            GX_msglist.addItem("N�o existem OS para alterar!");
         }
      }

      protected void S112( )
      {
         /* 'QTDATUAL' Routine */
         AV9Atual = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Atual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Atual), 4, 0)));
         /* Optimized group. */
         /* Using cursor H00E13 */
         pr_default.execute(1, new Object[] {AV6NewSistema_Codigo});
         cV9Atual = H00E13_AV9Atual[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "cV9Atual", StringUtil.LTrim( StringUtil.Str( (decimal)(cV9Atual), 4, 0)));
         pr_default.close(1);
         AV9Atual = (short)(AV9Atual+cV9Atual*1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Atual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Atual), 4, 0)));
         /* End optimized group. */
         lblLblatualmente_Caption = "atualmente com "+StringUtil.Trim( StringUtil.Str( (decimal)(AV9Atual), 4, 0))+" demandas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblLblatualmente_Internalname, "Caption", lblLblatualmente_Caption);
      }

      protected void wb_table2_19_E12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:50px")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(25), 2, 0)+","+"null"+");", bttBtnenter_Caption, bttBtnenter_Jsonclick, 5, "Alterar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_UpdOSSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"25\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWith", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(50), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "N� Refer�ncia") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(185), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(174), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Status") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Qtde), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A457ContagemResultado_Demanda);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A509ContagemrResultado_SistemaSigla));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 25 )
         {
            wbEnd = 0;
            nRC_GXsfl_25 = (short)(nGXsfl_25_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_19_E12e( true) ;
         }
         else
         {
            wb_table2_19_E12e( false) ;
         }
      }

      protected void wb_table1_2_E12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "&nbsp; ") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Alterar OS", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Microsoft Sans Serif'; font-size:16.0pt; font-weight:normal; font-style:normal;", "Title", 0, "", 1, 1, 0, "HLP_WP_UpdOSSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:90px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Do Sistema ID:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_UpdOSSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'" + sGXsfl_25_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOldsistema_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5OldSistema_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV5OldSistema_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,12);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOldsistema_codigo_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_UpdOSSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:90px")+"\">") ;
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Para o Sistema ID:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_UpdOSSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'" + sGXsfl_25_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNewsistema_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6NewSistema_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV6NewSistema_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,15);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNewsistema_codigo_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_UpdOSSistema.htm");
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblatualmente_Internalname, lblLblatualmente_Caption, "", "", lblLblatualmente_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_UpdOSSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_E12e( true) ;
         }
         else
         {
            wb_table1_2_E12e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAE12( ) ;
         WSE12( ) ;
         WEE12( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020312049594");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_updossistema.js", "?2020312049594");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_252( )
      {
         edtavQtde_Internalname = "vQTDE_"+sGXsfl_25_idx;
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_25_idx;
         edtContagemrResultado_SistemaSigla_Internalname = "CONTAGEMRRESULTADO_SISTEMASIGLA_"+sGXsfl_25_idx;
         cmbContagemResultado_StatusDmn_Internalname = "CONTAGEMRESULTADO_STATUSDMN_"+sGXsfl_25_idx;
      }

      protected void SubsflControlProps_fel_252( )
      {
         edtavQtde_Internalname = "vQTDE_"+sGXsfl_25_fel_idx;
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_25_fel_idx;
         edtContagemrResultado_SistemaSigla_Internalname = "CONTAGEMRRESULTADO_SISTEMASIGLA_"+sGXsfl_25_fel_idx;
         cmbContagemResultado_StatusDmn_Internalname = "CONTAGEMRESULTADO_STATUSDMN_"+sGXsfl_25_fel_idx;
      }

      protected void sendrow_252( )
      {
         SubsflControlProps_252( ) ;
         WBE10( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_25_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_25_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtde_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Qtde), 5, 0, ",", "")),context.localUtil.Format( (decimal)(AV7Qtde), "ZZZ9)"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavQtde_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)50,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)25,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Demanda_Internalname,(String)A457ContagemResultado_Demanda,StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Demanda_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)25,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemrResultado_SistemaSigla_Internalname,StringUtil.RTrim( A509ContagemrResultado_SistemaSigla),StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemrResultado_SistemaSigla_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)185,(String)"px",(short)17,(String)"px",(short)25,(short)0,(short)0,(short)25,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         if ( ( nGXsfl_25_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "CONTAGEMRESULTADO_STATUSDMN_" + sGXsfl_25_idx;
            cmbContagemResultado_StatusDmn.Name = GXCCtl;
            cmbContagemResultado_StatusDmn.WebTags = "";
            cmbContagemResultado_StatusDmn.addItem("B", "Stand by", 0);
            cmbContagemResultado_StatusDmn.addItem("S", "Solicitada", 0);
            cmbContagemResultado_StatusDmn.addItem("E", "Em An�lise", 0);
            cmbContagemResultado_StatusDmn.addItem("A", "Em execu��o", 0);
            cmbContagemResultado_StatusDmn.addItem("R", "Resolvida", 0);
            cmbContagemResultado_StatusDmn.addItem("C", "Conferida", 0);
            cmbContagemResultado_StatusDmn.addItem("D", "Rejeitada", 0);
            cmbContagemResultado_StatusDmn.addItem("H", "Homologada", 0);
            cmbContagemResultado_StatusDmn.addItem("O", "Aceite", 0);
            cmbContagemResultado_StatusDmn.addItem("P", "A Pagar", 0);
            cmbContagemResultado_StatusDmn.addItem("L", "Liquidada", 0);
            cmbContagemResultado_StatusDmn.addItem("X", "Cancelada", 0);
            cmbContagemResultado_StatusDmn.addItem("N", "N�o Faturada", 0);
            cmbContagemResultado_StatusDmn.addItem("J", "Planejamento", 0);
            cmbContagemResultado_StatusDmn.addItem("I", "An�lise Planejamento", 0);
            cmbContagemResultado_StatusDmn.addItem("T", "Validacao T�cnica", 0);
            cmbContagemResultado_StatusDmn.addItem("Q", "Validacao Qualidade", 0);
            cmbContagemResultado_StatusDmn.addItem("G", "Em Homologa��o", 0);
            cmbContagemResultado_StatusDmn.addItem("M", "Valida��o Mensura��o", 0);
            cmbContagemResultado_StatusDmn.addItem("U", "Rascunho", 0);
            if ( cmbContagemResultado_StatusDmn.ItemCount > 0 )
            {
               A484ContagemResultado_StatusDmn = cmbContagemResultado_StatusDmn.getValidValue(A484ContagemResultado_StatusDmn);
               n484ContagemResultado_StatusDmn = false;
            }
         }
         /* ComboBox */
         GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemResultado_StatusDmn,(String)cmbContagemResultado_StatusDmn_Internalname,StringUtil.RTrim( A484ContagemResultado_StatusDmn),(short)1,(String)cmbContagemResultado_StatusDmn_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)174,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",(String)"",(bool)true});
         cmbContagemResultado_StatusDmn.CurrentValue = StringUtil.RTrim( A484ContagemResultado_StatusDmn);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultado_StatusDmn_Internalname, "Values", (String)(cmbContagemResultado_StatusDmn.ToJavascriptSource()));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DEMANDA"+"_"+sGXsfl_25_idx, GetSecureSignedToken( sGXsfl_25_idx, StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_STATUSDMN"+"_"+sGXsfl_25_idx, GetSecureSignedToken( sGXsfl_25_idx, StringUtil.RTrim( context.localUtil.Format( A484ContagemResultado_StatusDmn, ""))));
         GridContainer.AddRow(GridRow);
         nGXsfl_25_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_25_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_25_idx+1));
         sGXsfl_25_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_25_idx), 4, 0)), 4, "0");
         SubsflControlProps_252( ) ;
         /* End function sendrow_252 */
      }

      protected void init_default_properties( )
      {
         lblTextblock3_Internalname = "TEXTBLOCK3";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         edtavOldsistema_codigo_Internalname = "vOLDSISTEMA_CODIGO";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavNewsistema_codigo_Internalname = "vNEWSISTEMA_CODIGO";
         lblLblatualmente_Internalname = "LBLATUALMENTE";
         tblTable1_Internalname = "TABLE1";
         bttBtnenter_Internalname = "BTNENTER";
         edtavQtde_Internalname = "vQTDE";
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA";
         edtContagemrResultado_SistemaSigla_Internalname = "CONTAGEMRRESULTADO_SISTEMASIGLA";
         cmbContagemResultado_StatusDmn_Internalname = "CONTAGEMRESULTADO_STATUSDMN";
         tblTable2_Internalname = "TABLE2";
         edtavAtual_Internalname = "vATUAL";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbContagemResultado_StatusDmn_Jsonclick = "";
         edtContagemrResultado_SistemaSigla_Jsonclick = "";
         edtContagemResultado_Demanda_Jsonclick = "";
         edtavQtde_Jsonclick = "";
         edtavNewsistema_codigo_Jsonclick = "";
         edtavOldsistema_codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         subGrid_Class = "WorkWith";
         lblLblatualmente_Caption = "atualmente com";
         bttBtnenter_Caption = "Alterar";
         subGrid_Backcolorstyle = 3;
         edtavAtual_Jsonclick = "";
         edtavAtual_Enabled = 1;
         edtavAtual_Visible = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Alterar Sistema";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV5OldSistema_Codigo',fld:'vOLDSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Qtde',fld:'vQTDE',pic:'ZZZ9)',nv:0}],oparms:[{av:'AV7Qtde',fld:'vQTDE',pic:'ZZZ9)',nv:0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E15E12',iparms:[{av:'AV7Qtde',fld:'vQTDE',pic:'ZZZ9)',nv:0}],oparms:[{av:'AV7Qtde',fld:'vQTDE',pic:'ZZZ9)',nv:0},{ctrl:'BTNENTER',prop:'Caption'}]}");
         setEventMetadata("VNEWSISTEMA_CODIGO.ISVALID","{handler:'E11E12',iparms:[{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV6NewSistema_Codigo',fld:'vNEWSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV9Atual',fld:'vATUAL',pic:'ZZZ9',nv:0},{av:'lblLblatualmente_Caption',ctrl:'LBLATUALMENTE',prop:'Caption'}]}");
         setEventMetadata("ENTER","{handler:'E12E12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV5OldSistema_Codigo',fld:'vOLDSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Qtde',fld:'vQTDE',pic:'ZZZ9)',nv:0},{av:'AV6NewSistema_Codigo',fld:'vNEWSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV6NewSistema_Codigo',fld:'vNEWSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5OldSistema_Codigo',fld:'vOLDSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9Atual',fld:'vATUAL',pic:'ZZZ9',nv:0},{av:'lblLblatualmente_Caption',ctrl:'LBLATUALMENTE',prop:'Caption'},{ctrl:'BTNENTER',prop:'Caption'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A457ContagemResultado_Demanda = "";
         A509ContagemrResultado_SistemaSigla = "";
         A484ContagemResultado_StatusDmn = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00E12_A456ContagemResultado_Codigo = new int[1] ;
         H00E12_A489ContagemResultado_SistemaCod = new int[1] ;
         H00E12_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00E12_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00E12_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00E12_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00E12_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00E12_A457ContagemResultado_Demanda = new String[] {""} ;
         H00E12_n457ContagemResultado_Demanda = new bool[] {false} ;
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         H00E13_AV9Atual = new short[1] ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtnenter_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblTextblock3_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         lblLblatualmente_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_updossistema__default(),
            new Object[][] {
                new Object[] {
               H00E12_A456ContagemResultado_Codigo, H00E12_A489ContagemResultado_SistemaCod, H00E12_n489ContagemResultado_SistemaCod, H00E12_A484ContagemResultado_StatusDmn, H00E12_n484ContagemResultado_StatusDmn, H00E12_A509ContagemrResultado_SistemaSigla, H00E12_n509ContagemrResultado_SistemaSigla, H00E12_A457ContagemResultado_Demanda, H00E12_n457ContagemResultado_Demanda
               }
               , new Object[] {
               H00E13_AV9Atual
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavAtual_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_25 ;
      private short nGXsfl_25_idx=1 ;
      private short AV7Qtde ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short AV9Atual ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_25_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short GRID_nEOF ;
      private short cV9Atual ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV5OldSistema_Codigo ;
      private int A489ContagemResultado_SistemaCod ;
      private int edtavAtual_Enabled ;
      private int edtavAtual_Visible ;
      private int subGrid_Islastpage ;
      private int AV6NewSistema_Codigo ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_25_idx="0001" ;
      private String edtavQtde_Internalname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavAtual_Internalname ;
      private String edtavAtual_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtContagemResultado_Demanda_Internalname ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String edtContagemrResultado_SistemaSigla_Internalname ;
      private String cmbContagemResultado_StatusDmn_Internalname ;
      private String A484ContagemResultado_StatusDmn ;
      private String GXCCtl ;
      private String edtavOldsistema_codigo_Internalname ;
      private String scmdbuf ;
      private String edtavNewsistema_codigo_Internalname ;
      private String bttBtnenter_Caption ;
      private String bttBtnenter_Internalname ;
      private String lblLblatualmente_Caption ;
      private String lblLblatualmente_Internalname ;
      private String sStyleString ;
      private String tblTable2_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnenter_Jsonclick ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTable1_Internalname ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String edtavOldsistema_codigo_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String edtavNewsistema_codigo_Jsonclick ;
      private String lblLblatualmente_Jsonclick ;
      private String sGXsfl_25_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavQtde_Jsonclick ;
      private String edtContagemResultado_Demanda_Jsonclick ;
      private String edtContagemrResultado_SistemaSigla_Jsonclick ;
      private String cmbContagemResultado_StatusDmn_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n457ContagemResultado_Demanda ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String A457ContagemResultado_Demanda ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContagemResultado_StatusDmn ;
      private IDataStoreProvider pr_default ;
      private int[] H00E12_A456ContagemResultado_Codigo ;
      private int[] H00E12_A489ContagemResultado_SistemaCod ;
      private bool[] H00E12_n489ContagemResultado_SistemaCod ;
      private String[] H00E12_A484ContagemResultado_StatusDmn ;
      private bool[] H00E12_n484ContagemResultado_StatusDmn ;
      private String[] H00E12_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00E12_n509ContagemrResultado_SistemaSigla ;
      private String[] H00E12_A457ContagemResultado_Demanda ;
      private bool[] H00E12_n457ContagemResultado_Demanda ;
      private short[] H00E13_AV9Atual ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class wp_updossistema__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00E12 ;
          prmH00E12 = new Object[] {
          new Object[] {"@AV5OldSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00E13 ;
          prmH00E13 = new Object[] {
          new Object[] {"@AV6NewSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00E12", "SELECT T1.[ContagemResultado_Codigo], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_StatusDmn], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Demanda] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE (T1.[ContagemResultado_SistemaCod] = @AV5OldSistema_Codigo) AND (T1.[ContagemResultado_StatusDmn] = 'A' or T1.[ContagemResultado_StatusDmn] = 'C' or T1.[ContagemResultado_StatusDmn] = 'H' or T1.[ContagemResultado_StatusDmn] = 'R' or T1.[ContagemResultado_StatusDmn] = 'D' or T1.[ContagemResultado_StatusDmn] = 'S') ORDER BY T1.[ContagemResultado_SistemaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00E12,11,0,true,false )
             ,new CursorDef("H00E13", "SELECT COUNT(*) FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_SistemaCod] = @AV6NewSistema_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00E13,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 25) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
