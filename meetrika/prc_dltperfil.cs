/*
               File: PRC_DLTPerfil
        Description: Delete Perfil e desvincula associados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:12.35
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_dltperfil : GXProcedure
   {
      public prc_dltperfil( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_dltperfil( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Perfil_Codigo )
      {
         this.A3Perfil_Codigo = aP0_Perfil_Codigo;
         initialize();
         executePrivate();
         aP0_Perfil_Codigo=this.A3Perfil_Codigo;
      }

      public int executeUdp( )
      {
         this.A3Perfil_Codigo = aP0_Perfil_Codigo;
         initialize();
         executePrivate();
         aP0_Perfil_Codigo=this.A3Perfil_Codigo;
         return A3Perfil_Codigo ;
      }

      public void executeSubmit( ref int aP0_Perfil_Codigo )
      {
         prc_dltperfil objprc_dltperfil;
         objprc_dltperfil = new prc_dltperfil();
         objprc_dltperfil.A3Perfil_Codigo = aP0_Perfil_Codigo;
         objprc_dltperfil.context.SetSubmitInitialConfig(context);
         objprc_dltperfil.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_dltperfil);
         aP0_Perfil_Codigo=this.A3Perfil_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_dltperfil)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00VS2 */
         pr_default.execute(0, new Object[] {A3Perfil_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            /* Optimized DELETE. */
            /* Using cursor P00VS3 */
            pr_default.execute(1, new Object[] {A3Perfil_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("MenuPerfil") ;
            /* End optimized DELETE. */
            /* Optimized DELETE. */
            /* Using cursor P00VS4 */
            pr_default.execute(2, new Object[] {A3Perfil_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("UsuarioPerfil") ;
            /* End optimized DELETE. */
            /* Using cursor P00VS5 */
            pr_default.execute(3, new Object[] {A3Perfil_Codigo});
            pr_default.close(3);
            dsDefault.SmartCacheProvider.SetUpdated("Perfil") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_DLTPerfil");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00VS2_A3Perfil_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_dltperfil__default(),
            new Object[][] {
                new Object[] {
               P00VS2_A3Perfil_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A3Perfil_Codigo ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Perfil_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00VS2_A3Perfil_Codigo ;
   }

   public class prc_dltperfil__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VS2 ;
          prmP00VS2 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VS3 ;
          prmP00VS3 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VS4 ;
          prmP00VS4 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VS5 ;
          prmP00VS5 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VS2", "SELECT [Perfil_Codigo] FROM [Perfil] WITH (UPDLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ORDER BY [Perfil_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VS2,1,0,true,true )
             ,new CursorDef("P00VS3", "DELETE FROM [MenuPerfil]  WHERE [Perfil_Codigo] = @Perfil_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VS3)
             ,new CursorDef("P00VS4", "DELETE FROM [UsuarioPerfil]  WHERE [Perfil_Codigo] = @Perfil_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VS4)
             ,new CursorDef("P00VS5", "DELETE FROM [Perfil]  WHERE [Perfil_Codigo] = @Perfil_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VS5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
