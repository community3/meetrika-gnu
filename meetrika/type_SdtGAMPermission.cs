/*
               File: type_SdtGAMPermission
        Description: GAMPermission
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 21:58:11.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMPermission : GxUserType, IGxExternalObject
   {
      public SdtGAMPermission( )
      {
         initialize();
      }

      public SdtGAMPermission( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMPermission_externalReference == null )
         {
            GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
         }
         returntostring = "";
         returntostring = (String)(GAMPermission_externalReference.ToString());
         return returntostring ;
      }

      public long gxTpr_Applicationid
      {
         get {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            return GAMPermission_externalReference.ApplicationId ;
         }

         set {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            GAMPermission_externalReference.ApplicationId = value;
         }

      }

      public String gxTpr_Guid
      {
         get {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            return GAMPermission_externalReference.GUID ;
         }

         set {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            GAMPermission_externalReference.GUID = value;
         }

      }

      public String gxTpr_Name
      {
         get {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            return GAMPermission_externalReference.Name ;
         }

         set {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            GAMPermission_externalReference.Name = value;
         }

      }

      public String gxTpr_Description
      {
         get {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            return GAMPermission_externalReference.Description ;
         }

         set {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            GAMPermission_externalReference.Description = value;
         }

      }

      public String gxTpr_Token
      {
         get {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            return GAMPermission_externalReference.Token ;
         }

         set {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            GAMPermission_externalReference.Token = value;
         }

      }

      public String gxTpr_Type
      {
         get {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            return GAMPermission_externalReference.Type ;
         }

         set {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            GAMPermission_externalReference.Type = value;
         }

      }

      public bool gxTpr_Inherited
      {
         get {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            return GAMPermission_externalReference.Inherited ;
         }

         set {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            GAMPermission_externalReference.Inherited = value;
         }

      }

      public bool gxTpr_Isautomaticpermission
      {
         get {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            return GAMPermission_externalReference.IsAutomaticPermission ;
         }

         set {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            GAMPermission_externalReference.IsAutomaticPermission = value;
         }

      }

      public DateTime gxTpr_Datecreated
      {
         get {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            return GAMPermission_externalReference.DateCreated ;
         }

         set {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            GAMPermission_externalReference.DateCreated = value;
         }

      }

      public String gxTpr_Usercreated
      {
         get {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            return GAMPermission_externalReference.UserCreated ;
         }

         set {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            GAMPermission_externalReference.UserCreated = value;
         }

      }

      public DateTime gxTpr_Dateupdated
      {
         get {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            return GAMPermission_externalReference.DateUpdated ;
         }

         set {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            GAMPermission_externalReference.DateUpdated = value;
         }

      }

      public String gxTpr_Userupdated
      {
         get {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            return GAMPermission_externalReference.UserUpdated ;
         }

         set {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            GAMPermission_externalReference.UserUpdated = value;
         }

      }

      public IGxCollection gxTpr_Properties
      {
         get {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            IGxCollection intValue ;
            intValue = new GxExternalCollection( context, "SdtGAMProperty", "GeneXus.Programs");
            System.Collections.Generic.List<Artech.Security.GAMProperty> externalParm0 ;
            externalParm0 = GAMPermission_externalReference.Properties;
            intValue.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMProperty>), externalParm0);
            return intValue ;
         }

         set {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            IGxCollection intValue ;
            System.Collections.Generic.List<Artech.Security.GAMProperty> externalParm1 ;
            intValue = value;
            externalParm1 = (System.Collections.Generic.List<Artech.Security.GAMProperty>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List<Artech.Security.GAMProperty>), intValue.ExternalInstance);
            GAMPermission_externalReference.Properties = externalParm1;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMPermission_externalReference == null )
            {
               GAMPermission_externalReference = new Artech.Security.GAMPermission(context);
            }
            return GAMPermission_externalReference ;
         }

         set {
            GAMPermission_externalReference = (Artech.Security.GAMPermission)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMPermission GAMPermission_externalReference=null ;
   }

}
