/*
               File: PromptSolicitacaoMudancaItem
        Description: Selecione Item da Mudan�a
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 18:55:47.37
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptsolicitacaomudancaitem : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptsolicitacaomudancaitem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptsolicitacaomudancaitem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutSolicitacaoMudanca_Codigo ,
                           ref int aP1_InOutSolicitacaoMudancaItem_FuncaoAPF )
      {
         this.AV7InOutSolicitacaoMudanca_Codigo = aP0_InOutSolicitacaoMudanca_Codigo;
         this.AV8InOutSolicitacaoMudancaItem_FuncaoAPF = aP1_InOutSolicitacaoMudancaItem_FuncaoAPF;
         executePrivate();
         aP0_InOutSolicitacaoMudanca_Codigo=this.AV7InOutSolicitacaoMudanca_Codigo;
         aP1_InOutSolicitacaoMudancaItem_FuncaoAPF=this.AV8InOutSolicitacaoMudancaItem_FuncaoAPF;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_27 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_27_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_27_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV9OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
               AV10OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
               AV13TFSolicitacaoMudanca_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13TFSolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13TFSolicitacaoMudanca_Codigo), 6, 0)));
               AV14TFSolicitacaoMudanca_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14TFSolicitacaoMudanca_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14TFSolicitacaoMudanca_Codigo_To), 6, 0)));
               AV17TFSolicitacaoMudancaItem_FuncaoAPF = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TFSolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17TFSolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
               AV18TFSolicitacaoMudancaItem_FuncaoAPF_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18TFSolicitacaoMudancaItem_FuncaoAPF_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFSolicitacaoMudancaItem_FuncaoAPF_To), 6, 0)));
               AV21TFSolicitacaoMudanca_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFSolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFSolicitacaoMudanca_SistemaCod), 6, 0)));
               AV22TFSolicitacaoMudanca_SistemaCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFSolicitacaoMudanca_SistemaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFSolicitacaoMudanca_SistemaCod_To), 6, 0)));
               AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace", AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace);
               AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace", AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace);
               AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace", AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV9OrderedBy, AV10OrderedDsc, AV13TFSolicitacaoMudanca_Codigo, AV14TFSolicitacaoMudanca_Codigo_To, AV17TFSolicitacaoMudancaItem_FuncaoAPF, AV18TFSolicitacaoMudancaItem_FuncaoAPF_To, AV21TFSolicitacaoMudanca_SistemaCod, AV22TFSolicitacaoMudanca_SistemaCod_To, AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace, AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutSolicitacaoMudanca_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutSolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutSolicitacaoMudanca_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutSolicitacaoMudancaItem_FuncaoAPF = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutSolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutSolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAH42( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSH42( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEH42( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203118554745");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptsolicitacaomudancaitem.aspx") + "?" + UrlEncode("" +AV7InOutSolicitacaoMudanca_Codigo) + "," + UrlEncode("" +AV8InOutSolicitacaoMudancaItem_FuncaoAPF)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV10OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACAOMUDANCA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13TFSolicitacaoMudanca_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACAOMUDANCA_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14TFSolicitacaoMudanca_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17TFSolicitacaoMudancaItem_FuncaoAPF), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18TFSolicitacaoMudancaItem_FuncaoAPF_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACAOMUDANCA_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21TFSolicitacaoMudanca_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACAOMUDANCA_SISTEMACOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22TFSolicitacaoMudanca_SistemaCod_To), 6, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_27", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_27), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV24DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV24DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSOLICITACAOMUDANCA_CODIGOTITLEFILTERDATA", AV12SolicitacaoMudanca_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSOLICITACAOMUDANCA_CODIGOTITLEFILTERDATA", AV12SolicitacaoMudanca_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSOLICITACAOMUDANCAITEM_FUNCAOAPFTITLEFILTERDATA", AV16SolicitacaoMudancaItem_FuncaoAPFTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSOLICITACAOMUDANCAITEM_FUNCAOAPFTITLEFILTERDATA", AV16SolicitacaoMudancaItem_FuncaoAPFTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSOLICITACAOMUDANCA_SISTEMACODTITLEFILTERDATA", AV20SolicitacaoMudanca_SistemaCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSOLICITACAOMUDANCA_SISTEMACODTITLEFILTERDATA", AV20SolicitacaoMudanca_SistemaCodTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vINOUTSOLICITACAOMUDANCA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutSolicitacaoMudanca_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTSOLICITACAOMUDANCAITEM_FUNCAOAPF", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8InOutSolicitacaoMudancaItem_FuncaoAPF), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Caption", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Tooltip", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Cls", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_solicitacaomudanca_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_solicitacaomudanca_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_solicitacaomudanca_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Filtertype", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_solicitacaomudanca_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_solicitacaomudanca_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Sortasc", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Caption", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Tooltip", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Cls", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filteredtext_set", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filteredtextto_set", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Dropdownoptionstype", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Includesortasc", StringUtil.BoolToStr( Ddo_solicitacaomudancaitem_funcaoapf_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Includesortdsc", StringUtil.BoolToStr( Ddo_solicitacaomudancaitem_funcaoapf_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Sortedstatus", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Includefilter", StringUtil.BoolToStr( Ddo_solicitacaomudancaitem_funcaoapf_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filtertype", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filterisrange", StringUtil.BoolToStr( Ddo_solicitacaomudancaitem_funcaoapf_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Includedatalist", StringUtil.BoolToStr( Ddo_solicitacaomudancaitem_funcaoapf_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Sortasc", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Sortdsc", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Cleanfilter", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Rangefilterfrom", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Rangefilterto", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Searchbuttontext", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Caption", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Tooltip", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Cls", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filteredtext_set", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filteredtextto_set", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Includesortasc", StringUtil.BoolToStr( Ddo_solicitacaomudanca_sistemacod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Includesortdsc", StringUtil.BoolToStr( Ddo_solicitacaomudanca_sistemacod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Sortedstatus", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Includefilter", StringUtil.BoolToStr( Ddo_solicitacaomudanca_sistemacod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filtertype", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filterisrange", StringUtil.BoolToStr( Ddo_solicitacaomudanca_sistemacod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Includedatalist", StringUtil.BoolToStr( Ddo_solicitacaomudanca_sistemacod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Sortasc", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Sortdsc", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Cleanfilter", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Rangefilterfrom", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Rangefilterto", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Searchbuttontext", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_solicitacaomudanca_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Activeeventkey", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filteredtext_get", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filteredtextto_get", StringUtil.RTrim( Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Activeeventkey", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filteredtext_get", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filteredtextto_get", StringUtil.RTrim( Ddo_solicitacaomudanca_sistemacod_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormH42( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptSolicitacaoMudancaItem" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Item da Mudan�a" ;
      }

      protected void WBH40( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_H42( true) ;
         }
         else
         {
            wb_table1_2_H42( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_H42e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacaomudanca_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13TFSolicitacaoMudanca_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13TFSolicitacaoMudanca_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacaomudanca_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacaomudanca_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudancaItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacaomudanca_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14TFSolicitacaoMudanca_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV14TFSolicitacaoMudanca_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacaomudanca_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacaomudanca_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudancaItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacaomudancaitem_funcaoapf_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17TFSolicitacaoMudancaItem_FuncaoAPF), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV17TFSolicitacaoMudancaItem_FuncaoAPF), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacaomudancaitem_funcaoapf_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacaomudancaitem_funcaoapf_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudancaItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacaomudancaitem_funcaoapf_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18TFSolicitacaoMudancaItem_FuncaoAPF_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV18TFSolicitacaoMudancaItem_FuncaoAPF_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacaomudancaitem_funcaoapf_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacaomudancaitem_funcaoapf_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudancaItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacaomudanca_sistemacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21TFSolicitacaoMudanca_SistemaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV21TFSolicitacaoMudanca_SistemaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacaomudanca_sistemacod_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacaomudanca_sistemacod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudancaItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacaomudanca_sistemacod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22TFSolicitacaoMudanca_SistemaCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV22TFSolicitacaoMudanca_SistemaCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacaomudanca_sistemacod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacaomudanca_sistemacod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudancaItem.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SOLICITACAOMUDANCA_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_27_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Internalname, AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", 0, edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSolicitacaoMudancaItem.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPFContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_27_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Internalname, AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", 0, edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSolicitacaoMudancaItem.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SOLICITACAOMUDANCA_SISTEMACODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_27_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Internalname, AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", 0, edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSolicitacaoMudancaItem.htm");
         }
         wbLoad = true;
      }

      protected void STARTH42( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Item da Mudan�a", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPH40( ) ;
      }

      protected void WSH42( )
      {
         STARTH42( ) ;
         EVTH42( ) ;
      }

      protected void EVTH42( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11H42 */
                           E11H42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SOLICITACAOMUDANCA_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12H42 */
                           E12H42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13H42 */
                           E13H42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SOLICITACAOMUDANCA_SISTEMACOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14H42 */
                           E14H42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15H42 */
                           E15H42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16H42 */
                           E16H42 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_27_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
                           SubsflControlProps_272( ) ;
                           GXCCtl = "SOLICITACAOMUDANCAITEM_DESCRICAO_Enabled_" + sGXsfl_27_idx;
                           Solicitacaomudancaitem_descricao_Enabled = StringUtil.StrToBool( cgiGet( GXCCtl));
                           context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Solicitacaomudancaitem_descricao_Internalname, "Enabled", StringUtil.BoolToStr( Solicitacaomudancaitem_descricao_Enabled));
                           AV11Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV11Select)) ? AV30Select_GXI : context.convertURL( context.PathToRelativeUrl( AV11Select))));
                           A996SolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSolicitacaoMudanca_Codigo_Internalname), ",", "."));
                           A995SolicitacaoMudancaItem_FuncaoAPF = (int)(context.localUtil.CToN( cgiGet( edtSolicitacaoMudancaItem_FuncaoAPF_Internalname), ",", "."));
                           A993SolicitacaoMudanca_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtSolicitacaoMudanca_SistemaCod_Internalname), ",", "."));
                           n993SolicitacaoMudanca_SistemaCod = false;
                           GXCCtl = "SOLICITACAOMUDANCAITEM_DESCRICAO_" + sGXsfl_27_idx;
                           A998SolicitacaoMudancaItem_Descricao = cgiGet( GXCCtl);
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E17H42 */
                                 E17H42 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E18H42 */
                                 E18H42 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E19H42 */
                                 E19H42 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV9OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV10OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacaomudanca_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_CODIGO"), ",", ".") != Convert.ToDecimal( AV13TFSolicitacaoMudanca_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacaomudanca_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV14TFSolicitacaoMudanca_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacaomudancaitem_funcaoapf Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF"), ",", ".") != Convert.ToDecimal( AV17TFSolicitacaoMudancaItem_FuncaoAPF )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacaomudancaitem_funcaoapf_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO"), ",", ".") != Convert.ToDecimal( AV18TFSolicitacaoMudancaItem_FuncaoAPF_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacaomudanca_sistemacod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_SISTEMACOD"), ",", ".") != Convert.ToDecimal( AV21TFSolicitacaoMudanca_SistemaCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacaomudanca_sistemacod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_SISTEMACOD_TO"), ",", ".") != Convert.ToDecimal( AV22TFSolicitacaoMudanca_SistemaCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E20H42 */
                                       E20H42 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEH42( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormH42( ) ;
            }
         }
      }

      protected void PAH42( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV9OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_272( ) ;
         while ( nGXsfl_27_idx <= nRC_GXsfl_27 )
         {
            sendrow_272( ) ;
            nGXsfl_27_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_27_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_27_idx+1));
            sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
            SubsflControlProps_272( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV9OrderedBy ,
                                       bool AV10OrderedDsc ,
                                       int AV13TFSolicitacaoMudanca_Codigo ,
                                       int AV14TFSolicitacaoMudanca_Codigo_To ,
                                       int AV17TFSolicitacaoMudancaItem_FuncaoAPF ,
                                       int AV18TFSolicitacaoMudancaItem_FuncaoAPF_To ,
                                       int AV21TFSolicitacaoMudanca_SistemaCod ,
                                       int AV22TFSolicitacaoMudanca_SistemaCod_To ,
                                       String AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace ,
                                       String AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace ,
                                       String AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFH42( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACAOMUDANCA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SOLICITACAOMUDANCA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACAOMUDANCAITEM_FUNCAOAPF", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SOLICITACAOMUDANCAITEM_FUNCAOAPF", StringUtil.LTrim( StringUtil.NToC( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV9OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFH42( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFH42( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 27;
         /* Execute user event: E18H42 */
         E18H42 ();
         nGXsfl_27_idx = 1;
         sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
         SubsflControlProps_272( ) ;
         nGXsfl_27_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_272( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV13TFSolicitacaoMudanca_Codigo ,
                                                 AV14TFSolicitacaoMudanca_Codigo_To ,
                                                 AV17TFSolicitacaoMudancaItem_FuncaoAPF ,
                                                 AV18TFSolicitacaoMudancaItem_FuncaoAPF_To ,
                                                 AV21TFSolicitacaoMudanca_SistemaCod ,
                                                 AV22TFSolicitacaoMudanca_SistemaCod_To ,
                                                 A996SolicitacaoMudanca_Codigo ,
                                                 A995SolicitacaoMudancaItem_FuncaoAPF ,
                                                 A993SolicitacaoMudanca_SistemaCod ,
                                                 AV9OrderedBy ,
                                                 AV10OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00H42 */
            pr_default.execute(0, new Object[] {AV13TFSolicitacaoMudanca_Codigo, AV14TFSolicitacaoMudanca_Codigo_To, AV17TFSolicitacaoMudancaItem_FuncaoAPF, AV18TFSolicitacaoMudancaItem_FuncaoAPF_To, AV21TFSolicitacaoMudanca_SistemaCod, AV22TFSolicitacaoMudanca_SistemaCod_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_27_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A998SolicitacaoMudancaItem_Descricao = H00H42_A998SolicitacaoMudancaItem_Descricao[0];
               A993SolicitacaoMudanca_SistemaCod = H00H42_A993SolicitacaoMudanca_SistemaCod[0];
               n993SolicitacaoMudanca_SistemaCod = H00H42_n993SolicitacaoMudanca_SistemaCod[0];
               A995SolicitacaoMudancaItem_FuncaoAPF = H00H42_A995SolicitacaoMudancaItem_FuncaoAPF[0];
               A996SolicitacaoMudanca_Codigo = H00H42_A996SolicitacaoMudanca_Codigo[0];
               A993SolicitacaoMudanca_SistemaCod = H00H42_A993SolicitacaoMudanca_SistemaCod[0];
               n993SolicitacaoMudanca_SistemaCod = H00H42_n993SolicitacaoMudanca_SistemaCod[0];
               /* Execute user event: E19H42 */
               E19H42 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 27;
            WBH40( ) ;
         }
         nGXsfl_27_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV13TFSolicitacaoMudanca_Codigo ,
                                              AV14TFSolicitacaoMudanca_Codigo_To ,
                                              AV17TFSolicitacaoMudancaItem_FuncaoAPF ,
                                              AV18TFSolicitacaoMudancaItem_FuncaoAPF_To ,
                                              AV21TFSolicitacaoMudanca_SistemaCod ,
                                              AV22TFSolicitacaoMudanca_SistemaCod_To ,
                                              A996SolicitacaoMudanca_Codigo ,
                                              A995SolicitacaoMudancaItem_FuncaoAPF ,
                                              A993SolicitacaoMudanca_SistemaCod ,
                                              AV9OrderedBy ,
                                              AV10OrderedDsc },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00H43 */
         pr_default.execute(1, new Object[] {AV13TFSolicitacaoMudanca_Codigo, AV14TFSolicitacaoMudanca_Codigo_To, AV17TFSolicitacaoMudancaItem_FuncaoAPF, AV18TFSolicitacaoMudancaItem_FuncaoAPF_To, AV21TFSolicitacaoMudanca_SistemaCod, AV22TFSolicitacaoMudanca_SistemaCod_To});
         GRID_nRecordCount = H00H43_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV9OrderedBy, AV10OrderedDsc, AV13TFSolicitacaoMudanca_Codigo, AV14TFSolicitacaoMudanca_Codigo_To, AV17TFSolicitacaoMudancaItem_FuncaoAPF, AV18TFSolicitacaoMudancaItem_FuncaoAPF_To, AV21TFSolicitacaoMudanca_SistemaCod, AV22TFSolicitacaoMudanca_SistemaCod_To, AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace, AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV9OrderedBy, AV10OrderedDsc, AV13TFSolicitacaoMudanca_Codigo, AV14TFSolicitacaoMudanca_Codigo_To, AV17TFSolicitacaoMudancaItem_FuncaoAPF, AV18TFSolicitacaoMudancaItem_FuncaoAPF_To, AV21TFSolicitacaoMudanca_SistemaCod, AV22TFSolicitacaoMudanca_SistemaCod_To, AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace, AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV9OrderedBy, AV10OrderedDsc, AV13TFSolicitacaoMudanca_Codigo, AV14TFSolicitacaoMudanca_Codigo_To, AV17TFSolicitacaoMudancaItem_FuncaoAPF, AV18TFSolicitacaoMudancaItem_FuncaoAPF_To, AV21TFSolicitacaoMudanca_SistemaCod, AV22TFSolicitacaoMudanca_SistemaCod_To, AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace, AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV9OrderedBy, AV10OrderedDsc, AV13TFSolicitacaoMudanca_Codigo, AV14TFSolicitacaoMudanca_Codigo_To, AV17TFSolicitacaoMudancaItem_FuncaoAPF, AV18TFSolicitacaoMudancaItem_FuncaoAPF_To, AV21TFSolicitacaoMudanca_SistemaCod, AV22TFSolicitacaoMudanca_SistemaCod_To, AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace, AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV9OrderedBy, AV10OrderedDsc, AV13TFSolicitacaoMudanca_Codigo, AV14TFSolicitacaoMudanca_Codigo_To, AV17TFSolicitacaoMudancaItem_FuncaoAPF, AV18TFSolicitacaoMudancaItem_FuncaoAPF_To, AV21TFSolicitacaoMudanca_SistemaCod, AV22TFSolicitacaoMudanca_SistemaCod_To, AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace, AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace) ;
         }
         return (int)(0) ;
      }

      protected void STRUPH40( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E17H42 */
         E17H42 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV24DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vSOLICITACAOMUDANCA_CODIGOTITLEFILTERDATA"), AV12SolicitacaoMudanca_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSOLICITACAOMUDANCAITEM_FUNCAOAPFTITLEFILTERDATA"), AV16SolicitacaoMudancaItem_FuncaoAPFTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSOLICITACAOMUDANCA_SISTEMACODTITLEFILTERDATA"), AV20SolicitacaoMudanca_SistemaCodTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV9OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            AV10OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACAOMUDANCA_CODIGO");
               GX_FocusControl = edtavTfsolicitacaomudanca_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13TFSolicitacaoMudanca_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13TFSolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13TFSolicitacaoMudanca_Codigo), 6, 0)));
            }
            else
            {
               AV13TFSolicitacaoMudanca_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13TFSolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13TFSolicitacaoMudanca_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACAOMUDANCA_CODIGO_TO");
               GX_FocusControl = edtavTfsolicitacaomudanca_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14TFSolicitacaoMudanca_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14TFSolicitacaoMudanca_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14TFSolicitacaoMudanca_Codigo_To), 6, 0)));
            }
            else
            {
               AV14TFSolicitacaoMudanca_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14TFSolicitacaoMudanca_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14TFSolicitacaoMudanca_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudancaitem_funcaoapf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudancaitem_funcaoapf_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF");
               GX_FocusControl = edtavTfsolicitacaomudancaitem_funcaoapf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17TFSolicitacaoMudancaItem_FuncaoAPF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TFSolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17TFSolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
            }
            else
            {
               AV17TFSolicitacaoMudancaItem_FuncaoAPF = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudancaitem_funcaoapf_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TFSolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17TFSolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudancaitem_funcaoapf_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudancaitem_funcaoapf_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO");
               GX_FocusControl = edtavTfsolicitacaomudancaitem_funcaoapf_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18TFSolicitacaoMudancaItem_FuncaoAPF_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18TFSolicitacaoMudancaItem_FuncaoAPF_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFSolicitacaoMudancaItem_FuncaoAPF_To), 6, 0)));
            }
            else
            {
               AV18TFSolicitacaoMudancaItem_FuncaoAPF_To = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudancaitem_funcaoapf_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18TFSolicitacaoMudancaItem_FuncaoAPF_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFSolicitacaoMudancaItem_FuncaoAPF_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_sistemacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_sistemacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACAOMUDANCA_SISTEMACOD");
               GX_FocusControl = edtavTfsolicitacaomudanca_sistemacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21TFSolicitacaoMudanca_SistemaCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFSolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFSolicitacaoMudanca_SistemaCod), 6, 0)));
            }
            else
            {
               AV21TFSolicitacaoMudanca_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_sistemacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFSolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFSolicitacaoMudanca_SistemaCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_sistemacod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_sistemacod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACAOMUDANCA_SISTEMACOD_TO");
               GX_FocusControl = edtavTfsolicitacaomudanca_sistemacod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22TFSolicitacaoMudanca_SistemaCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFSolicitacaoMudanca_SistemaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFSolicitacaoMudanca_SistemaCod_To), 6, 0)));
            }
            else
            {
               AV22TFSolicitacaoMudanca_SistemaCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacaomudanca_sistemacod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFSolicitacaoMudanca_SistemaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFSolicitacaoMudanca_SistemaCod_To), 6, 0)));
            }
            AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace", AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace);
            AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace = cgiGet( edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace", AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace);
            AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace = cgiGet( edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace", AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_27 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_27"), ",", "."));
            AV26GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV27GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_solicitacaomudanca_codigo_Caption = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Caption");
            Ddo_solicitacaomudanca_codigo_Tooltip = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Tooltip");
            Ddo_solicitacaomudanca_codigo_Cls = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Cls");
            Ddo_solicitacaomudanca_codigo_Filteredtext_set = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Filteredtext_set");
            Ddo_solicitacaomudanca_codigo_Filteredtextto_set = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Filteredtextto_set");
            Ddo_solicitacaomudanca_codigo_Dropdownoptionstype = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Dropdownoptionstype");
            Ddo_solicitacaomudanca_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Titlecontrolidtoreplace");
            Ddo_solicitacaomudanca_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Includesortasc"));
            Ddo_solicitacaomudanca_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Includesortdsc"));
            Ddo_solicitacaomudanca_codigo_Sortedstatus = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Sortedstatus");
            Ddo_solicitacaomudanca_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Includefilter"));
            Ddo_solicitacaomudanca_codigo_Filtertype = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Filtertype");
            Ddo_solicitacaomudanca_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Filterisrange"));
            Ddo_solicitacaomudanca_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Includedatalist"));
            Ddo_solicitacaomudanca_codigo_Sortasc = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Sortasc");
            Ddo_solicitacaomudanca_codigo_Sortdsc = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Sortdsc");
            Ddo_solicitacaomudanca_codigo_Cleanfilter = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Cleanfilter");
            Ddo_solicitacaomudanca_codigo_Rangefilterfrom = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Rangefilterfrom");
            Ddo_solicitacaomudanca_codigo_Rangefilterto = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Rangefilterto");
            Ddo_solicitacaomudanca_codigo_Searchbuttontext = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Searchbuttontext");
            Ddo_solicitacaomudancaitem_funcaoapf_Caption = cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Caption");
            Ddo_solicitacaomudancaitem_funcaoapf_Tooltip = cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Tooltip");
            Ddo_solicitacaomudancaitem_funcaoapf_Cls = cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Cls");
            Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_set = cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filteredtext_set");
            Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_set = cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filteredtextto_set");
            Ddo_solicitacaomudancaitem_funcaoapf_Dropdownoptionstype = cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Dropdownoptionstype");
            Ddo_solicitacaomudancaitem_funcaoapf_Titlecontrolidtoreplace = cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Titlecontrolidtoreplace");
            Ddo_solicitacaomudancaitem_funcaoapf_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Includesortasc"));
            Ddo_solicitacaomudancaitem_funcaoapf_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Includesortdsc"));
            Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus = cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Sortedstatus");
            Ddo_solicitacaomudancaitem_funcaoapf_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Includefilter"));
            Ddo_solicitacaomudancaitem_funcaoapf_Filtertype = cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filtertype");
            Ddo_solicitacaomudancaitem_funcaoapf_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filterisrange"));
            Ddo_solicitacaomudancaitem_funcaoapf_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Includedatalist"));
            Ddo_solicitacaomudancaitem_funcaoapf_Sortasc = cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Sortasc");
            Ddo_solicitacaomudancaitem_funcaoapf_Sortdsc = cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Sortdsc");
            Ddo_solicitacaomudancaitem_funcaoapf_Cleanfilter = cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Cleanfilter");
            Ddo_solicitacaomudancaitem_funcaoapf_Rangefilterfrom = cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Rangefilterfrom");
            Ddo_solicitacaomudancaitem_funcaoapf_Rangefilterto = cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Rangefilterto");
            Ddo_solicitacaomudancaitem_funcaoapf_Searchbuttontext = cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Searchbuttontext");
            Ddo_solicitacaomudanca_sistemacod_Caption = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Caption");
            Ddo_solicitacaomudanca_sistemacod_Tooltip = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Tooltip");
            Ddo_solicitacaomudanca_sistemacod_Cls = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Cls");
            Ddo_solicitacaomudanca_sistemacod_Filteredtext_set = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filteredtext_set");
            Ddo_solicitacaomudanca_sistemacod_Filteredtextto_set = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filteredtextto_set");
            Ddo_solicitacaomudanca_sistemacod_Dropdownoptionstype = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Dropdownoptionstype");
            Ddo_solicitacaomudanca_sistemacod_Titlecontrolidtoreplace = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Titlecontrolidtoreplace");
            Ddo_solicitacaomudanca_sistemacod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Includesortasc"));
            Ddo_solicitacaomudanca_sistemacod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Includesortdsc"));
            Ddo_solicitacaomudanca_sistemacod_Sortedstatus = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Sortedstatus");
            Ddo_solicitacaomudanca_sistemacod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Includefilter"));
            Ddo_solicitacaomudanca_sistemacod_Filtertype = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filtertype");
            Ddo_solicitacaomudanca_sistemacod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filterisrange"));
            Ddo_solicitacaomudanca_sistemacod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Includedatalist"));
            Ddo_solicitacaomudanca_sistemacod_Sortasc = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Sortasc");
            Ddo_solicitacaomudanca_sistemacod_Sortdsc = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Sortdsc");
            Ddo_solicitacaomudanca_sistemacod_Cleanfilter = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Cleanfilter");
            Ddo_solicitacaomudanca_sistemacod_Rangefilterfrom = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Rangefilterfrom");
            Ddo_solicitacaomudanca_sistemacod_Rangefilterto = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Rangefilterto");
            Ddo_solicitacaomudanca_sistemacod_Searchbuttontext = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_solicitacaomudanca_codigo_Activeeventkey = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Activeeventkey");
            Ddo_solicitacaomudanca_codigo_Filteredtext_get = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Filteredtext_get");
            Ddo_solicitacaomudanca_codigo_Filteredtextto_get = cgiGet( "DDO_SOLICITACAOMUDANCA_CODIGO_Filteredtextto_get");
            Ddo_solicitacaomudancaitem_funcaoapf_Activeeventkey = cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Activeeventkey");
            Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_get = cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filteredtext_get");
            Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_get = cgiGet( "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF_Filteredtextto_get");
            Ddo_solicitacaomudanca_sistemacod_Activeeventkey = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Activeeventkey");
            Ddo_solicitacaomudanca_sistemacod_Filteredtext_get = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filteredtext_get");
            Ddo_solicitacaomudanca_sistemacod_Filteredtextto_get = cgiGet( "DDO_SOLICITACAOMUDANCA_SISTEMACOD_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV9OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV10OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_CODIGO"), ",", ".") != Convert.ToDecimal( AV13TFSolicitacaoMudanca_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV14TFSolicitacaoMudanca_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF"), ",", ".") != Convert.ToDecimal( AV17TFSolicitacaoMudancaItem_FuncaoAPF )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO"), ",", ".") != Convert.ToDecimal( AV18TFSolicitacaoMudancaItem_FuncaoAPF_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_SISTEMACOD"), ",", ".") != Convert.ToDecimal( AV21TFSolicitacaoMudanca_SistemaCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACAOMUDANCA_SISTEMACOD_TO"), ",", ".") != Convert.ToDecimal( AV22TFSolicitacaoMudanca_SistemaCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E17H42 */
         E17H42 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17H42( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfsolicitacaomudanca_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacaomudanca_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacaomudanca_codigo_Visible), 5, 0)));
         edtavTfsolicitacaomudanca_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacaomudanca_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacaomudanca_codigo_to_Visible), 5, 0)));
         edtavTfsolicitacaomudancaitem_funcaoapf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacaomudancaitem_funcaoapf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacaomudancaitem_funcaoapf_Visible), 5, 0)));
         edtavTfsolicitacaomudancaitem_funcaoapf_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacaomudancaitem_funcaoapf_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacaomudancaitem_funcaoapf_to_Visible), 5, 0)));
         edtavTfsolicitacaomudanca_sistemacod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacaomudanca_sistemacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacaomudanca_sistemacod_Visible), 5, 0)));
         edtavTfsolicitacaomudanca_sistemacod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacaomudanca_sistemacod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacaomudanca_sistemacod_to_Visible), 5, 0)));
         Ddo_solicitacaomudanca_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_SolicitacaoMudanca_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_codigo_Internalname, "TitleControlIdToReplace", Ddo_solicitacaomudanca_codigo_Titlecontrolidtoreplace);
         AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace = Ddo_solicitacaomudanca_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace", AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace);
         edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_solicitacaomudancaitem_funcaoapf_Titlecontrolidtoreplace = subGrid_Internalname+"_SolicitacaoMudancaItem_FuncaoAPF";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudancaitem_funcaoapf_Internalname, "TitleControlIdToReplace", Ddo_solicitacaomudancaitem_funcaoapf_Titlecontrolidtoreplace);
         AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace = Ddo_solicitacaomudancaitem_funcaoapf_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace", AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace);
         edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_solicitacaomudanca_sistemacod_Titlecontrolidtoreplace = subGrid_Internalname+"_SolicitacaoMudanca_SistemaCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_sistemacod_Internalname, "TitleControlIdToReplace", Ddo_solicitacaomudanca_sistemacod_Titlecontrolidtoreplace);
         AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace = Ddo_solicitacaomudanca_sistemacod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace", AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace);
         edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Item da Mudan�a";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Id", 0);
         cmbavOrderedby.addItem("2", "Item", 0);
         cmbavOrderedby.addItem("3", "Sistema", 0);
         if ( AV9OrderedBy < 1 )
         {
            AV9OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV24DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV24DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E18H42( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV12SolicitacaoMudanca_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV16SolicitacaoMudancaItem_FuncaoAPFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV20SolicitacaoMudanca_SistemaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         edtSolicitacaoMudanca_Codigo_Titleformat = 2;
         edtSolicitacaoMudanca_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Id", AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudanca_Codigo_Internalname, "Title", edtSolicitacaoMudanca_Codigo_Title);
         edtSolicitacaoMudancaItem_FuncaoAPF_Titleformat = 2;
         edtSolicitacaoMudancaItem_FuncaoAPF_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Item", AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudancaItem_FuncaoAPF_Internalname, "Title", edtSolicitacaoMudancaItem_FuncaoAPF_Title);
         edtSolicitacaoMudanca_SistemaCod_Titleformat = 2;
         edtSolicitacaoMudanca_SistemaCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sistema", AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacaoMudanca_SistemaCod_Internalname, "Title", edtSolicitacaoMudanca_SistemaCod_Title);
         AV26GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26GridCurrentPage), 10, 0)));
         AV27GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12SolicitacaoMudanca_CodigoTitleFilterData", AV12SolicitacaoMudanca_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV16SolicitacaoMudancaItem_FuncaoAPFTitleFilterData", AV16SolicitacaoMudancaItem_FuncaoAPFTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20SolicitacaoMudanca_SistemaCodTitleFilterData", AV20SolicitacaoMudanca_SistemaCodTitleFilterData);
      }

      protected void E11H42( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV25PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV25PageToGo) ;
         }
      }

      protected void E12H42( )
      {
         /* Ddo_solicitacaomudanca_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_solicitacaomudanca_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV9OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            AV10OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
            Ddo_solicitacaomudanca_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_codigo_Internalname, "SortedStatus", Ddo_solicitacaomudanca_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacaomudanca_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV9OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            AV10OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
            Ddo_solicitacaomudanca_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_codigo_Internalname, "SortedStatus", Ddo_solicitacaomudanca_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacaomudanca_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV13TFSolicitacaoMudanca_Codigo = (int)(NumberUtil.Val( Ddo_solicitacaomudanca_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13TFSolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13TFSolicitacaoMudanca_Codigo), 6, 0)));
            AV14TFSolicitacaoMudanca_Codigo_To = (int)(NumberUtil.Val( Ddo_solicitacaomudanca_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14TFSolicitacaoMudanca_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14TFSolicitacaoMudanca_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13H42( )
      {
         /* Ddo_solicitacaomudancaitem_funcaoapf_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_solicitacaomudancaitem_funcaoapf_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV9OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            AV10OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
            Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudancaitem_funcaoapf_Internalname, "SortedStatus", Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacaomudancaitem_funcaoapf_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV9OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            AV10OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
            Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudancaitem_funcaoapf_Internalname, "SortedStatus", Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacaomudancaitem_funcaoapf_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV17TFSolicitacaoMudancaItem_FuncaoAPF = (int)(NumberUtil.Val( Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TFSolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17TFSolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
            AV18TFSolicitacaoMudancaItem_FuncaoAPF_To = (int)(NumberUtil.Val( Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18TFSolicitacaoMudancaItem_FuncaoAPF_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFSolicitacaoMudancaItem_FuncaoAPF_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14H42( )
      {
         /* Ddo_solicitacaomudanca_sistemacod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_solicitacaomudanca_sistemacod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV9OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            AV10OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
            Ddo_solicitacaomudanca_sistemacod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_sistemacod_Internalname, "SortedStatus", Ddo_solicitacaomudanca_sistemacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacaomudanca_sistemacod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV9OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)));
            AV10OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10OrderedDsc", AV10OrderedDsc);
            Ddo_solicitacaomudanca_sistemacod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_sistemacod_Internalname, "SortedStatus", Ddo_solicitacaomudanca_sistemacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacaomudanca_sistemacod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV21TFSolicitacaoMudanca_SistemaCod = (int)(NumberUtil.Val( Ddo_solicitacaomudanca_sistemacod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFSolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFSolicitacaoMudanca_SistemaCod), 6, 0)));
            AV22TFSolicitacaoMudanca_SistemaCod_To = (int)(NumberUtil.Val( Ddo_solicitacaomudanca_sistemacod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFSolicitacaoMudanca_SistemaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFSolicitacaoMudanca_SistemaCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E19H42( )
      {
         /* Grid_Load Routine */
         AV11Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV11Select);
         AV30Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 27;
         }
         sendrow_272( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_27_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(27, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E20H42 */
         E20H42 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20H42( )
      {
         /* Enter Routine */
         AV7InOutSolicitacaoMudanca_Codigo = A996SolicitacaoMudanca_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutSolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutSolicitacaoMudanca_Codigo), 6, 0)));
         AV8InOutSolicitacaoMudancaItem_FuncaoAPF = A995SolicitacaoMudancaItem_FuncaoAPF;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutSolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutSolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
         context.setWebReturnParms(new Object[] {(int)AV7InOutSolicitacaoMudanca_Codigo,(int)AV8InOutSolicitacaoMudancaItem_FuncaoAPF});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E15H42( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E16H42( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
      }

      protected void S122( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_solicitacaomudanca_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_codigo_Internalname, "SortedStatus", Ddo_solicitacaomudanca_codigo_Sortedstatus);
         Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudancaitem_funcaoapf_Internalname, "SortedStatus", Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus);
         Ddo_solicitacaomudanca_sistemacod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_sistemacod_Internalname, "SortedStatus", Ddo_solicitacaomudanca_sistemacod_Sortedstatus);
      }

      protected void S112( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV9OrderedBy == 1 )
         {
            Ddo_solicitacaomudanca_codigo_Sortedstatus = (AV10OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_codigo_Internalname, "SortedStatus", Ddo_solicitacaomudanca_codigo_Sortedstatus);
         }
         else if ( AV9OrderedBy == 2 )
         {
            Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus = (AV10OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudancaitem_funcaoapf_Internalname, "SortedStatus", Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus);
         }
         else if ( AV9OrderedBy == 3 )
         {
            Ddo_solicitacaomudanca_sistemacod_Sortedstatus = (AV10OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_sistemacod_Internalname, "SortedStatus", Ddo_solicitacaomudanca_sistemacod_Sortedstatus);
         }
      }

      protected void S132( )
      {
         /* 'CLEANFILTERS' Routine */
         AV13TFSolicitacaoMudanca_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13TFSolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13TFSolicitacaoMudanca_Codigo), 6, 0)));
         Ddo_solicitacaomudanca_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_codigo_Internalname, "FilteredText_set", Ddo_solicitacaomudanca_codigo_Filteredtext_set);
         AV14TFSolicitacaoMudanca_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14TFSolicitacaoMudanca_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14TFSolicitacaoMudanca_Codigo_To), 6, 0)));
         Ddo_solicitacaomudanca_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_codigo_Internalname, "FilteredTextTo_set", Ddo_solicitacaomudanca_codigo_Filteredtextto_set);
         AV17TFSolicitacaoMudancaItem_FuncaoAPF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TFSolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17TFSolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
         Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudancaitem_funcaoapf_Internalname, "FilteredText_set", Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_set);
         AV18TFSolicitacaoMudancaItem_FuncaoAPF_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18TFSolicitacaoMudancaItem_FuncaoAPF_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFSolicitacaoMudancaItem_FuncaoAPF_To), 6, 0)));
         Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudancaitem_funcaoapf_Internalname, "FilteredTextTo_set", Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_set);
         AV21TFSolicitacaoMudanca_SistemaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFSolicitacaoMudanca_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFSolicitacaoMudanca_SistemaCod), 6, 0)));
         Ddo_solicitacaomudanca_sistemacod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_sistemacod_Internalname, "FilteredText_set", Ddo_solicitacaomudanca_sistemacod_Filteredtext_set);
         AV22TFSolicitacaoMudanca_SistemaCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFSolicitacaoMudanca_SistemaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFSolicitacaoMudanca_SistemaCod_To), 6, 0)));
         Ddo_solicitacaomudanca_sistemacod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacaomudanca_sistemacod_Internalname, "FilteredTextTo_set", Ddo_solicitacaomudanca_sistemacod_Filteredtextto_set);
      }

      protected void wb_table1_2_H42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_H42( true) ;
         }
         else
         {
            wb_table2_5_H42( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_H42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_21_H42( true) ;
         }
         else
         {
            wb_table3_21_H42( false) ;
         }
         return  ;
      }

      protected void wb_table3_21_H42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_H42e( true) ;
         }
         else
         {
            wb_table1_2_H42e( false) ;
         }
      }

      protected void wb_table3_21_H42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_24_H42( true) ;
         }
         else
         {
            wb_table4_24_H42( false) ;
         }
         return  ;
      }

      protected void wb_table4_24_H42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_21_H42e( true) ;
         }
         else
         {
            wb_table3_21_H42e( false) ;
         }
      }

      protected void wb_table4_24_H42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"27\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSolicitacaoMudanca_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtSolicitacaoMudanca_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSolicitacaoMudanca_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSolicitacaoMudancaItem_FuncaoAPF_Titleformat == 0 )
               {
                  context.SendWebValue( edtSolicitacaoMudancaItem_FuncaoAPF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSolicitacaoMudancaItem_FuncaoAPF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSolicitacaoMudanca_SistemaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtSolicitacaoMudanca_SistemaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSolicitacaoMudanca_SistemaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV11Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSolicitacaoMudanca_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSolicitacaoMudanca_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSolicitacaoMudancaItem_FuncaoAPF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSolicitacaoMudancaItem_FuncaoAPF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSolicitacaoMudanca_SistemaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSolicitacaoMudanca_SistemaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 27 )
         {
            wbEnd = 0;
            nRC_GXsfl_27 = (short)(nGXsfl_27_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_24_H42e( true) ;
         }
         else
         {
            wb_table4_24_H42e( false) ;
         }
      }

      protected void wb_table2_5_H42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptSolicitacaoMudancaItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_27_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptSolicitacaoMudancaItem.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV10OrderedDsc), StringUtil.BoolToStr( AV10OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptSolicitacaoMudancaItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_H42( true) ;
         }
         else
         {
            wb_table5_14_H42( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_H42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_H42e( true) ;
         }
         else
         {
            wb_table2_5_H42e( false) ;
         }
      }

      protected void wb_table5_14_H42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptSolicitacaoMudancaItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_H42e( true) ;
         }
         else
         {
            wb_table5_14_H42e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutSolicitacaoMudanca_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutSolicitacaoMudanca_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutSolicitacaoMudanca_Codigo), 6, 0)));
         AV8InOutSolicitacaoMudancaItem_FuncaoAPF = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutSolicitacaoMudancaItem_FuncaoAPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutSolicitacaoMudancaItem_FuncaoAPF), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAH42( ) ;
         WSH42( ) ;
         WEH42( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203118555014");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptsolicitacaomudancaitem.js", "?20203118555015");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_272( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_27_idx;
         edtSolicitacaoMudanca_Codigo_Internalname = "SOLICITACAOMUDANCA_CODIGO_"+sGXsfl_27_idx;
         edtSolicitacaoMudancaItem_FuncaoAPF_Internalname = "SOLICITACAOMUDANCAITEM_FUNCAOAPF_"+sGXsfl_27_idx;
         edtSolicitacaoMudanca_SistemaCod_Internalname = "SOLICITACAOMUDANCA_SISTEMACOD_"+sGXsfl_27_idx;
         Solicitacaomudancaitem_descricao_Internalname = "SOLICITACAOMUDANCAITEM_DESCRICAO_"+sGXsfl_27_idx;
      }

      protected void SubsflControlProps_fel_272( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_27_fel_idx;
         edtSolicitacaoMudanca_Codigo_Internalname = "SOLICITACAOMUDANCA_CODIGO_"+sGXsfl_27_fel_idx;
         edtSolicitacaoMudancaItem_FuncaoAPF_Internalname = "SOLICITACAOMUDANCAITEM_FUNCAOAPF_"+sGXsfl_27_fel_idx;
         edtSolicitacaoMudanca_SistemaCod_Internalname = "SOLICITACAOMUDANCA_SISTEMACOD_"+sGXsfl_27_fel_idx;
         Solicitacaomudancaitem_descricao_Internalname = "SOLICITACAOMUDANCAITEM_DESCRICAO_"+sGXsfl_27_fel_idx;
      }

      protected void sendrow_272( )
      {
         SubsflControlProps_272( ) ;
         WBH40( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_27_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_27_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_27_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 28,'',false,'',27)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV11Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV11Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV30Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV11Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV11Select)) ? AV30Select_GXI : context.PathToRelativeUrl( AV11Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_27_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV11Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSolicitacaoMudanca_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A996SolicitacaoMudanca_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSolicitacaoMudanca_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSolicitacaoMudancaItem_FuncaoAPF_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSolicitacaoMudancaItem_FuncaoAPF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)46,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)27,(short)1,(short)0,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSolicitacaoMudanca_SistemaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A993SolicitacaoMudanca_SistemaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A993SolicitacaoMudanca_SistemaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSolicitacaoMudanca_SistemaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td>") ;
            }
            /* User Defined Control */
            GridRow.AddColumnProperties("usercontrol", -1, isAjaxCallMode( ), new Object[] {(String)"SOLICITACAOMUDANCAITEM_DESCRICAOContainer"+"_"+sGXsfl_27_idx,(short)1});
            GXCCtl = "SOLICITACAOMUDANCAITEM_DESCRICAO_" + sGXsfl_27_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, A998SolicitacaoMudancaItem_Descricao);
            GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACAOMUDANCA_CODIGO"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sGXsfl_27_idx, context.localUtil.Format( (decimal)(A996SolicitacaoMudanca_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACAOMUDANCAITEM_FUNCAOAPF"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sGXsfl_27_idx, context.localUtil.Format( (decimal)(A995SolicitacaoMudancaItem_FuncaoAPF), "ZZZZZ9")));
            GXCCtl = "SOLICITACAOMUDANCAITEM_DESCRICAO_Enabled_" + sGXsfl_27_idx;
            GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.BoolToStr( Solicitacaomudancaitem_descricao_Enabled));
            GridContainer.AddRow(GridRow);
            nGXsfl_27_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_27_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_27_idx+1));
            sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
            SubsflControlProps_272( ) ;
         }
         /* End function sendrow_272 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtSolicitacaoMudanca_Codigo_Internalname = "SOLICITACAOMUDANCA_CODIGO";
         edtSolicitacaoMudancaItem_FuncaoAPF_Internalname = "SOLICITACAOMUDANCAITEM_FUNCAOAPF";
         edtSolicitacaoMudanca_SistemaCod_Internalname = "SOLICITACAOMUDANCA_SISTEMACOD";
         Solicitacaomudancaitem_descricao_Internalname = "SOLICITACAOMUDANCAITEM_DESCRICAO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavTfsolicitacaomudanca_codigo_Internalname = "vTFSOLICITACAOMUDANCA_CODIGO";
         edtavTfsolicitacaomudanca_codigo_to_Internalname = "vTFSOLICITACAOMUDANCA_CODIGO_TO";
         edtavTfsolicitacaomudancaitem_funcaoapf_Internalname = "vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF";
         edtavTfsolicitacaomudancaitem_funcaoapf_to_Internalname = "vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO";
         edtavTfsolicitacaomudanca_sistemacod_Internalname = "vTFSOLICITACAOMUDANCA_SISTEMACOD";
         edtavTfsolicitacaomudanca_sistemacod_to_Internalname = "vTFSOLICITACAOMUDANCA_SISTEMACOD_TO";
         Ddo_solicitacaomudanca_codigo_Internalname = "DDO_SOLICITACAOMUDANCA_CODIGO";
         edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Internalname = "vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_solicitacaomudancaitem_funcaoapf_Internalname = "DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF";
         edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Internalname = "vDDO_SOLICITACAOMUDANCAITEM_FUNCAOAPFTITLECONTROLIDTOREPLACE";
         Ddo_solicitacaomudanca_sistemacod_Internalname = "DDO_SOLICITACAOMUDANCA_SISTEMACOD";
         edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Internalname = "vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Solicitacaomudancaitem_descricao_Enabled = Convert.ToBoolean( 0);
         edtSolicitacaoMudanca_SistemaCod_Jsonclick = "";
         edtSolicitacaoMudancaItem_FuncaoAPF_Jsonclick = "";
         edtSolicitacaoMudanca_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtSolicitacaoMudanca_SistemaCod_Titleformat = 0;
         edtSolicitacaoMudancaItem_FuncaoAPF_Titleformat = 0;
         edtSolicitacaoMudanca_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtSolicitacaoMudanca_SistemaCod_Title = "Sistema";
         edtSolicitacaoMudancaItem_FuncaoAPF_Title = "Item";
         edtSolicitacaoMudanca_Codigo_Title = "Id";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Visible = 1;
         edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfsolicitacaomudanca_sistemacod_to_Jsonclick = "";
         edtavTfsolicitacaomudanca_sistemacod_to_Visible = 1;
         edtavTfsolicitacaomudanca_sistemacod_Jsonclick = "";
         edtavTfsolicitacaomudanca_sistemacod_Visible = 1;
         edtavTfsolicitacaomudancaitem_funcaoapf_to_Jsonclick = "";
         edtavTfsolicitacaomudancaitem_funcaoapf_to_Visible = 1;
         edtavTfsolicitacaomudancaitem_funcaoapf_Jsonclick = "";
         edtavTfsolicitacaomudancaitem_funcaoapf_Visible = 1;
         edtavTfsolicitacaomudanca_codigo_to_Jsonclick = "";
         edtavTfsolicitacaomudanca_codigo_to_Visible = 1;
         edtavTfsolicitacaomudanca_codigo_Jsonclick = "";
         edtavTfsolicitacaomudanca_codigo_Visible = 1;
         Ddo_solicitacaomudanca_sistemacod_Searchbuttontext = "Pesquisar";
         Ddo_solicitacaomudanca_sistemacod_Rangefilterto = "At�";
         Ddo_solicitacaomudanca_sistemacod_Rangefilterfrom = "Desde";
         Ddo_solicitacaomudanca_sistemacod_Cleanfilter = "Limpar pesquisa";
         Ddo_solicitacaomudanca_sistemacod_Sortdsc = "Ordenar de Z � A";
         Ddo_solicitacaomudanca_sistemacod_Sortasc = "Ordenar de A � Z";
         Ddo_solicitacaomudanca_sistemacod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_solicitacaomudanca_sistemacod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_sistemacod_Filtertype = "Numeric";
         Ddo_solicitacaomudanca_sistemacod_Includefilter = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_sistemacod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_sistemacod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_sistemacod_Titlecontrolidtoreplace = "";
         Ddo_solicitacaomudanca_sistemacod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_solicitacaomudanca_sistemacod_Cls = "ColumnSettings";
         Ddo_solicitacaomudanca_sistemacod_Tooltip = "Op��es";
         Ddo_solicitacaomudanca_sistemacod_Caption = "";
         Ddo_solicitacaomudancaitem_funcaoapf_Searchbuttontext = "Pesquisar";
         Ddo_solicitacaomudancaitem_funcaoapf_Rangefilterto = "At�";
         Ddo_solicitacaomudancaitem_funcaoapf_Rangefilterfrom = "Desde";
         Ddo_solicitacaomudancaitem_funcaoapf_Cleanfilter = "Limpar pesquisa";
         Ddo_solicitacaomudancaitem_funcaoapf_Sortdsc = "Ordenar de Z � A";
         Ddo_solicitacaomudancaitem_funcaoapf_Sortasc = "Ordenar de A � Z";
         Ddo_solicitacaomudancaitem_funcaoapf_Includedatalist = Convert.ToBoolean( 0);
         Ddo_solicitacaomudancaitem_funcaoapf_Filterisrange = Convert.ToBoolean( -1);
         Ddo_solicitacaomudancaitem_funcaoapf_Filtertype = "Numeric";
         Ddo_solicitacaomudancaitem_funcaoapf_Includefilter = Convert.ToBoolean( -1);
         Ddo_solicitacaomudancaitem_funcaoapf_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_solicitacaomudancaitem_funcaoapf_Includesortasc = Convert.ToBoolean( -1);
         Ddo_solicitacaomudancaitem_funcaoapf_Titlecontrolidtoreplace = "";
         Ddo_solicitacaomudancaitem_funcaoapf_Dropdownoptionstype = "GridTitleSettings";
         Ddo_solicitacaomudancaitem_funcaoapf_Cls = "ColumnSettings";
         Ddo_solicitacaomudancaitem_funcaoapf_Tooltip = "Op��es";
         Ddo_solicitacaomudancaitem_funcaoapf_Caption = "";
         Ddo_solicitacaomudanca_codigo_Searchbuttontext = "Pesquisar";
         Ddo_solicitacaomudanca_codigo_Rangefilterto = "At�";
         Ddo_solicitacaomudanca_codigo_Rangefilterfrom = "Desde";
         Ddo_solicitacaomudanca_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_solicitacaomudanca_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_solicitacaomudanca_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_solicitacaomudanca_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_solicitacaomudanca_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_codigo_Filtertype = "Numeric";
         Ddo_solicitacaomudanca_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_solicitacaomudanca_codigo_Titlecontrolidtoreplace = "";
         Ddo_solicitacaomudanca_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_solicitacaomudanca_codigo_Cls = "ColumnSettings";
         Ddo_solicitacaomudanca_codigo_Tooltip = "Op��es";
         Ddo_solicitacaomudanca_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Item da Mudan�a";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV13TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV17TFSolicitacaoMudancaItem_FuncaoAPF',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0},{av:'AV18TFSolicitacaoMudancaItem_FuncaoAPF_To',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO',pic:'ZZZZZ9',nv:0},{av:'AV21TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV22TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCAITEM_FUNCAOAPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''}],oparms:[{av:'AV12SolicitacaoMudanca_CodigoTitleFilterData',fld:'vSOLICITACAOMUDANCA_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV16SolicitacaoMudancaItem_FuncaoAPFTitleFilterData',fld:'vSOLICITACAOMUDANCAITEM_FUNCAOAPFTITLEFILTERDATA',pic:'',nv:null},{av:'AV20SolicitacaoMudanca_SistemaCodTitleFilterData',fld:'vSOLICITACAOMUDANCA_SISTEMACODTITLEFILTERDATA',pic:'',nv:null},{av:'edtSolicitacaoMudanca_Codigo_Titleformat',ctrl:'SOLICITACAOMUDANCA_CODIGO',prop:'Titleformat'},{av:'edtSolicitacaoMudanca_Codigo_Title',ctrl:'SOLICITACAOMUDANCA_CODIGO',prop:'Title'},{av:'edtSolicitacaoMudancaItem_FuncaoAPF_Titleformat',ctrl:'SOLICITACAOMUDANCAITEM_FUNCAOAPF',prop:'Titleformat'},{av:'edtSolicitacaoMudancaItem_FuncaoAPF_Title',ctrl:'SOLICITACAOMUDANCAITEM_FUNCAOAPF',prop:'Title'},{av:'edtSolicitacaoMudanca_SistemaCod_Titleformat',ctrl:'SOLICITACAOMUDANCA_SISTEMACOD',prop:'Titleformat'},{av:'edtSolicitacaoMudanca_SistemaCod_Title',ctrl:'SOLICITACAOMUDANCA_SISTEMACOD',prop:'Title'},{av:'AV26GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV27GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11H42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV13TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV17TFSolicitacaoMudancaItem_FuncaoAPF',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0},{av:'AV18TFSolicitacaoMudancaItem_FuncaoAPF_To',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO',pic:'ZZZZZ9',nv:0},{av:'AV21TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV22TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCAITEM_FUNCAOAPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SOLICITACAOMUDANCA_CODIGO.ONOPTIONCLICKED","{handler:'E12H42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV13TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV17TFSolicitacaoMudancaItem_FuncaoAPF',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0},{av:'AV18TFSolicitacaoMudancaItem_FuncaoAPF_To',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO',pic:'ZZZZZ9',nv:0},{av:'AV21TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV22TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCAITEM_FUNCAOAPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_solicitacaomudanca_codigo_Activeeventkey',ctrl:'DDO_SOLICITACAOMUDANCA_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_solicitacaomudanca_codigo_Filteredtext_get',ctrl:'DDO_SOLICITACAOMUDANCA_CODIGO',prop:'FilteredText_get'},{av:'Ddo_solicitacaomudanca_codigo_Filteredtextto_get',ctrl:'DDO_SOLICITACAOMUDANCA_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_solicitacaomudanca_codigo_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_CODIGO',prop:'SortedStatus'},{av:'AV13TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF',prop:'SortedStatus'},{av:'Ddo_solicitacaomudanca_sistemacod_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_SISTEMACOD',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF.ONOPTIONCLICKED","{handler:'E13H42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV13TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV17TFSolicitacaoMudancaItem_FuncaoAPF',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0},{av:'AV18TFSolicitacaoMudancaItem_FuncaoAPF_To',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO',pic:'ZZZZZ9',nv:0},{av:'AV21TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV22TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCAITEM_FUNCAOAPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_solicitacaomudancaitem_funcaoapf_Activeeventkey',ctrl:'DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF',prop:'ActiveEventKey'},{av:'Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_get',ctrl:'DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF',prop:'FilteredText_get'},{av:'Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_get',ctrl:'DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF',prop:'FilteredTextTo_get'}],oparms:[{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF',prop:'SortedStatus'},{av:'AV17TFSolicitacaoMudancaItem_FuncaoAPF',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0},{av:'AV18TFSolicitacaoMudancaItem_FuncaoAPF_To',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudanca_codigo_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_CODIGO',prop:'SortedStatus'},{av:'Ddo_solicitacaomudanca_sistemacod_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_SISTEMACOD',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SOLICITACAOMUDANCA_SISTEMACOD.ONOPTIONCLICKED","{handler:'E14H42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV13TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV17TFSolicitacaoMudancaItem_FuncaoAPF',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0},{av:'AV18TFSolicitacaoMudancaItem_FuncaoAPF_To',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO',pic:'ZZZZZ9',nv:0},{av:'AV21TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV22TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCAITEM_FUNCAOAPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_solicitacaomudanca_sistemacod_Activeeventkey',ctrl:'DDO_SOLICITACAOMUDANCA_SISTEMACOD',prop:'ActiveEventKey'},{av:'Ddo_solicitacaomudanca_sistemacod_Filteredtext_get',ctrl:'DDO_SOLICITACAOMUDANCA_SISTEMACOD',prop:'FilteredText_get'},{av:'Ddo_solicitacaomudanca_sistemacod_Filteredtextto_get',ctrl:'DDO_SOLICITACAOMUDANCA_SISTEMACOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_solicitacaomudanca_sistemacod_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_SISTEMACOD',prop:'SortedStatus'},{av:'AV21TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV22TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudanca_codigo_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCA_CODIGO',prop:'SortedStatus'},{av:'Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus',ctrl:'DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E19H42',iparms:[],oparms:[{av:'AV11Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E20H42',iparms:[{av:'A996SolicitacaoMudanca_Codigo',fld:'SOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A995SolicitacaoMudancaItem_FuncaoAPF',fld:'SOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV7InOutSolicitacaoMudanca_Codigo',fld:'vINOUTSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutSolicitacaoMudancaItem_FuncaoAPF',fld:'vINOUTSOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15H42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV13TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV17TFSolicitacaoMudancaItem_FuncaoAPF',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0},{av:'AV18TFSolicitacaoMudancaItem_FuncaoAPF_To',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO',pic:'ZZZZZ9',nv:0},{av:'AV21TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV22TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCAITEM_FUNCAOAPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E16H42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV9OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV10OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV13TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV17TFSolicitacaoMudancaItem_FuncaoAPF',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0},{av:'AV18TFSolicitacaoMudancaItem_FuncaoAPF_To',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO',pic:'ZZZZZ9',nv:0},{av:'AV21TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV22TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCAITEM_FUNCAOAPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace',fld:'vDDO_SOLICITACAOMUDANCA_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''}],oparms:[{av:'AV13TFSolicitacaoMudanca_Codigo',fld:'vTFSOLICITACAOMUDANCA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudanca_codigo_Filteredtext_set',ctrl:'DDO_SOLICITACAOMUDANCA_CODIGO',prop:'FilteredText_set'},{av:'AV14TFSolicitacaoMudanca_Codigo_To',fld:'vTFSOLICITACAOMUDANCA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudanca_codigo_Filteredtextto_set',ctrl:'DDO_SOLICITACAOMUDANCA_CODIGO',prop:'FilteredTextTo_set'},{av:'AV17TFSolicitacaoMudancaItem_FuncaoAPF',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_set',ctrl:'DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF',prop:'FilteredText_set'},{av:'AV18TFSolicitacaoMudancaItem_FuncaoAPF_To',fld:'vTFSOLICITACAOMUDANCAITEM_FUNCAOAPF_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_set',ctrl:'DDO_SOLICITACAOMUDANCAITEM_FUNCAOAPF',prop:'FilteredTextTo_set'},{av:'AV21TFSolicitacaoMudanca_SistemaCod',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudanca_sistemacod_Filteredtext_set',ctrl:'DDO_SOLICITACAOMUDANCA_SISTEMACOD',prop:'FilteredText_set'},{av:'AV22TFSolicitacaoMudanca_SistemaCod_To',fld:'vTFSOLICITACAOMUDANCA_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacaomudanca_sistemacod_Filteredtextto_set',ctrl:'DDO_SOLICITACAOMUDANCA_SISTEMACOD',prop:'FilteredTextTo_set'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_solicitacaomudanca_codigo_Activeeventkey = "";
         Ddo_solicitacaomudanca_codigo_Filteredtext_get = "";
         Ddo_solicitacaomudanca_codigo_Filteredtextto_get = "";
         Ddo_solicitacaomudancaitem_funcaoapf_Activeeventkey = "";
         Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_get = "";
         Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_get = "";
         Ddo_solicitacaomudanca_sistemacod_Activeeventkey = "";
         Ddo_solicitacaomudanca_sistemacod_Filteredtext_get = "";
         Ddo_solicitacaomudanca_sistemacod_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace = "";
         AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace = "";
         AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV24DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV12SolicitacaoMudanca_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV16SolicitacaoMudancaItem_FuncaoAPFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV20SolicitacaoMudanca_SistemaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_solicitacaomudanca_codigo_Filteredtext_set = "";
         Ddo_solicitacaomudanca_codigo_Filteredtextto_set = "";
         Ddo_solicitacaomudanca_codigo_Sortedstatus = "";
         Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_set = "";
         Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_set = "";
         Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus = "";
         Ddo_solicitacaomudanca_sistemacod_Filteredtext_set = "";
         Ddo_solicitacaomudanca_sistemacod_Filteredtextto_set = "";
         Ddo_solicitacaomudanca_sistemacod_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         AV11Select = "";
         AV30Select_GXI = "";
         A998SolicitacaoMudancaItem_Descricao = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00H42_A998SolicitacaoMudancaItem_Descricao = new String[] {""} ;
         H00H42_A993SolicitacaoMudanca_SistemaCod = new int[1] ;
         H00H42_n993SolicitacaoMudanca_SistemaCod = new bool[] {false} ;
         H00H42_A995SolicitacaoMudancaItem_FuncaoAPF = new int[1] ;
         H00H42_A996SolicitacaoMudanca_Codigo = new int[1] ;
         H00H43_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptsolicitacaomudancaitem__default(),
            new Object[][] {
                new Object[] {
               H00H42_A998SolicitacaoMudancaItem_Descricao, H00H42_A993SolicitacaoMudanca_SistemaCod, H00H42_n993SolicitacaoMudanca_SistemaCod, H00H42_A995SolicitacaoMudancaItem_FuncaoAPF, H00H42_A996SolicitacaoMudanca_Codigo
               }
               , new Object[] {
               H00H43_AGRID_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_27 ;
      private short nGXsfl_27_idx=1 ;
      private short AV9OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_27_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtSolicitacaoMudanca_Codigo_Titleformat ;
      private short edtSolicitacaoMudancaItem_FuncaoAPF_Titleformat ;
      private short edtSolicitacaoMudanca_SistemaCod_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutSolicitacaoMudanca_Codigo ;
      private int AV8InOutSolicitacaoMudancaItem_FuncaoAPF ;
      private int wcpOAV7InOutSolicitacaoMudanca_Codigo ;
      private int wcpOAV8InOutSolicitacaoMudancaItem_FuncaoAPF ;
      private int subGrid_Rows ;
      private int AV13TFSolicitacaoMudanca_Codigo ;
      private int AV14TFSolicitacaoMudanca_Codigo_To ;
      private int AV17TFSolicitacaoMudancaItem_FuncaoAPF ;
      private int AV18TFSolicitacaoMudancaItem_FuncaoAPF_To ;
      private int AV21TFSolicitacaoMudanca_SistemaCod ;
      private int AV22TFSolicitacaoMudanca_SistemaCod_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtavTfsolicitacaomudanca_codigo_Visible ;
      private int edtavTfsolicitacaomudanca_codigo_to_Visible ;
      private int edtavTfsolicitacaomudancaitem_funcaoapf_Visible ;
      private int edtavTfsolicitacaomudancaitem_funcaoapf_to_Visible ;
      private int edtavTfsolicitacaomudanca_sistemacod_Visible ;
      private int edtavTfsolicitacaomudanca_sistemacod_to_Visible ;
      private int edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Visible ;
      private int edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Visible ;
      private int A996SolicitacaoMudanca_Codigo ;
      private int A995SolicitacaoMudancaItem_FuncaoAPF ;
      private int A993SolicitacaoMudanca_SistemaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV25PageToGo ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV26GridCurrentPage ;
      private long AV27GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_solicitacaomudanca_codigo_Activeeventkey ;
      private String Ddo_solicitacaomudanca_codigo_Filteredtext_get ;
      private String Ddo_solicitacaomudanca_codigo_Filteredtextto_get ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Activeeventkey ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_get ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_get ;
      private String Ddo_solicitacaomudanca_sistemacod_Activeeventkey ;
      private String Ddo_solicitacaomudanca_sistemacod_Filteredtext_get ;
      private String Ddo_solicitacaomudanca_sistemacod_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_27_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_solicitacaomudanca_codigo_Caption ;
      private String Ddo_solicitacaomudanca_codigo_Tooltip ;
      private String Ddo_solicitacaomudanca_codigo_Cls ;
      private String Ddo_solicitacaomudanca_codigo_Filteredtext_set ;
      private String Ddo_solicitacaomudanca_codigo_Filteredtextto_set ;
      private String Ddo_solicitacaomudanca_codigo_Dropdownoptionstype ;
      private String Ddo_solicitacaomudanca_codigo_Titlecontrolidtoreplace ;
      private String Ddo_solicitacaomudanca_codigo_Sortedstatus ;
      private String Ddo_solicitacaomudanca_codigo_Filtertype ;
      private String Ddo_solicitacaomudanca_codigo_Sortasc ;
      private String Ddo_solicitacaomudanca_codigo_Sortdsc ;
      private String Ddo_solicitacaomudanca_codigo_Cleanfilter ;
      private String Ddo_solicitacaomudanca_codigo_Rangefilterfrom ;
      private String Ddo_solicitacaomudanca_codigo_Rangefilterto ;
      private String Ddo_solicitacaomudanca_codigo_Searchbuttontext ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Caption ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Tooltip ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Cls ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Filteredtext_set ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Filteredtextto_set ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Dropdownoptionstype ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Titlecontrolidtoreplace ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Sortedstatus ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Filtertype ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Sortasc ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Sortdsc ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Cleanfilter ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Rangefilterfrom ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Rangefilterto ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Searchbuttontext ;
      private String Ddo_solicitacaomudanca_sistemacod_Caption ;
      private String Ddo_solicitacaomudanca_sistemacod_Tooltip ;
      private String Ddo_solicitacaomudanca_sistemacod_Cls ;
      private String Ddo_solicitacaomudanca_sistemacod_Filteredtext_set ;
      private String Ddo_solicitacaomudanca_sistemacod_Filteredtextto_set ;
      private String Ddo_solicitacaomudanca_sistemacod_Dropdownoptionstype ;
      private String Ddo_solicitacaomudanca_sistemacod_Titlecontrolidtoreplace ;
      private String Ddo_solicitacaomudanca_sistemacod_Sortedstatus ;
      private String Ddo_solicitacaomudanca_sistemacod_Filtertype ;
      private String Ddo_solicitacaomudanca_sistemacod_Sortasc ;
      private String Ddo_solicitacaomudanca_sistemacod_Sortdsc ;
      private String Ddo_solicitacaomudanca_sistemacod_Cleanfilter ;
      private String Ddo_solicitacaomudanca_sistemacod_Rangefilterfrom ;
      private String Ddo_solicitacaomudanca_sistemacod_Rangefilterto ;
      private String Ddo_solicitacaomudanca_sistemacod_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavTfsolicitacaomudanca_codigo_Internalname ;
      private String edtavTfsolicitacaomudanca_codigo_Jsonclick ;
      private String edtavTfsolicitacaomudanca_codigo_to_Internalname ;
      private String edtavTfsolicitacaomudanca_codigo_to_Jsonclick ;
      private String edtavTfsolicitacaomudancaitem_funcaoapf_Internalname ;
      private String edtavTfsolicitacaomudancaitem_funcaoapf_Jsonclick ;
      private String edtavTfsolicitacaomudancaitem_funcaoapf_to_Internalname ;
      private String edtavTfsolicitacaomudancaitem_funcaoapf_to_Jsonclick ;
      private String edtavTfsolicitacaomudanca_sistemacod_Internalname ;
      private String edtavTfsolicitacaomudanca_sistemacod_Jsonclick ;
      private String edtavTfsolicitacaomudanca_sistemacod_to_Internalname ;
      private String edtavTfsolicitacaomudanca_sistemacod_to_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_solicitacaomudanca_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_solicitacaomudancaitem_funcaoapftitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_solicitacaomudanca_sistemacodtitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String Solicitacaomudancaitem_descricao_Internalname ;
      private String edtavSelect_Internalname ;
      private String edtSolicitacaoMudanca_Codigo_Internalname ;
      private String edtSolicitacaoMudancaItem_FuncaoAPF_Internalname ;
      private String edtSolicitacaoMudanca_SistemaCod_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_solicitacaomudanca_codigo_Internalname ;
      private String Ddo_solicitacaomudancaitem_funcaoapf_Internalname ;
      private String Ddo_solicitacaomudanca_sistemacod_Internalname ;
      private String edtSolicitacaoMudanca_Codigo_Title ;
      private String edtSolicitacaoMudancaItem_FuncaoAPF_Title ;
      private String edtSolicitacaoMudanca_SistemaCod_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String sGXsfl_27_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtSolicitacaoMudanca_Codigo_Jsonclick ;
      private String edtSolicitacaoMudancaItem_FuncaoAPF_Jsonclick ;
      private String edtSolicitacaoMudanca_SistemaCod_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV10OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_solicitacaomudanca_codigo_Includesortasc ;
      private bool Ddo_solicitacaomudanca_codigo_Includesortdsc ;
      private bool Ddo_solicitacaomudanca_codigo_Includefilter ;
      private bool Ddo_solicitacaomudanca_codigo_Filterisrange ;
      private bool Ddo_solicitacaomudanca_codigo_Includedatalist ;
      private bool Ddo_solicitacaomudancaitem_funcaoapf_Includesortasc ;
      private bool Ddo_solicitacaomudancaitem_funcaoapf_Includesortdsc ;
      private bool Ddo_solicitacaomudancaitem_funcaoapf_Includefilter ;
      private bool Ddo_solicitacaomudancaitem_funcaoapf_Filterisrange ;
      private bool Ddo_solicitacaomudancaitem_funcaoapf_Includedatalist ;
      private bool Ddo_solicitacaomudanca_sistemacod_Includesortasc ;
      private bool Ddo_solicitacaomudanca_sistemacod_Includesortdsc ;
      private bool Ddo_solicitacaomudanca_sistemacod_Includefilter ;
      private bool Ddo_solicitacaomudanca_sistemacod_Filterisrange ;
      private bool Ddo_solicitacaomudanca_sistemacod_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool Solicitacaomudancaitem_descricao_Enabled ;
      private bool n993SolicitacaoMudanca_SistemaCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV11Select_IsBlob ;
      private String A998SolicitacaoMudancaItem_Descricao ;
      private String AV15ddo_SolicitacaoMudanca_CodigoTitleControlIdToReplace ;
      private String AV19ddo_SolicitacaoMudancaItem_FuncaoAPFTitleControlIdToReplace ;
      private String AV23ddo_SolicitacaoMudanca_SistemaCodTitleControlIdToReplace ;
      private String AV30Select_GXI ;
      private String AV11Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutSolicitacaoMudanca_Codigo ;
      private int aP1_InOutSolicitacaoMudancaItem_FuncaoAPF ;
      private GXCombobox cmbavOrderedby ;
      private IDataStoreProvider pr_default ;
      private String[] H00H42_A998SolicitacaoMudancaItem_Descricao ;
      private int[] H00H42_A993SolicitacaoMudanca_SistemaCod ;
      private bool[] H00H42_n993SolicitacaoMudanca_SistemaCod ;
      private int[] H00H42_A995SolicitacaoMudancaItem_FuncaoAPF ;
      private int[] H00H42_A996SolicitacaoMudanca_Codigo ;
      private long[] H00H43_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV12SolicitacaoMudanca_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV16SolicitacaoMudancaItem_FuncaoAPFTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV20SolicitacaoMudanca_SistemaCodTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV24DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptsolicitacaomudancaitem__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00H42( IGxContext context ,
                                             int AV13TFSolicitacaoMudanca_Codigo ,
                                             int AV14TFSolicitacaoMudanca_Codigo_To ,
                                             int AV17TFSolicitacaoMudancaItem_FuncaoAPF ,
                                             int AV18TFSolicitacaoMudancaItem_FuncaoAPF_To ,
                                             int AV21TFSolicitacaoMudanca_SistemaCod ,
                                             int AV22TFSolicitacaoMudanca_SistemaCod_To ,
                                             int A996SolicitacaoMudanca_Codigo ,
                                             int A995SolicitacaoMudancaItem_FuncaoAPF ,
                                             int A993SolicitacaoMudanca_SistemaCod ,
                                             short AV9OrderedBy ,
                                             bool AV10OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [11] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[SolicitacaoMudancaItem_Descricao], T2.[SolicitacaoMudanca_SistemaCod], T1.[SolicitacaoMudancaItem_FuncaoAPF], T1.[SolicitacaoMudanca_Codigo]";
         sFromString = " FROM ([SolicitacaoMudancaItem] T1 WITH (NOLOCK) INNER JOIN [SolicitacaoMudanca] T2 WITH (NOLOCK) ON T2.[SolicitacaoMudanca_Codigo] = T1.[SolicitacaoMudanca_Codigo])";
         sOrderString = "";
         if ( ! (0==AV13TFSolicitacaoMudanca_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[SolicitacaoMudanca_Codigo] >= @AV13TFSolicitacaoMudanca_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[SolicitacaoMudanca_Codigo] >= @AV13TFSolicitacaoMudanca_Codigo)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ! (0==AV14TFSolicitacaoMudanca_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[SolicitacaoMudanca_Codigo] <= @AV14TFSolicitacaoMudanca_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[SolicitacaoMudanca_Codigo] <= @AV14TFSolicitacaoMudanca_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (0==AV17TFSolicitacaoMudancaItem_FuncaoAPF) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[SolicitacaoMudancaItem_FuncaoAPF] >= @AV17TFSolicitacaoMudancaItem_FuncaoAPF)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[SolicitacaoMudancaItem_FuncaoAPF] >= @AV17TFSolicitacaoMudancaItem_FuncaoAPF)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (0==AV18TFSolicitacaoMudancaItem_FuncaoAPF_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[SolicitacaoMudancaItem_FuncaoAPF] <= @AV18TFSolicitacaoMudancaItem_FuncaoAPF_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[SolicitacaoMudancaItem_FuncaoAPF] <= @AV18TFSolicitacaoMudancaItem_FuncaoAPF_To)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (0==AV21TFSolicitacaoMudanca_SistemaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SolicitacaoMudanca_SistemaCod] >= @AV21TFSolicitacaoMudanca_SistemaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SolicitacaoMudanca_SistemaCod] >= @AV21TFSolicitacaoMudanca_SistemaCod)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! (0==AV22TFSolicitacaoMudanca_SistemaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SolicitacaoMudanca_SistemaCod] <= @AV22TFSolicitacaoMudanca_SistemaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SolicitacaoMudanca_SistemaCod] <= @AV22TFSolicitacaoMudanca_SistemaCod_To)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV9OrderedBy == 1 ) && ! AV10OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[SolicitacaoMudanca_Codigo]";
         }
         else if ( ( AV9OrderedBy == 1 ) && ( AV10OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[SolicitacaoMudanca_Codigo] DESC";
         }
         else if ( ( AV9OrderedBy == 2 ) && ! AV10OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[SolicitacaoMudancaItem_FuncaoAPF]";
         }
         else if ( ( AV9OrderedBy == 2 ) && ( AV10OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[SolicitacaoMudancaItem_FuncaoAPF] DESC";
         }
         else if ( ( AV9OrderedBy == 3 ) && ! AV10OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[SolicitacaoMudanca_SistemaCod]";
         }
         else if ( ( AV9OrderedBy == 3 ) && ( AV10OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[SolicitacaoMudanca_SistemaCod] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[SolicitacaoMudanca_Codigo], T1.[SolicitacaoMudancaItem_FuncaoAPF]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00H43( IGxContext context ,
                                             int AV13TFSolicitacaoMudanca_Codigo ,
                                             int AV14TFSolicitacaoMudanca_Codigo_To ,
                                             int AV17TFSolicitacaoMudancaItem_FuncaoAPF ,
                                             int AV18TFSolicitacaoMudancaItem_FuncaoAPF_To ,
                                             int AV21TFSolicitacaoMudanca_SistemaCod ,
                                             int AV22TFSolicitacaoMudanca_SistemaCod_To ,
                                             int A996SolicitacaoMudanca_Codigo ,
                                             int A995SolicitacaoMudancaItem_FuncaoAPF ,
                                             int A993SolicitacaoMudanca_SistemaCod ,
                                             short AV9OrderedBy ,
                                             bool AV10OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [6] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([SolicitacaoMudancaItem] T1 WITH (NOLOCK) INNER JOIN [SolicitacaoMudanca] T2 WITH (NOLOCK) ON T2.[SolicitacaoMudanca_Codigo] = T1.[SolicitacaoMudanca_Codigo])";
         if ( ! (0==AV13TFSolicitacaoMudanca_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[SolicitacaoMudanca_Codigo] >= @AV13TFSolicitacaoMudanca_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[SolicitacaoMudanca_Codigo] >= @AV13TFSolicitacaoMudanca_Codigo)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ! (0==AV14TFSolicitacaoMudanca_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[SolicitacaoMudanca_Codigo] <= @AV14TFSolicitacaoMudanca_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[SolicitacaoMudanca_Codigo] <= @AV14TFSolicitacaoMudanca_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (0==AV17TFSolicitacaoMudancaItem_FuncaoAPF) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[SolicitacaoMudancaItem_FuncaoAPF] >= @AV17TFSolicitacaoMudancaItem_FuncaoAPF)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[SolicitacaoMudancaItem_FuncaoAPF] >= @AV17TFSolicitacaoMudancaItem_FuncaoAPF)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (0==AV18TFSolicitacaoMudancaItem_FuncaoAPF_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[SolicitacaoMudancaItem_FuncaoAPF] <= @AV18TFSolicitacaoMudancaItem_FuncaoAPF_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[SolicitacaoMudancaItem_FuncaoAPF] <= @AV18TFSolicitacaoMudancaItem_FuncaoAPF_To)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (0==AV21TFSolicitacaoMudanca_SistemaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SolicitacaoMudanca_SistemaCod] >= @AV21TFSolicitacaoMudanca_SistemaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SolicitacaoMudanca_SistemaCod] >= @AV21TFSolicitacaoMudanca_SistemaCod)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! (0==AV22TFSolicitacaoMudanca_SistemaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[SolicitacaoMudanca_SistemaCod] <= @AV22TFSolicitacaoMudanca_SistemaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[SolicitacaoMudanca_SistemaCod] <= @AV22TFSolicitacaoMudanca_SistemaCod_To)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV9OrderedBy == 1 ) && ! AV10OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV9OrderedBy == 1 ) && ( AV10OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV9OrderedBy == 2 ) && ! AV10OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV9OrderedBy == 2 ) && ( AV10OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV9OrderedBy == 3 ) && ! AV10OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV9OrderedBy == 3 ) && ( AV10OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00H42(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] );
               case 1 :
                     return conditional_H00H43(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00H42 ;
          prmH00H42 = new Object[] {
          new Object[] {"@AV13TFSolicitacaoMudanca_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFSolicitacaoMudanca_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFSolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFSolicitacaoMudancaItem_FuncaoAPF_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFSolicitacaoMudanca_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22TFSolicitacaoMudanca_SistemaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00H43 ;
          prmH00H43 = new Object[] {
          new Object[] {"@AV13TFSolicitacaoMudanca_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFSolicitacaoMudanca_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFSolicitacaoMudancaItem_FuncaoAPF",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFSolicitacaoMudancaItem_FuncaoAPF_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFSolicitacaoMudanca_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22TFSolicitacaoMudanca_SistemaCod_To",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00H42", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H42,11,0,true,false )
             ,new CursorDef("H00H43", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H43,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                return;
       }
    }

 }

}
