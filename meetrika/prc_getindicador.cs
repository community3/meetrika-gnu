/*
               File: PRC_GetIndicador
        Description: Get Indicador
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:37.82
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_getindicador : GXProcedure
   {
      public prc_getindicador( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_getindicador( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicosIndicador_CntSrvCod ,
                           ref String aP1_ContratoServicosIndicador_Tipo ,
                           out String aP2_Indicador_Sigla ,
                           out String aP3_CAlculoSob ,
                           out int aP4_Indicador )
      {
         this.A1270ContratoServicosIndicador_CntSrvCod = aP0_ContratoServicosIndicador_CntSrvCod;
         this.A1308ContratoServicosIndicador_Tipo = aP1_ContratoServicosIndicador_Tipo;
         this.AV9Indicador_Sigla = "" ;
         this.AV10CAlculoSob = "" ;
         this.AV8Indicador = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicosIndicador_CntSrvCod=this.A1270ContratoServicosIndicador_CntSrvCod;
         aP1_ContratoServicosIndicador_Tipo=this.A1308ContratoServicosIndicador_Tipo;
         aP2_Indicador_Sigla=this.AV9Indicador_Sigla;
         aP3_CAlculoSob=this.AV10CAlculoSob;
         aP4_Indicador=this.AV8Indicador;
      }

      public int executeUdp( ref int aP0_ContratoServicosIndicador_CntSrvCod ,
                             ref String aP1_ContratoServicosIndicador_Tipo ,
                             out String aP2_Indicador_Sigla ,
                             out String aP3_CAlculoSob )
      {
         this.A1270ContratoServicosIndicador_CntSrvCod = aP0_ContratoServicosIndicador_CntSrvCod;
         this.A1308ContratoServicosIndicador_Tipo = aP1_ContratoServicosIndicador_Tipo;
         this.AV9Indicador_Sigla = "" ;
         this.AV10CAlculoSob = "" ;
         this.AV8Indicador = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicosIndicador_CntSrvCod=this.A1270ContratoServicosIndicador_CntSrvCod;
         aP1_ContratoServicosIndicador_Tipo=this.A1308ContratoServicosIndicador_Tipo;
         aP2_Indicador_Sigla=this.AV9Indicador_Sigla;
         aP3_CAlculoSob=this.AV10CAlculoSob;
         aP4_Indicador=this.AV8Indicador;
         return AV8Indicador ;
      }

      public void executeSubmit( ref int aP0_ContratoServicosIndicador_CntSrvCod ,
                                 ref String aP1_ContratoServicosIndicador_Tipo ,
                                 out String aP2_Indicador_Sigla ,
                                 out String aP3_CAlculoSob ,
                                 out int aP4_Indicador )
      {
         prc_getindicador objprc_getindicador;
         objprc_getindicador = new prc_getindicador();
         objprc_getindicador.A1270ContratoServicosIndicador_CntSrvCod = aP0_ContratoServicosIndicador_CntSrvCod;
         objprc_getindicador.A1308ContratoServicosIndicador_Tipo = aP1_ContratoServicosIndicador_Tipo;
         objprc_getindicador.AV9Indicador_Sigla = "" ;
         objprc_getindicador.AV10CAlculoSob = "" ;
         objprc_getindicador.AV8Indicador = 0 ;
         objprc_getindicador.context.SetSubmitInitialConfig(context);
         objprc_getindicador.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_getindicador);
         aP0_ContratoServicosIndicador_CntSrvCod=this.A1270ContratoServicosIndicador_CntSrvCod;
         aP1_ContratoServicosIndicador_Tipo=this.A1308ContratoServicosIndicador_Tipo;
         aP2_Indicador_Sigla=this.AV9Indicador_Sigla;
         aP3_CAlculoSob=this.AV10CAlculoSob;
         aP4_Indicador=this.AV8Indicador;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_getindicador)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00WV2 */
         pr_default.execute(0, new Object[] {A1270ContratoServicosIndicador_CntSrvCod, n1308ContratoServicosIndicador_Tipo, A1308ContratoServicosIndicador_Tipo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1269ContratoServicosIndicador_Codigo = P00WV2_A1269ContratoServicosIndicador_Codigo[0];
            A2051ContratoServicosIndicador_Sigla = P00WV2_A2051ContratoServicosIndicador_Sigla[0];
            n2051ContratoServicosIndicador_Sigla = P00WV2_n2051ContratoServicosIndicador_Sigla[0];
            A1345ContratoServicosIndicador_CalculoSob = P00WV2_A1345ContratoServicosIndicador_CalculoSob[0];
            n1345ContratoServicosIndicador_CalculoSob = P00WV2_n1345ContratoServicosIndicador_CalculoSob[0];
            AV8Indicador = A1269ContratoServicosIndicador_Codigo;
            AV9Indicador_Sigla = StringUtil.Trim( A2051ContratoServicosIndicador_Sigla);
            AV10CAlculoSob = A1345ContratoServicosIndicador_CalculoSob;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00WV2_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         P00WV2_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         P00WV2_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         P00WV2_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P00WV2_A2051ContratoServicosIndicador_Sigla = new String[] {""} ;
         P00WV2_n2051ContratoServicosIndicador_Sigla = new bool[] {false} ;
         P00WV2_A1345ContratoServicosIndicador_CalculoSob = new String[] {""} ;
         P00WV2_n1345ContratoServicosIndicador_CalculoSob = new bool[] {false} ;
         A2051ContratoServicosIndicador_Sigla = "";
         A1345ContratoServicosIndicador_CalculoSob = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_getindicador__default(),
            new Object[][] {
                new Object[] {
               P00WV2_A1270ContratoServicosIndicador_CntSrvCod, P00WV2_A1308ContratoServicosIndicador_Tipo, P00WV2_n1308ContratoServicosIndicador_Tipo, P00WV2_A1269ContratoServicosIndicador_Codigo, P00WV2_A2051ContratoServicosIndicador_Sigla, P00WV2_n2051ContratoServicosIndicador_Sigla, P00WV2_A1345ContratoServicosIndicador_CalculoSob, P00WV2_n1345ContratoServicosIndicador_CalculoSob
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1270ContratoServicosIndicador_CntSrvCod ;
      private int AV8Indicador ;
      private int A1269ContratoServicosIndicador_Codigo ;
      private String A1308ContratoServicosIndicador_Tipo ;
      private String scmdbuf ;
      private String A2051ContratoServicosIndicador_Sigla ;
      private String A1345ContratoServicosIndicador_CalculoSob ;
      private String AV9Indicador_Sigla ;
      private String AV10CAlculoSob ;
      private bool n1308ContratoServicosIndicador_Tipo ;
      private bool n2051ContratoServicosIndicador_Sigla ;
      private bool n1345ContratoServicosIndicador_CalculoSob ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicosIndicador_CntSrvCod ;
      private String aP1_ContratoServicosIndicador_Tipo ;
      private IDataStoreProvider pr_default ;
      private int[] P00WV2_A1270ContratoServicosIndicador_CntSrvCod ;
      private String[] P00WV2_A1308ContratoServicosIndicador_Tipo ;
      private bool[] P00WV2_n1308ContratoServicosIndicador_Tipo ;
      private int[] P00WV2_A1269ContratoServicosIndicador_Codigo ;
      private String[] P00WV2_A2051ContratoServicosIndicador_Sigla ;
      private bool[] P00WV2_n2051ContratoServicosIndicador_Sigla ;
      private String[] P00WV2_A1345ContratoServicosIndicador_CalculoSob ;
      private bool[] P00WV2_n1345ContratoServicosIndicador_CalculoSob ;
      private String aP2_Indicador_Sigla ;
      private String aP3_CAlculoSob ;
      private int aP4_Indicador ;
   }

   public class prc_getindicador__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WV2 ;
          prmP00WV2 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosIndicador_Tipo",SqlDbType.Char,2,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WV2", "SELECT TOP 1 [ContratoServicosIndicador_CntSrvCod], [ContratoServicosIndicador_Tipo], [ContratoServicosIndicador_Codigo], [ContratoServicosIndicador_Sigla], [ContratoServicosIndicador_CalculoSob] FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE ([ContratoServicosIndicador_CntSrvCod] = @ContratoServicosIndicador_CntSrvCod) AND ([ContratoServicosIndicador_Tipo] = @ContratoServicosIndicador_Tipo) ORDER BY [ContratoServicosIndicador_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WV2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                return;
       }
    }

 }

}
