/*
               File: PRC_AtrasoDaOS
        Description: Atraso Da OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:22.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_atrasodaos : GXProcedure
   {
      public prc_atrasodaos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_atrasodaos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultadoExecucao_OSCod ,
                           out short aP1_Atraso )
      {
         this.A1404ContagemResultadoExecucao_OSCod = aP0_ContagemResultadoExecucao_OSCod;
         this.AV8Atraso = 0 ;
         initialize();
         executePrivate();
         aP0_ContagemResultadoExecucao_OSCod=this.A1404ContagemResultadoExecucao_OSCod;
         aP1_Atraso=this.AV8Atraso;
      }

      public short executeUdp( ref int aP0_ContagemResultadoExecucao_OSCod )
      {
         this.A1404ContagemResultadoExecucao_OSCod = aP0_ContagemResultadoExecucao_OSCod;
         this.AV8Atraso = 0 ;
         initialize();
         executePrivate();
         aP0_ContagemResultadoExecucao_OSCod=this.A1404ContagemResultadoExecucao_OSCod;
         aP1_Atraso=this.AV8Atraso;
         return AV8Atraso ;
      }

      public void executeSubmit( ref int aP0_ContagemResultadoExecucao_OSCod ,
                                 out short aP1_Atraso )
      {
         prc_atrasodaos objprc_atrasodaos;
         objprc_atrasodaos = new prc_atrasodaos();
         objprc_atrasodaos.A1404ContagemResultadoExecucao_OSCod = aP0_ContagemResultadoExecucao_OSCod;
         objprc_atrasodaos.AV8Atraso = 0 ;
         objprc_atrasodaos.context.SetSubmitInitialConfig(context);
         objprc_atrasodaos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_atrasodaos);
         aP0_ContagemResultadoExecucao_OSCod=this.A1404ContagemResultadoExecucao_OSCod;
         aP1_Atraso=this.AV8Atraso;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_atrasodaos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized group. */
         /* Using cursor P00WC2 */
         pr_default.execute(0, new Object[] {A1404ContagemResultadoExecucao_OSCod});
         c1410ContagemResultadoExecucao_Dias = P00WC2_A1410ContagemResultadoExecucao_Dias[0];
         n1410ContagemResultadoExecucao_Dias = P00WC2_n1410ContagemResultadoExecucao_Dias[0];
         pr_default.close(0);
         AV8Atraso = (short)(AV8Atraso+c1410ContagemResultadoExecucao_Dias);
         /* End optimized group. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00WC2_A1410ContagemResultadoExecucao_Dias = new short[1] ;
         P00WC2_n1410ContagemResultadoExecucao_Dias = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_atrasodaos__default(),
            new Object[][] {
                new Object[] {
               P00WC2_A1410ContagemResultadoExecucao_Dias, P00WC2_n1410ContagemResultadoExecucao_Dias
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8Atraso ;
      private short c1410ContagemResultadoExecucao_Dias ;
      private int A1404ContagemResultadoExecucao_OSCod ;
      private String scmdbuf ;
      private bool n1410ContagemResultadoExecucao_Dias ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultadoExecucao_OSCod ;
      private IDataStoreProvider pr_default ;
      private short[] P00WC2_A1410ContagemResultadoExecucao_Dias ;
      private bool[] P00WC2_n1410ContagemResultadoExecucao_Dias ;
      private short aP1_Atraso ;
   }

   public class prc_atrasodaos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WC2 ;
          prmP00WC2 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_OSCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WC2", "SELECT SUM([ContagemResultadoExecucao_Dias]) FROM [ContagemResultadoExecucao] WITH (NOLOCK) WHERE ([ContagemResultadoExecucao_OSCod] = @ContagemResultadoExecucao_OSCod) AND ([ContagemResultadoExecucao_Dias] - [ContagemResultadoExecucao_PrazoDias] > 0) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WC2,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
