/*
               File: GxOnPendingEventFailed
        Description: Gx On Pending Event Failed
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:50.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxonpendingeventfailed : GXProcedure
   {
      public gxonpendingeventfailed( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public gxonpendingeventfailed( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( SdtGxSynchroEventSDT_GxSynchroEventSDTItem aP0_PendingEvent ,
                           String aP1_BCName ,
                           String aP2_BCJson ,
                           SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem aP3_EventResult ,
                           SdtGxSynchroInfoSDT aP4_GxSyncroInfo ,
                           out bool aP5_Continue )
      {
         this.AV8PendingEvent = aP0_PendingEvent;
         this.AV9BCName = aP1_BCName;
         this.AV10BCJson = aP2_BCJson;
         this.AV12EventResult = aP3_EventResult;
         this.GxSyncroInfo = aP4_GxSyncroInfo;
         this.AV11Continue = false ;
         initialize();
         executePrivate();
         aP5_Continue=this.AV11Continue;
      }

      public bool executeUdp( SdtGxSynchroEventSDT_GxSynchroEventSDTItem aP0_PendingEvent ,
                              String aP1_BCName ,
                              String aP2_BCJson ,
                              SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem aP3_EventResult ,
                              SdtGxSynchroInfoSDT aP4_GxSyncroInfo )
      {
         this.AV8PendingEvent = aP0_PendingEvent;
         this.AV9BCName = aP1_BCName;
         this.AV10BCJson = aP2_BCJson;
         this.AV12EventResult = aP3_EventResult;
         this.GxSyncroInfo = aP4_GxSyncroInfo;
         this.AV11Continue = false ;
         initialize();
         executePrivate();
         aP5_Continue=this.AV11Continue;
         return AV11Continue ;
      }

      public void executeSubmit( SdtGxSynchroEventSDT_GxSynchroEventSDTItem aP0_PendingEvent ,
                                 String aP1_BCName ,
                                 String aP2_BCJson ,
                                 SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem aP3_EventResult ,
                                 SdtGxSynchroInfoSDT aP4_GxSyncroInfo ,
                                 out bool aP5_Continue )
      {
         gxonpendingeventfailed objgxonpendingeventfailed;
         objgxonpendingeventfailed = new gxonpendingeventfailed();
         objgxonpendingeventfailed.AV8PendingEvent = aP0_PendingEvent;
         objgxonpendingeventfailed.AV9BCName = aP1_BCName;
         objgxonpendingeventfailed.AV10BCJson = aP2_BCJson;
         objgxonpendingeventfailed.AV12EventResult = aP3_EventResult;
         objgxonpendingeventfailed.GxSyncroInfo = aP4_GxSyncroInfo;
         objgxonpendingeventfailed.AV11Continue = false ;
         objgxonpendingeventfailed.context.SetSubmitInitialConfig(context);
         objgxonpendingeventfailed.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgxonpendingeventfailed);
         aP5_Continue=this.AV11Continue;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((gxonpendingeventfailed)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV11Continue = true;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private bool AV11Continue ;
      private String AV10BCJson ;
      private String AV9BCName ;
      private bool aP5_Continue ;
      private SdtGxSynchroEventSDT_GxSynchroEventSDTItem AV8PendingEvent ;
      private SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem AV12EventResult ;
      private SdtGxSynchroInfoSDT GxSyncroInfo ;
   }

}
