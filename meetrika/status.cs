/*
               File: Status
        Description: Status
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:20:43.81
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class status : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A421Status_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A421Status_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A421Status_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A421Status_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Status_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Status_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Status_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSTATUS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Status_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbStatus_Tipo.Name = "STATUS_TIPO";
         cmbStatus_Tipo.WebTags = "";
         cmbStatus_Tipo.addItem("1", "Demandas", 0);
         cmbStatus_Tipo.addItem("2", "Contagens", 0);
         cmbStatus_Tipo.addItem("3", "Itens da Contagem", 0);
         cmbStatus_Tipo.addItem("4", "Sistemas", 0);
         if ( cmbStatus_Tipo.ItemCount > 0 )
         {
            A425Status_Tipo = (short)(NumberUtil.Val( cmbStatus_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A425Status_Tipo), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A425Status_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A425Status_Tipo), 2, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Status", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtStatus_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public status( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public status( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Status_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Status_Codigo = aP1_Status_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbStatus_Tipo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbStatus_Tipo.ItemCount > 0 )
         {
            A425Status_Tipo = (short)(NumberUtil.Val( cmbStatus_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A425Status_Tipo), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A425Status_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A425Status_Tipo), 2, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1P64( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1P64e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtStatus_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A423Status_Codigo), 6, 0, ",", "")), ((edtStatus_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A423Status_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A423Status_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtStatus_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtStatus_Codigo_Visible, edtStatus_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Status.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1P64( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1P64( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1P64e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_26_1P64( true) ;
         }
         return  ;
      }

      protected void wb_table3_26_1P64e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1P64e( true) ;
         }
         else
         {
            wb_table1_2_1P64e( false) ;
         }
      }

      protected void wb_table3_26_1P64( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Status.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Status.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Status.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_26_1P64e( true) ;
         }
         else
         {
            wb_table3_26_1P64e( false) ;
         }
      }

      protected void wb_table2_5_1P64( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_1P64( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_1P64e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1P64e( true) ;
         }
         else
         {
            wb_table2_5_1P64e( false) ;
         }
      }

      protected void wb_table4_13_1P64( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockstatus_nome_Internalname, "Nome", "", "", lblTextblockstatus_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Status.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtStatus_Nome_Internalname, StringUtil.RTrim( A424Status_Nome), StringUtil.RTrim( context.localUtil.Format( A424Status_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtStatus_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtStatus_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Status.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockstatus_tipo_Internalname, "Tipo", "", "", lblTextblockstatus_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Status.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbStatus_Tipo, cmbStatus_Tipo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A425Status_Tipo), 2, 0)), 1, cmbStatus_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbStatus_Tipo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "", true, "HLP_Status.htm");
            cmbStatus_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A425Status_Tipo), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbStatus_Tipo_Internalname, "Values", (String)(cmbStatus_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_1P64e( true) ;
         }
         else
         {
            wb_table4_13_1P64e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E111P2 */
         E111P2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A424Status_Nome = StringUtil.Upper( cgiGet( edtStatus_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A424Status_Nome", A424Status_Nome);
               cmbStatus_Tipo.CurrentValue = cgiGet( cmbStatus_Tipo_Internalname);
               A425Status_Tipo = (short)(NumberUtil.Val( cgiGet( cmbStatus_Tipo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A425Status_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A425Status_Tipo), 2, 0)));
               A423Status_Codigo = (int)(context.localUtil.CToN( cgiGet( edtStatus_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A423Status_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A423Status_Codigo), 6, 0)));
               /* Read saved values. */
               Z423Status_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z423Status_Codigo"), ",", "."));
               Z424Status_Nome = cgiGet( "Z424Status_Nome");
               Z425Status_Tipo = (short)(context.localUtil.CToN( cgiGet( "Z425Status_Tipo"), ",", "."));
               Z421Status_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z421Status_AreaTrabalhoCod"), ",", "."));
               A421Status_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z421Status_AreaTrabalhoCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N421Status_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "N421Status_AreaTrabalhoCod"), ",", "."));
               AV7Status_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSTATUS_CODIGO"), ",", "."));
               AV11Insert_Status_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_STATUS_AREATRABALHOCOD"), ",", "."));
               A421Status_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "STATUS_AREATRABALHOCOD"), ",", "."));
               A422Status_AreaTrabalhoDes = cgiGet( "STATUS_AREATRABALHODES");
               n422Status_AreaTrabalhoDes = false;
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Status";
               A423Status_Codigo = (int)(context.localUtil.CToN( cgiGet( edtStatus_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A423Status_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A423Status_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A423Status_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A421Status_AreaTrabalhoCod), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A423Status_Codigo != Z423Status_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("status:[SecurityCheckFailed value for]"+"Status_Codigo:"+context.localUtil.Format( (decimal)(A423Status_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("status:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("status:[SecurityCheckFailed value for]"+"Status_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A421Status_AreaTrabalhoCod), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A423Status_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A423Status_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A423Status_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode64 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode64;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound64 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_1P0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "STATUS_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtStatus_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E111P2 */
                           E111P2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E121P2 */
                           E121P2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E121P2 */
            E121P2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1P64( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1P64( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_1P0( )
      {
         BeforeValidate1P64( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1P64( ) ;
            }
            else
            {
               CheckExtendedTable1P64( ) ;
               CloseExtendedTableCursors1P64( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption1P0( )
      {
      }

      protected void E111P2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Status_AreaTrabalhoCod") == 0 )
               {
                  AV11Insert_Status_AreaTrabalhoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Status_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Status_AreaTrabalhoCod), 6, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
         edtStatus_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtStatus_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtStatus_Codigo_Visible), 5, 0)));
      }

      protected void E121P2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwstatus.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM1P64( short GX_JID )
      {
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z424Status_Nome = T001P3_A424Status_Nome[0];
               Z425Status_Tipo = T001P3_A425Status_Tipo[0];
               Z421Status_AreaTrabalhoCod = T001P3_A421Status_AreaTrabalhoCod[0];
            }
            else
            {
               Z424Status_Nome = A424Status_Nome;
               Z425Status_Tipo = A425Status_Tipo;
               Z421Status_AreaTrabalhoCod = A421Status_AreaTrabalhoCod;
            }
         }
         if ( GX_JID == -10 )
         {
            Z423Status_Codigo = A423Status_Codigo;
            Z424Status_Nome = A424Status_Nome;
            Z425Status_Tipo = A425Status_Tipo;
            Z421Status_AreaTrabalhoCod = A421Status_AreaTrabalhoCod;
            Z422Status_AreaTrabalhoDes = A422Status_AreaTrabalhoDes;
         }
      }

      protected void standaloneNotModal( )
      {
         edtStatus_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtStatus_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtStatus_Codigo_Enabled), 5, 0)));
         AV13Pgmname = "Status";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         edtStatus_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtStatus_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtStatus_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Status_Codigo) )
         {
            A423Status_Codigo = AV7Status_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A423Status_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A423Status_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Status_AreaTrabalhoCod) )
         {
            A421Status_AreaTrabalhoCod = AV11Insert_Status_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A421Status_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A421Status_AreaTrabalhoCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T001P4 */
            pr_default.execute(2, new Object[] {A421Status_AreaTrabalhoCod});
            A422Status_AreaTrabalhoDes = T001P4_A422Status_AreaTrabalhoDes[0];
            n422Status_AreaTrabalhoDes = T001P4_n422Status_AreaTrabalhoDes[0];
            pr_default.close(2);
         }
      }

      protected void Load1P64( )
      {
         /* Using cursor T001P5 */
         pr_default.execute(3, new Object[] {A423Status_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound64 = 1;
            A424Status_Nome = T001P5_A424Status_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A424Status_Nome", A424Status_Nome);
            A422Status_AreaTrabalhoDes = T001P5_A422Status_AreaTrabalhoDes[0];
            n422Status_AreaTrabalhoDes = T001P5_n422Status_AreaTrabalhoDes[0];
            A425Status_Tipo = T001P5_A425Status_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A425Status_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A425Status_Tipo), 2, 0)));
            A421Status_AreaTrabalhoCod = T001P5_A421Status_AreaTrabalhoCod[0];
            ZM1P64( -10) ;
         }
         pr_default.close(3);
         OnLoadActions1P64( ) ;
      }

      protected void OnLoadActions1P64( )
      {
      }

      protected void CheckExtendedTable1P64( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A424Status_Nome)) )
         {
            GX_msglist.addItem("Nome � obrigat�rio.", 1, "STATUS_NOME");
            AnyError = 1;
            GX_FocusControl = edtStatus_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( A425Status_Tipo == 1 ) || ( A425Status_Tipo == 2 ) || ( A425Status_Tipo == 3 ) || ( A425Status_Tipo == 4 ) ) )
         {
            GX_msglist.addItem("Campo Tipo fora do intervalo", "OutOfRange", 1, "STATUS_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbStatus_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (0==A425Status_Tipo) )
         {
            GX_msglist.addItem("Tipo � obrigat�rio.", 1, "STATUS_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbStatus_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T001P4 */
         pr_default.execute(2, new Object[] {A421Status_AreaTrabalhoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Status_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A422Status_AreaTrabalhoDes = T001P4_A422Status_AreaTrabalhoDes[0];
         n422Status_AreaTrabalhoDes = T001P4_n422Status_AreaTrabalhoDes[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors1P64( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_11( int A421Status_AreaTrabalhoCod )
      {
         /* Using cursor T001P6 */
         pr_default.execute(4, new Object[] {A421Status_AreaTrabalhoCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Status_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A422Status_AreaTrabalhoDes = T001P6_A422Status_AreaTrabalhoDes[0];
         n422Status_AreaTrabalhoDes = T001P6_n422Status_AreaTrabalhoDes[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A422Status_AreaTrabalhoDes)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey1P64( )
      {
         /* Using cursor T001P7 */
         pr_default.execute(5, new Object[] {A423Status_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound64 = 1;
         }
         else
         {
            RcdFound64 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001P3 */
         pr_default.execute(1, new Object[] {A423Status_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1P64( 10) ;
            RcdFound64 = 1;
            A423Status_Codigo = T001P3_A423Status_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A423Status_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A423Status_Codigo), 6, 0)));
            A424Status_Nome = T001P3_A424Status_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A424Status_Nome", A424Status_Nome);
            A425Status_Tipo = T001P3_A425Status_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A425Status_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A425Status_Tipo), 2, 0)));
            A421Status_AreaTrabalhoCod = T001P3_A421Status_AreaTrabalhoCod[0];
            Z423Status_Codigo = A423Status_Codigo;
            sMode64 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1P64( ) ;
            if ( AnyError == 1 )
            {
               RcdFound64 = 0;
               InitializeNonKey1P64( ) ;
            }
            Gx_mode = sMode64;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound64 = 0;
            InitializeNonKey1P64( ) ;
            sMode64 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode64;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1P64( ) ;
         if ( RcdFound64 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound64 = 0;
         /* Using cursor T001P8 */
         pr_default.execute(6, new Object[] {A423Status_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T001P8_A423Status_Codigo[0] < A423Status_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T001P8_A423Status_Codigo[0] > A423Status_Codigo ) ) )
            {
               A423Status_Codigo = T001P8_A423Status_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A423Status_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A423Status_Codigo), 6, 0)));
               RcdFound64 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound64 = 0;
         /* Using cursor T001P9 */
         pr_default.execute(7, new Object[] {A423Status_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T001P9_A423Status_Codigo[0] > A423Status_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T001P9_A423Status_Codigo[0] < A423Status_Codigo ) ) )
            {
               A423Status_Codigo = T001P9_A423Status_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A423Status_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A423Status_Codigo), 6, 0)));
               RcdFound64 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1P64( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtStatus_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1P64( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound64 == 1 )
            {
               if ( A423Status_Codigo != Z423Status_Codigo )
               {
                  A423Status_Codigo = Z423Status_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A423Status_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A423Status_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "STATUS_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtStatus_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtStatus_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1P64( ) ;
                  GX_FocusControl = edtStatus_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A423Status_Codigo != Z423Status_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtStatus_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1P64( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "STATUS_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtStatus_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtStatus_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1P64( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A423Status_Codigo != Z423Status_Codigo )
         {
            A423Status_Codigo = Z423Status_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A423Status_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A423Status_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "STATUS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtStatus_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtStatus_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1P64( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001P2 */
            pr_default.execute(0, new Object[] {A423Status_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Status"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z424Status_Nome, T001P2_A424Status_Nome[0]) != 0 ) || ( Z425Status_Tipo != T001P2_A425Status_Tipo[0] ) || ( Z421Status_AreaTrabalhoCod != T001P2_A421Status_AreaTrabalhoCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z424Status_Nome, T001P2_A424Status_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("status:[seudo value changed for attri]"+"Status_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z424Status_Nome);
                  GXUtil.WriteLogRaw("Current: ",T001P2_A424Status_Nome[0]);
               }
               if ( Z425Status_Tipo != T001P2_A425Status_Tipo[0] )
               {
                  GXUtil.WriteLog("status:[seudo value changed for attri]"+"Status_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z425Status_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T001P2_A425Status_Tipo[0]);
               }
               if ( Z421Status_AreaTrabalhoCod != T001P2_A421Status_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("status:[seudo value changed for attri]"+"Status_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z421Status_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T001P2_A421Status_AreaTrabalhoCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Status"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1P64( )
      {
         BeforeValidate1P64( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1P64( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1P64( 0) ;
            CheckOptimisticConcurrency1P64( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1P64( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1P64( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001P10 */
                     pr_default.execute(8, new Object[] {A424Status_Nome, A425Status_Tipo, A421Status_AreaTrabalhoCod});
                     A423Status_Codigo = T001P10_A423Status_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A423Status_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A423Status_Codigo), 6, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Status") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1P0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1P64( ) ;
            }
            EndLevel1P64( ) ;
         }
         CloseExtendedTableCursors1P64( ) ;
      }

      protected void Update1P64( )
      {
         BeforeValidate1P64( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1P64( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1P64( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1P64( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1P64( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001P11 */
                     pr_default.execute(9, new Object[] {A424Status_Nome, A425Status_Tipo, A421Status_AreaTrabalhoCod, A423Status_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Status") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Status"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1P64( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1P64( ) ;
         }
         CloseExtendedTableCursors1P64( ) ;
      }

      protected void DeferredUpdate1P64( )
      {
      }

      protected void delete( )
      {
         BeforeValidate1P64( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1P64( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1P64( ) ;
            AfterConfirm1P64( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1P64( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001P12 */
                  pr_default.execute(10, new Object[] {A423Status_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("Status") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode64 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1P64( ) ;
         Gx_mode = sMode64;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1P64( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T001P13 */
            pr_default.execute(11, new Object[] {A421Status_AreaTrabalhoCod});
            A422Status_AreaTrabalhoDes = T001P13_A422Status_AreaTrabalhoDes[0];
            n422Status_AreaTrabalhoDes = T001P13_n422Status_AreaTrabalhoDes[0];
            pr_default.close(11);
         }
      }

      protected void EndLevel1P64( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1P64( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(11);
            context.CommitDataStores( "Status");
            if ( AnyError == 0 )
            {
               ConfirmValues1P0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(11);
            context.RollbackDataStores( "Status");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1P64( )
      {
         /* Scan By routine */
         /* Using cursor T001P14 */
         pr_default.execute(12);
         RcdFound64 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound64 = 1;
            A423Status_Codigo = T001P14_A423Status_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A423Status_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A423Status_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1P64( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound64 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound64 = 1;
            A423Status_Codigo = T001P14_A423Status_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A423Status_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A423Status_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd1P64( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm1P64( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1P64( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1P64( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1P64( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1P64( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1P64( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1P64( )
      {
         edtStatus_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtStatus_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtStatus_Nome_Enabled), 5, 0)));
         cmbStatus_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbStatus_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbStatus_Tipo.Enabled), 5, 0)));
         edtStatus_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtStatus_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtStatus_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1P0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117204486");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("status.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Status_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z423Status_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z423Status_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z424Status_Nome", StringUtil.RTrim( Z424Status_Nome));
         GxWebStd.gx_hidden_field( context, "Z425Status_Tipo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z425Status_Tipo), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z421Status_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z421Status_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N421Status_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A421Status_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vSTATUS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Status_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_STATUS_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Status_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "STATUS_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A421Status_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "STATUS_AREATRABALHODES", A422Status_AreaTrabalhoDes);
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSTATUS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Status_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Status";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A423Status_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A421Status_AreaTrabalhoCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("status:[SendSecurityCheck value for]"+"Status_Codigo:"+context.localUtil.Format( (decimal)(A423Status_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("status:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("status:[SendSecurityCheck value for]"+"Status_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A421Status_AreaTrabalhoCod), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("status.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Status_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Status" ;
      }

      public override String GetPgmdesc( )
      {
         return "Status" ;
      }

      protected void InitializeNonKey1P64( )
      {
         A421Status_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A421Status_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A421Status_AreaTrabalhoCod), 6, 0)));
         A424Status_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A424Status_Nome", A424Status_Nome);
         A422Status_AreaTrabalhoDes = "";
         n422Status_AreaTrabalhoDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A422Status_AreaTrabalhoDes", A422Status_AreaTrabalhoDes);
         A425Status_Tipo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A425Status_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A425Status_Tipo), 2, 0)));
         Z424Status_Nome = "";
         Z425Status_Tipo = 0;
         Z421Status_AreaTrabalhoCod = 0;
      }

      protected void InitAll1P64( )
      {
         A423Status_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A423Status_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A423Status_Codigo), 6, 0)));
         InitializeNonKey1P64( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311720458");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("status.js", "?2020311720459");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockstatus_nome_Internalname = "TEXTBLOCKSTATUS_NOME";
         edtStatus_Nome_Internalname = "STATUS_NOME";
         lblTextblockstatus_tipo_Internalname = "TEXTBLOCKSTATUS_TIPO";
         cmbStatus_Tipo_Internalname = "STATUS_TIPO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtStatus_Codigo_Internalname = "STATUS_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Status";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Status";
         cmbStatus_Tipo_Jsonclick = "";
         cmbStatus_Tipo.Enabled = 1;
         edtStatus_Nome_Jsonclick = "";
         edtStatus_Nome_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtStatus_Codigo_Jsonclick = "";
         edtStatus_Codigo_Enabled = 0;
         edtStatus_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Status_Codigo',fld:'vSTATUS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E121P2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z424Status_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockstatus_nome_Jsonclick = "";
         A424Status_Nome = "";
         lblTextblockstatus_tipo_Jsonclick = "";
         A422Status_AreaTrabalhoDes = "";
         AV13Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode64 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z422Status_AreaTrabalhoDes = "";
         T001P4_A422Status_AreaTrabalhoDes = new String[] {""} ;
         T001P4_n422Status_AreaTrabalhoDes = new bool[] {false} ;
         T001P5_A423Status_Codigo = new int[1] ;
         T001P5_A424Status_Nome = new String[] {""} ;
         T001P5_A422Status_AreaTrabalhoDes = new String[] {""} ;
         T001P5_n422Status_AreaTrabalhoDes = new bool[] {false} ;
         T001P5_A425Status_Tipo = new short[1] ;
         T001P5_A421Status_AreaTrabalhoCod = new int[1] ;
         T001P6_A422Status_AreaTrabalhoDes = new String[] {""} ;
         T001P6_n422Status_AreaTrabalhoDes = new bool[] {false} ;
         T001P7_A423Status_Codigo = new int[1] ;
         T001P3_A423Status_Codigo = new int[1] ;
         T001P3_A424Status_Nome = new String[] {""} ;
         T001P3_A425Status_Tipo = new short[1] ;
         T001P3_A421Status_AreaTrabalhoCod = new int[1] ;
         T001P8_A423Status_Codigo = new int[1] ;
         T001P9_A423Status_Codigo = new int[1] ;
         T001P2_A423Status_Codigo = new int[1] ;
         T001P2_A424Status_Nome = new String[] {""} ;
         T001P2_A425Status_Tipo = new short[1] ;
         T001P2_A421Status_AreaTrabalhoCod = new int[1] ;
         T001P10_A423Status_Codigo = new int[1] ;
         T001P13_A422Status_AreaTrabalhoDes = new String[] {""} ;
         T001P13_n422Status_AreaTrabalhoDes = new bool[] {false} ;
         T001P14_A423Status_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.status__default(),
            new Object[][] {
                new Object[] {
               T001P2_A423Status_Codigo, T001P2_A424Status_Nome, T001P2_A425Status_Tipo, T001P2_A421Status_AreaTrabalhoCod
               }
               , new Object[] {
               T001P3_A423Status_Codigo, T001P3_A424Status_Nome, T001P3_A425Status_Tipo, T001P3_A421Status_AreaTrabalhoCod
               }
               , new Object[] {
               T001P4_A422Status_AreaTrabalhoDes, T001P4_n422Status_AreaTrabalhoDes
               }
               , new Object[] {
               T001P5_A423Status_Codigo, T001P5_A424Status_Nome, T001P5_A422Status_AreaTrabalhoDes, T001P5_n422Status_AreaTrabalhoDes, T001P5_A425Status_Tipo, T001P5_A421Status_AreaTrabalhoCod
               }
               , new Object[] {
               T001P6_A422Status_AreaTrabalhoDes, T001P6_n422Status_AreaTrabalhoDes
               }
               , new Object[] {
               T001P7_A423Status_Codigo
               }
               , new Object[] {
               T001P8_A423Status_Codigo
               }
               , new Object[] {
               T001P9_A423Status_Codigo
               }
               , new Object[] {
               T001P10_A423Status_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001P13_A422Status_AreaTrabalhoDes, T001P13_n422Status_AreaTrabalhoDes
               }
               , new Object[] {
               T001P14_A423Status_Codigo
               }
            }
         );
         AV13Pgmname = "Status";
      }

      private short Z425Status_Tipo ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A425Status_Tipo ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound64 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private int wcpOAV7Status_Codigo ;
      private int Z423Status_Codigo ;
      private int Z421Status_AreaTrabalhoCod ;
      private int N421Status_AreaTrabalhoCod ;
      private int A421Status_AreaTrabalhoCod ;
      private int AV7Status_Codigo ;
      private int trnEnded ;
      private int A423Status_Codigo ;
      private int edtStatus_Codigo_Enabled ;
      private int edtStatus_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtStatus_Nome_Enabled ;
      private int AV11Insert_Status_AreaTrabalhoCod ;
      private int AV14GXV1 ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z424Status_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtStatus_Nome_Internalname ;
      private String edtStatus_Codigo_Internalname ;
      private String edtStatus_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockstatus_nome_Internalname ;
      private String lblTextblockstatus_nome_Jsonclick ;
      private String A424Status_Nome ;
      private String edtStatus_Nome_Jsonclick ;
      private String lblTextblockstatus_tipo_Internalname ;
      private String lblTextblockstatus_tipo_Jsonclick ;
      private String cmbStatus_Tipo_Internalname ;
      private String cmbStatus_Tipo_Jsonclick ;
      private String AV13Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode64 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n422Status_AreaTrabalhoDes ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String A422Status_AreaTrabalhoDes ;
      private String Z422Status_AreaTrabalhoDes ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbStatus_Tipo ;
      private IDataStoreProvider pr_default ;
      private String[] T001P4_A422Status_AreaTrabalhoDes ;
      private bool[] T001P4_n422Status_AreaTrabalhoDes ;
      private int[] T001P5_A423Status_Codigo ;
      private String[] T001P5_A424Status_Nome ;
      private String[] T001P5_A422Status_AreaTrabalhoDes ;
      private bool[] T001P5_n422Status_AreaTrabalhoDes ;
      private short[] T001P5_A425Status_Tipo ;
      private int[] T001P5_A421Status_AreaTrabalhoCod ;
      private String[] T001P6_A422Status_AreaTrabalhoDes ;
      private bool[] T001P6_n422Status_AreaTrabalhoDes ;
      private int[] T001P7_A423Status_Codigo ;
      private int[] T001P3_A423Status_Codigo ;
      private String[] T001P3_A424Status_Nome ;
      private short[] T001P3_A425Status_Tipo ;
      private int[] T001P3_A421Status_AreaTrabalhoCod ;
      private int[] T001P8_A423Status_Codigo ;
      private int[] T001P9_A423Status_Codigo ;
      private int[] T001P2_A423Status_Codigo ;
      private String[] T001P2_A424Status_Nome ;
      private short[] T001P2_A425Status_Tipo ;
      private int[] T001P2_A421Status_AreaTrabalhoCod ;
      private int[] T001P10_A423Status_Codigo ;
      private String[] T001P13_A422Status_AreaTrabalhoDes ;
      private bool[] T001P13_n422Status_AreaTrabalhoDes ;
      private int[] T001P14_A423Status_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class status__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001P5 ;
          prmT001P5 = new Object[] {
          new Object[] {"@Status_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001P4 ;
          prmT001P4 = new Object[] {
          new Object[] {"@Status_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001P6 ;
          prmT001P6 = new Object[] {
          new Object[] {"@Status_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001P7 ;
          prmT001P7 = new Object[] {
          new Object[] {"@Status_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001P3 ;
          prmT001P3 = new Object[] {
          new Object[] {"@Status_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001P8 ;
          prmT001P8 = new Object[] {
          new Object[] {"@Status_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001P9 ;
          prmT001P9 = new Object[] {
          new Object[] {"@Status_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001P2 ;
          prmT001P2 = new Object[] {
          new Object[] {"@Status_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001P10 ;
          prmT001P10 = new Object[] {
          new Object[] {"@Status_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Status_Tipo",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@Status_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001P11 ;
          prmT001P11 = new Object[] {
          new Object[] {"@Status_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Status_Tipo",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@Status_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Status_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001P12 ;
          prmT001P12 = new Object[] {
          new Object[] {"@Status_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001P13 ;
          prmT001P13 = new Object[] {
          new Object[] {"@Status_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001P14 ;
          prmT001P14 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T001P2", "SELECT [Status_Codigo], [Status_Nome], [Status_Tipo], [Status_AreaTrabalhoCod] AS Status_AreaTrabalhoCod FROM [Status] WITH (UPDLOCK) WHERE [Status_Codigo] = @Status_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001P2,1,0,true,false )
             ,new CursorDef("T001P3", "SELECT [Status_Codigo], [Status_Nome], [Status_Tipo], [Status_AreaTrabalhoCod] AS Status_AreaTrabalhoCod FROM [Status] WITH (NOLOCK) WHERE [Status_Codigo] = @Status_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001P3,1,0,true,false )
             ,new CursorDef("T001P4", "SELECT [AreaTrabalho_Descricao] AS Status_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Status_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001P4,1,0,true,false )
             ,new CursorDef("T001P5", "SELECT TM1.[Status_Codigo], TM1.[Status_Nome], T2.[AreaTrabalho_Descricao] AS Status_AreaTrabalhoDes, TM1.[Status_Tipo], TM1.[Status_AreaTrabalhoCod] AS Status_AreaTrabalhoCod FROM ([Status] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[Status_AreaTrabalhoCod]) WHERE TM1.[Status_Codigo] = @Status_Codigo ORDER BY TM1.[Status_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001P5,100,0,true,false )
             ,new CursorDef("T001P6", "SELECT [AreaTrabalho_Descricao] AS Status_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Status_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001P6,1,0,true,false )
             ,new CursorDef("T001P7", "SELECT [Status_Codigo] FROM [Status] WITH (NOLOCK) WHERE [Status_Codigo] = @Status_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001P7,1,0,true,false )
             ,new CursorDef("T001P8", "SELECT TOP 1 [Status_Codigo] FROM [Status] WITH (NOLOCK) WHERE ( [Status_Codigo] > @Status_Codigo) ORDER BY [Status_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001P8,1,0,true,true )
             ,new CursorDef("T001P9", "SELECT TOP 1 [Status_Codigo] FROM [Status] WITH (NOLOCK) WHERE ( [Status_Codigo] < @Status_Codigo) ORDER BY [Status_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001P9,1,0,true,true )
             ,new CursorDef("T001P10", "INSERT INTO [Status]([Status_Nome], [Status_Tipo], [Status_AreaTrabalhoCod]) VALUES(@Status_Nome, @Status_Tipo, @Status_AreaTrabalhoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT001P10)
             ,new CursorDef("T001P11", "UPDATE [Status] SET [Status_Nome]=@Status_Nome, [Status_Tipo]=@Status_Tipo, [Status_AreaTrabalhoCod]=@Status_AreaTrabalhoCod  WHERE [Status_Codigo] = @Status_Codigo", GxErrorMask.GX_NOMASK,prmT001P11)
             ,new CursorDef("T001P12", "DELETE FROM [Status]  WHERE [Status_Codigo] = @Status_Codigo", GxErrorMask.GX_NOMASK,prmT001P12)
             ,new CursorDef("T001P13", "SELECT [AreaTrabalho_Descricao] AS Status_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Status_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001P13,1,0,true,false )
             ,new CursorDef("T001P14", "SELECT [Status_Codigo] FROM [Status] WITH (NOLOCK) ORDER BY [Status_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001P14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
