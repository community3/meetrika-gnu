/*
               File: PRC_RetornaCodigoPerfilUsuarioAreaTrabalho
        Description: Retorna Codigo Perfil Usuario Area Trabalho
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:41.53
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_retornacodigoperfilusuarioareatrabalho : GXProcedure
   {
      public prc_retornacodigoperfilusuarioareatrabalho( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_retornacodigoperfilusuarioareatrabalho( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           out int aP1_Perfil_Codigo ,
                           out long aP2_Perfil_GamId )
      {
         this.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV9Perfil_Codigo = 0 ;
         this.AV10Perfil_GamId = 0 ;
         initialize();
         executePrivate();
         aP1_Perfil_Codigo=this.AV9Perfil_Codigo;
         aP2_Perfil_GamId=this.AV10Perfil_GamId;
      }

      public long executeUdp( int aP0_AreaTrabalho_Codigo ,
                              out int aP1_Perfil_Codigo )
      {
         this.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV9Perfil_Codigo = 0 ;
         this.AV10Perfil_GamId = 0 ;
         initialize();
         executePrivate();
         aP1_Perfil_Codigo=this.AV9Perfil_Codigo;
         aP2_Perfil_GamId=this.AV10Perfil_GamId;
         return AV10Perfil_GamId ;
      }

      public void executeSubmit( int aP0_AreaTrabalho_Codigo ,
                                 out int aP1_Perfil_Codigo ,
                                 out long aP2_Perfil_GamId )
      {
         prc_retornacodigoperfilusuarioareatrabalho objprc_retornacodigoperfilusuarioareatrabalho;
         objprc_retornacodigoperfilusuarioareatrabalho = new prc_retornacodigoperfilusuarioareatrabalho();
         objprc_retornacodigoperfilusuarioareatrabalho.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_retornacodigoperfilusuarioareatrabalho.AV9Perfil_Codigo = 0 ;
         objprc_retornacodigoperfilusuarioareatrabalho.AV10Perfil_GamId = 0 ;
         objprc_retornacodigoperfilusuarioareatrabalho.context.SetSubmitInitialConfig(context);
         objprc_retornacodigoperfilusuarioareatrabalho.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_retornacodigoperfilusuarioareatrabalho);
         aP1_Perfil_Codigo=this.AV9Perfil_Codigo;
         aP2_Perfil_GamId=this.AV10Perfil_GamId;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_retornacodigoperfilusuarioareatrabalho)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P001L2 */
         pr_default.execute(0, new Object[] {AV8AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A275Perfil_Tipo = P001L2_A275Perfil_Tipo[0];
            A7Perfil_AreaTrabalhoCod = P001L2_A7Perfil_AreaTrabalhoCod[0];
            A3Perfil_Codigo = P001L2_A3Perfil_Codigo[0];
            A329Perfil_GamId = P001L2_A329Perfil_GamId[0];
            AV9Perfil_Codigo = A3Perfil_Codigo;
            AV10Perfil_GamId = A329Perfil_GamId;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P001L2_A275Perfil_Tipo = new short[1] ;
         P001L2_A7Perfil_AreaTrabalhoCod = new int[1] ;
         P001L2_A3Perfil_Codigo = new int[1] ;
         P001L2_A329Perfil_GamId = new long[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_retornacodigoperfilusuarioareatrabalho__default(),
            new Object[][] {
                new Object[] {
               P001L2_A275Perfil_Tipo, P001L2_A7Perfil_AreaTrabalhoCod, P001L2_A3Perfil_Codigo, P001L2_A329Perfil_GamId
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A275Perfil_Tipo ;
      private int AV8AreaTrabalho_Codigo ;
      private int A7Perfil_AreaTrabalhoCod ;
      private int A3Perfil_Codigo ;
      private int AV9Perfil_Codigo ;
      private long AV10Perfil_GamId ;
      private long A329Perfil_GamId ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P001L2_A275Perfil_Tipo ;
      private int[] P001L2_A7Perfil_AreaTrabalhoCod ;
      private int[] P001L2_A3Perfil_Codigo ;
      private long[] P001L2_A329Perfil_GamId ;
      private int aP1_Perfil_Codigo ;
      private long aP2_Perfil_GamId ;
   }

   public class prc_retornacodigoperfilusuarioareatrabalho__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP001L2 ;
          prmP001L2 = new Object[] {
          new Object[] {"@AV8AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P001L2", "SELECT [Perfil_Tipo], [Perfil_AreaTrabalhoCod], [Perfil_Codigo], [Perfil_GamId] FROM [Perfil] WITH (NOLOCK) WHERE ([Perfil_AreaTrabalhoCod] = @AV8AreaTrabalho_Codigo) AND ([Perfil_Tipo] = 6) ORDER BY [Perfil_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001L2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((long[]) buf[3])[0] = rslt.getLong(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
