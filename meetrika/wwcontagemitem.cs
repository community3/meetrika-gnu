/*
               File: WWContagemItem
        Description:  Contagem Item
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:34:25.62
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontagemitem : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontagemitem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontagemitem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( short aP0_StatusItem )
      {
         this.AV62StatusItem = aP0_StatusItem;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbContagemItem_TipoUnidade = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_39 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_39_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_39_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV108TFContagem_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV108TFContagem_Codigo), 6, 0)));
               AV109TFContagem_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFContagem_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV109TFContagem_Codigo_To), 6, 0)));
               AV116TFContagemItem_PFB = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116TFContagemItem_PFB", StringUtil.LTrim( StringUtil.Str( AV116TFContagemItem_PFB, 14, 5)));
               AV117TFContagemItem_PFB_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117TFContagemItem_PFB_To", StringUtil.LTrim( StringUtil.Str( AV117TFContagemItem_PFB_To, 14, 5)));
               AV120TFContagemItem_PFL = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120TFContagemItem_PFL", StringUtil.LTrim( StringUtil.Str( AV120TFContagemItem_PFL, 14, 5)));
               AV121TFContagemItem_PFL_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFContagemItem_PFL_To", StringUtil.LTrim( StringUtil.Str( AV121TFContagemItem_PFL_To, 14, 5)));
               AV128TFContagemItem_Evidencias = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV128TFContagemItem_Evidencias", AV128TFContagemItem_Evidencias);
               AV129TFContagemItem_Evidencias_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV129TFContagemItem_Evidencias_Sel", AV129TFContagemItem_Evidencias_Sel);
               AV86ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV86ManageFiltersExecutionStep), 1, 0));
               AV110ddo_Contagem_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110ddo_Contagem_CodigoTitleControlIdToReplace", AV110ddo_Contagem_CodigoTitleControlIdToReplace);
               AV118ddo_ContagemItem_PFBTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118ddo_ContagemItem_PFBTitleControlIdToReplace", AV118ddo_ContagemItem_PFBTitleControlIdToReplace);
               AV122ddo_ContagemItem_PFLTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122ddo_ContagemItem_PFLTitleControlIdToReplace", AV122ddo_ContagemItem_PFLTitleControlIdToReplace);
               AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace", AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace);
               AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace", AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace);
               AV62StatusItem = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62StatusItem", StringUtil.Str( (decimal)(AV62StatusItem), 1, 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSTATUSITEM", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV62StatusItem), "9")));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV125TFContagemItem_TipoUnidade_Sels);
               AV145Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               A224ContagemItem_Lancamento = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
               A192Contagem_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV108TFContagem_Codigo, AV109TFContagem_Codigo_To, AV116TFContagemItem_PFB, AV117TFContagemItem_PFB_To, AV120TFContagemItem_PFL, AV121TFContagemItem_PFL_To, AV128TFContagemItem_Evidencias, AV129TFContagemItem_Evidencias_Sel, AV86ManageFiltersExecutionStep, AV110ddo_Contagem_CodigoTitleControlIdToReplace, AV118ddo_ContagemItem_PFBTitleControlIdToReplace, AV122ddo_ContagemItem_PFLTitleControlIdToReplace, AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace, AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace, AV62StatusItem, AV125TFContagemItem_TipoUnidade_Sels, AV145Pgmname, AV6WWPContext, A224ContagemItem_Lancamento, A192Contagem_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV62StatusItem = (short)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62StatusItem", StringUtil.Str( (decimal)(AV62StatusItem), 1, 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSTATUSITEM", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV62StatusItem), "9")));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA532( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START532( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117342598");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontagemitem.aspx") + "?" + UrlEncode("" +AV62StatusItem)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV108TFContagem_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEM_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV109TFContagem_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEM_PFB", StringUtil.LTrim( StringUtil.NToC( AV116TFContagemItem_PFB, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEM_PFB_TO", StringUtil.LTrim( StringUtil.NToC( AV117TFContagemItem_PFB_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEM_PFL", StringUtil.LTrim( StringUtil.NToC( AV120TFContagemItem_PFL, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEM_PFL_TO", StringUtil.LTrim( StringUtil.NToC( AV121TFContagemItem_PFL_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEM_EVIDENCIAS", AV128TFContagemItem_Evidencias);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMITEM_EVIDENCIAS_SEL", AV129TFContagemItem_Evidencias_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_39", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_39), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV90ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV90ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV113GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV114GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV111DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV111DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEM_CODIGOTITLEFILTERDATA", AV107Contagem_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEM_CODIGOTITLEFILTERDATA", AV107Contagem_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMITEM_PFBTITLEFILTERDATA", AV115ContagemItem_PFBTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMITEM_PFBTITLEFILTERDATA", AV115ContagemItem_PFBTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMITEM_PFLTITLEFILTERDATA", AV119ContagemItem_PFLTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMITEM_PFLTITLEFILTERDATA", AV119ContagemItem_PFLTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMITEM_TIPOUNIDADETITLEFILTERDATA", AV123ContagemItem_TipoUnidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMITEM_TIPOUNIDADETITLEFILTERDATA", AV123ContagemItem_TipoUnidadeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMITEM_EVIDENCIASTITLEFILTERDATA", AV127ContagemItem_EvidenciasTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMITEM_EVIDENCIASTITLEFILTERDATA", AV127ContagemItem_EvidenciasTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vSTATUSITEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62StatusItem), 1, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFCONTAGEMITEM_TIPOUNIDADE_SELS", AV125TFContagemItem_TipoUnidade_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFCONTAGEMITEM_TIPOUNIDADE_SELS", AV125TFContagemItem_TipoUnidade_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV145Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_LANCAMENTO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTIFICATIONINFO", AV85NotificationInfo);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTIFICATIONINFO", AV85NotificationInfo);
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSTATUSITEM", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV62StatusItem), "9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSTATUSITEM", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV62StatusItem), "9")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Caption", StringUtil.RTrim( Ddo_contagem_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contagem_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Cls", StringUtil.RTrim( Ddo_contagem_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contagem_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contagem_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagem_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagem_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contagem_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contagem_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contagem_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contagem_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contagem_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contagem_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contagem_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contagem_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contagem_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contagem_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contagem_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contagem_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contagem_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Caption", StringUtil.RTrim( Ddo_contagemitem_pfb_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Tooltip", StringUtil.RTrim( Ddo_contagemitem_pfb_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Cls", StringUtil.RTrim( Ddo_contagemitem_pfb_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Filteredtext_set", StringUtil.RTrim( Ddo_contagemitem_pfb_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemitem_pfb_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemitem_pfb_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemitem_pfb_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Includesortasc", StringUtil.BoolToStr( Ddo_contagemitem_pfb_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemitem_pfb_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Sortedstatus", StringUtil.RTrim( Ddo_contagemitem_pfb_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Includefilter", StringUtil.BoolToStr( Ddo_contagemitem_pfb_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Filtertype", StringUtil.RTrim( Ddo_contagemitem_pfb_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Filterisrange", StringUtil.BoolToStr( Ddo_contagemitem_pfb_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Includedatalist", StringUtil.BoolToStr( Ddo_contagemitem_pfb_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Sortasc", StringUtil.RTrim( Ddo_contagemitem_pfb_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Sortdsc", StringUtil.RTrim( Ddo_contagemitem_pfb_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Cleanfilter", StringUtil.RTrim( Ddo_contagemitem_pfb_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemitem_pfb_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Rangefilterto", StringUtil.RTrim( Ddo_contagemitem_pfb_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Searchbuttontext", StringUtil.RTrim( Ddo_contagemitem_pfb_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Caption", StringUtil.RTrim( Ddo_contagemitem_pfl_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Tooltip", StringUtil.RTrim( Ddo_contagemitem_pfl_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Cls", StringUtil.RTrim( Ddo_contagemitem_pfl_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Filteredtext_set", StringUtil.RTrim( Ddo_contagemitem_pfl_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemitem_pfl_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemitem_pfl_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemitem_pfl_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Includesortasc", StringUtil.BoolToStr( Ddo_contagemitem_pfl_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemitem_pfl_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Sortedstatus", StringUtil.RTrim( Ddo_contagemitem_pfl_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Includefilter", StringUtil.BoolToStr( Ddo_contagemitem_pfl_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Filtertype", StringUtil.RTrim( Ddo_contagemitem_pfl_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Filterisrange", StringUtil.BoolToStr( Ddo_contagemitem_pfl_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Includedatalist", StringUtil.BoolToStr( Ddo_contagemitem_pfl_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Sortasc", StringUtil.RTrim( Ddo_contagemitem_pfl_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Sortdsc", StringUtil.RTrim( Ddo_contagemitem_pfl_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Cleanfilter", StringUtil.RTrim( Ddo_contagemitem_pfl_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemitem_pfl_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Rangefilterto", StringUtil.RTrim( Ddo_contagemitem_pfl_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Searchbuttontext", StringUtil.RTrim( Ddo_contagemitem_pfl_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Caption", StringUtil.RTrim( Ddo_contagemitem_tipounidade_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Tooltip", StringUtil.RTrim( Ddo_contagemitem_tipounidade_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Cls", StringUtil.RTrim( Ddo_contagemitem_tipounidade_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemitem_tipounidade_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemitem_tipounidade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemitem_tipounidade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_contagemitem_tipounidade_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemitem_tipounidade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Sortedstatus", StringUtil.RTrim( Ddo_contagemitem_tipounidade_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Includefilter", StringUtil.BoolToStr( Ddo_contagemitem_tipounidade_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_contagemitem_tipounidade_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Datalisttype", StringUtil.RTrim( Ddo_contagemitem_tipounidade_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Allowmultipleselection", StringUtil.BoolToStr( Ddo_contagemitem_tipounidade_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Datalistfixedvalues", StringUtil.RTrim( Ddo_contagemitem_tipounidade_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Sortasc", StringUtil.RTrim( Ddo_contagemitem_tipounidade_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Sortdsc", StringUtil.RTrim( Ddo_contagemitem_tipounidade_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Cleanfilter", StringUtil.RTrim( Ddo_contagemitem_tipounidade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_contagemitem_tipounidade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Caption", StringUtil.RTrim( Ddo_contagemitem_evidencias_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Tooltip", StringUtil.RTrim( Ddo_contagemitem_evidencias_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Cls", StringUtil.RTrim( Ddo_contagemitem_evidencias_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Filteredtext_set", StringUtil.RTrim( Ddo_contagemitem_evidencias_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemitem_evidencias_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemitem_evidencias_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemitem_evidencias_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Includesortasc", StringUtil.BoolToStr( Ddo_contagemitem_evidencias_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemitem_evidencias_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Sortedstatus", StringUtil.RTrim( Ddo_contagemitem_evidencias_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Includefilter", StringUtil.BoolToStr( Ddo_contagemitem_evidencias_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Filtertype", StringUtil.RTrim( Ddo_contagemitem_evidencias_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Filterisrange", StringUtil.BoolToStr( Ddo_contagemitem_evidencias_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Includedatalist", StringUtil.BoolToStr( Ddo_contagemitem_evidencias_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Datalisttype", StringUtil.RTrim( Ddo_contagemitem_evidencias_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Datalistproc", StringUtil.RTrim( Ddo_contagemitem_evidencias_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemitem_evidencias_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Sortasc", StringUtil.RTrim( Ddo_contagemitem_evidencias_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Sortdsc", StringUtil.RTrim( Ddo_contagemitem_evidencias_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Loadingdata", StringUtil.RTrim( Ddo_contagemitem_evidencias_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Cleanfilter", StringUtil.RTrim( Ddo_contagemitem_evidencias_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Noresultsfound", StringUtil.RTrim( Ddo_contagemitem_evidencias_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Searchbuttontext", StringUtil.RTrim( Ddo_contagemitem_evidencias_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contagem_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contagem_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEM_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contagem_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Activeeventkey", StringUtil.RTrim( Ddo_contagemitem_pfb_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Filteredtext_get", StringUtil.RTrim( Ddo_contagemitem_pfb_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFB_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemitem_pfb_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Activeeventkey", StringUtil.RTrim( Ddo_contagemitem_pfl_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Filteredtext_get", StringUtil.RTrim( Ddo_contagemitem_pfl_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_PFL_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemitem_pfl_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Activeeventkey", StringUtil.RTrim( Ddo_contagemitem_tipounidade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_TIPOUNIDADE_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemitem_tipounidade_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Activeeventkey", StringUtil.RTrim( Ddo_contagemitem_evidencias_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Filteredtext_get", StringUtil.RTrim( Ddo_contagemitem_evidencias_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMITEM_EVIDENCIAS_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemitem_evidencias_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE532( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT532( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontagemitem.aspx") + "?" + UrlEncode("" +AV62StatusItem) ;
      }

      public override String GetPgmname( )
      {
         return "WWContagemItem" ;
      }

      public override String GetPgmdesc( )
      {
         return " Contagem Item" ;
      }

      protected void WB530( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_532( true) ;
         }
         else
         {
            wb_table1_2_532( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_532e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_39_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV86ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV86ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_39_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagem_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV108TFContagem_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV108TFContagem_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagem_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagem_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_39_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagem_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV109TFContagem_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV109TFContagem_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagem_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagem_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_39_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemitem_pfb_Internalname, StringUtil.LTrim( StringUtil.NToC( AV116TFContagemItem_PFB, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV116TFContagemItem_PFB, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemitem_pfb_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemitem_pfb_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_39_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemitem_pfb_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV117TFContagemItem_PFB_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV117TFContagemItem_PFB_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemitem_pfb_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemitem_pfb_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_39_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemitem_pfl_Internalname, StringUtil.LTrim( StringUtil.NToC( AV120TFContagemItem_PFL, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV120TFContagemItem_PFL, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemitem_pfl_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemitem_pfl_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_39_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemitem_pfl_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV121TFContagemItem_PFL_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV121TFContagemItem_PFL_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemitem_pfl_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemitem_pfl_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContagemItem.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_39_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontagemitem_evidencias_Internalname, AV128TFContagemItem_Evidencias, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", 0, edtavTfcontagemitem_evidencias_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWContagemItem.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_39_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontagemitem_evidencias_sel_Internalname, AV129TFContagemItem_Evidencias_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", 0, edtavTfcontagemitem_evidencias_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWContagemItem.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEM_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_39_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagem_codigotitlecontrolidtoreplace_Internalname, AV110ddo_Contagem_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", 0, edtavDdo_contagem_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemItem.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMITEM_PFBContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_39_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemitem_pfbtitlecontrolidtoreplace_Internalname, AV118ddo_ContagemItem_PFBTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", 0, edtavDdo_contagemitem_pfbtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemItem.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMITEM_PFLContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_39_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemitem_pfltitlecontrolidtoreplace_Internalname, AV122ddo_ContagemItem_PFLTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", 0, edtavDdo_contagemitem_pfltitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemItem.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMITEM_TIPOUNIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_39_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemitem_tipounidadetitlecontrolidtoreplace_Internalname, AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", 0, edtavDdo_contagemitem_tipounidadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemItem.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMITEM_EVIDENCIASContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_39_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemitem_evidenciastitlecontrolidtoreplace_Internalname, AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,70);\"", 0, edtavDdo_contagemitem_evidenciastitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContagemItem.htm");
         }
         wbLoad = true;
      }

      protected void START532( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Contagem Item", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP530( ) ;
      }

      protected void WS532( )
      {
         START532( ) ;
         EVT532( ) ;
      }

      protected void EVT532( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11532 */
                              E11532 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12532 */
                              E12532 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEM_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13532 */
                              E13532 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMITEM_PFB.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14532 */
                              E14532 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMITEM_PFL.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15532 */
                              E15532 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMITEM_TIPOUNIDADE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16532 */
                              E16532 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMITEM_EVIDENCIAS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17532 */
                              E17532 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18532 */
                              E18532 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19532 */
                              E19532 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ONMESSAGE_GX1") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20532 */
                              E20532 ();
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "ONMESSAGE_GX1") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_39_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_39_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_39_idx), 4, 0)), 4, "0");
                              SubsflControlProps_392( ) ;
                              AV31Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV142Update_GXI : context.convertURL( context.PathToRelativeUrl( AV31Update))));
                              AV32Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV143Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV32Delete))));
                              AV55Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV55Display)) ? AV144Display_GXI : context.convertURL( context.PathToRelativeUrl( AV55Display))));
                              A192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagem_Codigo_Internalname), ",", "."));
                              A950ContagemItem_PFB = context.localUtil.CToN( cgiGet( edtContagemItem_PFB_Internalname), ",", ".");
                              n950ContagemItem_PFB = false;
                              A951ContagemItem_PFL = context.localUtil.CToN( cgiGet( edtContagemItem_PFL_Internalname), ",", ".");
                              n951ContagemItem_PFL = false;
                              cmbContagemItem_TipoUnidade.Name = cmbContagemItem_TipoUnidade_Internalname;
                              cmbContagemItem_TipoUnidade.CurrentValue = cgiGet( cmbContagemItem_TipoUnidade_Internalname);
                              A952ContagemItem_TipoUnidade = cgiGet( cmbContagemItem_TipoUnidade_Internalname);
                              A953ContagemItem_Evidencias = cgiGet( edtContagemItem_Evidencias_Internalname);
                              n953ContagemItem_Evidencias = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E21532 */
                                    E21532 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E22532 */
                                    E22532 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E23532 */
                                    E23532 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ONMESSAGE_GX1") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E20532 */
                                    E20532 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagem_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEM_CODIGO"), ",", ".") != Convert.ToDecimal( AV108TFContagem_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagem_codigo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEM_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV109TFContagem_Codigo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitem_pfb Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEM_PFB"), ",", ".") != AV116TFContagemItem_PFB )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitem_pfb_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEM_PFB_TO"), ",", ".") != AV117TFContagemItem_PFB_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitem_pfl Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEM_PFL"), ",", ".") != AV120TFContagemItem_PFL )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitem_pfl_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEM_PFL_TO"), ",", ".") != AV121TFContagemItem_PFL_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitem_evidencias Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMITEM_EVIDENCIAS"), AV128TFContagemItem_Evidencias) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontagemitem_evidencias_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMITEM_EVIDENCIAS_SEL"), AV129TFContagemItem_Evidencias_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ONMESSAGE_GX1") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E20532 */
                                    E20532 ();
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE532( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA532( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            GXCCtl = "CONTAGEMITEM_TIPOUNIDADE_" + sGXsfl_39_idx;
            cmbContagemItem_TipoUnidade.Name = GXCCtl;
            cmbContagemItem_TipoUnidade.WebTags = "";
            if ( cmbContagemItem_TipoUnidade.ItemCount > 0 )
            {
               A952ContagemItem_TipoUnidade = cmbContagemItem_TipoUnidade.getValidValue(A952ContagemItem_TipoUnidade);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_392( ) ;
         while ( nGXsfl_39_idx <= nRC_GXsfl_39 )
         {
            sendrow_392( ) ;
            nGXsfl_39_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_39_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_39_idx+1));
            sGXsfl_39_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_39_idx), 4, 0)), 4, "0");
            SubsflControlProps_392( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       int AV108TFContagem_Codigo ,
                                       int AV109TFContagem_Codigo_To ,
                                       decimal AV116TFContagemItem_PFB ,
                                       decimal AV117TFContagemItem_PFB_To ,
                                       decimal AV120TFContagemItem_PFL ,
                                       decimal AV121TFContagemItem_PFL_To ,
                                       String AV128TFContagemItem_Evidencias ,
                                       String AV129TFContagemItem_Evidencias_Sel ,
                                       short AV86ManageFiltersExecutionStep ,
                                       String AV110ddo_Contagem_CodigoTitleControlIdToReplace ,
                                       String AV118ddo_ContagemItem_PFBTitleControlIdToReplace ,
                                       String AV122ddo_ContagemItem_PFLTitleControlIdToReplace ,
                                       String AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace ,
                                       String AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace ,
                                       short AV62StatusItem ,
                                       IGxCollection AV125TFContagemItem_TipoUnidade_Sels ,
                                       String AV145Pgmname ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int A224ContagemItem_Lancamento ,
                                       int A192Contagem_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF532( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_PFB", GetSecureSignedToken( "", context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_PFB", StringUtil.LTrim( StringUtil.NToC( A950ContagemItem_PFB, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_PFL", GetSecureSignedToken( "", context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_PFL", StringUtil.LTrim( StringUtil.NToC( A951ContagemItem_PFL, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_TIPOUNIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A952ContagemItem_TipoUnidade, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_TIPOUNIDADE", A952ContagemItem_TipoUnidade);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_EVIDENCIAS", GetSecureSignedToken( "", A953ContagemItem_Evidencias));
         GxWebStd.gx_hidden_field( context, "CONTAGEMITEM_EVIDENCIAS", A953ContagemItem_Evidencias);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF532( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV145Pgmname = "WWContagemItem";
         context.Gx_err = 0;
      }

      protected void RF532( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 39;
         /* Execute user event: E22532 */
         E22532 ();
         nGXsfl_39_idx = 1;
         sGXsfl_39_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_39_idx), 4, 0)), 4, "0");
         SubsflControlProps_392( ) ;
         nGXsfl_39_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_392( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A952ContagemItem_TipoUnidade ,
                                                 AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels ,
                                                 AV133WWContagemItemDS_1_Tfcontagem_codigo ,
                                                 AV134WWContagemItemDS_2_Tfcontagem_codigo_to ,
                                                 AV135WWContagemItemDS_3_Tfcontagemitem_pfb ,
                                                 AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to ,
                                                 AV137WWContagemItemDS_5_Tfcontagemitem_pfl ,
                                                 AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to ,
                                                 AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels.Count ,
                                                 AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel ,
                                                 AV140WWContagemItemDS_8_Tfcontagemitem_evidencias ,
                                                 A192Contagem_Codigo ,
                                                 A950ContagemItem_PFB ,
                                                 A951ContagemItem_PFL ,
                                                 A953ContagemItem_Evidencias ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV140WWContagemItemDS_8_Tfcontagemitem_evidencias = StringUtil.Concat( StringUtil.RTrim( AV140WWContagemItemDS_8_Tfcontagemitem_evidencias), "%", "");
            /* Using cursor H00532 */
            pr_default.execute(0, new Object[] {AV133WWContagemItemDS_1_Tfcontagem_codigo, AV134WWContagemItemDS_2_Tfcontagem_codigo_to, AV135WWContagemItemDS_3_Tfcontagemitem_pfb, AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to, AV137WWContagemItemDS_5_Tfcontagemitem_pfl, AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to, lV140WWContagemItemDS_8_Tfcontagemitem_evidencias, AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_39_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A207Contagem_FabricaSoftwareCod = H00532_A207Contagem_FabricaSoftwareCod[0];
               A224ContagemItem_Lancamento = H00532_A224ContagemItem_Lancamento[0];
               A953ContagemItem_Evidencias = H00532_A953ContagemItem_Evidencias[0];
               n953ContagemItem_Evidencias = H00532_n953ContagemItem_Evidencias[0];
               A952ContagemItem_TipoUnidade = H00532_A952ContagemItem_TipoUnidade[0];
               A951ContagemItem_PFL = H00532_A951ContagemItem_PFL[0];
               n951ContagemItem_PFL = H00532_n951ContagemItem_PFL[0];
               A950ContagemItem_PFB = H00532_A950ContagemItem_PFB[0];
               n950ContagemItem_PFB = H00532_n950ContagemItem_PFB[0];
               A192Contagem_Codigo = H00532_A192Contagem_Codigo[0];
               /* Execute user event: E23532 */
               E23532 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 39;
            WB530( ) ;
         }
         nGXsfl_39_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV133WWContagemItemDS_1_Tfcontagem_codigo = AV108TFContagem_Codigo;
         AV134WWContagemItemDS_2_Tfcontagem_codigo_to = AV109TFContagem_Codigo_To;
         AV135WWContagemItemDS_3_Tfcontagemitem_pfb = AV116TFContagemItem_PFB;
         AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to = AV117TFContagemItem_PFB_To;
         AV137WWContagemItemDS_5_Tfcontagemitem_pfl = AV120TFContagemItem_PFL;
         AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to = AV121TFContagemItem_PFL_To;
         AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels = AV125TFContagemItem_TipoUnidade_Sels;
         AV140WWContagemItemDS_8_Tfcontagemitem_evidencias = AV128TFContagemItem_Evidencias;
         AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel = AV129TFContagemItem_Evidencias_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A952ContagemItem_TipoUnidade ,
                                              AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels ,
                                              AV133WWContagemItemDS_1_Tfcontagem_codigo ,
                                              AV134WWContagemItemDS_2_Tfcontagem_codigo_to ,
                                              AV135WWContagemItemDS_3_Tfcontagemitem_pfb ,
                                              AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to ,
                                              AV137WWContagemItemDS_5_Tfcontagemitem_pfl ,
                                              AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to ,
                                              AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels.Count ,
                                              AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel ,
                                              AV140WWContagemItemDS_8_Tfcontagemitem_evidencias ,
                                              A192Contagem_Codigo ,
                                              A950ContagemItem_PFB ,
                                              A951ContagemItem_PFL ,
                                              A953ContagemItem_Evidencias ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV140WWContagemItemDS_8_Tfcontagemitem_evidencias = StringUtil.Concat( StringUtil.RTrim( AV140WWContagemItemDS_8_Tfcontagemitem_evidencias), "%", "");
         /* Using cursor H00533 */
         pr_default.execute(1, new Object[] {AV133WWContagemItemDS_1_Tfcontagem_codigo, AV134WWContagemItemDS_2_Tfcontagem_codigo_to, AV135WWContagemItemDS_3_Tfcontagemitem_pfb, AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to, AV137WWContagemItemDS_5_Tfcontagemitem_pfl, AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to, lV140WWContagemItemDS_8_Tfcontagemitem_evidencias, AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel});
         GRID_nRecordCount = H00533_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV133WWContagemItemDS_1_Tfcontagem_codigo = AV108TFContagem_Codigo;
         AV134WWContagemItemDS_2_Tfcontagem_codigo_to = AV109TFContagem_Codigo_To;
         AV135WWContagemItemDS_3_Tfcontagemitem_pfb = AV116TFContagemItem_PFB;
         AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to = AV117TFContagemItem_PFB_To;
         AV137WWContagemItemDS_5_Tfcontagemitem_pfl = AV120TFContagemItem_PFL;
         AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to = AV121TFContagemItem_PFL_To;
         AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels = AV125TFContagemItem_TipoUnidade_Sels;
         AV140WWContagemItemDS_8_Tfcontagemitem_evidencias = AV128TFContagemItem_Evidencias;
         AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel = AV129TFContagemItem_Evidencias_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV108TFContagem_Codigo, AV109TFContagem_Codigo_To, AV116TFContagemItem_PFB, AV117TFContagemItem_PFB_To, AV120TFContagemItem_PFL, AV121TFContagemItem_PFL_To, AV128TFContagemItem_Evidencias, AV129TFContagemItem_Evidencias_Sel, AV86ManageFiltersExecutionStep, AV110ddo_Contagem_CodigoTitleControlIdToReplace, AV118ddo_ContagemItem_PFBTitleControlIdToReplace, AV122ddo_ContagemItem_PFLTitleControlIdToReplace, AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace, AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace, AV62StatusItem, AV125TFContagemItem_TipoUnidade_Sels, AV145Pgmname, AV6WWPContext, A224ContagemItem_Lancamento, A192Contagem_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV133WWContagemItemDS_1_Tfcontagem_codigo = AV108TFContagem_Codigo;
         AV134WWContagemItemDS_2_Tfcontagem_codigo_to = AV109TFContagem_Codigo_To;
         AV135WWContagemItemDS_3_Tfcontagemitem_pfb = AV116TFContagemItem_PFB;
         AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to = AV117TFContagemItem_PFB_To;
         AV137WWContagemItemDS_5_Tfcontagemitem_pfl = AV120TFContagemItem_PFL;
         AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to = AV121TFContagemItem_PFL_To;
         AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels = AV125TFContagemItem_TipoUnidade_Sels;
         AV140WWContagemItemDS_8_Tfcontagemitem_evidencias = AV128TFContagemItem_Evidencias;
         AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel = AV129TFContagemItem_Evidencias_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV108TFContagem_Codigo, AV109TFContagem_Codigo_To, AV116TFContagemItem_PFB, AV117TFContagemItem_PFB_To, AV120TFContagemItem_PFL, AV121TFContagemItem_PFL_To, AV128TFContagemItem_Evidencias, AV129TFContagemItem_Evidencias_Sel, AV86ManageFiltersExecutionStep, AV110ddo_Contagem_CodigoTitleControlIdToReplace, AV118ddo_ContagemItem_PFBTitleControlIdToReplace, AV122ddo_ContagemItem_PFLTitleControlIdToReplace, AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace, AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace, AV62StatusItem, AV125TFContagemItem_TipoUnidade_Sels, AV145Pgmname, AV6WWPContext, A224ContagemItem_Lancamento, A192Contagem_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV133WWContagemItemDS_1_Tfcontagem_codigo = AV108TFContagem_Codigo;
         AV134WWContagemItemDS_2_Tfcontagem_codigo_to = AV109TFContagem_Codigo_To;
         AV135WWContagemItemDS_3_Tfcontagemitem_pfb = AV116TFContagemItem_PFB;
         AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to = AV117TFContagemItem_PFB_To;
         AV137WWContagemItemDS_5_Tfcontagemitem_pfl = AV120TFContagemItem_PFL;
         AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to = AV121TFContagemItem_PFL_To;
         AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels = AV125TFContagemItem_TipoUnidade_Sels;
         AV140WWContagemItemDS_8_Tfcontagemitem_evidencias = AV128TFContagemItem_Evidencias;
         AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel = AV129TFContagemItem_Evidencias_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV108TFContagem_Codigo, AV109TFContagem_Codigo_To, AV116TFContagemItem_PFB, AV117TFContagemItem_PFB_To, AV120TFContagemItem_PFL, AV121TFContagemItem_PFL_To, AV128TFContagemItem_Evidencias, AV129TFContagemItem_Evidencias_Sel, AV86ManageFiltersExecutionStep, AV110ddo_Contagem_CodigoTitleControlIdToReplace, AV118ddo_ContagemItem_PFBTitleControlIdToReplace, AV122ddo_ContagemItem_PFLTitleControlIdToReplace, AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace, AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace, AV62StatusItem, AV125TFContagemItem_TipoUnidade_Sels, AV145Pgmname, AV6WWPContext, A224ContagemItem_Lancamento, A192Contagem_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV133WWContagemItemDS_1_Tfcontagem_codigo = AV108TFContagem_Codigo;
         AV134WWContagemItemDS_2_Tfcontagem_codigo_to = AV109TFContagem_Codigo_To;
         AV135WWContagemItemDS_3_Tfcontagemitem_pfb = AV116TFContagemItem_PFB;
         AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to = AV117TFContagemItem_PFB_To;
         AV137WWContagemItemDS_5_Tfcontagemitem_pfl = AV120TFContagemItem_PFL;
         AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to = AV121TFContagemItem_PFL_To;
         AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels = AV125TFContagemItem_TipoUnidade_Sels;
         AV140WWContagemItemDS_8_Tfcontagemitem_evidencias = AV128TFContagemItem_Evidencias;
         AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel = AV129TFContagemItem_Evidencias_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV108TFContagem_Codigo, AV109TFContagem_Codigo_To, AV116TFContagemItem_PFB, AV117TFContagemItem_PFB_To, AV120TFContagemItem_PFL, AV121TFContagemItem_PFL_To, AV128TFContagemItem_Evidencias, AV129TFContagemItem_Evidencias_Sel, AV86ManageFiltersExecutionStep, AV110ddo_Contagem_CodigoTitleControlIdToReplace, AV118ddo_ContagemItem_PFBTitleControlIdToReplace, AV122ddo_ContagemItem_PFLTitleControlIdToReplace, AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace, AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace, AV62StatusItem, AV125TFContagemItem_TipoUnidade_Sels, AV145Pgmname, AV6WWPContext, A224ContagemItem_Lancamento, A192Contagem_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV133WWContagemItemDS_1_Tfcontagem_codigo = AV108TFContagem_Codigo;
         AV134WWContagemItemDS_2_Tfcontagem_codigo_to = AV109TFContagem_Codigo_To;
         AV135WWContagemItemDS_3_Tfcontagemitem_pfb = AV116TFContagemItem_PFB;
         AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to = AV117TFContagemItem_PFB_To;
         AV137WWContagemItemDS_5_Tfcontagemitem_pfl = AV120TFContagemItem_PFL;
         AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to = AV121TFContagemItem_PFL_To;
         AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels = AV125TFContagemItem_TipoUnidade_Sels;
         AV140WWContagemItemDS_8_Tfcontagemitem_evidencias = AV128TFContagemItem_Evidencias;
         AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel = AV129TFContagemItem_Evidencias_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV108TFContagem_Codigo, AV109TFContagem_Codigo_To, AV116TFContagemItem_PFB, AV117TFContagemItem_PFB_To, AV120TFContagemItem_PFL, AV121TFContagemItem_PFL_To, AV128TFContagemItem_Evidencias, AV129TFContagemItem_Evidencias_Sel, AV86ManageFiltersExecutionStep, AV110ddo_Contagem_CodigoTitleControlIdToReplace, AV118ddo_ContagemItem_PFBTitleControlIdToReplace, AV122ddo_ContagemItem_PFLTitleControlIdToReplace, AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace, AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace, AV62StatusItem, AV125TFContagemItem_TipoUnidade_Sels, AV145Pgmname, AV6WWPContext, A224ContagemItem_Lancamento, A192Contagem_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP530( )
      {
         /* Before Start, stand alone formulas. */
         AV145Pgmname = "WWContagemItem";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E21532 */
         E21532 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV90ManageFiltersData);
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV111DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEM_CODIGOTITLEFILTERDATA"), AV107Contagem_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMITEM_PFBTITLEFILTERDATA"), AV115ContagemItem_PFBTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMITEM_PFLTITLEFILTERDATA"), AV119ContagemItem_PFLTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMITEM_TIPOUNIDADETITLEFILTERDATA"), AV123ContagemItem_TipoUnidadeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMITEM_EVIDENCIASTITLEFILTERDATA"), AV127ContagemItem_EvidenciasTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vNOTIFICATIONINFO"), AV85NotificationInfo);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV86ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV86ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV86ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV86ManageFiltersExecutionStep), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagem_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagem_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEM_CODIGO");
               GX_FocusControl = edtavTfcontagem_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV108TFContagem_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV108TFContagem_Codigo), 6, 0)));
            }
            else
            {
               AV108TFContagem_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagem_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV108TFContagem_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagem_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagem_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEM_CODIGO_TO");
               GX_FocusControl = edtavTfcontagem_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV109TFContagem_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFContagem_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV109TFContagem_Codigo_To), 6, 0)));
            }
            else
            {
               AV109TFContagem_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagem_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFContagem_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV109TFContagem_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitem_pfb_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitem_pfb_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMITEM_PFB");
               GX_FocusControl = edtavTfcontagemitem_pfb_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV116TFContagemItem_PFB = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116TFContagemItem_PFB", StringUtil.LTrim( StringUtil.Str( AV116TFContagemItem_PFB, 14, 5)));
            }
            else
            {
               AV116TFContagemItem_PFB = context.localUtil.CToN( cgiGet( edtavTfcontagemitem_pfb_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116TFContagemItem_PFB", StringUtil.LTrim( StringUtil.Str( AV116TFContagemItem_PFB, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitem_pfb_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitem_pfb_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMITEM_PFB_TO");
               GX_FocusControl = edtavTfcontagemitem_pfb_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV117TFContagemItem_PFB_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117TFContagemItem_PFB_To", StringUtil.LTrim( StringUtil.Str( AV117TFContagemItem_PFB_To, 14, 5)));
            }
            else
            {
               AV117TFContagemItem_PFB_To = context.localUtil.CToN( cgiGet( edtavTfcontagemitem_pfb_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117TFContagemItem_PFB_To", StringUtil.LTrim( StringUtil.Str( AV117TFContagemItem_PFB_To, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitem_pfl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitem_pfl_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMITEM_PFL");
               GX_FocusControl = edtavTfcontagemitem_pfl_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV120TFContagemItem_PFL = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120TFContagemItem_PFL", StringUtil.LTrim( StringUtil.Str( AV120TFContagemItem_PFL, 14, 5)));
            }
            else
            {
               AV120TFContagemItem_PFL = context.localUtil.CToN( cgiGet( edtavTfcontagemitem_pfl_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120TFContagemItem_PFL", StringUtil.LTrim( StringUtil.Str( AV120TFContagemItem_PFL, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitem_pfl_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemitem_pfl_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMITEM_PFL_TO");
               GX_FocusControl = edtavTfcontagemitem_pfl_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV121TFContagemItem_PFL_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFContagemItem_PFL_To", StringUtil.LTrim( StringUtil.Str( AV121TFContagemItem_PFL_To, 14, 5)));
            }
            else
            {
               AV121TFContagemItem_PFL_To = context.localUtil.CToN( cgiGet( edtavTfcontagemitem_pfl_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFContagemItem_PFL_To", StringUtil.LTrim( StringUtil.Str( AV121TFContagemItem_PFL_To, 14, 5)));
            }
            AV128TFContagemItem_Evidencias = cgiGet( edtavTfcontagemitem_evidencias_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV128TFContagemItem_Evidencias", AV128TFContagemItem_Evidencias);
            AV129TFContagemItem_Evidencias_Sel = cgiGet( edtavTfcontagemitem_evidencias_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV129TFContagemItem_Evidencias_Sel", AV129TFContagemItem_Evidencias_Sel);
            AV110ddo_Contagem_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contagem_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110ddo_Contagem_CodigoTitleControlIdToReplace", AV110ddo_Contagem_CodigoTitleControlIdToReplace);
            AV118ddo_ContagemItem_PFBTitleControlIdToReplace = cgiGet( edtavDdo_contagemitem_pfbtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118ddo_ContagemItem_PFBTitleControlIdToReplace", AV118ddo_ContagemItem_PFBTitleControlIdToReplace);
            AV122ddo_ContagemItem_PFLTitleControlIdToReplace = cgiGet( edtavDdo_contagemitem_pfltitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122ddo_ContagemItem_PFLTitleControlIdToReplace", AV122ddo_ContagemItem_PFLTitleControlIdToReplace);
            AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace = cgiGet( edtavDdo_contagemitem_tipounidadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace", AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace);
            AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace = cgiGet( edtavDdo_contagemitem_evidenciastitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace", AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_39 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_39"), ",", "."));
            AV113GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV114GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contagem_codigo_Caption = cgiGet( "DDO_CONTAGEM_CODIGO_Caption");
            Ddo_contagem_codigo_Tooltip = cgiGet( "DDO_CONTAGEM_CODIGO_Tooltip");
            Ddo_contagem_codigo_Cls = cgiGet( "DDO_CONTAGEM_CODIGO_Cls");
            Ddo_contagem_codigo_Filteredtext_set = cgiGet( "DDO_CONTAGEM_CODIGO_Filteredtext_set");
            Ddo_contagem_codigo_Filteredtextto_set = cgiGet( "DDO_CONTAGEM_CODIGO_Filteredtextto_set");
            Ddo_contagem_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTAGEM_CODIGO_Dropdownoptionstype");
            Ddo_contagem_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEM_CODIGO_Titlecontrolidtoreplace");
            Ddo_contagem_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEM_CODIGO_Includesortasc"));
            Ddo_contagem_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEM_CODIGO_Includesortdsc"));
            Ddo_contagem_codigo_Sortedstatus = cgiGet( "DDO_CONTAGEM_CODIGO_Sortedstatus");
            Ddo_contagem_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEM_CODIGO_Includefilter"));
            Ddo_contagem_codigo_Filtertype = cgiGet( "DDO_CONTAGEM_CODIGO_Filtertype");
            Ddo_contagem_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEM_CODIGO_Filterisrange"));
            Ddo_contagem_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEM_CODIGO_Includedatalist"));
            Ddo_contagem_codigo_Sortasc = cgiGet( "DDO_CONTAGEM_CODIGO_Sortasc");
            Ddo_contagem_codigo_Sortdsc = cgiGet( "DDO_CONTAGEM_CODIGO_Sortdsc");
            Ddo_contagem_codigo_Cleanfilter = cgiGet( "DDO_CONTAGEM_CODIGO_Cleanfilter");
            Ddo_contagem_codigo_Rangefilterfrom = cgiGet( "DDO_CONTAGEM_CODIGO_Rangefilterfrom");
            Ddo_contagem_codigo_Rangefilterto = cgiGet( "DDO_CONTAGEM_CODIGO_Rangefilterto");
            Ddo_contagem_codigo_Searchbuttontext = cgiGet( "DDO_CONTAGEM_CODIGO_Searchbuttontext");
            Ddo_contagemitem_pfb_Caption = cgiGet( "DDO_CONTAGEMITEM_PFB_Caption");
            Ddo_contagemitem_pfb_Tooltip = cgiGet( "DDO_CONTAGEMITEM_PFB_Tooltip");
            Ddo_contagemitem_pfb_Cls = cgiGet( "DDO_CONTAGEMITEM_PFB_Cls");
            Ddo_contagemitem_pfb_Filteredtext_set = cgiGet( "DDO_CONTAGEMITEM_PFB_Filteredtext_set");
            Ddo_contagemitem_pfb_Filteredtextto_set = cgiGet( "DDO_CONTAGEMITEM_PFB_Filteredtextto_set");
            Ddo_contagemitem_pfb_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMITEM_PFB_Dropdownoptionstype");
            Ddo_contagemitem_pfb_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMITEM_PFB_Titlecontrolidtoreplace");
            Ddo_contagemitem_pfb_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_PFB_Includesortasc"));
            Ddo_contagemitem_pfb_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_PFB_Includesortdsc"));
            Ddo_contagemitem_pfb_Sortedstatus = cgiGet( "DDO_CONTAGEMITEM_PFB_Sortedstatus");
            Ddo_contagemitem_pfb_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_PFB_Includefilter"));
            Ddo_contagemitem_pfb_Filtertype = cgiGet( "DDO_CONTAGEMITEM_PFB_Filtertype");
            Ddo_contagemitem_pfb_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_PFB_Filterisrange"));
            Ddo_contagemitem_pfb_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_PFB_Includedatalist"));
            Ddo_contagemitem_pfb_Sortasc = cgiGet( "DDO_CONTAGEMITEM_PFB_Sortasc");
            Ddo_contagemitem_pfb_Sortdsc = cgiGet( "DDO_CONTAGEMITEM_PFB_Sortdsc");
            Ddo_contagemitem_pfb_Cleanfilter = cgiGet( "DDO_CONTAGEMITEM_PFB_Cleanfilter");
            Ddo_contagemitem_pfb_Rangefilterfrom = cgiGet( "DDO_CONTAGEMITEM_PFB_Rangefilterfrom");
            Ddo_contagemitem_pfb_Rangefilterto = cgiGet( "DDO_CONTAGEMITEM_PFB_Rangefilterto");
            Ddo_contagemitem_pfb_Searchbuttontext = cgiGet( "DDO_CONTAGEMITEM_PFB_Searchbuttontext");
            Ddo_contagemitem_pfl_Caption = cgiGet( "DDO_CONTAGEMITEM_PFL_Caption");
            Ddo_contagemitem_pfl_Tooltip = cgiGet( "DDO_CONTAGEMITEM_PFL_Tooltip");
            Ddo_contagemitem_pfl_Cls = cgiGet( "DDO_CONTAGEMITEM_PFL_Cls");
            Ddo_contagemitem_pfl_Filteredtext_set = cgiGet( "DDO_CONTAGEMITEM_PFL_Filteredtext_set");
            Ddo_contagemitem_pfl_Filteredtextto_set = cgiGet( "DDO_CONTAGEMITEM_PFL_Filteredtextto_set");
            Ddo_contagemitem_pfl_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMITEM_PFL_Dropdownoptionstype");
            Ddo_contagemitem_pfl_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMITEM_PFL_Titlecontrolidtoreplace");
            Ddo_contagemitem_pfl_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_PFL_Includesortasc"));
            Ddo_contagemitem_pfl_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_PFL_Includesortdsc"));
            Ddo_contagemitem_pfl_Sortedstatus = cgiGet( "DDO_CONTAGEMITEM_PFL_Sortedstatus");
            Ddo_contagemitem_pfl_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_PFL_Includefilter"));
            Ddo_contagemitem_pfl_Filtertype = cgiGet( "DDO_CONTAGEMITEM_PFL_Filtertype");
            Ddo_contagemitem_pfl_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_PFL_Filterisrange"));
            Ddo_contagemitem_pfl_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_PFL_Includedatalist"));
            Ddo_contagemitem_pfl_Sortasc = cgiGet( "DDO_CONTAGEMITEM_PFL_Sortasc");
            Ddo_contagemitem_pfl_Sortdsc = cgiGet( "DDO_CONTAGEMITEM_PFL_Sortdsc");
            Ddo_contagemitem_pfl_Cleanfilter = cgiGet( "DDO_CONTAGEMITEM_PFL_Cleanfilter");
            Ddo_contagemitem_pfl_Rangefilterfrom = cgiGet( "DDO_CONTAGEMITEM_PFL_Rangefilterfrom");
            Ddo_contagemitem_pfl_Rangefilterto = cgiGet( "DDO_CONTAGEMITEM_PFL_Rangefilterto");
            Ddo_contagemitem_pfl_Searchbuttontext = cgiGet( "DDO_CONTAGEMITEM_PFL_Searchbuttontext");
            Ddo_contagemitem_tipounidade_Caption = cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Caption");
            Ddo_contagemitem_tipounidade_Tooltip = cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Tooltip");
            Ddo_contagemitem_tipounidade_Cls = cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Cls");
            Ddo_contagemitem_tipounidade_Selectedvalue_set = cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Selectedvalue_set");
            Ddo_contagemitem_tipounidade_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Dropdownoptionstype");
            Ddo_contagemitem_tipounidade_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Titlecontrolidtoreplace");
            Ddo_contagemitem_tipounidade_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Includesortasc"));
            Ddo_contagemitem_tipounidade_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Includesortdsc"));
            Ddo_contagemitem_tipounidade_Sortedstatus = cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Sortedstatus");
            Ddo_contagemitem_tipounidade_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Includefilter"));
            Ddo_contagemitem_tipounidade_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Includedatalist"));
            Ddo_contagemitem_tipounidade_Datalisttype = cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Datalisttype");
            Ddo_contagemitem_tipounidade_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Allowmultipleselection"));
            Ddo_contagemitem_tipounidade_Datalistfixedvalues = cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Datalistfixedvalues");
            Ddo_contagemitem_tipounidade_Sortasc = cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Sortasc");
            Ddo_contagemitem_tipounidade_Sortdsc = cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Sortdsc");
            Ddo_contagemitem_tipounidade_Cleanfilter = cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Cleanfilter");
            Ddo_contagemitem_tipounidade_Searchbuttontext = cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Searchbuttontext");
            Ddo_contagemitem_evidencias_Caption = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Caption");
            Ddo_contagemitem_evidencias_Tooltip = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Tooltip");
            Ddo_contagemitem_evidencias_Cls = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Cls");
            Ddo_contagemitem_evidencias_Filteredtext_set = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Filteredtext_set");
            Ddo_contagemitem_evidencias_Selectedvalue_set = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Selectedvalue_set");
            Ddo_contagemitem_evidencias_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Dropdownoptionstype");
            Ddo_contagemitem_evidencias_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Titlecontrolidtoreplace");
            Ddo_contagemitem_evidencias_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Includesortasc"));
            Ddo_contagemitem_evidencias_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Includesortdsc"));
            Ddo_contagemitem_evidencias_Sortedstatus = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Sortedstatus");
            Ddo_contagemitem_evidencias_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Includefilter"));
            Ddo_contagemitem_evidencias_Filtertype = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Filtertype");
            Ddo_contagemitem_evidencias_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Filterisrange"));
            Ddo_contagemitem_evidencias_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Includedatalist"));
            Ddo_contagemitem_evidencias_Datalisttype = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Datalisttype");
            Ddo_contagemitem_evidencias_Datalistproc = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Datalistproc");
            Ddo_contagemitem_evidencias_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemitem_evidencias_Sortasc = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Sortasc");
            Ddo_contagemitem_evidencias_Sortdsc = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Sortdsc");
            Ddo_contagemitem_evidencias_Loadingdata = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Loadingdata");
            Ddo_contagemitem_evidencias_Cleanfilter = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Cleanfilter");
            Ddo_contagemitem_evidencias_Noresultsfound = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Noresultsfound");
            Ddo_contagemitem_evidencias_Searchbuttontext = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contagem_codigo_Activeeventkey = cgiGet( "DDO_CONTAGEM_CODIGO_Activeeventkey");
            Ddo_contagem_codigo_Filteredtext_get = cgiGet( "DDO_CONTAGEM_CODIGO_Filteredtext_get");
            Ddo_contagem_codigo_Filteredtextto_get = cgiGet( "DDO_CONTAGEM_CODIGO_Filteredtextto_get");
            Ddo_contagemitem_pfb_Activeeventkey = cgiGet( "DDO_CONTAGEMITEM_PFB_Activeeventkey");
            Ddo_contagemitem_pfb_Filteredtext_get = cgiGet( "DDO_CONTAGEMITEM_PFB_Filteredtext_get");
            Ddo_contagemitem_pfb_Filteredtextto_get = cgiGet( "DDO_CONTAGEMITEM_PFB_Filteredtextto_get");
            Ddo_contagemitem_pfl_Activeeventkey = cgiGet( "DDO_CONTAGEMITEM_PFL_Activeeventkey");
            Ddo_contagemitem_pfl_Filteredtext_get = cgiGet( "DDO_CONTAGEMITEM_PFL_Filteredtext_get");
            Ddo_contagemitem_pfl_Filteredtextto_get = cgiGet( "DDO_CONTAGEMITEM_PFL_Filteredtextto_get");
            Ddo_contagemitem_tipounidade_Activeeventkey = cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Activeeventkey");
            Ddo_contagemitem_tipounidade_Selectedvalue_get = cgiGet( "DDO_CONTAGEMITEM_TIPOUNIDADE_Selectedvalue_get");
            Ddo_contagemitem_evidencias_Activeeventkey = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Activeeventkey");
            Ddo_contagemitem_evidencias_Filteredtext_get = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Filteredtext_get");
            Ddo_contagemitem_evidencias_Selectedvalue_get = cgiGet( "DDO_CONTAGEMITEM_EVIDENCIAS_Selectedvalue_get");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEM_CODIGO"), ",", ".") != Convert.ToDecimal( AV108TFContagem_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEM_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV109TFContagem_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEM_PFB"), ",", ".") != AV116TFContagemItem_PFB )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEM_PFB_TO"), ",", ".") != AV117TFContagemItem_PFB_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEM_PFL"), ",", ".") != AV120TFContagemItem_PFL )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMITEM_PFL_TO"), ",", ".") != AV121TFContagemItem_PFL_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMITEM_EVIDENCIAS"), AV128TFContagemItem_Evidencias) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMITEM_EVIDENCIAS_SEL"), AV129TFContagemItem_Evidencias_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E21532 */
         E21532 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21532( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         edtavTfcontagem_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagem_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagem_codigo_Visible), 5, 0)));
         edtavTfcontagem_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagem_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagem_codigo_to_Visible), 5, 0)));
         edtavTfcontagemitem_pfb_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitem_pfb_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitem_pfb_Visible), 5, 0)));
         edtavTfcontagemitem_pfb_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitem_pfb_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitem_pfb_to_Visible), 5, 0)));
         edtavTfcontagemitem_pfl_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitem_pfl_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitem_pfl_Visible), 5, 0)));
         edtavTfcontagemitem_pfl_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitem_pfl_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitem_pfl_to_Visible), 5, 0)));
         edtavTfcontagemitem_evidencias_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitem_evidencias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitem_evidencias_Visible), 5, 0)));
         edtavTfcontagemitem_evidencias_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemitem_evidencias_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemitem_evidencias_sel_Visible), 5, 0)));
         Ddo_contagem_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Contagem_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagem_codigo_Internalname, "TitleControlIdToReplace", Ddo_contagem_codigo_Titlecontrolidtoreplace);
         AV110ddo_Contagem_CodigoTitleControlIdToReplace = Ddo_contagem_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110ddo_Contagem_CodigoTitleControlIdToReplace", AV110ddo_Contagem_CodigoTitleControlIdToReplace);
         edtavDdo_contagem_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagem_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagem_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemitem_pfb_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemItem_PFB";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_pfb_Internalname, "TitleControlIdToReplace", Ddo_contagemitem_pfb_Titlecontrolidtoreplace);
         AV118ddo_ContagemItem_PFBTitleControlIdToReplace = Ddo_contagemitem_pfb_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118ddo_ContagemItem_PFBTitleControlIdToReplace", AV118ddo_ContagemItem_PFBTitleControlIdToReplace);
         edtavDdo_contagemitem_pfbtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemitem_pfbtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemitem_pfbtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemitem_pfl_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemItem_PFL";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_pfl_Internalname, "TitleControlIdToReplace", Ddo_contagemitem_pfl_Titlecontrolidtoreplace);
         AV122ddo_ContagemItem_PFLTitleControlIdToReplace = Ddo_contagemitem_pfl_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122ddo_ContagemItem_PFLTitleControlIdToReplace", AV122ddo_ContagemItem_PFLTitleControlIdToReplace);
         edtavDdo_contagemitem_pfltitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemitem_pfltitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemitem_pfltitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemitem_tipounidade_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemItem_TipoUnidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_tipounidade_Internalname, "TitleControlIdToReplace", Ddo_contagemitem_tipounidade_Titlecontrolidtoreplace);
         AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace = Ddo_contagemitem_tipounidade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace", AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace);
         edtavDdo_contagemitem_tipounidadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemitem_tipounidadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemitem_tipounidadetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemitem_evidencias_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemItem_Evidencias";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_evidencias_Internalname, "TitleControlIdToReplace", Ddo_contagemitem_evidencias_Titlecontrolidtoreplace);
         AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace = Ddo_contagemitem_evidencias_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace", AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace);
         edtavDdo_contagemitem_evidenciastitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemitem_evidenciastitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemitem_evidenciastitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Contagem Item";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Contratada F�brica", 0);
         cmbavOrderedby.addItem("2", "Contagem", 0);
         cmbavOrderedby.addItem("3", "PFB", 0);
         cmbavOrderedby.addItem("4", "PFL", 0);
         cmbavOrderedby.addItem("5", "Tipo", 0);
         cmbavOrderedby.addItem("6", "Evidencias", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV111DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV111DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         AV84Contagem_AreaTrabalhoCod = AV6WWPContext.gxTpr_Areatrabalho_codigo;
         AV58ContagemItem_ValidacaoStatusFinal = AV62StatusItem;
      }

      protected void E22532( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV107Contagem_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV115ContagemItem_PFBTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV119ContagemItem_PFLTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV123ContagemItem_TipoUnidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV127ContagemItem_EvidenciasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         if ( AV86ManageFiltersExecutionStep == 1 )
         {
            AV86ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV86ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV86ManageFiltersExecutionStep == 2 )
         {
            AV86ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV86ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContagem_Codigo_Titleformat = 2;
         edtContagem_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contagem", AV110ddo_Contagem_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Codigo_Internalname, "Title", edtContagem_Codigo_Title);
         edtContagemItem_PFB_Titleformat = 2;
         edtContagemItem_PFB_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "PFB", AV118ddo_ContagemItem_PFBTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_PFB_Internalname, "Title", edtContagemItem_PFB_Title);
         edtContagemItem_PFL_Titleformat = 2;
         edtContagemItem_PFL_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "PFL", AV122ddo_ContagemItem_PFLTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_PFL_Internalname, "Title", edtContagemItem_PFL_Title);
         cmbContagemItem_TipoUnidade_Titleformat = 2;
         cmbContagemItem_TipoUnidade.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemItem_TipoUnidade_Internalname, "Title", cmbContagemItem_TipoUnidade.Title.Text);
         edtContagemItem_Evidencias_Titleformat = 2;
         edtContagemItem_Evidencias_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Evidencias", AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Evidencias_Internalname, "Title", edtContagemItem_Evidencias_Title);
         AV113GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV113GridCurrentPage), 10, 0)));
         AV114GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV114GridPageCount), 10, 0)));
         AV84Contagem_AreaTrabalhoCod = AV6WWPContext.gxTpr_Areatrabalho_codigo;
         if ( AV62StatusItem == 1 )
         {
            lblContagemitemtitle_Caption = "Caixa de Sa�da - Contagens";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemitemtitle_Internalname, "Caption", lblContagemitemtitle_Caption);
         }
         else if ( AV62StatusItem == 2 )
         {
            lblContagemitemtitle_Caption = "Caixa de Entrada - Contagens";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemitemtitle_Internalname, "Caption", lblContagemitemtitle_Caption);
         }
         AV133WWContagemItemDS_1_Tfcontagem_codigo = AV108TFContagem_Codigo;
         AV134WWContagemItemDS_2_Tfcontagem_codigo_to = AV109TFContagem_Codigo_To;
         AV135WWContagemItemDS_3_Tfcontagemitem_pfb = AV116TFContagemItem_PFB;
         AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to = AV117TFContagemItem_PFB_To;
         AV137WWContagemItemDS_5_Tfcontagemitem_pfl = AV120TFContagemItem_PFL;
         AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to = AV121TFContagemItem_PFL_To;
         AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels = AV125TFContagemItem_TipoUnidade_Sels;
         AV140WWContagemItemDS_8_Tfcontagemitem_evidencias = AV128TFContagemItem_Evidencias;
         AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel = AV129TFContagemItem_Evidencias_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV107Contagem_CodigoTitleFilterData", AV107Contagem_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV115ContagemItem_PFBTitleFilterData", AV115ContagemItem_PFBTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV119ContagemItem_PFLTitleFilterData", AV119ContagemItem_PFLTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV123ContagemItem_TipoUnidadeTitleFilterData", AV123ContagemItem_TipoUnidadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV127ContagemItem_EvidenciasTitleFilterData", AV127ContagemItem_EvidenciasTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV90ManageFiltersData", AV90ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E12532( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV112PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV112PageToGo) ;
         }
      }

      protected void E13532( )
      {
         /* Ddo_contagem_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagem_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagem_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagem_codigo_Internalname, "SortedStatus", Ddo_contagem_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagem_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagem_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagem_codigo_Internalname, "SortedStatus", Ddo_contagem_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagem_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV108TFContagem_Codigo = (int)(NumberUtil.Val( Ddo_contagem_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV108TFContagem_Codigo), 6, 0)));
            AV109TFContagem_Codigo_To = (int)(NumberUtil.Val( Ddo_contagem_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFContagem_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV109TFContagem_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14532( )
      {
         /* Ddo_contagemitem_pfb_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemitem_pfb_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitem_pfb_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_pfb_Internalname, "SortedStatus", Ddo_contagemitem_pfb_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitem_pfb_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitem_pfb_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_pfb_Internalname, "SortedStatus", Ddo_contagemitem_pfb_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitem_pfb_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV116TFContagemItem_PFB = NumberUtil.Val( Ddo_contagemitem_pfb_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116TFContagemItem_PFB", StringUtil.LTrim( StringUtil.Str( AV116TFContagemItem_PFB, 14, 5)));
            AV117TFContagemItem_PFB_To = NumberUtil.Val( Ddo_contagemitem_pfb_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117TFContagemItem_PFB_To", StringUtil.LTrim( StringUtil.Str( AV117TFContagemItem_PFB_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15532( )
      {
         /* Ddo_contagemitem_pfl_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemitem_pfl_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitem_pfl_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_pfl_Internalname, "SortedStatus", Ddo_contagemitem_pfl_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitem_pfl_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitem_pfl_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_pfl_Internalname, "SortedStatus", Ddo_contagemitem_pfl_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitem_pfl_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV120TFContagemItem_PFL = NumberUtil.Val( Ddo_contagemitem_pfl_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120TFContagemItem_PFL", StringUtil.LTrim( StringUtil.Str( AV120TFContagemItem_PFL, 14, 5)));
            AV121TFContagemItem_PFL_To = NumberUtil.Val( Ddo_contagemitem_pfl_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFContagemItem_PFL_To", StringUtil.LTrim( StringUtil.Str( AV121TFContagemItem_PFL_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16532( )
      {
         /* Ddo_contagemitem_tipounidade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemitem_tipounidade_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitem_tipounidade_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_tipounidade_Internalname, "SortedStatus", Ddo_contagemitem_tipounidade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitem_tipounidade_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitem_tipounidade_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_tipounidade_Internalname, "SortedStatus", Ddo_contagemitem_tipounidade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitem_tipounidade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV124TFContagemItem_TipoUnidade_SelsJson = Ddo_contagemitem_tipounidade_Selectedvalue_get;
            AV125TFContagemItem_TipoUnidade_Sels.FromJSonString(AV124TFContagemItem_TipoUnidade_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV125TFContagemItem_TipoUnidade_Sels", AV125TFContagemItem_TipoUnidade_Sels);
      }

      protected void E17532( )
      {
         /* Ddo_contagemitem_evidencias_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemitem_evidencias_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitem_evidencias_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_evidencias_Internalname, "SortedStatus", Ddo_contagemitem_evidencias_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitem_evidencias_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemitem_evidencias_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_evidencias_Internalname, "SortedStatus", Ddo_contagemitem_evidencias_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemitem_evidencias_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV128TFContagemItem_Evidencias = Ddo_contagemitem_evidencias_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV128TFContagemItem_Evidencias", AV128TFContagemItem_Evidencias);
            AV129TFContagemItem_Evidencias_Sel = Ddo_contagemitem_evidencias_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV129TFContagemItem_Evidencias_Sel", AV129TFContagemItem_Evidencias_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E23532( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("contagemitem.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A224ContagemItem_Lancamento) + "," + UrlEncode("" +A192Contagem_Codigo);
            AV31Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV31Update);
            AV142Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV31Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV31Update);
            AV142Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("contagemitem.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A224ContagemItem_Lancamento) + "," + UrlEncode("" +A192Contagem_Codigo);
            AV32Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
            AV143Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV32Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
            AV143Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewcontagemitem.aspx") + "?" + UrlEncode("" +A224ContagemItem_Lancamento) + "," + UrlEncode(StringUtil.RTrim(""));
            AV55Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV55Display);
            AV144Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV55Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV55Display);
            AV144Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 39;
         }
         sendrow_392( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_39_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(39, GridRow);
         }
      }

      protected void E18532( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E11532( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWContagemItemFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika"))), new Object[] {});
            AV86ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV86ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWContagemItemFilters")), new Object[] {});
            AV86ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV86ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char2 = AV87ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWContagemItemFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV87ManageFiltersXml = GXt_char2;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV87ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV145Pgmname+"GridState",  AV87ManageFiltersXml) ;
               AV10GridState.FromXml(AV87ManageFiltersXml, "");
               AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
               S142 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
               S182 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV125TFContagemItem_TipoUnidade_Sels", AV125TFContagemItem_TipoUnidade_Sels);
      }

      protected void E19532( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV125TFContagemItem_TipoUnidade_Sels", AV125TFContagemItem_TipoUnidade_Sels);
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contagem_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagem_codigo_Internalname, "SortedStatus", Ddo_contagem_codigo_Sortedstatus);
         Ddo_contagemitem_pfb_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_pfb_Internalname, "SortedStatus", Ddo_contagemitem_pfb_Sortedstatus);
         Ddo_contagemitem_pfl_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_pfl_Internalname, "SortedStatus", Ddo_contagemitem_pfl_Sortedstatus);
         Ddo_contagemitem_tipounidade_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_tipounidade_Internalname, "SortedStatus", Ddo_contagemitem_tipounidade_Sortedstatus);
         Ddo_contagemitem_evidencias_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_evidencias_Internalname, "SortedStatus", Ddo_contagemitem_evidencias_Sortedstatus);
      }

      protected void S142( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_contagem_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagem_codigo_Internalname, "SortedStatus", Ddo_contagem_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contagemitem_pfb_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_pfb_Internalname, "SortedStatus", Ddo_contagemitem_pfb_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contagemitem_pfl_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_pfl_Internalname, "SortedStatus", Ddo_contagemitem_pfl_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contagemitem_tipounidade_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_tipounidade_Internalname, "SortedStatus", Ddo_contagemitem_tipounidade_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_contagemitem_evidencias_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_evidencias_Internalname, "SortedStatus", Ddo_contagemitem_evidencias_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV90ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV91ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV91ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV91ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV91ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV91ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV90ManageFiltersData.Add(AV91ManageFiltersDataItem, 0);
         AV91ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV91ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV91ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV91ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV91ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV90ManageFiltersData.Add(AV91ManageFiltersDataItem, 0);
         AV91ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV91ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV90ManageFiltersData.Add(AV91ManageFiltersDataItem, 0);
         AV88ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWContagemItemFilters"), "");
         AV146GXV1 = 1;
         while ( AV146GXV1 <= AV88ManageFiltersItems.Count )
         {
            AV89ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV88ManageFiltersItems.Item(AV146GXV1));
            AV91ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV91ManageFiltersDataItem.gxTpr_Title = AV89ManageFiltersItem.gxTpr_Title;
            AV91ManageFiltersDataItem.gxTpr_Eventkey = AV89ManageFiltersItem.gxTpr_Title;
            AV91ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV90ManageFiltersData.Add(AV91ManageFiltersDataItem, 0);
            if ( AV90ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV146GXV1 = (int)(AV146GXV1+1);
         }
         if ( AV90ManageFiltersData.Count > 3 )
         {
            AV91ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV91ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV90ManageFiltersData.Add(AV91ManageFiltersDataItem, 0);
            AV91ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV91ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV91ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV91ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV91ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV91ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV90ManageFiltersData.Add(AV91ManageFiltersDataItem, 0);
         }
      }

      protected void S172( )
      {
         /* 'CLEANFILTERS' Routine */
         AV108TFContagem_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV108TFContagem_Codigo), 6, 0)));
         Ddo_contagem_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagem_codigo_Internalname, "FilteredText_set", Ddo_contagem_codigo_Filteredtext_set);
         AV109TFContagem_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFContagem_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV109TFContagem_Codigo_To), 6, 0)));
         Ddo_contagem_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagem_codigo_Internalname, "FilteredTextTo_set", Ddo_contagem_codigo_Filteredtextto_set);
         AV116TFContagemItem_PFB = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116TFContagemItem_PFB", StringUtil.LTrim( StringUtil.Str( AV116TFContagemItem_PFB, 14, 5)));
         Ddo_contagemitem_pfb_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_pfb_Internalname, "FilteredText_set", Ddo_contagemitem_pfb_Filteredtext_set);
         AV117TFContagemItem_PFB_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117TFContagemItem_PFB_To", StringUtil.LTrim( StringUtil.Str( AV117TFContagemItem_PFB_To, 14, 5)));
         Ddo_contagemitem_pfb_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_pfb_Internalname, "FilteredTextTo_set", Ddo_contagemitem_pfb_Filteredtextto_set);
         AV120TFContagemItem_PFL = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120TFContagemItem_PFL", StringUtil.LTrim( StringUtil.Str( AV120TFContagemItem_PFL, 14, 5)));
         Ddo_contagemitem_pfl_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_pfl_Internalname, "FilteredText_set", Ddo_contagemitem_pfl_Filteredtext_set);
         AV121TFContagemItem_PFL_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFContagemItem_PFL_To", StringUtil.LTrim( StringUtil.Str( AV121TFContagemItem_PFL_To, 14, 5)));
         Ddo_contagemitem_pfl_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_pfl_Internalname, "FilteredTextTo_set", Ddo_contagemitem_pfl_Filteredtextto_set);
         AV125TFContagemItem_TipoUnidade_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_contagemitem_tipounidade_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_tipounidade_Internalname, "SelectedValue_set", Ddo_contagemitem_tipounidade_Selectedvalue_set);
         AV128TFContagemItem_Evidencias = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV128TFContagemItem_Evidencias", AV128TFContagemItem_Evidencias);
         Ddo_contagemitem_evidencias_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_evidencias_Internalname, "FilteredText_set", Ddo_contagemitem_evidencias_Filteredtext_set);
         AV129TFContagemItem_Evidencias_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV129TFContagemItem_Evidencias_Sel", AV129TFContagemItem_Evidencias_Sel);
         Ddo_contagemitem_evidencias_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_evidencias_Internalname, "SelectedValue_set", Ddo_contagemitem_evidencias_Selectedvalue_set);
      }

      protected void S132( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get(AV145Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV145Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV33Session.Get(AV145Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S182( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV147GXV2 = 1;
         while ( AV147GXV2 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV147GXV2));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_CODIGO") == 0 )
            {
               AV108TFContagem_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFContagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV108TFContagem_Codigo), 6, 0)));
               AV109TFContagem_Codigo_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFContagem_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV109TFContagem_Codigo_To), 6, 0)));
               if ( ! (0==AV108TFContagem_Codigo) )
               {
                  Ddo_contagem_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV108TFContagem_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagem_codigo_Internalname, "FilteredText_set", Ddo_contagem_codigo_Filteredtext_set);
               }
               if ( ! (0==AV109TFContagem_Codigo_To) )
               {
                  Ddo_contagem_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV109TFContagem_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagem_codigo_Internalname, "FilteredTextTo_set", Ddo_contagem_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEM_PFB") == 0 )
            {
               AV116TFContagemItem_PFB = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116TFContagemItem_PFB", StringUtil.LTrim( StringUtil.Str( AV116TFContagemItem_PFB, 14, 5)));
               AV117TFContagemItem_PFB_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117TFContagemItem_PFB_To", StringUtil.LTrim( StringUtil.Str( AV117TFContagemItem_PFB_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV116TFContagemItem_PFB) )
               {
                  Ddo_contagemitem_pfb_Filteredtext_set = StringUtil.Str( AV116TFContagemItem_PFB, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_pfb_Internalname, "FilteredText_set", Ddo_contagemitem_pfb_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV117TFContagemItem_PFB_To) )
               {
                  Ddo_contagemitem_pfb_Filteredtextto_set = StringUtil.Str( AV117TFContagemItem_PFB_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_pfb_Internalname, "FilteredTextTo_set", Ddo_contagemitem_pfb_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEM_PFL") == 0 )
            {
               AV120TFContagemItem_PFL = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120TFContagemItem_PFL", StringUtil.LTrim( StringUtil.Str( AV120TFContagemItem_PFL, 14, 5)));
               AV121TFContagemItem_PFL_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFContagemItem_PFL_To", StringUtil.LTrim( StringUtil.Str( AV121TFContagemItem_PFL_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV120TFContagemItem_PFL) )
               {
                  Ddo_contagemitem_pfl_Filteredtext_set = StringUtil.Str( AV120TFContagemItem_PFL, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_pfl_Internalname, "FilteredText_set", Ddo_contagemitem_pfl_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV121TFContagemItem_PFL_To) )
               {
                  Ddo_contagemitem_pfl_Filteredtextto_set = StringUtil.Str( AV121TFContagemItem_PFL_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_pfl_Internalname, "FilteredTextTo_set", Ddo_contagemitem_pfl_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEM_TIPOUNIDADE_SEL") == 0 )
            {
               AV124TFContagemItem_TipoUnidade_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV125TFContagemItem_TipoUnidade_Sels.FromJSonString(AV124TFContagemItem_TipoUnidade_SelsJson);
               if ( ! ( AV125TFContagemItem_TipoUnidade_Sels.Count == 0 ) )
               {
                  Ddo_contagemitem_tipounidade_Selectedvalue_set = AV124TFContagemItem_TipoUnidade_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_tipounidade_Internalname, "SelectedValue_set", Ddo_contagemitem_tipounidade_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEM_EVIDENCIAS") == 0 )
            {
               AV128TFContagemItem_Evidencias = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV128TFContagemItem_Evidencias", AV128TFContagemItem_Evidencias);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV128TFContagemItem_Evidencias)) )
               {
                  Ddo_contagemitem_evidencias_Filteredtext_set = AV128TFContagemItem_Evidencias;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_evidencias_Internalname, "FilteredText_set", Ddo_contagemitem_evidencias_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEM_EVIDENCIAS_SEL") == 0 )
            {
               AV129TFContagemItem_Evidencias_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV129TFContagemItem_Evidencias_Sel", AV129TFContagemItem_Evidencias_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129TFContagemItem_Evidencias_Sel)) )
               {
                  Ddo_contagemitem_evidencias_Selectedvalue_set = AV129TFContagemItem_Evidencias_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemitem_evidencias_Internalname, "SelectedValue_set", Ddo_contagemitem_evidencias_Selectedvalue_set);
               }
            }
            AV147GXV2 = (int)(AV147GXV2+1);
         }
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV33Session.Get(AV145Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV108TFContagem_Codigo) && (0==AV109TFContagem_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEM_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV108TFContagem_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV109TFContagem_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV116TFContagemItem_PFB) && (Convert.ToDecimal(0)==AV117TFContagemItem_PFB_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMITEM_PFB";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV116TFContagemItem_PFB, 14, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV117TFContagemItem_PFB_To, 14, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV120TFContagemItem_PFL) && (Convert.ToDecimal(0)==AV121TFContagemItem_PFL_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMITEM_PFL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV120TFContagemItem_PFL, 14, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV121TFContagemItem_PFL_To, 14, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV125TFContagemItem_TipoUnidade_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMITEM_TIPOUNIDADE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV125TFContagemItem_TipoUnidade_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV128TFContagemItem_Evidencias)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMITEM_EVIDENCIAS";
            AV11GridStateFilterValue.gxTpr_Value = AV128TFContagemItem_Evidencias;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129TFContagemItem_Evidencias_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTAGEMITEM_EVIDENCIAS_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV129TFContagemItem_Evidencias_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV62StatusItem) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "PARM_&STATUSITEM";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV62StatusItem), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV145Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S122( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV145Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContagemItem";
         AV33Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void E20532( )
      {
         /* Onmessage_gx1 Routine */
         if ( StringUtil.StrCmp(AV85NotificationInfo.gxTpr_Id, "ALTERAR_AREA_TRABALHO") == 0 )
         {
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
            AV84Contagem_AreaTrabalhoCod = AV6WWPContext.gxTpr_Areatrabalho_codigo;
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV108TFContagem_Codigo, AV109TFContagem_Codigo_To, AV116TFContagemItem_PFB, AV117TFContagemItem_PFB_To, AV120TFContagemItem_PFL, AV121TFContagemItem_PFL_To, AV128TFContagemItem_Evidencias, AV129TFContagemItem_Evidencias_Sel, AV86ManageFiltersExecutionStep, AV110ddo_Contagem_CodigoTitleControlIdToReplace, AV118ddo_ContagemItem_PFBTitleControlIdToReplace, AV122ddo_ContagemItem_PFLTitleControlIdToReplace, AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace, AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace, AV62StatusItem, AV125TFContagemItem_TipoUnidade_Sels, AV145Pgmname, AV6WWPContext, A224ContagemItem_Lancamento, A192Contagem_Codigo) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
      }

      protected void wb_table1_2_532( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_532( true) ;
         }
         else
         {
            wb_table2_8_532( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_532e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_33_532( true) ;
         }
         else
         {
            wb_table3_33_532( false) ;
         }
         return  ;
      }

      protected void wb_table3_33_532e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_532e( true) ;
         }
         else
         {
            wb_table1_2_532e( false) ;
         }
      }

      protected void wb_table3_33_532( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_36_532( true) ;
         }
         else
         {
            wb_table4_36_532( false) ;
         }
         return  ;
      }

      protected void wb_table4_36_532e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_33_532e( true) ;
         }
         else
         {
            wb_table3_33_532e( false) ;
         }
      }

      protected void wb_table4_36_532( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"39\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagem_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagem_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagem_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_PFB_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_PFB_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_PFB_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_PFL_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_PFL_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_PFL_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagemItem_TipoUnidade_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagemItem_TipoUnidade.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagemItem_TipoUnidade.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemItem_Evidencias_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemItem_Evidencias_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemItem_Evidencias_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV55Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagem_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagem_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A950ContagemItem_PFB, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_PFB_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_PFB_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A951ContagemItem_PFL, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_PFL_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_PFL_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A952ContagemItem_TipoUnidade);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagemItem_TipoUnidade.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagemItem_TipoUnidade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A953ContagemItem_Evidencias);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemItem_Evidencias_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemItem_Evidencias_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 39 )
         {
            wbEnd = 0;
            nRC_GXsfl_39 = (short)(nGXsfl_39_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_36_532e( true) ;
         }
         else
         {
            wb_table4_36_532e( false) ;
         }
      }

      protected void wb_table2_8_532( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_532( true) ;
         }
         else
         {
            wb_table5_11_532( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_532e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_25_532( true) ;
         }
         else
         {
            wb_table6_25_532( false) ;
         }
         return  ;
      }

      protected void wb_table6_25_532e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_532e( true) ;
         }
         else
         {
            wb_table2_8_532e( false) ;
         }
      }

      protected void wb_table6_25_532( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_532( true) ;
         }
         else
         {
            wb_table7_28_532( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_532e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_25_532e( true) ;
         }
         else
         {
            wb_table6_25_532e( false) ;
         }
      }

      protected void wb_table7_28_532( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_532e( true) ;
         }
         else
         {
            wb_table7_28_532e( false) ;
         }
      }

      protected void wb_table5_11_532( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemitemtitle_Internalname, lblContagemitemtitle_Caption, "", "", lblContagemitemtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'" + sGXsfl_39_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_WWContagemItem.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_39_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContagemItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_532e( true) ;
         }
         else
         {
            wb_table5_11_532e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV62StatusItem = Convert.ToInt16(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62StatusItem", StringUtil.Str( (decimal)(AV62StatusItem), 1, 0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSTATUSITEM", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV62StatusItem), "9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA532( ) ;
         WS532( ) ;
         WE532( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117343273");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontagemitem.js", "?20203117343273");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_392( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_39_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_39_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_39_idx;
         edtContagem_Codigo_Internalname = "CONTAGEM_CODIGO_"+sGXsfl_39_idx;
         edtContagemItem_PFB_Internalname = "CONTAGEMITEM_PFB_"+sGXsfl_39_idx;
         edtContagemItem_PFL_Internalname = "CONTAGEMITEM_PFL_"+sGXsfl_39_idx;
         cmbContagemItem_TipoUnidade_Internalname = "CONTAGEMITEM_TIPOUNIDADE_"+sGXsfl_39_idx;
         edtContagemItem_Evidencias_Internalname = "CONTAGEMITEM_EVIDENCIAS_"+sGXsfl_39_idx;
      }

      protected void SubsflControlProps_fel_392( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_39_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_39_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_39_fel_idx;
         edtContagem_Codigo_Internalname = "CONTAGEM_CODIGO_"+sGXsfl_39_fel_idx;
         edtContagemItem_PFB_Internalname = "CONTAGEMITEM_PFB_"+sGXsfl_39_fel_idx;
         edtContagemItem_PFL_Internalname = "CONTAGEMITEM_PFL_"+sGXsfl_39_fel_idx;
         cmbContagemItem_TipoUnidade_Internalname = "CONTAGEMITEM_TIPOUNIDADE_"+sGXsfl_39_fel_idx;
         edtContagemItem_Evidencias_Internalname = "CONTAGEMITEM_EVIDENCIAS_"+sGXsfl_39_fel_idx;
      }

      protected void sendrow_392( )
      {
         SubsflControlProps_392( ) ;
         WB530( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_39_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_39_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_39_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV31Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV142Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV142Update_GXI : context.PathToRelativeUrl( AV31Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV31Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV143Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV143Delete_GXI : context.PathToRelativeUrl( AV32Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV55Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV55Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV144Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV55Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV55Display)) ? AV144Display_GXI : context.PathToRelativeUrl( AV55Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV55Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagem_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagem_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)39,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_PFB_Internalname,StringUtil.LTrim( StringUtil.NToC( A950ContagemItem_PFB, 14, 5, ",", "")),context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_PFB_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)39,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_PFL_Internalname,StringUtil.LTrim( StringUtil.NToC( A951ContagemItem_PFL, 14, 5, ",", "")),context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_PFL_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)39,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_39_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEMITEM_TIPOUNIDADE_" + sGXsfl_39_idx;
               cmbContagemItem_TipoUnidade.Name = GXCCtl;
               cmbContagemItem_TipoUnidade.WebTags = "";
               if ( cmbContagemItem_TipoUnidade.ItemCount > 0 )
               {
                  A952ContagemItem_TipoUnidade = cmbContagemItem_TipoUnidade.getValidValue(A952ContagemItem_TipoUnidade);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemItem_TipoUnidade,(String)cmbContagemItem_TipoUnidade_Internalname,StringUtil.RTrim( A952ContagemItem_TipoUnidade),(short)1,(String)cmbContagemItem_TipoUnidade_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"svchar",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagemItem_TipoUnidade.CurrentValue = StringUtil.RTrim( A952ContagemItem_TipoUnidade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemItem_TipoUnidade_Internalname, "Values", (String)(cmbContagemItem_TipoUnidade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemItem_Evidencias_Internalname,(String)A953ContagemItem_Evidencias,(String)A953ContagemItem_Evidencias,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemItem_Evidencias_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5000,(short)0,(short)0,(short)39,(short)1,(short)0,(short)-1,(bool)true,(String)"Evidencias",(String)"left",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEM_CODIGO"+"_"+sGXsfl_39_idx, GetSecureSignedToken( sGXsfl_39_idx, context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_PFB"+"_"+sGXsfl_39_idx, GetSecureSignedToken( sGXsfl_39_idx, context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_PFL"+"_"+sGXsfl_39_idx, GetSecureSignedToken( sGXsfl_39_idx, context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_TIPOUNIDADE"+"_"+sGXsfl_39_idx, GetSecureSignedToken( sGXsfl_39_idx, StringUtil.RTrim( context.localUtil.Format( A952ContagemItem_TipoUnidade, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMITEM_EVIDENCIAS"+"_"+sGXsfl_39_idx, GetSecureSignedToken( sGXsfl_39_idx, A953ContagemItem_Evidencias));
            GridContainer.AddRow(GridRow);
            nGXsfl_39_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_39_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_39_idx+1));
            sGXsfl_39_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_39_idx), 4, 0)), 4, "0");
            SubsflControlProps_392( ) ;
         }
         /* End function sendrow_392 */
      }

      protected void init_default_properties( )
      {
         lblContagemitemtitle_Internalname = "CONTAGEMITEMTITLE";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtContagem_Codigo_Internalname = "CONTAGEM_CODIGO";
         edtContagemItem_PFB_Internalname = "CONTAGEMITEM_PFB";
         edtContagemItem_PFL_Internalname = "CONTAGEMITEM_PFL";
         cmbContagemItem_TipoUnidade_Internalname = "CONTAGEMITEM_TIPOUNIDADE";
         edtContagemItem_Evidencias_Internalname = "CONTAGEMITEM_EVIDENCIAS";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         edtavTfcontagem_codigo_Internalname = "vTFCONTAGEM_CODIGO";
         edtavTfcontagem_codigo_to_Internalname = "vTFCONTAGEM_CODIGO_TO";
         edtavTfcontagemitem_pfb_Internalname = "vTFCONTAGEMITEM_PFB";
         edtavTfcontagemitem_pfb_to_Internalname = "vTFCONTAGEMITEM_PFB_TO";
         edtavTfcontagemitem_pfl_Internalname = "vTFCONTAGEMITEM_PFL";
         edtavTfcontagemitem_pfl_to_Internalname = "vTFCONTAGEMITEM_PFL_TO";
         edtavTfcontagemitem_evidencias_Internalname = "vTFCONTAGEMITEM_EVIDENCIAS";
         edtavTfcontagemitem_evidencias_sel_Internalname = "vTFCONTAGEMITEM_EVIDENCIAS_SEL";
         Ddo_contagem_codigo_Internalname = "DDO_CONTAGEM_CODIGO";
         edtavDdo_contagem_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contagemitem_pfb_Internalname = "DDO_CONTAGEMITEM_PFB";
         edtavDdo_contagemitem_pfbtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMITEM_PFBTITLECONTROLIDTOREPLACE";
         Ddo_contagemitem_pfl_Internalname = "DDO_CONTAGEMITEM_PFL";
         edtavDdo_contagemitem_pfltitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMITEM_PFLTITLECONTROLIDTOREPLACE";
         Ddo_contagemitem_tipounidade_Internalname = "DDO_CONTAGEMITEM_TIPOUNIDADE";
         edtavDdo_contagemitem_tipounidadetitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMITEM_TIPOUNIDADETITLECONTROLIDTOREPLACE";
         Ddo_contagemitem_evidencias_Internalname = "DDO_CONTAGEMITEM_EVIDENCIAS";
         edtavDdo_contagemitem_evidenciastitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMITEM_EVIDENCIASTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContagemItem_Evidencias_Jsonclick = "";
         cmbContagemItem_TipoUnidade_Jsonclick = "";
         edtContagemItem_PFL_Jsonclick = "";
         edtContagemItem_PFB_Jsonclick = "";
         edtContagem_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         edtContagemItem_Evidencias_Titleformat = 0;
         cmbContagemItem_TipoUnidade_Titleformat = 0;
         edtContagemItem_PFL_Titleformat = 0;
         edtContagemItem_PFB_Titleformat = 0;
         edtContagem_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         lblContagemitemtitle_Caption = "Caixa de Entrada - Contagens";
         edtContagemItem_Evidencias_Title = "Evidencias";
         cmbContagemItem_TipoUnidade.Title.Text = "Tipo";
         edtContagemItem_PFL_Title = "PFL";
         edtContagemItem_PFB_Title = "PFB";
         edtContagem_Codigo_Title = "Contagem";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_contagemitem_evidenciastitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemitem_tipounidadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemitem_pfltitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemitem_pfbtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagem_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfcontagemitem_evidencias_sel_Visible = 1;
         edtavTfcontagemitem_evidencias_Visible = 1;
         edtavTfcontagemitem_pfl_to_Jsonclick = "";
         edtavTfcontagemitem_pfl_to_Visible = 1;
         edtavTfcontagemitem_pfl_Jsonclick = "";
         edtavTfcontagemitem_pfl_Visible = 1;
         edtavTfcontagemitem_pfb_to_Jsonclick = "";
         edtavTfcontagemitem_pfb_to_Visible = 1;
         edtavTfcontagemitem_pfb_Jsonclick = "";
         edtavTfcontagemitem_pfb_Visible = 1;
         edtavTfcontagem_codigo_to_Jsonclick = "";
         edtavTfcontagem_codigo_to_Visible = 1;
         edtavTfcontagem_codigo_Jsonclick = "";
         edtavTfcontagem_codigo_Visible = 1;
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         Ddo_contagemitem_evidencias_Searchbuttontext = "Pesquisar";
         Ddo_contagemitem_evidencias_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemitem_evidencias_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemitem_evidencias_Loadingdata = "Carregando dados...";
         Ddo_contagemitem_evidencias_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemitem_evidencias_Sortasc = "Ordenar de A � Z";
         Ddo_contagemitem_evidencias_Datalistupdateminimumcharacters = 0;
         Ddo_contagemitem_evidencias_Datalistproc = "GetWWContagemItemFilterData";
         Ddo_contagemitem_evidencias_Datalisttype = "Dynamic";
         Ddo_contagemitem_evidencias_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemitem_evidencias_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemitem_evidencias_Filtertype = "Character";
         Ddo_contagemitem_evidencias_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemitem_evidencias_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemitem_evidencias_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemitem_evidencias_Titlecontrolidtoreplace = "";
         Ddo_contagemitem_evidencias_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemitem_evidencias_Cls = "ColumnSettings";
         Ddo_contagemitem_evidencias_Tooltip = "Op��es";
         Ddo_contagemitem_evidencias_Caption = "";
         Ddo_contagemitem_tipounidade_Searchbuttontext = "Filtrar Selecionados";
         Ddo_contagemitem_tipounidade_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemitem_tipounidade_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemitem_tipounidade_Sortasc = "Ordenar de A � Z";
         Ddo_contagemitem_tipounidade_Datalistfixedvalues = "";
         Ddo_contagemitem_tipounidade_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_contagemitem_tipounidade_Datalisttype = "FixedValues";
         Ddo_contagemitem_tipounidade_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemitem_tipounidade_Includefilter = Convert.ToBoolean( 0);
         Ddo_contagemitem_tipounidade_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemitem_tipounidade_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemitem_tipounidade_Titlecontrolidtoreplace = "";
         Ddo_contagemitem_tipounidade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemitem_tipounidade_Cls = "ColumnSettings";
         Ddo_contagemitem_tipounidade_Tooltip = "Op��es";
         Ddo_contagemitem_tipounidade_Caption = "";
         Ddo_contagemitem_pfl_Searchbuttontext = "Pesquisar";
         Ddo_contagemitem_pfl_Rangefilterto = "At�";
         Ddo_contagemitem_pfl_Rangefilterfrom = "Desde";
         Ddo_contagemitem_pfl_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemitem_pfl_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemitem_pfl_Sortasc = "Ordenar de A � Z";
         Ddo_contagemitem_pfl_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemitem_pfl_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemitem_pfl_Filtertype = "Numeric";
         Ddo_contagemitem_pfl_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemitem_pfl_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemitem_pfl_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemitem_pfl_Titlecontrolidtoreplace = "";
         Ddo_contagemitem_pfl_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemitem_pfl_Cls = "ColumnSettings";
         Ddo_contagemitem_pfl_Tooltip = "Op��es";
         Ddo_contagemitem_pfl_Caption = "";
         Ddo_contagemitem_pfb_Searchbuttontext = "Pesquisar";
         Ddo_contagemitem_pfb_Rangefilterto = "At�";
         Ddo_contagemitem_pfb_Rangefilterfrom = "Desde";
         Ddo_contagemitem_pfb_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemitem_pfb_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemitem_pfb_Sortasc = "Ordenar de A � Z";
         Ddo_contagemitem_pfb_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemitem_pfb_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemitem_pfb_Filtertype = "Numeric";
         Ddo_contagemitem_pfb_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemitem_pfb_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemitem_pfb_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemitem_pfb_Titlecontrolidtoreplace = "";
         Ddo_contagemitem_pfb_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemitem_pfb_Cls = "ColumnSettings";
         Ddo_contagemitem_pfb_Tooltip = "Op��es";
         Ddo_contagemitem_pfb_Caption = "";
         Ddo_contagem_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contagem_codigo_Rangefilterto = "At�";
         Ddo_contagem_codigo_Rangefilterfrom = "Desde";
         Ddo_contagem_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contagem_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contagem_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contagem_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagem_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagem_codigo_Filtertype = "Numeric";
         Ddo_contagem_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagem_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagem_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagem_codigo_Titlecontrolidtoreplace = "";
         Ddo_contagem_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagem_codigo_Cls = "ColumnSettings";
         Ddo_contagem_codigo_Tooltip = "Op��es";
         Ddo_contagem_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Contagem Item";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV86ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV110ddo_Contagem_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_ContagemItem_PFBTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_ContagemItem_PFLTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFLTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_TIPOUNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_EVIDENCIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62StatusItem',fld:'vSTATUSITEM',pic:'9',hsh:true,nv:0},{av:'AV108TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV109TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV116TFContagemItem_PFB',fld:'vTFCONTAGEMITEM_PFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV117TFContagemItem_PFB_To',fld:'vTFCONTAGEMITEM_PFB_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV120TFContagemItem_PFL',fld:'vTFCONTAGEMITEM_PFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFContagemItem_PFL_To',fld:'vTFCONTAGEMITEM_PFL_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV125TFContagemItem_TipoUnidade_Sels',fld:'vTFCONTAGEMITEM_TIPOUNIDADE_SELS',pic:'',nv:null},{av:'AV128TFContagemItem_Evidencias',fld:'vTFCONTAGEMITEM_EVIDENCIAS',pic:'',nv:''},{av:'AV129TFContagemItem_Evidencias_Sel',fld:'vTFCONTAGEMITEM_EVIDENCIAS_SEL',pic:'',nv:''},{av:'AV145Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false}],oparms:[{av:'AV107Contagem_CodigoTitleFilterData',fld:'vCONTAGEM_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV115ContagemItem_PFBTitleFilterData',fld:'vCONTAGEMITEM_PFBTITLEFILTERDATA',pic:'',nv:null},{av:'AV119ContagemItem_PFLTitleFilterData',fld:'vCONTAGEMITEM_PFLTITLEFILTERDATA',pic:'',nv:null},{av:'AV123ContagemItem_TipoUnidadeTitleFilterData',fld:'vCONTAGEMITEM_TIPOUNIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV127ContagemItem_EvidenciasTitleFilterData',fld:'vCONTAGEMITEM_EVIDENCIASTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV86ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'edtContagem_Codigo_Titleformat',ctrl:'CONTAGEM_CODIGO',prop:'Titleformat'},{av:'edtContagem_Codigo_Title',ctrl:'CONTAGEM_CODIGO',prop:'Title'},{av:'edtContagemItem_PFB_Titleformat',ctrl:'CONTAGEMITEM_PFB',prop:'Titleformat'},{av:'edtContagemItem_PFB_Title',ctrl:'CONTAGEMITEM_PFB',prop:'Title'},{av:'edtContagemItem_PFL_Titleformat',ctrl:'CONTAGEMITEM_PFL',prop:'Titleformat'},{av:'edtContagemItem_PFL_Title',ctrl:'CONTAGEMITEM_PFL',prop:'Title'},{av:'cmbContagemItem_TipoUnidade'},{av:'edtContagemItem_Evidencias_Titleformat',ctrl:'CONTAGEMITEM_EVIDENCIAS',prop:'Titleformat'},{av:'edtContagemItem_Evidencias_Title',ctrl:'CONTAGEMITEM_EVIDENCIAS',prop:'Title'},{av:'AV113GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV114GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'lblContagemitemtitle_Caption',ctrl:'CONTAGEMITEMTITLE',prop:'Caption'},{av:'AV90ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E12532',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV108TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV109TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV116TFContagemItem_PFB',fld:'vTFCONTAGEMITEM_PFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV117TFContagemItem_PFB_To',fld:'vTFCONTAGEMITEM_PFB_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV120TFContagemItem_PFL',fld:'vTFCONTAGEMITEM_PFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFContagemItem_PFL_To',fld:'vTFCONTAGEMITEM_PFL_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV128TFContagemItem_Evidencias',fld:'vTFCONTAGEMITEM_EVIDENCIAS',pic:'',nv:''},{av:'AV129TFContagemItem_Evidencias_Sel',fld:'vTFCONTAGEMITEM_EVIDENCIAS_SEL',pic:'',nv:''},{av:'AV86ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV110ddo_Contagem_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_ContagemItem_PFBTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_ContagemItem_PFLTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFLTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_TIPOUNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_EVIDENCIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62StatusItem',fld:'vSTATUSITEM',pic:'9',hsh:true,nv:0},{av:'AV125TFContagemItem_TipoUnidade_Sels',fld:'vTFCONTAGEMITEM_TIPOUNIDADE_SELS',pic:'',nv:null},{av:'AV145Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTAGEM_CODIGO.ONOPTIONCLICKED","{handler:'E13532',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV108TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV109TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV116TFContagemItem_PFB',fld:'vTFCONTAGEMITEM_PFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV117TFContagemItem_PFB_To',fld:'vTFCONTAGEMITEM_PFB_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV120TFContagemItem_PFL',fld:'vTFCONTAGEMITEM_PFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFContagemItem_PFL_To',fld:'vTFCONTAGEMITEM_PFL_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV128TFContagemItem_Evidencias',fld:'vTFCONTAGEMITEM_EVIDENCIAS',pic:'',nv:''},{av:'AV129TFContagemItem_Evidencias_Sel',fld:'vTFCONTAGEMITEM_EVIDENCIAS_SEL',pic:'',nv:''},{av:'AV86ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV110ddo_Contagem_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_ContagemItem_PFBTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_ContagemItem_PFLTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFLTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_TIPOUNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_EVIDENCIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62StatusItem',fld:'vSTATUSITEM',pic:'9',hsh:true,nv:0},{av:'AV125TFContagemItem_TipoUnidade_Sels',fld:'vTFCONTAGEMITEM_TIPOUNIDADE_SELS',pic:'',nv:null},{av:'AV145Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagem_codigo_Activeeventkey',ctrl:'DDO_CONTAGEM_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contagem_codigo_Filteredtext_get',ctrl:'DDO_CONTAGEM_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contagem_codigo_Filteredtextto_get',ctrl:'DDO_CONTAGEM_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagem_codigo_Sortedstatus',ctrl:'DDO_CONTAGEM_CODIGO',prop:'SortedStatus'},{av:'AV108TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV109TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemitem_pfb_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_PFB',prop:'SortedStatus'},{av:'Ddo_contagemitem_pfl_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_PFL',prop:'SortedStatus'},{av:'Ddo_contagemitem_tipounidade_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_TIPOUNIDADE',prop:'SortedStatus'},{av:'Ddo_contagemitem_evidencias_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_EVIDENCIAS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMITEM_PFB.ONOPTIONCLICKED","{handler:'E14532',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV108TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV109TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV116TFContagemItem_PFB',fld:'vTFCONTAGEMITEM_PFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV117TFContagemItem_PFB_To',fld:'vTFCONTAGEMITEM_PFB_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV120TFContagemItem_PFL',fld:'vTFCONTAGEMITEM_PFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFContagemItem_PFL_To',fld:'vTFCONTAGEMITEM_PFL_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV128TFContagemItem_Evidencias',fld:'vTFCONTAGEMITEM_EVIDENCIAS',pic:'',nv:''},{av:'AV129TFContagemItem_Evidencias_Sel',fld:'vTFCONTAGEMITEM_EVIDENCIAS_SEL',pic:'',nv:''},{av:'AV86ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV110ddo_Contagem_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_ContagemItem_PFBTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_ContagemItem_PFLTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFLTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_TIPOUNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_EVIDENCIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62StatusItem',fld:'vSTATUSITEM',pic:'9',hsh:true,nv:0},{av:'AV125TFContagemItem_TipoUnidade_Sels',fld:'vTFCONTAGEMITEM_TIPOUNIDADE_SELS',pic:'',nv:null},{av:'AV145Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemitem_pfb_Activeeventkey',ctrl:'DDO_CONTAGEMITEM_PFB',prop:'ActiveEventKey'},{av:'Ddo_contagemitem_pfb_Filteredtext_get',ctrl:'DDO_CONTAGEMITEM_PFB',prop:'FilteredText_get'},{av:'Ddo_contagemitem_pfb_Filteredtextto_get',ctrl:'DDO_CONTAGEMITEM_PFB',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemitem_pfb_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_PFB',prop:'SortedStatus'},{av:'AV116TFContagemItem_PFB',fld:'vTFCONTAGEMITEM_PFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV117TFContagemItem_PFB_To',fld:'vTFCONTAGEMITEM_PFB_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagem_codigo_Sortedstatus',ctrl:'DDO_CONTAGEM_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemitem_pfl_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_PFL',prop:'SortedStatus'},{av:'Ddo_contagemitem_tipounidade_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_TIPOUNIDADE',prop:'SortedStatus'},{av:'Ddo_contagemitem_evidencias_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_EVIDENCIAS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMITEM_PFL.ONOPTIONCLICKED","{handler:'E15532',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV108TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV109TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV116TFContagemItem_PFB',fld:'vTFCONTAGEMITEM_PFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV117TFContagemItem_PFB_To',fld:'vTFCONTAGEMITEM_PFB_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV120TFContagemItem_PFL',fld:'vTFCONTAGEMITEM_PFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFContagemItem_PFL_To',fld:'vTFCONTAGEMITEM_PFL_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV128TFContagemItem_Evidencias',fld:'vTFCONTAGEMITEM_EVIDENCIAS',pic:'',nv:''},{av:'AV129TFContagemItem_Evidencias_Sel',fld:'vTFCONTAGEMITEM_EVIDENCIAS_SEL',pic:'',nv:''},{av:'AV86ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV110ddo_Contagem_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_ContagemItem_PFBTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_ContagemItem_PFLTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFLTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_TIPOUNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_EVIDENCIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62StatusItem',fld:'vSTATUSITEM',pic:'9',hsh:true,nv:0},{av:'AV125TFContagemItem_TipoUnidade_Sels',fld:'vTFCONTAGEMITEM_TIPOUNIDADE_SELS',pic:'',nv:null},{av:'AV145Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemitem_pfl_Activeeventkey',ctrl:'DDO_CONTAGEMITEM_PFL',prop:'ActiveEventKey'},{av:'Ddo_contagemitem_pfl_Filteredtext_get',ctrl:'DDO_CONTAGEMITEM_PFL',prop:'FilteredText_get'},{av:'Ddo_contagemitem_pfl_Filteredtextto_get',ctrl:'DDO_CONTAGEMITEM_PFL',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemitem_pfl_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_PFL',prop:'SortedStatus'},{av:'AV120TFContagemItem_PFL',fld:'vTFCONTAGEMITEM_PFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFContagemItem_PFL_To',fld:'vTFCONTAGEMITEM_PFL_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagem_codigo_Sortedstatus',ctrl:'DDO_CONTAGEM_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemitem_pfb_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_PFB',prop:'SortedStatus'},{av:'Ddo_contagemitem_tipounidade_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_TIPOUNIDADE',prop:'SortedStatus'},{av:'Ddo_contagemitem_evidencias_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_EVIDENCIAS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMITEM_TIPOUNIDADE.ONOPTIONCLICKED","{handler:'E16532',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV108TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV109TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV116TFContagemItem_PFB',fld:'vTFCONTAGEMITEM_PFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV117TFContagemItem_PFB_To',fld:'vTFCONTAGEMITEM_PFB_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV120TFContagemItem_PFL',fld:'vTFCONTAGEMITEM_PFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFContagemItem_PFL_To',fld:'vTFCONTAGEMITEM_PFL_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV128TFContagemItem_Evidencias',fld:'vTFCONTAGEMITEM_EVIDENCIAS',pic:'',nv:''},{av:'AV129TFContagemItem_Evidencias_Sel',fld:'vTFCONTAGEMITEM_EVIDENCIAS_SEL',pic:'',nv:''},{av:'AV86ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV110ddo_Contagem_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_ContagemItem_PFBTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_ContagemItem_PFLTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFLTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_TIPOUNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_EVIDENCIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62StatusItem',fld:'vSTATUSITEM',pic:'9',hsh:true,nv:0},{av:'AV125TFContagemItem_TipoUnidade_Sels',fld:'vTFCONTAGEMITEM_TIPOUNIDADE_SELS',pic:'',nv:null},{av:'AV145Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemitem_tipounidade_Activeeventkey',ctrl:'DDO_CONTAGEMITEM_TIPOUNIDADE',prop:'ActiveEventKey'},{av:'Ddo_contagemitem_tipounidade_Selectedvalue_get',ctrl:'DDO_CONTAGEMITEM_TIPOUNIDADE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemitem_tipounidade_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_TIPOUNIDADE',prop:'SortedStatus'},{av:'AV125TFContagemItem_TipoUnidade_Sels',fld:'vTFCONTAGEMITEM_TIPOUNIDADE_SELS',pic:'',nv:null},{av:'Ddo_contagem_codigo_Sortedstatus',ctrl:'DDO_CONTAGEM_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemitem_pfb_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_PFB',prop:'SortedStatus'},{av:'Ddo_contagemitem_pfl_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_PFL',prop:'SortedStatus'},{av:'Ddo_contagemitem_evidencias_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_EVIDENCIAS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMITEM_EVIDENCIAS.ONOPTIONCLICKED","{handler:'E17532',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV108TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV109TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV116TFContagemItem_PFB',fld:'vTFCONTAGEMITEM_PFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV117TFContagemItem_PFB_To',fld:'vTFCONTAGEMITEM_PFB_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV120TFContagemItem_PFL',fld:'vTFCONTAGEMITEM_PFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFContagemItem_PFL_To',fld:'vTFCONTAGEMITEM_PFL_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV128TFContagemItem_Evidencias',fld:'vTFCONTAGEMITEM_EVIDENCIAS',pic:'',nv:''},{av:'AV129TFContagemItem_Evidencias_Sel',fld:'vTFCONTAGEMITEM_EVIDENCIAS_SEL',pic:'',nv:''},{av:'AV86ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV110ddo_Contagem_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_ContagemItem_PFBTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_ContagemItem_PFLTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFLTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_TIPOUNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_EVIDENCIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62StatusItem',fld:'vSTATUSITEM',pic:'9',hsh:true,nv:0},{av:'AV125TFContagemItem_TipoUnidade_Sels',fld:'vTFCONTAGEMITEM_TIPOUNIDADE_SELS',pic:'',nv:null},{av:'AV145Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contagemitem_evidencias_Activeeventkey',ctrl:'DDO_CONTAGEMITEM_EVIDENCIAS',prop:'ActiveEventKey'},{av:'Ddo_contagemitem_evidencias_Filteredtext_get',ctrl:'DDO_CONTAGEMITEM_EVIDENCIAS',prop:'FilteredText_get'},{av:'Ddo_contagemitem_evidencias_Selectedvalue_get',ctrl:'DDO_CONTAGEMITEM_EVIDENCIAS',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemitem_evidencias_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_EVIDENCIAS',prop:'SortedStatus'},{av:'AV128TFContagemItem_Evidencias',fld:'vTFCONTAGEMITEM_EVIDENCIAS',pic:'',nv:''},{av:'AV129TFContagemItem_Evidencias_Sel',fld:'vTFCONTAGEMITEM_EVIDENCIAS_SEL',pic:'',nv:''},{av:'Ddo_contagem_codigo_Sortedstatus',ctrl:'DDO_CONTAGEM_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemitem_pfb_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_PFB',prop:'SortedStatus'},{av:'Ddo_contagemitem_pfl_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_PFL',prop:'SortedStatus'},{av:'Ddo_contagemitem_tipounidade_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_TIPOUNIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E23532',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV31Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV32Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV55Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E18532',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV108TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV109TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV116TFContagemItem_PFB',fld:'vTFCONTAGEMITEM_PFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV117TFContagemItem_PFB_To',fld:'vTFCONTAGEMITEM_PFB_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV120TFContagemItem_PFL',fld:'vTFCONTAGEMITEM_PFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFContagemItem_PFL_To',fld:'vTFCONTAGEMITEM_PFL_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV128TFContagemItem_Evidencias',fld:'vTFCONTAGEMITEM_EVIDENCIAS',pic:'',nv:''},{av:'AV129TFContagemItem_Evidencias_Sel',fld:'vTFCONTAGEMITEM_EVIDENCIAS_SEL',pic:'',nv:''},{av:'AV86ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV110ddo_Contagem_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_ContagemItem_PFBTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_ContagemItem_PFLTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFLTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_TIPOUNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_EVIDENCIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62StatusItem',fld:'vSTATUSITEM',pic:'9',hsh:true,nv:0},{av:'AV125TFContagemItem_TipoUnidade_Sels',fld:'vTFCONTAGEMITEM_TIPOUNIDADE_SELS',pic:'',nv:null},{av:'AV145Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E11532',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV108TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV109TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV116TFContagemItem_PFB',fld:'vTFCONTAGEMITEM_PFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV117TFContagemItem_PFB_To',fld:'vTFCONTAGEMITEM_PFB_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV120TFContagemItem_PFL',fld:'vTFCONTAGEMITEM_PFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFContagemItem_PFL_To',fld:'vTFCONTAGEMITEM_PFL_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV128TFContagemItem_Evidencias',fld:'vTFCONTAGEMITEM_EVIDENCIAS',pic:'',nv:''},{av:'AV129TFContagemItem_Evidencias_Sel',fld:'vTFCONTAGEMITEM_EVIDENCIAS_SEL',pic:'',nv:''},{av:'AV86ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV110ddo_Contagem_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_ContagemItem_PFBTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_ContagemItem_PFLTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFLTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_TIPOUNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_EVIDENCIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62StatusItem',fld:'vSTATUSITEM',pic:'9',hsh:true,nv:0},{av:'AV125TFContagemItem_TipoUnidade_Sels',fld:'vTFCONTAGEMITEM_TIPOUNIDADE_SELS',pic:'',nv:null},{av:'AV145Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}],oparms:[{av:'AV86ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV108TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagem_codigo_Filteredtext_set',ctrl:'DDO_CONTAGEM_CODIGO',prop:'FilteredText_set'},{av:'AV109TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagem_codigo_Filteredtextto_set',ctrl:'DDO_CONTAGEM_CODIGO',prop:'FilteredTextTo_set'},{av:'AV116TFContagemItem_PFB',fld:'vTFCONTAGEMITEM_PFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemitem_pfb_Filteredtext_set',ctrl:'DDO_CONTAGEMITEM_PFB',prop:'FilteredText_set'},{av:'AV117TFContagemItem_PFB_To',fld:'vTFCONTAGEMITEM_PFB_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemitem_pfb_Filteredtextto_set',ctrl:'DDO_CONTAGEMITEM_PFB',prop:'FilteredTextTo_set'},{av:'AV120TFContagemItem_PFL',fld:'vTFCONTAGEMITEM_PFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemitem_pfl_Filteredtext_set',ctrl:'DDO_CONTAGEMITEM_PFL',prop:'FilteredText_set'},{av:'AV121TFContagemItem_PFL_To',fld:'vTFCONTAGEMITEM_PFL_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemitem_pfl_Filteredtextto_set',ctrl:'DDO_CONTAGEMITEM_PFL',prop:'FilteredTextTo_set'},{av:'AV125TFContagemItem_TipoUnidade_Sels',fld:'vTFCONTAGEMITEM_TIPOUNIDADE_SELS',pic:'',nv:null},{av:'Ddo_contagemitem_tipounidade_Selectedvalue_set',ctrl:'DDO_CONTAGEMITEM_TIPOUNIDADE',prop:'SelectedValue_set'},{av:'AV128TFContagemItem_Evidencias',fld:'vTFCONTAGEMITEM_EVIDENCIAS',pic:'',nv:''},{av:'Ddo_contagemitem_evidencias_Filteredtext_set',ctrl:'DDO_CONTAGEMITEM_EVIDENCIAS',prop:'FilteredText_set'},{av:'AV129TFContagemItem_Evidencias_Sel',fld:'vTFCONTAGEMITEM_EVIDENCIAS_SEL',pic:'',nv:''},{av:'Ddo_contagemitem_evidencias_Selectedvalue_set',ctrl:'DDO_CONTAGEMITEM_EVIDENCIAS',prop:'SelectedValue_set'},{av:'Ddo_contagem_codigo_Sortedstatus',ctrl:'DDO_CONTAGEM_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemitem_pfb_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_PFB',prop:'SortedStatus'},{av:'Ddo_contagemitem_pfl_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_PFL',prop:'SortedStatus'},{av:'Ddo_contagemitem_tipounidade_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_TIPOUNIDADE',prop:'SortedStatus'},{av:'Ddo_contagemitem_evidencias_Sortedstatus',ctrl:'DDO_CONTAGEMITEM_EVIDENCIAS',prop:'SortedStatus'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E19532',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV108TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV109TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV116TFContagemItem_PFB',fld:'vTFCONTAGEMITEM_PFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV117TFContagemItem_PFB_To',fld:'vTFCONTAGEMITEM_PFB_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV120TFContagemItem_PFL',fld:'vTFCONTAGEMITEM_PFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFContagemItem_PFL_To',fld:'vTFCONTAGEMITEM_PFL_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV128TFContagemItem_Evidencias',fld:'vTFCONTAGEMITEM_EVIDENCIAS',pic:'',nv:''},{av:'AV129TFContagemItem_Evidencias_Sel',fld:'vTFCONTAGEMITEM_EVIDENCIAS_SEL',pic:'',nv:''},{av:'AV86ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV110ddo_Contagem_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_ContagemItem_PFBTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_ContagemItem_PFLTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFLTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_TIPOUNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_EVIDENCIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62StatusItem',fld:'vSTATUSITEM',pic:'9',hsh:true,nv:0},{av:'AV125TFContagemItem_TipoUnidade_Sels',fld:'vTFCONTAGEMITEM_TIPOUNIDADE_SELS',pic:'',nv:null},{av:'AV145Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV108TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagem_codigo_Filteredtext_set',ctrl:'DDO_CONTAGEM_CODIGO',prop:'FilteredText_set'},{av:'AV109TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagem_codigo_Filteredtextto_set',ctrl:'DDO_CONTAGEM_CODIGO',prop:'FilteredTextTo_set'},{av:'AV116TFContagemItem_PFB',fld:'vTFCONTAGEMITEM_PFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemitem_pfb_Filteredtext_set',ctrl:'DDO_CONTAGEMITEM_PFB',prop:'FilteredText_set'},{av:'AV117TFContagemItem_PFB_To',fld:'vTFCONTAGEMITEM_PFB_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemitem_pfb_Filteredtextto_set',ctrl:'DDO_CONTAGEMITEM_PFB',prop:'FilteredTextTo_set'},{av:'AV120TFContagemItem_PFL',fld:'vTFCONTAGEMITEM_PFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemitem_pfl_Filteredtext_set',ctrl:'DDO_CONTAGEMITEM_PFL',prop:'FilteredText_set'},{av:'AV121TFContagemItem_PFL_To',fld:'vTFCONTAGEMITEM_PFL_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contagemitem_pfl_Filteredtextto_set',ctrl:'DDO_CONTAGEMITEM_PFL',prop:'FilteredTextTo_set'},{av:'AV125TFContagemItem_TipoUnidade_Sels',fld:'vTFCONTAGEMITEM_TIPOUNIDADE_SELS',pic:'',nv:null},{av:'Ddo_contagemitem_tipounidade_Selectedvalue_set',ctrl:'DDO_CONTAGEMITEM_TIPOUNIDADE',prop:'SelectedValue_set'},{av:'AV128TFContagemItem_Evidencias',fld:'vTFCONTAGEMITEM_EVIDENCIAS',pic:'',nv:''},{av:'Ddo_contagemitem_evidencias_Filteredtext_set',ctrl:'DDO_CONTAGEMITEM_EVIDENCIAS',prop:'FilteredText_set'},{av:'AV129TFContagemItem_Evidencias_Sel',fld:'vTFCONTAGEMITEM_EVIDENCIAS_SEL',pic:'',nv:''},{av:'Ddo_contagemitem_evidencias_Selectedvalue_set',ctrl:'DDO_CONTAGEMITEM_EVIDENCIAS',prop:'SelectedValue_set'}]}");
         setEventMetadata("ONMESSAGE_GX1","{handler:'E20532',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV108TFContagem_Codigo',fld:'vTFCONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV109TFContagem_Codigo_To',fld:'vTFCONTAGEM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV116TFContagemItem_PFB',fld:'vTFCONTAGEMITEM_PFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV117TFContagemItem_PFB_To',fld:'vTFCONTAGEMITEM_PFB_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV120TFContagemItem_PFL',fld:'vTFCONTAGEMITEM_PFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV121TFContagemItem_PFL_To',fld:'vTFCONTAGEMITEM_PFL_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV128TFContagemItem_Evidencias',fld:'vTFCONTAGEMITEM_EVIDENCIAS',pic:'',nv:''},{av:'AV129TFContagemItem_Evidencias_Sel',fld:'vTFCONTAGEMITEM_EVIDENCIAS_SEL',pic:'',nv:''},{av:'AV86ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV110ddo_Contagem_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV118ddo_ContagemItem_PFBTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_ContagemItem_PFLTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_PFLTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_TIPOUNIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace',fld:'vDDO_CONTAGEMITEM_EVIDENCIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62StatusItem',fld:'vSTATUSITEM',pic:'9',hsh:true,nv:0},{av:'AV125TFContagemItem_TipoUnidade_Sels',fld:'vTFCONTAGEMITEM_TIPOUNIDADE_SELS',pic:'',nv:null},{av:'AV145Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV85NotificationInfo',fld:'vNOTIFICATIONINFO',pic:'',nv:null}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contagem_codigo_Activeeventkey = "";
         Ddo_contagem_codigo_Filteredtext_get = "";
         Ddo_contagem_codigo_Filteredtextto_get = "";
         Ddo_contagemitem_pfb_Activeeventkey = "";
         Ddo_contagemitem_pfb_Filteredtext_get = "";
         Ddo_contagemitem_pfb_Filteredtextto_get = "";
         Ddo_contagemitem_pfl_Activeeventkey = "";
         Ddo_contagemitem_pfl_Filteredtext_get = "";
         Ddo_contagemitem_pfl_Filteredtextto_get = "";
         Ddo_contagemitem_tipounidade_Activeeventkey = "";
         Ddo_contagemitem_tipounidade_Selectedvalue_get = "";
         Ddo_contagemitem_evidencias_Activeeventkey = "";
         Ddo_contagemitem_evidencias_Filteredtext_get = "";
         Ddo_contagemitem_evidencias_Selectedvalue_get = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV128TFContagemItem_Evidencias = "";
         AV129TFContagemItem_Evidencias_Sel = "";
         AV110ddo_Contagem_CodigoTitleControlIdToReplace = "";
         AV118ddo_ContagemItem_PFBTitleControlIdToReplace = "";
         AV122ddo_ContagemItem_PFLTitleControlIdToReplace = "";
         AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace = "";
         AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace = "";
         AV125TFContagemItem_TipoUnidade_Sels = new GxSimpleCollection();
         AV145Pgmname = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV90ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV111DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV107Contagem_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV115ContagemItem_PFBTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV119ContagemItem_PFLTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV123ContagemItem_TipoUnidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV127ContagemItem_EvidenciasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV85NotificationInfo = new SdtNotificationInfo(context);
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         Ddo_contagem_codigo_Filteredtext_set = "";
         Ddo_contagem_codigo_Filteredtextto_set = "";
         Ddo_contagem_codigo_Sortedstatus = "";
         Ddo_contagemitem_pfb_Filteredtext_set = "";
         Ddo_contagemitem_pfb_Filteredtextto_set = "";
         Ddo_contagemitem_pfb_Sortedstatus = "";
         Ddo_contagemitem_pfl_Filteredtext_set = "";
         Ddo_contagemitem_pfl_Filteredtextto_set = "";
         Ddo_contagemitem_pfl_Sortedstatus = "";
         Ddo_contagemitem_tipounidade_Selectedvalue_set = "";
         Ddo_contagemitem_tipounidade_Sortedstatus = "";
         Ddo_contagemitem_evidencias_Filteredtext_set = "";
         Ddo_contagemitem_evidencias_Selectedvalue_set = "";
         Ddo_contagemitem_evidencias_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Update = "";
         AV142Update_GXI = "";
         AV32Delete = "";
         AV143Delete_GXI = "";
         AV55Display = "";
         AV144Display_GXI = "";
         A952ContagemItem_TipoUnidade = "";
         A953ContagemItem_Evidencias = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV140WWContagemItemDS_8_Tfcontagemitem_evidencias = "";
         AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel = "";
         AV140WWContagemItemDS_8_Tfcontagemitem_evidencias = "";
         H00532_A207Contagem_FabricaSoftwareCod = new int[1] ;
         H00532_A224ContagemItem_Lancamento = new int[1] ;
         H00532_A953ContagemItem_Evidencias = new String[] {""} ;
         H00532_n953ContagemItem_Evidencias = new bool[] {false} ;
         H00532_A952ContagemItem_TipoUnidade = new String[] {""} ;
         H00532_A951ContagemItem_PFL = new decimal[1] ;
         H00532_n951ContagemItem_PFL = new bool[] {false} ;
         H00532_A950ContagemItem_PFB = new decimal[1] ;
         H00532_n950ContagemItem_PFB = new bool[] {false} ;
         H00532_A192Contagem_Codigo = new int[1] ;
         H00533_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV124TFContagemItem_TipoUnidade_SelsJson = "";
         GridRow = new GXWebRow();
         AV87ManageFiltersXml = "";
         GXt_char2 = "";
         AV91ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV88ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV89ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV33Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContagemitemtitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontagemitem__default(),
            new Object[][] {
                new Object[] {
               H00532_A207Contagem_FabricaSoftwareCod, H00532_A224ContagemItem_Lancamento, H00532_A953ContagemItem_Evidencias, H00532_n953ContagemItem_Evidencias, H00532_A952ContagemItem_TipoUnidade, H00532_A951ContagemItem_PFL, H00532_n951ContagemItem_PFL, H00532_A950ContagemItem_PFB, H00532_n950ContagemItem_PFB, H00532_A192Contagem_Codigo
               }
               , new Object[] {
               H00533_AGRID_nRecordCount
               }
            }
         );
         AV145Pgmname = "WWContagemItem";
         /* GeneXus formulas. */
         AV145Pgmname = "WWContagemItem";
         context.Gx_err = 0;
      }

      private short AV62StatusItem ;
      private short wcpOAV62StatusItem ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_39 ;
      private short nGXsfl_39_idx=1 ;
      private short AV13OrderedBy ;
      private short AV86ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_39_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV58ContagemItem_ValidacaoStatusFinal ;
      private short edtContagem_Codigo_Titleformat ;
      private short edtContagemItem_PFB_Titleformat ;
      private short edtContagemItem_PFL_Titleformat ;
      private short cmbContagemItem_TipoUnidade_Titleformat ;
      private short edtContagemItem_Evidencias_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV108TFContagem_Codigo ;
      private int AV109TFContagem_Codigo_To ;
      private int A224ContagemItem_Lancamento ;
      private int A192Contagem_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contagemitem_evidencias_Datalistupdateminimumcharacters ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int edtavTfcontagem_codigo_Visible ;
      private int edtavTfcontagem_codigo_to_Visible ;
      private int edtavTfcontagemitem_pfb_Visible ;
      private int edtavTfcontagemitem_pfb_to_Visible ;
      private int edtavTfcontagemitem_pfl_Visible ;
      private int edtavTfcontagemitem_pfl_to_Visible ;
      private int edtavTfcontagemitem_evidencias_Visible ;
      private int edtavTfcontagemitem_evidencias_sel_Visible ;
      private int edtavDdo_contagem_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemitem_pfbtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemitem_pfltitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemitem_tipounidadetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemitem_evidenciastitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels_Count ;
      private int AV133WWContagemItemDS_1_Tfcontagem_codigo ;
      private int AV134WWContagemItemDS_2_Tfcontagem_codigo_to ;
      private int A207Contagem_FabricaSoftwareCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV84Contagem_AreaTrabalhoCod ;
      private int AV112PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int AV146GXV1 ;
      private int AV147GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV113GridCurrentPage ;
      private long AV114GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV116TFContagemItem_PFB ;
      private decimal AV117TFContagemItem_PFB_To ;
      private decimal AV120TFContagemItem_PFL ;
      private decimal AV121TFContagemItem_PFL_To ;
      private decimal A950ContagemItem_PFB ;
      private decimal A951ContagemItem_PFL ;
      private decimal AV135WWContagemItemDS_3_Tfcontagemitem_pfb ;
      private decimal AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to ;
      private decimal AV137WWContagemItemDS_5_Tfcontagemitem_pfl ;
      private decimal AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contagem_codigo_Activeeventkey ;
      private String Ddo_contagem_codigo_Filteredtext_get ;
      private String Ddo_contagem_codigo_Filteredtextto_get ;
      private String Ddo_contagemitem_pfb_Activeeventkey ;
      private String Ddo_contagemitem_pfb_Filteredtext_get ;
      private String Ddo_contagemitem_pfb_Filteredtextto_get ;
      private String Ddo_contagemitem_pfl_Activeeventkey ;
      private String Ddo_contagemitem_pfl_Filteredtext_get ;
      private String Ddo_contagemitem_pfl_Filteredtextto_get ;
      private String Ddo_contagemitem_tipounidade_Activeeventkey ;
      private String Ddo_contagemitem_tipounidade_Selectedvalue_get ;
      private String Ddo_contagemitem_evidencias_Activeeventkey ;
      private String Ddo_contagemitem_evidencias_Filteredtext_get ;
      private String Ddo_contagemitem_evidencias_Selectedvalue_get ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_39_idx="0001" ;
      private String AV145Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contagem_codigo_Caption ;
      private String Ddo_contagem_codigo_Tooltip ;
      private String Ddo_contagem_codigo_Cls ;
      private String Ddo_contagem_codigo_Filteredtext_set ;
      private String Ddo_contagem_codigo_Filteredtextto_set ;
      private String Ddo_contagem_codigo_Dropdownoptionstype ;
      private String Ddo_contagem_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contagem_codigo_Sortedstatus ;
      private String Ddo_contagem_codigo_Filtertype ;
      private String Ddo_contagem_codigo_Sortasc ;
      private String Ddo_contagem_codigo_Sortdsc ;
      private String Ddo_contagem_codigo_Cleanfilter ;
      private String Ddo_contagem_codigo_Rangefilterfrom ;
      private String Ddo_contagem_codigo_Rangefilterto ;
      private String Ddo_contagem_codigo_Searchbuttontext ;
      private String Ddo_contagemitem_pfb_Caption ;
      private String Ddo_contagemitem_pfb_Tooltip ;
      private String Ddo_contagemitem_pfb_Cls ;
      private String Ddo_contagemitem_pfb_Filteredtext_set ;
      private String Ddo_contagemitem_pfb_Filteredtextto_set ;
      private String Ddo_contagemitem_pfb_Dropdownoptionstype ;
      private String Ddo_contagemitem_pfb_Titlecontrolidtoreplace ;
      private String Ddo_contagemitem_pfb_Sortedstatus ;
      private String Ddo_contagemitem_pfb_Filtertype ;
      private String Ddo_contagemitem_pfb_Sortasc ;
      private String Ddo_contagemitem_pfb_Sortdsc ;
      private String Ddo_contagemitem_pfb_Cleanfilter ;
      private String Ddo_contagemitem_pfb_Rangefilterfrom ;
      private String Ddo_contagemitem_pfb_Rangefilterto ;
      private String Ddo_contagemitem_pfb_Searchbuttontext ;
      private String Ddo_contagemitem_pfl_Caption ;
      private String Ddo_contagemitem_pfl_Tooltip ;
      private String Ddo_contagemitem_pfl_Cls ;
      private String Ddo_contagemitem_pfl_Filteredtext_set ;
      private String Ddo_contagemitem_pfl_Filteredtextto_set ;
      private String Ddo_contagemitem_pfl_Dropdownoptionstype ;
      private String Ddo_contagemitem_pfl_Titlecontrolidtoreplace ;
      private String Ddo_contagemitem_pfl_Sortedstatus ;
      private String Ddo_contagemitem_pfl_Filtertype ;
      private String Ddo_contagemitem_pfl_Sortasc ;
      private String Ddo_contagemitem_pfl_Sortdsc ;
      private String Ddo_contagemitem_pfl_Cleanfilter ;
      private String Ddo_contagemitem_pfl_Rangefilterfrom ;
      private String Ddo_contagemitem_pfl_Rangefilterto ;
      private String Ddo_contagemitem_pfl_Searchbuttontext ;
      private String Ddo_contagemitem_tipounidade_Caption ;
      private String Ddo_contagemitem_tipounidade_Tooltip ;
      private String Ddo_contagemitem_tipounidade_Cls ;
      private String Ddo_contagemitem_tipounidade_Selectedvalue_set ;
      private String Ddo_contagemitem_tipounidade_Dropdownoptionstype ;
      private String Ddo_contagemitem_tipounidade_Titlecontrolidtoreplace ;
      private String Ddo_contagemitem_tipounidade_Sortedstatus ;
      private String Ddo_contagemitem_tipounidade_Datalisttype ;
      private String Ddo_contagemitem_tipounidade_Datalistfixedvalues ;
      private String Ddo_contagemitem_tipounidade_Sortasc ;
      private String Ddo_contagemitem_tipounidade_Sortdsc ;
      private String Ddo_contagemitem_tipounidade_Cleanfilter ;
      private String Ddo_contagemitem_tipounidade_Searchbuttontext ;
      private String Ddo_contagemitem_evidencias_Caption ;
      private String Ddo_contagemitem_evidencias_Tooltip ;
      private String Ddo_contagemitem_evidencias_Cls ;
      private String Ddo_contagemitem_evidencias_Filteredtext_set ;
      private String Ddo_contagemitem_evidencias_Selectedvalue_set ;
      private String Ddo_contagemitem_evidencias_Dropdownoptionstype ;
      private String Ddo_contagemitem_evidencias_Titlecontrolidtoreplace ;
      private String Ddo_contagemitem_evidencias_Sortedstatus ;
      private String Ddo_contagemitem_evidencias_Filtertype ;
      private String Ddo_contagemitem_evidencias_Datalisttype ;
      private String Ddo_contagemitem_evidencias_Datalistproc ;
      private String Ddo_contagemitem_evidencias_Sortasc ;
      private String Ddo_contagemitem_evidencias_Sortdsc ;
      private String Ddo_contagemitem_evidencias_Loadingdata ;
      private String Ddo_contagemitem_evidencias_Cleanfilter ;
      private String Ddo_contagemitem_evidencias_Noresultsfound ;
      private String Ddo_contagemitem_evidencias_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String edtavTfcontagem_codigo_Internalname ;
      private String edtavTfcontagem_codigo_Jsonclick ;
      private String edtavTfcontagem_codigo_to_Internalname ;
      private String edtavTfcontagem_codigo_to_Jsonclick ;
      private String edtavTfcontagemitem_pfb_Internalname ;
      private String edtavTfcontagemitem_pfb_Jsonclick ;
      private String edtavTfcontagemitem_pfb_to_Internalname ;
      private String edtavTfcontagemitem_pfb_to_Jsonclick ;
      private String edtavTfcontagemitem_pfl_Internalname ;
      private String edtavTfcontagemitem_pfl_Jsonclick ;
      private String edtavTfcontagemitem_pfl_to_Internalname ;
      private String edtavTfcontagemitem_pfl_to_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavTfcontagemitem_evidencias_Internalname ;
      private String edtavTfcontagemitem_evidencias_sel_Internalname ;
      private String edtavDdo_contagem_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemitem_pfbtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemitem_pfltitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemitem_tipounidadetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemitem_evidenciastitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtContagem_Codigo_Internalname ;
      private String edtContagemItem_PFB_Internalname ;
      private String edtContagemItem_PFL_Internalname ;
      private String cmbContagemItem_TipoUnidade_Internalname ;
      private String edtContagemItem_Evidencias_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contagem_codigo_Internalname ;
      private String Ddo_contagemitem_pfb_Internalname ;
      private String Ddo_contagemitem_pfl_Internalname ;
      private String Ddo_contagemitem_tipounidade_Internalname ;
      private String Ddo_contagemitem_evidencias_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtContagem_Codigo_Title ;
      private String edtContagemItem_PFB_Title ;
      private String edtContagemItem_PFL_Title ;
      private String edtContagemItem_Evidencias_Title ;
      private String lblContagemitemtitle_Caption ;
      private String lblContagemitemtitle_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String GXt_char2 ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String tblTableactions_Internalname ;
      private String lblContagemitemtitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String sGXsfl_39_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContagem_Codigo_Jsonclick ;
      private String edtContagemItem_PFB_Jsonclick ;
      private String edtContagemItem_PFL_Jsonclick ;
      private String cmbContagemItem_TipoUnidade_Jsonclick ;
      private String edtContagemItem_Evidencias_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contagem_codigo_Includesortasc ;
      private bool Ddo_contagem_codigo_Includesortdsc ;
      private bool Ddo_contagem_codigo_Includefilter ;
      private bool Ddo_contagem_codigo_Filterisrange ;
      private bool Ddo_contagem_codigo_Includedatalist ;
      private bool Ddo_contagemitem_pfb_Includesortasc ;
      private bool Ddo_contagemitem_pfb_Includesortdsc ;
      private bool Ddo_contagemitem_pfb_Includefilter ;
      private bool Ddo_contagemitem_pfb_Filterisrange ;
      private bool Ddo_contagemitem_pfb_Includedatalist ;
      private bool Ddo_contagemitem_pfl_Includesortasc ;
      private bool Ddo_contagemitem_pfl_Includesortdsc ;
      private bool Ddo_contagemitem_pfl_Includefilter ;
      private bool Ddo_contagemitem_pfl_Filterisrange ;
      private bool Ddo_contagemitem_pfl_Includedatalist ;
      private bool Ddo_contagemitem_tipounidade_Includesortasc ;
      private bool Ddo_contagemitem_tipounidade_Includesortdsc ;
      private bool Ddo_contagemitem_tipounidade_Includefilter ;
      private bool Ddo_contagemitem_tipounidade_Includedatalist ;
      private bool Ddo_contagemitem_tipounidade_Allowmultipleselection ;
      private bool Ddo_contagemitem_evidencias_Includesortasc ;
      private bool Ddo_contagemitem_evidencias_Includesortdsc ;
      private bool Ddo_contagemitem_evidencias_Includefilter ;
      private bool Ddo_contagemitem_evidencias_Filterisrange ;
      private bool Ddo_contagemitem_evidencias_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n950ContagemItem_PFB ;
      private bool n951ContagemItem_PFL ;
      private bool n953ContagemItem_Evidencias ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Update_IsBlob ;
      private bool AV32Delete_IsBlob ;
      private bool AV55Display_IsBlob ;
      private String A953ContagemItem_Evidencias ;
      private String AV124TFContagemItem_TipoUnidade_SelsJson ;
      private String AV87ManageFiltersXml ;
      private String AV128TFContagemItem_Evidencias ;
      private String AV129TFContagemItem_Evidencias_Sel ;
      private String AV110ddo_Contagem_CodigoTitleControlIdToReplace ;
      private String AV118ddo_ContagemItem_PFBTitleControlIdToReplace ;
      private String AV122ddo_ContagemItem_PFLTitleControlIdToReplace ;
      private String AV126ddo_ContagemItem_TipoUnidadeTitleControlIdToReplace ;
      private String AV130ddo_ContagemItem_EvidenciasTitleControlIdToReplace ;
      private String AV142Update_GXI ;
      private String AV143Delete_GXI ;
      private String AV144Display_GXI ;
      private String A952ContagemItem_TipoUnidade ;
      private String lV140WWContagemItemDS_8_Tfcontagemitem_evidencias ;
      private String AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel ;
      private String AV140WWContagemItemDS_8_Tfcontagemitem_evidencias ;
      private String AV31Update ;
      private String AV32Delete ;
      private String AV55Display ;
      private IGxSession AV33Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbContagemItem_TipoUnidade ;
      private IDataStoreProvider pr_default ;
      private int[] H00532_A207Contagem_FabricaSoftwareCod ;
      private int[] H00532_A224ContagemItem_Lancamento ;
      private String[] H00532_A953ContagemItem_Evidencias ;
      private bool[] H00532_n953ContagemItem_Evidencias ;
      private String[] H00532_A952ContagemItem_TipoUnidade ;
      private decimal[] H00532_A951ContagemItem_PFL ;
      private bool[] H00532_n951ContagemItem_PFL ;
      private decimal[] H00532_A950ContagemItem_PFB ;
      private bool[] H00532_n950ContagemItem_PFB ;
      private int[] H00532_A192Contagem_Codigo ;
      private long[] H00533_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV125TFContagemItem_TipoUnidade_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV88ManageFiltersItems ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV90ManageFiltersData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV107Contagem_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV115ContagemItem_PFBTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV119ContagemItem_PFLTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV123ContagemItem_TipoUnidadeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV127ContagemItem_EvidenciasTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private SdtNotificationInfo AV85NotificationInfo ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV89ManageFiltersItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV91ManageFiltersDataItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV111DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwcontagemitem__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00532( IGxContext context ,
                                             String A952ContagemItem_TipoUnidade ,
                                             IGxCollection AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels ,
                                             int AV133WWContagemItemDS_1_Tfcontagem_codigo ,
                                             int AV134WWContagemItemDS_2_Tfcontagem_codigo_to ,
                                             decimal AV135WWContagemItemDS_3_Tfcontagemitem_pfb ,
                                             decimal AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to ,
                                             decimal AV137WWContagemItemDS_5_Tfcontagemitem_pfl ,
                                             decimal AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to ,
                                             int AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels_Count ,
                                             String AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel ,
                                             String AV140WWContagemItemDS_8_Tfcontagemitem_evidencias ,
                                             int A192Contagem_Codigo ,
                                             decimal A950ContagemItem_PFB ,
                                             decimal A951ContagemItem_PFL ,
                                             String A953ContagemItem_Evidencias ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [13] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Contagem_FabricaSoftwareCod], [ContagemItem_Lancamento], [ContagemItem_Evidencias], [ContagemItem_TipoUnidade], [ContagemItem_PFL], [ContagemItem_PFB], [Contagem_Codigo]";
         sFromString = " FROM [ContagemItem] WITH (NOLOCK)";
         sOrderString = "";
         if ( ! (0==AV133WWContagemItemDS_1_Tfcontagem_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Contagem_Codigo] >= @AV133WWContagemItemDS_1_Tfcontagem_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Contagem_Codigo] >= @AV133WWContagemItemDS_1_Tfcontagem_codigo)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ! (0==AV134WWContagemItemDS_2_Tfcontagem_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Contagem_Codigo] <= @AV134WWContagemItemDS_2_Tfcontagem_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Contagem_Codigo] <= @AV134WWContagemItemDS_2_Tfcontagem_codigo_to)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV135WWContagemItemDS_3_Tfcontagemitem_pfb) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemItem_PFB] >= @AV135WWContagemItemDS_3_Tfcontagemitem_pfb)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemItem_PFB] >= @AV135WWContagemItemDS_3_Tfcontagemitem_pfb)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemItem_PFB] <= @AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemItem_PFB] <= @AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV137WWContagemItemDS_5_Tfcontagemitem_pfl) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemItem_PFL] >= @AV137WWContagemItemDS_5_Tfcontagemitem_pfl)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemItem_PFL] >= @AV137WWContagemItemDS_5_Tfcontagemitem_pfl)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemItem_PFL] <= @AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemItem_PFL] <= @AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels, "[ContagemItem_TipoUnidade] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels, "[ContagemItem_TipoUnidade] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContagemItemDS_8_Tfcontagemitem_evidencias)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemItem_Evidencias] like @lV140WWContagemItemDS_8_Tfcontagemitem_evidencias)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemItem_Evidencias] like @lV140WWContagemItemDS_8_Tfcontagemitem_evidencias)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemItem_Evidencias] = @AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemItem_Evidencias] = @AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( AV13OrderedBy == 1 )
         {
            sOrderString = sOrderString + " ORDER BY [Contagem_FabricaSoftwareCod]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Contagem_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Contagem_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemItem_PFB]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemItem_PFB] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemItem_PFL]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemItem_PFL] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemItem_TipoUnidade]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemItem_TipoUnidade] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemItem_Evidencias]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemItem_Evidencias] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemItem_Lancamento]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00533( IGxContext context ,
                                             String A952ContagemItem_TipoUnidade ,
                                             IGxCollection AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels ,
                                             int AV133WWContagemItemDS_1_Tfcontagem_codigo ,
                                             int AV134WWContagemItemDS_2_Tfcontagem_codigo_to ,
                                             decimal AV135WWContagemItemDS_3_Tfcontagemitem_pfb ,
                                             decimal AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to ,
                                             decimal AV137WWContagemItemDS_5_Tfcontagemitem_pfl ,
                                             decimal AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to ,
                                             int AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels_Count ,
                                             String AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel ,
                                             String AV140WWContagemItemDS_8_Tfcontagemitem_evidencias ,
                                             int A192Contagem_Codigo ,
                                             decimal A950ContagemItem_PFB ,
                                             decimal A951ContagemItem_PFL ,
                                             String A953ContagemItem_Evidencias ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [8] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [ContagemItem] WITH (NOLOCK)";
         if ( ! (0==AV133WWContagemItemDS_1_Tfcontagem_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Contagem_Codigo] >= @AV133WWContagemItemDS_1_Tfcontagem_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Contagem_Codigo] >= @AV133WWContagemItemDS_1_Tfcontagem_codigo)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ! (0==AV134WWContagemItemDS_2_Tfcontagem_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Contagem_Codigo] <= @AV134WWContagemItemDS_2_Tfcontagem_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Contagem_Codigo] <= @AV134WWContagemItemDS_2_Tfcontagem_codigo_to)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV135WWContagemItemDS_3_Tfcontagemitem_pfb) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemItem_PFB] >= @AV135WWContagemItemDS_3_Tfcontagemitem_pfb)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemItem_PFB] >= @AV135WWContagemItemDS_3_Tfcontagemitem_pfb)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemItem_PFB] <= @AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemItem_PFB] <= @AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV137WWContagemItemDS_5_Tfcontagemitem_pfl) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemItem_PFL] >= @AV137WWContagemItemDS_5_Tfcontagemitem_pfl)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemItem_PFL] >= @AV137WWContagemItemDS_5_Tfcontagemitem_pfl)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemItem_PFL] <= @AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemItem_PFL] <= @AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels, "[ContagemItem_TipoUnidade] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV139WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels, "[ContagemItem_TipoUnidade] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContagemItemDS_8_Tfcontagemitem_evidencias)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemItem_Evidencias] like @lV140WWContagemItemDS_8_Tfcontagemitem_evidencias)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemItem_Evidencias] like @lV140WWContagemItemDS_8_Tfcontagemitem_evidencias)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemItem_Evidencias] = @AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemItem_Evidencias] = @AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( AV13OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00532(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (decimal)dynConstraints[6] , (decimal)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (bool)dynConstraints[16] );
               case 1 :
                     return conditional_H00533(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (decimal)dynConstraints[6] , (decimal)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (bool)dynConstraints[16] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00532 ;
          prmH00532 = new Object[] {
          new Object[] {"@AV133WWContagemItemDS_1_Tfcontagem_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV134WWContagemItemDS_2_Tfcontagem_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV135WWContagemItemDS_3_Tfcontagemitem_pfb",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV137WWContagemItemDS_5_Tfcontagemitem_pfl",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@lV140WWContagemItemDS_8_Tfcontagemitem_evidencias",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00533 ;
          prmH00533 = new Object[] {
          new Object[] {"@AV133WWContagemItemDS_1_Tfcontagem_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV134WWContagemItemDS_2_Tfcontagem_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV135WWContagemItemDS_3_Tfcontagemitem_pfb",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV136WWContagemItemDS_4_Tfcontagemitem_pfb_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV137WWContagemItemDS_5_Tfcontagemitem_pfl",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV138WWContagemItemDS_6_Tfcontagemitem_pfl_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@lV140WWContagemItemDS_8_Tfcontagemitem_evidencias",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV141WWContagemItemDS_9_Tfcontagemitem_evidencias_sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00532", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00532,11,0,true,false )
             ,new CursorDef("H00533", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00533,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
       }
    }

 }

}
