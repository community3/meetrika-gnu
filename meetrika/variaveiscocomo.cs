/*
               File: VariaveisCocomo
        Description: Vari�vel Cocomo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:26:2.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class variaveiscocomo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_20") == 0 )
         {
            A961VariavelCocomo_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_20( A961VariavelCocomo_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_21") == 0 )
         {
            A965VariavelCocomo_ProjetoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n965VariavelCocomo_ProjetoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A965VariavelCocomo_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A965VariavelCocomo_ProjetoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_21( A965VariavelCocomo_ProjetoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7VariavelCocomo_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7VariavelCocomo_AreaTrabalhoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vVARIAVELCOCOMO_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7VariavelCocomo_AreaTrabalhoCod), "ZZZZZ9")));
               AV8VariavelCocomo_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8VariavelCocomo_Sigla", AV8VariavelCocomo_Sigla);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vVARIAVELCOCOMO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV8VariavelCocomo_Sigla, "@!"))));
               AV9VariavelCocomo_Tipo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9VariavelCocomo_Tipo", AV9VariavelCocomo_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vVARIAVELCOCOMO_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV9VariavelCocomo_Tipo, ""))));
               AV16VariavelCocomo_Sequencial = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16VariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16VariavelCocomo_Sequencial), 3, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vVARIAVELCOCOMO_SEQUENCIAL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16VariavelCocomo_Sequencial), "ZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbVariavelCocomo_Tipo.Name = "VARIAVELCOCOMO_TIPO";
         cmbVariavelCocomo_Tipo.WebTags = "";
         cmbVariavelCocomo_Tipo.addItem("", "(Selecionar)", 0);
         cmbVariavelCocomo_Tipo.addItem("E", "Escala", 0);
         cmbVariavelCocomo_Tipo.addItem("M", "Multiplicador", 0);
         if ( cmbVariavelCocomo_Tipo.ItemCount > 0 )
         {
            A964VariavelCocomo_Tipo = cmbVariavelCocomo_Tipo.getValidValue(A964VariavelCocomo_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Vari�vel Cocomo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtVariavelCocomo_Sigla_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public variaveiscocomo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public variaveiscocomo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_VariavelCocomo_AreaTrabalhoCod ,
                           String aP2_VariavelCocomo_Sigla ,
                           String aP3_VariavelCocomo_Tipo ,
                           short aP4_VariavelCocomo_Sequencial )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7VariavelCocomo_AreaTrabalhoCod = aP1_VariavelCocomo_AreaTrabalhoCod;
         this.AV8VariavelCocomo_Sigla = aP2_VariavelCocomo_Sigla;
         this.AV9VariavelCocomo_Tipo = aP3_VariavelCocomo_Tipo;
         this.AV16VariavelCocomo_Sequencial = aP4_VariavelCocomo_Sequencial;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbVariavelCocomo_Tipo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbVariavelCocomo_Tipo.ItemCount > 0 )
         {
            A964VariavelCocomo_Tipo = cmbVariavelCocomo_Tipo.getValidValue(A964VariavelCocomo_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2X120( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2X120e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_AreaTrabalhoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_AreaTrabalhoCod_Jsonclick, 0, "Attribute", "", "", "", edtVariavelCocomo_AreaTrabalhoCod_Visible, edtVariavelCocomo_AreaTrabalhoCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_VariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_ProjetoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A965VariavelCocomo_ProjetoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A965VariavelCocomo_ProjetoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_ProjetoCod_Jsonclick, 0, "Attribute", "", "", "", edtVariavelCocomo_ProjetoCod_Visible, edtVariavelCocomo_ProjetoCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_VariaveisCocomo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_Usado_Internalname, StringUtil.LTrim( StringUtil.NToC( A973VariavelCocomo_Usado, 18, 2, ",", "")), ((edtVariavelCocomo_Usado_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A973VariavelCocomo_Usado, "ZZ,ZZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A973VariavelCocomo_Usado, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_Usado_Jsonclick, 0, "Attribute", "", "", "", edtVariavelCocomo_Usado_Visible, edtVariavelCocomo_Usado_Enabled, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor2Casas", "right", false, "HLP_VariaveisCocomo.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2X120( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2X120( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2X120e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_71_2X120( true) ;
         }
         return  ;
      }

      protected void wb_table3_71_2X120e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2X120e( true) ;
         }
         else
         {
            wb_table1_2_2X120e( false) ;
         }
      }

      protected void wb_table3_71_2X120( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_71_2X120e( true) ;
         }
         else
         {
            wb_table3_71_2X120e( false) ;
         }
      }

      protected void wb_table2_5_2X120( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2X120( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2X120e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2X120e( true) ;
         }
         else
         {
            wb_table2_5_2X120e( false) ;
         }
      }

      protected void wb_table4_13_2X120( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_sigla_Internalname, "Sigla", "", "", lblTextblockvariavelcocomo_sigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_Sigla_Internalname, StringUtil.RTrim( A962VariavelCocomo_Sigla), StringUtil.RTrim( context.localUtil.Format( A962VariavelCocomo_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_Sigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtVariavelCocomo_Sigla_Enabled, 1, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_nome_Internalname, "Nome", "", "", lblTextblockvariavelcocomo_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_Nome_Internalname, StringUtil.RTrim( A963VariavelCocomo_Nome), StringUtil.RTrim( context.localUtil.Format( A963VariavelCocomo_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtVariavelCocomo_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_tipo_Internalname, "Tipo", "", "", lblTextblockvariavelcocomo_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbVariavelCocomo_Tipo, cmbVariavelCocomo_Tipo_Internalname, StringUtil.RTrim( A964VariavelCocomo_Tipo), 1, cmbVariavelCocomo_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbVariavelCocomo_Tipo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_VariaveisCocomo.htm");
            cmbVariavelCocomo_Tipo.CurrentValue = StringUtil.RTrim( A964VariavelCocomo_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbVariavelCocomo_Tipo_Internalname, "Values", (String)(cmbVariavelCocomo_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_data_Internalname, "Data", "", "", lblTextblockvariavelcocomo_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtVariavelCocomo_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_Data_Internalname, context.localUtil.Format(A966VariavelCocomo_Data, "99/99/99"), context.localUtil.Format( A966VariavelCocomo_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_Data_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtVariavelCocomo_Data_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_VariaveisCocomo.htm");
            GxWebStd.gx_bitmap( context, edtVariavelCocomo_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtVariavelCocomo_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_extrabaixo_Internalname, "Extra Baixo", "", "", lblTextblockvariavelcocomo_extrabaixo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_ExtraBaixo_Internalname, StringUtil.LTrim( StringUtil.NToC( A986VariavelCocomo_ExtraBaixo, 18, 2, ",", "")), ((edtVariavelCocomo_ExtraBaixo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A986VariavelCocomo_ExtraBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A986VariavelCocomo_ExtraBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_ExtraBaixo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtVariavelCocomo_ExtraBaixo_Enabled, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor2Casas", "right", false, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_muitobaixo_Internalname, "Muito Baixo", "", "", lblTextblockvariavelcocomo_muitobaixo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_MuitoBaixo_Internalname, StringUtil.LTrim( StringUtil.NToC( A967VariavelCocomo_MuitoBaixo, 18, 2, ",", "")), ((edtVariavelCocomo_MuitoBaixo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A967VariavelCocomo_MuitoBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A967VariavelCocomo_MuitoBaixo, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_MuitoBaixo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtVariavelCocomo_MuitoBaixo_Enabled, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor2Casas", "right", false, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_baixo_Internalname, "Baixo", "", "", lblTextblockvariavelcocomo_baixo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_Baixo_Internalname, StringUtil.LTrim( StringUtil.NToC( A968VariavelCocomo_Baixo, 18, 2, ",", "")), ((edtVariavelCocomo_Baixo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A968VariavelCocomo_Baixo, "ZZ,ZZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A968VariavelCocomo_Baixo, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_Baixo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtVariavelCocomo_Baixo_Enabled, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor2Casas", "right", false, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_nominal_Internalname, "Nominal", "", "", lblTextblockvariavelcocomo_nominal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_Nominal_Internalname, StringUtil.LTrim( StringUtil.NToC( A969VariavelCocomo_Nominal, 18, 2, ",", "")), ((edtVariavelCocomo_Nominal_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A969VariavelCocomo_Nominal, "ZZ,ZZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A969VariavelCocomo_Nominal, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_Nominal_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtVariavelCocomo_Nominal_Enabled, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor2Casas", "right", false, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_alto_Internalname, "Alto", "", "", lblTextblockvariavelcocomo_alto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_Alto_Internalname, StringUtil.LTrim( StringUtil.NToC( A970VariavelCocomo_Alto, 18, 2, ",", "")), ((edtVariavelCocomo_Alto_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A970VariavelCocomo_Alto, "ZZ,ZZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A970VariavelCocomo_Alto, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_Alto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtVariavelCocomo_Alto_Enabled, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor2Casas", "right", false, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_muitoalto_Internalname, "Muito Alto", "", "", lblTextblockvariavelcocomo_muitoalto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_MuitoAlto_Internalname, StringUtil.LTrim( StringUtil.NToC( A971VariavelCocomo_MuitoAlto, 18, 2, ",", "")), ((edtVariavelCocomo_MuitoAlto_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A971VariavelCocomo_MuitoAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A971VariavelCocomo_MuitoAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_MuitoAlto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtVariavelCocomo_MuitoAlto_Enabled, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor2Casas", "right", false, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvariavelcocomo_extraalto_Internalname, "Extra Alto", "", "", lblTextblockvariavelcocomo_extraalto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_ExtraAlto_Internalname, StringUtil.LTrim( StringUtil.NToC( A972VariavelCocomo_ExtraAlto, 18, 2, ",", "")), ((edtVariavelCocomo_ExtraAlto_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A972VariavelCocomo_ExtraAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A972VariavelCocomo_ExtraAlto, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_ExtraAlto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtVariavelCocomo_ExtraAlto_Enabled, 0, "text", "", 50, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor2Casas", "right", false, "HLP_VariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2X120e( true) ;
         }
         else
         {
            wb_table4_13_2X120e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112X2 */
         E112X2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A962VariavelCocomo_Sigla = StringUtil.Upper( cgiGet( edtVariavelCocomo_Sigla_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A962VariavelCocomo_Sigla", A962VariavelCocomo_Sigla);
               A963VariavelCocomo_Nome = StringUtil.Upper( cgiGet( edtVariavelCocomo_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A963VariavelCocomo_Nome", A963VariavelCocomo_Nome);
               cmbVariavelCocomo_Tipo.CurrentValue = cgiGet( cmbVariavelCocomo_Tipo_Internalname);
               A964VariavelCocomo_Tipo = cgiGet( cmbVariavelCocomo_Tipo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
               if ( context.localUtil.VCDate( cgiGet( edtVariavelCocomo_Data_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data"}), 1, "VARIAVELCOCOMO_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtVariavelCocomo_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A966VariavelCocomo_Data = DateTime.MinValue;
                  n966VariavelCocomo_Data = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A966VariavelCocomo_Data", context.localUtil.Format(A966VariavelCocomo_Data, "99/99/99"));
               }
               else
               {
                  A966VariavelCocomo_Data = context.localUtil.CToD( cgiGet( edtVariavelCocomo_Data_Internalname), 2);
                  n966VariavelCocomo_Data = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A966VariavelCocomo_Data", context.localUtil.Format(A966VariavelCocomo_Data, "99/99/99"));
               }
               n966VariavelCocomo_Data = ((DateTime.MinValue==A966VariavelCocomo_Data) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_ExtraBaixo_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_ExtraBaixo_Internalname), ",", ".") > 999999999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "VARIAVELCOCOMO_EXTRABAIXO");
                  AnyError = 1;
                  GX_FocusControl = edtVariavelCocomo_ExtraBaixo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A986VariavelCocomo_ExtraBaixo = 0;
                  n986VariavelCocomo_ExtraBaixo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A986VariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( A986VariavelCocomo_ExtraBaixo, 12, 2)));
               }
               else
               {
                  A986VariavelCocomo_ExtraBaixo = context.localUtil.CToN( cgiGet( edtVariavelCocomo_ExtraBaixo_Internalname), ",", ".");
                  n986VariavelCocomo_ExtraBaixo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A986VariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( A986VariavelCocomo_ExtraBaixo, 12, 2)));
               }
               n986VariavelCocomo_ExtraBaixo = ((Convert.ToDecimal(0)==A986VariavelCocomo_ExtraBaixo) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_MuitoBaixo_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_MuitoBaixo_Internalname), ",", ".") > 999999999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "VARIAVELCOCOMO_MUITOBAIXO");
                  AnyError = 1;
                  GX_FocusControl = edtVariavelCocomo_MuitoBaixo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A967VariavelCocomo_MuitoBaixo = 0;
                  n967VariavelCocomo_MuitoBaixo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A967VariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( A967VariavelCocomo_MuitoBaixo, 12, 2)));
               }
               else
               {
                  A967VariavelCocomo_MuitoBaixo = context.localUtil.CToN( cgiGet( edtVariavelCocomo_MuitoBaixo_Internalname), ",", ".");
                  n967VariavelCocomo_MuitoBaixo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A967VariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( A967VariavelCocomo_MuitoBaixo, 12, 2)));
               }
               n967VariavelCocomo_MuitoBaixo = ((Convert.ToDecimal(0)==A967VariavelCocomo_MuitoBaixo) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_Baixo_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_Baixo_Internalname), ",", ".") > 999999999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "VARIAVELCOCOMO_BAIXO");
                  AnyError = 1;
                  GX_FocusControl = edtVariavelCocomo_Baixo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A968VariavelCocomo_Baixo = 0;
                  n968VariavelCocomo_Baixo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A968VariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( A968VariavelCocomo_Baixo, 12, 2)));
               }
               else
               {
                  A968VariavelCocomo_Baixo = context.localUtil.CToN( cgiGet( edtVariavelCocomo_Baixo_Internalname), ",", ".");
                  n968VariavelCocomo_Baixo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A968VariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( A968VariavelCocomo_Baixo, 12, 2)));
               }
               n968VariavelCocomo_Baixo = ((Convert.ToDecimal(0)==A968VariavelCocomo_Baixo) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_Nominal_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_Nominal_Internalname), ",", ".") > 999999999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "VARIAVELCOCOMO_NOMINAL");
                  AnyError = 1;
                  GX_FocusControl = edtVariavelCocomo_Nominal_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A969VariavelCocomo_Nominal = 0;
                  n969VariavelCocomo_Nominal = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A969VariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( A969VariavelCocomo_Nominal, 12, 2)));
               }
               else
               {
                  A969VariavelCocomo_Nominal = context.localUtil.CToN( cgiGet( edtVariavelCocomo_Nominal_Internalname), ",", ".");
                  n969VariavelCocomo_Nominal = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A969VariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( A969VariavelCocomo_Nominal, 12, 2)));
               }
               n969VariavelCocomo_Nominal = ((Convert.ToDecimal(0)==A969VariavelCocomo_Nominal) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_Alto_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_Alto_Internalname), ",", ".") > 999999999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "VARIAVELCOCOMO_ALTO");
                  AnyError = 1;
                  GX_FocusControl = edtVariavelCocomo_Alto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A970VariavelCocomo_Alto = 0;
                  n970VariavelCocomo_Alto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A970VariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( A970VariavelCocomo_Alto, 12, 2)));
               }
               else
               {
                  A970VariavelCocomo_Alto = context.localUtil.CToN( cgiGet( edtVariavelCocomo_Alto_Internalname), ",", ".");
                  n970VariavelCocomo_Alto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A970VariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( A970VariavelCocomo_Alto, 12, 2)));
               }
               n970VariavelCocomo_Alto = ((Convert.ToDecimal(0)==A970VariavelCocomo_Alto) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_MuitoAlto_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_MuitoAlto_Internalname), ",", ".") > 999999999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "VARIAVELCOCOMO_MUITOALTO");
                  AnyError = 1;
                  GX_FocusControl = edtVariavelCocomo_MuitoAlto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A971VariavelCocomo_MuitoAlto = 0;
                  n971VariavelCocomo_MuitoAlto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A971VariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( A971VariavelCocomo_MuitoAlto, 12, 2)));
               }
               else
               {
                  A971VariavelCocomo_MuitoAlto = context.localUtil.CToN( cgiGet( edtVariavelCocomo_MuitoAlto_Internalname), ",", ".");
                  n971VariavelCocomo_MuitoAlto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A971VariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( A971VariavelCocomo_MuitoAlto, 12, 2)));
               }
               n971VariavelCocomo_MuitoAlto = ((Convert.ToDecimal(0)==A971VariavelCocomo_MuitoAlto) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_ExtraAlto_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_ExtraAlto_Internalname), ",", ".") > 999999999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "VARIAVELCOCOMO_EXTRAALTO");
                  AnyError = 1;
                  GX_FocusControl = edtVariavelCocomo_ExtraAlto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A972VariavelCocomo_ExtraAlto = 0;
                  n972VariavelCocomo_ExtraAlto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A972VariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( A972VariavelCocomo_ExtraAlto, 12, 2)));
               }
               else
               {
                  A972VariavelCocomo_ExtraAlto = context.localUtil.CToN( cgiGet( edtVariavelCocomo_ExtraAlto_Internalname), ",", ".");
                  n972VariavelCocomo_ExtraAlto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A972VariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( A972VariavelCocomo_ExtraAlto, 12, 2)));
               }
               n972VariavelCocomo_ExtraAlto = ((Convert.ToDecimal(0)==A972VariavelCocomo_ExtraAlto) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_AreaTrabalhoCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_AreaTrabalhoCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "VARIAVELCOCOMO_AREATRABALHOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtVariavelCocomo_AreaTrabalhoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A961VariavelCocomo_AreaTrabalhoCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
               }
               else
               {
                  A961VariavelCocomo_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtVariavelCocomo_AreaTrabalhoCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_ProjetoCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_ProjetoCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "VARIAVELCOCOMO_PROJETOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtVariavelCocomo_ProjetoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A965VariavelCocomo_ProjetoCod = 0;
                  n965VariavelCocomo_ProjetoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A965VariavelCocomo_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A965VariavelCocomo_ProjetoCod), 6, 0)));
               }
               else
               {
                  A965VariavelCocomo_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( edtVariavelCocomo_ProjetoCod_Internalname), ",", "."));
                  n965VariavelCocomo_ProjetoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A965VariavelCocomo_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A965VariavelCocomo_ProjetoCod), 6, 0)));
               }
               n965VariavelCocomo_ProjetoCod = ((0==A965VariavelCocomo_ProjetoCod) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_Usado_Internalname), ",", ".") < -99999999.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtVariavelCocomo_Usado_Internalname), ",", ".") > 999999999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "VARIAVELCOCOMO_USADO");
                  AnyError = 1;
                  GX_FocusControl = edtVariavelCocomo_Usado_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A973VariavelCocomo_Usado = 0;
                  n973VariavelCocomo_Usado = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A973VariavelCocomo_Usado", StringUtil.LTrim( StringUtil.Str( A973VariavelCocomo_Usado, 12, 2)));
               }
               else
               {
                  A973VariavelCocomo_Usado = context.localUtil.CToN( cgiGet( edtVariavelCocomo_Usado_Internalname), ",", ".");
                  n973VariavelCocomo_Usado = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A973VariavelCocomo_Usado", StringUtil.LTrim( StringUtil.Str( A973VariavelCocomo_Usado, 12, 2)));
               }
               n973VariavelCocomo_Usado = ((Convert.ToDecimal(0)==A973VariavelCocomo_Usado) ? true : false);
               /* Read saved values. */
               Z961VariavelCocomo_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z961VariavelCocomo_AreaTrabalhoCod"), ",", "."));
               Z962VariavelCocomo_Sigla = cgiGet( "Z962VariavelCocomo_Sigla");
               Z964VariavelCocomo_Tipo = cgiGet( "Z964VariavelCocomo_Tipo");
               Z992VariavelCocomo_Sequencial = (short)(context.localUtil.CToN( cgiGet( "Z992VariavelCocomo_Sequencial"), ",", "."));
               Z963VariavelCocomo_Nome = cgiGet( "Z963VariavelCocomo_Nome");
               Z966VariavelCocomo_Data = context.localUtil.CToD( cgiGet( "Z966VariavelCocomo_Data"), 0);
               n966VariavelCocomo_Data = ((DateTime.MinValue==A966VariavelCocomo_Data) ? true : false);
               Z986VariavelCocomo_ExtraBaixo = context.localUtil.CToN( cgiGet( "Z986VariavelCocomo_ExtraBaixo"), ",", ".");
               n986VariavelCocomo_ExtraBaixo = ((Convert.ToDecimal(0)==A986VariavelCocomo_ExtraBaixo) ? true : false);
               Z967VariavelCocomo_MuitoBaixo = context.localUtil.CToN( cgiGet( "Z967VariavelCocomo_MuitoBaixo"), ",", ".");
               n967VariavelCocomo_MuitoBaixo = ((Convert.ToDecimal(0)==A967VariavelCocomo_MuitoBaixo) ? true : false);
               Z968VariavelCocomo_Baixo = context.localUtil.CToN( cgiGet( "Z968VariavelCocomo_Baixo"), ",", ".");
               n968VariavelCocomo_Baixo = ((Convert.ToDecimal(0)==A968VariavelCocomo_Baixo) ? true : false);
               Z969VariavelCocomo_Nominal = context.localUtil.CToN( cgiGet( "Z969VariavelCocomo_Nominal"), ",", ".");
               n969VariavelCocomo_Nominal = ((Convert.ToDecimal(0)==A969VariavelCocomo_Nominal) ? true : false);
               Z970VariavelCocomo_Alto = context.localUtil.CToN( cgiGet( "Z970VariavelCocomo_Alto"), ",", ".");
               n970VariavelCocomo_Alto = ((Convert.ToDecimal(0)==A970VariavelCocomo_Alto) ? true : false);
               Z971VariavelCocomo_MuitoAlto = context.localUtil.CToN( cgiGet( "Z971VariavelCocomo_MuitoAlto"), ",", ".");
               n971VariavelCocomo_MuitoAlto = ((Convert.ToDecimal(0)==A971VariavelCocomo_MuitoAlto) ? true : false);
               Z972VariavelCocomo_ExtraAlto = context.localUtil.CToN( cgiGet( "Z972VariavelCocomo_ExtraAlto"), ",", ".");
               n972VariavelCocomo_ExtraAlto = ((Convert.ToDecimal(0)==A972VariavelCocomo_ExtraAlto) ? true : false);
               Z973VariavelCocomo_Usado = context.localUtil.CToN( cgiGet( "Z973VariavelCocomo_Usado"), ",", ".");
               n973VariavelCocomo_Usado = ((Convert.ToDecimal(0)==A973VariavelCocomo_Usado) ? true : false);
               Z965VariavelCocomo_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( "Z965VariavelCocomo_ProjetoCod"), ",", "."));
               n965VariavelCocomo_ProjetoCod = ((0==A965VariavelCocomo_ProjetoCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N965VariavelCocomo_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( "N965VariavelCocomo_ProjetoCod"), ",", "."));
               n965VariavelCocomo_ProjetoCod = ((0==A965VariavelCocomo_ProjetoCod) ? true : false);
               AV7VariavelCocomo_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vVARIAVELCOCOMO_AREATRABALHOCOD"), ",", "."));
               AV8VariavelCocomo_Sigla = cgiGet( "vVARIAVELCOCOMO_SIGLA");
               AV9VariavelCocomo_Tipo = cgiGet( "vVARIAVELCOCOMO_TIPO");
               AV16VariavelCocomo_Sequencial = (short)(context.localUtil.CToN( cgiGet( "vVARIAVELCOCOMO_SEQUENCIAL"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A992VariavelCocomo_Sequencial = (short)(context.localUtil.CToN( cgiGet( "VARIAVELCOCOMO_SEQUENCIAL"), ",", "."));
               AV13Insert_VariavelCocomo_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_VARIAVELCOCOMO_PROJETOCOD"), ",", "."));
               AV17Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "VariaveisCocomo";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A992VariavelCocomo_Sequencial), "ZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A961VariavelCocomo_AreaTrabalhoCod != Z961VariavelCocomo_AreaTrabalhoCod ) || ( StringUtil.StrCmp(A962VariavelCocomo_Sigla, Z962VariavelCocomo_Sigla) != 0 ) || ( StringUtil.StrCmp(A964VariavelCocomo_Tipo, Z964VariavelCocomo_Tipo) != 0 ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("variaveiscocomo:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("variaveiscocomo:[SecurityCheckFailed value for]"+"VariavelCocomo_Sequencial:"+context.localUtil.Format( (decimal)(A992VariavelCocomo_Sequencial), "ZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A961VariavelCocomo_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
                  A962VariavelCocomo_Sigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A962VariavelCocomo_Sigla", A962VariavelCocomo_Sigla);
                  A964VariavelCocomo_Tipo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
                  A992VariavelCocomo_Sequencial = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A992VariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A992VariavelCocomo_Sequencial), 3, 0)));
                  getEqualNoModal( ) ;
                  if ( ! (0==AV16VariavelCocomo_Sequencial) )
                  {
                     A992VariavelCocomo_Sequencial = AV16VariavelCocomo_Sequencial;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A992VariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A992VariavelCocomo_Sequencial), 3, 0)));
                  }
                  else
                  {
                     if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A992VariavelCocomo_Sequencial) && ( Gx_BScreen == 0 ) )
                     {
                        A992VariavelCocomo_Sequencial = 0;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A992VariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A992VariavelCocomo_Sequencial), 3, 0)));
                     }
                  }
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode120 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     if ( ! (0==AV16VariavelCocomo_Sequencial) )
                     {
                        A992VariavelCocomo_Sequencial = AV16VariavelCocomo_Sequencial;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A992VariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A992VariavelCocomo_Sequencial), 3, 0)));
                     }
                     else
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A992VariavelCocomo_Sequencial) && ( Gx_BScreen == 0 ) )
                        {
                           A992VariavelCocomo_Sequencial = 0;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A992VariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A992VariavelCocomo_Sequencial), 3, 0)));
                        }
                     }
                     Gx_mode = sMode120;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound120 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_2X0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "VARIAVELCOCOMO_AREATRABALHOCOD");
                        AnyError = 1;
                        GX_FocusControl = edtVariavelCocomo_AreaTrabalhoCod_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E112X2 */
                           E112X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E122X2 */
                           E122X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E122X2 */
            E122X2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2X120( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2X120( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_2X0( )
      {
         BeforeValidate2X120( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2X120( ) ;
            }
            else
            {
               CheckExtendedTable2X120( ) ;
               CloseExtendedTableCursors2X120( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption2X0( )
      {
      }

      protected void E112X2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV10WWPContext) ;
         AV11TrnContext.FromXml(AV12WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV11TrnContext.gxTpr_Transactionname, AV17Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV18GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18GXV1), 8, 0)));
            while ( AV18GXV1 <= AV11TrnContext.gxTpr_Attributes.Count )
            {
               AV14TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV11TrnContext.gxTpr_Attributes.Item(AV18GXV1));
               if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "VariavelCocomo_ProjetoCod") == 0 )
               {
                  AV13Insert_VariavelCocomo_ProjetoCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_VariavelCocomo_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_VariavelCocomo_ProjetoCod), 6, 0)));
               }
               AV18GXV1 = (int)(AV18GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18GXV1), 8, 0)));
            }
         }
         edtVariavelCocomo_AreaTrabalhoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_AreaTrabalhoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_AreaTrabalhoCod_Visible), 5, 0)));
         edtVariavelCocomo_ProjetoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_ProjetoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_ProjetoCod_Visible), 5, 0)));
         edtVariavelCocomo_Usado_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Usado_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_Usado_Visible), 5, 0)));
      }

      protected void E122X2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV11TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwvariaveiscocomo.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2X120( short GX_JID )
      {
         if ( ( GX_JID == 19 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z963VariavelCocomo_Nome = T002X3_A963VariavelCocomo_Nome[0];
               Z966VariavelCocomo_Data = T002X3_A966VariavelCocomo_Data[0];
               Z986VariavelCocomo_ExtraBaixo = T002X3_A986VariavelCocomo_ExtraBaixo[0];
               Z967VariavelCocomo_MuitoBaixo = T002X3_A967VariavelCocomo_MuitoBaixo[0];
               Z968VariavelCocomo_Baixo = T002X3_A968VariavelCocomo_Baixo[0];
               Z969VariavelCocomo_Nominal = T002X3_A969VariavelCocomo_Nominal[0];
               Z970VariavelCocomo_Alto = T002X3_A970VariavelCocomo_Alto[0];
               Z971VariavelCocomo_MuitoAlto = T002X3_A971VariavelCocomo_MuitoAlto[0];
               Z972VariavelCocomo_ExtraAlto = T002X3_A972VariavelCocomo_ExtraAlto[0];
               Z973VariavelCocomo_Usado = T002X3_A973VariavelCocomo_Usado[0];
               Z965VariavelCocomo_ProjetoCod = T002X3_A965VariavelCocomo_ProjetoCod[0];
            }
            else
            {
               Z963VariavelCocomo_Nome = A963VariavelCocomo_Nome;
               Z966VariavelCocomo_Data = A966VariavelCocomo_Data;
               Z986VariavelCocomo_ExtraBaixo = A986VariavelCocomo_ExtraBaixo;
               Z967VariavelCocomo_MuitoBaixo = A967VariavelCocomo_MuitoBaixo;
               Z968VariavelCocomo_Baixo = A968VariavelCocomo_Baixo;
               Z969VariavelCocomo_Nominal = A969VariavelCocomo_Nominal;
               Z970VariavelCocomo_Alto = A970VariavelCocomo_Alto;
               Z971VariavelCocomo_MuitoAlto = A971VariavelCocomo_MuitoAlto;
               Z972VariavelCocomo_ExtraAlto = A972VariavelCocomo_ExtraAlto;
               Z973VariavelCocomo_Usado = A973VariavelCocomo_Usado;
               Z965VariavelCocomo_ProjetoCod = A965VariavelCocomo_ProjetoCod;
            }
         }
         if ( GX_JID == -19 )
         {
            Z962VariavelCocomo_Sigla = A962VariavelCocomo_Sigla;
            Z964VariavelCocomo_Tipo = A964VariavelCocomo_Tipo;
            Z992VariavelCocomo_Sequencial = A992VariavelCocomo_Sequencial;
            Z963VariavelCocomo_Nome = A963VariavelCocomo_Nome;
            Z966VariavelCocomo_Data = A966VariavelCocomo_Data;
            Z986VariavelCocomo_ExtraBaixo = A986VariavelCocomo_ExtraBaixo;
            Z967VariavelCocomo_MuitoBaixo = A967VariavelCocomo_MuitoBaixo;
            Z968VariavelCocomo_Baixo = A968VariavelCocomo_Baixo;
            Z969VariavelCocomo_Nominal = A969VariavelCocomo_Nominal;
            Z970VariavelCocomo_Alto = A970VariavelCocomo_Alto;
            Z971VariavelCocomo_MuitoAlto = A971VariavelCocomo_MuitoAlto;
            Z972VariavelCocomo_ExtraAlto = A972VariavelCocomo_ExtraAlto;
            Z973VariavelCocomo_Usado = A973VariavelCocomo_Usado;
            Z961VariavelCocomo_AreaTrabalhoCod = A961VariavelCocomo_AreaTrabalhoCod;
            Z965VariavelCocomo_ProjetoCod = A965VariavelCocomo_ProjetoCod;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV17Pgmname = "VariaveisCocomo";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Pgmname", AV17Pgmname);
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7VariavelCocomo_AreaTrabalhoCod) )
         {
            A961VariavelCocomo_AreaTrabalhoCod = AV7VariavelCocomo_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
         }
         if ( ! (0==AV7VariavelCocomo_AreaTrabalhoCod) )
         {
            edtVariavelCocomo_AreaTrabalhoCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_AreaTrabalhoCod_Enabled), 5, 0)));
         }
         else
         {
            edtVariavelCocomo_AreaTrabalhoCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_AreaTrabalhoCod_Enabled), 5, 0)));
         }
         if ( ! (0==AV7VariavelCocomo_AreaTrabalhoCod) )
         {
            edtVariavelCocomo_AreaTrabalhoCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_AreaTrabalhoCod_Enabled), 5, 0)));
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8VariavelCocomo_Sigla)) )
         {
            A962VariavelCocomo_Sigla = AV8VariavelCocomo_Sigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A962VariavelCocomo_Sigla", A962VariavelCocomo_Sigla);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8VariavelCocomo_Sigla)) )
         {
            edtVariavelCocomo_Sigla_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_Sigla_Enabled), 5, 0)));
         }
         else
         {
            edtVariavelCocomo_Sigla_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_Sigla_Enabled), 5, 0)));
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8VariavelCocomo_Sigla)) )
         {
            edtVariavelCocomo_Sigla_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_Sigla_Enabled), 5, 0)));
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9VariavelCocomo_Tipo)) )
         {
            A964VariavelCocomo_Tipo = AV9VariavelCocomo_Tipo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9VariavelCocomo_Tipo)) )
         {
            cmbVariavelCocomo_Tipo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbVariavelCocomo_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbVariavelCocomo_Tipo.Enabled), 5, 0)));
         }
         else
         {
            cmbVariavelCocomo_Tipo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbVariavelCocomo_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbVariavelCocomo_Tipo.Enabled), 5, 0)));
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9VariavelCocomo_Tipo)) )
         {
            cmbVariavelCocomo_Tipo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbVariavelCocomo_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbVariavelCocomo_Tipo.Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_VariavelCocomo_ProjetoCod) )
         {
            edtVariavelCocomo_ProjetoCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_ProjetoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_ProjetoCod_Enabled), 5, 0)));
         }
         else
         {
            edtVariavelCocomo_ProjetoCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_ProjetoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_ProjetoCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_VariavelCocomo_ProjetoCod) )
         {
            A965VariavelCocomo_ProjetoCod = AV13Insert_VariavelCocomo_ProjetoCod;
            n965VariavelCocomo_ProjetoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A965VariavelCocomo_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A965VariavelCocomo_ProjetoCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ! (0==AV16VariavelCocomo_Sequencial) )
         {
            A992VariavelCocomo_Sequencial = AV16VariavelCocomo_Sequencial;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A992VariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A992VariavelCocomo_Sequencial), 3, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A992VariavelCocomo_Sequencial) && ( Gx_BScreen == 0 ) )
            {
               A992VariavelCocomo_Sequencial = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A992VariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A992VariavelCocomo_Sequencial), 3, 0)));
            }
         }
      }

      protected void Load2X120( )
      {
         /* Using cursor T002X6 */
         pr_default.execute(4, new Object[] {A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound120 = 1;
            A963VariavelCocomo_Nome = T002X6_A963VariavelCocomo_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A963VariavelCocomo_Nome", A963VariavelCocomo_Nome);
            A966VariavelCocomo_Data = T002X6_A966VariavelCocomo_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A966VariavelCocomo_Data", context.localUtil.Format(A966VariavelCocomo_Data, "99/99/99"));
            n966VariavelCocomo_Data = T002X6_n966VariavelCocomo_Data[0];
            A986VariavelCocomo_ExtraBaixo = T002X6_A986VariavelCocomo_ExtraBaixo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A986VariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( A986VariavelCocomo_ExtraBaixo, 12, 2)));
            n986VariavelCocomo_ExtraBaixo = T002X6_n986VariavelCocomo_ExtraBaixo[0];
            A967VariavelCocomo_MuitoBaixo = T002X6_A967VariavelCocomo_MuitoBaixo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A967VariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( A967VariavelCocomo_MuitoBaixo, 12, 2)));
            n967VariavelCocomo_MuitoBaixo = T002X6_n967VariavelCocomo_MuitoBaixo[0];
            A968VariavelCocomo_Baixo = T002X6_A968VariavelCocomo_Baixo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A968VariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( A968VariavelCocomo_Baixo, 12, 2)));
            n968VariavelCocomo_Baixo = T002X6_n968VariavelCocomo_Baixo[0];
            A969VariavelCocomo_Nominal = T002X6_A969VariavelCocomo_Nominal[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A969VariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( A969VariavelCocomo_Nominal, 12, 2)));
            n969VariavelCocomo_Nominal = T002X6_n969VariavelCocomo_Nominal[0];
            A970VariavelCocomo_Alto = T002X6_A970VariavelCocomo_Alto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A970VariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( A970VariavelCocomo_Alto, 12, 2)));
            n970VariavelCocomo_Alto = T002X6_n970VariavelCocomo_Alto[0];
            A971VariavelCocomo_MuitoAlto = T002X6_A971VariavelCocomo_MuitoAlto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A971VariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( A971VariavelCocomo_MuitoAlto, 12, 2)));
            n971VariavelCocomo_MuitoAlto = T002X6_n971VariavelCocomo_MuitoAlto[0];
            A972VariavelCocomo_ExtraAlto = T002X6_A972VariavelCocomo_ExtraAlto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A972VariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( A972VariavelCocomo_ExtraAlto, 12, 2)));
            n972VariavelCocomo_ExtraAlto = T002X6_n972VariavelCocomo_ExtraAlto[0];
            A973VariavelCocomo_Usado = T002X6_A973VariavelCocomo_Usado[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A973VariavelCocomo_Usado", StringUtil.LTrim( StringUtil.Str( A973VariavelCocomo_Usado, 12, 2)));
            n973VariavelCocomo_Usado = T002X6_n973VariavelCocomo_Usado[0];
            A965VariavelCocomo_ProjetoCod = T002X6_A965VariavelCocomo_ProjetoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A965VariavelCocomo_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A965VariavelCocomo_ProjetoCod), 6, 0)));
            n965VariavelCocomo_ProjetoCod = T002X6_n965VariavelCocomo_ProjetoCod[0];
            ZM2X120( -19) ;
         }
         pr_default.close(4);
         OnLoadActions2X120( ) ;
      }

      protected void OnLoadActions2X120( )
      {
      }

      protected void CheckExtendedTable2X120( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T002X4 */
         pr_default.execute(2, new Object[] {A961VariavelCocomo_AreaTrabalhoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Vriaveis Cocomo_Area Trabalho'.", "ForeignKeyNotFound", 1, "VARIAVELCOCOMO_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtVariavelCocomo_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T002X5 */
         pr_default.execute(3, new Object[] {n965VariavelCocomo_ProjetoCod, A965VariavelCocomo_ProjetoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A965VariavelCocomo_ProjetoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Variaveis Cocomo_Projeto'.", "ForeignKeyNotFound", 1, "VARIAVELCOCOMO_PROJETOCOD");
               AnyError = 1;
               GX_FocusControl = edtVariavelCocomo_ProjetoCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(3);
         if ( ! ( (DateTime.MinValue==A966VariavelCocomo_Data) || ( A966VariavelCocomo_Data >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "VARIAVELCOCOMO_DATA");
            AnyError = 1;
            GX_FocusControl = edtVariavelCocomo_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors2X120( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_20( int A961VariavelCocomo_AreaTrabalhoCod )
      {
         /* Using cursor T002X7 */
         pr_default.execute(5, new Object[] {A961VariavelCocomo_AreaTrabalhoCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Vriaveis Cocomo_Area Trabalho'.", "ForeignKeyNotFound", 1, "VARIAVELCOCOMO_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtVariavelCocomo_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_21( int A965VariavelCocomo_ProjetoCod )
      {
         /* Using cursor T002X8 */
         pr_default.execute(6, new Object[] {n965VariavelCocomo_ProjetoCod, A965VariavelCocomo_ProjetoCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A965VariavelCocomo_ProjetoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Variaveis Cocomo_Projeto'.", "ForeignKeyNotFound", 1, "VARIAVELCOCOMO_PROJETOCOD");
               AnyError = 1;
               GX_FocusControl = edtVariavelCocomo_ProjetoCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey2X120( )
      {
         /* Using cursor T002X9 */
         pr_default.execute(7, new Object[] {A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound120 = 1;
         }
         else
         {
            RcdFound120 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002X3 */
         pr_default.execute(1, new Object[] {A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2X120( 19) ;
            RcdFound120 = 1;
            A962VariavelCocomo_Sigla = T002X3_A962VariavelCocomo_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A962VariavelCocomo_Sigla", A962VariavelCocomo_Sigla);
            A964VariavelCocomo_Tipo = T002X3_A964VariavelCocomo_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
            A992VariavelCocomo_Sequencial = T002X3_A992VariavelCocomo_Sequencial[0];
            A963VariavelCocomo_Nome = T002X3_A963VariavelCocomo_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A963VariavelCocomo_Nome", A963VariavelCocomo_Nome);
            A966VariavelCocomo_Data = T002X3_A966VariavelCocomo_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A966VariavelCocomo_Data", context.localUtil.Format(A966VariavelCocomo_Data, "99/99/99"));
            n966VariavelCocomo_Data = T002X3_n966VariavelCocomo_Data[0];
            A986VariavelCocomo_ExtraBaixo = T002X3_A986VariavelCocomo_ExtraBaixo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A986VariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( A986VariavelCocomo_ExtraBaixo, 12, 2)));
            n986VariavelCocomo_ExtraBaixo = T002X3_n986VariavelCocomo_ExtraBaixo[0];
            A967VariavelCocomo_MuitoBaixo = T002X3_A967VariavelCocomo_MuitoBaixo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A967VariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( A967VariavelCocomo_MuitoBaixo, 12, 2)));
            n967VariavelCocomo_MuitoBaixo = T002X3_n967VariavelCocomo_MuitoBaixo[0];
            A968VariavelCocomo_Baixo = T002X3_A968VariavelCocomo_Baixo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A968VariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( A968VariavelCocomo_Baixo, 12, 2)));
            n968VariavelCocomo_Baixo = T002X3_n968VariavelCocomo_Baixo[0];
            A969VariavelCocomo_Nominal = T002X3_A969VariavelCocomo_Nominal[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A969VariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( A969VariavelCocomo_Nominal, 12, 2)));
            n969VariavelCocomo_Nominal = T002X3_n969VariavelCocomo_Nominal[0];
            A970VariavelCocomo_Alto = T002X3_A970VariavelCocomo_Alto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A970VariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( A970VariavelCocomo_Alto, 12, 2)));
            n970VariavelCocomo_Alto = T002X3_n970VariavelCocomo_Alto[0];
            A971VariavelCocomo_MuitoAlto = T002X3_A971VariavelCocomo_MuitoAlto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A971VariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( A971VariavelCocomo_MuitoAlto, 12, 2)));
            n971VariavelCocomo_MuitoAlto = T002X3_n971VariavelCocomo_MuitoAlto[0];
            A972VariavelCocomo_ExtraAlto = T002X3_A972VariavelCocomo_ExtraAlto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A972VariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( A972VariavelCocomo_ExtraAlto, 12, 2)));
            n972VariavelCocomo_ExtraAlto = T002X3_n972VariavelCocomo_ExtraAlto[0];
            A973VariavelCocomo_Usado = T002X3_A973VariavelCocomo_Usado[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A973VariavelCocomo_Usado", StringUtil.LTrim( StringUtil.Str( A973VariavelCocomo_Usado, 12, 2)));
            n973VariavelCocomo_Usado = T002X3_n973VariavelCocomo_Usado[0];
            A961VariavelCocomo_AreaTrabalhoCod = T002X3_A961VariavelCocomo_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
            A965VariavelCocomo_ProjetoCod = T002X3_A965VariavelCocomo_ProjetoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A965VariavelCocomo_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A965VariavelCocomo_ProjetoCod), 6, 0)));
            n965VariavelCocomo_ProjetoCod = T002X3_n965VariavelCocomo_ProjetoCod[0];
            Z961VariavelCocomo_AreaTrabalhoCod = A961VariavelCocomo_AreaTrabalhoCod;
            Z962VariavelCocomo_Sigla = A962VariavelCocomo_Sigla;
            Z964VariavelCocomo_Tipo = A964VariavelCocomo_Tipo;
            Z992VariavelCocomo_Sequencial = A992VariavelCocomo_Sequencial;
            sMode120 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2X120( ) ;
            if ( AnyError == 1 )
            {
               RcdFound120 = 0;
               InitializeNonKey2X120( ) ;
            }
            Gx_mode = sMode120;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound120 = 0;
            InitializeNonKey2X120( ) ;
            sMode120 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode120;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2X120( ) ;
         if ( RcdFound120 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound120 = 0;
         /* Using cursor T002X10 */
         pr_default.execute(8, new Object[] {A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T002X10_A961VariavelCocomo_AreaTrabalhoCod[0] < A961VariavelCocomo_AreaTrabalhoCod ) || ( T002X10_A961VariavelCocomo_AreaTrabalhoCod[0] == A961VariavelCocomo_AreaTrabalhoCod ) && ( StringUtil.StrCmp(T002X10_A962VariavelCocomo_Sigla[0], A962VariavelCocomo_Sigla) < 0 ) || ( StringUtil.StrCmp(T002X10_A962VariavelCocomo_Sigla[0], A962VariavelCocomo_Sigla) == 0 ) && ( T002X10_A961VariavelCocomo_AreaTrabalhoCod[0] == A961VariavelCocomo_AreaTrabalhoCod ) && ( StringUtil.StrCmp(T002X10_A964VariavelCocomo_Tipo[0], A964VariavelCocomo_Tipo) < 0 ) || ( StringUtil.StrCmp(T002X10_A964VariavelCocomo_Tipo[0], A964VariavelCocomo_Tipo) == 0 ) && ( StringUtil.StrCmp(T002X10_A962VariavelCocomo_Sigla[0], A962VariavelCocomo_Sigla) == 0 ) && ( T002X10_A961VariavelCocomo_AreaTrabalhoCod[0] == A961VariavelCocomo_AreaTrabalhoCod ) && ( T002X10_A992VariavelCocomo_Sequencial[0] < A992VariavelCocomo_Sequencial ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T002X10_A961VariavelCocomo_AreaTrabalhoCod[0] > A961VariavelCocomo_AreaTrabalhoCod ) || ( T002X10_A961VariavelCocomo_AreaTrabalhoCod[0] == A961VariavelCocomo_AreaTrabalhoCod ) && ( StringUtil.StrCmp(T002X10_A962VariavelCocomo_Sigla[0], A962VariavelCocomo_Sigla) > 0 ) || ( StringUtil.StrCmp(T002X10_A962VariavelCocomo_Sigla[0], A962VariavelCocomo_Sigla) == 0 ) && ( T002X10_A961VariavelCocomo_AreaTrabalhoCod[0] == A961VariavelCocomo_AreaTrabalhoCod ) && ( StringUtil.StrCmp(T002X10_A964VariavelCocomo_Tipo[0], A964VariavelCocomo_Tipo) > 0 ) || ( StringUtil.StrCmp(T002X10_A964VariavelCocomo_Tipo[0], A964VariavelCocomo_Tipo) == 0 ) && ( StringUtil.StrCmp(T002X10_A962VariavelCocomo_Sigla[0], A962VariavelCocomo_Sigla) == 0 ) && ( T002X10_A961VariavelCocomo_AreaTrabalhoCod[0] == A961VariavelCocomo_AreaTrabalhoCod ) && ( T002X10_A992VariavelCocomo_Sequencial[0] > A992VariavelCocomo_Sequencial ) ) )
            {
               A961VariavelCocomo_AreaTrabalhoCod = T002X10_A961VariavelCocomo_AreaTrabalhoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
               A962VariavelCocomo_Sigla = T002X10_A962VariavelCocomo_Sigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A962VariavelCocomo_Sigla", A962VariavelCocomo_Sigla);
               A964VariavelCocomo_Tipo = T002X10_A964VariavelCocomo_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
               A992VariavelCocomo_Sequencial = T002X10_A992VariavelCocomo_Sequencial[0];
               RcdFound120 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound120 = 0;
         /* Using cursor T002X11 */
         pr_default.execute(9, new Object[] {A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T002X11_A961VariavelCocomo_AreaTrabalhoCod[0] > A961VariavelCocomo_AreaTrabalhoCod ) || ( T002X11_A961VariavelCocomo_AreaTrabalhoCod[0] == A961VariavelCocomo_AreaTrabalhoCod ) && ( StringUtil.StrCmp(T002X11_A962VariavelCocomo_Sigla[0], A962VariavelCocomo_Sigla) > 0 ) || ( StringUtil.StrCmp(T002X11_A962VariavelCocomo_Sigla[0], A962VariavelCocomo_Sigla) == 0 ) && ( T002X11_A961VariavelCocomo_AreaTrabalhoCod[0] == A961VariavelCocomo_AreaTrabalhoCod ) && ( StringUtil.StrCmp(T002X11_A964VariavelCocomo_Tipo[0], A964VariavelCocomo_Tipo) > 0 ) || ( StringUtil.StrCmp(T002X11_A964VariavelCocomo_Tipo[0], A964VariavelCocomo_Tipo) == 0 ) && ( StringUtil.StrCmp(T002X11_A962VariavelCocomo_Sigla[0], A962VariavelCocomo_Sigla) == 0 ) && ( T002X11_A961VariavelCocomo_AreaTrabalhoCod[0] == A961VariavelCocomo_AreaTrabalhoCod ) && ( T002X11_A992VariavelCocomo_Sequencial[0] > A992VariavelCocomo_Sequencial ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T002X11_A961VariavelCocomo_AreaTrabalhoCod[0] < A961VariavelCocomo_AreaTrabalhoCod ) || ( T002X11_A961VariavelCocomo_AreaTrabalhoCod[0] == A961VariavelCocomo_AreaTrabalhoCod ) && ( StringUtil.StrCmp(T002X11_A962VariavelCocomo_Sigla[0], A962VariavelCocomo_Sigla) < 0 ) || ( StringUtil.StrCmp(T002X11_A962VariavelCocomo_Sigla[0], A962VariavelCocomo_Sigla) == 0 ) && ( T002X11_A961VariavelCocomo_AreaTrabalhoCod[0] == A961VariavelCocomo_AreaTrabalhoCod ) && ( StringUtil.StrCmp(T002X11_A964VariavelCocomo_Tipo[0], A964VariavelCocomo_Tipo) < 0 ) || ( StringUtil.StrCmp(T002X11_A964VariavelCocomo_Tipo[0], A964VariavelCocomo_Tipo) == 0 ) && ( StringUtil.StrCmp(T002X11_A962VariavelCocomo_Sigla[0], A962VariavelCocomo_Sigla) == 0 ) && ( T002X11_A961VariavelCocomo_AreaTrabalhoCod[0] == A961VariavelCocomo_AreaTrabalhoCod ) && ( T002X11_A992VariavelCocomo_Sequencial[0] < A992VariavelCocomo_Sequencial ) ) )
            {
               A961VariavelCocomo_AreaTrabalhoCod = T002X11_A961VariavelCocomo_AreaTrabalhoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
               A962VariavelCocomo_Sigla = T002X11_A962VariavelCocomo_Sigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A962VariavelCocomo_Sigla", A962VariavelCocomo_Sigla);
               A964VariavelCocomo_Tipo = T002X11_A964VariavelCocomo_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
               A992VariavelCocomo_Sequencial = T002X11_A992VariavelCocomo_Sequencial[0];
               RcdFound120 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2X120( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtVariavelCocomo_Sigla_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2X120( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound120 == 1 )
            {
               if ( ( A961VariavelCocomo_AreaTrabalhoCod != Z961VariavelCocomo_AreaTrabalhoCod ) || ( StringUtil.StrCmp(A962VariavelCocomo_Sigla, Z962VariavelCocomo_Sigla) != 0 ) || ( StringUtil.StrCmp(A964VariavelCocomo_Tipo, Z964VariavelCocomo_Tipo) != 0 ) || ( A992VariavelCocomo_Sequencial != Z992VariavelCocomo_Sequencial ) )
               {
                  A961VariavelCocomo_AreaTrabalhoCod = Z961VariavelCocomo_AreaTrabalhoCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
                  A962VariavelCocomo_Sigla = Z962VariavelCocomo_Sigla;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A962VariavelCocomo_Sigla", A962VariavelCocomo_Sigla);
                  A964VariavelCocomo_Tipo = Z964VariavelCocomo_Tipo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
                  A992VariavelCocomo_Sequencial = Z992VariavelCocomo_Sequencial;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A992VariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A992VariavelCocomo_Sequencial), 3, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "VARIAVELCOCOMO_AREATRABALHOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtVariavelCocomo_AreaTrabalhoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtVariavelCocomo_Sigla_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2X120( ) ;
                  GX_FocusControl = edtVariavelCocomo_Sigla_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A961VariavelCocomo_AreaTrabalhoCod != Z961VariavelCocomo_AreaTrabalhoCod ) || ( StringUtil.StrCmp(A962VariavelCocomo_Sigla, Z962VariavelCocomo_Sigla) != 0 ) || ( StringUtil.StrCmp(A964VariavelCocomo_Tipo, Z964VariavelCocomo_Tipo) != 0 ) || ( A992VariavelCocomo_Sequencial != Z992VariavelCocomo_Sequencial ) )
               {
                  /* Insert record */
                  GX_FocusControl = edtVariavelCocomo_Sigla_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2X120( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "VARIAVELCOCOMO_AREATRABALHOCOD");
                     AnyError = 1;
                     GX_FocusControl = edtVariavelCocomo_AreaTrabalhoCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtVariavelCocomo_Sigla_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2X120( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( ( A961VariavelCocomo_AreaTrabalhoCod != Z961VariavelCocomo_AreaTrabalhoCod ) || ( StringUtil.StrCmp(A962VariavelCocomo_Sigla, Z962VariavelCocomo_Sigla) != 0 ) || ( StringUtil.StrCmp(A964VariavelCocomo_Tipo, Z964VariavelCocomo_Tipo) != 0 ) || ( A992VariavelCocomo_Sequencial != Z992VariavelCocomo_Sequencial ) )
         {
            A961VariavelCocomo_AreaTrabalhoCod = Z961VariavelCocomo_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
            A962VariavelCocomo_Sigla = Z962VariavelCocomo_Sigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A962VariavelCocomo_Sigla", A962VariavelCocomo_Sigla);
            A964VariavelCocomo_Tipo = Z964VariavelCocomo_Tipo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
            A992VariavelCocomo_Sequencial = Z992VariavelCocomo_Sequencial;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A992VariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A992VariavelCocomo_Sequencial), 3, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "VARIAVELCOCOMO_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtVariavelCocomo_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtVariavelCocomo_Sigla_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2X120( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002X2 */
            pr_default.execute(0, new Object[] {A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"VariaveisCocomo"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z963VariavelCocomo_Nome, T002X2_A963VariavelCocomo_Nome[0]) != 0 ) || ( Z966VariavelCocomo_Data != T002X2_A966VariavelCocomo_Data[0] ) || ( Z986VariavelCocomo_ExtraBaixo != T002X2_A986VariavelCocomo_ExtraBaixo[0] ) || ( Z967VariavelCocomo_MuitoBaixo != T002X2_A967VariavelCocomo_MuitoBaixo[0] ) || ( Z968VariavelCocomo_Baixo != T002X2_A968VariavelCocomo_Baixo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z969VariavelCocomo_Nominal != T002X2_A969VariavelCocomo_Nominal[0] ) || ( Z970VariavelCocomo_Alto != T002X2_A970VariavelCocomo_Alto[0] ) || ( Z971VariavelCocomo_MuitoAlto != T002X2_A971VariavelCocomo_MuitoAlto[0] ) || ( Z972VariavelCocomo_ExtraAlto != T002X2_A972VariavelCocomo_ExtraAlto[0] ) || ( Z973VariavelCocomo_Usado != T002X2_A973VariavelCocomo_Usado[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z965VariavelCocomo_ProjetoCod != T002X2_A965VariavelCocomo_ProjetoCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z963VariavelCocomo_Nome, T002X2_A963VariavelCocomo_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("variaveiscocomo:[seudo value changed for attri]"+"VariavelCocomo_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z963VariavelCocomo_Nome);
                  GXUtil.WriteLogRaw("Current: ",T002X2_A963VariavelCocomo_Nome[0]);
               }
               if ( Z966VariavelCocomo_Data != T002X2_A966VariavelCocomo_Data[0] )
               {
                  GXUtil.WriteLog("variaveiscocomo:[seudo value changed for attri]"+"VariavelCocomo_Data");
                  GXUtil.WriteLogRaw("Old: ",Z966VariavelCocomo_Data);
                  GXUtil.WriteLogRaw("Current: ",T002X2_A966VariavelCocomo_Data[0]);
               }
               if ( Z986VariavelCocomo_ExtraBaixo != T002X2_A986VariavelCocomo_ExtraBaixo[0] )
               {
                  GXUtil.WriteLog("variaveiscocomo:[seudo value changed for attri]"+"VariavelCocomo_ExtraBaixo");
                  GXUtil.WriteLogRaw("Old: ",Z986VariavelCocomo_ExtraBaixo);
                  GXUtil.WriteLogRaw("Current: ",T002X2_A986VariavelCocomo_ExtraBaixo[0]);
               }
               if ( Z967VariavelCocomo_MuitoBaixo != T002X2_A967VariavelCocomo_MuitoBaixo[0] )
               {
                  GXUtil.WriteLog("variaveiscocomo:[seudo value changed for attri]"+"VariavelCocomo_MuitoBaixo");
                  GXUtil.WriteLogRaw("Old: ",Z967VariavelCocomo_MuitoBaixo);
                  GXUtil.WriteLogRaw("Current: ",T002X2_A967VariavelCocomo_MuitoBaixo[0]);
               }
               if ( Z968VariavelCocomo_Baixo != T002X2_A968VariavelCocomo_Baixo[0] )
               {
                  GXUtil.WriteLog("variaveiscocomo:[seudo value changed for attri]"+"VariavelCocomo_Baixo");
                  GXUtil.WriteLogRaw("Old: ",Z968VariavelCocomo_Baixo);
                  GXUtil.WriteLogRaw("Current: ",T002X2_A968VariavelCocomo_Baixo[0]);
               }
               if ( Z969VariavelCocomo_Nominal != T002X2_A969VariavelCocomo_Nominal[0] )
               {
                  GXUtil.WriteLog("variaveiscocomo:[seudo value changed for attri]"+"VariavelCocomo_Nominal");
                  GXUtil.WriteLogRaw("Old: ",Z969VariavelCocomo_Nominal);
                  GXUtil.WriteLogRaw("Current: ",T002X2_A969VariavelCocomo_Nominal[0]);
               }
               if ( Z970VariavelCocomo_Alto != T002X2_A970VariavelCocomo_Alto[0] )
               {
                  GXUtil.WriteLog("variaveiscocomo:[seudo value changed for attri]"+"VariavelCocomo_Alto");
                  GXUtil.WriteLogRaw("Old: ",Z970VariavelCocomo_Alto);
                  GXUtil.WriteLogRaw("Current: ",T002X2_A970VariavelCocomo_Alto[0]);
               }
               if ( Z971VariavelCocomo_MuitoAlto != T002X2_A971VariavelCocomo_MuitoAlto[0] )
               {
                  GXUtil.WriteLog("variaveiscocomo:[seudo value changed for attri]"+"VariavelCocomo_MuitoAlto");
                  GXUtil.WriteLogRaw("Old: ",Z971VariavelCocomo_MuitoAlto);
                  GXUtil.WriteLogRaw("Current: ",T002X2_A971VariavelCocomo_MuitoAlto[0]);
               }
               if ( Z972VariavelCocomo_ExtraAlto != T002X2_A972VariavelCocomo_ExtraAlto[0] )
               {
                  GXUtil.WriteLog("variaveiscocomo:[seudo value changed for attri]"+"VariavelCocomo_ExtraAlto");
                  GXUtil.WriteLogRaw("Old: ",Z972VariavelCocomo_ExtraAlto);
                  GXUtil.WriteLogRaw("Current: ",T002X2_A972VariavelCocomo_ExtraAlto[0]);
               }
               if ( Z973VariavelCocomo_Usado != T002X2_A973VariavelCocomo_Usado[0] )
               {
                  GXUtil.WriteLog("variaveiscocomo:[seudo value changed for attri]"+"VariavelCocomo_Usado");
                  GXUtil.WriteLogRaw("Old: ",Z973VariavelCocomo_Usado);
                  GXUtil.WriteLogRaw("Current: ",T002X2_A973VariavelCocomo_Usado[0]);
               }
               if ( Z965VariavelCocomo_ProjetoCod != T002X2_A965VariavelCocomo_ProjetoCod[0] )
               {
                  GXUtil.WriteLog("variaveiscocomo:[seudo value changed for attri]"+"VariavelCocomo_ProjetoCod");
                  GXUtil.WriteLogRaw("Old: ",Z965VariavelCocomo_ProjetoCod);
                  GXUtil.WriteLogRaw("Current: ",T002X2_A965VariavelCocomo_ProjetoCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"VariaveisCocomo"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2X120( )
      {
         BeforeValidate2X120( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2X120( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2X120( 0) ;
            CheckOptimisticConcurrency2X120( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2X120( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2X120( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002X12 */
                     pr_default.execute(10, new Object[] {A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial, A963VariavelCocomo_Nome, n966VariavelCocomo_Data, A966VariavelCocomo_Data, n986VariavelCocomo_ExtraBaixo, A986VariavelCocomo_ExtraBaixo, n967VariavelCocomo_MuitoBaixo, A967VariavelCocomo_MuitoBaixo, n968VariavelCocomo_Baixo, A968VariavelCocomo_Baixo, n969VariavelCocomo_Nominal, A969VariavelCocomo_Nominal, n970VariavelCocomo_Alto, A970VariavelCocomo_Alto, n971VariavelCocomo_MuitoAlto, A971VariavelCocomo_MuitoAlto, n972VariavelCocomo_ExtraAlto, A972VariavelCocomo_ExtraAlto, n973VariavelCocomo_Usado, A973VariavelCocomo_Usado, A961VariavelCocomo_AreaTrabalhoCod, n965VariavelCocomo_ProjetoCod, A965VariavelCocomo_ProjetoCod});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("VariaveisCocomo") ;
                     if ( (pr_default.getStatus(10) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2X0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2X120( ) ;
            }
            EndLevel2X120( ) ;
         }
         CloseExtendedTableCursors2X120( ) ;
      }

      protected void Update2X120( )
      {
         BeforeValidate2X120( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2X120( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2X120( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2X120( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2X120( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002X13 */
                     pr_default.execute(11, new Object[] {A963VariavelCocomo_Nome, n966VariavelCocomo_Data, A966VariavelCocomo_Data, n986VariavelCocomo_ExtraBaixo, A986VariavelCocomo_ExtraBaixo, n967VariavelCocomo_MuitoBaixo, A967VariavelCocomo_MuitoBaixo, n968VariavelCocomo_Baixo, A968VariavelCocomo_Baixo, n969VariavelCocomo_Nominal, A969VariavelCocomo_Nominal, n970VariavelCocomo_Alto, A970VariavelCocomo_Alto, n971VariavelCocomo_MuitoAlto, A971VariavelCocomo_MuitoAlto, n972VariavelCocomo_ExtraAlto, A972VariavelCocomo_ExtraAlto, n973VariavelCocomo_Usado, A973VariavelCocomo_Usado, n965VariavelCocomo_ProjetoCod, A965VariavelCocomo_ProjetoCod, A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("VariaveisCocomo") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"VariaveisCocomo"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2X120( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2X120( ) ;
         }
         CloseExtendedTableCursors2X120( ) ;
      }

      protected void DeferredUpdate2X120( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2X120( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2X120( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2X120( ) ;
            AfterConfirm2X120( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2X120( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002X14 */
                  pr_default.execute(12, new Object[] {A961VariavelCocomo_AreaTrabalhoCod, A962VariavelCocomo_Sigla, A964VariavelCocomo_Tipo, A992VariavelCocomo_Sequencial});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("VariaveisCocomo") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode120 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2X120( ) ;
         Gx_mode = sMode120;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2X120( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel2X120( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2X120( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "VariaveisCocomo");
            if ( AnyError == 0 )
            {
               ConfirmValues2X0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "VariaveisCocomo");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2X120( )
      {
         /* Scan By routine */
         /* Using cursor T002X15 */
         pr_default.execute(13);
         RcdFound120 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound120 = 1;
            A961VariavelCocomo_AreaTrabalhoCod = T002X15_A961VariavelCocomo_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
            A962VariavelCocomo_Sigla = T002X15_A962VariavelCocomo_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A962VariavelCocomo_Sigla", A962VariavelCocomo_Sigla);
            A964VariavelCocomo_Tipo = T002X15_A964VariavelCocomo_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
            A992VariavelCocomo_Sequencial = T002X15_A992VariavelCocomo_Sequencial[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2X120( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound120 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound120 = 1;
            A961VariavelCocomo_AreaTrabalhoCod = T002X15_A961VariavelCocomo_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
            A962VariavelCocomo_Sigla = T002X15_A962VariavelCocomo_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A962VariavelCocomo_Sigla", A962VariavelCocomo_Sigla);
            A964VariavelCocomo_Tipo = T002X15_A964VariavelCocomo_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
            A992VariavelCocomo_Sequencial = T002X15_A992VariavelCocomo_Sequencial[0];
         }
      }

      protected void ScanEnd2X120( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm2X120( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2X120( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2X120( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2X120( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2X120( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2X120( )
      {
         /* Before Validate Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A961VariavelCocomo_AreaTrabalhoCod = AV10WWPContext.gxTpr_Areatrabalho_codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
         }
      }

      protected void DisableAttributes2X120( )
      {
         edtVariavelCocomo_Sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_Sigla_Enabled), 5, 0)));
         edtVariavelCocomo_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_Nome_Enabled), 5, 0)));
         cmbVariavelCocomo_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbVariavelCocomo_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbVariavelCocomo_Tipo.Enabled), 5, 0)));
         edtVariavelCocomo_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_Data_Enabled), 5, 0)));
         edtVariavelCocomo_ExtraBaixo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_ExtraBaixo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_ExtraBaixo_Enabled), 5, 0)));
         edtVariavelCocomo_MuitoBaixo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_MuitoBaixo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_MuitoBaixo_Enabled), 5, 0)));
         edtVariavelCocomo_Baixo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Baixo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_Baixo_Enabled), 5, 0)));
         edtVariavelCocomo_Nominal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Nominal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_Nominal_Enabled), 5, 0)));
         edtVariavelCocomo_Alto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Alto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_Alto_Enabled), 5, 0)));
         edtVariavelCocomo_MuitoAlto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_MuitoAlto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_MuitoAlto_Enabled), 5, 0)));
         edtVariavelCocomo_ExtraAlto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_ExtraAlto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_ExtraAlto_Enabled), 5, 0)));
         edtVariavelCocomo_AreaTrabalhoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_AreaTrabalhoCod_Enabled), 5, 0)));
         edtVariavelCocomo_ProjetoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_ProjetoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_ProjetoCod_Enabled), 5, 0)));
         edtVariavelCocomo_Usado_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtVariavelCocomo_Usado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtVariavelCocomo_Usado_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2X0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311726425");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("variaveiscocomo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7VariavelCocomo_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(AV8VariavelCocomo_Sigla)) + "," + UrlEncode(StringUtil.RTrim(AV9VariavelCocomo_Tipo)) + "," + UrlEncode("" +AV16VariavelCocomo_Sequencial)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z961VariavelCocomo_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z962VariavelCocomo_Sigla", StringUtil.RTrim( Z962VariavelCocomo_Sigla));
         GxWebStd.gx_hidden_field( context, "Z964VariavelCocomo_Tipo", StringUtil.RTrim( Z964VariavelCocomo_Tipo));
         GxWebStd.gx_hidden_field( context, "Z992VariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z992VariavelCocomo_Sequencial), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z963VariavelCocomo_Nome", StringUtil.RTrim( Z963VariavelCocomo_Nome));
         GxWebStd.gx_hidden_field( context, "Z966VariavelCocomo_Data", context.localUtil.DToC( Z966VariavelCocomo_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z986VariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.NToC( Z986VariavelCocomo_ExtraBaixo, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z967VariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.NToC( Z967VariavelCocomo_MuitoBaixo, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z968VariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.NToC( Z968VariavelCocomo_Baixo, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z969VariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.NToC( Z969VariavelCocomo_Nominal, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z970VariavelCocomo_Alto", StringUtil.LTrim( StringUtil.NToC( Z970VariavelCocomo_Alto, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z971VariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.NToC( Z971VariavelCocomo_MuitoAlto, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z972VariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.NToC( Z972VariavelCocomo_ExtraAlto, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z973VariavelCocomo_Usado", StringUtil.LTrim( StringUtil.NToC( Z973VariavelCocomo_Usado, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z965VariavelCocomo_ProjetoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z965VariavelCocomo_ProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N965VariavelCocomo_ProjetoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A965VariavelCocomo_ProjetoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV11TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV11TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vVARIAVELCOCOMO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7VariavelCocomo_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vVARIAVELCOCOMO_SIGLA", StringUtil.RTrim( AV8VariavelCocomo_Sigla));
         GxWebStd.gx_hidden_field( context, "vVARIAVELCOCOMO_TIPO", StringUtil.RTrim( AV9VariavelCocomo_Tipo));
         GxWebStd.gx_hidden_field( context, "vVARIAVELCOCOMO_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16VariavelCocomo_Sequencial), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "VARIAVELCOCOMO_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A992VariavelCocomo_Sequencial), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_VARIAVELCOCOMO_PROJETOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_VariavelCocomo_ProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV17Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vVARIAVELCOCOMO_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7VariavelCocomo_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vVARIAVELCOCOMO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV8VariavelCocomo_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vVARIAVELCOCOMO_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV9VariavelCocomo_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vVARIAVELCOCOMO_SEQUENCIAL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16VariavelCocomo_Sequencial), "ZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "VariaveisCocomo";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A992VariavelCocomo_Sequencial), "ZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("variaveiscocomo:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("variaveiscocomo:[SendSecurityCheck value for]"+"VariavelCocomo_Sequencial:"+context.localUtil.Format( (decimal)(A992VariavelCocomo_Sequencial), "ZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("variaveiscocomo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7VariavelCocomo_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(AV8VariavelCocomo_Sigla)) + "," + UrlEncode(StringUtil.RTrim(AV9VariavelCocomo_Tipo)) + "," + UrlEncode("" +AV16VariavelCocomo_Sequencial) ;
      }

      public override String GetPgmname( )
      {
         return "VariaveisCocomo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Vari�vel Cocomo" ;
      }

      protected void InitializeNonKey2X120( )
      {
         A965VariavelCocomo_ProjetoCod = 0;
         n965VariavelCocomo_ProjetoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A965VariavelCocomo_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A965VariavelCocomo_ProjetoCod), 6, 0)));
         n965VariavelCocomo_ProjetoCod = ((0==A965VariavelCocomo_ProjetoCod) ? true : false);
         A963VariavelCocomo_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A963VariavelCocomo_Nome", A963VariavelCocomo_Nome);
         A966VariavelCocomo_Data = DateTime.MinValue;
         n966VariavelCocomo_Data = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A966VariavelCocomo_Data", context.localUtil.Format(A966VariavelCocomo_Data, "99/99/99"));
         n966VariavelCocomo_Data = ((DateTime.MinValue==A966VariavelCocomo_Data) ? true : false);
         A986VariavelCocomo_ExtraBaixo = 0;
         n986VariavelCocomo_ExtraBaixo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A986VariavelCocomo_ExtraBaixo", StringUtil.LTrim( StringUtil.Str( A986VariavelCocomo_ExtraBaixo, 12, 2)));
         n986VariavelCocomo_ExtraBaixo = ((Convert.ToDecimal(0)==A986VariavelCocomo_ExtraBaixo) ? true : false);
         A967VariavelCocomo_MuitoBaixo = 0;
         n967VariavelCocomo_MuitoBaixo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A967VariavelCocomo_MuitoBaixo", StringUtil.LTrim( StringUtil.Str( A967VariavelCocomo_MuitoBaixo, 12, 2)));
         n967VariavelCocomo_MuitoBaixo = ((Convert.ToDecimal(0)==A967VariavelCocomo_MuitoBaixo) ? true : false);
         A968VariavelCocomo_Baixo = 0;
         n968VariavelCocomo_Baixo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A968VariavelCocomo_Baixo", StringUtil.LTrim( StringUtil.Str( A968VariavelCocomo_Baixo, 12, 2)));
         n968VariavelCocomo_Baixo = ((Convert.ToDecimal(0)==A968VariavelCocomo_Baixo) ? true : false);
         A969VariavelCocomo_Nominal = 0;
         n969VariavelCocomo_Nominal = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A969VariavelCocomo_Nominal", StringUtil.LTrim( StringUtil.Str( A969VariavelCocomo_Nominal, 12, 2)));
         n969VariavelCocomo_Nominal = ((Convert.ToDecimal(0)==A969VariavelCocomo_Nominal) ? true : false);
         A970VariavelCocomo_Alto = 0;
         n970VariavelCocomo_Alto = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A970VariavelCocomo_Alto", StringUtil.LTrim( StringUtil.Str( A970VariavelCocomo_Alto, 12, 2)));
         n970VariavelCocomo_Alto = ((Convert.ToDecimal(0)==A970VariavelCocomo_Alto) ? true : false);
         A971VariavelCocomo_MuitoAlto = 0;
         n971VariavelCocomo_MuitoAlto = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A971VariavelCocomo_MuitoAlto", StringUtil.LTrim( StringUtil.Str( A971VariavelCocomo_MuitoAlto, 12, 2)));
         n971VariavelCocomo_MuitoAlto = ((Convert.ToDecimal(0)==A971VariavelCocomo_MuitoAlto) ? true : false);
         A972VariavelCocomo_ExtraAlto = 0;
         n972VariavelCocomo_ExtraAlto = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A972VariavelCocomo_ExtraAlto", StringUtil.LTrim( StringUtil.Str( A972VariavelCocomo_ExtraAlto, 12, 2)));
         n972VariavelCocomo_ExtraAlto = ((Convert.ToDecimal(0)==A972VariavelCocomo_ExtraAlto) ? true : false);
         A973VariavelCocomo_Usado = 0;
         n973VariavelCocomo_Usado = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A973VariavelCocomo_Usado", StringUtil.LTrim( StringUtil.Str( A973VariavelCocomo_Usado, 12, 2)));
         n973VariavelCocomo_Usado = ((Convert.ToDecimal(0)==A973VariavelCocomo_Usado) ? true : false);
         Z963VariavelCocomo_Nome = "";
         Z966VariavelCocomo_Data = DateTime.MinValue;
         Z986VariavelCocomo_ExtraBaixo = 0;
         Z967VariavelCocomo_MuitoBaixo = 0;
         Z968VariavelCocomo_Baixo = 0;
         Z969VariavelCocomo_Nominal = 0;
         Z970VariavelCocomo_Alto = 0;
         Z971VariavelCocomo_MuitoAlto = 0;
         Z972VariavelCocomo_ExtraAlto = 0;
         Z973VariavelCocomo_Usado = 0;
         Z965VariavelCocomo_ProjetoCod = 0;
      }

      protected void InitAll2X120( )
      {
         A961VariavelCocomo_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A961VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A961VariavelCocomo_AreaTrabalhoCod), 6, 0)));
         A962VariavelCocomo_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A962VariavelCocomo_Sigla", A962VariavelCocomo_Sigla);
         A964VariavelCocomo_Tipo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A964VariavelCocomo_Tipo", A964VariavelCocomo_Tipo);
         A992VariavelCocomo_Sequencial = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A992VariavelCocomo_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A992VariavelCocomo_Sequencial), 3, 0)));
         InitializeNonKey2X120( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311726449");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("variaveiscocomo.js", "?2020311726449");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockvariavelcocomo_sigla_Internalname = "TEXTBLOCKVARIAVELCOCOMO_SIGLA";
         edtVariavelCocomo_Sigla_Internalname = "VARIAVELCOCOMO_SIGLA";
         lblTextblockvariavelcocomo_nome_Internalname = "TEXTBLOCKVARIAVELCOCOMO_NOME";
         edtVariavelCocomo_Nome_Internalname = "VARIAVELCOCOMO_NOME";
         lblTextblockvariavelcocomo_tipo_Internalname = "TEXTBLOCKVARIAVELCOCOMO_TIPO";
         cmbVariavelCocomo_Tipo_Internalname = "VARIAVELCOCOMO_TIPO";
         lblTextblockvariavelcocomo_data_Internalname = "TEXTBLOCKVARIAVELCOCOMO_DATA";
         edtVariavelCocomo_Data_Internalname = "VARIAVELCOCOMO_DATA";
         lblTextblockvariavelcocomo_extrabaixo_Internalname = "TEXTBLOCKVARIAVELCOCOMO_EXTRABAIXO";
         edtVariavelCocomo_ExtraBaixo_Internalname = "VARIAVELCOCOMO_EXTRABAIXO";
         lblTextblockvariavelcocomo_muitobaixo_Internalname = "TEXTBLOCKVARIAVELCOCOMO_MUITOBAIXO";
         edtVariavelCocomo_MuitoBaixo_Internalname = "VARIAVELCOCOMO_MUITOBAIXO";
         lblTextblockvariavelcocomo_baixo_Internalname = "TEXTBLOCKVARIAVELCOCOMO_BAIXO";
         edtVariavelCocomo_Baixo_Internalname = "VARIAVELCOCOMO_BAIXO";
         lblTextblockvariavelcocomo_nominal_Internalname = "TEXTBLOCKVARIAVELCOCOMO_NOMINAL";
         edtVariavelCocomo_Nominal_Internalname = "VARIAVELCOCOMO_NOMINAL";
         lblTextblockvariavelcocomo_alto_Internalname = "TEXTBLOCKVARIAVELCOCOMO_ALTO";
         edtVariavelCocomo_Alto_Internalname = "VARIAVELCOCOMO_ALTO";
         lblTextblockvariavelcocomo_muitoalto_Internalname = "TEXTBLOCKVARIAVELCOCOMO_MUITOALTO";
         edtVariavelCocomo_MuitoAlto_Internalname = "VARIAVELCOCOMO_MUITOALTO";
         lblTextblockvariavelcocomo_extraalto_Internalname = "TEXTBLOCKVARIAVELCOCOMO_EXTRAALTO";
         edtVariavelCocomo_ExtraAlto_Internalname = "VARIAVELCOCOMO_EXTRAALTO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtVariavelCocomo_AreaTrabalhoCod_Internalname = "VARIAVELCOCOMO_AREATRABALHOCOD";
         edtVariavelCocomo_ProjetoCod_Internalname = "VARIAVELCOCOMO_PROJETOCOD";
         edtVariavelCocomo_Usado_Internalname = "VARIAVELCOCOMO_USADO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Vari�vel Cocomo";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Vari�vel Cocomo";
         edtVariavelCocomo_ExtraAlto_Jsonclick = "";
         edtVariavelCocomo_ExtraAlto_Enabled = 1;
         edtVariavelCocomo_MuitoAlto_Jsonclick = "";
         edtVariavelCocomo_MuitoAlto_Enabled = 1;
         edtVariavelCocomo_Alto_Jsonclick = "";
         edtVariavelCocomo_Alto_Enabled = 1;
         edtVariavelCocomo_Nominal_Jsonclick = "";
         edtVariavelCocomo_Nominal_Enabled = 1;
         edtVariavelCocomo_Baixo_Jsonclick = "";
         edtVariavelCocomo_Baixo_Enabled = 1;
         edtVariavelCocomo_MuitoBaixo_Jsonclick = "";
         edtVariavelCocomo_MuitoBaixo_Enabled = 1;
         edtVariavelCocomo_ExtraBaixo_Jsonclick = "";
         edtVariavelCocomo_ExtraBaixo_Enabled = 1;
         edtVariavelCocomo_Data_Jsonclick = "";
         edtVariavelCocomo_Data_Enabled = 1;
         cmbVariavelCocomo_Tipo_Jsonclick = "";
         cmbVariavelCocomo_Tipo.Enabled = 1;
         edtVariavelCocomo_Nome_Jsonclick = "";
         edtVariavelCocomo_Nome_Enabled = 1;
         edtVariavelCocomo_Sigla_Jsonclick = "";
         edtVariavelCocomo_Sigla_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtVariavelCocomo_Usado_Jsonclick = "";
         edtVariavelCocomo_Usado_Enabled = 1;
         edtVariavelCocomo_Usado_Visible = 1;
         edtVariavelCocomo_ProjetoCod_Jsonclick = "";
         edtVariavelCocomo_ProjetoCod_Enabled = 1;
         edtVariavelCocomo_ProjetoCod_Visible = 1;
         edtVariavelCocomo_AreaTrabalhoCod_Jsonclick = "";
         edtVariavelCocomo_AreaTrabalhoCod_Enabled = 1;
         edtVariavelCocomo_AreaTrabalhoCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Variavelcocomo_areatrabalhocod( int GX_Parm1 )
      {
         A961VariavelCocomo_AreaTrabalhoCod = GX_Parm1;
         /* Using cursor T002X16 */
         pr_default.execute(14, new Object[] {A961VariavelCocomo_AreaTrabalhoCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Vriaveis Cocomo_Area Trabalho'.", "ForeignKeyNotFound", 1, "VARIAVELCOCOMO_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtVariavelCocomo_AreaTrabalhoCod_Internalname;
         }
         pr_default.close(14);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Variavelcocomo_projetocod( int GX_Parm1 )
      {
         A965VariavelCocomo_ProjetoCod = GX_Parm1;
         n965VariavelCocomo_ProjetoCod = false;
         /* Using cursor T002X17 */
         pr_default.execute(15, new Object[] {n965VariavelCocomo_ProjetoCod, A965VariavelCocomo_ProjetoCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            if ( ! ( (0==A965VariavelCocomo_ProjetoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Variaveis Cocomo_Projeto'.", "ForeignKeyNotFound", 1, "VARIAVELCOCOMO_PROJETOCOD");
               AnyError = 1;
               GX_FocusControl = edtVariavelCocomo_ProjetoCod_Internalname;
            }
         }
         pr_default.close(15);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7VariavelCocomo_AreaTrabalhoCod',fld:'vVARIAVELCOCOMO_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV8VariavelCocomo_Sigla',fld:'vVARIAVELCOCOMO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'AV9VariavelCocomo_Tipo',fld:'vVARIAVELCOCOMO_TIPO',pic:'',hsh:true,nv:''},{av:'AV16VariavelCocomo_Sequencial',fld:'vVARIAVELCOCOMO_SEQUENCIAL',pic:'ZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E122X2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV11TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(14);
         pr_default.close(15);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         wcpOAV8VariavelCocomo_Sigla = "";
         wcpOAV9VariavelCocomo_Tipo = "";
         Z962VariavelCocomo_Sigla = "";
         Z964VariavelCocomo_Tipo = "";
         Z963VariavelCocomo_Nome = "";
         Z966VariavelCocomo_Data = DateTime.MinValue;
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A964VariavelCocomo_Tipo = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockvariavelcocomo_sigla_Jsonclick = "";
         A962VariavelCocomo_Sigla = "";
         lblTextblockvariavelcocomo_nome_Jsonclick = "";
         A963VariavelCocomo_Nome = "";
         lblTextblockvariavelcocomo_tipo_Jsonclick = "";
         lblTextblockvariavelcocomo_data_Jsonclick = "";
         A966VariavelCocomo_Data = DateTime.MinValue;
         lblTextblockvariavelcocomo_extrabaixo_Jsonclick = "";
         lblTextblockvariavelcocomo_muitobaixo_Jsonclick = "";
         lblTextblockvariavelcocomo_baixo_Jsonclick = "";
         lblTextblockvariavelcocomo_nominal_Jsonclick = "";
         lblTextblockvariavelcocomo_alto_Jsonclick = "";
         lblTextblockvariavelcocomo_muitoalto_Jsonclick = "";
         lblTextblockvariavelcocomo_extraalto_Jsonclick = "";
         AV17Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode120 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV10WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV11TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV12WebSession = context.GetSession();
         AV14TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         T002X6_A962VariavelCocomo_Sigla = new String[] {""} ;
         T002X6_A964VariavelCocomo_Tipo = new String[] {""} ;
         T002X6_A992VariavelCocomo_Sequencial = new short[1] ;
         T002X6_A963VariavelCocomo_Nome = new String[] {""} ;
         T002X6_A966VariavelCocomo_Data = new DateTime[] {DateTime.MinValue} ;
         T002X6_n966VariavelCocomo_Data = new bool[] {false} ;
         T002X6_A986VariavelCocomo_ExtraBaixo = new decimal[1] ;
         T002X6_n986VariavelCocomo_ExtraBaixo = new bool[] {false} ;
         T002X6_A967VariavelCocomo_MuitoBaixo = new decimal[1] ;
         T002X6_n967VariavelCocomo_MuitoBaixo = new bool[] {false} ;
         T002X6_A968VariavelCocomo_Baixo = new decimal[1] ;
         T002X6_n968VariavelCocomo_Baixo = new bool[] {false} ;
         T002X6_A969VariavelCocomo_Nominal = new decimal[1] ;
         T002X6_n969VariavelCocomo_Nominal = new bool[] {false} ;
         T002X6_A970VariavelCocomo_Alto = new decimal[1] ;
         T002X6_n970VariavelCocomo_Alto = new bool[] {false} ;
         T002X6_A971VariavelCocomo_MuitoAlto = new decimal[1] ;
         T002X6_n971VariavelCocomo_MuitoAlto = new bool[] {false} ;
         T002X6_A972VariavelCocomo_ExtraAlto = new decimal[1] ;
         T002X6_n972VariavelCocomo_ExtraAlto = new bool[] {false} ;
         T002X6_A973VariavelCocomo_Usado = new decimal[1] ;
         T002X6_n973VariavelCocomo_Usado = new bool[] {false} ;
         T002X6_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         T002X6_A965VariavelCocomo_ProjetoCod = new int[1] ;
         T002X6_n965VariavelCocomo_ProjetoCod = new bool[] {false} ;
         T002X4_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         T002X5_A965VariavelCocomo_ProjetoCod = new int[1] ;
         T002X5_n965VariavelCocomo_ProjetoCod = new bool[] {false} ;
         T002X7_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         T002X8_A965VariavelCocomo_ProjetoCod = new int[1] ;
         T002X8_n965VariavelCocomo_ProjetoCod = new bool[] {false} ;
         T002X9_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         T002X9_A962VariavelCocomo_Sigla = new String[] {""} ;
         T002X9_A964VariavelCocomo_Tipo = new String[] {""} ;
         T002X9_A992VariavelCocomo_Sequencial = new short[1] ;
         T002X3_A962VariavelCocomo_Sigla = new String[] {""} ;
         T002X3_A964VariavelCocomo_Tipo = new String[] {""} ;
         T002X3_A992VariavelCocomo_Sequencial = new short[1] ;
         T002X3_A963VariavelCocomo_Nome = new String[] {""} ;
         T002X3_A966VariavelCocomo_Data = new DateTime[] {DateTime.MinValue} ;
         T002X3_n966VariavelCocomo_Data = new bool[] {false} ;
         T002X3_A986VariavelCocomo_ExtraBaixo = new decimal[1] ;
         T002X3_n986VariavelCocomo_ExtraBaixo = new bool[] {false} ;
         T002X3_A967VariavelCocomo_MuitoBaixo = new decimal[1] ;
         T002X3_n967VariavelCocomo_MuitoBaixo = new bool[] {false} ;
         T002X3_A968VariavelCocomo_Baixo = new decimal[1] ;
         T002X3_n968VariavelCocomo_Baixo = new bool[] {false} ;
         T002X3_A969VariavelCocomo_Nominal = new decimal[1] ;
         T002X3_n969VariavelCocomo_Nominal = new bool[] {false} ;
         T002X3_A970VariavelCocomo_Alto = new decimal[1] ;
         T002X3_n970VariavelCocomo_Alto = new bool[] {false} ;
         T002X3_A971VariavelCocomo_MuitoAlto = new decimal[1] ;
         T002X3_n971VariavelCocomo_MuitoAlto = new bool[] {false} ;
         T002X3_A972VariavelCocomo_ExtraAlto = new decimal[1] ;
         T002X3_n972VariavelCocomo_ExtraAlto = new bool[] {false} ;
         T002X3_A973VariavelCocomo_Usado = new decimal[1] ;
         T002X3_n973VariavelCocomo_Usado = new bool[] {false} ;
         T002X3_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         T002X3_A965VariavelCocomo_ProjetoCod = new int[1] ;
         T002X3_n965VariavelCocomo_ProjetoCod = new bool[] {false} ;
         T002X10_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         T002X10_A962VariavelCocomo_Sigla = new String[] {""} ;
         T002X10_A964VariavelCocomo_Tipo = new String[] {""} ;
         T002X10_A992VariavelCocomo_Sequencial = new short[1] ;
         T002X11_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         T002X11_A962VariavelCocomo_Sigla = new String[] {""} ;
         T002X11_A964VariavelCocomo_Tipo = new String[] {""} ;
         T002X11_A992VariavelCocomo_Sequencial = new short[1] ;
         T002X2_A962VariavelCocomo_Sigla = new String[] {""} ;
         T002X2_A964VariavelCocomo_Tipo = new String[] {""} ;
         T002X2_A992VariavelCocomo_Sequencial = new short[1] ;
         T002X2_A963VariavelCocomo_Nome = new String[] {""} ;
         T002X2_A966VariavelCocomo_Data = new DateTime[] {DateTime.MinValue} ;
         T002X2_n966VariavelCocomo_Data = new bool[] {false} ;
         T002X2_A986VariavelCocomo_ExtraBaixo = new decimal[1] ;
         T002X2_n986VariavelCocomo_ExtraBaixo = new bool[] {false} ;
         T002X2_A967VariavelCocomo_MuitoBaixo = new decimal[1] ;
         T002X2_n967VariavelCocomo_MuitoBaixo = new bool[] {false} ;
         T002X2_A968VariavelCocomo_Baixo = new decimal[1] ;
         T002X2_n968VariavelCocomo_Baixo = new bool[] {false} ;
         T002X2_A969VariavelCocomo_Nominal = new decimal[1] ;
         T002X2_n969VariavelCocomo_Nominal = new bool[] {false} ;
         T002X2_A970VariavelCocomo_Alto = new decimal[1] ;
         T002X2_n970VariavelCocomo_Alto = new bool[] {false} ;
         T002X2_A971VariavelCocomo_MuitoAlto = new decimal[1] ;
         T002X2_n971VariavelCocomo_MuitoAlto = new bool[] {false} ;
         T002X2_A972VariavelCocomo_ExtraAlto = new decimal[1] ;
         T002X2_n972VariavelCocomo_ExtraAlto = new bool[] {false} ;
         T002X2_A973VariavelCocomo_Usado = new decimal[1] ;
         T002X2_n973VariavelCocomo_Usado = new bool[] {false} ;
         T002X2_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         T002X2_A965VariavelCocomo_ProjetoCod = new int[1] ;
         T002X2_n965VariavelCocomo_ProjetoCod = new bool[] {false} ;
         T002X15_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         T002X15_A962VariavelCocomo_Sigla = new String[] {""} ;
         T002X15_A964VariavelCocomo_Tipo = new String[] {""} ;
         T002X15_A992VariavelCocomo_Sequencial = new short[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T002X16_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         T002X17_A965VariavelCocomo_ProjetoCod = new int[1] ;
         T002X17_n965VariavelCocomo_ProjetoCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.variaveiscocomo__default(),
            new Object[][] {
                new Object[] {
               T002X2_A962VariavelCocomo_Sigla, T002X2_A964VariavelCocomo_Tipo, T002X2_A992VariavelCocomo_Sequencial, T002X2_A963VariavelCocomo_Nome, T002X2_A966VariavelCocomo_Data, T002X2_n966VariavelCocomo_Data, T002X2_A986VariavelCocomo_ExtraBaixo, T002X2_n986VariavelCocomo_ExtraBaixo, T002X2_A967VariavelCocomo_MuitoBaixo, T002X2_n967VariavelCocomo_MuitoBaixo,
               T002X2_A968VariavelCocomo_Baixo, T002X2_n968VariavelCocomo_Baixo, T002X2_A969VariavelCocomo_Nominal, T002X2_n969VariavelCocomo_Nominal, T002X2_A970VariavelCocomo_Alto, T002X2_n970VariavelCocomo_Alto, T002X2_A971VariavelCocomo_MuitoAlto, T002X2_n971VariavelCocomo_MuitoAlto, T002X2_A972VariavelCocomo_ExtraAlto, T002X2_n972VariavelCocomo_ExtraAlto,
               T002X2_A973VariavelCocomo_Usado, T002X2_n973VariavelCocomo_Usado, T002X2_A961VariavelCocomo_AreaTrabalhoCod, T002X2_A965VariavelCocomo_ProjetoCod, T002X2_n965VariavelCocomo_ProjetoCod
               }
               , new Object[] {
               T002X3_A962VariavelCocomo_Sigla, T002X3_A964VariavelCocomo_Tipo, T002X3_A992VariavelCocomo_Sequencial, T002X3_A963VariavelCocomo_Nome, T002X3_A966VariavelCocomo_Data, T002X3_n966VariavelCocomo_Data, T002X3_A986VariavelCocomo_ExtraBaixo, T002X3_n986VariavelCocomo_ExtraBaixo, T002X3_A967VariavelCocomo_MuitoBaixo, T002X3_n967VariavelCocomo_MuitoBaixo,
               T002X3_A968VariavelCocomo_Baixo, T002X3_n968VariavelCocomo_Baixo, T002X3_A969VariavelCocomo_Nominal, T002X3_n969VariavelCocomo_Nominal, T002X3_A970VariavelCocomo_Alto, T002X3_n970VariavelCocomo_Alto, T002X3_A971VariavelCocomo_MuitoAlto, T002X3_n971VariavelCocomo_MuitoAlto, T002X3_A972VariavelCocomo_ExtraAlto, T002X3_n972VariavelCocomo_ExtraAlto,
               T002X3_A973VariavelCocomo_Usado, T002X3_n973VariavelCocomo_Usado, T002X3_A961VariavelCocomo_AreaTrabalhoCod, T002X3_A965VariavelCocomo_ProjetoCod, T002X3_n965VariavelCocomo_ProjetoCod
               }
               , new Object[] {
               T002X4_A961VariavelCocomo_AreaTrabalhoCod
               }
               , new Object[] {
               T002X5_A965VariavelCocomo_ProjetoCod
               }
               , new Object[] {
               T002X6_A962VariavelCocomo_Sigla, T002X6_A964VariavelCocomo_Tipo, T002X6_A992VariavelCocomo_Sequencial, T002X6_A963VariavelCocomo_Nome, T002X6_A966VariavelCocomo_Data, T002X6_n966VariavelCocomo_Data, T002X6_A986VariavelCocomo_ExtraBaixo, T002X6_n986VariavelCocomo_ExtraBaixo, T002X6_A967VariavelCocomo_MuitoBaixo, T002X6_n967VariavelCocomo_MuitoBaixo,
               T002X6_A968VariavelCocomo_Baixo, T002X6_n968VariavelCocomo_Baixo, T002X6_A969VariavelCocomo_Nominal, T002X6_n969VariavelCocomo_Nominal, T002X6_A970VariavelCocomo_Alto, T002X6_n970VariavelCocomo_Alto, T002X6_A971VariavelCocomo_MuitoAlto, T002X6_n971VariavelCocomo_MuitoAlto, T002X6_A972VariavelCocomo_ExtraAlto, T002X6_n972VariavelCocomo_ExtraAlto,
               T002X6_A973VariavelCocomo_Usado, T002X6_n973VariavelCocomo_Usado, T002X6_A961VariavelCocomo_AreaTrabalhoCod, T002X6_A965VariavelCocomo_ProjetoCod, T002X6_n965VariavelCocomo_ProjetoCod
               }
               , new Object[] {
               T002X7_A961VariavelCocomo_AreaTrabalhoCod
               }
               , new Object[] {
               T002X8_A965VariavelCocomo_ProjetoCod
               }
               , new Object[] {
               T002X9_A961VariavelCocomo_AreaTrabalhoCod, T002X9_A962VariavelCocomo_Sigla, T002X9_A964VariavelCocomo_Tipo, T002X9_A992VariavelCocomo_Sequencial
               }
               , new Object[] {
               T002X10_A961VariavelCocomo_AreaTrabalhoCod, T002X10_A962VariavelCocomo_Sigla, T002X10_A964VariavelCocomo_Tipo, T002X10_A992VariavelCocomo_Sequencial
               }
               , new Object[] {
               T002X11_A961VariavelCocomo_AreaTrabalhoCod, T002X11_A962VariavelCocomo_Sigla, T002X11_A964VariavelCocomo_Tipo, T002X11_A992VariavelCocomo_Sequencial
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002X15_A961VariavelCocomo_AreaTrabalhoCod, T002X15_A962VariavelCocomo_Sigla, T002X15_A964VariavelCocomo_Tipo, T002X15_A992VariavelCocomo_Sequencial
               }
               , new Object[] {
               T002X16_A961VariavelCocomo_AreaTrabalhoCod
               }
               , new Object[] {
               T002X17_A965VariavelCocomo_ProjetoCod
               }
            }
         );
         Z992VariavelCocomo_Sequencial = 0;
         A992VariavelCocomo_Sequencial = 0;
         AV17Pgmname = "VariaveisCocomo";
      }

      private short wcpOAV16VariavelCocomo_Sequencial ;
      private short Z992VariavelCocomo_Sequencial ;
      private short GxWebError ;
      private short AV16VariavelCocomo_Sequencial ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short A992VariavelCocomo_Sequencial ;
      private short RcdFound120 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7VariavelCocomo_AreaTrabalhoCod ;
      private int Z961VariavelCocomo_AreaTrabalhoCod ;
      private int Z965VariavelCocomo_ProjetoCod ;
      private int N965VariavelCocomo_ProjetoCod ;
      private int A961VariavelCocomo_AreaTrabalhoCod ;
      private int A965VariavelCocomo_ProjetoCod ;
      private int AV7VariavelCocomo_AreaTrabalhoCod ;
      private int trnEnded ;
      private int edtVariavelCocomo_AreaTrabalhoCod_Visible ;
      private int edtVariavelCocomo_AreaTrabalhoCod_Enabled ;
      private int edtVariavelCocomo_ProjetoCod_Visible ;
      private int edtVariavelCocomo_ProjetoCod_Enabled ;
      private int edtVariavelCocomo_Usado_Enabled ;
      private int edtVariavelCocomo_Usado_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtVariavelCocomo_Sigla_Enabled ;
      private int edtVariavelCocomo_Nome_Enabled ;
      private int edtVariavelCocomo_Data_Enabled ;
      private int edtVariavelCocomo_ExtraBaixo_Enabled ;
      private int edtVariavelCocomo_MuitoBaixo_Enabled ;
      private int edtVariavelCocomo_Baixo_Enabled ;
      private int edtVariavelCocomo_Nominal_Enabled ;
      private int edtVariavelCocomo_Alto_Enabled ;
      private int edtVariavelCocomo_MuitoAlto_Enabled ;
      private int edtVariavelCocomo_ExtraAlto_Enabled ;
      private int AV13Insert_VariavelCocomo_ProjetoCod ;
      private int AV18GXV1 ;
      private int idxLst ;
      private decimal Z986VariavelCocomo_ExtraBaixo ;
      private decimal Z967VariavelCocomo_MuitoBaixo ;
      private decimal Z968VariavelCocomo_Baixo ;
      private decimal Z969VariavelCocomo_Nominal ;
      private decimal Z970VariavelCocomo_Alto ;
      private decimal Z971VariavelCocomo_MuitoAlto ;
      private decimal Z972VariavelCocomo_ExtraAlto ;
      private decimal Z973VariavelCocomo_Usado ;
      private decimal A973VariavelCocomo_Usado ;
      private decimal A986VariavelCocomo_ExtraBaixo ;
      private decimal A967VariavelCocomo_MuitoBaixo ;
      private decimal A968VariavelCocomo_Baixo ;
      private decimal A969VariavelCocomo_Nominal ;
      private decimal A970VariavelCocomo_Alto ;
      private decimal A971VariavelCocomo_MuitoAlto ;
      private decimal A972VariavelCocomo_ExtraAlto ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String wcpOAV8VariavelCocomo_Sigla ;
      private String wcpOAV9VariavelCocomo_Tipo ;
      private String Z962VariavelCocomo_Sigla ;
      private String Z964VariavelCocomo_Tipo ;
      private String Z963VariavelCocomo_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String AV8VariavelCocomo_Sigla ;
      private String AV9VariavelCocomo_Tipo ;
      private String GXKey ;
      private String A964VariavelCocomo_Tipo ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtVariavelCocomo_Sigla_Internalname ;
      private String TempTags ;
      private String edtVariavelCocomo_AreaTrabalhoCod_Internalname ;
      private String edtVariavelCocomo_AreaTrabalhoCod_Jsonclick ;
      private String edtVariavelCocomo_ProjetoCod_Internalname ;
      private String edtVariavelCocomo_ProjetoCod_Jsonclick ;
      private String edtVariavelCocomo_Usado_Internalname ;
      private String edtVariavelCocomo_Usado_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockvariavelcocomo_sigla_Internalname ;
      private String lblTextblockvariavelcocomo_sigla_Jsonclick ;
      private String A962VariavelCocomo_Sigla ;
      private String edtVariavelCocomo_Sigla_Jsonclick ;
      private String lblTextblockvariavelcocomo_nome_Internalname ;
      private String lblTextblockvariavelcocomo_nome_Jsonclick ;
      private String edtVariavelCocomo_Nome_Internalname ;
      private String A963VariavelCocomo_Nome ;
      private String edtVariavelCocomo_Nome_Jsonclick ;
      private String lblTextblockvariavelcocomo_tipo_Internalname ;
      private String lblTextblockvariavelcocomo_tipo_Jsonclick ;
      private String cmbVariavelCocomo_Tipo_Internalname ;
      private String cmbVariavelCocomo_Tipo_Jsonclick ;
      private String lblTextblockvariavelcocomo_data_Internalname ;
      private String lblTextblockvariavelcocomo_data_Jsonclick ;
      private String edtVariavelCocomo_Data_Internalname ;
      private String edtVariavelCocomo_Data_Jsonclick ;
      private String lblTextblockvariavelcocomo_extrabaixo_Internalname ;
      private String lblTextblockvariavelcocomo_extrabaixo_Jsonclick ;
      private String edtVariavelCocomo_ExtraBaixo_Internalname ;
      private String edtVariavelCocomo_ExtraBaixo_Jsonclick ;
      private String lblTextblockvariavelcocomo_muitobaixo_Internalname ;
      private String lblTextblockvariavelcocomo_muitobaixo_Jsonclick ;
      private String edtVariavelCocomo_MuitoBaixo_Internalname ;
      private String edtVariavelCocomo_MuitoBaixo_Jsonclick ;
      private String lblTextblockvariavelcocomo_baixo_Internalname ;
      private String lblTextblockvariavelcocomo_baixo_Jsonclick ;
      private String edtVariavelCocomo_Baixo_Internalname ;
      private String edtVariavelCocomo_Baixo_Jsonclick ;
      private String lblTextblockvariavelcocomo_nominal_Internalname ;
      private String lblTextblockvariavelcocomo_nominal_Jsonclick ;
      private String edtVariavelCocomo_Nominal_Internalname ;
      private String edtVariavelCocomo_Nominal_Jsonclick ;
      private String lblTextblockvariavelcocomo_alto_Internalname ;
      private String lblTextblockvariavelcocomo_alto_Jsonclick ;
      private String edtVariavelCocomo_Alto_Internalname ;
      private String edtVariavelCocomo_Alto_Jsonclick ;
      private String lblTextblockvariavelcocomo_muitoalto_Internalname ;
      private String lblTextblockvariavelcocomo_muitoalto_Jsonclick ;
      private String edtVariavelCocomo_MuitoAlto_Internalname ;
      private String edtVariavelCocomo_MuitoAlto_Jsonclick ;
      private String lblTextblockvariavelcocomo_extraalto_Internalname ;
      private String lblTextblockvariavelcocomo_extraalto_Jsonclick ;
      private String edtVariavelCocomo_ExtraAlto_Internalname ;
      private String edtVariavelCocomo_ExtraAlto_Jsonclick ;
      private String AV17Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode120 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime Z966VariavelCocomo_Data ;
      private DateTime A966VariavelCocomo_Data ;
      private bool entryPointCalled ;
      private bool n965VariavelCocomo_ProjetoCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n966VariavelCocomo_Data ;
      private bool n986VariavelCocomo_ExtraBaixo ;
      private bool n967VariavelCocomo_MuitoBaixo ;
      private bool n968VariavelCocomo_Baixo ;
      private bool n969VariavelCocomo_Nominal ;
      private bool n970VariavelCocomo_Alto ;
      private bool n971VariavelCocomo_MuitoAlto ;
      private bool n972VariavelCocomo_ExtraAlto ;
      private bool n973VariavelCocomo_Usado ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private IGxSession AV12WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbVariavelCocomo_Tipo ;
      private IDataStoreProvider pr_default ;
      private String[] T002X6_A962VariavelCocomo_Sigla ;
      private String[] T002X6_A964VariavelCocomo_Tipo ;
      private short[] T002X6_A992VariavelCocomo_Sequencial ;
      private String[] T002X6_A963VariavelCocomo_Nome ;
      private DateTime[] T002X6_A966VariavelCocomo_Data ;
      private bool[] T002X6_n966VariavelCocomo_Data ;
      private decimal[] T002X6_A986VariavelCocomo_ExtraBaixo ;
      private bool[] T002X6_n986VariavelCocomo_ExtraBaixo ;
      private decimal[] T002X6_A967VariavelCocomo_MuitoBaixo ;
      private bool[] T002X6_n967VariavelCocomo_MuitoBaixo ;
      private decimal[] T002X6_A968VariavelCocomo_Baixo ;
      private bool[] T002X6_n968VariavelCocomo_Baixo ;
      private decimal[] T002X6_A969VariavelCocomo_Nominal ;
      private bool[] T002X6_n969VariavelCocomo_Nominal ;
      private decimal[] T002X6_A970VariavelCocomo_Alto ;
      private bool[] T002X6_n970VariavelCocomo_Alto ;
      private decimal[] T002X6_A971VariavelCocomo_MuitoAlto ;
      private bool[] T002X6_n971VariavelCocomo_MuitoAlto ;
      private decimal[] T002X6_A972VariavelCocomo_ExtraAlto ;
      private bool[] T002X6_n972VariavelCocomo_ExtraAlto ;
      private decimal[] T002X6_A973VariavelCocomo_Usado ;
      private bool[] T002X6_n973VariavelCocomo_Usado ;
      private int[] T002X6_A961VariavelCocomo_AreaTrabalhoCod ;
      private int[] T002X6_A965VariavelCocomo_ProjetoCod ;
      private bool[] T002X6_n965VariavelCocomo_ProjetoCod ;
      private int[] T002X4_A961VariavelCocomo_AreaTrabalhoCod ;
      private int[] T002X5_A965VariavelCocomo_ProjetoCod ;
      private bool[] T002X5_n965VariavelCocomo_ProjetoCod ;
      private int[] T002X7_A961VariavelCocomo_AreaTrabalhoCod ;
      private int[] T002X8_A965VariavelCocomo_ProjetoCod ;
      private bool[] T002X8_n965VariavelCocomo_ProjetoCod ;
      private int[] T002X9_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] T002X9_A962VariavelCocomo_Sigla ;
      private String[] T002X9_A964VariavelCocomo_Tipo ;
      private short[] T002X9_A992VariavelCocomo_Sequencial ;
      private String[] T002X3_A962VariavelCocomo_Sigla ;
      private String[] T002X3_A964VariavelCocomo_Tipo ;
      private short[] T002X3_A992VariavelCocomo_Sequencial ;
      private String[] T002X3_A963VariavelCocomo_Nome ;
      private DateTime[] T002X3_A966VariavelCocomo_Data ;
      private bool[] T002X3_n966VariavelCocomo_Data ;
      private decimal[] T002X3_A986VariavelCocomo_ExtraBaixo ;
      private bool[] T002X3_n986VariavelCocomo_ExtraBaixo ;
      private decimal[] T002X3_A967VariavelCocomo_MuitoBaixo ;
      private bool[] T002X3_n967VariavelCocomo_MuitoBaixo ;
      private decimal[] T002X3_A968VariavelCocomo_Baixo ;
      private bool[] T002X3_n968VariavelCocomo_Baixo ;
      private decimal[] T002X3_A969VariavelCocomo_Nominal ;
      private bool[] T002X3_n969VariavelCocomo_Nominal ;
      private decimal[] T002X3_A970VariavelCocomo_Alto ;
      private bool[] T002X3_n970VariavelCocomo_Alto ;
      private decimal[] T002X3_A971VariavelCocomo_MuitoAlto ;
      private bool[] T002X3_n971VariavelCocomo_MuitoAlto ;
      private decimal[] T002X3_A972VariavelCocomo_ExtraAlto ;
      private bool[] T002X3_n972VariavelCocomo_ExtraAlto ;
      private decimal[] T002X3_A973VariavelCocomo_Usado ;
      private bool[] T002X3_n973VariavelCocomo_Usado ;
      private int[] T002X3_A961VariavelCocomo_AreaTrabalhoCod ;
      private int[] T002X3_A965VariavelCocomo_ProjetoCod ;
      private bool[] T002X3_n965VariavelCocomo_ProjetoCod ;
      private int[] T002X10_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] T002X10_A962VariavelCocomo_Sigla ;
      private String[] T002X10_A964VariavelCocomo_Tipo ;
      private short[] T002X10_A992VariavelCocomo_Sequencial ;
      private int[] T002X11_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] T002X11_A962VariavelCocomo_Sigla ;
      private String[] T002X11_A964VariavelCocomo_Tipo ;
      private short[] T002X11_A992VariavelCocomo_Sequencial ;
      private String[] T002X2_A962VariavelCocomo_Sigla ;
      private String[] T002X2_A964VariavelCocomo_Tipo ;
      private short[] T002X2_A992VariavelCocomo_Sequencial ;
      private String[] T002X2_A963VariavelCocomo_Nome ;
      private DateTime[] T002X2_A966VariavelCocomo_Data ;
      private bool[] T002X2_n966VariavelCocomo_Data ;
      private decimal[] T002X2_A986VariavelCocomo_ExtraBaixo ;
      private bool[] T002X2_n986VariavelCocomo_ExtraBaixo ;
      private decimal[] T002X2_A967VariavelCocomo_MuitoBaixo ;
      private bool[] T002X2_n967VariavelCocomo_MuitoBaixo ;
      private decimal[] T002X2_A968VariavelCocomo_Baixo ;
      private bool[] T002X2_n968VariavelCocomo_Baixo ;
      private decimal[] T002X2_A969VariavelCocomo_Nominal ;
      private bool[] T002X2_n969VariavelCocomo_Nominal ;
      private decimal[] T002X2_A970VariavelCocomo_Alto ;
      private bool[] T002X2_n970VariavelCocomo_Alto ;
      private decimal[] T002X2_A971VariavelCocomo_MuitoAlto ;
      private bool[] T002X2_n971VariavelCocomo_MuitoAlto ;
      private decimal[] T002X2_A972VariavelCocomo_ExtraAlto ;
      private bool[] T002X2_n972VariavelCocomo_ExtraAlto ;
      private decimal[] T002X2_A973VariavelCocomo_Usado ;
      private bool[] T002X2_n973VariavelCocomo_Usado ;
      private int[] T002X2_A961VariavelCocomo_AreaTrabalhoCod ;
      private int[] T002X2_A965VariavelCocomo_ProjetoCod ;
      private bool[] T002X2_n965VariavelCocomo_ProjetoCod ;
      private int[] T002X15_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] T002X15_A962VariavelCocomo_Sigla ;
      private String[] T002X15_A964VariavelCocomo_Tipo ;
      private short[] T002X15_A992VariavelCocomo_Sequencial ;
      private int[] T002X16_A961VariavelCocomo_AreaTrabalhoCod ;
      private int[] T002X17_A965VariavelCocomo_ProjetoCod ;
      private bool[] T002X17_n965VariavelCocomo_ProjetoCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV10WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV11TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV14TrnContextAtt ;
   }

   public class variaveiscocomo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002X6 ;
          prmT002X6 = new Object[] {
          new Object[] {"@VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@VariavelCocomo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@VariavelCocomo_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@VariavelCocomo_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002X4 ;
          prmT002X4 = new Object[] {
          new Object[] {"@VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002X5 ;
          prmT002X5 = new Object[] {
          new Object[] {"@VariavelCocomo_ProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002X7 ;
          prmT002X7 = new Object[] {
          new Object[] {"@VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002X8 ;
          prmT002X8 = new Object[] {
          new Object[] {"@VariavelCocomo_ProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002X9 ;
          prmT002X9 = new Object[] {
          new Object[] {"@VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@VariavelCocomo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@VariavelCocomo_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@VariavelCocomo_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002X3 ;
          prmT002X3 = new Object[] {
          new Object[] {"@VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@VariavelCocomo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@VariavelCocomo_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@VariavelCocomo_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002X10 ;
          prmT002X10 = new Object[] {
          new Object[] {"@VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@VariavelCocomo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@VariavelCocomo_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@VariavelCocomo_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002X11 ;
          prmT002X11 = new Object[] {
          new Object[] {"@VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@VariavelCocomo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@VariavelCocomo_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@VariavelCocomo_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002X2 ;
          prmT002X2 = new Object[] {
          new Object[] {"@VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@VariavelCocomo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@VariavelCocomo_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@VariavelCocomo_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002X12 ;
          prmT002X12 = new Object[] {
          new Object[] {"@VariavelCocomo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@VariavelCocomo_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@VariavelCocomo_Sequencial",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@VariavelCocomo_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@VariavelCocomo_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@VariavelCocomo_ExtraBaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_MuitoBaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_Baixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_Nominal",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_Alto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_MuitoAlto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_ExtraAlto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_Usado",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@VariavelCocomo_ProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002X13 ;
          prmT002X13 = new Object[] {
          new Object[] {"@VariavelCocomo_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@VariavelCocomo_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@VariavelCocomo_ExtraBaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_MuitoBaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_Baixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_Nominal",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_Alto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_MuitoAlto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_ExtraAlto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_Usado",SqlDbType.Decimal,12,2} ,
          new Object[] {"@VariavelCocomo_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@VariavelCocomo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@VariavelCocomo_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@VariavelCocomo_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002X14 ;
          prmT002X14 = new Object[] {
          new Object[] {"@VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@VariavelCocomo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@VariavelCocomo_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@VariavelCocomo_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002X15 ;
          prmT002X15 = new Object[] {
          } ;
          Object[] prmT002X16 ;
          prmT002X16 = new Object[] {
          new Object[] {"@VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002X17 ;
          prmT002X17 = new Object[] {
          new Object[] {"@VariavelCocomo_ProjetoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002X2", "SELECT [VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial], [VariavelCocomo_Nome], [VariavelCocomo_Data], [VariavelCocomo_ExtraBaixo], [VariavelCocomo_MuitoBaixo], [VariavelCocomo_Baixo], [VariavelCocomo_Nominal], [VariavelCocomo_Alto], [VariavelCocomo_MuitoAlto], [VariavelCocomo_ExtraAlto], [VariavelCocomo_Usado], [VariavelCocomo_AreaTrabalhoCod] AS VariavelCocomo_AreaTrabalhoCod, [VariavelCocomo_ProjetoCod] AS VariavelCocomo_ProjetoCod FROM [VariaveisCocomo] WITH (UPDLOCK) WHERE [VariavelCocomo_AreaTrabalhoCod] = @VariavelCocomo_AreaTrabalhoCod AND [VariavelCocomo_Sigla] = @VariavelCocomo_Sigla AND [VariavelCocomo_Tipo] = @VariavelCocomo_Tipo AND [VariavelCocomo_Sequencial] = @VariavelCocomo_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmT002X2,1,0,true,false )
             ,new CursorDef("T002X3", "SELECT [VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial], [VariavelCocomo_Nome], [VariavelCocomo_Data], [VariavelCocomo_ExtraBaixo], [VariavelCocomo_MuitoBaixo], [VariavelCocomo_Baixo], [VariavelCocomo_Nominal], [VariavelCocomo_Alto], [VariavelCocomo_MuitoAlto], [VariavelCocomo_ExtraAlto], [VariavelCocomo_Usado], [VariavelCocomo_AreaTrabalhoCod] AS VariavelCocomo_AreaTrabalhoCod, [VariavelCocomo_ProjetoCod] AS VariavelCocomo_ProjetoCod FROM [VariaveisCocomo] WITH (NOLOCK) WHERE [VariavelCocomo_AreaTrabalhoCod] = @VariavelCocomo_AreaTrabalhoCod AND [VariavelCocomo_Sigla] = @VariavelCocomo_Sigla AND [VariavelCocomo_Tipo] = @VariavelCocomo_Tipo AND [VariavelCocomo_Sequencial] = @VariavelCocomo_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmT002X3,1,0,true,false )
             ,new CursorDef("T002X4", "SELECT [AreaTrabalho_Codigo] AS VariavelCocomo_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @VariavelCocomo_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002X4,1,0,true,false )
             ,new CursorDef("T002X5", "SELECT [Projeto_Codigo] AS VariavelCocomo_ProjetoCod FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @VariavelCocomo_ProjetoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002X5,1,0,true,false )
             ,new CursorDef("T002X6", "SELECT TM1.[VariavelCocomo_Sigla], TM1.[VariavelCocomo_Tipo], TM1.[VariavelCocomo_Sequencial], TM1.[VariavelCocomo_Nome], TM1.[VariavelCocomo_Data], TM1.[VariavelCocomo_ExtraBaixo], TM1.[VariavelCocomo_MuitoBaixo], TM1.[VariavelCocomo_Baixo], TM1.[VariavelCocomo_Nominal], TM1.[VariavelCocomo_Alto], TM1.[VariavelCocomo_MuitoAlto], TM1.[VariavelCocomo_ExtraAlto], TM1.[VariavelCocomo_Usado], TM1.[VariavelCocomo_AreaTrabalhoCod] AS VariavelCocomo_AreaTrabalhoCod, TM1.[VariavelCocomo_ProjetoCod] AS VariavelCocomo_ProjetoCod FROM [VariaveisCocomo] TM1 WITH (NOLOCK) WHERE TM1.[VariavelCocomo_AreaTrabalhoCod] = @VariavelCocomo_AreaTrabalhoCod and TM1.[VariavelCocomo_Sigla] = @VariavelCocomo_Sigla and TM1.[VariavelCocomo_Tipo] = @VariavelCocomo_Tipo and TM1.[VariavelCocomo_Sequencial] = @VariavelCocomo_Sequencial ORDER BY TM1.[VariavelCocomo_AreaTrabalhoCod], TM1.[VariavelCocomo_Sigla], TM1.[VariavelCocomo_Tipo], TM1.[VariavelCocomo_Sequencial]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002X6,100,0,true,false )
             ,new CursorDef("T002X7", "SELECT [AreaTrabalho_Codigo] AS VariavelCocomo_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @VariavelCocomo_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002X7,1,0,true,false )
             ,new CursorDef("T002X8", "SELECT [Projeto_Codigo] AS VariavelCocomo_ProjetoCod FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @VariavelCocomo_ProjetoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002X8,1,0,true,false )
             ,new CursorDef("T002X9", "SELECT [VariavelCocomo_AreaTrabalhoCod] AS VariavelCocomo_AreaTrabalhoCod, [VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial] FROM [VariaveisCocomo] WITH (NOLOCK) WHERE [VariavelCocomo_AreaTrabalhoCod] = @VariavelCocomo_AreaTrabalhoCod AND [VariavelCocomo_Sigla] = @VariavelCocomo_Sigla AND [VariavelCocomo_Tipo] = @VariavelCocomo_Tipo AND [VariavelCocomo_Sequencial] = @VariavelCocomo_Sequencial  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002X9,1,0,true,false )
             ,new CursorDef("T002X10", "SELECT TOP 1 [VariavelCocomo_AreaTrabalhoCod] AS VariavelCocomo_AreaTrabalhoCod, [VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial] FROM [VariaveisCocomo] WITH (NOLOCK) WHERE ( [VariavelCocomo_AreaTrabalhoCod] > @VariavelCocomo_AreaTrabalhoCod or [VariavelCocomo_AreaTrabalhoCod] = @VariavelCocomo_AreaTrabalhoCod and [VariavelCocomo_Sigla] > @VariavelCocomo_Sigla or [VariavelCocomo_Sigla] = @VariavelCocomo_Sigla and [VariavelCocomo_AreaTrabalhoCod] = @VariavelCocomo_AreaTrabalhoCod and [VariavelCocomo_Tipo] > @VariavelCocomo_Tipo or [VariavelCocomo_Tipo] = @VariavelCocomo_Tipo and [VariavelCocomo_Sigla] = @VariavelCocomo_Sigla and [VariavelCocomo_AreaTrabalhoCod] = @VariavelCocomo_AreaTrabalhoCod and [VariavelCocomo_Sequencial] > @VariavelCocomo_Sequencial) ORDER BY [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002X10,1,0,true,true )
             ,new CursorDef("T002X11", "SELECT TOP 1 [VariavelCocomo_AreaTrabalhoCod] AS VariavelCocomo_AreaTrabalhoCod, [VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial] FROM [VariaveisCocomo] WITH (NOLOCK) WHERE ( [VariavelCocomo_AreaTrabalhoCod] < @VariavelCocomo_AreaTrabalhoCod or [VariavelCocomo_AreaTrabalhoCod] = @VariavelCocomo_AreaTrabalhoCod and [VariavelCocomo_Sigla] < @VariavelCocomo_Sigla or [VariavelCocomo_Sigla] = @VariavelCocomo_Sigla and [VariavelCocomo_AreaTrabalhoCod] = @VariavelCocomo_AreaTrabalhoCod and [VariavelCocomo_Tipo] < @VariavelCocomo_Tipo or [VariavelCocomo_Tipo] = @VariavelCocomo_Tipo and [VariavelCocomo_Sigla] = @VariavelCocomo_Sigla and [VariavelCocomo_AreaTrabalhoCod] = @VariavelCocomo_AreaTrabalhoCod and [VariavelCocomo_Sequencial] < @VariavelCocomo_Sequencial) ORDER BY [VariavelCocomo_AreaTrabalhoCod] DESC, [VariavelCocomo_Sigla] DESC, [VariavelCocomo_Tipo] DESC, [VariavelCocomo_Sequencial] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002X11,1,0,true,true )
             ,new CursorDef("T002X12", "INSERT INTO [VariaveisCocomo]([VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial], [VariavelCocomo_Nome], [VariavelCocomo_Data], [VariavelCocomo_ExtraBaixo], [VariavelCocomo_MuitoBaixo], [VariavelCocomo_Baixo], [VariavelCocomo_Nominal], [VariavelCocomo_Alto], [VariavelCocomo_MuitoAlto], [VariavelCocomo_ExtraAlto], [VariavelCocomo_Usado], [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_ProjetoCod]) VALUES(@VariavelCocomo_Sigla, @VariavelCocomo_Tipo, @VariavelCocomo_Sequencial, @VariavelCocomo_Nome, @VariavelCocomo_Data, @VariavelCocomo_ExtraBaixo, @VariavelCocomo_MuitoBaixo, @VariavelCocomo_Baixo, @VariavelCocomo_Nominal, @VariavelCocomo_Alto, @VariavelCocomo_MuitoAlto, @VariavelCocomo_ExtraAlto, @VariavelCocomo_Usado, @VariavelCocomo_AreaTrabalhoCod, @VariavelCocomo_ProjetoCod)", GxErrorMask.GX_NOMASK,prmT002X12)
             ,new CursorDef("T002X13", "UPDATE [VariaveisCocomo] SET [VariavelCocomo_Nome]=@VariavelCocomo_Nome, [VariavelCocomo_Data]=@VariavelCocomo_Data, [VariavelCocomo_ExtraBaixo]=@VariavelCocomo_ExtraBaixo, [VariavelCocomo_MuitoBaixo]=@VariavelCocomo_MuitoBaixo, [VariavelCocomo_Baixo]=@VariavelCocomo_Baixo, [VariavelCocomo_Nominal]=@VariavelCocomo_Nominal, [VariavelCocomo_Alto]=@VariavelCocomo_Alto, [VariavelCocomo_MuitoAlto]=@VariavelCocomo_MuitoAlto, [VariavelCocomo_ExtraAlto]=@VariavelCocomo_ExtraAlto, [VariavelCocomo_Usado]=@VariavelCocomo_Usado, [VariavelCocomo_ProjetoCod]=@VariavelCocomo_ProjetoCod  WHERE [VariavelCocomo_AreaTrabalhoCod] = @VariavelCocomo_AreaTrabalhoCod AND [VariavelCocomo_Sigla] = @VariavelCocomo_Sigla AND [VariavelCocomo_Tipo] = @VariavelCocomo_Tipo AND [VariavelCocomo_Sequencial] = @VariavelCocomo_Sequencial", GxErrorMask.GX_NOMASK,prmT002X13)
             ,new CursorDef("T002X14", "DELETE FROM [VariaveisCocomo]  WHERE [VariavelCocomo_AreaTrabalhoCod] = @VariavelCocomo_AreaTrabalhoCod AND [VariavelCocomo_Sigla] = @VariavelCocomo_Sigla AND [VariavelCocomo_Tipo] = @VariavelCocomo_Tipo AND [VariavelCocomo_Sequencial] = @VariavelCocomo_Sequencial", GxErrorMask.GX_NOMASK,prmT002X14)
             ,new CursorDef("T002X15", "SELECT [VariavelCocomo_AreaTrabalhoCod] AS VariavelCocomo_AreaTrabalhoCod, [VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial] FROM [VariaveisCocomo] WITH (NOLOCK) ORDER BY [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002X15,100,0,true,false )
             ,new CursorDef("T002X16", "SELECT [AreaTrabalho_Codigo] AS VariavelCocomo_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @VariavelCocomo_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002X16,1,0,true,false )
             ,new CursorDef("T002X17", "SELECT [Projeto_Codigo] AS VariavelCocomo_ProjetoCod FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @VariavelCocomo_ProjetoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002X17,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((int[]) buf[22])[0] = rslt.getInt(14) ;
                ((int[]) buf[23])[0] = rslt.getInt(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((int[]) buf[22])[0] = rslt.getInt(14) ;
                ((int[]) buf[23])[0] = rslt.getInt(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((int[]) buf[22])[0] = rslt.getInt(14) ;
                ((int[]) buf[23])[0] = rslt.getInt(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(5, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 10 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(10, (decimal)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 13 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(13, (decimal)parms[21]);
                }
                stmt.SetParameter(14, (int)parms[22]);
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[24]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(8, (decimal)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(9, (decimal)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(10, (decimal)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[20]);
                }
                stmt.SetParameter(12, (int)parms[21]);
                stmt.SetParameter(13, (String)parms[22]);
                stmt.SetParameter(14, (String)parms[23]);
                stmt.SetParameter(15, (short)parms[24]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
