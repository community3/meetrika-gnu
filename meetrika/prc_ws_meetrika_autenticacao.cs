/*
               File: PRC_WS_Meetrika_Autenticacao
        Description: Realiza a Autentica��o do WS Meetrica
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:56.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_ws_meetrika_autenticacao : GXProcedure
   {
      public prc_ws_meetrika_autenticacao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_ws_meetrika_autenticacao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( SdtSDT_WSAutenticacao aP0_SDT_Autenticacao ,
                           out String aP1_Retorno ,
                           out bool aP2_IsAutenticado )
      {
         this.AV9SDT_Autenticacao = aP0_SDT_Autenticacao;
         this.AV8Retorno = "" ;
         this.AV10IsAutenticado = false ;
         initialize();
         executePrivate();
         aP1_Retorno=this.AV8Retorno;
         aP2_IsAutenticado=this.AV10IsAutenticado;
      }

      public bool executeUdp( SdtSDT_WSAutenticacao aP0_SDT_Autenticacao ,
                              out String aP1_Retorno )
      {
         this.AV9SDT_Autenticacao = aP0_SDT_Autenticacao;
         this.AV8Retorno = "" ;
         this.AV10IsAutenticado = false ;
         initialize();
         executePrivate();
         aP1_Retorno=this.AV8Retorno;
         aP2_IsAutenticado=this.AV10IsAutenticado;
         return AV10IsAutenticado ;
      }

      public void executeSubmit( SdtSDT_WSAutenticacao aP0_SDT_Autenticacao ,
                                 out String aP1_Retorno ,
                                 out bool aP2_IsAutenticado )
      {
         prc_ws_meetrika_autenticacao objprc_ws_meetrika_autenticacao;
         objprc_ws_meetrika_autenticacao = new prc_ws_meetrika_autenticacao();
         objprc_ws_meetrika_autenticacao.AV9SDT_Autenticacao = aP0_SDT_Autenticacao;
         objprc_ws_meetrika_autenticacao.AV8Retorno = "" ;
         objprc_ws_meetrika_autenticacao.AV10IsAutenticado = false ;
         objprc_ws_meetrika_autenticacao.context.SetSubmitInitialConfig(context);
         objprc_ws_meetrika_autenticacao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_ws_meetrika_autenticacao);
         aP1_Retorno=this.AV8Retorno;
         aP2_IsAutenticado=this.AV10IsAutenticado;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_ws_meetrika_autenticacao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Retorno = "";
         AV10IsAutenticado = false;
         AV10IsAutenticado = new SdtGAMRepository(context).login(AV9SDT_Autenticacao.gxTpr_Usuario, AV9SDT_Autenticacao.gxTpr_Senha, AV13GAMAdditionalParameter, out  AV12Errors);
         if ( AV10IsAutenticado )
         {
            AV8Retorno = new SdtGAMUser(context).getid();
         }
         else
         {
            AV8Retorno = "Falha na Autentica��o do Usu�rio.";
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV13GAMAdditionalParameter = new SdtGAMLoginAdditionalParameters(context);
         AV12Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private bool AV10IsAutenticado ;
      private String AV8Retorno ;
      private String aP1_Retorno ;
      private bool aP2_IsAutenticado ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV12Errors ;
      private SdtSDT_WSAutenticacao AV9SDT_Autenticacao ;
      private SdtGAMLoginAdditionalParameters AV13GAMAdditionalParameter ;
   }

}
