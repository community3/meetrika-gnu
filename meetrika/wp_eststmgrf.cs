/*
               File: WP_EstStmGrf
        Description: Estatística
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:47:51.37
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_eststmgrf : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_eststmgrf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_eststmgrf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_AreaTrabalho_Codigo ,
                           ref int aP1_Contratada_Codigo ,
                           ref short aP2_Ano ,
                           ref long aP3_Mes ,
                           ref String aP4_Graficar )
      {
         this.AV18AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV17Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV19Ano = aP2_Ano;
         this.AV20Mes = aP3_Mes;
         this.AV21Graficar = aP4_Graficar;
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.AV18AreaTrabalho_Codigo;
         aP1_Contratada_Codigo=this.AV17Contratada_Codigo;
         aP2_Ano=this.AV19Ano;
         aP3_Mes=this.AV20Mes;
         aP4_Graficar=this.AV21Graficar;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV18AreaTrabalho_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18AreaTrabalho_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV17Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0)));
                  AV19Ano = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Ano), 4, 0)));
                  AV20Mes = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Mes), 10, 0)));
                  AV21Graficar = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Graficar", AV21Graficar);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAEJ2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTEJ2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117475140");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("QueryViewer/xpertPivot/swf.js", "");
         context.AddJavascriptSource("QueryViewer/FusionCharts/FusionCharts.js", "");
         context.AddJavascriptSource("QueryViewer/Flash/AC_OETags.js", "");
         context.AddJavascriptSource("QueryViewer/js/highcharts.js", "");
         context.AddJavascriptSource("QueryViewer/js/modules/funnel.js", "");
         context.AddJavascriptSource("QueryViewer/oatPivot/gxpivotjs.js", "");
         context.AddJavascriptSource("QueryViewer/QueryViewerRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_eststmgrf.aspx") + "?" + UrlEncode("" +AV18AreaTrabalho_Codigo) + "," + UrlEncode("" +AV17Contratada_Codigo) + "," + UrlEncode("" +AV19Ano) + "," + UrlEncode("" +AV20Mes) + "," + UrlEncode(StringUtil.RTrim(AV21Graficar))+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPARAMETERS", AV5Parameters);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPARAMETERS", AV5Parameters);
         }
         GxWebStd.gx_hidden_field( context, "vAREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vANO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19Ano), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMES", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20Mes), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRAFICAR", StringUtil.RTrim( AV21Graficar));
         GxWebStd.gx_hidden_field( context, "QUERYVIEWER_Width", StringUtil.RTrim( Queryviewer_Width));
         GxWebStd.gx_hidden_field( context, "QUERYVIEWER_Height", StringUtil.RTrim( Queryviewer_Height));
         GxWebStd.gx_hidden_field( context, "QUERYVIEWER_Type", StringUtil.RTrim( Queryviewer_Type));
         GxWebStd.gx_hidden_field( context, "QUERYVIEWER_Objectname", StringUtil.RTrim( Queryviewer_Objectname));
         GxWebStd.gx_hidden_field( context, "QUERYVIEWER_Autoresize", StringUtil.BoolToStr( Queryviewer_Autoresize));
         GxWebStd.gx_hidden_field( context, "QUERYVIEWER_Disableexpandcollapse", StringUtil.BoolToStr( Queryviewer_Disableexpandcollapse));
         GxWebStd.gx_hidden_field( context, "QUERYVIEWER_Charttype", StringUtil.RTrim( Queryviewer_Charttype));
         GxWebStd.gx_hidden_field( context, "QUERYVIEWER_Rememberlayout", StringUtil.BoolToStr( Queryviewer_Rememberlayout));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEEJ2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTEJ2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_eststmgrf.aspx") + "?" + UrlEncode("" +AV18AreaTrabalho_Codigo) + "," + UrlEncode("" +AV17Contratada_Codigo) + "," + UrlEncode("" +AV19Ano) + "," + UrlEncode("" +AV20Mes) + "," + UrlEncode(StringUtil.RTrim(AV21Graficar)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_EstStmGrf" ;
      }

      public override String GetPgmdesc( )
      {
         return "Estatística" ;
      }

      protected void WBEJ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"QUERYVIEWERContainer"+"\"></div>") ;
         }
         wbLoad = true;
      }

      protected void STARTEJ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Estatística", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPEJ0( ) ;
      }

      protected void WSEJ2( )
      {
         STARTEJ2( ) ;
         EVTEJ2( ) ;
      }

      protected void EVTEJ2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11EJ2 */
                              E11EJ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12EJ2 */
                              E12EJ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEEJ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAEJ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFEJ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFEJ2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E12EJ2 */
            E12EJ2 ();
            WBEJ0( ) ;
         }
      }

      protected void STRUPEJ0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11EJ2 */
         E11EJ2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vPARAMETERS"), AV5Parameters);
            /* Read variables values. */
            /* Read saved values. */
            Queryviewer_Width = cgiGet( "QUERYVIEWER_Width");
            Queryviewer_Height = cgiGet( "QUERYVIEWER_Height");
            Queryviewer_Type = cgiGet( "QUERYVIEWER_Type");
            Queryviewer_Objectname = cgiGet( "QUERYVIEWER_Objectname");
            Queryviewer_Autoresize = StringUtil.StrToBool( cgiGet( "QUERYVIEWER_Autoresize"));
            Queryviewer_Disableexpandcollapse = StringUtil.StrToBool( cgiGet( "QUERYVIEWER_Disableexpandcollapse"));
            Queryviewer_Charttype = cgiGet( "QUERYVIEWER_Charttype");
            Queryviewer_Rememberlayout = StringUtil.StrToBool( cgiGet( "QUERYVIEWER_Rememberlayout"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11EJ2 */
         E11EJ2 ();
         if (returnInSub) return;
      }

      protected void E11EJ2( )
      {
         /* Start Routine */
         /* Execute user subroutine: 'SETQUERYVIEWERPARAMETERS' */
         S112 ();
         if (returnInSub) return;
      }

      protected void S122( )
      {
         /* 'SETOBJECTNAMEEXAMPLE' Routine */
         Queryviewer_Objectname = "Qry_GrfDmnStt";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Queryviewer_Internalname, "ObjectName", Queryviewer_Objectname);
      }

      protected void S112( )
      {
         /* 'SETQUERYVIEWERPARAMETERS' Routine */
         AV5Parameters = new GxObjectCollection( context, "QueryViewerParameters.Parameter", "GxEv3Up14_Meetrika", "SdtQueryViewerParameters_Parameter", "GeneXus.Programs");
         AV6Parameter = new SdtQueryViewerParameters_Parameter(context);
         AV6Parameter.gxTpr_Name = "AreaTrabalho_Codigo";
         AV6Parameter.gxTpr_Value = StringUtil.Str( (decimal)(AV18AreaTrabalho_Codigo), 6, 0);
         AV5Parameters.Add(AV6Parameter, 0);
         AV6Parameter = new SdtQueryViewerParameters_Parameter(context);
         AV6Parameter.gxTpr_Name = "Contratada_Codigo";
         AV6Parameter.gxTpr_Value = StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0);
         AV5Parameters.Add(AV6Parameter, 0);
         AV6Parameter = new SdtQueryViewerParameters_Parameter(context);
         AV6Parameter.gxTpr_Name = "Ano";
         AV6Parameter.gxTpr_Value = StringUtil.Str( (decimal)(AV19Ano), 4, 0);
         AV5Parameters.Add(AV6Parameter, 0);
         AV6Parameter = new SdtQueryViewerParameters_Parameter(context);
         AV6Parameter.gxTpr_Name = "Mes";
         AV6Parameter.gxTpr_Value = StringUtil.Str( (decimal)(AV20Mes), 10, 0);
         AV5Parameters.Add(AV6Parameter, 0);
      }

      protected void S132( )
      {
         /* 'SETAXESCOLLECTIONEXAMPLE' Routine */
      }

      protected void nextLoad( )
      {
      }

      protected void E12EJ2( )
      {
         /* Load Routine */
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV18AreaTrabalho_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18AreaTrabalho_Codigo), 6, 0)));
         AV17Contratada_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratada_Codigo), 6, 0)));
         AV19Ano = Convert.ToInt16(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Ano), 4, 0)));
         AV20Mes = Convert.ToInt64(getParm(obj,3));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Mes), 10, 0)));
         AV21Graficar = (String)getParm(obj,4);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Graficar", AV21Graficar);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAEJ2( ) ;
         WSEJ2( ) ;
         WEEJ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("QueryViewer/QueryViewer.css", "?10720");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311747522");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("wp_eststmgrf.js", "?2020311747522");
            context.AddJavascriptSource("QueryViewer/xpertPivot/swf.js", "");
            context.AddJavascriptSource("QueryViewer/FusionCharts/FusionCharts.js", "");
            context.AddJavascriptSource("QueryViewer/Flash/AC_OETags.js", "");
            context.AddJavascriptSource("QueryViewer/js/highcharts.js", "");
            context.AddJavascriptSource("QueryViewer/js/modules/funnel.js", "");
            context.AddJavascriptSource("QueryViewer/oatPivot/gxpivotjs.js", "");
            context.AddJavascriptSource("QueryViewer/QueryViewerRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         Queryviewer_Internalname = "QUERYVIEWER";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Queryviewer_Rememberlayout = Convert.ToBoolean( 0);
         Queryviewer_Charttype = "Pie";
         Queryviewer_Disableexpandcollapse = Convert.ToBoolean( -1);
         Queryviewer_Autoresize = Convert.ToBoolean( -1);
         Queryviewer_Objectname = "Qry_GrfDmnStt";
         Queryviewer_Type = "Chart";
         Queryviewer_Height = "500";
         Queryviewer_Width = "680";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Estatística";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV21Graficar = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV5Parameters = new GxObjectCollection( context, "QueryViewerParameters.Parameter", "GxEv3Up14_Meetrika", "SdtQueryViewerParameters_Parameter", "GeneXus.Programs");
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV6Parameter = new SdtQueryViewerParameters_Parameter(context);
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV19Ano ;
      private short wcpOAV19Ano ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private int AV18AreaTrabalho_Codigo ;
      private int AV17Contratada_Codigo ;
      private int wcpOAV18AreaTrabalho_Codigo ;
      private int wcpOAV17Contratada_Codigo ;
      private int idxLst ;
      private long AV20Mes ;
      private long wcpOAV20Mes ;
      private String AV21Graficar ;
      private String wcpOAV21Graficar ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Queryviewer_Width ;
      private String Queryviewer_Height ;
      private String Queryviewer_Type ;
      private String Queryviewer_Objectname ;
      private String Queryviewer_Charttype ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Queryviewer_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Queryviewer_Autoresize ;
      private bool Queryviewer_Disableexpandcollapse ;
      private bool Queryviewer_Rememberlayout ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_AreaTrabalho_Codigo ;
      private int aP1_Contratada_Codigo ;
      private short aP2_Ano ;
      private long aP3_Mes ;
      private String aP4_Graficar ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtQueryViewerParameters_Parameter ))]
      private IGxCollection AV5Parameters ;
      private GXWebForm Form ;
      private SdtQueryViewerParameters_Parameter AV6Parameter ;
   }

}
