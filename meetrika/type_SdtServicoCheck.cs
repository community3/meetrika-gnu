/*
               File: type_SdtServicoCheck
        Description: Check Lists
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:30:5.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ServicoCheck" )]
   [XmlType(TypeName =  "ServicoCheck" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtServicoCheck : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtServicoCheck( )
      {
         /* Constructor for serialization */
         gxTv_SdtServicoCheck_Servico_sigla = "";
         gxTv_SdtServicoCheck_Check_nome = "";
         gxTv_SdtServicoCheck_Mode = "";
         gxTv_SdtServicoCheck_Servico_sigla_Z = "";
         gxTv_SdtServicoCheck_Check_nome_Z = "";
      }

      public SdtServicoCheck( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV155Servico_Codigo ,
                        int AV1839Check_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV155Servico_Codigo,(int)AV1839Check_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Servico_Codigo", typeof(int)}, new Object[]{"Check_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ServicoCheck");
         metadata.Set("BT", "ServicoCheck");
         metadata.Set("PK", "[ \"Servico_Codigo\",\"Check_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Check_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Servico_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_sigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Check_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Check_nome_Z" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtServicoCheck deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtServicoCheck)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtServicoCheck obj ;
         obj = this;
         obj.gxTpr_Servico_codigo = deserialized.gxTpr_Servico_codigo;
         obj.gxTpr_Servico_sigla = deserialized.gxTpr_Servico_sigla;
         obj.gxTpr_Check_codigo = deserialized.gxTpr_Check_codigo;
         obj.gxTpr_Check_nome = deserialized.gxTpr_Check_nome;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Servico_codigo_Z = deserialized.gxTpr_Servico_codigo_Z;
         obj.gxTpr_Servico_sigla_Z = deserialized.gxTpr_Servico_sigla_Z;
         obj.gxTpr_Check_codigo_Z = deserialized.gxTpr_Check_codigo_Z;
         obj.gxTpr_Check_nome_Z = deserialized.gxTpr_Check_nome_Z;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Codigo") )
               {
                  gxTv_SdtServicoCheck_Servico_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Sigla") )
               {
                  gxTv_SdtServicoCheck_Servico_sigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Check_Codigo") )
               {
                  gxTv_SdtServicoCheck_Check_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Check_Nome") )
               {
                  gxTv_SdtServicoCheck_Check_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtServicoCheck_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtServicoCheck_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Codigo_Z") )
               {
                  gxTv_SdtServicoCheck_Servico_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Sigla_Z") )
               {
                  gxTv_SdtServicoCheck_Servico_sigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Check_Codigo_Z") )
               {
                  gxTv_SdtServicoCheck_Check_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Check_Nome_Z") )
               {
                  gxTv_SdtServicoCheck_Check_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ServicoCheck";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Servico_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoCheck_Servico_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Servico_Sigla", StringUtil.RTrim( gxTv_SdtServicoCheck_Servico_sigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Check_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoCheck_Check_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Check_Nome", StringUtil.RTrim( gxTv_SdtServicoCheck_Check_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtServicoCheck_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoCheck_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Servico_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoCheck_Servico_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Servico_Sigla_Z", StringUtil.RTrim( gxTv_SdtServicoCheck_Servico_sigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Check_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoCheck_Check_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Check_Nome_Z", StringUtil.RTrim( gxTv_SdtServicoCheck_Check_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Servico_Codigo", gxTv_SdtServicoCheck_Servico_codigo, false);
         AddObjectProperty("Servico_Sigla", gxTv_SdtServicoCheck_Servico_sigla, false);
         AddObjectProperty("Check_Codigo", gxTv_SdtServicoCheck_Check_codigo, false);
         AddObjectProperty("Check_Nome", gxTv_SdtServicoCheck_Check_nome, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtServicoCheck_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtServicoCheck_Initialized, false);
            AddObjectProperty("Servico_Codigo_Z", gxTv_SdtServicoCheck_Servico_codigo_Z, false);
            AddObjectProperty("Servico_Sigla_Z", gxTv_SdtServicoCheck_Servico_sigla_Z, false);
            AddObjectProperty("Check_Codigo_Z", gxTv_SdtServicoCheck_Check_codigo_Z, false);
            AddObjectProperty("Check_Nome_Z", gxTv_SdtServicoCheck_Check_nome_Z, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Servico_Codigo" )]
      [  XmlElement( ElementName = "Servico_Codigo"   )]
      public int gxTpr_Servico_codigo
      {
         get {
            return gxTv_SdtServicoCheck_Servico_codigo ;
         }

         set {
            if ( gxTv_SdtServicoCheck_Servico_codigo != value )
            {
               gxTv_SdtServicoCheck_Mode = "INS";
               this.gxTv_SdtServicoCheck_Servico_codigo_Z_SetNull( );
               this.gxTv_SdtServicoCheck_Servico_sigla_Z_SetNull( );
               this.gxTv_SdtServicoCheck_Check_codigo_Z_SetNull( );
               this.gxTv_SdtServicoCheck_Check_nome_Z_SetNull( );
            }
            gxTv_SdtServicoCheck_Servico_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Servico_Sigla" )]
      [  XmlElement( ElementName = "Servico_Sigla"   )]
      public String gxTpr_Servico_sigla
      {
         get {
            return gxTv_SdtServicoCheck_Servico_sigla ;
         }

         set {
            gxTv_SdtServicoCheck_Servico_sigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Check_Codigo" )]
      [  XmlElement( ElementName = "Check_Codigo"   )]
      public int gxTpr_Check_codigo
      {
         get {
            return gxTv_SdtServicoCheck_Check_codigo ;
         }

         set {
            if ( gxTv_SdtServicoCheck_Check_codigo != value )
            {
               gxTv_SdtServicoCheck_Mode = "INS";
               this.gxTv_SdtServicoCheck_Servico_codigo_Z_SetNull( );
               this.gxTv_SdtServicoCheck_Servico_sigla_Z_SetNull( );
               this.gxTv_SdtServicoCheck_Check_codigo_Z_SetNull( );
               this.gxTv_SdtServicoCheck_Check_nome_Z_SetNull( );
            }
            gxTv_SdtServicoCheck_Check_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Check_Nome" )]
      [  XmlElement( ElementName = "Check_Nome"   )]
      public String gxTpr_Check_nome
      {
         get {
            return gxTv_SdtServicoCheck_Check_nome ;
         }

         set {
            gxTv_SdtServicoCheck_Check_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtServicoCheck_Mode ;
         }

         set {
            gxTv_SdtServicoCheck_Mode = (String)(value);
         }

      }

      public void gxTv_SdtServicoCheck_Mode_SetNull( )
      {
         gxTv_SdtServicoCheck_Mode = "";
         return  ;
      }

      public bool gxTv_SdtServicoCheck_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtServicoCheck_Initialized ;
         }

         set {
            gxTv_SdtServicoCheck_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtServicoCheck_Initialized_SetNull( )
      {
         gxTv_SdtServicoCheck_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtServicoCheck_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Codigo_Z" )]
      [  XmlElement( ElementName = "Servico_Codigo_Z"   )]
      public int gxTpr_Servico_codigo_Z
      {
         get {
            return gxTv_SdtServicoCheck_Servico_codigo_Z ;
         }

         set {
            gxTv_SdtServicoCheck_Servico_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtServicoCheck_Servico_codigo_Z_SetNull( )
      {
         gxTv_SdtServicoCheck_Servico_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServicoCheck_Servico_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Sigla_Z" )]
      [  XmlElement( ElementName = "Servico_Sigla_Z"   )]
      public String gxTpr_Servico_sigla_Z
      {
         get {
            return gxTv_SdtServicoCheck_Servico_sigla_Z ;
         }

         set {
            gxTv_SdtServicoCheck_Servico_sigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtServicoCheck_Servico_sigla_Z_SetNull( )
      {
         gxTv_SdtServicoCheck_Servico_sigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtServicoCheck_Servico_sigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Check_Codigo_Z" )]
      [  XmlElement( ElementName = "Check_Codigo_Z"   )]
      public int gxTpr_Check_codigo_Z
      {
         get {
            return gxTv_SdtServicoCheck_Check_codigo_Z ;
         }

         set {
            gxTv_SdtServicoCheck_Check_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtServicoCheck_Check_codigo_Z_SetNull( )
      {
         gxTv_SdtServicoCheck_Check_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServicoCheck_Check_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Check_Nome_Z" )]
      [  XmlElement( ElementName = "Check_Nome_Z"   )]
      public String gxTpr_Check_nome_Z
      {
         get {
            return gxTv_SdtServicoCheck_Check_nome_Z ;
         }

         set {
            gxTv_SdtServicoCheck_Check_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtServicoCheck_Check_nome_Z_SetNull( )
      {
         gxTv_SdtServicoCheck_Check_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtServicoCheck_Check_nome_Z_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtServicoCheck_Servico_sigla = "";
         gxTv_SdtServicoCheck_Check_nome = "";
         gxTv_SdtServicoCheck_Mode = "";
         gxTv_SdtServicoCheck_Servico_sigla_Z = "";
         gxTv_SdtServicoCheck_Check_nome_Z = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "servicocheck", "GeneXus.Programs.servicocheck_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtServicoCheck_Initialized ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtServicoCheck_Servico_codigo ;
      private int gxTv_SdtServicoCheck_Check_codigo ;
      private int gxTv_SdtServicoCheck_Servico_codigo_Z ;
      private int gxTv_SdtServicoCheck_Check_codigo_Z ;
      private String gxTv_SdtServicoCheck_Servico_sigla ;
      private String gxTv_SdtServicoCheck_Check_nome ;
      private String gxTv_SdtServicoCheck_Mode ;
      private String gxTv_SdtServicoCheck_Servico_sigla_Z ;
      private String gxTv_SdtServicoCheck_Check_nome_Z ;
      private String sTagName ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ServicoCheck", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtServicoCheck_RESTInterface : GxGenericCollectionItem<SdtServicoCheck>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtServicoCheck_RESTInterface( ) : base()
      {
      }

      public SdtServicoCheck_RESTInterface( SdtServicoCheck psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Servico_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servico_codigo
      {
         get {
            return sdt.gxTpr_Servico_codigo ;
         }

         set {
            sdt.gxTpr_Servico_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Servico_Sigla" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Servico_sigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Servico_sigla) ;
         }

         set {
            sdt.gxTpr_Servico_sigla = (String)(value);
         }

      }

      [DataMember( Name = "Check_Codigo" , Order = 2 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Check_codigo
      {
         get {
            return sdt.gxTpr_Check_codigo ;
         }

         set {
            sdt.gxTpr_Check_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Check_Nome" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Check_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Check_nome) ;
         }

         set {
            sdt.gxTpr_Check_nome = (String)(value);
         }

      }

      public SdtServicoCheck sdt
      {
         get {
            return (SdtServicoCheck)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtServicoCheck() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 10 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
