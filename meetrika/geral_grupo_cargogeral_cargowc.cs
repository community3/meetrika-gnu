/*
               File: Geral_Grupo_CargoGeral_CargoWC
        Description: Geral_Grupo_Cargo Geral_Cargo WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:22:15.50
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class geral_grupo_cargogeral_cargowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public geral_grupo_cargogeral_cargowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public geral_grupo_cargogeral_cargowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_GrupoCargo_Codigo )
      {
         this.AV7GrupoCargo_Codigo = aP0_GrupoCargo_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         chkavCargo_ativo = new GXCheckbox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7GrupoCargo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7GrupoCargo_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7GrupoCargo_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_75 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_75_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_75_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV29OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV15DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
                  AV16Cargo_Nome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16Cargo_Nome1", AV16Cargo_Nome1);
                  AV30Cargo_UONom1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30Cargo_UONom1", AV30Cargo_UONom1);
                  AV18DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
                  AV19Cargo_Nome2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Cargo_Nome2", AV19Cargo_Nome2);
                  AV31Cargo_UONom2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31Cargo_UONom2", AV31Cargo_UONom2);
                  AV21DynamicFiltersSelector3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
                  AV22Cargo_Nome3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Cargo_Nome3", AV22Cargo_Nome3);
                  AV32Cargo_UONom3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Cargo_UONom3", AV32Cargo_UONom3);
                  AV17DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
                  AV20DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
                  AV36TFCargo_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFCargo_Nome", AV36TFCargo_Nome);
                  AV37TFCargo_Nome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFCargo_Nome_Sel", AV37TFCargo_Nome_Sel);
                  AV40TFCargo_UONom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFCargo_UONom", AV40TFCargo_UONom);
                  AV41TFCargo_UONom_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFCargo_UONom_Sel", AV41TFCargo_UONom_Sel);
                  AV7GrupoCargo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7GrupoCargo_Codigo), 6, 0)));
                  AV38ddo_Cargo_NomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ddo_Cargo_NomeTitleControlIdToReplace", AV38ddo_Cargo_NomeTitleControlIdToReplace);
                  AV42ddo_Cargo_UONomTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42ddo_Cargo_UONomTitleControlIdToReplace", AV42ddo_Cargo_UONomTitleControlIdToReplace);
                  AV51Pgmname = GetNextPar( );
                  AV28Cargo_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Cargo_Ativo", AV28Cargo_Ativo);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV24DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
                  AV23DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
                  A617Cargo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A1002Cargo_UOCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1002Cargo_UOCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1002Cargo_UOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1002Cargo_UOCod), 6, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV29OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Cargo_Nome1, AV30Cargo_UONom1, AV18DynamicFiltersSelector2, AV19Cargo_Nome2, AV31Cargo_UONom2, AV21DynamicFiltersSelector3, AV22Cargo_Nome3, AV32Cargo_UONom3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV36TFCargo_Nome, AV37TFCargo_Nome_Sel, AV40TFCargo_UONom, AV41TFCargo_UONom_Sel, AV7GrupoCargo_Codigo, AV38ddo_Cargo_NomeTitleControlIdToReplace, AV42ddo_Cargo_UONomTitleControlIdToReplace, AV51Pgmname, AV28Cargo_Ativo, AV11GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A617Cargo_Codigo, A1002Cargo_UOCod, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PACU2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV51Pgmname = "Geral_Grupo_CargoGeral_CargoWC";
               context.Gx_err = 0;
               WSCU2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Geral_Grupo_Cargo Geral_Cargo WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117221597");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("geral_grupo_cargogeral_cargowc.aspx") + "?" + UrlEncode("" +AV7GrupoCargo_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCARGO_NOME1", AV16Cargo_Nome1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCARGO_UONOM1", StringUtil.RTrim( AV30Cargo_UONom1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV18DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCARGO_NOME2", AV19Cargo_Nome2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCARGO_UONOM2", StringUtil.RTrim( AV31Cargo_UONom2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3", AV21DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCARGO_NOME3", AV22Cargo_Nome3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCARGO_UONOM3", StringUtil.RTrim( AV32Cargo_UONom3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV17DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV20DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCARGO_NOME", AV36TFCargo_Nome);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCARGO_NOME_SEL", AV37TFCargo_Nome_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCARGO_UONOM", StringUtil.RTrim( AV40TFCargo_UONom));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCARGO_UONOM_SEL", StringUtil.RTrim( AV41TFCargo_UONom_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_75", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_75), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV43DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV43DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCARGO_NOMETITLEFILTERDATA", AV35Cargo_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCARGO_NOMETITLEFILTERDATA", AV35Cargo_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCARGO_UONOMTITLEFILTERDATA", AV39Cargo_UONomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCARGO_UONOMTITLEFILTERDATA", AV39Cargo_UONomTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7GrupoCargo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRUPOCARGO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7GrupoCargo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV51Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV24DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV23DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, sPrefix+"CARGO_UOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1002Cargo_UOCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Caption", StringUtil.RTrim( Ddo_cargo_nome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Tooltip", StringUtil.RTrim( Ddo_cargo_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Cls", StringUtil.RTrim( Ddo_cargo_nome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_cargo_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_cargo_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_cargo_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_cargo_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_cargo_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_cargo_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_cargo_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_cargo_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Filtertype", StringUtil.RTrim( Ddo_cargo_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_cargo_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_cargo_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Datalisttype", StringUtil.RTrim( Ddo_cargo_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Datalistproc", StringUtil.RTrim( Ddo_cargo_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_cargo_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Sortasc", StringUtil.RTrim( Ddo_cargo_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Sortdsc", StringUtil.RTrim( Ddo_cargo_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Loadingdata", StringUtil.RTrim( Ddo_cargo_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_cargo_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_cargo_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_cargo_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Caption", StringUtil.RTrim( Ddo_cargo_uonom_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Tooltip", StringUtil.RTrim( Ddo_cargo_uonom_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Cls", StringUtil.RTrim( Ddo_cargo_uonom_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Filteredtext_set", StringUtil.RTrim( Ddo_cargo_uonom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Selectedvalue_set", StringUtil.RTrim( Ddo_cargo_uonom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_cargo_uonom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_cargo_uonom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Includesortasc", StringUtil.BoolToStr( Ddo_cargo_uonom_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Includesortdsc", StringUtil.BoolToStr( Ddo_cargo_uonom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Sortedstatus", StringUtil.RTrim( Ddo_cargo_uonom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Includefilter", StringUtil.BoolToStr( Ddo_cargo_uonom_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Filtertype", StringUtil.RTrim( Ddo_cargo_uonom_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Filterisrange", StringUtil.BoolToStr( Ddo_cargo_uonom_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Includedatalist", StringUtil.BoolToStr( Ddo_cargo_uonom_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Datalisttype", StringUtil.RTrim( Ddo_cargo_uonom_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Datalistproc", StringUtil.RTrim( Ddo_cargo_uonom_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_cargo_uonom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Sortasc", StringUtil.RTrim( Ddo_cargo_uonom_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Sortdsc", StringUtil.RTrim( Ddo_cargo_uonom_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Loadingdata", StringUtil.RTrim( Ddo_cargo_uonom_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Cleanfilter", StringUtil.RTrim( Ddo_cargo_uonom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Noresultsfound", StringUtil.RTrim( Ddo_cargo_uonom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Searchbuttontext", StringUtil.RTrim( Ddo_cargo_uonom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_cargo_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_cargo_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_cargo_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Activeeventkey", StringUtil.RTrim( Ddo_cargo_uonom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Filteredtext_get", StringUtil.RTrim( Ddo_cargo_uonom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CARGO_UONOM_Selectedvalue_get", StringUtil.RTrim( Ddo_cargo_uonom_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormCU2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("geral_grupo_cargogeral_cargowc.js", "?20203117221734");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "Geral_Grupo_CargoGeral_CargoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Geral_Grupo_Cargo Geral_Cargo WC" ;
      }

      protected void WBCU0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "geral_grupo_cargogeral_cargowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_CU2( true) ;
         }
         else
         {
            wb_table1_2_CU2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_CU2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtGrupoCargo_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A615GrupoCargo_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A615GrupoCargo_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGrupoCargo_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtGrupoCargo_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV17DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(86, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,86);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(87, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,87);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcargo_nome_Internalname, AV36TFCargo_Nome, StringUtil.RTrim( context.localUtil.Format( AV36TFCargo_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,88);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcargo_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfcargo_nome_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcargo_nome_sel_Internalname, AV37TFCargo_Nome_Sel, StringUtil.RTrim( context.localUtil.Format( AV37TFCargo_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,89);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcargo_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcargo_nome_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcargo_uonom_Internalname, StringUtil.RTrim( AV40TFCargo_UONom), StringUtil.RTrim( context.localUtil.Format( AV40TFCargo_UONom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,90);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcargo_uonom_Jsonclick, 0, "Attribute", "", "", "", edtavTfcargo_uonom_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcargo_uonom_sel_Internalname, StringUtil.RTrim( AV41TFCargo_UONom_Sel), StringUtil.RTrim( context.localUtil.Format( AV41TFCargo_UONom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,91);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcargo_uonom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcargo_uonom_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CARGO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_cargo_nometitlecontrolidtoreplace_Internalname, AV38ddo_Cargo_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,93);\"", 0, edtavDdo_cargo_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CARGO_UONOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_cargo_uonomtitlecontrolidtoreplace_Internalname, AV42ddo_Cargo_UONomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"", 0, edtavDdo_cargo_uonomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTCU2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Geral_Grupo_Cargo Geral_Cargo WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPCU0( ) ;
            }
         }
      }

      protected void WSCU2( )
      {
         STARTCU2( ) ;
         EVTCU2( ) ;
      }

      protected void EVTCU2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPCU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPCU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11CU2 */
                                    E11CU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CARGO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPCU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12CU2 */
                                    E12CU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CARGO_UONOM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPCU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13CU2 */
                                    E13CU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPCU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14CU2 */
                                    E14CU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPCU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15CU2 */
                                    E15CU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPCU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16CU2 */
                                    E16CU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPCU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17CU2 */
                                    E17CU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPCU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18CU2 */
                                    E18CU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPCU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19CU2 */
                                    E19CU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPCU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20CU2 */
                                    E20CU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPCU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21CU2 */
                                    E21CU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPCU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E22CU2 */
                                    E22CU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPCU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E23CU2 */
                                    E23CU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPCU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E24CU2 */
                                    E24CU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPCU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPCU0( ) ;
                              }
                              nGXsfl_75_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_75_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_75_idx), 4, 0)), 4, "0");
                              SubsflControlProps_752( ) ;
                              AV25Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)) ? AV49Update_GXI : context.convertURL( context.PathToRelativeUrl( AV25Update))));
                              AV26Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)) ? AV50Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV26Delete))));
                              A617Cargo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCargo_Codigo_Internalname), ",", "."));
                              A618Cargo_Nome = StringUtil.Upper( cgiGet( edtCargo_Nome_Internalname));
                              A1003Cargo_UONom = StringUtil.Upper( cgiGet( edtCargo_UONom_Internalname));
                              n1003Cargo_UONom = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E25CU2 */
                                          E25CU2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E26CU2 */
                                          E26CU2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E27CU2 */
                                          E27CU2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV29OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Cargo_nome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCARGO_NOME1"), AV16Cargo_Nome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Cargo_uonom1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCARGO_UONOM1"), AV30Cargo_UONom1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Cargo_nome2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCARGO_NOME2"), AV19Cargo_Nome2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Cargo_uonom2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCARGO_UONOM2"), AV31Cargo_UONom2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Cargo_nome3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCARGO_NOME3"), AV22Cargo_Nome3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Cargo_uonom3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCARGO_UONOM3"), AV32Cargo_UONom3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcargo_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCARGO_NOME"), AV36TFCargo_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcargo_nome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCARGO_NOME_SEL"), AV37TFCargo_Nome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcargo_uonom Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCARGO_UONOM"), AV40TFCargo_UONom) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcargo_uonom_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCARGO_UONOM_SEL"), AV41TFCargo_UONom_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPCU0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WECU2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormCU2( ) ;
            }
         }
      }

      protected void PACU2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV29OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV29OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29OrderedBy), 4, 0)));
            }
            chkavCargo_ativo.Name = "vCARGO_ATIVO";
            chkavCargo_ativo.WebTags = "";
            chkavCargo_ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavCargo_ativo_Internalname, "TitleCaption", chkavCargo_ativo.Caption);
            chkavCargo_ativo.CheckedValue = "false";
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CARGO_NOME", "Cargo", 0);
            cmbavDynamicfiltersselector1.addItem("CARGO_UONOM", "Unidade Organizacional", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CARGO_NOME", "Cargo", 0);
            cmbavDynamicfiltersselector2.addItem("CARGO_UONOM", "Unidade Organizacional", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CARGO_NOME", "Cargo", 0);
            cmbavDynamicfiltersselector3.addItem("CARGO_UONOM", "Unidade Organizacional", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_752( ) ;
         while ( nGXsfl_75_idx <= nRC_GXsfl_75 )
         {
            sendrow_752( ) ;
            nGXsfl_75_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_75_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_75_idx+1));
            sGXsfl_75_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_75_idx), 4, 0)), 4, "0");
            SubsflControlProps_752( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV29OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV16Cargo_Nome1 ,
                                       String AV30Cargo_UONom1 ,
                                       String AV18DynamicFiltersSelector2 ,
                                       String AV19Cargo_Nome2 ,
                                       String AV31Cargo_UONom2 ,
                                       String AV21DynamicFiltersSelector3 ,
                                       String AV22Cargo_Nome3 ,
                                       String AV32Cargo_UONom3 ,
                                       bool AV17DynamicFiltersEnabled2 ,
                                       bool AV20DynamicFiltersEnabled3 ,
                                       String AV36TFCargo_Nome ,
                                       String AV37TFCargo_Nome_Sel ,
                                       String AV40TFCargo_UONom ,
                                       String AV41TFCargo_UONom_Sel ,
                                       int AV7GrupoCargo_Codigo ,
                                       String AV38ddo_Cargo_NomeTitleControlIdToReplace ,
                                       String AV42ddo_Cargo_UONomTitleControlIdToReplace ,
                                       String AV51Pgmname ,
                                       bool AV28Cargo_Ativo ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV24DynamicFiltersIgnoreFirst ,
                                       bool AV23DynamicFiltersRemoving ,
                                       int A617Cargo_Codigo ,
                                       int A1002Cargo_UOCod ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFCU2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CARGO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A617Cargo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CARGO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A617Cargo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CARGO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A618Cargo_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CARGO_NOME", A618Cargo_Nome);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV29OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV29OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFCU2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV51Pgmname = "Geral_Grupo_CargoGeral_CargoWC";
         context.Gx_err = 0;
      }

      protected void RFCU2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 75;
         /* Execute user event: E26CU2 */
         E26CU2 ();
         nGXsfl_75_idx = 1;
         sGXsfl_75_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_75_idx), 4, 0)), 4, "0");
         SubsflControlProps_752( ) ;
         nGXsfl_75_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_752( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16Cargo_Nome1 ,
                                                 AV30Cargo_UONom1 ,
                                                 AV17DynamicFiltersEnabled2 ,
                                                 AV18DynamicFiltersSelector2 ,
                                                 AV19Cargo_Nome2 ,
                                                 AV31Cargo_UONom2 ,
                                                 AV20DynamicFiltersEnabled3 ,
                                                 AV21DynamicFiltersSelector3 ,
                                                 AV22Cargo_Nome3 ,
                                                 AV32Cargo_UONom3 ,
                                                 AV37TFCargo_Nome_Sel ,
                                                 AV36TFCargo_Nome ,
                                                 AV41TFCargo_UONom_Sel ,
                                                 AV40TFCargo_UONom ,
                                                 A618Cargo_Nome ,
                                                 A1003Cargo_UONom ,
                                                 AV29OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A615GrupoCargo_Codigo ,
                                                 AV7GrupoCargo_Codigo ,
                                                 A628Cargo_Ativo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            lV16Cargo_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV16Cargo_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16Cargo_Nome1", AV16Cargo_Nome1);
            lV30Cargo_UONom1 = StringUtil.PadR( StringUtil.RTrim( AV30Cargo_UONom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30Cargo_UONom1", AV30Cargo_UONom1);
            lV19Cargo_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV19Cargo_Nome2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Cargo_Nome2", AV19Cargo_Nome2);
            lV31Cargo_UONom2 = StringUtil.PadR( StringUtil.RTrim( AV31Cargo_UONom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31Cargo_UONom2", AV31Cargo_UONom2);
            lV22Cargo_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV22Cargo_Nome3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Cargo_Nome3", AV22Cargo_Nome3);
            lV32Cargo_UONom3 = StringUtil.PadR( StringUtil.RTrim( AV32Cargo_UONom3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Cargo_UONom3", AV32Cargo_UONom3);
            lV36TFCargo_Nome = StringUtil.Concat( StringUtil.RTrim( AV36TFCargo_Nome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFCargo_Nome", AV36TFCargo_Nome);
            lV40TFCargo_UONom = StringUtil.PadR( StringUtil.RTrim( AV40TFCargo_UONom), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFCargo_UONom", AV40TFCargo_UONom);
            /* Using cursor H00CU2 */
            pr_default.execute(0, new Object[] {AV7GrupoCargo_Codigo, lV16Cargo_Nome1, lV30Cargo_UONom1, lV19Cargo_Nome2, lV31Cargo_UONom2, lV22Cargo_Nome3, lV32Cargo_UONom3, lV36TFCargo_Nome, AV37TFCargo_Nome_Sel, lV40TFCargo_UONom, AV41TFCargo_UONom_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_75_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A628Cargo_Ativo = H00CU2_A628Cargo_Ativo[0];
               A1002Cargo_UOCod = H00CU2_A1002Cargo_UOCod[0];
               n1002Cargo_UOCod = H00CU2_n1002Cargo_UOCod[0];
               A615GrupoCargo_Codigo = H00CU2_A615GrupoCargo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
               A1003Cargo_UONom = H00CU2_A1003Cargo_UONom[0];
               n1003Cargo_UONom = H00CU2_n1003Cargo_UONom[0];
               A618Cargo_Nome = H00CU2_A618Cargo_Nome[0];
               A617Cargo_Codigo = H00CU2_A617Cargo_Codigo[0];
               A1003Cargo_UONom = H00CU2_A1003Cargo_UONom[0];
               n1003Cargo_UONom = H00CU2_n1003Cargo_UONom[0];
               /* Execute user event: E27CU2 */
               E27CU2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 75;
            WBCU0( ) ;
         }
         nGXsfl_75_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16Cargo_Nome1 ,
                                              AV30Cargo_UONom1 ,
                                              AV17DynamicFiltersEnabled2 ,
                                              AV18DynamicFiltersSelector2 ,
                                              AV19Cargo_Nome2 ,
                                              AV31Cargo_UONom2 ,
                                              AV20DynamicFiltersEnabled3 ,
                                              AV21DynamicFiltersSelector3 ,
                                              AV22Cargo_Nome3 ,
                                              AV32Cargo_UONom3 ,
                                              AV37TFCargo_Nome_Sel ,
                                              AV36TFCargo_Nome ,
                                              AV41TFCargo_UONom_Sel ,
                                              AV40TFCargo_UONom ,
                                              A618Cargo_Nome ,
                                              A1003Cargo_UONom ,
                                              AV29OrderedBy ,
                                              AV14OrderedDsc ,
                                              A615GrupoCargo_Codigo ,
                                              AV7GrupoCargo_Codigo ,
                                              A628Cargo_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV16Cargo_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV16Cargo_Nome1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16Cargo_Nome1", AV16Cargo_Nome1);
         lV30Cargo_UONom1 = StringUtil.PadR( StringUtil.RTrim( AV30Cargo_UONom1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30Cargo_UONom1", AV30Cargo_UONom1);
         lV19Cargo_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV19Cargo_Nome2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Cargo_Nome2", AV19Cargo_Nome2);
         lV31Cargo_UONom2 = StringUtil.PadR( StringUtil.RTrim( AV31Cargo_UONom2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31Cargo_UONom2", AV31Cargo_UONom2);
         lV22Cargo_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV22Cargo_Nome3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Cargo_Nome3", AV22Cargo_Nome3);
         lV32Cargo_UONom3 = StringUtil.PadR( StringUtil.RTrim( AV32Cargo_UONom3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Cargo_UONom3", AV32Cargo_UONom3);
         lV36TFCargo_Nome = StringUtil.Concat( StringUtil.RTrim( AV36TFCargo_Nome), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFCargo_Nome", AV36TFCargo_Nome);
         lV40TFCargo_UONom = StringUtil.PadR( StringUtil.RTrim( AV40TFCargo_UONom), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFCargo_UONom", AV40TFCargo_UONom);
         /* Using cursor H00CU3 */
         pr_default.execute(1, new Object[] {AV7GrupoCargo_Codigo, lV16Cargo_Nome1, lV30Cargo_UONom1, lV19Cargo_Nome2, lV31Cargo_UONom2, lV22Cargo_Nome3, lV32Cargo_UONom3, lV36TFCargo_Nome, AV37TFCargo_Nome_Sel, lV40TFCargo_UONom, AV41TFCargo_UONom_Sel});
         GRID_nRecordCount = H00CU3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV29OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Cargo_Nome1, AV30Cargo_UONom1, AV18DynamicFiltersSelector2, AV19Cargo_Nome2, AV31Cargo_UONom2, AV21DynamicFiltersSelector3, AV22Cargo_Nome3, AV32Cargo_UONom3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV36TFCargo_Nome, AV37TFCargo_Nome_Sel, AV40TFCargo_UONom, AV41TFCargo_UONom_Sel, AV7GrupoCargo_Codigo, AV38ddo_Cargo_NomeTitleControlIdToReplace, AV42ddo_Cargo_UONomTitleControlIdToReplace, AV51Pgmname, AV28Cargo_Ativo, AV11GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A617Cargo_Codigo, A1002Cargo_UOCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV29OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Cargo_Nome1, AV30Cargo_UONom1, AV18DynamicFiltersSelector2, AV19Cargo_Nome2, AV31Cargo_UONom2, AV21DynamicFiltersSelector3, AV22Cargo_Nome3, AV32Cargo_UONom3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV36TFCargo_Nome, AV37TFCargo_Nome_Sel, AV40TFCargo_UONom, AV41TFCargo_UONom_Sel, AV7GrupoCargo_Codigo, AV38ddo_Cargo_NomeTitleControlIdToReplace, AV42ddo_Cargo_UONomTitleControlIdToReplace, AV51Pgmname, AV28Cargo_Ativo, AV11GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A617Cargo_Codigo, A1002Cargo_UOCod, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV29OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Cargo_Nome1, AV30Cargo_UONom1, AV18DynamicFiltersSelector2, AV19Cargo_Nome2, AV31Cargo_UONom2, AV21DynamicFiltersSelector3, AV22Cargo_Nome3, AV32Cargo_UONom3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV36TFCargo_Nome, AV37TFCargo_Nome_Sel, AV40TFCargo_UONom, AV41TFCargo_UONom_Sel, AV7GrupoCargo_Codigo, AV38ddo_Cargo_NomeTitleControlIdToReplace, AV42ddo_Cargo_UONomTitleControlIdToReplace, AV51Pgmname, AV28Cargo_Ativo, AV11GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A617Cargo_Codigo, A1002Cargo_UOCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV29OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Cargo_Nome1, AV30Cargo_UONom1, AV18DynamicFiltersSelector2, AV19Cargo_Nome2, AV31Cargo_UONom2, AV21DynamicFiltersSelector3, AV22Cargo_Nome3, AV32Cargo_UONom3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV36TFCargo_Nome, AV37TFCargo_Nome_Sel, AV40TFCargo_UONom, AV41TFCargo_UONom_Sel, AV7GrupoCargo_Codigo, AV38ddo_Cargo_NomeTitleControlIdToReplace, AV42ddo_Cargo_UONomTitleControlIdToReplace, AV51Pgmname, AV28Cargo_Ativo, AV11GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A617Cargo_Codigo, A1002Cargo_UOCod, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV29OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Cargo_Nome1, AV30Cargo_UONom1, AV18DynamicFiltersSelector2, AV19Cargo_Nome2, AV31Cargo_UONom2, AV21DynamicFiltersSelector3, AV22Cargo_Nome3, AV32Cargo_UONom3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV36TFCargo_Nome, AV37TFCargo_Nome_Sel, AV40TFCargo_UONom, AV41TFCargo_UONom_Sel, AV7GrupoCargo_Codigo, AV38ddo_Cargo_NomeTitleControlIdToReplace, AV42ddo_Cargo_UONomTitleControlIdToReplace, AV51Pgmname, AV28Cargo_Ativo, AV11GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A617Cargo_Codigo, A1002Cargo_UOCod, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPCU0( )
      {
         /* Before Start, stand alone formulas. */
         AV51Pgmname = "Geral_Grupo_CargoGeral_CargoWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E25CU2 */
         E25CU2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV43DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCARGO_NOMETITLEFILTERDATA"), AV35Cargo_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCARGO_UONOMTITLEFILTERDATA"), AV39Cargo_UONomTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV29OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            AV28Cargo_Ativo = StringUtil.StrToBool( cgiGet( chkavCargo_ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Cargo_Ativo", AV28Cargo_Ativo);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV16Cargo_Nome1 = StringUtil.Upper( cgiGet( edtavCargo_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16Cargo_Nome1", AV16Cargo_Nome1);
            AV30Cargo_UONom1 = StringUtil.Upper( cgiGet( edtavCargo_uonom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30Cargo_UONom1", AV30Cargo_UONom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV18DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            AV19Cargo_Nome2 = StringUtil.Upper( cgiGet( edtavCargo_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Cargo_Nome2", AV19Cargo_Nome2);
            AV31Cargo_UONom2 = StringUtil.Upper( cgiGet( edtavCargo_uonom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31Cargo_UONom2", AV31Cargo_UONom2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV21DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            AV22Cargo_Nome3 = StringUtil.Upper( cgiGet( edtavCargo_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Cargo_Nome3", AV22Cargo_Nome3);
            AV32Cargo_UONom3 = StringUtil.Upper( cgiGet( edtavCargo_uonom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Cargo_UONom3", AV32Cargo_UONom3);
            A615GrupoCargo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGrupoCargo_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A615GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A615GrupoCargo_Codigo), 6, 0)));
            AV17DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
            AV20DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
            AV36TFCargo_Nome = StringUtil.Upper( cgiGet( edtavTfcargo_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFCargo_Nome", AV36TFCargo_Nome);
            AV37TFCargo_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfcargo_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFCargo_Nome_Sel", AV37TFCargo_Nome_Sel);
            AV40TFCargo_UONom = StringUtil.Upper( cgiGet( edtavTfcargo_uonom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFCargo_UONom", AV40TFCargo_UONom);
            AV41TFCargo_UONom_Sel = StringUtil.Upper( cgiGet( edtavTfcargo_uonom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFCargo_UONom_Sel", AV41TFCargo_UONom_Sel);
            AV38ddo_Cargo_NomeTitleControlIdToReplace = cgiGet( edtavDdo_cargo_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ddo_Cargo_NomeTitleControlIdToReplace", AV38ddo_Cargo_NomeTitleControlIdToReplace);
            AV42ddo_Cargo_UONomTitleControlIdToReplace = cgiGet( edtavDdo_cargo_uonomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42ddo_Cargo_UONomTitleControlIdToReplace", AV42ddo_Cargo_UONomTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_75 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_75"), ",", "."));
            AV45GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV46GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7GrupoCargo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7GrupoCargo_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_cargo_nome_Caption = cgiGet( sPrefix+"DDO_CARGO_NOME_Caption");
            Ddo_cargo_nome_Tooltip = cgiGet( sPrefix+"DDO_CARGO_NOME_Tooltip");
            Ddo_cargo_nome_Cls = cgiGet( sPrefix+"DDO_CARGO_NOME_Cls");
            Ddo_cargo_nome_Filteredtext_set = cgiGet( sPrefix+"DDO_CARGO_NOME_Filteredtext_set");
            Ddo_cargo_nome_Selectedvalue_set = cgiGet( sPrefix+"DDO_CARGO_NOME_Selectedvalue_set");
            Ddo_cargo_nome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CARGO_NOME_Dropdownoptionstype");
            Ddo_cargo_nome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CARGO_NOME_Titlecontrolidtoreplace");
            Ddo_cargo_nome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CARGO_NOME_Includesortasc"));
            Ddo_cargo_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CARGO_NOME_Includesortdsc"));
            Ddo_cargo_nome_Sortedstatus = cgiGet( sPrefix+"DDO_CARGO_NOME_Sortedstatus");
            Ddo_cargo_nome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CARGO_NOME_Includefilter"));
            Ddo_cargo_nome_Filtertype = cgiGet( sPrefix+"DDO_CARGO_NOME_Filtertype");
            Ddo_cargo_nome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CARGO_NOME_Filterisrange"));
            Ddo_cargo_nome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CARGO_NOME_Includedatalist"));
            Ddo_cargo_nome_Datalisttype = cgiGet( sPrefix+"DDO_CARGO_NOME_Datalisttype");
            Ddo_cargo_nome_Datalistproc = cgiGet( sPrefix+"DDO_CARGO_NOME_Datalistproc");
            Ddo_cargo_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CARGO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_cargo_nome_Sortasc = cgiGet( sPrefix+"DDO_CARGO_NOME_Sortasc");
            Ddo_cargo_nome_Sortdsc = cgiGet( sPrefix+"DDO_CARGO_NOME_Sortdsc");
            Ddo_cargo_nome_Loadingdata = cgiGet( sPrefix+"DDO_CARGO_NOME_Loadingdata");
            Ddo_cargo_nome_Cleanfilter = cgiGet( sPrefix+"DDO_CARGO_NOME_Cleanfilter");
            Ddo_cargo_nome_Noresultsfound = cgiGet( sPrefix+"DDO_CARGO_NOME_Noresultsfound");
            Ddo_cargo_nome_Searchbuttontext = cgiGet( sPrefix+"DDO_CARGO_NOME_Searchbuttontext");
            Ddo_cargo_uonom_Caption = cgiGet( sPrefix+"DDO_CARGO_UONOM_Caption");
            Ddo_cargo_uonom_Tooltip = cgiGet( sPrefix+"DDO_CARGO_UONOM_Tooltip");
            Ddo_cargo_uonom_Cls = cgiGet( sPrefix+"DDO_CARGO_UONOM_Cls");
            Ddo_cargo_uonom_Filteredtext_set = cgiGet( sPrefix+"DDO_CARGO_UONOM_Filteredtext_set");
            Ddo_cargo_uonom_Selectedvalue_set = cgiGet( sPrefix+"DDO_CARGO_UONOM_Selectedvalue_set");
            Ddo_cargo_uonom_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CARGO_UONOM_Dropdownoptionstype");
            Ddo_cargo_uonom_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CARGO_UONOM_Titlecontrolidtoreplace");
            Ddo_cargo_uonom_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CARGO_UONOM_Includesortasc"));
            Ddo_cargo_uonom_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CARGO_UONOM_Includesortdsc"));
            Ddo_cargo_uonom_Sortedstatus = cgiGet( sPrefix+"DDO_CARGO_UONOM_Sortedstatus");
            Ddo_cargo_uonom_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CARGO_UONOM_Includefilter"));
            Ddo_cargo_uonom_Filtertype = cgiGet( sPrefix+"DDO_CARGO_UONOM_Filtertype");
            Ddo_cargo_uonom_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CARGO_UONOM_Filterisrange"));
            Ddo_cargo_uonom_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CARGO_UONOM_Includedatalist"));
            Ddo_cargo_uonom_Datalisttype = cgiGet( sPrefix+"DDO_CARGO_UONOM_Datalisttype");
            Ddo_cargo_uonom_Datalistproc = cgiGet( sPrefix+"DDO_CARGO_UONOM_Datalistproc");
            Ddo_cargo_uonom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CARGO_UONOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_cargo_uonom_Sortasc = cgiGet( sPrefix+"DDO_CARGO_UONOM_Sortasc");
            Ddo_cargo_uonom_Sortdsc = cgiGet( sPrefix+"DDO_CARGO_UONOM_Sortdsc");
            Ddo_cargo_uonom_Loadingdata = cgiGet( sPrefix+"DDO_CARGO_UONOM_Loadingdata");
            Ddo_cargo_uonom_Cleanfilter = cgiGet( sPrefix+"DDO_CARGO_UONOM_Cleanfilter");
            Ddo_cargo_uonom_Noresultsfound = cgiGet( sPrefix+"DDO_CARGO_UONOM_Noresultsfound");
            Ddo_cargo_uonom_Searchbuttontext = cgiGet( sPrefix+"DDO_CARGO_UONOM_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_cargo_nome_Activeeventkey = cgiGet( sPrefix+"DDO_CARGO_NOME_Activeeventkey");
            Ddo_cargo_nome_Filteredtext_get = cgiGet( sPrefix+"DDO_CARGO_NOME_Filteredtext_get");
            Ddo_cargo_nome_Selectedvalue_get = cgiGet( sPrefix+"DDO_CARGO_NOME_Selectedvalue_get");
            Ddo_cargo_uonom_Activeeventkey = cgiGet( sPrefix+"DDO_CARGO_UONOM_Activeeventkey");
            Ddo_cargo_uonom_Filteredtext_get = cgiGet( sPrefix+"DDO_CARGO_UONOM_Filteredtext_get");
            Ddo_cargo_uonom_Selectedvalue_get = cgiGet( sPrefix+"DDO_CARGO_UONOM_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV29OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCARGO_NOME1"), AV16Cargo_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCARGO_UONOM1"), AV30Cargo_UONom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCARGO_NOME2"), AV19Cargo_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCARGO_UONOM2"), AV31Cargo_UONom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCARGO_NOME3"), AV22Cargo_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCARGO_UONOM3"), AV32Cargo_UONom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCARGO_NOME"), AV36TFCargo_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCARGO_NOME_SEL"), AV37TFCargo_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCARGO_UONOM"), AV40TFCargo_UONom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCARGO_UONOM_SEL"), AV41TFCargo_UONom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E25CU2 */
         E25CU2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25CU2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CARGO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV18DynamicFiltersSelector2 = "CARGO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector3 = "CARGO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcargo_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcargo_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcargo_nome_Visible), 5, 0)));
         edtavTfcargo_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcargo_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcargo_nome_sel_Visible), 5, 0)));
         edtavTfcargo_uonom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcargo_uonom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcargo_uonom_Visible), 5, 0)));
         edtavTfcargo_uonom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcargo_uonom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcargo_uonom_sel_Visible), 5, 0)));
         Ddo_cargo_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Cargo_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_nome_Internalname, "TitleControlIdToReplace", Ddo_cargo_nome_Titlecontrolidtoreplace);
         AV38ddo_Cargo_NomeTitleControlIdToReplace = Ddo_cargo_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38ddo_Cargo_NomeTitleControlIdToReplace", AV38ddo_Cargo_NomeTitleControlIdToReplace);
         edtavDdo_cargo_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_cargo_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_cargo_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_cargo_uonom_Titlecontrolidtoreplace = subGrid_Internalname+"_Cargo_UONom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_uonom_Internalname, "TitleControlIdToReplace", Ddo_cargo_uonom_Titlecontrolidtoreplace);
         AV42ddo_Cargo_UONomTitleControlIdToReplace = Ddo_cargo_uonom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42ddo_Cargo_UONomTitleControlIdToReplace", AV42ddo_Cargo_UONomTitleControlIdToReplace);
         edtavDdo_cargo_uonomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_cargo_uonomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_cargo_uonomtitlecontrolidtoreplace_Visible), 5, 0)));
         edtGrupoCargo_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtGrupoCargo_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGrupoCargo_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Cargo", 0);
         cmbavOrderedby.addItem("2", "Unidade Organizacional", 0);
         if ( AV29OrderedBy < 1 )
         {
            AV29OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV43DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV43DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E26CU2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV35Cargo_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39Cargo_UONomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtCargo_Nome_Titleformat = 2;
         edtCargo_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV38ddo_Cargo_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtCargo_Nome_Internalname, "Title", edtCargo_Nome_Title);
         edtCargo_UONom_Titleformat = 2;
         edtCargo_UONom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Unidade Organizacional", AV42ddo_Cargo_UONomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtCargo_UONom_Internalname, "Title", edtCargo_UONom_Title);
         AV45GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45GridCurrentPage), 10, 0)));
         AV46GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV35Cargo_NomeTitleFilterData", AV35Cargo_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV39Cargo_UONomTitleFilterData", AV39Cargo_UONomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11CU2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV44PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV44PageToGo) ;
         }
      }

      protected void E12CU2( )
      {
         /* Ddo_cargo_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_cargo_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV29OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_cargo_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_nome_Internalname, "SortedStatus", Ddo_cargo_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_cargo_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV29OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_cargo_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_nome_Internalname, "SortedStatus", Ddo_cargo_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_cargo_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV36TFCargo_Nome = Ddo_cargo_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFCargo_Nome", AV36TFCargo_Nome);
            AV37TFCargo_Nome_Sel = Ddo_cargo_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFCargo_Nome_Sel", AV37TFCargo_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13CU2( )
      {
         /* Ddo_cargo_uonom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_cargo_uonom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV29OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_cargo_uonom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_uonom_Internalname, "SortedStatus", Ddo_cargo_uonom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_cargo_uonom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV29OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_cargo_uonom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_uonom_Internalname, "SortedStatus", Ddo_cargo_uonom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_cargo_uonom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV40TFCargo_UONom = Ddo_cargo_uonom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFCargo_UONom", AV40TFCargo_UONom);
            AV41TFCargo_UONom_Sel = Ddo_cargo_uonom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFCargo_UONom_Sel", AV41TFCargo_UONom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E27CU2( )
      {
         /* Grid_Load Routine */
         AV25Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV25Update);
         AV49Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("geral_cargo.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A617Cargo_Codigo);
         AV26Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV26Delete);
         AV50Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("geral_cargo.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A617Cargo_Codigo);
         edtCargo_Nome_Link = formatLink("viewgeral_cargo.aspx") + "?" + UrlEncode("" +A617Cargo_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtCargo_UONom_Link = formatLink("viewgeral_unidadeorganizacional.aspx") + "?" + UrlEncode("" +A1002Cargo_UOCod) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 75;
         }
         sendrow_752( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_75_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(75, GridRow);
         }
      }

      protected void E14CU2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E20CU2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV17DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E15CU2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV29OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Cargo_Nome1, AV30Cargo_UONom1, AV18DynamicFiltersSelector2, AV19Cargo_Nome2, AV31Cargo_UONom2, AV21DynamicFiltersSelector3, AV22Cargo_Nome3, AV32Cargo_UONom3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV36TFCargo_Nome, AV37TFCargo_Nome_Sel, AV40TFCargo_UONom, AV41TFCargo_UONom_Sel, AV7GrupoCargo_Codigo, AV38ddo_Cargo_NomeTitleControlIdToReplace, AV42ddo_Cargo_UONomTitleControlIdToReplace, AV51Pgmname, AV28Cargo_Ativo, AV11GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A617Cargo_Codigo, A1002Cargo_UOCod, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E21CU2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22CU2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV20DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E16CU2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV29OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Cargo_Nome1, AV30Cargo_UONom1, AV18DynamicFiltersSelector2, AV19Cargo_Nome2, AV31Cargo_UONom2, AV21DynamicFiltersSelector3, AV22Cargo_Nome3, AV32Cargo_UONom3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV36TFCargo_Nome, AV37TFCargo_Nome_Sel, AV40TFCargo_UONom, AV41TFCargo_UONom_Sel, AV7GrupoCargo_Codigo, AV38ddo_Cargo_NomeTitleControlIdToReplace, AV42ddo_Cargo_UONomTitleControlIdToReplace, AV51Pgmname, AV28Cargo_Ativo, AV11GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A617Cargo_Codigo, A1002Cargo_UOCod, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E23CU2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17CU2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV29OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Cargo_Nome1, AV30Cargo_UONom1, AV18DynamicFiltersSelector2, AV19Cargo_Nome2, AV31Cargo_UONom2, AV21DynamicFiltersSelector3, AV22Cargo_Nome3, AV32Cargo_UONom3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV36TFCargo_Nome, AV37TFCargo_Nome_Sel, AV40TFCargo_UONom, AV41TFCargo_UONom_Sel, AV7GrupoCargo_Codigo, AV38ddo_Cargo_NomeTitleControlIdToReplace, AV42ddo_Cargo_UONomTitleControlIdToReplace, AV51Pgmname, AV28Cargo_Ativo, AV11GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A617Cargo_Codigo, A1002Cargo_UOCod, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E24CU2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18CU2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void E19CU2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("geral_cargo.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_cargo_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_nome_Internalname, "SortedStatus", Ddo_cargo_nome_Sortedstatus);
         Ddo_cargo_uonom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_uonom_Internalname, "SortedStatus", Ddo_cargo_uonom_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV29OrderedBy == 1 )
         {
            Ddo_cargo_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_nome_Internalname, "SortedStatus", Ddo_cargo_nome_Sortedstatus);
         }
         else if ( AV29OrderedBy == 2 )
         {
            Ddo_cargo_uonom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_uonom_Internalname, "SortedStatus", Ddo_cargo_uonom_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavCargo_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCargo_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_nome1_Visible), 5, 0)));
         edtavCargo_uonom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCargo_uonom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_uonom1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CARGO_NOME") == 0 )
         {
            edtavCargo_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCargo_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CARGO_UONOM") == 0 )
         {
            edtavCargo_uonom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCargo_uonom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_uonom1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavCargo_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCargo_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_nome2_Visible), 5, 0)));
         edtavCargo_uonom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCargo_uonom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_uonom2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CARGO_NOME") == 0 )
         {
            edtavCargo_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCargo_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CARGO_UONOM") == 0 )
         {
            edtavCargo_uonom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCargo_uonom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_uonom2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavCargo_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCargo_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_nome3_Visible), 5, 0)));
         edtavCargo_uonom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCargo_uonom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_uonom3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CARGO_NOME") == 0 )
         {
            edtavCargo_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCargo_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_nome3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CARGO_UONOM") == 0 )
         {
            edtavCargo_uonom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCargo_uonom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCargo_uonom3_Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         AV18DynamicFiltersSelector2 = "CARGO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         AV19Cargo_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Cargo_Nome2", AV19Cargo_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         AV21DynamicFiltersSelector3 = "CARGO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         AV22Cargo_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Cargo_Nome3", AV22Cargo_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV28Cargo_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Cargo_Ativo", AV28Cargo_Ativo);
         AV36TFCargo_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFCargo_Nome", AV36TFCargo_Nome);
         Ddo_cargo_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_nome_Internalname, "FilteredText_set", Ddo_cargo_nome_Filteredtext_set);
         AV37TFCargo_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFCargo_Nome_Sel", AV37TFCargo_Nome_Sel);
         Ddo_cargo_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_nome_Internalname, "SelectedValue_set", Ddo_cargo_nome_Selectedvalue_set);
         AV40TFCargo_UONom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFCargo_UONom", AV40TFCargo_UONom);
         Ddo_cargo_uonom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_uonom_Internalname, "FilteredText_set", Ddo_cargo_uonom_Filteredtext_set);
         AV41TFCargo_UONom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFCargo_UONom_Sel", AV41TFCargo_UONom_Sel);
         Ddo_cargo_uonom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_uonom_Internalname, "SelectedValue_set", Ddo_cargo_uonom_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "CARGO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16Cargo_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16Cargo_Nome1", AV16Cargo_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get(AV51Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV51Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV27Session.Get(AV51Pgmname+"GridState"), "");
         }
         AV29OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV52GXV1 = 1;
         while ( AV52GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV52GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "CARGO_ATIVO") == 0 )
            {
               AV28Cargo_Ativo = BooleanUtil.Val( AV12GridStateFilterValue.gxTpr_Value);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Cargo_Ativo", AV28Cargo_Ativo);
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCARGO_NOME") == 0 )
            {
               AV36TFCargo_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36TFCargo_Nome", AV36TFCargo_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFCargo_Nome)) )
               {
                  Ddo_cargo_nome_Filteredtext_set = AV36TFCargo_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_nome_Internalname, "FilteredText_set", Ddo_cargo_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCARGO_NOME_SEL") == 0 )
            {
               AV37TFCargo_Nome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37TFCargo_Nome_Sel", AV37TFCargo_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFCargo_Nome_Sel)) )
               {
                  Ddo_cargo_nome_Selectedvalue_set = AV37TFCargo_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_nome_Internalname, "SelectedValue_set", Ddo_cargo_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCARGO_UONOM") == 0 )
            {
               AV40TFCargo_UONom = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFCargo_UONom", AV40TFCargo_UONom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFCargo_UONom)) )
               {
                  Ddo_cargo_uonom_Filteredtext_set = AV40TFCargo_UONom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_uonom_Internalname, "FilteredText_set", Ddo_cargo_uonom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCARGO_UONOM_SEL") == 0 )
            {
               AV41TFCargo_UONom_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFCargo_UONom_Sel", AV41TFCargo_UONom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFCargo_UONom_Sel)) )
               {
                  Ddo_cargo_uonom_Selectedvalue_set = AV41TFCargo_UONom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_cargo_uonom_Internalname, "SelectedValue_set", Ddo_cargo_uonom_Selectedvalue_set);
               }
            }
            AV52GXV1 = (int)(AV52GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CARGO_NOME") == 0 )
            {
               AV16Cargo_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16Cargo_Nome1", AV16Cargo_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CARGO_UONOM") == 0 )
            {
               AV30Cargo_UONom1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30Cargo_UONom1", AV30Cargo_UONom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV17DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV18DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CARGO_NOME") == 0 )
               {
                  AV19Cargo_Nome2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Cargo_Nome2", AV19Cargo_Nome2);
               }
               else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CARGO_UONOM") == 0 )
               {
                  AV31Cargo_UONom2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31Cargo_UONom2", AV31Cargo_UONom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV20DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV21DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CARGO_NOME") == 0 )
                  {
                     AV22Cargo_Nome3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22Cargo_Nome3", AV22Cargo_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CARGO_UONOM") == 0 )
                  {
                     AV32Cargo_UONom3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Cargo_UONom3", AV32Cargo_UONom3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV23DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV27Session.Get(AV51Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV29OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! (false==AV28Cargo_Ativo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "CARGO_ATIVO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.BoolToStr( AV28Cargo_Ativo);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFCargo_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCARGO_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV36TFCargo_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFCargo_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCARGO_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV37TFCargo_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFCargo_UONom)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCARGO_UONOM";
            AV12GridStateFilterValue.gxTpr_Value = AV40TFCargo_UONom;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFCargo_UONom_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCARGO_UONOM_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV41TFCargo_UONom_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7GrupoCargo_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&GRUPOCARGO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7GrupoCargo_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV51Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV24DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CARGO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV16Cargo_Nome1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV16Cargo_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CARGO_UONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV30Cargo_UONom1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV30Cargo_UONom1;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV17DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV18DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CARGO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Cargo_Nome2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV19Cargo_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CARGO_UONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Cargo_UONom2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV31Cargo_UONom2;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CARGO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Cargo_Nome3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV22Cargo_Nome3;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CARGO_UONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV32Cargo_UONom3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV32Cargo_UONom3;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV51Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Geral_Cargo";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "GrupoCargo_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7GrupoCargo_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV27Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_CU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_CU2( true) ;
         }
         else
         {
            wb_table2_8_CU2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_CU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table3_72_CU2( true) ;
         }
         else
         {
            wb_table3_72_CU2( false) ;
         }
         return  ;
      }

      protected void wb_table3_72_CU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_CU2e( true) ;
         }
         else
         {
            wb_table1_2_CU2e( false) ;
         }
      }

      protected void wb_table3_72_CU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"75\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Cargo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCargo_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtCargo_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCargo_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCargo_UONom_Titleformat == 0 )
               {
                  context.SendWebValue( edtCargo_UONom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCargo_UONom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV25Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV26Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A617Cargo_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A618Cargo_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCargo_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCargo_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtCargo_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1003Cargo_UONom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCargo_UONom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCargo_UONom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtCargo_UONom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 75 )
         {
            wbEnd = 0;
            nRC_GXsfl_75 = (short)(nGXsfl_75_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_72_CU2e( true) ;
         }
         else
         {
            wb_table3_72_CU2e( false) ;
         }
      }

      protected void wb_table2_8_CU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table4_11_CU2( true) ;
         }
         else
         {
            wb_table4_11_CU2( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_CU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV29OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV29OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_21_CU2( true) ;
         }
         else
         {
            wb_table5_21_CU2( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_CU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_CU2e( true) ;
         }
         else
         {
            wb_table2_8_CU2e( false) ;
         }
      }

      protected void wb_table5_21_CU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcargo_ativo_Internalname, "Ativo?", "", "", lblFiltertextcargo_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavCargo_ativo_Internalname, StringUtil.BoolToStr( AV28Cargo_Ativo), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(28, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_30_CU2( true) ;
         }
         else
         {
            wb_table6_30_CU2( false) ;
         }
         return  ;
      }

      protected void wb_table6_30_CU2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_CU2e( true) ;
         }
         else
         {
            wb_table5_21_CU2e( false) ;
         }
      }

      protected void wb_table6_30_CU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "", true, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCargo_nome1_Internalname, AV16Cargo_Nome1, StringUtil.RTrim( context.localUtil.Format( AV16Cargo_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,39);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCargo_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCargo_nome1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCargo_uonom1_Internalname, StringUtil.RTrim( AV30Cargo_UONom1), StringUtil.RTrim( context.localUtil.Format( AV30Cargo_UONom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,40);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCargo_uonom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCargo_uonom1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV18DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCargo_nome2_Internalname, AV19Cargo_Nome2, StringUtil.RTrim( context.localUtil.Format( AV19Cargo_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,52);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCargo_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCargo_nome2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCargo_uonom2_Internalname, StringUtil.RTrim( AV31Cargo_UONom2), StringUtil.RTrim( context.localUtil.Format( AV31Cargo_UONom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,53);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCargo_uonom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCargo_uonom2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "", true, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCargo_nome3_Internalname, AV22Cargo_Nome3, StringUtil.RTrim( context.localUtil.Format( AV22Cargo_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,65);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCargo_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCargo_nome3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'" + sPrefix + "',false,'" + sGXsfl_75_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCargo_uonom3_Internalname, StringUtil.RTrim( AV32Cargo_UONom3), StringUtil.RTrim( context.localUtil.Format( AV32Cargo_UONom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,66);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCargo_uonom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCargo_uonom3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_30_CU2e( true) ;
         }
         else
         {
            wb_table6_30_CU2e( false) ;
         }
      }

      protected void wb_table4_11_CU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_Grupo_CargoGeral_CargoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_CU2e( true) ;
         }
         else
         {
            wb_table4_11_CU2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7GrupoCargo_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7GrupoCargo_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PACU2( ) ;
         WSCU2( ) ;
         WECU2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7GrupoCargo_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PACU2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "geral_grupo_cargogeral_cargowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PACU2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7GrupoCargo_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7GrupoCargo_Codigo), 6, 0)));
         }
         wcpOAV7GrupoCargo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7GrupoCargo_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7GrupoCargo_Codigo != wcpOAV7GrupoCargo_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7GrupoCargo_Codigo = AV7GrupoCargo_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7GrupoCargo_Codigo = cgiGet( sPrefix+"AV7GrupoCargo_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7GrupoCargo_Codigo) > 0 )
         {
            AV7GrupoCargo_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7GrupoCargo_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7GrupoCargo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7GrupoCargo_Codigo), 6, 0)));
         }
         else
         {
            AV7GrupoCargo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7GrupoCargo_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PACU2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSCU2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSCU2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7GrupoCargo_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7GrupoCargo_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7GrupoCargo_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7GrupoCargo_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7GrupoCargo_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WECU2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117222258");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("geral_grupo_cargogeral_cargowc.js", "?20203117222258");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_752( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_75_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_75_idx;
         edtCargo_Codigo_Internalname = sPrefix+"CARGO_CODIGO_"+sGXsfl_75_idx;
         edtCargo_Nome_Internalname = sPrefix+"CARGO_NOME_"+sGXsfl_75_idx;
         edtCargo_UONom_Internalname = sPrefix+"CARGO_UONOM_"+sGXsfl_75_idx;
      }

      protected void SubsflControlProps_fel_752( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_75_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_75_fel_idx;
         edtCargo_Codigo_Internalname = sPrefix+"CARGO_CODIGO_"+sGXsfl_75_fel_idx;
         edtCargo_Nome_Internalname = sPrefix+"CARGO_NOME_"+sGXsfl_75_fel_idx;
         edtCargo_UONom_Internalname = sPrefix+"CARGO_UONOM_"+sGXsfl_75_fel_idx;
      }

      protected void sendrow_752( )
      {
         SubsflControlProps_752( ) ;
         WBCU0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_75_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_75_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_75_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV25Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV25Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV49Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)) ? AV49Update_GXI : context.PathToRelativeUrl( AV25Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV25Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV26Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV50Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)) ? AV50Delete_GXI : context.PathToRelativeUrl( AV26Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV26Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCargo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A617Cargo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A617Cargo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCargo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)75,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCargo_Nome_Internalname,(String)A618Cargo_Nome,StringUtil.RTrim( context.localUtil.Format( A618Cargo_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtCargo_Nome_Link,(String)"",(String)"",(String)"",(String)edtCargo_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)80,(short)0,(short)0,(short)75,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCargo_UONom_Internalname,StringUtil.RTrim( A1003Cargo_UONom),StringUtil.RTrim( context.localUtil.Format( A1003Cargo_UONom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtCargo_UONom_Link,(String)"",(String)"",(String)"",(String)edtCargo_UONom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)75,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CARGO_CODIGO"+"_"+sGXsfl_75_idx, GetSecureSignedToken( sPrefix+sGXsfl_75_idx, context.localUtil.Format( (decimal)(A617Cargo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CARGO_NOME"+"_"+sGXsfl_75_idx, GetSecureSignedToken( sPrefix+sGXsfl_75_idx, StringUtil.RTrim( context.localUtil.Format( A618Cargo_Nome, "@!"))));
            GridContainer.AddRow(GridRow);
            nGXsfl_75_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_75_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_75_idx+1));
            sGXsfl_75_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_75_idx), 4, 0)), 4, "0");
            SubsflControlProps_752( ) ;
         }
         /* End function sendrow_752 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblFiltertextcargo_ativo_Internalname = sPrefix+"FILTERTEXTCARGO_ATIVO";
         chkavCargo_ativo_Internalname = sPrefix+"vCARGO_ATIVO";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         edtavCargo_nome1_Internalname = sPrefix+"vCARGO_NOME1";
         edtavCargo_uonom1_Internalname = sPrefix+"vCARGO_UONOM1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         edtavCargo_nome2_Internalname = sPrefix+"vCARGO_NOME2";
         edtavCargo_uonom2_Internalname = sPrefix+"vCARGO_UONOM2";
         imgAdddynamicfilters2_Internalname = sPrefix+"ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = sPrefix+"DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE3";
         edtavCargo_nome3_Internalname = sPrefix+"vCARGO_NOME3";
         edtavCargo_uonom3_Internalname = sPrefix+"vCARGO_UONOM3";
         imgRemovedynamicfilters3_Internalname = sPrefix+"REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtCargo_Codigo_Internalname = sPrefix+"CARGO_CODIGO";
         edtCargo_Nome_Internalname = sPrefix+"CARGO_NOME";
         edtCargo_UONom_Internalname = sPrefix+"CARGO_UONOM";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtGrupoCargo_Codigo_Internalname = sPrefix+"GRUPOCARGO_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = sPrefix+"vDYNAMICFILTERSENABLED3";
         edtavTfcargo_nome_Internalname = sPrefix+"vTFCARGO_NOME";
         edtavTfcargo_nome_sel_Internalname = sPrefix+"vTFCARGO_NOME_SEL";
         edtavTfcargo_uonom_Internalname = sPrefix+"vTFCARGO_UONOM";
         edtavTfcargo_uonom_sel_Internalname = sPrefix+"vTFCARGO_UONOM_SEL";
         Ddo_cargo_nome_Internalname = sPrefix+"DDO_CARGO_NOME";
         edtavDdo_cargo_nometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_cargo_uonom_Internalname = sPrefix+"DDO_CARGO_UONOM";
         edtavDdo_cargo_uonomtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtCargo_UONom_Jsonclick = "";
         edtCargo_Nome_Jsonclick = "";
         edtCargo_Codigo_Jsonclick = "";
         edtavCargo_uonom3_Jsonclick = "";
         edtavCargo_nome3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavCargo_uonom2_Jsonclick = "";
         edtavCargo_nome2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavCargo_uonom1_Jsonclick = "";
         edtavCargo_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtCargo_UONom_Link = "";
         edtCargo_Nome_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtCargo_UONom_Titleformat = 0;
         edtCargo_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavCargo_uonom3_Visible = 1;
         edtavCargo_nome3_Visible = 1;
         edtavCargo_uonom2_Visible = 1;
         edtavCargo_nome2_Visible = 1;
         edtavCargo_uonom1_Visible = 1;
         edtavCargo_nome1_Visible = 1;
         edtCargo_UONom_Title = "Unidade Organizacional";
         edtCargo_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavCargo_ativo.Caption = "";
         edtavDdo_cargo_uonomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_cargo_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfcargo_uonom_sel_Jsonclick = "";
         edtavTfcargo_uonom_sel_Visible = 1;
         edtavTfcargo_uonom_Jsonclick = "";
         edtavTfcargo_uonom_Visible = 1;
         edtavTfcargo_nome_sel_Jsonclick = "";
         edtavTfcargo_nome_sel_Visible = 1;
         edtavTfcargo_nome_Jsonclick = "";
         edtavTfcargo_nome_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtGrupoCargo_Codigo_Jsonclick = "";
         edtGrupoCargo_Codigo_Visible = 1;
         Ddo_cargo_uonom_Searchbuttontext = "Pesquisar";
         Ddo_cargo_uonom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_cargo_uonom_Cleanfilter = "Limpar pesquisa";
         Ddo_cargo_uonom_Loadingdata = "Carregando dados...";
         Ddo_cargo_uonom_Sortdsc = "Ordenar de Z � A";
         Ddo_cargo_uonom_Sortasc = "Ordenar de A � Z";
         Ddo_cargo_uonom_Datalistupdateminimumcharacters = 0;
         Ddo_cargo_uonom_Datalistproc = "GetGeral_Grupo_CargoGeral_CargoWCFilterData";
         Ddo_cargo_uonom_Datalisttype = "Dynamic";
         Ddo_cargo_uonom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_cargo_uonom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_cargo_uonom_Filtertype = "Character";
         Ddo_cargo_uonom_Includefilter = Convert.ToBoolean( -1);
         Ddo_cargo_uonom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_cargo_uonom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_cargo_uonom_Titlecontrolidtoreplace = "";
         Ddo_cargo_uonom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_cargo_uonom_Cls = "ColumnSettings";
         Ddo_cargo_uonom_Tooltip = "Op��es";
         Ddo_cargo_uonom_Caption = "";
         Ddo_cargo_nome_Searchbuttontext = "Pesquisar";
         Ddo_cargo_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_cargo_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_cargo_nome_Loadingdata = "Carregando dados...";
         Ddo_cargo_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_cargo_nome_Sortasc = "Ordenar de A � Z";
         Ddo_cargo_nome_Datalistupdateminimumcharacters = 0;
         Ddo_cargo_nome_Datalistproc = "GetGeral_Grupo_CargoGeral_CargoWCFilterData";
         Ddo_cargo_nome_Datalisttype = "Dynamic";
         Ddo_cargo_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_cargo_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_cargo_nome_Filtertype = "Character";
         Ddo_cargo_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_cargo_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_cargo_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_cargo_nome_Titlecontrolidtoreplace = "";
         Ddo_cargo_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_cargo_nome_Cls = "ColumnSettings";
         Ddo_cargo_nome_Tooltip = "Op��es";
         Ddo_cargo_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV38ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV28Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV36TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV40TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV41TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV7GrupoCargo_Codigo',fld:'vGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV30Cargo_UONom1',fld:'vCARGO_UONOM1',pic:'@!',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV31Cargo_UONom2',fld:'vCARGO_UONOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV32Cargo_UONom3',fld:'vCARGO_UONOM3',pic:'@!',nv:''}],oparms:[{av:'AV35Cargo_NomeTitleFilterData',fld:'vCARGO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV39Cargo_UONomTitleFilterData',fld:'vCARGO_UONOMTITLEFILTERDATA',pic:'',nv:null},{av:'edtCargo_Nome_Titleformat',ctrl:'CARGO_NOME',prop:'Titleformat'},{av:'edtCargo_Nome_Title',ctrl:'CARGO_NOME',prop:'Title'},{av:'edtCargo_UONom_Titleformat',ctrl:'CARGO_UONOM',prop:'Titleformat'},{av:'edtCargo_UONom_Title',ctrl:'CARGO_UONOM',prop:'Title'},{av:'AV45GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV46GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11CU2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV29OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV30Cargo_UONom1',fld:'vCARGO_UONOM1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV31Cargo_UONom2',fld:'vCARGO_UONOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV32Cargo_UONom3',fld:'vCARGO_UONOM3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV40TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV41TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV7GrupoCargo_Codigo',fld:'vGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV28Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CARGO_NOME.ONOPTIONCLICKED","{handler:'E12CU2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV29OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV30Cargo_UONom1',fld:'vCARGO_UONOM1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV31Cargo_UONom2',fld:'vCARGO_UONOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV32Cargo_UONom3',fld:'vCARGO_UONOM3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV40TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV41TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV7GrupoCargo_Codigo',fld:'vGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV28Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_cargo_nome_Activeeventkey',ctrl:'DDO_CARGO_NOME',prop:'ActiveEventKey'},{av:'Ddo_cargo_nome_Filteredtext_get',ctrl:'DDO_CARGO_NOME',prop:'FilteredText_get'},{av:'Ddo_cargo_nome_Selectedvalue_get',ctrl:'DDO_CARGO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV29OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_cargo_nome_Sortedstatus',ctrl:'DDO_CARGO_NOME',prop:'SortedStatus'},{av:'AV36TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_cargo_uonom_Sortedstatus',ctrl:'DDO_CARGO_UONOM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CARGO_UONOM.ONOPTIONCLICKED","{handler:'E13CU2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV29OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV30Cargo_UONom1',fld:'vCARGO_UONOM1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV31Cargo_UONom2',fld:'vCARGO_UONOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV32Cargo_UONom3',fld:'vCARGO_UONOM3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV40TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV41TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV7GrupoCargo_Codigo',fld:'vGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV28Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_cargo_uonom_Activeeventkey',ctrl:'DDO_CARGO_UONOM',prop:'ActiveEventKey'},{av:'Ddo_cargo_uonom_Filteredtext_get',ctrl:'DDO_CARGO_UONOM',prop:'FilteredText_get'},{av:'Ddo_cargo_uonom_Selectedvalue_get',ctrl:'DDO_CARGO_UONOM',prop:'SelectedValue_get'}],oparms:[{av:'AV29OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_cargo_uonom_Sortedstatus',ctrl:'DDO_CARGO_UONOM',prop:'SortedStatus'},{av:'AV40TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV41TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'Ddo_cargo_nome_Sortedstatus',ctrl:'DDO_CARGO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E27CU2',iparms:[{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV25Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV26Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtCargo_Nome_Link',ctrl:'CARGO_NOME',prop:'Link'},{av:'edtCargo_UONom_Link',ctrl:'CARGO_UONOM',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E14CU2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV29OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV30Cargo_UONom1',fld:'vCARGO_UONOM1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV31Cargo_UONom2',fld:'vCARGO_UONOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV32Cargo_UONom3',fld:'vCARGO_UONOM3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV40TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV41TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV7GrupoCargo_Codigo',fld:'vGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV28Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E20CU2',iparms:[],oparms:[{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E15CU2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV29OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV30Cargo_UONom1',fld:'vCARGO_UONOM1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV31Cargo_UONom2',fld:'vCARGO_UONOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV32Cargo_UONom3',fld:'vCARGO_UONOM3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV40TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV41TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV7GrupoCargo_Codigo',fld:'vGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV28Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV30Cargo_UONom1',fld:'vCARGO_UONOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV31Cargo_UONom2',fld:'vCARGO_UONOM2',pic:'@!',nv:''},{av:'AV32Cargo_UONom3',fld:'vCARGO_UONOM3',pic:'@!',nv:''},{av:'edtavCargo_nome2_Visible',ctrl:'vCARGO_NOME2',prop:'Visible'},{av:'edtavCargo_uonom2_Visible',ctrl:'vCARGO_UONOM2',prop:'Visible'},{av:'edtavCargo_nome3_Visible',ctrl:'vCARGO_NOME3',prop:'Visible'},{av:'edtavCargo_uonom3_Visible',ctrl:'vCARGO_UONOM3',prop:'Visible'},{av:'edtavCargo_nome1_Visible',ctrl:'vCARGO_NOME1',prop:'Visible'},{av:'edtavCargo_uonom1_Visible',ctrl:'vCARGO_UONOM1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E21CU2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavCargo_nome1_Visible',ctrl:'vCARGO_NOME1',prop:'Visible'},{av:'edtavCargo_uonom1_Visible',ctrl:'vCARGO_UONOM1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E22CU2',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E16CU2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV29OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV30Cargo_UONom1',fld:'vCARGO_UONOM1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV31Cargo_UONom2',fld:'vCARGO_UONOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV32Cargo_UONom3',fld:'vCARGO_UONOM3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV40TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV41TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV7GrupoCargo_Codigo',fld:'vGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV28Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV30Cargo_UONom1',fld:'vCARGO_UONOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV31Cargo_UONom2',fld:'vCARGO_UONOM2',pic:'@!',nv:''},{av:'AV32Cargo_UONom3',fld:'vCARGO_UONOM3',pic:'@!',nv:''},{av:'edtavCargo_nome2_Visible',ctrl:'vCARGO_NOME2',prop:'Visible'},{av:'edtavCargo_uonom2_Visible',ctrl:'vCARGO_UONOM2',prop:'Visible'},{av:'edtavCargo_nome3_Visible',ctrl:'vCARGO_NOME3',prop:'Visible'},{av:'edtavCargo_uonom3_Visible',ctrl:'vCARGO_UONOM3',prop:'Visible'},{av:'edtavCargo_nome1_Visible',ctrl:'vCARGO_NOME1',prop:'Visible'},{av:'edtavCargo_uonom1_Visible',ctrl:'vCARGO_UONOM1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E23CU2',iparms:[{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavCargo_nome2_Visible',ctrl:'vCARGO_NOME2',prop:'Visible'},{av:'edtavCargo_uonom2_Visible',ctrl:'vCARGO_UONOM2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E17CU2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV29OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV30Cargo_UONom1',fld:'vCARGO_UONOM1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV31Cargo_UONom2',fld:'vCARGO_UONOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV32Cargo_UONom3',fld:'vCARGO_UONOM3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV40TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV41TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV7GrupoCargo_Codigo',fld:'vGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV28Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV30Cargo_UONom1',fld:'vCARGO_UONOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV31Cargo_UONom2',fld:'vCARGO_UONOM2',pic:'@!',nv:''},{av:'AV32Cargo_UONom3',fld:'vCARGO_UONOM3',pic:'@!',nv:''},{av:'edtavCargo_nome2_Visible',ctrl:'vCARGO_NOME2',prop:'Visible'},{av:'edtavCargo_uonom2_Visible',ctrl:'vCARGO_UONOM2',prop:'Visible'},{av:'edtavCargo_nome3_Visible',ctrl:'vCARGO_NOME3',prop:'Visible'},{av:'edtavCargo_uonom3_Visible',ctrl:'vCARGO_UONOM3',prop:'Visible'},{av:'edtavCargo_nome1_Visible',ctrl:'vCARGO_NOME1',prop:'Visible'},{av:'edtavCargo_uonom1_Visible',ctrl:'vCARGO_UONOM1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E24CU2',iparms:[{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavCargo_nome3_Visible',ctrl:'vCARGO_NOME3',prop:'Visible'},{av:'edtavCargo_uonom3_Visible',ctrl:'vCARGO_UONOM3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E18CU2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV29OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV30Cargo_UONom1',fld:'vCARGO_UONOM1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV31Cargo_UONom2',fld:'vCARGO_UONOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'AV32Cargo_UONom3',fld:'vCARGO_UONOM3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'AV37TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'AV40TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'AV41TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'AV7GrupoCargo_Codigo',fld:'vGRUPOCARGO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_Cargo_NomeTitleControlIdToReplace',fld:'vDDO_CARGO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_Cargo_UONomTitleControlIdToReplace',fld:'vDDO_CARGO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV28Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1002Cargo_UOCod',fld:'CARGO_UOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV28Cargo_Ativo',fld:'vCARGO_ATIVO',pic:'',nv:false},{av:'AV36TFCargo_Nome',fld:'vTFCARGO_NOME',pic:'@!',nv:''},{av:'Ddo_cargo_nome_Filteredtext_set',ctrl:'DDO_CARGO_NOME',prop:'FilteredText_set'},{av:'AV37TFCargo_Nome_Sel',fld:'vTFCARGO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_cargo_nome_Selectedvalue_set',ctrl:'DDO_CARGO_NOME',prop:'SelectedValue_set'},{av:'AV40TFCargo_UONom',fld:'vTFCARGO_UONOM',pic:'@!',nv:''},{av:'Ddo_cargo_uonom_Filteredtext_set',ctrl:'DDO_CARGO_UONOM',prop:'FilteredText_set'},{av:'AV41TFCargo_UONom_Sel',fld:'vTFCARGO_UONOM_SEL',pic:'@!',nv:''},{av:'Ddo_cargo_uonom_Selectedvalue_set',ctrl:'DDO_CARGO_UONOM',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Cargo_Nome1',fld:'vCARGO_NOME1',pic:'@!',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavCargo_nome1_Visible',ctrl:'vCARGO_NOME1',prop:'Visible'},{av:'edtavCargo_uonom1_Visible',ctrl:'vCARGO_UONOM1',prop:'Visible'},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Cargo_Nome2',fld:'vCARGO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Cargo_Nome3',fld:'vCARGO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV30Cargo_UONom1',fld:'vCARGO_UONOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV31Cargo_UONom2',fld:'vCARGO_UONOM2',pic:'@!',nv:''},{av:'AV32Cargo_UONom3',fld:'vCARGO_UONOM3',pic:'@!',nv:''},{av:'edtavCargo_nome2_Visible',ctrl:'vCARGO_NOME2',prop:'Visible'},{av:'edtavCargo_uonom2_Visible',ctrl:'vCARGO_UONOM2',prop:'Visible'},{av:'edtavCargo_nome3_Visible',ctrl:'vCARGO_NOME3',prop:'Visible'},{av:'edtavCargo_uonom3_Visible',ctrl:'vCARGO_UONOM3',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E19CU2',iparms:[{av:'A617Cargo_Codigo',fld:'CARGO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_cargo_nome_Activeeventkey = "";
         Ddo_cargo_nome_Filteredtext_get = "";
         Ddo_cargo_nome_Selectedvalue_get = "";
         Ddo_cargo_uonom_Activeeventkey = "";
         Ddo_cargo_uonom_Filteredtext_get = "";
         Ddo_cargo_uonom_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15DynamicFiltersSelector1 = "";
         AV16Cargo_Nome1 = "";
         AV30Cargo_UONom1 = "";
         AV18DynamicFiltersSelector2 = "";
         AV19Cargo_Nome2 = "";
         AV31Cargo_UONom2 = "";
         AV21DynamicFiltersSelector3 = "";
         AV22Cargo_Nome3 = "";
         AV32Cargo_UONom3 = "";
         AV36TFCargo_Nome = "";
         AV37TFCargo_Nome_Sel = "";
         AV40TFCargo_UONom = "";
         AV41TFCargo_UONom_Sel = "";
         AV38ddo_Cargo_NomeTitleControlIdToReplace = "";
         AV42ddo_Cargo_UONomTitleControlIdToReplace = "";
         AV51Pgmname = "";
         AV28Cargo_Ativo = true;
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV43DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV35Cargo_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39Cargo_UONomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_cargo_nome_Filteredtext_set = "";
         Ddo_cargo_nome_Selectedvalue_set = "";
         Ddo_cargo_nome_Sortedstatus = "";
         Ddo_cargo_uonom_Filteredtext_set = "";
         Ddo_cargo_uonom_Selectedvalue_set = "";
         Ddo_cargo_uonom_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV25Update = "";
         AV49Update_GXI = "";
         AV26Delete = "";
         AV50Delete_GXI = "";
         A618Cargo_Nome = "";
         A1003Cargo_UONom = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV16Cargo_Nome1 = "";
         lV30Cargo_UONom1 = "";
         lV19Cargo_Nome2 = "";
         lV31Cargo_UONom2 = "";
         lV22Cargo_Nome3 = "";
         lV32Cargo_UONom3 = "";
         lV36TFCargo_Nome = "";
         lV40TFCargo_UONom = "";
         H00CU2_A628Cargo_Ativo = new bool[] {false} ;
         H00CU2_A1002Cargo_UOCod = new int[1] ;
         H00CU2_n1002Cargo_UOCod = new bool[] {false} ;
         H00CU2_A615GrupoCargo_Codigo = new int[1] ;
         H00CU2_A1003Cargo_UONom = new String[] {""} ;
         H00CU2_n1003Cargo_UONom = new bool[] {false} ;
         H00CU2_A618Cargo_Nome = new String[] {""} ;
         H00CU2_A617Cargo_Codigo = new int[1] ;
         H00CU3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV27Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblFiltertextcargo_ativo_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7GrupoCargo_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.geral_grupo_cargogeral_cargowc__default(),
            new Object[][] {
                new Object[] {
               H00CU2_A628Cargo_Ativo, H00CU2_A1002Cargo_UOCod, H00CU2_n1002Cargo_UOCod, H00CU2_A615GrupoCargo_Codigo, H00CU2_A1003Cargo_UONom, H00CU2_n1003Cargo_UONom, H00CU2_A618Cargo_Nome, H00CU2_A617Cargo_Codigo
               }
               , new Object[] {
               H00CU3_AGRID_nRecordCount
               }
            }
         );
         AV51Pgmname = "Geral_Grupo_CargoGeral_CargoWC";
         /* GeneXus formulas. */
         AV51Pgmname = "Geral_Grupo_CargoGeral_CargoWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_75 ;
      private short nGXsfl_75_idx=1 ;
      private short AV29OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_75_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtCargo_Nome_Titleformat ;
      private short edtCargo_UONom_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7GrupoCargo_Codigo ;
      private int wcpOAV7GrupoCargo_Codigo ;
      private int subGrid_Rows ;
      private int A617Cargo_Codigo ;
      private int A1002Cargo_UOCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_cargo_nome_Datalistupdateminimumcharacters ;
      private int Ddo_cargo_uonom_Datalistupdateminimumcharacters ;
      private int A615GrupoCargo_Codigo ;
      private int edtGrupoCargo_Codigo_Visible ;
      private int edtavTfcargo_nome_Visible ;
      private int edtavTfcargo_nome_sel_Visible ;
      private int edtavTfcargo_uonom_Visible ;
      private int edtavTfcargo_uonom_sel_Visible ;
      private int edtavDdo_cargo_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_cargo_uonomtitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV44PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavCargo_nome1_Visible ;
      private int edtavCargo_uonom1_Visible ;
      private int edtavCargo_nome2_Visible ;
      private int edtavCargo_uonom2_Visible ;
      private int edtavCargo_nome3_Visible ;
      private int edtavCargo_uonom3_Visible ;
      private int AV52GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV45GridCurrentPage ;
      private long AV46GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_cargo_nome_Activeeventkey ;
      private String Ddo_cargo_nome_Filteredtext_get ;
      private String Ddo_cargo_nome_Selectedvalue_get ;
      private String Ddo_cargo_uonom_Activeeventkey ;
      private String Ddo_cargo_uonom_Filteredtext_get ;
      private String Ddo_cargo_uonom_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_75_idx="0001" ;
      private String AV30Cargo_UONom1 ;
      private String AV31Cargo_UONom2 ;
      private String AV32Cargo_UONom3 ;
      private String AV40TFCargo_UONom ;
      private String AV41TFCargo_UONom_Sel ;
      private String AV51Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_cargo_nome_Caption ;
      private String Ddo_cargo_nome_Tooltip ;
      private String Ddo_cargo_nome_Cls ;
      private String Ddo_cargo_nome_Filteredtext_set ;
      private String Ddo_cargo_nome_Selectedvalue_set ;
      private String Ddo_cargo_nome_Dropdownoptionstype ;
      private String Ddo_cargo_nome_Titlecontrolidtoreplace ;
      private String Ddo_cargo_nome_Sortedstatus ;
      private String Ddo_cargo_nome_Filtertype ;
      private String Ddo_cargo_nome_Datalisttype ;
      private String Ddo_cargo_nome_Datalistproc ;
      private String Ddo_cargo_nome_Sortasc ;
      private String Ddo_cargo_nome_Sortdsc ;
      private String Ddo_cargo_nome_Loadingdata ;
      private String Ddo_cargo_nome_Cleanfilter ;
      private String Ddo_cargo_nome_Noresultsfound ;
      private String Ddo_cargo_nome_Searchbuttontext ;
      private String Ddo_cargo_uonom_Caption ;
      private String Ddo_cargo_uonom_Tooltip ;
      private String Ddo_cargo_uonom_Cls ;
      private String Ddo_cargo_uonom_Filteredtext_set ;
      private String Ddo_cargo_uonom_Selectedvalue_set ;
      private String Ddo_cargo_uonom_Dropdownoptionstype ;
      private String Ddo_cargo_uonom_Titlecontrolidtoreplace ;
      private String Ddo_cargo_uonom_Sortedstatus ;
      private String Ddo_cargo_uonom_Filtertype ;
      private String Ddo_cargo_uonom_Datalisttype ;
      private String Ddo_cargo_uonom_Datalistproc ;
      private String Ddo_cargo_uonom_Sortasc ;
      private String Ddo_cargo_uonom_Sortdsc ;
      private String Ddo_cargo_uonom_Loadingdata ;
      private String Ddo_cargo_uonom_Cleanfilter ;
      private String Ddo_cargo_uonom_Noresultsfound ;
      private String Ddo_cargo_uonom_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtGrupoCargo_Codigo_Internalname ;
      private String edtGrupoCargo_Codigo_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcargo_nome_Internalname ;
      private String edtavTfcargo_nome_Jsonclick ;
      private String edtavTfcargo_nome_sel_Internalname ;
      private String edtavTfcargo_nome_sel_Jsonclick ;
      private String edtavTfcargo_uonom_Internalname ;
      private String edtavTfcargo_uonom_Jsonclick ;
      private String edtavTfcargo_uonom_sel_Internalname ;
      private String edtavTfcargo_uonom_sel_Jsonclick ;
      private String edtavDdo_cargo_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_cargo_uonomtitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtCargo_Codigo_Internalname ;
      private String edtCargo_Nome_Internalname ;
      private String A1003Cargo_UONom ;
      private String edtCargo_UONom_Internalname ;
      private String chkavCargo_ativo_Internalname ;
      private String scmdbuf ;
      private String lV30Cargo_UONom1 ;
      private String lV31Cargo_UONom2 ;
      private String lV32Cargo_UONom3 ;
      private String lV40TFCargo_UONom ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavCargo_nome1_Internalname ;
      private String edtavCargo_uonom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavCargo_nome2_Internalname ;
      private String edtavCargo_uonom2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavCargo_nome3_Internalname ;
      private String edtavCargo_uonom3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_cargo_nome_Internalname ;
      private String Ddo_cargo_uonom_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtCargo_Nome_Title ;
      private String edtCargo_UONom_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtCargo_Nome_Link ;
      private String edtCargo_UONom_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextcargo_ativo_Internalname ;
      private String lblFiltertextcargo_ativo_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavCargo_nome1_Jsonclick ;
      private String edtavCargo_uonom1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavCargo_nome2_Jsonclick ;
      private String edtavCargo_uonom2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavCargo_nome3_Jsonclick ;
      private String edtavCargo_uonom3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7GrupoCargo_Codigo ;
      private String sGXsfl_75_fel_idx="0001" ;
      private String ROClassString ;
      private String edtCargo_Codigo_Jsonclick ;
      private String edtCargo_Nome_Jsonclick ;
      private String edtCargo_UONom_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV17DynamicFiltersEnabled2 ;
      private bool AV20DynamicFiltersEnabled3 ;
      private bool AV28Cargo_Ativo ;
      private bool AV24DynamicFiltersIgnoreFirst ;
      private bool AV23DynamicFiltersRemoving ;
      private bool n1002Cargo_UOCod ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_cargo_nome_Includesortasc ;
      private bool Ddo_cargo_nome_Includesortdsc ;
      private bool Ddo_cargo_nome_Includefilter ;
      private bool Ddo_cargo_nome_Filterisrange ;
      private bool Ddo_cargo_nome_Includedatalist ;
      private bool Ddo_cargo_uonom_Includesortasc ;
      private bool Ddo_cargo_uonom_Includesortdsc ;
      private bool Ddo_cargo_uonom_Includefilter ;
      private bool Ddo_cargo_uonom_Filterisrange ;
      private bool Ddo_cargo_uonom_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1003Cargo_UONom ;
      private bool A628Cargo_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV25Update_IsBlob ;
      private bool AV26Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV16Cargo_Nome1 ;
      private String AV18DynamicFiltersSelector2 ;
      private String AV19Cargo_Nome2 ;
      private String AV21DynamicFiltersSelector3 ;
      private String AV22Cargo_Nome3 ;
      private String AV36TFCargo_Nome ;
      private String AV37TFCargo_Nome_Sel ;
      private String AV38ddo_Cargo_NomeTitleControlIdToReplace ;
      private String AV42ddo_Cargo_UONomTitleControlIdToReplace ;
      private String AV49Update_GXI ;
      private String AV50Delete_GXI ;
      private String A618Cargo_Nome ;
      private String lV16Cargo_Nome1 ;
      private String lV19Cargo_Nome2 ;
      private String lV22Cargo_Nome3 ;
      private String lV36TFCargo_Nome ;
      private String AV25Update ;
      private String AV26Delete ;
      private IGxSession AV27Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCheckbox chkavCargo_ativo ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private bool[] H00CU2_A628Cargo_Ativo ;
      private int[] H00CU2_A1002Cargo_UOCod ;
      private bool[] H00CU2_n1002Cargo_UOCod ;
      private int[] H00CU2_A615GrupoCargo_Codigo ;
      private String[] H00CU2_A1003Cargo_UONom ;
      private bool[] H00CU2_n1003Cargo_UONom ;
      private String[] H00CU2_A618Cargo_Nome ;
      private int[] H00CU2_A617Cargo_Codigo ;
      private long[] H00CU3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV35Cargo_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV39Cargo_UONomTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV43DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class geral_grupo_cargogeral_cargowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00CU2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV16Cargo_Nome1 ,
                                             String AV30Cargo_UONom1 ,
                                             bool AV17DynamicFiltersEnabled2 ,
                                             String AV18DynamicFiltersSelector2 ,
                                             String AV19Cargo_Nome2 ,
                                             String AV31Cargo_UONom2 ,
                                             bool AV20DynamicFiltersEnabled3 ,
                                             String AV21DynamicFiltersSelector3 ,
                                             String AV22Cargo_Nome3 ,
                                             String AV32Cargo_UONom3 ,
                                             String AV37TFCargo_Nome_Sel ,
                                             String AV36TFCargo_Nome ,
                                             String AV41TFCargo_UONom_Sel ,
                                             String AV40TFCargo_UONom ,
                                             String A618Cargo_Nome ,
                                             String A1003Cargo_UONom ,
                                             short AV29OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A615GrupoCargo_Codigo ,
                                             int AV7GrupoCargo_Codigo ,
                                             bool A628Cargo_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [16] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Cargo_Ativo], T1.[Cargo_UOCod] AS Cargo_UOCod, T1.[GrupoCargo_Codigo], T2.[UnidadeOrganizacional_Nome] AS Cargo_UONom, T1.[Cargo_Nome], T1.[Cargo_Codigo]";
         sFromString = " FROM ([Geral_Cargo] T1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = T1.[Cargo_UOCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[GrupoCargo_Codigo] = @AV7GrupoCargo_Codigo)";
         sWhereString = sWhereString + " and (T1.[Cargo_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16Cargo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV16Cargo_Nome1 + '%')";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CARGO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30Cargo_UONom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV30Cargo_UONom1 + '%')";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Cargo_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV19Cargo_Nome2 + '%')";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CARGO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Cargo_UONom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV31Cargo_UONom2 + '%')";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV20DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Cargo_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV22Cargo_Nome3 + '%')";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CARGO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32Cargo_UONom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV32Cargo_UONom3 + '%')";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV37TFCargo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFCargo_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like @lV36TFCargo_Nome)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFCargo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] = @AV37TFCargo_Nome_Sel)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV41TFCargo_UONom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFCargo_UONom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV40TFCargo_UONom)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFCargo_UONom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] = @AV41TFCargo_UONom_Sel)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ( AV29OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[GrupoCargo_Codigo], T1.[Cargo_Nome]";
         }
         else if ( ( AV29OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[GrupoCargo_Codigo] DESC, T1.[Cargo_Nome] DESC";
         }
         else if ( ( AV29OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[GrupoCargo_Codigo], T2.[UnidadeOrganizacional_Nome]";
         }
         else if ( ( AV29OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[GrupoCargo_Codigo] DESC, T2.[UnidadeOrganizacional_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Cargo_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00CU3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV16Cargo_Nome1 ,
                                             String AV30Cargo_UONom1 ,
                                             bool AV17DynamicFiltersEnabled2 ,
                                             String AV18DynamicFiltersSelector2 ,
                                             String AV19Cargo_Nome2 ,
                                             String AV31Cargo_UONom2 ,
                                             bool AV20DynamicFiltersEnabled3 ,
                                             String AV21DynamicFiltersSelector3 ,
                                             String AV22Cargo_Nome3 ,
                                             String AV32Cargo_UONom3 ,
                                             String AV37TFCargo_Nome_Sel ,
                                             String AV36TFCargo_Nome ,
                                             String AV41TFCargo_UONom_Sel ,
                                             String AV40TFCargo_UONom ,
                                             String A618Cargo_Nome ,
                                             String A1003Cargo_UONom ,
                                             short AV29OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A615GrupoCargo_Codigo ,
                                             int AV7GrupoCargo_Codigo ,
                                             bool A628Cargo_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [11] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Geral_Cargo] T1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = T1.[Cargo_UOCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[GrupoCargo_Codigo] = @AV7GrupoCargo_Codigo)";
         scmdbuf = scmdbuf + " and (T1.[Cargo_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16Cargo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV16Cargo_Nome1 + '%')";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CARGO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30Cargo_UONom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV30Cargo_UONom1 + '%')";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Cargo_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV19Cargo_Nome2 + '%')";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "CARGO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Cargo_UONom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV31Cargo_UONom2 + '%')";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV20DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Cargo_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV22Cargo_Nome3 + '%')";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "CARGO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32Cargo_UONom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV32Cargo_UONom3 + '%')";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV37TFCargo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFCargo_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like @lV36TFCargo_Nome)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFCargo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] = @AV37TFCargo_Nome_Sel)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV41TFCargo_UONom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFCargo_UONom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV40TFCargo_UONom)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFCargo_UONom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] = @AV41TFCargo_UONom_Sel)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV29OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV29OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV29OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV29OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00CU2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (bool)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (bool)dynConstraints[21] );
               case 1 :
                     return conditional_H00CU3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (bool)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (bool)dynConstraints[21] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00CU2 ;
          prmH00CU2 = new Object[] {
          new Object[] {"@AV7GrupoCargo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV16Cargo_Nome1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV30Cargo_UONom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19Cargo_Nome2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV31Cargo_UONom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22Cargo_Nome3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV32Cargo_UONom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV36TFCargo_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV37TFCargo_Nome_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV40TFCargo_UONom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV41TFCargo_UONom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00CU3 ;
          prmH00CU3 = new Object[] {
          new Object[] {"@AV7GrupoCargo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV16Cargo_Nome1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV30Cargo_UONom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19Cargo_Nome2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV31Cargo_UONom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22Cargo_Nome3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV32Cargo_UONom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV36TFCargo_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV37TFCargo_Nome_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV40TFCargo_UONom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV41TFCargo_UONom_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00CU2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CU2,11,0,true,false )
             ,new CursorDef("H00CU3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CU3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
       }
    }

 }

}
