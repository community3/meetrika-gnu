/*
               File: Projeto_BC
        Description: Projeto
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:21:9.7
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class projeto_bc : GXHttpHandler, IGxSilentTrn
   {
      public projeto_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public projeto_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow2986( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey2986( ) ;
         standaloneModal( ) ;
         AddRow2986( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E11292 */
            E11292 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z648Projeto_Codigo = A648Projeto_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_290( )
      {
         BeforeValidate2986( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2986( ) ;
            }
            else
            {
               CheckExtendedTable2986( ) ;
               if ( AnyError == 0 )
               {
                  ZM2986( 10) ;
                  ZM2986( 11) ;
               }
               CloseExtendedTableCursors2986( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E12292( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV19Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV20GXV1 = 1;
            while ( AV20GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV20GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Projeto_TipoProjetoCod") == 0 )
               {
                  AV12Insert_Projeto_TipoProjetoCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV20GXV1 = (int)(AV20GXV1+1);
            }
         }
         AV16FatorEscala = 0;
         AV15FatorMultiplicador = 0;
      }

      protected void E11292( )
      {
         /* After Trn Routine */
      }

      protected void E13292( )
      {
         /* 'DoFatorEscala' Routine */
         context.PopUp(formatLink("wp_projetofatores.aspx") + "?" + UrlEncode("" +AV8WWPContext.gxTpr_Areatrabalho_codigo) + "," + UrlEncode("" +AV7Projeto_Codigo) + "," + UrlEncode(StringUtil.RTrim("E")) + "," + UrlEncode(StringUtil.RTrim("")), new Object[] {"AV7Projeto_Codigo","","A985Projeto_FatorEscala"});
      }

      protected void E14292( )
      {
         /* 'DoFatorMultiplicador' Routine */
         context.PopUp(formatLink("wp_projetofatores.aspx") + "?" + UrlEncode("" +AV8WWPContext.gxTpr_Areatrabalho_codigo) + "," + UrlEncode("" +AV7Projeto_Codigo) + "," + UrlEncode(StringUtil.RTrim("M")) + "," + UrlEncode(StringUtil.RTrim("")), new Object[] {"AV7Projeto_Codigo","","A989Projeto_FatorMultiplicador"});
      }

      protected void ZM2986( short GX_JID )
      {
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            Z649Projeto_Nome = A649Projeto_Nome;
            Z650Projeto_Sigla = A650Projeto_Sigla;
            Z651Projeto_TipoContagem = A651Projeto_TipoContagem;
            Z652Projeto_TecnicaContagem = A652Projeto_TecnicaContagem;
            Z1541Projeto_Previsao = A1541Projeto_Previsao;
            Z1542Projeto_GerenteCod = A1542Projeto_GerenteCod;
            Z1543Projeto_ServicoCod = A1543Projeto_ServicoCod;
            Z658Projeto_Status = A658Projeto_Status;
            Z985Projeto_FatorEscala = A985Projeto_FatorEscala;
            Z989Projeto_FatorMultiplicador = A989Projeto_FatorMultiplicador;
            Z990Projeto_ConstACocomo = A990Projeto_ConstACocomo;
            Z1232Projeto_Incremental = A1232Projeto_Incremental;
            Z983Projeto_TipoProjetoCod = A983Projeto_TipoProjetoCod;
            Z655Projeto_Custo = A655Projeto_Custo;
            Z656Projeto_Prazo = A656Projeto_Prazo;
            Z657Projeto_Esforco = A657Projeto_Esforco;
            Z740Projeto_SistemaCod = A740Projeto_SistemaCod;
         }
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
            Z655Projeto_Custo = A655Projeto_Custo;
            Z656Projeto_Prazo = A656Projeto_Prazo;
            Z657Projeto_Esforco = A657Projeto_Esforco;
            Z740Projeto_SistemaCod = A740Projeto_SistemaCod;
         }
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
            Z655Projeto_Custo = A655Projeto_Custo;
            Z656Projeto_Prazo = A656Projeto_Prazo;
            Z657Projeto_Esforco = A657Projeto_Esforco;
            Z740Projeto_SistemaCod = A740Projeto_SistemaCod;
         }
         if ( GX_JID == -9 )
         {
            Z648Projeto_Codigo = A648Projeto_Codigo;
            Z649Projeto_Nome = A649Projeto_Nome;
            Z650Projeto_Sigla = A650Projeto_Sigla;
            Z651Projeto_TipoContagem = A651Projeto_TipoContagem;
            Z652Projeto_TecnicaContagem = A652Projeto_TecnicaContagem;
            Z1540Projeto_Introducao = A1540Projeto_Introducao;
            Z653Projeto_Escopo = A653Projeto_Escopo;
            Z1541Projeto_Previsao = A1541Projeto_Previsao;
            Z1542Projeto_GerenteCod = A1542Projeto_GerenteCod;
            Z1543Projeto_ServicoCod = A1543Projeto_ServicoCod;
            Z658Projeto_Status = A658Projeto_Status;
            Z985Projeto_FatorEscala = A985Projeto_FatorEscala;
            Z989Projeto_FatorMultiplicador = A989Projeto_FatorMultiplicador;
            Z990Projeto_ConstACocomo = A990Projeto_ConstACocomo;
            Z1232Projeto_Incremental = A1232Projeto_Incremental;
            Z983Projeto_TipoProjetoCod = A983Projeto_TipoProjetoCod;
            Z655Projeto_Custo = A655Projeto_Custo;
            Z656Projeto_Prazo = A656Projeto_Prazo;
            Z657Projeto_Esforco = A657Projeto_Esforco;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         AV19Pgmname = "Projeto_BC";
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A658Projeto_Status)) && ( Gx_BScreen == 0 ) )
         {
            A658Projeto_Status = "A";
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load2986( )
      {
         /* Using cursor BC00298 */
         pr_default.execute(4, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound86 = 1;
            A649Projeto_Nome = BC00298_A649Projeto_Nome[0];
            A650Projeto_Sigla = BC00298_A650Projeto_Sigla[0];
            A651Projeto_TipoContagem = BC00298_A651Projeto_TipoContagem[0];
            A652Projeto_TecnicaContagem = BC00298_A652Projeto_TecnicaContagem[0];
            A1540Projeto_Introducao = BC00298_A1540Projeto_Introducao[0];
            n1540Projeto_Introducao = BC00298_n1540Projeto_Introducao[0];
            A653Projeto_Escopo = BC00298_A653Projeto_Escopo[0];
            A1541Projeto_Previsao = BC00298_A1541Projeto_Previsao[0];
            n1541Projeto_Previsao = BC00298_n1541Projeto_Previsao[0];
            A1542Projeto_GerenteCod = BC00298_A1542Projeto_GerenteCod[0];
            n1542Projeto_GerenteCod = BC00298_n1542Projeto_GerenteCod[0];
            A1543Projeto_ServicoCod = BC00298_A1543Projeto_ServicoCod[0];
            n1543Projeto_ServicoCod = BC00298_n1543Projeto_ServicoCod[0];
            A658Projeto_Status = BC00298_A658Projeto_Status[0];
            A985Projeto_FatorEscala = BC00298_A985Projeto_FatorEscala[0];
            n985Projeto_FatorEscala = BC00298_n985Projeto_FatorEscala[0];
            A989Projeto_FatorMultiplicador = BC00298_A989Projeto_FatorMultiplicador[0];
            n989Projeto_FatorMultiplicador = BC00298_n989Projeto_FatorMultiplicador[0];
            A990Projeto_ConstACocomo = BC00298_A990Projeto_ConstACocomo[0];
            n990Projeto_ConstACocomo = BC00298_n990Projeto_ConstACocomo[0];
            A1232Projeto_Incremental = BC00298_A1232Projeto_Incremental[0];
            n1232Projeto_Incremental = BC00298_n1232Projeto_Incremental[0];
            A983Projeto_TipoProjetoCod = BC00298_A983Projeto_TipoProjetoCod[0];
            n983Projeto_TipoProjetoCod = BC00298_n983Projeto_TipoProjetoCod[0];
            A655Projeto_Custo = BC00298_A655Projeto_Custo[0];
            A656Projeto_Prazo = BC00298_A656Projeto_Prazo[0];
            A657Projeto_Esforco = BC00298_A657Projeto_Esforco[0];
            ZM2986( -9) ;
         }
         pr_default.close(4);
         OnLoadActions2986( ) ;
      }

      protected void OnLoadActions2986( )
      {
         GXt_int1 = A740Projeto_SistemaCod;
         new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
         A740Projeto_SistemaCod = GXt_int1;
      }

      protected void CheckExtendedTable2986( )
      {
         standaloneModal( ) ;
         /* Using cursor BC00296 */
         pr_default.execute(3, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            A655Projeto_Custo = BC00296_A655Projeto_Custo[0];
            A656Projeto_Prazo = BC00296_A656Projeto_Prazo[0];
            A657Projeto_Esforco = BC00296_A657Projeto_Esforco[0];
         }
         else
         {
            A655Projeto_Custo = 0;
            A656Projeto_Prazo = 0;
            A657Projeto_Esforco = 0;
         }
         pr_default.close(3);
         GXt_int1 = A740Projeto_SistemaCod;
         new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
         A740Projeto_SistemaCod = GXt_int1;
         /* Using cursor BC00294 */
         pr_default.execute(2, new Object[] {n983Projeto_TipoProjetoCod, A983Projeto_TipoProjetoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A983Projeto_TipoProjetoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Projeto_Tipo Projeto'.", "ForeignKeyNotFound", 1, "PROJETO_TIPOPROJETOCOD");
               AnyError = 1;
            }
         }
         pr_default.close(2);
         if ( ! ( ( StringUtil.StrCmp(A651Projeto_TipoContagem, "D") == 0 ) || ( StringUtil.StrCmp(A651Projeto_TipoContagem, "M") == 0 ) || ( StringUtil.StrCmp(A651Projeto_TipoContagem, "C") == 0 ) || ( StringUtil.StrCmp(A651Projeto_TipoContagem, "A") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Tipo fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( StringUtil.StrCmp(A652Projeto_TecnicaContagem, "1") == 0 ) || ( StringUtil.StrCmp(A652Projeto_TecnicaContagem, "2") == 0 ) || ( StringUtil.StrCmp(A652Projeto_TecnicaContagem, "3") == 0 ) ) )
         {
            GX_msglist.addItem("Campo T�cnica fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A1541Projeto_Previsao) || ( A1541Projeto_Previsao >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Previs�o de Entrega fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( StringUtil.StrCmp(A658Projeto_Status, "A") == 0 ) || ( StringUtil.StrCmp(A658Projeto_Status, "E") == 0 ) || ( StringUtil.StrCmp(A658Projeto_Status, "C") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Status fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors2986( )
      {
         pr_default.close(3);
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey2986( )
      {
         /* Using cursor BC00299 */
         pr_default.execute(5, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound86 = 1;
         }
         else
         {
            RcdFound86 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00293 */
         pr_default.execute(1, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2986( 9) ;
            RcdFound86 = 1;
            A648Projeto_Codigo = BC00293_A648Projeto_Codigo[0];
            A649Projeto_Nome = BC00293_A649Projeto_Nome[0];
            A650Projeto_Sigla = BC00293_A650Projeto_Sigla[0];
            A651Projeto_TipoContagem = BC00293_A651Projeto_TipoContagem[0];
            A652Projeto_TecnicaContagem = BC00293_A652Projeto_TecnicaContagem[0];
            A1540Projeto_Introducao = BC00293_A1540Projeto_Introducao[0];
            n1540Projeto_Introducao = BC00293_n1540Projeto_Introducao[0];
            A653Projeto_Escopo = BC00293_A653Projeto_Escopo[0];
            A1541Projeto_Previsao = BC00293_A1541Projeto_Previsao[0];
            n1541Projeto_Previsao = BC00293_n1541Projeto_Previsao[0];
            A1542Projeto_GerenteCod = BC00293_A1542Projeto_GerenteCod[0];
            n1542Projeto_GerenteCod = BC00293_n1542Projeto_GerenteCod[0];
            A1543Projeto_ServicoCod = BC00293_A1543Projeto_ServicoCod[0];
            n1543Projeto_ServicoCod = BC00293_n1543Projeto_ServicoCod[0];
            A658Projeto_Status = BC00293_A658Projeto_Status[0];
            A985Projeto_FatorEscala = BC00293_A985Projeto_FatorEscala[0];
            n985Projeto_FatorEscala = BC00293_n985Projeto_FatorEscala[0];
            A989Projeto_FatorMultiplicador = BC00293_A989Projeto_FatorMultiplicador[0];
            n989Projeto_FatorMultiplicador = BC00293_n989Projeto_FatorMultiplicador[0];
            A990Projeto_ConstACocomo = BC00293_A990Projeto_ConstACocomo[0];
            n990Projeto_ConstACocomo = BC00293_n990Projeto_ConstACocomo[0];
            A1232Projeto_Incremental = BC00293_A1232Projeto_Incremental[0];
            n1232Projeto_Incremental = BC00293_n1232Projeto_Incremental[0];
            A983Projeto_TipoProjetoCod = BC00293_A983Projeto_TipoProjetoCod[0];
            n983Projeto_TipoProjetoCod = BC00293_n983Projeto_TipoProjetoCod[0];
            Z648Projeto_Codigo = A648Projeto_Codigo;
            sMode86 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load2986( ) ;
            if ( AnyError == 1 )
            {
               RcdFound86 = 0;
               InitializeNonKey2986( ) ;
            }
            Gx_mode = sMode86;
         }
         else
         {
            RcdFound86 = 0;
            InitializeNonKey2986( ) ;
            sMode86 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode86;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2986( ) ;
         if ( RcdFound86 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_290( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency2986( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00292 */
            pr_default.execute(0, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Projeto"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z649Projeto_Nome, BC00292_A649Projeto_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z650Projeto_Sigla, BC00292_A650Projeto_Sigla[0]) != 0 ) || ( StringUtil.StrCmp(Z651Projeto_TipoContagem, BC00292_A651Projeto_TipoContagem[0]) != 0 ) || ( StringUtil.StrCmp(Z652Projeto_TecnicaContagem, BC00292_A652Projeto_TecnicaContagem[0]) != 0 ) || ( Z1541Projeto_Previsao != BC00292_A1541Projeto_Previsao[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1542Projeto_GerenteCod != BC00292_A1542Projeto_GerenteCod[0] ) || ( Z1543Projeto_ServicoCod != BC00292_A1543Projeto_ServicoCod[0] ) || ( StringUtil.StrCmp(Z658Projeto_Status, BC00292_A658Projeto_Status[0]) != 0 ) || ( Z985Projeto_FatorEscala != BC00292_A985Projeto_FatorEscala[0] ) || ( Z989Projeto_FatorMultiplicador != BC00292_A989Projeto_FatorMultiplicador[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z990Projeto_ConstACocomo != BC00292_A990Projeto_ConstACocomo[0] ) || ( Z1232Projeto_Incremental != BC00292_A1232Projeto_Incremental[0] ) || ( Z983Projeto_TipoProjetoCod != BC00292_A983Projeto_TipoProjetoCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Projeto"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2986( )
      {
         BeforeValidate2986( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2986( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2986( 0) ;
            CheckOptimisticConcurrency2986( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2986( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2986( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002910 */
                     pr_default.execute(6, new Object[] {A649Projeto_Nome, A650Projeto_Sigla, A651Projeto_TipoContagem, A652Projeto_TecnicaContagem, n1540Projeto_Introducao, A1540Projeto_Introducao, A653Projeto_Escopo, n1541Projeto_Previsao, A1541Projeto_Previsao, n1542Projeto_GerenteCod, A1542Projeto_GerenteCod, n1543Projeto_ServicoCod, A1543Projeto_ServicoCod, A658Projeto_Status, n985Projeto_FatorEscala, A985Projeto_FatorEscala, n989Projeto_FatorMultiplicador, A989Projeto_FatorMultiplicador, n990Projeto_ConstACocomo, A990Projeto_ConstACocomo, n1232Projeto_Incremental, A1232Projeto_Incremental, n983Projeto_TipoProjetoCod, A983Projeto_TipoProjetoCod});
                     A648Projeto_Codigo = BC002910_A648Projeto_Codigo[0];
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Projeto") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2986( ) ;
            }
            EndLevel2986( ) ;
         }
         CloseExtendedTableCursors2986( ) ;
      }

      protected void Update2986( )
      {
         BeforeValidate2986( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2986( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2986( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2986( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2986( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002911 */
                     pr_default.execute(7, new Object[] {A649Projeto_Nome, A650Projeto_Sigla, A651Projeto_TipoContagem, A652Projeto_TecnicaContagem, n1540Projeto_Introducao, A1540Projeto_Introducao, A653Projeto_Escopo, n1541Projeto_Previsao, A1541Projeto_Previsao, n1542Projeto_GerenteCod, A1542Projeto_GerenteCod, n1543Projeto_ServicoCod, A1543Projeto_ServicoCod, A658Projeto_Status, n985Projeto_FatorEscala, A985Projeto_FatorEscala, n989Projeto_FatorMultiplicador, A989Projeto_FatorMultiplicador, n990Projeto_ConstACocomo, A990Projeto_ConstACocomo, n1232Projeto_Incremental, A1232Projeto_Incremental, n983Projeto_TipoProjetoCod, A983Projeto_TipoProjetoCod, A648Projeto_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("Projeto") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Projeto"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2986( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2986( ) ;
         }
         CloseExtendedTableCursors2986( ) ;
      }

      protected void DeferredUpdate2986( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate2986( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2986( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2986( ) ;
            AfterConfirm2986( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2986( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC002912 */
                  pr_default.execute(8, new Object[] {A648Projeto_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("Projeto") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode86 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel2986( ) ;
         Gx_mode = sMode86;
      }

      protected void OnDeleteControls2986( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC002914 */
            pr_default.execute(9, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(9) != 101) )
            {
               A655Projeto_Custo = BC002914_A655Projeto_Custo[0];
               A656Projeto_Prazo = BC002914_A656Projeto_Prazo[0];
               A657Projeto_Esforco = BC002914_A657Projeto_Esforco[0];
            }
            else
            {
               A655Projeto_Custo = 0;
               A656Projeto_Prazo = 0;
               A657Projeto_Esforco = 0;
            }
            pr_default.close(9);
            GXt_int1 = A740Projeto_SistemaCod;
            new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
            A740Projeto_SistemaCod = GXt_int1;
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC002915 */
            pr_default.execute(10, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(10) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Vari�vel Cocomo"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(10);
            /* Using cursor BC002916 */
            pr_default.execute(11, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(11) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor BC002917 */
            pr_default.execute(12, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Projeto Melhoria"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor BC002918 */
            pr_default.execute(13, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistema"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
            /* Using cursor BC002919 */
            pr_default.execute(14, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Projeto Desenvolvimento"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
            /* Using cursor BC002920 */
            pr_default.execute(15, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(15) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Proposta"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(15);
         }
      }

      protected void EndLevel2986( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2986( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart2986( )
      {
         /* Scan By routine */
         /* Using cursor BC002922 */
         pr_default.execute(16, new Object[] {A648Projeto_Codigo});
         RcdFound86 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound86 = 1;
            A648Projeto_Codigo = BC002922_A648Projeto_Codigo[0];
            A649Projeto_Nome = BC002922_A649Projeto_Nome[0];
            A650Projeto_Sigla = BC002922_A650Projeto_Sigla[0];
            A651Projeto_TipoContagem = BC002922_A651Projeto_TipoContagem[0];
            A652Projeto_TecnicaContagem = BC002922_A652Projeto_TecnicaContagem[0];
            A1540Projeto_Introducao = BC002922_A1540Projeto_Introducao[0];
            n1540Projeto_Introducao = BC002922_n1540Projeto_Introducao[0];
            A653Projeto_Escopo = BC002922_A653Projeto_Escopo[0];
            A1541Projeto_Previsao = BC002922_A1541Projeto_Previsao[0];
            n1541Projeto_Previsao = BC002922_n1541Projeto_Previsao[0];
            A1542Projeto_GerenteCod = BC002922_A1542Projeto_GerenteCod[0];
            n1542Projeto_GerenteCod = BC002922_n1542Projeto_GerenteCod[0];
            A1543Projeto_ServicoCod = BC002922_A1543Projeto_ServicoCod[0];
            n1543Projeto_ServicoCod = BC002922_n1543Projeto_ServicoCod[0];
            A658Projeto_Status = BC002922_A658Projeto_Status[0];
            A985Projeto_FatorEscala = BC002922_A985Projeto_FatorEscala[0];
            n985Projeto_FatorEscala = BC002922_n985Projeto_FatorEscala[0];
            A989Projeto_FatorMultiplicador = BC002922_A989Projeto_FatorMultiplicador[0];
            n989Projeto_FatorMultiplicador = BC002922_n989Projeto_FatorMultiplicador[0];
            A990Projeto_ConstACocomo = BC002922_A990Projeto_ConstACocomo[0];
            n990Projeto_ConstACocomo = BC002922_n990Projeto_ConstACocomo[0];
            A1232Projeto_Incremental = BC002922_A1232Projeto_Incremental[0];
            n1232Projeto_Incremental = BC002922_n1232Projeto_Incremental[0];
            A983Projeto_TipoProjetoCod = BC002922_A983Projeto_TipoProjetoCod[0];
            n983Projeto_TipoProjetoCod = BC002922_n983Projeto_TipoProjetoCod[0];
            A655Projeto_Custo = BC002922_A655Projeto_Custo[0];
            A656Projeto_Prazo = BC002922_A656Projeto_Prazo[0];
            A657Projeto_Esforco = BC002922_A657Projeto_Esforco[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext2986( )
      {
         /* Scan next routine */
         pr_default.readNext(16);
         RcdFound86 = 0;
         ScanKeyLoad2986( ) ;
      }

      protected void ScanKeyLoad2986( )
      {
         sMode86 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound86 = 1;
            A648Projeto_Codigo = BC002922_A648Projeto_Codigo[0];
            A649Projeto_Nome = BC002922_A649Projeto_Nome[0];
            A650Projeto_Sigla = BC002922_A650Projeto_Sigla[0];
            A651Projeto_TipoContagem = BC002922_A651Projeto_TipoContagem[0];
            A652Projeto_TecnicaContagem = BC002922_A652Projeto_TecnicaContagem[0];
            A1540Projeto_Introducao = BC002922_A1540Projeto_Introducao[0];
            n1540Projeto_Introducao = BC002922_n1540Projeto_Introducao[0];
            A653Projeto_Escopo = BC002922_A653Projeto_Escopo[0];
            A1541Projeto_Previsao = BC002922_A1541Projeto_Previsao[0];
            n1541Projeto_Previsao = BC002922_n1541Projeto_Previsao[0];
            A1542Projeto_GerenteCod = BC002922_A1542Projeto_GerenteCod[0];
            n1542Projeto_GerenteCod = BC002922_n1542Projeto_GerenteCod[0];
            A1543Projeto_ServicoCod = BC002922_A1543Projeto_ServicoCod[0];
            n1543Projeto_ServicoCod = BC002922_n1543Projeto_ServicoCod[0];
            A658Projeto_Status = BC002922_A658Projeto_Status[0];
            A985Projeto_FatorEscala = BC002922_A985Projeto_FatorEscala[0];
            n985Projeto_FatorEscala = BC002922_n985Projeto_FatorEscala[0];
            A989Projeto_FatorMultiplicador = BC002922_A989Projeto_FatorMultiplicador[0];
            n989Projeto_FatorMultiplicador = BC002922_n989Projeto_FatorMultiplicador[0];
            A990Projeto_ConstACocomo = BC002922_A990Projeto_ConstACocomo[0];
            n990Projeto_ConstACocomo = BC002922_n990Projeto_ConstACocomo[0];
            A1232Projeto_Incremental = BC002922_A1232Projeto_Incremental[0];
            n1232Projeto_Incremental = BC002922_n1232Projeto_Incremental[0];
            A983Projeto_TipoProjetoCod = BC002922_A983Projeto_TipoProjetoCod[0];
            n983Projeto_TipoProjetoCod = BC002922_n983Projeto_TipoProjetoCod[0];
            A655Projeto_Custo = BC002922_A655Projeto_Custo[0];
            A656Projeto_Prazo = BC002922_A656Projeto_Prazo[0];
            A657Projeto_Esforco = BC002922_A657Projeto_Esforco[0];
         }
         Gx_mode = sMode86;
      }

      protected void ScanKeyEnd2986( )
      {
         pr_default.close(16);
      }

      protected void AfterConfirm2986( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2986( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2986( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2986( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2986( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2986( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2986( )
      {
      }

      protected void AddRow2986( )
      {
         VarsToRow86( bcProjeto) ;
      }

      protected void ReadRow2986( )
      {
         RowToVars86( bcProjeto, 1) ;
      }

      protected void InitializeNonKey2986( )
      {
         A740Projeto_SistemaCod = 0;
         A649Projeto_Nome = "";
         A650Projeto_Sigla = "";
         A983Projeto_TipoProjetoCod = 0;
         n983Projeto_TipoProjetoCod = false;
         A651Projeto_TipoContagem = "";
         A652Projeto_TecnicaContagem = "";
         A1540Projeto_Introducao = "";
         n1540Projeto_Introducao = false;
         A653Projeto_Escopo = "";
         A1541Projeto_Previsao = DateTime.MinValue;
         n1541Projeto_Previsao = false;
         A1542Projeto_GerenteCod = 0;
         n1542Projeto_GerenteCod = false;
         A1543Projeto_ServicoCod = 0;
         n1543Projeto_ServicoCod = false;
         A655Projeto_Custo = 0;
         A656Projeto_Prazo = 0;
         A657Projeto_Esforco = 0;
         A985Projeto_FatorEscala = 0;
         n985Projeto_FatorEscala = false;
         A989Projeto_FatorMultiplicador = 0;
         n989Projeto_FatorMultiplicador = false;
         A990Projeto_ConstACocomo = 0;
         n990Projeto_ConstACocomo = false;
         A1232Projeto_Incremental = false;
         n1232Projeto_Incremental = false;
         A658Projeto_Status = "A";
         Z649Projeto_Nome = "";
         Z650Projeto_Sigla = "";
         Z651Projeto_TipoContagem = "";
         Z652Projeto_TecnicaContagem = "";
         Z1541Projeto_Previsao = DateTime.MinValue;
         Z1542Projeto_GerenteCod = 0;
         Z1543Projeto_ServicoCod = 0;
         Z658Projeto_Status = "";
         Z985Projeto_FatorEscala = 0;
         Z989Projeto_FatorMultiplicador = 0;
         Z990Projeto_ConstACocomo = 0;
         Z1232Projeto_Incremental = false;
         Z983Projeto_TipoProjetoCod = 0;
      }

      protected void InitAll2986( )
      {
         A648Projeto_Codigo = 0;
         InitializeNonKey2986( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A658Projeto_Status = i658Projeto_Status;
      }

      public void VarsToRow86( SdtProjeto obj86 )
      {
         obj86.gxTpr_Mode = Gx_mode;
         obj86.gxTpr_Projeto_sistemacod = A740Projeto_SistemaCod;
         obj86.gxTpr_Projeto_nome = A649Projeto_Nome;
         obj86.gxTpr_Projeto_sigla = A650Projeto_Sigla;
         obj86.gxTpr_Projeto_tipoprojetocod = A983Projeto_TipoProjetoCod;
         obj86.gxTpr_Projeto_tipocontagem = A651Projeto_TipoContagem;
         obj86.gxTpr_Projeto_tecnicacontagem = A652Projeto_TecnicaContagem;
         obj86.gxTpr_Projeto_introducao = A1540Projeto_Introducao;
         obj86.gxTpr_Projeto_escopo = A653Projeto_Escopo;
         obj86.gxTpr_Projeto_previsao = A1541Projeto_Previsao;
         obj86.gxTpr_Projeto_gerentecod = A1542Projeto_GerenteCod;
         obj86.gxTpr_Projeto_servicocod = A1543Projeto_ServicoCod;
         obj86.gxTpr_Projeto_custo = A655Projeto_Custo;
         obj86.gxTpr_Projeto_prazo = A656Projeto_Prazo;
         obj86.gxTpr_Projeto_esforco = A657Projeto_Esforco;
         obj86.gxTpr_Projeto_fatorescala = A985Projeto_FatorEscala;
         obj86.gxTpr_Projeto_fatormultiplicador = A989Projeto_FatorMultiplicador;
         obj86.gxTpr_Projeto_constacocomo = A990Projeto_ConstACocomo;
         obj86.gxTpr_Projeto_incremental = A1232Projeto_Incremental;
         obj86.gxTpr_Projeto_status = A658Projeto_Status;
         obj86.gxTpr_Projeto_codigo = A648Projeto_Codigo;
         obj86.gxTpr_Projeto_codigo_Z = Z648Projeto_Codigo;
         obj86.gxTpr_Projeto_nome_Z = Z649Projeto_Nome;
         obj86.gxTpr_Projeto_sigla_Z = Z650Projeto_Sigla;
         obj86.gxTpr_Projeto_tipoprojetocod_Z = Z983Projeto_TipoProjetoCod;
         obj86.gxTpr_Projeto_tipocontagem_Z = Z651Projeto_TipoContagem;
         obj86.gxTpr_Projeto_tecnicacontagem_Z = Z652Projeto_TecnicaContagem;
         obj86.gxTpr_Projeto_previsao_Z = Z1541Projeto_Previsao;
         obj86.gxTpr_Projeto_gerentecod_Z = Z1542Projeto_GerenteCod;
         obj86.gxTpr_Projeto_servicocod_Z = Z1543Projeto_ServicoCod;
         obj86.gxTpr_Projeto_custo_Z = Z655Projeto_Custo;
         obj86.gxTpr_Projeto_prazo_Z = Z656Projeto_Prazo;
         obj86.gxTpr_Projeto_esforco_Z = Z657Projeto_Esforco;
         obj86.gxTpr_Projeto_sistemacod_Z = Z740Projeto_SistemaCod;
         obj86.gxTpr_Projeto_status_Z = Z658Projeto_Status;
         obj86.gxTpr_Projeto_fatorescala_Z = Z985Projeto_FatorEscala;
         obj86.gxTpr_Projeto_fatormultiplicador_Z = Z989Projeto_FatorMultiplicador;
         obj86.gxTpr_Projeto_constacocomo_Z = Z990Projeto_ConstACocomo;
         obj86.gxTpr_Projeto_incremental_Z = Z1232Projeto_Incremental;
         obj86.gxTpr_Projeto_tipoprojetocod_N = (short)(Convert.ToInt16(n983Projeto_TipoProjetoCod));
         obj86.gxTpr_Projeto_introducao_N = (short)(Convert.ToInt16(n1540Projeto_Introducao));
         obj86.gxTpr_Projeto_previsao_N = (short)(Convert.ToInt16(n1541Projeto_Previsao));
         obj86.gxTpr_Projeto_gerentecod_N = (short)(Convert.ToInt16(n1542Projeto_GerenteCod));
         obj86.gxTpr_Projeto_servicocod_N = (short)(Convert.ToInt16(n1543Projeto_ServicoCod));
         obj86.gxTpr_Projeto_fatorescala_N = (short)(Convert.ToInt16(n985Projeto_FatorEscala));
         obj86.gxTpr_Projeto_fatormultiplicador_N = (short)(Convert.ToInt16(n989Projeto_FatorMultiplicador));
         obj86.gxTpr_Projeto_constacocomo_N = (short)(Convert.ToInt16(n990Projeto_ConstACocomo));
         obj86.gxTpr_Projeto_incremental_N = (short)(Convert.ToInt16(n1232Projeto_Incremental));
         obj86.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow86( SdtProjeto obj86 )
      {
         obj86.gxTpr_Projeto_codigo = A648Projeto_Codigo;
         return  ;
      }

      public void RowToVars86( SdtProjeto obj86 ,
                               int forceLoad )
      {
         Gx_mode = obj86.gxTpr_Mode;
         A740Projeto_SistemaCod = obj86.gxTpr_Projeto_sistemacod;
         A649Projeto_Nome = obj86.gxTpr_Projeto_nome;
         A650Projeto_Sigla = obj86.gxTpr_Projeto_sigla;
         A983Projeto_TipoProjetoCod = obj86.gxTpr_Projeto_tipoprojetocod;
         n983Projeto_TipoProjetoCod = false;
         A651Projeto_TipoContagem = obj86.gxTpr_Projeto_tipocontagem;
         A652Projeto_TecnicaContagem = obj86.gxTpr_Projeto_tecnicacontagem;
         A1540Projeto_Introducao = obj86.gxTpr_Projeto_introducao;
         n1540Projeto_Introducao = false;
         A653Projeto_Escopo = obj86.gxTpr_Projeto_escopo;
         A1541Projeto_Previsao = obj86.gxTpr_Projeto_previsao;
         n1541Projeto_Previsao = false;
         A1542Projeto_GerenteCod = obj86.gxTpr_Projeto_gerentecod;
         n1542Projeto_GerenteCod = false;
         A1543Projeto_ServicoCod = obj86.gxTpr_Projeto_servicocod;
         n1543Projeto_ServicoCod = false;
         A655Projeto_Custo = obj86.gxTpr_Projeto_custo;
         A656Projeto_Prazo = obj86.gxTpr_Projeto_prazo;
         A657Projeto_Esforco = obj86.gxTpr_Projeto_esforco;
         A985Projeto_FatorEscala = obj86.gxTpr_Projeto_fatorescala;
         n985Projeto_FatorEscala = false;
         A989Projeto_FatorMultiplicador = obj86.gxTpr_Projeto_fatormultiplicador;
         n989Projeto_FatorMultiplicador = false;
         A990Projeto_ConstACocomo = obj86.gxTpr_Projeto_constacocomo;
         n990Projeto_ConstACocomo = false;
         A1232Projeto_Incremental = obj86.gxTpr_Projeto_incremental;
         n1232Projeto_Incremental = false;
         A658Projeto_Status = obj86.gxTpr_Projeto_status;
         A648Projeto_Codigo = obj86.gxTpr_Projeto_codigo;
         Z648Projeto_Codigo = obj86.gxTpr_Projeto_codigo_Z;
         Z649Projeto_Nome = obj86.gxTpr_Projeto_nome_Z;
         Z650Projeto_Sigla = obj86.gxTpr_Projeto_sigla_Z;
         Z983Projeto_TipoProjetoCod = obj86.gxTpr_Projeto_tipoprojetocod_Z;
         Z651Projeto_TipoContagem = obj86.gxTpr_Projeto_tipocontagem_Z;
         Z652Projeto_TecnicaContagem = obj86.gxTpr_Projeto_tecnicacontagem_Z;
         Z1541Projeto_Previsao = obj86.gxTpr_Projeto_previsao_Z;
         Z1542Projeto_GerenteCod = obj86.gxTpr_Projeto_gerentecod_Z;
         Z1543Projeto_ServicoCod = obj86.gxTpr_Projeto_servicocod_Z;
         Z655Projeto_Custo = obj86.gxTpr_Projeto_custo_Z;
         Z656Projeto_Prazo = obj86.gxTpr_Projeto_prazo_Z;
         Z657Projeto_Esforco = obj86.gxTpr_Projeto_esforco_Z;
         Z740Projeto_SistemaCod = obj86.gxTpr_Projeto_sistemacod_Z;
         Z658Projeto_Status = obj86.gxTpr_Projeto_status_Z;
         Z985Projeto_FatorEscala = obj86.gxTpr_Projeto_fatorescala_Z;
         Z989Projeto_FatorMultiplicador = obj86.gxTpr_Projeto_fatormultiplicador_Z;
         Z990Projeto_ConstACocomo = obj86.gxTpr_Projeto_constacocomo_Z;
         Z1232Projeto_Incremental = obj86.gxTpr_Projeto_incremental_Z;
         n983Projeto_TipoProjetoCod = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_tipoprojetocod_N));
         n1540Projeto_Introducao = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_introducao_N));
         n1541Projeto_Previsao = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_previsao_N));
         n1542Projeto_GerenteCod = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_gerentecod_N));
         n1543Projeto_ServicoCod = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_servicocod_N));
         n985Projeto_FatorEscala = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_fatorescala_N));
         n989Projeto_FatorMultiplicador = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_fatormultiplicador_N));
         n990Projeto_ConstACocomo = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_constacocomo_N));
         n1232Projeto_Incremental = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_incremental_N));
         Gx_mode = obj86.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A648Projeto_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey2986( ) ;
         ScanKeyStart2986( ) ;
         if ( RcdFound86 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z648Projeto_Codigo = A648Projeto_Codigo;
         }
         ZM2986( -9) ;
         OnLoadActions2986( ) ;
         AddRow2986( ) ;
         ScanKeyEnd2986( ) ;
         if ( RcdFound86 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars86( bcProjeto, 0) ;
         ScanKeyStart2986( ) ;
         if ( RcdFound86 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z648Projeto_Codigo = A648Projeto_Codigo;
         }
         ZM2986( -9) ;
         OnLoadActions2986( ) ;
         AddRow2986( ) ;
         ScanKeyEnd2986( ) ;
         if ( RcdFound86 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars86( bcProjeto, 0) ;
         nKeyPressed = 1;
         GetKey2986( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert2986( ) ;
         }
         else
         {
            if ( RcdFound86 == 1 )
            {
               if ( A648Projeto_Codigo != Z648Projeto_Codigo )
               {
                  A648Projeto_Codigo = Z648Projeto_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update2986( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A648Projeto_Codigo != Z648Projeto_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert2986( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert2986( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow86( bcProjeto) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars86( bcProjeto, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey2986( ) ;
         if ( RcdFound86 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A648Projeto_Codigo != Z648Projeto_Codigo )
            {
               A648Projeto_Codigo = Z648Projeto_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A648Projeto_Codigo != Z648Projeto_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(9);
         context.RollbackDataStores( "Projeto_BC");
         VarsToRow86( bcProjeto) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcProjeto.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcProjeto.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcProjeto )
         {
            bcProjeto = (SdtProjeto)(sdt);
            if ( StringUtil.StrCmp(bcProjeto.gxTpr_Mode, "") == 0 )
            {
               bcProjeto.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow86( bcProjeto) ;
            }
            else
            {
               RowToVars86( bcProjeto, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcProjeto.gxTpr_Mode, "") == 0 )
            {
               bcProjeto.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars86( bcProjeto, 1) ;
         return  ;
      }

      public SdtProjeto Projeto_BC
      {
         get {
            return bcProjeto ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV19Pgmname = "";
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z649Projeto_Nome = "";
         A649Projeto_Nome = "";
         Z650Projeto_Sigla = "";
         A650Projeto_Sigla = "";
         Z651Projeto_TipoContagem = "";
         A651Projeto_TipoContagem = "";
         Z652Projeto_TecnicaContagem = "";
         A652Projeto_TecnicaContagem = "";
         Z1541Projeto_Previsao = DateTime.MinValue;
         A1541Projeto_Previsao = DateTime.MinValue;
         Z658Projeto_Status = "";
         A658Projeto_Status = "";
         Z1540Projeto_Introducao = "";
         A1540Projeto_Introducao = "";
         Z653Projeto_Escopo = "";
         A653Projeto_Escopo = "";
         BC00298_A648Projeto_Codigo = new int[1] ;
         BC00298_A649Projeto_Nome = new String[] {""} ;
         BC00298_A650Projeto_Sigla = new String[] {""} ;
         BC00298_A651Projeto_TipoContagem = new String[] {""} ;
         BC00298_A652Projeto_TecnicaContagem = new String[] {""} ;
         BC00298_A1540Projeto_Introducao = new String[] {""} ;
         BC00298_n1540Projeto_Introducao = new bool[] {false} ;
         BC00298_A653Projeto_Escopo = new String[] {""} ;
         BC00298_A1541Projeto_Previsao = new DateTime[] {DateTime.MinValue} ;
         BC00298_n1541Projeto_Previsao = new bool[] {false} ;
         BC00298_A1542Projeto_GerenteCod = new int[1] ;
         BC00298_n1542Projeto_GerenteCod = new bool[] {false} ;
         BC00298_A1543Projeto_ServicoCod = new int[1] ;
         BC00298_n1543Projeto_ServicoCod = new bool[] {false} ;
         BC00298_A658Projeto_Status = new String[] {""} ;
         BC00298_A985Projeto_FatorEscala = new decimal[1] ;
         BC00298_n985Projeto_FatorEscala = new bool[] {false} ;
         BC00298_A989Projeto_FatorMultiplicador = new decimal[1] ;
         BC00298_n989Projeto_FatorMultiplicador = new bool[] {false} ;
         BC00298_A990Projeto_ConstACocomo = new decimal[1] ;
         BC00298_n990Projeto_ConstACocomo = new bool[] {false} ;
         BC00298_A1232Projeto_Incremental = new bool[] {false} ;
         BC00298_n1232Projeto_Incremental = new bool[] {false} ;
         BC00298_A983Projeto_TipoProjetoCod = new int[1] ;
         BC00298_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         BC00298_A655Projeto_Custo = new decimal[1] ;
         BC00298_A656Projeto_Prazo = new short[1] ;
         BC00298_A657Projeto_Esforco = new short[1] ;
         BC00296_A655Projeto_Custo = new decimal[1] ;
         BC00296_A656Projeto_Prazo = new short[1] ;
         BC00296_A657Projeto_Esforco = new short[1] ;
         BC00294_A983Projeto_TipoProjetoCod = new int[1] ;
         BC00294_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         BC00299_A648Projeto_Codigo = new int[1] ;
         BC00293_A648Projeto_Codigo = new int[1] ;
         BC00293_A649Projeto_Nome = new String[] {""} ;
         BC00293_A650Projeto_Sigla = new String[] {""} ;
         BC00293_A651Projeto_TipoContagem = new String[] {""} ;
         BC00293_A652Projeto_TecnicaContagem = new String[] {""} ;
         BC00293_A1540Projeto_Introducao = new String[] {""} ;
         BC00293_n1540Projeto_Introducao = new bool[] {false} ;
         BC00293_A653Projeto_Escopo = new String[] {""} ;
         BC00293_A1541Projeto_Previsao = new DateTime[] {DateTime.MinValue} ;
         BC00293_n1541Projeto_Previsao = new bool[] {false} ;
         BC00293_A1542Projeto_GerenteCod = new int[1] ;
         BC00293_n1542Projeto_GerenteCod = new bool[] {false} ;
         BC00293_A1543Projeto_ServicoCod = new int[1] ;
         BC00293_n1543Projeto_ServicoCod = new bool[] {false} ;
         BC00293_A658Projeto_Status = new String[] {""} ;
         BC00293_A985Projeto_FatorEscala = new decimal[1] ;
         BC00293_n985Projeto_FatorEscala = new bool[] {false} ;
         BC00293_A989Projeto_FatorMultiplicador = new decimal[1] ;
         BC00293_n989Projeto_FatorMultiplicador = new bool[] {false} ;
         BC00293_A990Projeto_ConstACocomo = new decimal[1] ;
         BC00293_n990Projeto_ConstACocomo = new bool[] {false} ;
         BC00293_A1232Projeto_Incremental = new bool[] {false} ;
         BC00293_n1232Projeto_Incremental = new bool[] {false} ;
         BC00293_A983Projeto_TipoProjetoCod = new int[1] ;
         BC00293_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         sMode86 = "";
         BC00292_A648Projeto_Codigo = new int[1] ;
         BC00292_A649Projeto_Nome = new String[] {""} ;
         BC00292_A650Projeto_Sigla = new String[] {""} ;
         BC00292_A651Projeto_TipoContagem = new String[] {""} ;
         BC00292_A652Projeto_TecnicaContagem = new String[] {""} ;
         BC00292_A1540Projeto_Introducao = new String[] {""} ;
         BC00292_n1540Projeto_Introducao = new bool[] {false} ;
         BC00292_A653Projeto_Escopo = new String[] {""} ;
         BC00292_A1541Projeto_Previsao = new DateTime[] {DateTime.MinValue} ;
         BC00292_n1541Projeto_Previsao = new bool[] {false} ;
         BC00292_A1542Projeto_GerenteCod = new int[1] ;
         BC00292_n1542Projeto_GerenteCod = new bool[] {false} ;
         BC00292_A1543Projeto_ServicoCod = new int[1] ;
         BC00292_n1543Projeto_ServicoCod = new bool[] {false} ;
         BC00292_A658Projeto_Status = new String[] {""} ;
         BC00292_A985Projeto_FatorEscala = new decimal[1] ;
         BC00292_n985Projeto_FatorEscala = new bool[] {false} ;
         BC00292_A989Projeto_FatorMultiplicador = new decimal[1] ;
         BC00292_n989Projeto_FatorMultiplicador = new bool[] {false} ;
         BC00292_A990Projeto_ConstACocomo = new decimal[1] ;
         BC00292_n990Projeto_ConstACocomo = new bool[] {false} ;
         BC00292_A1232Projeto_Incremental = new bool[] {false} ;
         BC00292_n1232Projeto_Incremental = new bool[] {false} ;
         BC00292_A983Projeto_TipoProjetoCod = new int[1] ;
         BC00292_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         BC002910_A648Projeto_Codigo = new int[1] ;
         BC002914_A655Projeto_Custo = new decimal[1] ;
         BC002914_A656Projeto_Prazo = new short[1] ;
         BC002914_A657Projeto_Esforco = new short[1] ;
         BC002915_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         BC002915_A962VariavelCocomo_Sigla = new String[] {""} ;
         BC002915_A964VariavelCocomo_Tipo = new String[] {""} ;
         BC002915_A992VariavelCocomo_Sequencial = new short[1] ;
         BC002916_A192Contagem_Codigo = new int[1] ;
         BC002917_A736ProjetoMelhoria_Codigo = new int[1] ;
         BC002918_A127Sistema_Codigo = new int[1] ;
         BC002919_A669ProjetoDesenvolvimento_ProjetoCod = new int[1] ;
         BC002919_A670ProjetoDesenvolvimento_SistemaCod = new int[1] ;
         BC002920_A1685Proposta_Codigo = new int[1] ;
         BC002922_A648Projeto_Codigo = new int[1] ;
         BC002922_A649Projeto_Nome = new String[] {""} ;
         BC002922_A650Projeto_Sigla = new String[] {""} ;
         BC002922_A651Projeto_TipoContagem = new String[] {""} ;
         BC002922_A652Projeto_TecnicaContagem = new String[] {""} ;
         BC002922_A1540Projeto_Introducao = new String[] {""} ;
         BC002922_n1540Projeto_Introducao = new bool[] {false} ;
         BC002922_A653Projeto_Escopo = new String[] {""} ;
         BC002922_A1541Projeto_Previsao = new DateTime[] {DateTime.MinValue} ;
         BC002922_n1541Projeto_Previsao = new bool[] {false} ;
         BC002922_A1542Projeto_GerenteCod = new int[1] ;
         BC002922_n1542Projeto_GerenteCod = new bool[] {false} ;
         BC002922_A1543Projeto_ServicoCod = new int[1] ;
         BC002922_n1543Projeto_ServicoCod = new bool[] {false} ;
         BC002922_A658Projeto_Status = new String[] {""} ;
         BC002922_A985Projeto_FatorEscala = new decimal[1] ;
         BC002922_n985Projeto_FatorEscala = new bool[] {false} ;
         BC002922_A989Projeto_FatorMultiplicador = new decimal[1] ;
         BC002922_n989Projeto_FatorMultiplicador = new bool[] {false} ;
         BC002922_A990Projeto_ConstACocomo = new decimal[1] ;
         BC002922_n990Projeto_ConstACocomo = new bool[] {false} ;
         BC002922_A1232Projeto_Incremental = new bool[] {false} ;
         BC002922_n1232Projeto_Incremental = new bool[] {false} ;
         BC002922_A983Projeto_TipoProjetoCod = new int[1] ;
         BC002922_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         BC002922_A655Projeto_Custo = new decimal[1] ;
         BC002922_A656Projeto_Prazo = new short[1] ;
         BC002922_A657Projeto_Esforco = new short[1] ;
         i658Projeto_Status = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.projeto_bc__default(),
            new Object[][] {
                new Object[] {
               BC00292_A648Projeto_Codigo, BC00292_A649Projeto_Nome, BC00292_A650Projeto_Sigla, BC00292_A651Projeto_TipoContagem, BC00292_A652Projeto_TecnicaContagem, BC00292_A1540Projeto_Introducao, BC00292_n1540Projeto_Introducao, BC00292_A653Projeto_Escopo, BC00292_A1541Projeto_Previsao, BC00292_n1541Projeto_Previsao,
               BC00292_A1542Projeto_GerenteCod, BC00292_n1542Projeto_GerenteCod, BC00292_A1543Projeto_ServicoCod, BC00292_n1543Projeto_ServicoCod, BC00292_A658Projeto_Status, BC00292_A985Projeto_FatorEscala, BC00292_n985Projeto_FatorEscala, BC00292_A989Projeto_FatorMultiplicador, BC00292_n989Projeto_FatorMultiplicador, BC00292_A990Projeto_ConstACocomo,
               BC00292_n990Projeto_ConstACocomo, BC00292_A1232Projeto_Incremental, BC00292_n1232Projeto_Incremental, BC00292_A983Projeto_TipoProjetoCod, BC00292_n983Projeto_TipoProjetoCod
               }
               , new Object[] {
               BC00293_A648Projeto_Codigo, BC00293_A649Projeto_Nome, BC00293_A650Projeto_Sigla, BC00293_A651Projeto_TipoContagem, BC00293_A652Projeto_TecnicaContagem, BC00293_A1540Projeto_Introducao, BC00293_n1540Projeto_Introducao, BC00293_A653Projeto_Escopo, BC00293_A1541Projeto_Previsao, BC00293_n1541Projeto_Previsao,
               BC00293_A1542Projeto_GerenteCod, BC00293_n1542Projeto_GerenteCod, BC00293_A1543Projeto_ServicoCod, BC00293_n1543Projeto_ServicoCod, BC00293_A658Projeto_Status, BC00293_A985Projeto_FatorEscala, BC00293_n985Projeto_FatorEscala, BC00293_A989Projeto_FatorMultiplicador, BC00293_n989Projeto_FatorMultiplicador, BC00293_A990Projeto_ConstACocomo,
               BC00293_n990Projeto_ConstACocomo, BC00293_A1232Projeto_Incremental, BC00293_n1232Projeto_Incremental, BC00293_A983Projeto_TipoProjetoCod, BC00293_n983Projeto_TipoProjetoCod
               }
               , new Object[] {
               BC00294_A983Projeto_TipoProjetoCod
               }
               , new Object[] {
               BC00296_A655Projeto_Custo, BC00296_A656Projeto_Prazo, BC00296_A657Projeto_Esforco
               }
               , new Object[] {
               BC00298_A648Projeto_Codigo, BC00298_A649Projeto_Nome, BC00298_A650Projeto_Sigla, BC00298_A651Projeto_TipoContagem, BC00298_A652Projeto_TecnicaContagem, BC00298_A1540Projeto_Introducao, BC00298_n1540Projeto_Introducao, BC00298_A653Projeto_Escopo, BC00298_A1541Projeto_Previsao, BC00298_n1541Projeto_Previsao,
               BC00298_A1542Projeto_GerenteCod, BC00298_n1542Projeto_GerenteCod, BC00298_A1543Projeto_ServicoCod, BC00298_n1543Projeto_ServicoCod, BC00298_A658Projeto_Status, BC00298_A985Projeto_FatorEscala, BC00298_n985Projeto_FatorEscala, BC00298_A989Projeto_FatorMultiplicador, BC00298_n989Projeto_FatorMultiplicador, BC00298_A990Projeto_ConstACocomo,
               BC00298_n990Projeto_ConstACocomo, BC00298_A1232Projeto_Incremental, BC00298_n1232Projeto_Incremental, BC00298_A983Projeto_TipoProjetoCod, BC00298_n983Projeto_TipoProjetoCod, BC00298_A655Projeto_Custo, BC00298_A656Projeto_Prazo, BC00298_A657Projeto_Esforco
               }
               , new Object[] {
               BC00299_A648Projeto_Codigo
               }
               , new Object[] {
               BC002910_A648Projeto_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC002914_A655Projeto_Custo, BC002914_A656Projeto_Prazo, BC002914_A657Projeto_Esforco
               }
               , new Object[] {
               BC002915_A961VariavelCocomo_AreaTrabalhoCod, BC002915_A962VariavelCocomo_Sigla, BC002915_A964VariavelCocomo_Tipo, BC002915_A992VariavelCocomo_Sequencial
               }
               , new Object[] {
               BC002916_A192Contagem_Codigo
               }
               , new Object[] {
               BC002917_A736ProjetoMelhoria_Codigo
               }
               , new Object[] {
               BC002918_A127Sistema_Codigo
               }
               , new Object[] {
               BC002919_A669ProjetoDesenvolvimento_ProjetoCod, BC002919_A670ProjetoDesenvolvimento_SistemaCod
               }
               , new Object[] {
               BC002920_A1685Proposta_Codigo
               }
               , new Object[] {
               BC002922_A648Projeto_Codigo, BC002922_A649Projeto_Nome, BC002922_A650Projeto_Sigla, BC002922_A651Projeto_TipoContagem, BC002922_A652Projeto_TecnicaContagem, BC002922_A1540Projeto_Introducao, BC002922_n1540Projeto_Introducao, BC002922_A653Projeto_Escopo, BC002922_A1541Projeto_Previsao, BC002922_n1541Projeto_Previsao,
               BC002922_A1542Projeto_GerenteCod, BC002922_n1542Projeto_GerenteCod, BC002922_A1543Projeto_ServicoCod, BC002922_n1543Projeto_ServicoCod, BC002922_A658Projeto_Status, BC002922_A985Projeto_FatorEscala, BC002922_n985Projeto_FatorEscala, BC002922_A989Projeto_FatorMultiplicador, BC002922_n989Projeto_FatorMultiplicador, BC002922_A990Projeto_ConstACocomo,
               BC002922_n990Projeto_ConstACocomo, BC002922_A1232Projeto_Incremental, BC002922_n1232Projeto_Incremental, BC002922_A983Projeto_TipoProjetoCod, BC002922_n983Projeto_TipoProjetoCod, BC002922_A655Projeto_Custo, BC002922_A656Projeto_Prazo, BC002922_A657Projeto_Esforco
               }
            }
         );
         Z658Projeto_Status = "A";
         A658Projeto_Status = "A";
         i658Projeto_Status = "A";
         AV19Pgmname = "Projeto_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E12292 */
         E12292 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z656Projeto_Prazo ;
      private short A656Projeto_Prazo ;
      private short Z657Projeto_Esforco ;
      private short A657Projeto_Esforco ;
      private short Gx_BScreen ;
      private short RcdFound86 ;
      private int trnEnded ;
      private int Z648Projeto_Codigo ;
      private int A648Projeto_Codigo ;
      private int AV20GXV1 ;
      private int AV12Insert_Projeto_TipoProjetoCod ;
      private int AV7Projeto_Codigo ;
      private int Z1542Projeto_GerenteCod ;
      private int A1542Projeto_GerenteCod ;
      private int Z1543Projeto_ServicoCod ;
      private int A1543Projeto_ServicoCod ;
      private int Z983Projeto_TipoProjetoCod ;
      private int A983Projeto_TipoProjetoCod ;
      private int Z740Projeto_SistemaCod ;
      private int A740Projeto_SistemaCod ;
      private int GXt_int1 ;
      private decimal AV16FatorEscala ;
      private decimal AV15FatorMultiplicador ;
      private decimal Z985Projeto_FatorEscala ;
      private decimal A985Projeto_FatorEscala ;
      private decimal Z989Projeto_FatorMultiplicador ;
      private decimal A989Projeto_FatorMultiplicador ;
      private decimal Z990Projeto_ConstACocomo ;
      private decimal A990Projeto_ConstACocomo ;
      private decimal Z655Projeto_Custo ;
      private decimal A655Projeto_Custo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV19Pgmname ;
      private String Z649Projeto_Nome ;
      private String A649Projeto_Nome ;
      private String Z650Projeto_Sigla ;
      private String A650Projeto_Sigla ;
      private String Z651Projeto_TipoContagem ;
      private String A651Projeto_TipoContagem ;
      private String Z652Projeto_TecnicaContagem ;
      private String A652Projeto_TecnicaContagem ;
      private String Z658Projeto_Status ;
      private String A658Projeto_Status ;
      private String sMode86 ;
      private String i658Projeto_Status ;
      private DateTime Z1541Projeto_Previsao ;
      private DateTime A1541Projeto_Previsao ;
      private bool Z1232Projeto_Incremental ;
      private bool A1232Projeto_Incremental ;
      private bool n1540Projeto_Introducao ;
      private bool n1541Projeto_Previsao ;
      private bool n1542Projeto_GerenteCod ;
      private bool n1543Projeto_ServicoCod ;
      private bool n985Projeto_FatorEscala ;
      private bool n989Projeto_FatorMultiplicador ;
      private bool n990Projeto_ConstACocomo ;
      private bool n1232Projeto_Incremental ;
      private bool n983Projeto_TipoProjetoCod ;
      private bool Gx_longc ;
      private String Z1540Projeto_Introducao ;
      private String A1540Projeto_Introducao ;
      private String Z653Projeto_Escopo ;
      private String A653Projeto_Escopo ;
      private IGxSession AV10WebSession ;
      private SdtProjeto bcProjeto ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC00298_A648Projeto_Codigo ;
      private String[] BC00298_A649Projeto_Nome ;
      private String[] BC00298_A650Projeto_Sigla ;
      private String[] BC00298_A651Projeto_TipoContagem ;
      private String[] BC00298_A652Projeto_TecnicaContagem ;
      private String[] BC00298_A1540Projeto_Introducao ;
      private bool[] BC00298_n1540Projeto_Introducao ;
      private String[] BC00298_A653Projeto_Escopo ;
      private DateTime[] BC00298_A1541Projeto_Previsao ;
      private bool[] BC00298_n1541Projeto_Previsao ;
      private int[] BC00298_A1542Projeto_GerenteCod ;
      private bool[] BC00298_n1542Projeto_GerenteCod ;
      private int[] BC00298_A1543Projeto_ServicoCod ;
      private bool[] BC00298_n1543Projeto_ServicoCod ;
      private String[] BC00298_A658Projeto_Status ;
      private decimal[] BC00298_A985Projeto_FatorEscala ;
      private bool[] BC00298_n985Projeto_FatorEscala ;
      private decimal[] BC00298_A989Projeto_FatorMultiplicador ;
      private bool[] BC00298_n989Projeto_FatorMultiplicador ;
      private decimal[] BC00298_A990Projeto_ConstACocomo ;
      private bool[] BC00298_n990Projeto_ConstACocomo ;
      private bool[] BC00298_A1232Projeto_Incremental ;
      private bool[] BC00298_n1232Projeto_Incremental ;
      private int[] BC00298_A983Projeto_TipoProjetoCod ;
      private bool[] BC00298_n983Projeto_TipoProjetoCod ;
      private decimal[] BC00298_A655Projeto_Custo ;
      private short[] BC00298_A656Projeto_Prazo ;
      private short[] BC00298_A657Projeto_Esforco ;
      private decimal[] BC00296_A655Projeto_Custo ;
      private short[] BC00296_A656Projeto_Prazo ;
      private short[] BC00296_A657Projeto_Esforco ;
      private int[] BC00294_A983Projeto_TipoProjetoCod ;
      private bool[] BC00294_n983Projeto_TipoProjetoCod ;
      private int[] BC00299_A648Projeto_Codigo ;
      private int[] BC00293_A648Projeto_Codigo ;
      private String[] BC00293_A649Projeto_Nome ;
      private String[] BC00293_A650Projeto_Sigla ;
      private String[] BC00293_A651Projeto_TipoContagem ;
      private String[] BC00293_A652Projeto_TecnicaContagem ;
      private String[] BC00293_A1540Projeto_Introducao ;
      private bool[] BC00293_n1540Projeto_Introducao ;
      private String[] BC00293_A653Projeto_Escopo ;
      private DateTime[] BC00293_A1541Projeto_Previsao ;
      private bool[] BC00293_n1541Projeto_Previsao ;
      private int[] BC00293_A1542Projeto_GerenteCod ;
      private bool[] BC00293_n1542Projeto_GerenteCod ;
      private int[] BC00293_A1543Projeto_ServicoCod ;
      private bool[] BC00293_n1543Projeto_ServicoCod ;
      private String[] BC00293_A658Projeto_Status ;
      private decimal[] BC00293_A985Projeto_FatorEscala ;
      private bool[] BC00293_n985Projeto_FatorEscala ;
      private decimal[] BC00293_A989Projeto_FatorMultiplicador ;
      private bool[] BC00293_n989Projeto_FatorMultiplicador ;
      private decimal[] BC00293_A990Projeto_ConstACocomo ;
      private bool[] BC00293_n990Projeto_ConstACocomo ;
      private bool[] BC00293_A1232Projeto_Incremental ;
      private bool[] BC00293_n1232Projeto_Incremental ;
      private int[] BC00293_A983Projeto_TipoProjetoCod ;
      private bool[] BC00293_n983Projeto_TipoProjetoCod ;
      private int[] BC00292_A648Projeto_Codigo ;
      private String[] BC00292_A649Projeto_Nome ;
      private String[] BC00292_A650Projeto_Sigla ;
      private String[] BC00292_A651Projeto_TipoContagem ;
      private String[] BC00292_A652Projeto_TecnicaContagem ;
      private String[] BC00292_A1540Projeto_Introducao ;
      private bool[] BC00292_n1540Projeto_Introducao ;
      private String[] BC00292_A653Projeto_Escopo ;
      private DateTime[] BC00292_A1541Projeto_Previsao ;
      private bool[] BC00292_n1541Projeto_Previsao ;
      private int[] BC00292_A1542Projeto_GerenteCod ;
      private bool[] BC00292_n1542Projeto_GerenteCod ;
      private int[] BC00292_A1543Projeto_ServicoCod ;
      private bool[] BC00292_n1543Projeto_ServicoCod ;
      private String[] BC00292_A658Projeto_Status ;
      private decimal[] BC00292_A985Projeto_FatorEscala ;
      private bool[] BC00292_n985Projeto_FatorEscala ;
      private decimal[] BC00292_A989Projeto_FatorMultiplicador ;
      private bool[] BC00292_n989Projeto_FatorMultiplicador ;
      private decimal[] BC00292_A990Projeto_ConstACocomo ;
      private bool[] BC00292_n990Projeto_ConstACocomo ;
      private bool[] BC00292_A1232Projeto_Incremental ;
      private bool[] BC00292_n1232Projeto_Incremental ;
      private int[] BC00292_A983Projeto_TipoProjetoCod ;
      private bool[] BC00292_n983Projeto_TipoProjetoCod ;
      private int[] BC002910_A648Projeto_Codigo ;
      private decimal[] BC002914_A655Projeto_Custo ;
      private short[] BC002914_A656Projeto_Prazo ;
      private short[] BC002914_A657Projeto_Esforco ;
      private int[] BC002915_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] BC002915_A962VariavelCocomo_Sigla ;
      private String[] BC002915_A964VariavelCocomo_Tipo ;
      private short[] BC002915_A992VariavelCocomo_Sequencial ;
      private int[] BC002916_A192Contagem_Codigo ;
      private int[] BC002917_A736ProjetoMelhoria_Codigo ;
      private int[] BC002918_A127Sistema_Codigo ;
      private int[] BC002919_A669ProjetoDesenvolvimento_ProjetoCod ;
      private int[] BC002919_A670ProjetoDesenvolvimento_SistemaCod ;
      private int[] BC002920_A1685Proposta_Codigo ;
      private int[] BC002922_A648Projeto_Codigo ;
      private String[] BC002922_A649Projeto_Nome ;
      private String[] BC002922_A650Projeto_Sigla ;
      private String[] BC002922_A651Projeto_TipoContagem ;
      private String[] BC002922_A652Projeto_TecnicaContagem ;
      private String[] BC002922_A1540Projeto_Introducao ;
      private bool[] BC002922_n1540Projeto_Introducao ;
      private String[] BC002922_A653Projeto_Escopo ;
      private DateTime[] BC002922_A1541Projeto_Previsao ;
      private bool[] BC002922_n1541Projeto_Previsao ;
      private int[] BC002922_A1542Projeto_GerenteCod ;
      private bool[] BC002922_n1542Projeto_GerenteCod ;
      private int[] BC002922_A1543Projeto_ServicoCod ;
      private bool[] BC002922_n1543Projeto_ServicoCod ;
      private String[] BC002922_A658Projeto_Status ;
      private decimal[] BC002922_A985Projeto_FatorEscala ;
      private bool[] BC002922_n985Projeto_FatorEscala ;
      private decimal[] BC002922_A989Projeto_FatorMultiplicador ;
      private bool[] BC002922_n989Projeto_FatorMultiplicador ;
      private decimal[] BC002922_A990Projeto_ConstACocomo ;
      private bool[] BC002922_n990Projeto_ConstACocomo ;
      private bool[] BC002922_A1232Projeto_Incremental ;
      private bool[] BC002922_n1232Projeto_Incremental ;
      private int[] BC002922_A983Projeto_TipoProjetoCod ;
      private bool[] BC002922_n983Projeto_TipoProjetoCod ;
      private decimal[] BC002922_A655Projeto_Custo ;
      private short[] BC002922_A656Projeto_Prazo ;
      private short[] BC002922_A657Projeto_Esforco ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class projeto_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00298 ;
          prmBC00298 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00296 ;
          prmBC00296 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00294 ;
          prmBC00294 = new Object[] {
          new Object[] {"@Projeto_TipoProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00299 ;
          prmBC00299 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00293 ;
          prmBC00293 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00292 ;
          prmBC00292 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002910 ;
          prmBC002910 = new Object[] {
          new Object[] {"@Projeto_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Projeto_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Projeto_TipoContagem",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_TecnicaContagem",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_Introducao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_Escopo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_Previsao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Projeto_GerenteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_FatorEscala",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_FatorMultiplicador",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_ConstACocomo",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_Incremental",SqlDbType.Bit,4,0} ,
          new Object[] {"@Projeto_TipoProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002911 ;
          prmBC002911 = new Object[] {
          new Object[] {"@Projeto_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Projeto_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Projeto_TipoContagem",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_TecnicaContagem",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_Introducao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_Escopo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_Previsao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Projeto_GerenteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_FatorEscala",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_FatorMultiplicador",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_ConstACocomo",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_Incremental",SqlDbType.Bit,4,0} ,
          new Object[] {"@Projeto_TipoProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002912 ;
          prmBC002912 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002914 ;
          prmBC002914 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002915 ;
          prmBC002915 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002916 ;
          prmBC002916 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002917 ;
          prmBC002917 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002918 ;
          prmBC002918 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002919 ;
          prmBC002919 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002920 ;
          prmBC002920 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002922 ;
          prmBC002922 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00292", "SELECT [Projeto_Codigo], [Projeto_Nome], [Projeto_Sigla], [Projeto_TipoContagem], [Projeto_TecnicaContagem], [Projeto_Introducao], [Projeto_Escopo], [Projeto_Previsao], [Projeto_GerenteCod], [Projeto_ServicoCod], [Projeto_Status], [Projeto_FatorEscala], [Projeto_FatorMultiplicador], [Projeto_ConstACocomo], [Projeto_Incremental], [Projeto_TipoProjetoCod] AS Projeto_TipoProjetoCod FROM [Projeto] WITH (UPDLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00292,1,0,true,false )
             ,new CursorDef("BC00293", "SELECT [Projeto_Codigo], [Projeto_Nome], [Projeto_Sigla], [Projeto_TipoContagem], [Projeto_TecnicaContagem], [Projeto_Introducao], [Projeto_Escopo], [Projeto_Previsao], [Projeto_GerenteCod], [Projeto_ServicoCod], [Projeto_Status], [Projeto_FatorEscala], [Projeto_FatorMultiplicador], [Projeto_ConstACocomo], [Projeto_Incremental], [Projeto_TipoProjetoCod] AS Projeto_TipoProjetoCod FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00293,1,0,true,false )
             ,new CursorDef("BC00294", "SELECT [TipoProjeto_Codigo] AS Projeto_TipoProjetoCod FROM [TipoProjeto] WITH (NOLOCK) WHERE [TipoProjeto_Codigo] = @Projeto_TipoProjetoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00294,1,0,true,false )
             ,new CursorDef("BC00296", "SELECT COALESCE( T1.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T1.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T1.[Projeto_Esforco], 0) AS Projeto_Esforco FROM (SELECT SUM([Sistema_Custo]) AS Projeto_Custo, [Sistema_ProjetoCod], SUM([Sistema_Prazo]) AS Projeto_Prazo, SUM([Sistema_Esforco]) AS Projeto_Esforco FROM [Sistema] WITH (NOLOCK) GROUP BY [Sistema_ProjetoCod] ) T1 WHERE T1.[Sistema_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00296,1,0,true,false )
             ,new CursorDef("BC00298", "SELECT TM1.[Projeto_Codigo], TM1.[Projeto_Nome], TM1.[Projeto_Sigla], TM1.[Projeto_TipoContagem], TM1.[Projeto_TecnicaContagem], TM1.[Projeto_Introducao], TM1.[Projeto_Escopo], TM1.[Projeto_Previsao], TM1.[Projeto_GerenteCod], TM1.[Projeto_ServicoCod], TM1.[Projeto_Status], TM1.[Projeto_FatorEscala], TM1.[Projeto_FatorMultiplicador], TM1.[Projeto_ConstACocomo], TM1.[Projeto_Incremental], TM1.[Projeto_TipoProjetoCod] AS Projeto_TipoProjetoCod, COALESCE( T2.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T2.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T2.[Projeto_Esforco], 0) AS Projeto_Esforco FROM ([Projeto] TM1 WITH (NOLOCK) LEFT JOIN (SELECT SUM([Sistema_Custo]) AS Projeto_Custo, [Sistema_ProjetoCod], SUM([Sistema_Prazo]) AS Projeto_Prazo, SUM([Sistema_Esforco]) AS Projeto_Esforco FROM [Sistema] WITH (NOLOCK) GROUP BY [Sistema_ProjetoCod] ) T2 ON T2.[Sistema_ProjetoCod] = TM1.[Projeto_Codigo]) WHERE TM1.[Projeto_Codigo] = @Projeto_Codigo ORDER BY TM1.[Projeto_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00298,100,0,true,false )
             ,new CursorDef("BC00299", "SELECT [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00299,1,0,true,false )
             ,new CursorDef("BC002910", "INSERT INTO [Projeto]([Projeto_Nome], [Projeto_Sigla], [Projeto_TipoContagem], [Projeto_TecnicaContagem], [Projeto_Introducao], [Projeto_Escopo], [Projeto_Previsao], [Projeto_GerenteCod], [Projeto_ServicoCod], [Projeto_Status], [Projeto_FatorEscala], [Projeto_FatorMultiplicador], [Projeto_ConstACocomo], [Projeto_Incremental], [Projeto_TipoProjetoCod]) VALUES(@Projeto_Nome, @Projeto_Sigla, @Projeto_TipoContagem, @Projeto_TecnicaContagem, @Projeto_Introducao, @Projeto_Escopo, @Projeto_Previsao, @Projeto_GerenteCod, @Projeto_ServicoCod, @Projeto_Status, @Projeto_FatorEscala, @Projeto_FatorMultiplicador, @Projeto_ConstACocomo, @Projeto_Incremental, @Projeto_TipoProjetoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC002910)
             ,new CursorDef("BC002911", "UPDATE [Projeto] SET [Projeto_Nome]=@Projeto_Nome, [Projeto_Sigla]=@Projeto_Sigla, [Projeto_TipoContagem]=@Projeto_TipoContagem, [Projeto_TecnicaContagem]=@Projeto_TecnicaContagem, [Projeto_Introducao]=@Projeto_Introducao, [Projeto_Escopo]=@Projeto_Escopo, [Projeto_Previsao]=@Projeto_Previsao, [Projeto_GerenteCod]=@Projeto_GerenteCod, [Projeto_ServicoCod]=@Projeto_ServicoCod, [Projeto_Status]=@Projeto_Status, [Projeto_FatorEscala]=@Projeto_FatorEscala, [Projeto_FatorMultiplicador]=@Projeto_FatorMultiplicador, [Projeto_ConstACocomo]=@Projeto_ConstACocomo, [Projeto_Incremental]=@Projeto_Incremental, [Projeto_TipoProjetoCod]=@Projeto_TipoProjetoCod  WHERE [Projeto_Codigo] = @Projeto_Codigo", GxErrorMask.GX_NOMASK,prmBC002911)
             ,new CursorDef("BC002912", "DELETE FROM [Projeto]  WHERE [Projeto_Codigo] = @Projeto_Codigo", GxErrorMask.GX_NOMASK,prmBC002912)
             ,new CursorDef("BC002914", "SELECT COALESCE( T1.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T1.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T1.[Projeto_Esforco], 0) AS Projeto_Esforco FROM (SELECT SUM([Sistema_Custo]) AS Projeto_Custo, [Sistema_ProjetoCod], SUM([Sistema_Prazo]) AS Projeto_Prazo, SUM([Sistema_Esforco]) AS Projeto_Esforco FROM [Sistema] WITH (NOLOCK) GROUP BY [Sistema_ProjetoCod] ) T1 WHERE T1.[Sistema_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002914,1,0,true,false )
             ,new CursorDef("BC002915", "SELECT TOP 1 [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial] FROM [VariaveisCocomo] WITH (NOLOCK) WHERE [VariavelCocomo_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002915,1,0,true,true )
             ,new CursorDef("BC002916", "SELECT TOP 1 [Contagem_Codigo] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002916,1,0,true,true )
             ,new CursorDef("BC002917", "SELECT TOP 1 [ProjetoMelhoria_Codigo] FROM [ProjetoMelhoria] WITH (NOLOCK) WHERE [ProjetoMelhoria_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002917,1,0,true,true )
             ,new CursorDef("BC002918", "SELECT TOP 1 [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002918,1,0,true,true )
             ,new CursorDef("BC002919", "SELECT TOP 1 [ProjetoDesenvolvimento_ProjetoCod], [ProjetoDesenvolvimento_SistemaCod] FROM [ProjetoDesenvolvimento] WITH (NOLOCK) WHERE [ProjetoDesenvolvimento_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002919,1,0,true,true )
             ,new CursorDef("BC002920", "SELECT TOP 1 [Proposta_Codigo] FROM dbo.[Proposta] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002920,1,0,true,true )
             ,new CursorDef("BC002922", "SELECT TM1.[Projeto_Codigo], TM1.[Projeto_Nome], TM1.[Projeto_Sigla], TM1.[Projeto_TipoContagem], TM1.[Projeto_TecnicaContagem], TM1.[Projeto_Introducao], TM1.[Projeto_Escopo], TM1.[Projeto_Previsao], TM1.[Projeto_GerenteCod], TM1.[Projeto_ServicoCod], TM1.[Projeto_Status], TM1.[Projeto_FatorEscala], TM1.[Projeto_FatorMultiplicador], TM1.[Projeto_ConstACocomo], TM1.[Projeto_Incremental], TM1.[Projeto_TipoProjetoCod] AS Projeto_TipoProjetoCod, COALESCE( T2.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T2.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T2.[Projeto_Esforco], 0) AS Projeto_Esforco FROM ([Projeto] TM1 WITH (NOLOCK) LEFT JOIN (SELECT SUM([Sistema_Custo]) AS Projeto_Custo, [Sistema_ProjetoCod], SUM([Sistema_Prazo]) AS Projeto_Prazo, SUM([Sistema_Esforco]) AS Projeto_Esforco FROM [Sistema] WITH (NOLOCK) GROUP BY [Sistema_ProjetoCod] ) T2 ON T2.[Sistema_ProjetoCod] = TM1.[Projeto_Codigo]) WHERE TM1.[Projeto_Codigo] = @Projeto_Codigo ORDER BY TM1.[Projeto_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002922,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 1) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((String[]) buf[14])[0] = rslt.getString(11, 1) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(12);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(13);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                ((bool[]) buf[21])[0] = rslt.getBool(15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((int[]) buf[23])[0] = rslt.getInt(16) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 1) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((String[]) buf[14])[0] = rslt.getString(11, 1) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(12);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(13);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                ((bool[]) buf[21])[0] = rslt.getBool(15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((int[]) buf[23])[0] = rslt.getInt(16) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 1) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((String[]) buf[14])[0] = rslt.getString(11, 1) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(12);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(13);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                ((bool[]) buf[21])[0] = rslt.getBool(15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((int[]) buf[23])[0] = rslt.getInt(16) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                ((decimal[]) buf[25])[0] = rslt.getDecimal(17) ;
                ((short[]) buf[26])[0] = rslt.getShort(18) ;
                ((short[]) buf[27])[0] = rslt.getShort(19) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 1) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((String[]) buf[14])[0] = rslt.getString(11, 1) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(12);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(13);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                ((bool[]) buf[21])[0] = rslt.getBool(15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((int[]) buf[23])[0] = rslt.getInt(16) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                ((decimal[]) buf[25])[0] = rslt.getDecimal(17) ;
                ((short[]) buf[26])[0] = rslt.getShort(18) ;
                ((short[]) buf[27])[0] = rslt.getShort(19) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[5]);
                }
                stmt.SetParameter(6, (String)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(7, (DateTime)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[12]);
                }
                stmt.SetParameter(10, (String)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 13 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(13, (decimal)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 14 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(14, (bool)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[23]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[5]);
                }
                stmt.SetParameter(6, (String)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(7, (DateTime)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[12]);
                }
                stmt.SetParameter(10, (String)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 13 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(13, (decimal)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 14 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(14, (bool)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[23]);
                }
                stmt.SetParameter(16, (int)parms[24]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
