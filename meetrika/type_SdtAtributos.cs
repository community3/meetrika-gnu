/*
               File: type_SdtAtributos
        Description: Atributos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:51.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Atributos" )]
   [XmlType(TypeName =  "Atributos" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtAtributos : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtAtributos( )
      {
         /* Constructor for serialization */
         gxTv_SdtAtributos_Atributos_nome = "";
         gxTv_SdtAtributos_Atributos_detalhes = "";
         gxTv_SdtAtributos_Atributos_descricao = "";
         gxTv_SdtAtributos_Atributos_tipodados = "";
         gxTv_SdtAtributos_Atributos_tabelanom = "";
         gxTv_SdtAtributos_Mode = "";
         gxTv_SdtAtributos_Atributos_nome_Z = "";
         gxTv_SdtAtributos_Atributos_detalhes_Z = "";
         gxTv_SdtAtributos_Atributos_tipodados_Z = "";
         gxTv_SdtAtributos_Atributos_tabelanom_Z = "";
      }

      public SdtAtributos( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV176Atributos_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV176Atributos_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Atributos_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Atributos");
         metadata.Set("BT", "Atributos");
         metadata.Set("PK", "[ \"Atributos_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"Atributos_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Atributos_Codigo\" ],\"FKMap\":[ \"Atributos_MelhoraCod-Atributos_Codigo\" ] },{ \"FK\":[ \"Tabela_Codigo\" ],\"FKMap\":[ \"Atributos_TabelaCod-Tabela_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Atributos_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Atributos_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Atributos_detalhes_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Atributos_tipodados_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Atributos_pk_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Atributos_fk_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Atributos_tabelacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Atributos_tabelanom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_sistemacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Atributos_melhoracod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Atributos_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Atributos_detalhes_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Atributos_descricao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Atributos_tipodados_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Atributos_pk_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Atributos_fk_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Atributos_tabelanom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_sistemacod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Atributos_melhoracod_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtAtributos deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtAtributos)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtAtributos obj ;
         obj = this;
         obj.gxTpr_Atributos_codigo = deserialized.gxTpr_Atributos_codigo;
         obj.gxTpr_Atributos_nome = deserialized.gxTpr_Atributos_nome;
         obj.gxTpr_Atributos_detalhes = deserialized.gxTpr_Atributos_detalhes;
         obj.gxTpr_Atributos_descricao = deserialized.gxTpr_Atributos_descricao;
         obj.gxTpr_Atributos_tipodados = deserialized.gxTpr_Atributos_tipodados;
         obj.gxTpr_Atributos_pk = deserialized.gxTpr_Atributos_pk;
         obj.gxTpr_Atributos_fk = deserialized.gxTpr_Atributos_fk;
         obj.gxTpr_Atributos_tabelacod = deserialized.gxTpr_Atributos_tabelacod;
         obj.gxTpr_Atributos_tabelanom = deserialized.gxTpr_Atributos_tabelanom;
         obj.gxTpr_Tabela_sistemacod = deserialized.gxTpr_Tabela_sistemacod;
         obj.gxTpr_Atributos_melhoracod = deserialized.gxTpr_Atributos_melhoracod;
         obj.gxTpr_Atributos_ativo = deserialized.gxTpr_Atributos_ativo;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Atributos_codigo_Z = deserialized.gxTpr_Atributos_codigo_Z;
         obj.gxTpr_Atributos_nome_Z = deserialized.gxTpr_Atributos_nome_Z;
         obj.gxTpr_Atributos_detalhes_Z = deserialized.gxTpr_Atributos_detalhes_Z;
         obj.gxTpr_Atributos_tipodados_Z = deserialized.gxTpr_Atributos_tipodados_Z;
         obj.gxTpr_Atributos_pk_Z = deserialized.gxTpr_Atributos_pk_Z;
         obj.gxTpr_Atributos_fk_Z = deserialized.gxTpr_Atributos_fk_Z;
         obj.gxTpr_Atributos_tabelacod_Z = deserialized.gxTpr_Atributos_tabelacod_Z;
         obj.gxTpr_Atributos_tabelanom_Z = deserialized.gxTpr_Atributos_tabelanom_Z;
         obj.gxTpr_Tabela_sistemacod_Z = deserialized.gxTpr_Tabela_sistemacod_Z;
         obj.gxTpr_Atributos_melhoracod_Z = deserialized.gxTpr_Atributos_melhoracod_Z;
         obj.gxTpr_Atributos_ativo_Z = deserialized.gxTpr_Atributos_ativo_Z;
         obj.gxTpr_Atributos_detalhes_N = deserialized.gxTpr_Atributos_detalhes_N;
         obj.gxTpr_Atributos_descricao_N = deserialized.gxTpr_Atributos_descricao_N;
         obj.gxTpr_Atributos_tipodados_N = deserialized.gxTpr_Atributos_tipodados_N;
         obj.gxTpr_Atributos_pk_N = deserialized.gxTpr_Atributos_pk_N;
         obj.gxTpr_Atributos_fk_N = deserialized.gxTpr_Atributos_fk_N;
         obj.gxTpr_Atributos_tabelanom_N = deserialized.gxTpr_Atributos_tabelanom_N;
         obj.gxTpr_Tabela_sistemacod_N = deserialized.gxTpr_Tabela_sistemacod_N;
         obj.gxTpr_Atributos_melhoracod_N = deserialized.gxTpr_Atributos_melhoracod_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_Codigo") )
               {
                  gxTv_SdtAtributos_Atributos_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_Nome") )
               {
                  gxTv_SdtAtributos_Atributos_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_Detalhes") )
               {
                  gxTv_SdtAtributos_Atributos_detalhes = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_Descricao") )
               {
                  gxTv_SdtAtributos_Atributos_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_TipoDados") )
               {
                  gxTv_SdtAtributos_Atributos_tipodados = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_PK") )
               {
                  gxTv_SdtAtributos_Atributos_pk = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_FK") )
               {
                  gxTv_SdtAtributos_Atributos_fk = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_TabelaCod") )
               {
                  gxTv_SdtAtributos_Atributos_tabelacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_TabelaNom") )
               {
                  gxTv_SdtAtributos_Atributos_tabelanom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_SistemaCod") )
               {
                  gxTv_SdtAtributos_Tabela_sistemacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_MelhoraCod") )
               {
                  gxTv_SdtAtributos_Atributos_melhoracod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_Ativo") )
               {
                  gxTv_SdtAtributos_Atributos_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtAtributos_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtAtributos_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_Codigo_Z") )
               {
                  gxTv_SdtAtributos_Atributos_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_Nome_Z") )
               {
                  gxTv_SdtAtributos_Atributos_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_Detalhes_Z") )
               {
                  gxTv_SdtAtributos_Atributos_detalhes_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_TipoDados_Z") )
               {
                  gxTv_SdtAtributos_Atributos_tipodados_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_PK_Z") )
               {
                  gxTv_SdtAtributos_Atributos_pk_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_FK_Z") )
               {
                  gxTv_SdtAtributos_Atributos_fk_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_TabelaCod_Z") )
               {
                  gxTv_SdtAtributos_Atributos_tabelacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_TabelaNom_Z") )
               {
                  gxTv_SdtAtributos_Atributos_tabelanom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_SistemaCod_Z") )
               {
                  gxTv_SdtAtributos_Tabela_sistemacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_MelhoraCod_Z") )
               {
                  gxTv_SdtAtributos_Atributos_melhoracod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_Ativo_Z") )
               {
                  gxTv_SdtAtributos_Atributos_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_Detalhes_N") )
               {
                  gxTv_SdtAtributos_Atributos_detalhes_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_Descricao_N") )
               {
                  gxTv_SdtAtributos_Atributos_descricao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_TipoDados_N") )
               {
                  gxTv_SdtAtributos_Atributos_tipodados_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_PK_N") )
               {
                  gxTv_SdtAtributos_Atributos_pk_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_FK_N") )
               {
                  gxTv_SdtAtributos_Atributos_fk_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_TabelaNom_N") )
               {
                  gxTv_SdtAtributos_Atributos_tabelanom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_SistemaCod_N") )
               {
                  gxTv_SdtAtributos_Tabela_sistemacod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atributos_MelhoraCod_N") )
               {
                  gxTv_SdtAtributos_Atributos_melhoracod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Atributos";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Atributos_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAtributos_Atributos_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Atributos_Nome", StringUtil.RTrim( gxTv_SdtAtributos_Atributos_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Atributos_Detalhes", StringUtil.RTrim( gxTv_SdtAtributos_Atributos_detalhes));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Atributos_Descricao", StringUtil.RTrim( gxTv_SdtAtributos_Atributos_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Atributos_TipoDados", StringUtil.RTrim( gxTv_SdtAtributos_Atributos_tipodados));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Atributos_PK", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtAtributos_Atributos_pk)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Atributos_FK", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtAtributos_Atributos_fk)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Atributos_TabelaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAtributos_Atributos_tabelacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Atributos_TabelaNom", StringUtil.RTrim( gxTv_SdtAtributos_Atributos_tabelanom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Tabela_SistemaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAtributos_Tabela_sistemacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Atributos_MelhoraCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAtributos_Atributos_melhoracod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Atributos_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtAtributos_Atributos_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtAtributos_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAtributos_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Atributos_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAtributos_Atributos_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Atributos_Nome_Z", StringUtil.RTrim( gxTv_SdtAtributos_Atributos_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Atributos_Detalhes_Z", StringUtil.RTrim( gxTv_SdtAtributos_Atributos_detalhes_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Atributos_TipoDados_Z", StringUtil.RTrim( gxTv_SdtAtributos_Atributos_tipodados_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Atributos_PK_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtAtributos_Atributos_pk_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Atributos_FK_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtAtributos_Atributos_fk_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Atributos_TabelaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAtributos_Atributos_tabelacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Atributos_TabelaNom_Z", StringUtil.RTrim( gxTv_SdtAtributos_Atributos_tabelanom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_SistemaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAtributos_Tabela_sistemacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Atributos_MelhoraCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAtributos_Atributos_melhoracod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Atributos_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtAtributos_Atributos_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Atributos_Detalhes_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAtributos_Atributos_detalhes_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Atributos_Descricao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAtributos_Atributos_descricao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Atributos_TipoDados_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAtributos_Atributos_tipodados_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Atributos_PK_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAtributos_Atributos_pk_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Atributos_FK_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAtributos_Atributos_fk_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Atributos_TabelaNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAtributos_Atributos_tabelanom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_SistemaCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAtributos_Tabela_sistemacod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Atributos_MelhoraCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAtributos_Atributos_melhoracod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Atributos_Codigo", gxTv_SdtAtributos_Atributos_codigo, false);
         AddObjectProperty("Atributos_Nome", gxTv_SdtAtributos_Atributos_nome, false);
         AddObjectProperty("Atributos_Detalhes", gxTv_SdtAtributos_Atributos_detalhes, false);
         AddObjectProperty("Atributos_Descricao", gxTv_SdtAtributos_Atributos_descricao, false);
         AddObjectProperty("Atributos_TipoDados", gxTv_SdtAtributos_Atributos_tipodados, false);
         AddObjectProperty("Atributos_PK", gxTv_SdtAtributos_Atributos_pk, false);
         AddObjectProperty("Atributos_FK", gxTv_SdtAtributos_Atributos_fk, false);
         AddObjectProperty("Atributos_TabelaCod", gxTv_SdtAtributos_Atributos_tabelacod, false);
         AddObjectProperty("Atributos_TabelaNom", gxTv_SdtAtributos_Atributos_tabelanom, false);
         AddObjectProperty("Tabela_SistemaCod", gxTv_SdtAtributos_Tabela_sistemacod, false);
         AddObjectProperty("Atributos_MelhoraCod", gxTv_SdtAtributos_Atributos_melhoracod, false);
         AddObjectProperty("Atributos_Ativo", gxTv_SdtAtributos_Atributos_ativo, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtAtributos_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtAtributos_Initialized, false);
            AddObjectProperty("Atributos_Codigo_Z", gxTv_SdtAtributos_Atributos_codigo_Z, false);
            AddObjectProperty("Atributos_Nome_Z", gxTv_SdtAtributos_Atributos_nome_Z, false);
            AddObjectProperty("Atributos_Detalhes_Z", gxTv_SdtAtributos_Atributos_detalhes_Z, false);
            AddObjectProperty("Atributos_TipoDados_Z", gxTv_SdtAtributos_Atributos_tipodados_Z, false);
            AddObjectProperty("Atributos_PK_Z", gxTv_SdtAtributos_Atributos_pk_Z, false);
            AddObjectProperty("Atributos_FK_Z", gxTv_SdtAtributos_Atributos_fk_Z, false);
            AddObjectProperty("Atributos_TabelaCod_Z", gxTv_SdtAtributos_Atributos_tabelacod_Z, false);
            AddObjectProperty("Atributos_TabelaNom_Z", gxTv_SdtAtributos_Atributos_tabelanom_Z, false);
            AddObjectProperty("Tabela_SistemaCod_Z", gxTv_SdtAtributos_Tabela_sistemacod_Z, false);
            AddObjectProperty("Atributos_MelhoraCod_Z", gxTv_SdtAtributos_Atributos_melhoracod_Z, false);
            AddObjectProperty("Atributos_Ativo_Z", gxTv_SdtAtributos_Atributos_ativo_Z, false);
            AddObjectProperty("Atributos_Detalhes_N", gxTv_SdtAtributos_Atributos_detalhes_N, false);
            AddObjectProperty("Atributos_Descricao_N", gxTv_SdtAtributos_Atributos_descricao_N, false);
            AddObjectProperty("Atributos_TipoDados_N", gxTv_SdtAtributos_Atributos_tipodados_N, false);
            AddObjectProperty("Atributos_PK_N", gxTv_SdtAtributos_Atributos_pk_N, false);
            AddObjectProperty("Atributos_FK_N", gxTv_SdtAtributos_Atributos_fk_N, false);
            AddObjectProperty("Atributos_TabelaNom_N", gxTv_SdtAtributos_Atributos_tabelanom_N, false);
            AddObjectProperty("Tabela_SistemaCod_N", gxTv_SdtAtributos_Tabela_sistemacod_N, false);
            AddObjectProperty("Atributos_MelhoraCod_N", gxTv_SdtAtributos_Atributos_melhoracod_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Atributos_Codigo" )]
      [  XmlElement( ElementName = "Atributos_Codigo"   )]
      public int gxTpr_Atributos_codigo
      {
         get {
            return gxTv_SdtAtributos_Atributos_codigo ;
         }

         set {
            if ( gxTv_SdtAtributos_Atributos_codigo != value )
            {
               gxTv_SdtAtributos_Mode = "INS";
               this.gxTv_SdtAtributos_Atributos_codigo_Z_SetNull( );
               this.gxTv_SdtAtributos_Atributos_nome_Z_SetNull( );
               this.gxTv_SdtAtributos_Atributos_detalhes_Z_SetNull( );
               this.gxTv_SdtAtributos_Atributos_tipodados_Z_SetNull( );
               this.gxTv_SdtAtributos_Atributos_pk_Z_SetNull( );
               this.gxTv_SdtAtributos_Atributos_fk_Z_SetNull( );
               this.gxTv_SdtAtributos_Atributos_tabelacod_Z_SetNull( );
               this.gxTv_SdtAtributos_Atributos_tabelanom_Z_SetNull( );
               this.gxTv_SdtAtributos_Tabela_sistemacod_Z_SetNull( );
               this.gxTv_SdtAtributos_Atributos_melhoracod_Z_SetNull( );
               this.gxTv_SdtAtributos_Atributos_ativo_Z_SetNull( );
            }
            gxTv_SdtAtributos_Atributos_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Atributos_Nome" )]
      [  XmlElement( ElementName = "Atributos_Nome"   )]
      public String gxTpr_Atributos_nome
      {
         get {
            return gxTv_SdtAtributos_Atributos_nome ;
         }

         set {
            gxTv_SdtAtributos_Atributos_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Atributos_Detalhes" )]
      [  XmlElement( ElementName = "Atributos_Detalhes"   )]
      public String gxTpr_Atributos_detalhes
      {
         get {
            return gxTv_SdtAtributos_Atributos_detalhes ;
         }

         set {
            gxTv_SdtAtributos_Atributos_detalhes_N = 0;
            gxTv_SdtAtributos_Atributos_detalhes = (String)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_detalhes_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_detalhes_N = 1;
         gxTv_SdtAtributos_Atributos_detalhes = "";
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_detalhes_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_Descricao" )]
      [  XmlElement( ElementName = "Atributos_Descricao"   )]
      public String gxTpr_Atributos_descricao
      {
         get {
            return gxTv_SdtAtributos_Atributos_descricao ;
         }

         set {
            gxTv_SdtAtributos_Atributos_descricao_N = 0;
            gxTv_SdtAtributos_Atributos_descricao = (String)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_descricao_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_descricao_N = 1;
         gxTv_SdtAtributos_Atributos_descricao = "";
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_descricao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_TipoDados" )]
      [  XmlElement( ElementName = "Atributos_TipoDados"   )]
      public String gxTpr_Atributos_tipodados
      {
         get {
            return gxTv_SdtAtributos_Atributos_tipodados ;
         }

         set {
            gxTv_SdtAtributos_Atributos_tipodados_N = 0;
            gxTv_SdtAtributos_Atributos_tipodados = (String)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_tipodados_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_tipodados_N = 1;
         gxTv_SdtAtributos_Atributos_tipodados = "";
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_tipodados_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_PK" )]
      [  XmlElement( ElementName = "Atributos_PK"   )]
      public bool gxTpr_Atributos_pk
      {
         get {
            return gxTv_SdtAtributos_Atributos_pk ;
         }

         set {
            gxTv_SdtAtributos_Atributos_pk_N = 0;
            gxTv_SdtAtributos_Atributos_pk = value;
         }

      }

      public void gxTv_SdtAtributos_Atributos_pk_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_pk_N = 1;
         gxTv_SdtAtributos_Atributos_pk = false;
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_pk_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_FK" )]
      [  XmlElement( ElementName = "Atributos_FK"   )]
      public bool gxTpr_Atributos_fk
      {
         get {
            return gxTv_SdtAtributos_Atributos_fk ;
         }

         set {
            gxTv_SdtAtributos_Atributos_fk_N = 0;
            gxTv_SdtAtributos_Atributos_fk = value;
         }

      }

      public void gxTv_SdtAtributos_Atributos_fk_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_fk_N = 1;
         gxTv_SdtAtributos_Atributos_fk = false;
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_fk_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_TabelaCod" )]
      [  XmlElement( ElementName = "Atributos_TabelaCod"   )]
      public int gxTpr_Atributos_tabelacod
      {
         get {
            return gxTv_SdtAtributos_Atributos_tabelacod ;
         }

         set {
            gxTv_SdtAtributos_Atributos_tabelacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Atributos_TabelaNom" )]
      [  XmlElement( ElementName = "Atributos_TabelaNom"   )]
      public String gxTpr_Atributos_tabelanom
      {
         get {
            return gxTv_SdtAtributos_Atributos_tabelanom ;
         }

         set {
            gxTv_SdtAtributos_Atributos_tabelanom_N = 0;
            gxTv_SdtAtributos_Atributos_tabelanom = (String)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_tabelanom_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_tabelanom_N = 1;
         gxTv_SdtAtributos_Atributos_tabelanom = "";
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_tabelanom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_SistemaCod" )]
      [  XmlElement( ElementName = "Tabela_SistemaCod"   )]
      public int gxTpr_Tabela_sistemacod
      {
         get {
            return gxTv_SdtAtributos_Tabela_sistemacod ;
         }

         set {
            gxTv_SdtAtributos_Tabela_sistemacod_N = 0;
            gxTv_SdtAtributos_Tabela_sistemacod = (int)(value);
         }

      }

      public void gxTv_SdtAtributos_Tabela_sistemacod_SetNull( )
      {
         gxTv_SdtAtributos_Tabela_sistemacod_N = 1;
         gxTv_SdtAtributos_Tabela_sistemacod = 0;
         return  ;
      }

      public bool gxTv_SdtAtributos_Tabela_sistemacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_MelhoraCod" )]
      [  XmlElement( ElementName = "Atributos_MelhoraCod"   )]
      public int gxTpr_Atributos_melhoracod
      {
         get {
            return gxTv_SdtAtributos_Atributos_melhoracod ;
         }

         set {
            gxTv_SdtAtributos_Atributos_melhoracod_N = 0;
            gxTv_SdtAtributos_Atributos_melhoracod = (int)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_melhoracod_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_melhoracod_N = 1;
         gxTv_SdtAtributos_Atributos_melhoracod = 0;
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_melhoracod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_Ativo" )]
      [  XmlElement( ElementName = "Atributos_Ativo"   )]
      public bool gxTpr_Atributos_ativo
      {
         get {
            return gxTv_SdtAtributos_Atributos_ativo ;
         }

         set {
            gxTv_SdtAtributos_Atributos_ativo = value;
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtAtributos_Mode ;
         }

         set {
            gxTv_SdtAtributos_Mode = (String)(value);
         }

      }

      public void gxTv_SdtAtributos_Mode_SetNull( )
      {
         gxTv_SdtAtributos_Mode = "";
         return  ;
      }

      public bool gxTv_SdtAtributos_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtAtributos_Initialized ;
         }

         set {
            gxTv_SdtAtributos_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtAtributos_Initialized_SetNull( )
      {
         gxTv_SdtAtributos_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtAtributos_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_Codigo_Z" )]
      [  XmlElement( ElementName = "Atributos_Codigo_Z"   )]
      public int gxTpr_Atributos_codigo_Z
      {
         get {
            return gxTv_SdtAtributos_Atributos_codigo_Z ;
         }

         set {
            gxTv_SdtAtributos_Atributos_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_codigo_Z_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_Nome_Z" )]
      [  XmlElement( ElementName = "Atributos_Nome_Z"   )]
      public String gxTpr_Atributos_nome_Z
      {
         get {
            return gxTv_SdtAtributos_Atributos_nome_Z ;
         }

         set {
            gxTv_SdtAtributos_Atributos_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_nome_Z_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_Detalhes_Z" )]
      [  XmlElement( ElementName = "Atributos_Detalhes_Z"   )]
      public String gxTpr_Atributos_detalhes_Z
      {
         get {
            return gxTv_SdtAtributos_Atributos_detalhes_Z ;
         }

         set {
            gxTv_SdtAtributos_Atributos_detalhes_Z = (String)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_detalhes_Z_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_detalhes_Z = "";
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_detalhes_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_TipoDados_Z" )]
      [  XmlElement( ElementName = "Atributos_TipoDados_Z"   )]
      public String gxTpr_Atributos_tipodados_Z
      {
         get {
            return gxTv_SdtAtributos_Atributos_tipodados_Z ;
         }

         set {
            gxTv_SdtAtributos_Atributos_tipodados_Z = (String)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_tipodados_Z_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_tipodados_Z = "";
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_tipodados_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_PK_Z" )]
      [  XmlElement( ElementName = "Atributos_PK_Z"   )]
      public bool gxTpr_Atributos_pk_Z
      {
         get {
            return gxTv_SdtAtributos_Atributos_pk_Z ;
         }

         set {
            gxTv_SdtAtributos_Atributos_pk_Z = value;
         }

      }

      public void gxTv_SdtAtributos_Atributos_pk_Z_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_pk_Z = false;
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_pk_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_FK_Z" )]
      [  XmlElement( ElementName = "Atributos_FK_Z"   )]
      public bool gxTpr_Atributos_fk_Z
      {
         get {
            return gxTv_SdtAtributos_Atributos_fk_Z ;
         }

         set {
            gxTv_SdtAtributos_Atributos_fk_Z = value;
         }

      }

      public void gxTv_SdtAtributos_Atributos_fk_Z_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_fk_Z = false;
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_fk_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_TabelaCod_Z" )]
      [  XmlElement( ElementName = "Atributos_TabelaCod_Z"   )]
      public int gxTpr_Atributos_tabelacod_Z
      {
         get {
            return gxTv_SdtAtributos_Atributos_tabelacod_Z ;
         }

         set {
            gxTv_SdtAtributos_Atributos_tabelacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_tabelacod_Z_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_tabelacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_tabelacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_TabelaNom_Z" )]
      [  XmlElement( ElementName = "Atributos_TabelaNom_Z"   )]
      public String gxTpr_Atributos_tabelanom_Z
      {
         get {
            return gxTv_SdtAtributos_Atributos_tabelanom_Z ;
         }

         set {
            gxTv_SdtAtributos_Atributos_tabelanom_Z = (String)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_tabelanom_Z_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_tabelanom_Z = "";
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_tabelanom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_SistemaCod_Z" )]
      [  XmlElement( ElementName = "Tabela_SistemaCod_Z"   )]
      public int gxTpr_Tabela_sistemacod_Z
      {
         get {
            return gxTv_SdtAtributos_Tabela_sistemacod_Z ;
         }

         set {
            gxTv_SdtAtributos_Tabela_sistemacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtAtributos_Tabela_sistemacod_Z_SetNull( )
      {
         gxTv_SdtAtributos_Tabela_sistemacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAtributos_Tabela_sistemacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_MelhoraCod_Z" )]
      [  XmlElement( ElementName = "Atributos_MelhoraCod_Z"   )]
      public int gxTpr_Atributos_melhoracod_Z
      {
         get {
            return gxTv_SdtAtributos_Atributos_melhoracod_Z ;
         }

         set {
            gxTv_SdtAtributos_Atributos_melhoracod_Z = (int)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_melhoracod_Z_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_melhoracod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_melhoracod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_Ativo_Z" )]
      [  XmlElement( ElementName = "Atributos_Ativo_Z"   )]
      public bool gxTpr_Atributos_ativo_Z
      {
         get {
            return gxTv_SdtAtributos_Atributos_ativo_Z ;
         }

         set {
            gxTv_SdtAtributos_Atributos_ativo_Z = value;
         }

      }

      public void gxTv_SdtAtributos_Atributos_ativo_Z_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_Detalhes_N" )]
      [  XmlElement( ElementName = "Atributos_Detalhes_N"   )]
      public short gxTpr_Atributos_detalhes_N
      {
         get {
            return gxTv_SdtAtributos_Atributos_detalhes_N ;
         }

         set {
            gxTv_SdtAtributos_Atributos_detalhes_N = (short)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_detalhes_N_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_detalhes_N = 0;
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_detalhes_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_Descricao_N" )]
      [  XmlElement( ElementName = "Atributos_Descricao_N"   )]
      public short gxTpr_Atributos_descricao_N
      {
         get {
            return gxTv_SdtAtributos_Atributos_descricao_N ;
         }

         set {
            gxTv_SdtAtributos_Atributos_descricao_N = (short)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_descricao_N_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_descricao_N = 0;
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_descricao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_TipoDados_N" )]
      [  XmlElement( ElementName = "Atributos_TipoDados_N"   )]
      public short gxTpr_Atributos_tipodados_N
      {
         get {
            return gxTv_SdtAtributos_Atributos_tipodados_N ;
         }

         set {
            gxTv_SdtAtributos_Atributos_tipodados_N = (short)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_tipodados_N_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_tipodados_N = 0;
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_tipodados_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_PK_N" )]
      [  XmlElement( ElementName = "Atributos_PK_N"   )]
      public short gxTpr_Atributos_pk_N
      {
         get {
            return gxTv_SdtAtributos_Atributos_pk_N ;
         }

         set {
            gxTv_SdtAtributos_Atributos_pk_N = (short)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_pk_N_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_pk_N = 0;
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_pk_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_FK_N" )]
      [  XmlElement( ElementName = "Atributos_FK_N"   )]
      public short gxTpr_Atributos_fk_N
      {
         get {
            return gxTv_SdtAtributos_Atributos_fk_N ;
         }

         set {
            gxTv_SdtAtributos_Atributos_fk_N = (short)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_fk_N_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_fk_N = 0;
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_fk_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_TabelaNom_N" )]
      [  XmlElement( ElementName = "Atributos_TabelaNom_N"   )]
      public short gxTpr_Atributos_tabelanom_N
      {
         get {
            return gxTv_SdtAtributos_Atributos_tabelanom_N ;
         }

         set {
            gxTv_SdtAtributos_Atributos_tabelanom_N = (short)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_tabelanom_N_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_tabelanom_N = 0;
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_tabelanom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_SistemaCod_N" )]
      [  XmlElement( ElementName = "Tabela_SistemaCod_N"   )]
      public short gxTpr_Tabela_sistemacod_N
      {
         get {
            return gxTv_SdtAtributos_Tabela_sistemacod_N ;
         }

         set {
            gxTv_SdtAtributos_Tabela_sistemacod_N = (short)(value);
         }

      }

      public void gxTv_SdtAtributos_Tabela_sistemacod_N_SetNull( )
      {
         gxTv_SdtAtributos_Tabela_sistemacod_N = 0;
         return  ;
      }

      public bool gxTv_SdtAtributos_Tabela_sistemacod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Atributos_MelhoraCod_N" )]
      [  XmlElement( ElementName = "Atributos_MelhoraCod_N"   )]
      public short gxTpr_Atributos_melhoracod_N
      {
         get {
            return gxTv_SdtAtributos_Atributos_melhoracod_N ;
         }

         set {
            gxTv_SdtAtributos_Atributos_melhoracod_N = (short)(value);
         }

      }

      public void gxTv_SdtAtributos_Atributos_melhoracod_N_SetNull( )
      {
         gxTv_SdtAtributos_Atributos_melhoracod_N = 0;
         return  ;
      }

      public bool gxTv_SdtAtributos_Atributos_melhoracod_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtAtributos_Atributos_nome = "";
         gxTv_SdtAtributos_Atributos_detalhes = "";
         gxTv_SdtAtributos_Atributos_descricao = "";
         gxTv_SdtAtributos_Atributos_tipodados = "";
         gxTv_SdtAtributos_Atributos_tabelanom = "";
         gxTv_SdtAtributos_Mode = "";
         gxTv_SdtAtributos_Atributos_nome_Z = "";
         gxTv_SdtAtributos_Atributos_detalhes_Z = "";
         gxTv_SdtAtributos_Atributos_tipodados_Z = "";
         gxTv_SdtAtributos_Atributos_tabelanom_Z = "";
         gxTv_SdtAtributos_Atributos_ativo = true;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "atributos", "GeneXus.Programs.atributos_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtAtributos_Initialized ;
      private short gxTv_SdtAtributos_Atributos_detalhes_N ;
      private short gxTv_SdtAtributos_Atributos_descricao_N ;
      private short gxTv_SdtAtributos_Atributos_tipodados_N ;
      private short gxTv_SdtAtributos_Atributos_pk_N ;
      private short gxTv_SdtAtributos_Atributos_fk_N ;
      private short gxTv_SdtAtributos_Atributos_tabelanom_N ;
      private short gxTv_SdtAtributos_Tabela_sistemacod_N ;
      private short gxTv_SdtAtributos_Atributos_melhoracod_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtAtributos_Atributos_codigo ;
      private int gxTv_SdtAtributos_Atributos_tabelacod ;
      private int gxTv_SdtAtributos_Tabela_sistemacod ;
      private int gxTv_SdtAtributos_Atributos_melhoracod ;
      private int gxTv_SdtAtributos_Atributos_codigo_Z ;
      private int gxTv_SdtAtributos_Atributos_tabelacod_Z ;
      private int gxTv_SdtAtributos_Tabela_sistemacod_Z ;
      private int gxTv_SdtAtributos_Atributos_melhoracod_Z ;
      private String gxTv_SdtAtributos_Atributos_nome ;
      private String gxTv_SdtAtributos_Atributos_detalhes ;
      private String gxTv_SdtAtributos_Atributos_tipodados ;
      private String gxTv_SdtAtributos_Atributos_tabelanom ;
      private String gxTv_SdtAtributos_Mode ;
      private String gxTv_SdtAtributos_Atributos_nome_Z ;
      private String gxTv_SdtAtributos_Atributos_detalhes_Z ;
      private String gxTv_SdtAtributos_Atributos_tipodados_Z ;
      private String gxTv_SdtAtributos_Atributos_tabelanom_Z ;
      private String sTagName ;
      private bool gxTv_SdtAtributos_Atributos_pk ;
      private bool gxTv_SdtAtributos_Atributos_fk ;
      private bool gxTv_SdtAtributos_Atributos_ativo ;
      private bool gxTv_SdtAtributos_Atributos_pk_Z ;
      private bool gxTv_SdtAtributos_Atributos_fk_Z ;
      private bool gxTv_SdtAtributos_Atributos_ativo_Z ;
      private String gxTv_SdtAtributos_Atributos_descricao ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Atributos", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtAtributos_RESTInterface : GxGenericCollectionItem<SdtAtributos>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtAtributos_RESTInterface( ) : base()
      {
      }

      public SdtAtributos_RESTInterface( SdtAtributos psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Atributos_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Atributos_codigo
      {
         get {
            return sdt.gxTpr_Atributos_codigo ;
         }

         set {
            sdt.gxTpr_Atributos_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Atributos_Nome" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Atributos_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Atributos_nome) ;
         }

         set {
            sdt.gxTpr_Atributos_nome = (String)(value);
         }

      }

      [DataMember( Name = "Atributos_Detalhes" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Atributos_detalhes
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Atributos_detalhes) ;
         }

         set {
            sdt.gxTpr_Atributos_detalhes = (String)(value);
         }

      }

      [DataMember( Name = "Atributos_Descricao" , Order = 3 )]
      public String gxTpr_Atributos_descricao
      {
         get {
            return sdt.gxTpr_Atributos_descricao ;
         }

         set {
            sdt.gxTpr_Atributos_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Atributos_TipoDados" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Atributos_tipodados
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Atributos_tipodados) ;
         }

         set {
            sdt.gxTpr_Atributos_tipodados = (String)(value);
         }

      }

      [DataMember( Name = "Atributos_PK" , Order = 5 )]
      [GxSeudo()]
      public bool gxTpr_Atributos_pk
      {
         get {
            return sdt.gxTpr_Atributos_pk ;
         }

         set {
            sdt.gxTpr_Atributos_pk = value;
         }

      }

      [DataMember( Name = "Atributos_FK" , Order = 6 )]
      [GxSeudo()]
      public bool gxTpr_Atributos_fk
      {
         get {
            return sdt.gxTpr_Atributos_fk ;
         }

         set {
            sdt.gxTpr_Atributos_fk = value;
         }

      }

      [DataMember( Name = "Atributos_TabelaCod" , Order = 7 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Atributos_tabelacod
      {
         get {
            return sdt.gxTpr_Atributos_tabelacod ;
         }

         set {
            sdt.gxTpr_Atributos_tabelacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Atributos_TabelaNom" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Atributos_tabelanom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Atributos_tabelanom) ;
         }

         set {
            sdt.gxTpr_Atributos_tabelanom = (String)(value);
         }

      }

      [DataMember( Name = "Tabela_SistemaCod" , Order = 9 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Tabela_sistemacod
      {
         get {
            return sdt.gxTpr_Tabela_sistemacod ;
         }

         set {
            sdt.gxTpr_Tabela_sistemacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Atributos_MelhoraCod" , Order = 10 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Atributos_melhoracod
      {
         get {
            return sdt.gxTpr_Atributos_melhoracod ;
         }

         set {
            sdt.gxTpr_Atributos_melhoracod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Atributos_Ativo" , Order = 11 )]
      [GxSeudo()]
      public bool gxTpr_Atributos_ativo
      {
         get {
            return sdt.gxTpr_Atributos_ativo ;
         }

         set {
            sdt.gxTpr_Atributos_ativo = value;
         }

      }

      public SdtAtributos sdt
      {
         get {
            return (SdtAtributos)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtAtributos() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 33 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
