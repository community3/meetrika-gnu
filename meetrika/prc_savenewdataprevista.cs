/*
               File: PRC_SaveNewDataPrevista
        Description: Save New Data Prevista
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:5.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_savenewdataprevista : GXProcedure
   {
      public prc_savenewdataprevista( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_savenewdataprevista( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           ref DateTime aP1_ContagemResultado_DataPrevista )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8ContagemResultado_DataPrevista = aP1_ContagemResultado_DataPrevista;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_ContagemResultado_DataPrevista=this.AV8ContagemResultado_DataPrevista;
      }

      public DateTime executeUdp( ref int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8ContagemResultado_DataPrevista = aP1_ContagemResultado_DataPrevista;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_ContagemResultado_DataPrevista=this.AV8ContagemResultado_DataPrevista;
         return AV8ContagemResultado_DataPrevista ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 ref DateTime aP1_ContagemResultado_DataPrevista )
      {
         prc_savenewdataprevista objprc_savenewdataprevista;
         objprc_savenewdataprevista = new prc_savenewdataprevista();
         objprc_savenewdataprevista.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_savenewdataprevista.AV8ContagemResultado_DataPrevista = aP1_ContagemResultado_DataPrevista;
         objprc_savenewdataprevista.context.SetSubmitInitialConfig(context);
         objprc_savenewdataprevista.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_savenewdataprevista);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_ContagemResultado_DataPrevista=this.AV8ContagemResultado_DataPrevista;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_savenewdataprevista)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00VL2 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1351ContagemResultado_DataPrevista = P00VL2_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00VL2_n1351ContagemResultado_DataPrevista[0];
            A1351ContagemResultado_DataPrevista = AV8ContagemResultado_DataPrevista;
            n1351ContagemResultado_DataPrevista = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00VL3 */
            pr_default.execute(1, new Object[] {n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P00VL4 */
            pr_default.execute(2, new Object[] {n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, A456ContagemResultado_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00VL2_A456ContagemResultado_Codigo = new int[1] ;
         P00VL2_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00VL2_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_savenewdataprevista__default(),
            new Object[][] {
                new Object[] {
               P00VL2_A456ContagemResultado_Codigo, P00VL2_A1351ContagemResultado_DataPrevista, P00VL2_n1351ContagemResultado_DataPrevista
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private String scmdbuf ;
      private DateTime AV8ContagemResultado_DataPrevista ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private bool n1351ContagemResultado_DataPrevista ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private DateTime aP1_ContagemResultado_DataPrevista ;
      private IDataStoreProvider pr_default ;
      private int[] P00VL2_A456ContagemResultado_Codigo ;
      private DateTime[] P00VL2_A1351ContagemResultado_DataPrevista ;
      private bool[] P00VL2_n1351ContagemResultado_DataPrevista ;
   }

   public class prc_savenewdataprevista__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VL2 ;
          prmP00VL2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VL3 ;
          prmP00VL3 = new Object[] {
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VL4 ;
          prmP00VL4 = new Object[] {
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VL2", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_DataPrevista] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VL2,1,0,true,true )
             ,new CursorDef("P00VL3", "UPDATE [ContagemResultado] SET [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VL3)
             ,new CursorDef("P00VL4", "UPDATE [ContagemResultado] SET [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VL4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
