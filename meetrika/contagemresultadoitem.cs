/*
               File: ContagemResultadoItem
        Description: Contagem Resultado Item
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:23:16.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadoitem : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         chkContagemResultadoItem_ValidadoFM.Name = "CONTAGEMRESULTADOITEM_VALIDADOFM";
         chkContagemResultadoItem_ValidadoFM.WebTags = "";
         chkContagemResultadoItem_ValidadoFM.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContagemResultadoItem_ValidadoFM_Internalname, "TitleCaption", chkContagemResultadoItem_ValidadoFM.Caption);
         chkContagemResultadoItem_ValidadoFM.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem Resultado Item", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemResultadoItem_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemresultadoitem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadoitem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkContagemResultadoItem_ValidadoFM = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2K100( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2K100e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2K100( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2K100( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2K100e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Contagem Resultado Item", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ContagemResultadoItem.htm");
            wb_table3_28_2K100( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_2K100e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2K100e( true) ;
         }
         else
         {
            wb_table1_2_2K100e( false) ;
         }
      }

      protected void wb_table3_28_2K100( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_2K100( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_2K100e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 162,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoItem.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 163,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoItem.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 164,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_2K100e( true) ;
         }
         else
         {
            wb_table3_28_2K100e( false) ;
         }
      }

      protected void wb_table4_34_2K100( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_codigo_Internalname, "Resultado Item_Codigo", "", "", lblTextblockcontagemresultadoitem_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A772ContagemResultadoItem_Codigo), 6, 0, ",", "")), ((edtContagemResultadoItem_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A772ContagemResultadoItem_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A772ContagemResultadoItem_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_contagemcod_Internalname, "Item_Contagem Cod", "", "", lblTextblockcontagemresultadoitem_contagemcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_ContagemCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A773ContagemResultadoItem_ContagemCod), 6, 0, ",", "")), ((edtContagemResultadoItem_ContagemCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A773ContagemResultadoItem_ContagemCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A773ContagemResultadoItem_ContagemCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_ContagemCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_ContagemCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_demanda_Internalname, "Resultado Item_Demanda", "", "", lblTextblockcontagemresultadoitem_demanda_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_Demanda_Internalname, A793ContagemResultadoItem_Demanda, StringUtil.RTrim( context.localUtil.Format( A793ContagemResultadoItem_Demanda, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_Demanda_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_Demanda_Enabled, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_demandacorrigida_Internalname, "Item_Demanda Corrigida", "", "", lblTextblockcontagemresultadoitem_demandacorrigida_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_DemandaCorrigida_Internalname, A809ContagemResultadoItem_DemandaCorrigida, StringUtil.RTrim( context.localUtil.Format( A809ContagemResultadoItem_DemandaCorrigida, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_DemandaCorrigida_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_DemandaCorrigida_Enabled, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_cfps_Internalname, "Resultado Item_CFPS", "", "", lblTextblockcontagemresultadoitem_cfps_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_CFPS_Internalname, A794ContagemResultadoItem_CFPS, StringUtil.RTrim( context.localUtil.Format( A794ContagemResultadoItem_CFPS, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_CFPS_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_CFPS_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_datacnt_Internalname, "Item_Data Cnt", "", "", lblTextblockcontagemresultadoitem_datacnt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemResultadoItem_DataCnt_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_DataCnt_Internalname, context.localUtil.Format(A792ContagemResultadoItem_DataCnt, "99/99/99"), context.localUtil.Format( A792ContagemResultadoItem_DataCnt, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_DataCnt_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_DataCnt_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoItem.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultadoItem_DataCnt_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemResultadoItem_DataCnt_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_pfbtot_Internalname, "Resultado Item_PFBTot", "", "", lblTextblockcontagemresultadoitem_pfbtot_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_PFBTot_Internalname, StringUtil.LTrim( StringUtil.NToC( A796ContagemResultadoItem_PFBTot, 14, 5, ",", "")), ((edtContagemResultadoItem_PFBTot_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A796ContagemResultadoItem_PFBTot, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A796ContagemResultadoItem_PFBTot, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_PFBTot_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_PFBTot_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_pfltot_Internalname, "Resultado Item_PFLTot", "", "", lblTextblockcontagemresultadoitem_pfltot_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_PFLTot_Internalname, StringUtil.LTrim( StringUtil.NToC( A797ContagemResultadoItem_PFLTot, 14, 5, ",", "")), ((edtContagemResultadoItem_PFLTot_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A797ContagemResultadoItem_PFLTot, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A797ContagemResultadoItem_PFLTot, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_PFLTot_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_PFLTot_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_descricao_Internalname, "Resultado Item_Descricao", "", "", lblTextblockcontagemresultadoitem_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemResultadoItem_Descricao_Internalname, A774ContagemResultadoItem_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"", 0, 1, edtContagemResultadoItem_Descricao_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_tipo_Internalname, "Resultado Item_Tipo", "", "", lblTextblockcontagemresultadoitem_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_Tipo_Internalname, A775ContagemResultadoItem_Tipo, StringUtil.RTrim( context.localUtil.Format( A775ContagemResultadoItem_Tipo, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_Tipo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_Tipo_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_deflator_Internalname, "Resultado Item_Deflator", "", "", lblTextblockcontagemresultadoitem_deflator_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_Deflator_Internalname, A776ContagemResultadoItem_Deflator, StringUtil.RTrim( context.localUtil.Format( A776ContagemResultadoItem_Deflator, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_Deflator_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_Deflator_Enabled, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_der_Internalname, "Resultado Item_DER", "", "", lblTextblockcontagemresultadoitem_der_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_DER_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A777ContagemResultadoItem_DER), 4, 0, ",", "")), ((edtContagemResultadoItem_DER_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A777ContagemResultadoItem_DER), "ZZZ9")) : context.localUtil.Format( (decimal)(A777ContagemResultadoItem_DER), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_DER_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_DER_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_ra_Internalname, "Resultado Item_RA", "", "", lblTextblockcontagemresultadoitem_ra_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_RA_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A778ContagemResultadoItem_RA), 4, 0, ",", "")), ((edtContagemResultadoItem_RA_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A778ContagemResultadoItem_RA), "ZZZ9")) : context.localUtil.Format( (decimal)(A778ContagemResultadoItem_RA), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_RA_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_RA_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_cp_Internalname, "Resultado Item_CP", "", "", lblTextblockcontagemresultadoitem_cp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_CP_Internalname, StringUtil.RTrim( A779ContagemResultadoItem_CP), StringUtil.RTrim( context.localUtil.Format( A779ContagemResultadoItem_CP, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_CP_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_CP_Enabled, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "CP", "left", true, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_pfb_Internalname, "Resultado Item_PFB", "", "", lblTextblockcontagemresultadoitem_pfb_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_PFB_Internalname, StringUtil.LTrim( StringUtil.NToC( A780ContagemResultadoItem_PFB, 14, 5, ",", "")), ((edtContagemResultadoItem_PFB_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A780ContagemResultadoItem_PFB, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A780ContagemResultadoItem_PFB, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_PFB_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_PFB_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_pfl_Internalname, "Resultado Item_PFL", "", "", lblTextblockcontagemresultadoitem_pfl_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_PFL_Internalname, StringUtil.LTrim( StringUtil.NToC( A781ContagemResultadoItem_PFL, 14, 5, ",", "")), ((edtContagemResultadoItem_PFL_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A781ContagemResultadoItem_PFL, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A781ContagemResultadoItem_PFL, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_PFL_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_PFL_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_evidencia_Internalname, "Resultado Item_Evidencia", "", "", lblTextblockcontagemresultadoitem_evidencia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemResultadoItem_Evidencia_Internalname, A782ContagemResultadoItem_Evidencia, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", 0, 1, edtContagemResultadoItem_Evidencia_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_validadofm_Internalname, "Item_Validado FM", "", "", lblTextblockcontagemresultadoitem_validadofm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContagemResultadoItem_ValidadoFM_Internalname, StringUtil.BoolToStr( A789ContagemResultadoItem_ValidadoFM), "", "", 1, chkContagemResultadoItem_ValidadoFM.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(124, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,124);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_tipofm_Internalname, "Item_Tipo FM", "", "", lblTextblockcontagemresultadoitem_tipofm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_TipoFM_Internalname, StringUtil.RTrim( A790ContagemResultadoItem_TipoFM), StringUtil.RTrim( context.localUtil.Format( A790ContagemResultadoItem_TipoFM, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,129);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_TipoFM_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_TipoFM_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_deflatorfm_Internalname, "Item_Deflator FM", "", "", lblTextblockcontagemresultadoitem_deflatorfm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_DeflatorFM_Internalname, A786ContagemResultadoItem_DeflatorFM, StringUtil.RTrim( context.localUtil.Format( A786ContagemResultadoItem_DeflatorFM, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,134);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_DeflatorFM_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_DeflatorFM_Enabled, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_derfm_Internalname, "Resultado Item_DERFM", "", "", lblTextblockcontagemresultadoitem_derfm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_DERFM_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A787ContagemResultadoItem_DERFM), 4, 0, ",", "")), ((edtContagemResultadoItem_DERFM_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A787ContagemResultadoItem_DERFM), "ZZZ9")) : context.localUtil.Format( (decimal)(A787ContagemResultadoItem_DERFM), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,139);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_DERFM_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_DERFM_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_rafm_Internalname, "Resultado Item_RAFM", "", "", lblTextblockcontagemresultadoitem_rafm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 144,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_RAFM_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A791ContagemResultadoItem_RAFM), 4, 0, ",", "")), ((edtContagemResultadoItem_RAFM_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A791ContagemResultadoItem_RAFM), "ZZZ9")) : context.localUtil.Format( (decimal)(A791ContagemResultadoItem_RAFM), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,144);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_RAFM_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_RAFM_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_cpfm_Internalname, "Resultado Item_CPFM", "", "", lblTextblockcontagemresultadoitem_cpfm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_CPFM_Internalname, StringUtil.RTrim( A795ContagemResultadoItem_CPFM), StringUtil.RTrim( context.localUtil.Format( A795ContagemResultadoItem_CPFM, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,149);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_CPFM_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_CPFM_Enabled, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "CP", "left", true, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_pfbfm_Internalname, "Resultado Item_PFBFM", "", "", lblTextblockcontagemresultadoitem_pfbfm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 154,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_PFBFM_Internalname, StringUtil.LTrim( StringUtil.NToC( A784ContagemResultadoItem_PFBFM, 14, 5, ",", "")), ((edtContagemResultadoItem_PFBFM_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A784ContagemResultadoItem_PFBFM, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A784ContagemResultadoItem_PFBFM, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,154);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_PFBFM_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_PFBFM_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoitem_pflfm_Internalname, "Resultado Item_PFLFM", "", "", lblTextblockcontagemresultadoitem_pflfm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 159,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoItem_PFLFM_Internalname, StringUtil.LTrim( StringUtil.NToC( A785ContagemResultadoItem_PFLFM, 14, 5, ",", "")), ((edtContagemResultadoItem_PFLFM_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A785ContagemResultadoItem_PFLFM, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A785ContagemResultadoItem_PFLFM, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,159);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoItem_PFLFM_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoItem_PFLFM_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemResultadoItem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_2K100e( true) ;
         }
         else
         {
            wb_table4_34_2K100e( false) ;
         }
      }

      protected void wb_table2_5_2K100( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoItem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoItem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoItem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoItem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoItem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoItem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoItem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoItem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoItem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoItem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoItem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoItem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoItem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoItem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoItem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoItem.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2K100e( true) ;
         }
         else
         {
            wb_table2_5_2K100e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOITEM_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoItem_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A772ContagemResultadoItem_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A772ContagemResultadoItem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A772ContagemResultadoItem_Codigo), 6, 0)));
               }
               else
               {
                  A772ContagemResultadoItem_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoItem_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A772ContagemResultadoItem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A772ContagemResultadoItem_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_ContagemCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_ContagemCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOITEM_CONTAGEMCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoItem_ContagemCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A773ContagemResultadoItem_ContagemCod = 0;
                  n773ContagemResultadoItem_ContagemCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A773ContagemResultadoItem_ContagemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A773ContagemResultadoItem_ContagemCod), 6, 0)));
               }
               else
               {
                  A773ContagemResultadoItem_ContagemCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoItem_ContagemCod_Internalname), ",", "."));
                  n773ContagemResultadoItem_ContagemCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A773ContagemResultadoItem_ContagemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A773ContagemResultadoItem_ContagemCod), 6, 0)));
               }
               n773ContagemResultadoItem_ContagemCod = ((0==A773ContagemResultadoItem_ContagemCod) ? true : false);
               A793ContagemResultadoItem_Demanda = cgiGet( edtContagemResultadoItem_Demanda_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A793ContagemResultadoItem_Demanda", A793ContagemResultadoItem_Demanda);
               A809ContagemResultadoItem_DemandaCorrigida = cgiGet( edtContagemResultadoItem_DemandaCorrigida_Internalname);
               n809ContagemResultadoItem_DemandaCorrigida = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A809ContagemResultadoItem_DemandaCorrigida", A809ContagemResultadoItem_DemandaCorrigida);
               n809ContagemResultadoItem_DemandaCorrigida = (String.IsNullOrEmpty(StringUtil.RTrim( A809ContagemResultadoItem_DemandaCorrigida)) ? true : false);
               A794ContagemResultadoItem_CFPS = cgiGet( edtContagemResultadoItem_CFPS_Internalname);
               n794ContagemResultadoItem_CFPS = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A794ContagemResultadoItem_CFPS", A794ContagemResultadoItem_CFPS);
               n794ContagemResultadoItem_CFPS = (String.IsNullOrEmpty(StringUtil.RTrim( A794ContagemResultadoItem_CFPS)) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtContagemResultadoItem_DataCnt_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado Item_Data Cnt"}), 1, "CONTAGEMRESULTADOITEM_DATACNT");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoItem_DataCnt_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A792ContagemResultadoItem_DataCnt = DateTime.MinValue;
                  n792ContagemResultadoItem_DataCnt = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A792ContagemResultadoItem_DataCnt", context.localUtil.Format(A792ContagemResultadoItem_DataCnt, "99/99/99"));
               }
               else
               {
                  A792ContagemResultadoItem_DataCnt = context.localUtil.CToD( cgiGet( edtContagemResultadoItem_DataCnt_Internalname), 2);
                  n792ContagemResultadoItem_DataCnt = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A792ContagemResultadoItem_DataCnt", context.localUtil.Format(A792ContagemResultadoItem_DataCnt, "99/99/99"));
               }
               n792ContagemResultadoItem_DataCnt = ((DateTime.MinValue==A792ContagemResultadoItem_DataCnt) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_PFBTot_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_PFBTot_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOITEM_PFBTOT");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoItem_PFBTot_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A796ContagemResultadoItem_PFBTot = 0;
                  n796ContagemResultadoItem_PFBTot = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A796ContagemResultadoItem_PFBTot", StringUtil.LTrim( StringUtil.Str( A796ContagemResultadoItem_PFBTot, 14, 5)));
               }
               else
               {
                  A796ContagemResultadoItem_PFBTot = context.localUtil.CToN( cgiGet( edtContagemResultadoItem_PFBTot_Internalname), ",", ".");
                  n796ContagemResultadoItem_PFBTot = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A796ContagemResultadoItem_PFBTot", StringUtil.LTrim( StringUtil.Str( A796ContagemResultadoItem_PFBTot, 14, 5)));
               }
               n796ContagemResultadoItem_PFBTot = ((Convert.ToDecimal(0)==A796ContagemResultadoItem_PFBTot) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_PFLTot_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_PFLTot_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOITEM_PFLTOT");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoItem_PFLTot_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A797ContagemResultadoItem_PFLTot = 0;
                  n797ContagemResultadoItem_PFLTot = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A797ContagemResultadoItem_PFLTot", StringUtil.LTrim( StringUtil.Str( A797ContagemResultadoItem_PFLTot, 14, 5)));
               }
               else
               {
                  A797ContagemResultadoItem_PFLTot = context.localUtil.CToN( cgiGet( edtContagemResultadoItem_PFLTot_Internalname), ",", ".");
                  n797ContagemResultadoItem_PFLTot = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A797ContagemResultadoItem_PFLTot", StringUtil.LTrim( StringUtil.Str( A797ContagemResultadoItem_PFLTot, 14, 5)));
               }
               n797ContagemResultadoItem_PFLTot = ((Convert.ToDecimal(0)==A797ContagemResultadoItem_PFLTot) ? true : false);
               A774ContagemResultadoItem_Descricao = cgiGet( edtContagemResultadoItem_Descricao_Internalname);
               n774ContagemResultadoItem_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A774ContagemResultadoItem_Descricao", A774ContagemResultadoItem_Descricao);
               n774ContagemResultadoItem_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A774ContagemResultadoItem_Descricao)) ? true : false);
               A775ContagemResultadoItem_Tipo = cgiGet( edtContagemResultadoItem_Tipo_Internalname);
               n775ContagemResultadoItem_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A775ContagemResultadoItem_Tipo", A775ContagemResultadoItem_Tipo);
               n775ContagemResultadoItem_Tipo = (String.IsNullOrEmpty(StringUtil.RTrim( A775ContagemResultadoItem_Tipo)) ? true : false);
               A776ContagemResultadoItem_Deflator = cgiGet( edtContagemResultadoItem_Deflator_Internalname);
               n776ContagemResultadoItem_Deflator = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A776ContagemResultadoItem_Deflator", A776ContagemResultadoItem_Deflator);
               n776ContagemResultadoItem_Deflator = (String.IsNullOrEmpty(StringUtil.RTrim( A776ContagemResultadoItem_Deflator)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_DER_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_DER_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOITEM_DER");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoItem_DER_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A777ContagemResultadoItem_DER = 0;
                  n777ContagemResultadoItem_DER = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A777ContagemResultadoItem_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A777ContagemResultadoItem_DER), 4, 0)));
               }
               else
               {
                  A777ContagemResultadoItem_DER = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoItem_DER_Internalname), ",", "."));
                  n777ContagemResultadoItem_DER = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A777ContagemResultadoItem_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A777ContagemResultadoItem_DER), 4, 0)));
               }
               n777ContagemResultadoItem_DER = ((0==A777ContagemResultadoItem_DER) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_RA_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_RA_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOITEM_RA");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoItem_RA_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A778ContagemResultadoItem_RA = 0;
                  n778ContagemResultadoItem_RA = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A778ContagemResultadoItem_RA", StringUtil.LTrim( StringUtil.Str( (decimal)(A778ContagemResultadoItem_RA), 4, 0)));
               }
               else
               {
                  A778ContagemResultadoItem_RA = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoItem_RA_Internalname), ",", "."));
                  n778ContagemResultadoItem_RA = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A778ContagemResultadoItem_RA", StringUtil.LTrim( StringUtil.Str( (decimal)(A778ContagemResultadoItem_RA), 4, 0)));
               }
               n778ContagemResultadoItem_RA = ((0==A778ContagemResultadoItem_RA) ? true : false);
               A779ContagemResultadoItem_CP = cgiGet( edtContagemResultadoItem_CP_Internalname);
               n779ContagemResultadoItem_CP = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A779ContagemResultadoItem_CP", A779ContagemResultadoItem_CP);
               n779ContagemResultadoItem_CP = (String.IsNullOrEmpty(StringUtil.RTrim( A779ContagemResultadoItem_CP)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_PFB_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_PFB_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOITEM_PFB");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoItem_PFB_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A780ContagemResultadoItem_PFB = 0;
                  n780ContagemResultadoItem_PFB = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A780ContagemResultadoItem_PFB", StringUtil.LTrim( StringUtil.Str( A780ContagemResultadoItem_PFB, 14, 5)));
               }
               else
               {
                  A780ContagemResultadoItem_PFB = context.localUtil.CToN( cgiGet( edtContagemResultadoItem_PFB_Internalname), ",", ".");
                  n780ContagemResultadoItem_PFB = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A780ContagemResultadoItem_PFB", StringUtil.LTrim( StringUtil.Str( A780ContagemResultadoItem_PFB, 14, 5)));
               }
               n780ContagemResultadoItem_PFB = ((Convert.ToDecimal(0)==A780ContagemResultadoItem_PFB) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_PFL_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_PFL_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOITEM_PFL");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoItem_PFL_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A781ContagemResultadoItem_PFL = 0;
                  n781ContagemResultadoItem_PFL = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A781ContagemResultadoItem_PFL", StringUtil.LTrim( StringUtil.Str( A781ContagemResultadoItem_PFL, 14, 5)));
               }
               else
               {
                  A781ContagemResultadoItem_PFL = context.localUtil.CToN( cgiGet( edtContagemResultadoItem_PFL_Internalname), ",", ".");
                  n781ContagemResultadoItem_PFL = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A781ContagemResultadoItem_PFL", StringUtil.LTrim( StringUtil.Str( A781ContagemResultadoItem_PFL, 14, 5)));
               }
               n781ContagemResultadoItem_PFL = ((Convert.ToDecimal(0)==A781ContagemResultadoItem_PFL) ? true : false);
               A782ContagemResultadoItem_Evidencia = cgiGet( edtContagemResultadoItem_Evidencia_Internalname);
               n782ContagemResultadoItem_Evidencia = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A782ContagemResultadoItem_Evidencia", A782ContagemResultadoItem_Evidencia);
               n782ContagemResultadoItem_Evidencia = (String.IsNullOrEmpty(StringUtil.RTrim( A782ContagemResultadoItem_Evidencia)) ? true : false);
               A789ContagemResultadoItem_ValidadoFM = StringUtil.StrToBool( cgiGet( chkContagemResultadoItem_ValidadoFM_Internalname));
               n789ContagemResultadoItem_ValidadoFM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A789ContagemResultadoItem_ValidadoFM", A789ContagemResultadoItem_ValidadoFM);
               n789ContagemResultadoItem_ValidadoFM = ((false==A789ContagemResultadoItem_ValidadoFM) ? true : false);
               A790ContagemResultadoItem_TipoFM = cgiGet( edtContagemResultadoItem_TipoFM_Internalname);
               n790ContagemResultadoItem_TipoFM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A790ContagemResultadoItem_TipoFM", A790ContagemResultadoItem_TipoFM);
               n790ContagemResultadoItem_TipoFM = (String.IsNullOrEmpty(StringUtil.RTrim( A790ContagemResultadoItem_TipoFM)) ? true : false);
               A786ContagemResultadoItem_DeflatorFM = cgiGet( edtContagemResultadoItem_DeflatorFM_Internalname);
               n786ContagemResultadoItem_DeflatorFM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A786ContagemResultadoItem_DeflatorFM", A786ContagemResultadoItem_DeflatorFM);
               n786ContagemResultadoItem_DeflatorFM = (String.IsNullOrEmpty(StringUtil.RTrim( A786ContagemResultadoItem_DeflatorFM)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_DERFM_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_DERFM_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOITEM_DERFM");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoItem_DERFM_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A787ContagemResultadoItem_DERFM = 0;
                  n787ContagemResultadoItem_DERFM = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A787ContagemResultadoItem_DERFM", StringUtil.LTrim( StringUtil.Str( (decimal)(A787ContagemResultadoItem_DERFM), 4, 0)));
               }
               else
               {
                  A787ContagemResultadoItem_DERFM = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoItem_DERFM_Internalname), ",", "."));
                  n787ContagemResultadoItem_DERFM = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A787ContagemResultadoItem_DERFM", StringUtil.LTrim( StringUtil.Str( (decimal)(A787ContagemResultadoItem_DERFM), 4, 0)));
               }
               n787ContagemResultadoItem_DERFM = ((0==A787ContagemResultadoItem_DERFM) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_RAFM_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_RAFM_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOITEM_RAFM");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoItem_RAFM_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A791ContagemResultadoItem_RAFM = 0;
                  n791ContagemResultadoItem_RAFM = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A791ContagemResultadoItem_RAFM", StringUtil.LTrim( StringUtil.Str( (decimal)(A791ContagemResultadoItem_RAFM), 4, 0)));
               }
               else
               {
                  A791ContagemResultadoItem_RAFM = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoItem_RAFM_Internalname), ",", "."));
                  n791ContagemResultadoItem_RAFM = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A791ContagemResultadoItem_RAFM", StringUtil.LTrim( StringUtil.Str( (decimal)(A791ContagemResultadoItem_RAFM), 4, 0)));
               }
               n791ContagemResultadoItem_RAFM = ((0==A791ContagemResultadoItem_RAFM) ? true : false);
               A795ContagemResultadoItem_CPFM = cgiGet( edtContagemResultadoItem_CPFM_Internalname);
               n795ContagemResultadoItem_CPFM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A795ContagemResultadoItem_CPFM", A795ContagemResultadoItem_CPFM);
               n795ContagemResultadoItem_CPFM = (String.IsNullOrEmpty(StringUtil.RTrim( A795ContagemResultadoItem_CPFM)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_PFBFM_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_PFBFM_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOITEM_PFBFM");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoItem_PFBFM_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A784ContagemResultadoItem_PFBFM = 0;
                  n784ContagemResultadoItem_PFBFM = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A784ContagemResultadoItem_PFBFM", StringUtil.LTrim( StringUtil.Str( A784ContagemResultadoItem_PFBFM, 14, 5)));
               }
               else
               {
                  A784ContagemResultadoItem_PFBFM = context.localUtil.CToN( cgiGet( edtContagemResultadoItem_PFBFM_Internalname), ",", ".");
                  n784ContagemResultadoItem_PFBFM = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A784ContagemResultadoItem_PFBFM", StringUtil.LTrim( StringUtil.Str( A784ContagemResultadoItem_PFBFM, 14, 5)));
               }
               n784ContagemResultadoItem_PFBFM = ((Convert.ToDecimal(0)==A784ContagemResultadoItem_PFBFM) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_PFLFM_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoItem_PFLFM_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOITEM_PFLFM");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoItem_PFLFM_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A785ContagemResultadoItem_PFLFM = 0;
                  n785ContagemResultadoItem_PFLFM = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A785ContagemResultadoItem_PFLFM", StringUtil.LTrim( StringUtil.Str( A785ContagemResultadoItem_PFLFM, 14, 5)));
               }
               else
               {
                  A785ContagemResultadoItem_PFLFM = context.localUtil.CToN( cgiGet( edtContagemResultadoItem_PFLFM_Internalname), ",", ".");
                  n785ContagemResultadoItem_PFLFM = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A785ContagemResultadoItem_PFLFM", StringUtil.LTrim( StringUtil.Str( A785ContagemResultadoItem_PFLFM, 14, 5)));
               }
               n785ContagemResultadoItem_PFLFM = ((Convert.ToDecimal(0)==A785ContagemResultadoItem_PFLFM) ? true : false);
               /* Read saved values. */
               Z772ContagemResultadoItem_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z772ContagemResultadoItem_Codigo"), ",", "."));
               Z773ContagemResultadoItem_ContagemCod = (int)(context.localUtil.CToN( cgiGet( "Z773ContagemResultadoItem_ContagemCod"), ",", "."));
               n773ContagemResultadoItem_ContagemCod = ((0==A773ContagemResultadoItem_ContagemCod) ? true : false);
               Z793ContagemResultadoItem_Demanda = cgiGet( "Z793ContagemResultadoItem_Demanda");
               Z809ContagemResultadoItem_DemandaCorrigida = cgiGet( "Z809ContagemResultadoItem_DemandaCorrigida");
               n809ContagemResultadoItem_DemandaCorrigida = (String.IsNullOrEmpty(StringUtil.RTrim( A809ContagemResultadoItem_DemandaCorrigida)) ? true : false);
               Z794ContagemResultadoItem_CFPS = cgiGet( "Z794ContagemResultadoItem_CFPS");
               n794ContagemResultadoItem_CFPS = (String.IsNullOrEmpty(StringUtil.RTrim( A794ContagemResultadoItem_CFPS)) ? true : false);
               Z792ContagemResultadoItem_DataCnt = context.localUtil.CToD( cgiGet( "Z792ContagemResultadoItem_DataCnt"), 0);
               n792ContagemResultadoItem_DataCnt = ((DateTime.MinValue==A792ContagemResultadoItem_DataCnt) ? true : false);
               Z796ContagemResultadoItem_PFBTot = context.localUtil.CToN( cgiGet( "Z796ContagemResultadoItem_PFBTot"), ",", ".");
               n796ContagemResultadoItem_PFBTot = ((Convert.ToDecimal(0)==A796ContagemResultadoItem_PFBTot) ? true : false);
               Z797ContagemResultadoItem_PFLTot = context.localUtil.CToN( cgiGet( "Z797ContagemResultadoItem_PFLTot"), ",", ".");
               n797ContagemResultadoItem_PFLTot = ((Convert.ToDecimal(0)==A797ContagemResultadoItem_PFLTot) ? true : false);
               Z775ContagemResultadoItem_Tipo = cgiGet( "Z775ContagemResultadoItem_Tipo");
               n775ContagemResultadoItem_Tipo = (String.IsNullOrEmpty(StringUtil.RTrim( A775ContagemResultadoItem_Tipo)) ? true : false);
               Z776ContagemResultadoItem_Deflator = cgiGet( "Z776ContagemResultadoItem_Deflator");
               n776ContagemResultadoItem_Deflator = (String.IsNullOrEmpty(StringUtil.RTrim( A776ContagemResultadoItem_Deflator)) ? true : false);
               Z777ContagemResultadoItem_DER = (short)(context.localUtil.CToN( cgiGet( "Z777ContagemResultadoItem_DER"), ",", "."));
               n777ContagemResultadoItem_DER = ((0==A777ContagemResultadoItem_DER) ? true : false);
               Z778ContagemResultadoItem_RA = (short)(context.localUtil.CToN( cgiGet( "Z778ContagemResultadoItem_RA"), ",", "."));
               n778ContagemResultadoItem_RA = ((0==A778ContagemResultadoItem_RA) ? true : false);
               Z779ContagemResultadoItem_CP = cgiGet( "Z779ContagemResultadoItem_CP");
               n779ContagemResultadoItem_CP = (String.IsNullOrEmpty(StringUtil.RTrim( A779ContagemResultadoItem_CP)) ? true : false);
               Z780ContagemResultadoItem_PFB = context.localUtil.CToN( cgiGet( "Z780ContagemResultadoItem_PFB"), ",", ".");
               n780ContagemResultadoItem_PFB = ((Convert.ToDecimal(0)==A780ContagemResultadoItem_PFB) ? true : false);
               Z781ContagemResultadoItem_PFL = context.localUtil.CToN( cgiGet( "Z781ContagemResultadoItem_PFL"), ",", ".");
               n781ContagemResultadoItem_PFL = ((Convert.ToDecimal(0)==A781ContagemResultadoItem_PFL) ? true : false);
               Z789ContagemResultadoItem_ValidadoFM = StringUtil.StrToBool( cgiGet( "Z789ContagemResultadoItem_ValidadoFM"));
               n789ContagemResultadoItem_ValidadoFM = ((false==A789ContagemResultadoItem_ValidadoFM) ? true : false);
               Z790ContagemResultadoItem_TipoFM = cgiGet( "Z790ContagemResultadoItem_TipoFM");
               n790ContagemResultadoItem_TipoFM = (String.IsNullOrEmpty(StringUtil.RTrim( A790ContagemResultadoItem_TipoFM)) ? true : false);
               Z786ContagemResultadoItem_DeflatorFM = cgiGet( "Z786ContagemResultadoItem_DeflatorFM");
               n786ContagemResultadoItem_DeflatorFM = (String.IsNullOrEmpty(StringUtil.RTrim( A786ContagemResultadoItem_DeflatorFM)) ? true : false);
               Z787ContagemResultadoItem_DERFM = (short)(context.localUtil.CToN( cgiGet( "Z787ContagemResultadoItem_DERFM"), ",", "."));
               n787ContagemResultadoItem_DERFM = ((0==A787ContagemResultadoItem_DERFM) ? true : false);
               Z791ContagemResultadoItem_RAFM = (short)(context.localUtil.CToN( cgiGet( "Z791ContagemResultadoItem_RAFM"), ",", "."));
               n791ContagemResultadoItem_RAFM = ((0==A791ContagemResultadoItem_RAFM) ? true : false);
               Z795ContagemResultadoItem_CPFM = cgiGet( "Z795ContagemResultadoItem_CPFM");
               n795ContagemResultadoItem_CPFM = (String.IsNullOrEmpty(StringUtil.RTrim( A795ContagemResultadoItem_CPFM)) ? true : false);
               Z784ContagemResultadoItem_PFBFM = context.localUtil.CToN( cgiGet( "Z784ContagemResultadoItem_PFBFM"), ",", ".");
               n784ContagemResultadoItem_PFBFM = ((Convert.ToDecimal(0)==A784ContagemResultadoItem_PFBFM) ? true : false);
               Z785ContagemResultadoItem_PFLFM = context.localUtil.CToN( cgiGet( "Z785ContagemResultadoItem_PFLFM"), ",", ".");
               n785ContagemResultadoItem_PFLFM = ((Convert.ToDecimal(0)==A785ContagemResultadoItem_PFLFM) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A772ContagemResultadoItem_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A772ContagemResultadoItem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A772ContagemResultadoItem_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2K100( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes2K100( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption2K0( )
      {
      }

      protected void ZM2K100( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z773ContagemResultadoItem_ContagemCod = T002K3_A773ContagemResultadoItem_ContagemCod[0];
               Z793ContagemResultadoItem_Demanda = T002K3_A793ContagemResultadoItem_Demanda[0];
               Z809ContagemResultadoItem_DemandaCorrigida = T002K3_A809ContagemResultadoItem_DemandaCorrigida[0];
               Z794ContagemResultadoItem_CFPS = T002K3_A794ContagemResultadoItem_CFPS[0];
               Z792ContagemResultadoItem_DataCnt = T002K3_A792ContagemResultadoItem_DataCnt[0];
               Z796ContagemResultadoItem_PFBTot = T002K3_A796ContagemResultadoItem_PFBTot[0];
               Z797ContagemResultadoItem_PFLTot = T002K3_A797ContagemResultadoItem_PFLTot[0];
               Z775ContagemResultadoItem_Tipo = T002K3_A775ContagemResultadoItem_Tipo[0];
               Z776ContagemResultadoItem_Deflator = T002K3_A776ContagemResultadoItem_Deflator[0];
               Z777ContagemResultadoItem_DER = T002K3_A777ContagemResultadoItem_DER[0];
               Z778ContagemResultadoItem_RA = T002K3_A778ContagemResultadoItem_RA[0];
               Z779ContagemResultadoItem_CP = T002K3_A779ContagemResultadoItem_CP[0];
               Z780ContagemResultadoItem_PFB = T002K3_A780ContagemResultadoItem_PFB[0];
               Z781ContagemResultadoItem_PFL = T002K3_A781ContagemResultadoItem_PFL[0];
               Z789ContagemResultadoItem_ValidadoFM = T002K3_A789ContagemResultadoItem_ValidadoFM[0];
               Z790ContagemResultadoItem_TipoFM = T002K3_A790ContagemResultadoItem_TipoFM[0];
               Z786ContagemResultadoItem_DeflatorFM = T002K3_A786ContagemResultadoItem_DeflatorFM[0];
               Z787ContagemResultadoItem_DERFM = T002K3_A787ContagemResultadoItem_DERFM[0];
               Z791ContagemResultadoItem_RAFM = T002K3_A791ContagemResultadoItem_RAFM[0];
               Z795ContagemResultadoItem_CPFM = T002K3_A795ContagemResultadoItem_CPFM[0];
               Z784ContagemResultadoItem_PFBFM = T002K3_A784ContagemResultadoItem_PFBFM[0];
               Z785ContagemResultadoItem_PFLFM = T002K3_A785ContagemResultadoItem_PFLFM[0];
            }
            else
            {
               Z773ContagemResultadoItem_ContagemCod = A773ContagemResultadoItem_ContagemCod;
               Z793ContagemResultadoItem_Demanda = A793ContagemResultadoItem_Demanda;
               Z809ContagemResultadoItem_DemandaCorrigida = A809ContagemResultadoItem_DemandaCorrigida;
               Z794ContagemResultadoItem_CFPS = A794ContagemResultadoItem_CFPS;
               Z792ContagemResultadoItem_DataCnt = A792ContagemResultadoItem_DataCnt;
               Z796ContagemResultadoItem_PFBTot = A796ContagemResultadoItem_PFBTot;
               Z797ContagemResultadoItem_PFLTot = A797ContagemResultadoItem_PFLTot;
               Z775ContagemResultadoItem_Tipo = A775ContagemResultadoItem_Tipo;
               Z776ContagemResultadoItem_Deflator = A776ContagemResultadoItem_Deflator;
               Z777ContagemResultadoItem_DER = A777ContagemResultadoItem_DER;
               Z778ContagemResultadoItem_RA = A778ContagemResultadoItem_RA;
               Z779ContagemResultadoItem_CP = A779ContagemResultadoItem_CP;
               Z780ContagemResultadoItem_PFB = A780ContagemResultadoItem_PFB;
               Z781ContagemResultadoItem_PFL = A781ContagemResultadoItem_PFL;
               Z789ContagemResultadoItem_ValidadoFM = A789ContagemResultadoItem_ValidadoFM;
               Z790ContagemResultadoItem_TipoFM = A790ContagemResultadoItem_TipoFM;
               Z786ContagemResultadoItem_DeflatorFM = A786ContagemResultadoItem_DeflatorFM;
               Z787ContagemResultadoItem_DERFM = A787ContagemResultadoItem_DERFM;
               Z791ContagemResultadoItem_RAFM = A791ContagemResultadoItem_RAFM;
               Z795ContagemResultadoItem_CPFM = A795ContagemResultadoItem_CPFM;
               Z784ContagemResultadoItem_PFBFM = A784ContagemResultadoItem_PFBFM;
               Z785ContagemResultadoItem_PFLFM = A785ContagemResultadoItem_PFLFM;
            }
         }
         if ( GX_JID == -2 )
         {
            Z772ContagemResultadoItem_Codigo = A772ContagemResultadoItem_Codigo;
            Z773ContagemResultadoItem_ContagemCod = A773ContagemResultadoItem_ContagemCod;
            Z793ContagemResultadoItem_Demanda = A793ContagemResultadoItem_Demanda;
            Z809ContagemResultadoItem_DemandaCorrigida = A809ContagemResultadoItem_DemandaCorrigida;
            Z794ContagemResultadoItem_CFPS = A794ContagemResultadoItem_CFPS;
            Z792ContagemResultadoItem_DataCnt = A792ContagemResultadoItem_DataCnt;
            Z796ContagemResultadoItem_PFBTot = A796ContagemResultadoItem_PFBTot;
            Z797ContagemResultadoItem_PFLTot = A797ContagemResultadoItem_PFLTot;
            Z774ContagemResultadoItem_Descricao = A774ContagemResultadoItem_Descricao;
            Z775ContagemResultadoItem_Tipo = A775ContagemResultadoItem_Tipo;
            Z776ContagemResultadoItem_Deflator = A776ContagemResultadoItem_Deflator;
            Z777ContagemResultadoItem_DER = A777ContagemResultadoItem_DER;
            Z778ContagemResultadoItem_RA = A778ContagemResultadoItem_RA;
            Z779ContagemResultadoItem_CP = A779ContagemResultadoItem_CP;
            Z780ContagemResultadoItem_PFB = A780ContagemResultadoItem_PFB;
            Z781ContagemResultadoItem_PFL = A781ContagemResultadoItem_PFL;
            Z782ContagemResultadoItem_Evidencia = A782ContagemResultadoItem_Evidencia;
            Z789ContagemResultadoItem_ValidadoFM = A789ContagemResultadoItem_ValidadoFM;
            Z790ContagemResultadoItem_TipoFM = A790ContagemResultadoItem_TipoFM;
            Z786ContagemResultadoItem_DeflatorFM = A786ContagemResultadoItem_DeflatorFM;
            Z787ContagemResultadoItem_DERFM = A787ContagemResultadoItem_DERFM;
            Z791ContagemResultadoItem_RAFM = A791ContagemResultadoItem_RAFM;
            Z795ContagemResultadoItem_CPFM = A795ContagemResultadoItem_CPFM;
            Z784ContagemResultadoItem_PFBFM = A784ContagemResultadoItem_PFBFM;
            Z785ContagemResultadoItem_PFLFM = A785ContagemResultadoItem_PFLFM;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load2K100( )
      {
         /* Using cursor T002K4 */
         pr_default.execute(2, new Object[] {A772ContagemResultadoItem_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound100 = 1;
            A773ContagemResultadoItem_ContagemCod = T002K4_A773ContagemResultadoItem_ContagemCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A773ContagemResultadoItem_ContagemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A773ContagemResultadoItem_ContagemCod), 6, 0)));
            n773ContagemResultadoItem_ContagemCod = T002K4_n773ContagemResultadoItem_ContagemCod[0];
            A793ContagemResultadoItem_Demanda = T002K4_A793ContagemResultadoItem_Demanda[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A793ContagemResultadoItem_Demanda", A793ContagemResultadoItem_Demanda);
            A809ContagemResultadoItem_DemandaCorrigida = T002K4_A809ContagemResultadoItem_DemandaCorrigida[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A809ContagemResultadoItem_DemandaCorrigida", A809ContagemResultadoItem_DemandaCorrigida);
            n809ContagemResultadoItem_DemandaCorrigida = T002K4_n809ContagemResultadoItem_DemandaCorrigida[0];
            A794ContagemResultadoItem_CFPS = T002K4_A794ContagemResultadoItem_CFPS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A794ContagemResultadoItem_CFPS", A794ContagemResultadoItem_CFPS);
            n794ContagemResultadoItem_CFPS = T002K4_n794ContagemResultadoItem_CFPS[0];
            A792ContagemResultadoItem_DataCnt = T002K4_A792ContagemResultadoItem_DataCnt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A792ContagemResultadoItem_DataCnt", context.localUtil.Format(A792ContagemResultadoItem_DataCnt, "99/99/99"));
            n792ContagemResultadoItem_DataCnt = T002K4_n792ContagemResultadoItem_DataCnt[0];
            A796ContagemResultadoItem_PFBTot = T002K4_A796ContagemResultadoItem_PFBTot[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A796ContagemResultadoItem_PFBTot", StringUtil.LTrim( StringUtil.Str( A796ContagemResultadoItem_PFBTot, 14, 5)));
            n796ContagemResultadoItem_PFBTot = T002K4_n796ContagemResultadoItem_PFBTot[0];
            A797ContagemResultadoItem_PFLTot = T002K4_A797ContagemResultadoItem_PFLTot[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A797ContagemResultadoItem_PFLTot", StringUtil.LTrim( StringUtil.Str( A797ContagemResultadoItem_PFLTot, 14, 5)));
            n797ContagemResultadoItem_PFLTot = T002K4_n797ContagemResultadoItem_PFLTot[0];
            A774ContagemResultadoItem_Descricao = T002K4_A774ContagemResultadoItem_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A774ContagemResultadoItem_Descricao", A774ContagemResultadoItem_Descricao);
            n774ContagemResultadoItem_Descricao = T002K4_n774ContagemResultadoItem_Descricao[0];
            A775ContagemResultadoItem_Tipo = T002K4_A775ContagemResultadoItem_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A775ContagemResultadoItem_Tipo", A775ContagemResultadoItem_Tipo);
            n775ContagemResultadoItem_Tipo = T002K4_n775ContagemResultadoItem_Tipo[0];
            A776ContagemResultadoItem_Deflator = T002K4_A776ContagemResultadoItem_Deflator[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A776ContagemResultadoItem_Deflator", A776ContagemResultadoItem_Deflator);
            n776ContagemResultadoItem_Deflator = T002K4_n776ContagemResultadoItem_Deflator[0];
            A777ContagemResultadoItem_DER = T002K4_A777ContagemResultadoItem_DER[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A777ContagemResultadoItem_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A777ContagemResultadoItem_DER), 4, 0)));
            n777ContagemResultadoItem_DER = T002K4_n777ContagemResultadoItem_DER[0];
            A778ContagemResultadoItem_RA = T002K4_A778ContagemResultadoItem_RA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A778ContagemResultadoItem_RA", StringUtil.LTrim( StringUtil.Str( (decimal)(A778ContagemResultadoItem_RA), 4, 0)));
            n778ContagemResultadoItem_RA = T002K4_n778ContagemResultadoItem_RA[0];
            A779ContagemResultadoItem_CP = T002K4_A779ContagemResultadoItem_CP[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A779ContagemResultadoItem_CP", A779ContagemResultadoItem_CP);
            n779ContagemResultadoItem_CP = T002K4_n779ContagemResultadoItem_CP[0];
            A780ContagemResultadoItem_PFB = T002K4_A780ContagemResultadoItem_PFB[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A780ContagemResultadoItem_PFB", StringUtil.LTrim( StringUtil.Str( A780ContagemResultadoItem_PFB, 14, 5)));
            n780ContagemResultadoItem_PFB = T002K4_n780ContagemResultadoItem_PFB[0];
            A781ContagemResultadoItem_PFL = T002K4_A781ContagemResultadoItem_PFL[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A781ContagemResultadoItem_PFL", StringUtil.LTrim( StringUtil.Str( A781ContagemResultadoItem_PFL, 14, 5)));
            n781ContagemResultadoItem_PFL = T002K4_n781ContagemResultadoItem_PFL[0];
            A782ContagemResultadoItem_Evidencia = T002K4_A782ContagemResultadoItem_Evidencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A782ContagemResultadoItem_Evidencia", A782ContagemResultadoItem_Evidencia);
            n782ContagemResultadoItem_Evidencia = T002K4_n782ContagemResultadoItem_Evidencia[0];
            A789ContagemResultadoItem_ValidadoFM = T002K4_A789ContagemResultadoItem_ValidadoFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A789ContagemResultadoItem_ValidadoFM", A789ContagemResultadoItem_ValidadoFM);
            n789ContagemResultadoItem_ValidadoFM = T002K4_n789ContagemResultadoItem_ValidadoFM[0];
            A790ContagemResultadoItem_TipoFM = T002K4_A790ContagemResultadoItem_TipoFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A790ContagemResultadoItem_TipoFM", A790ContagemResultadoItem_TipoFM);
            n790ContagemResultadoItem_TipoFM = T002K4_n790ContagemResultadoItem_TipoFM[0];
            A786ContagemResultadoItem_DeflatorFM = T002K4_A786ContagemResultadoItem_DeflatorFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A786ContagemResultadoItem_DeflatorFM", A786ContagemResultadoItem_DeflatorFM);
            n786ContagemResultadoItem_DeflatorFM = T002K4_n786ContagemResultadoItem_DeflatorFM[0];
            A787ContagemResultadoItem_DERFM = T002K4_A787ContagemResultadoItem_DERFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A787ContagemResultadoItem_DERFM", StringUtil.LTrim( StringUtil.Str( (decimal)(A787ContagemResultadoItem_DERFM), 4, 0)));
            n787ContagemResultadoItem_DERFM = T002K4_n787ContagemResultadoItem_DERFM[0];
            A791ContagemResultadoItem_RAFM = T002K4_A791ContagemResultadoItem_RAFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A791ContagemResultadoItem_RAFM", StringUtil.LTrim( StringUtil.Str( (decimal)(A791ContagemResultadoItem_RAFM), 4, 0)));
            n791ContagemResultadoItem_RAFM = T002K4_n791ContagemResultadoItem_RAFM[0];
            A795ContagemResultadoItem_CPFM = T002K4_A795ContagemResultadoItem_CPFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A795ContagemResultadoItem_CPFM", A795ContagemResultadoItem_CPFM);
            n795ContagemResultadoItem_CPFM = T002K4_n795ContagemResultadoItem_CPFM[0];
            A784ContagemResultadoItem_PFBFM = T002K4_A784ContagemResultadoItem_PFBFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A784ContagemResultadoItem_PFBFM", StringUtil.LTrim( StringUtil.Str( A784ContagemResultadoItem_PFBFM, 14, 5)));
            n784ContagemResultadoItem_PFBFM = T002K4_n784ContagemResultadoItem_PFBFM[0];
            A785ContagemResultadoItem_PFLFM = T002K4_A785ContagemResultadoItem_PFLFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A785ContagemResultadoItem_PFLFM", StringUtil.LTrim( StringUtil.Str( A785ContagemResultadoItem_PFLFM, 14, 5)));
            n785ContagemResultadoItem_PFLFM = T002K4_n785ContagemResultadoItem_PFLFM[0];
            ZM2K100( -2) ;
         }
         pr_default.close(2);
         OnLoadActions2K100( ) ;
      }

      protected void OnLoadActions2K100( )
      {
      }

      protected void CheckExtendedTable2K100( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A792ContagemResultadoItem_DataCnt) || ( A792ContagemResultadoItem_DataCnt >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Contagem Resultado Item_Data Cnt fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADOITEM_DATACNT");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoItem_DataCnt_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors2K100( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey2K100( )
      {
         /* Using cursor T002K5 */
         pr_default.execute(3, new Object[] {A772ContagemResultadoItem_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound100 = 1;
         }
         else
         {
            RcdFound100 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002K3 */
         pr_default.execute(1, new Object[] {A772ContagemResultadoItem_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2K100( 2) ;
            RcdFound100 = 1;
            A772ContagemResultadoItem_Codigo = T002K3_A772ContagemResultadoItem_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A772ContagemResultadoItem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A772ContagemResultadoItem_Codigo), 6, 0)));
            A773ContagemResultadoItem_ContagemCod = T002K3_A773ContagemResultadoItem_ContagemCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A773ContagemResultadoItem_ContagemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A773ContagemResultadoItem_ContagemCod), 6, 0)));
            n773ContagemResultadoItem_ContagemCod = T002K3_n773ContagemResultadoItem_ContagemCod[0];
            A793ContagemResultadoItem_Demanda = T002K3_A793ContagemResultadoItem_Demanda[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A793ContagemResultadoItem_Demanda", A793ContagemResultadoItem_Demanda);
            A809ContagemResultadoItem_DemandaCorrigida = T002K3_A809ContagemResultadoItem_DemandaCorrigida[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A809ContagemResultadoItem_DemandaCorrigida", A809ContagemResultadoItem_DemandaCorrigida);
            n809ContagemResultadoItem_DemandaCorrigida = T002K3_n809ContagemResultadoItem_DemandaCorrigida[0];
            A794ContagemResultadoItem_CFPS = T002K3_A794ContagemResultadoItem_CFPS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A794ContagemResultadoItem_CFPS", A794ContagemResultadoItem_CFPS);
            n794ContagemResultadoItem_CFPS = T002K3_n794ContagemResultadoItem_CFPS[0];
            A792ContagemResultadoItem_DataCnt = T002K3_A792ContagemResultadoItem_DataCnt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A792ContagemResultadoItem_DataCnt", context.localUtil.Format(A792ContagemResultadoItem_DataCnt, "99/99/99"));
            n792ContagemResultadoItem_DataCnt = T002K3_n792ContagemResultadoItem_DataCnt[0];
            A796ContagemResultadoItem_PFBTot = T002K3_A796ContagemResultadoItem_PFBTot[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A796ContagemResultadoItem_PFBTot", StringUtil.LTrim( StringUtil.Str( A796ContagemResultadoItem_PFBTot, 14, 5)));
            n796ContagemResultadoItem_PFBTot = T002K3_n796ContagemResultadoItem_PFBTot[0];
            A797ContagemResultadoItem_PFLTot = T002K3_A797ContagemResultadoItem_PFLTot[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A797ContagemResultadoItem_PFLTot", StringUtil.LTrim( StringUtil.Str( A797ContagemResultadoItem_PFLTot, 14, 5)));
            n797ContagemResultadoItem_PFLTot = T002K3_n797ContagemResultadoItem_PFLTot[0];
            A774ContagemResultadoItem_Descricao = T002K3_A774ContagemResultadoItem_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A774ContagemResultadoItem_Descricao", A774ContagemResultadoItem_Descricao);
            n774ContagemResultadoItem_Descricao = T002K3_n774ContagemResultadoItem_Descricao[0];
            A775ContagemResultadoItem_Tipo = T002K3_A775ContagemResultadoItem_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A775ContagemResultadoItem_Tipo", A775ContagemResultadoItem_Tipo);
            n775ContagemResultadoItem_Tipo = T002K3_n775ContagemResultadoItem_Tipo[0];
            A776ContagemResultadoItem_Deflator = T002K3_A776ContagemResultadoItem_Deflator[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A776ContagemResultadoItem_Deflator", A776ContagemResultadoItem_Deflator);
            n776ContagemResultadoItem_Deflator = T002K3_n776ContagemResultadoItem_Deflator[0];
            A777ContagemResultadoItem_DER = T002K3_A777ContagemResultadoItem_DER[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A777ContagemResultadoItem_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A777ContagemResultadoItem_DER), 4, 0)));
            n777ContagemResultadoItem_DER = T002K3_n777ContagemResultadoItem_DER[0];
            A778ContagemResultadoItem_RA = T002K3_A778ContagemResultadoItem_RA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A778ContagemResultadoItem_RA", StringUtil.LTrim( StringUtil.Str( (decimal)(A778ContagemResultadoItem_RA), 4, 0)));
            n778ContagemResultadoItem_RA = T002K3_n778ContagemResultadoItem_RA[0];
            A779ContagemResultadoItem_CP = T002K3_A779ContagemResultadoItem_CP[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A779ContagemResultadoItem_CP", A779ContagemResultadoItem_CP);
            n779ContagemResultadoItem_CP = T002K3_n779ContagemResultadoItem_CP[0];
            A780ContagemResultadoItem_PFB = T002K3_A780ContagemResultadoItem_PFB[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A780ContagemResultadoItem_PFB", StringUtil.LTrim( StringUtil.Str( A780ContagemResultadoItem_PFB, 14, 5)));
            n780ContagemResultadoItem_PFB = T002K3_n780ContagemResultadoItem_PFB[0];
            A781ContagemResultadoItem_PFL = T002K3_A781ContagemResultadoItem_PFL[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A781ContagemResultadoItem_PFL", StringUtil.LTrim( StringUtil.Str( A781ContagemResultadoItem_PFL, 14, 5)));
            n781ContagemResultadoItem_PFL = T002K3_n781ContagemResultadoItem_PFL[0];
            A782ContagemResultadoItem_Evidencia = T002K3_A782ContagemResultadoItem_Evidencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A782ContagemResultadoItem_Evidencia", A782ContagemResultadoItem_Evidencia);
            n782ContagemResultadoItem_Evidencia = T002K3_n782ContagemResultadoItem_Evidencia[0];
            A789ContagemResultadoItem_ValidadoFM = T002K3_A789ContagemResultadoItem_ValidadoFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A789ContagemResultadoItem_ValidadoFM", A789ContagemResultadoItem_ValidadoFM);
            n789ContagemResultadoItem_ValidadoFM = T002K3_n789ContagemResultadoItem_ValidadoFM[0];
            A790ContagemResultadoItem_TipoFM = T002K3_A790ContagemResultadoItem_TipoFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A790ContagemResultadoItem_TipoFM", A790ContagemResultadoItem_TipoFM);
            n790ContagemResultadoItem_TipoFM = T002K3_n790ContagemResultadoItem_TipoFM[0];
            A786ContagemResultadoItem_DeflatorFM = T002K3_A786ContagemResultadoItem_DeflatorFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A786ContagemResultadoItem_DeflatorFM", A786ContagemResultadoItem_DeflatorFM);
            n786ContagemResultadoItem_DeflatorFM = T002K3_n786ContagemResultadoItem_DeflatorFM[0];
            A787ContagemResultadoItem_DERFM = T002K3_A787ContagemResultadoItem_DERFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A787ContagemResultadoItem_DERFM", StringUtil.LTrim( StringUtil.Str( (decimal)(A787ContagemResultadoItem_DERFM), 4, 0)));
            n787ContagemResultadoItem_DERFM = T002K3_n787ContagemResultadoItem_DERFM[0];
            A791ContagemResultadoItem_RAFM = T002K3_A791ContagemResultadoItem_RAFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A791ContagemResultadoItem_RAFM", StringUtil.LTrim( StringUtil.Str( (decimal)(A791ContagemResultadoItem_RAFM), 4, 0)));
            n791ContagemResultadoItem_RAFM = T002K3_n791ContagemResultadoItem_RAFM[0];
            A795ContagemResultadoItem_CPFM = T002K3_A795ContagemResultadoItem_CPFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A795ContagemResultadoItem_CPFM", A795ContagemResultadoItem_CPFM);
            n795ContagemResultadoItem_CPFM = T002K3_n795ContagemResultadoItem_CPFM[0];
            A784ContagemResultadoItem_PFBFM = T002K3_A784ContagemResultadoItem_PFBFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A784ContagemResultadoItem_PFBFM", StringUtil.LTrim( StringUtil.Str( A784ContagemResultadoItem_PFBFM, 14, 5)));
            n784ContagemResultadoItem_PFBFM = T002K3_n784ContagemResultadoItem_PFBFM[0];
            A785ContagemResultadoItem_PFLFM = T002K3_A785ContagemResultadoItem_PFLFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A785ContagemResultadoItem_PFLFM", StringUtil.LTrim( StringUtil.Str( A785ContagemResultadoItem_PFLFM, 14, 5)));
            n785ContagemResultadoItem_PFLFM = T002K3_n785ContagemResultadoItem_PFLFM[0];
            Z772ContagemResultadoItem_Codigo = A772ContagemResultadoItem_Codigo;
            sMode100 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load2K100( ) ;
            if ( AnyError == 1 )
            {
               RcdFound100 = 0;
               InitializeNonKey2K100( ) ;
            }
            Gx_mode = sMode100;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound100 = 0;
            InitializeNonKey2K100( ) ;
            sMode100 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode100;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2K100( ) ;
         if ( RcdFound100 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound100 = 0;
         /* Using cursor T002K6 */
         pr_default.execute(4, new Object[] {A772ContagemResultadoItem_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T002K6_A772ContagemResultadoItem_Codigo[0] < A772ContagemResultadoItem_Codigo ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T002K6_A772ContagemResultadoItem_Codigo[0] > A772ContagemResultadoItem_Codigo ) ) )
            {
               A772ContagemResultadoItem_Codigo = T002K6_A772ContagemResultadoItem_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A772ContagemResultadoItem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A772ContagemResultadoItem_Codigo), 6, 0)));
               RcdFound100 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound100 = 0;
         /* Using cursor T002K7 */
         pr_default.execute(5, new Object[] {A772ContagemResultadoItem_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T002K7_A772ContagemResultadoItem_Codigo[0] > A772ContagemResultadoItem_Codigo ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T002K7_A772ContagemResultadoItem_Codigo[0] < A772ContagemResultadoItem_Codigo ) ) )
            {
               A772ContagemResultadoItem_Codigo = T002K7_A772ContagemResultadoItem_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A772ContagemResultadoItem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A772ContagemResultadoItem_Codigo), 6, 0)));
               RcdFound100 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2K100( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagemResultadoItem_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2K100( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound100 == 1 )
            {
               if ( A772ContagemResultadoItem_Codigo != Z772ContagemResultadoItem_Codigo )
               {
                  A772ContagemResultadoItem_Codigo = Z772ContagemResultadoItem_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A772ContagemResultadoItem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A772ContagemResultadoItem_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMRESULTADOITEM_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoItem_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemResultadoItem_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update2K100( ) ;
                  GX_FocusControl = edtContagemResultadoItem_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A772ContagemResultadoItem_Codigo != Z772ContagemResultadoItem_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtContagemResultadoItem_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2K100( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMRESULTADOITEM_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContagemResultadoItem_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtContagemResultadoItem_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2K100( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A772ContagemResultadoItem_Codigo != Z772ContagemResultadoItem_Codigo )
         {
            A772ContagemResultadoItem_Codigo = Z772ContagemResultadoItem_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A772ContagemResultadoItem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A772ContagemResultadoItem_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMRESULTADOITEM_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoItem_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemResultadoItem_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound100 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTAGEMRESULTADOITEM_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoItem_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtContagemResultadoItem_ContagemCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart2K100( ) ;
         if ( RcdFound100 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoItem_ContagemCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd2K100( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound100 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoItem_ContagemCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound100 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoItem_ContagemCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart2K100( ) ;
         if ( RcdFound100 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound100 != 0 )
            {
               ScanNext2K100( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoItem_ContagemCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd2K100( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency2K100( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002K2 */
            pr_default.execute(0, new Object[] {A772ContagemResultadoItem_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoItem"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z773ContagemResultadoItem_ContagemCod != T002K2_A773ContagemResultadoItem_ContagemCod[0] ) || ( StringUtil.StrCmp(Z793ContagemResultadoItem_Demanda, T002K2_A793ContagemResultadoItem_Demanda[0]) != 0 ) || ( StringUtil.StrCmp(Z809ContagemResultadoItem_DemandaCorrigida, T002K2_A809ContagemResultadoItem_DemandaCorrigida[0]) != 0 ) || ( StringUtil.StrCmp(Z794ContagemResultadoItem_CFPS, T002K2_A794ContagemResultadoItem_CFPS[0]) != 0 ) || ( Z792ContagemResultadoItem_DataCnt != T002K2_A792ContagemResultadoItem_DataCnt[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z796ContagemResultadoItem_PFBTot != T002K2_A796ContagemResultadoItem_PFBTot[0] ) || ( Z797ContagemResultadoItem_PFLTot != T002K2_A797ContagemResultadoItem_PFLTot[0] ) || ( StringUtil.StrCmp(Z775ContagemResultadoItem_Tipo, T002K2_A775ContagemResultadoItem_Tipo[0]) != 0 ) || ( StringUtil.StrCmp(Z776ContagemResultadoItem_Deflator, T002K2_A776ContagemResultadoItem_Deflator[0]) != 0 ) || ( Z777ContagemResultadoItem_DER != T002K2_A777ContagemResultadoItem_DER[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z778ContagemResultadoItem_RA != T002K2_A778ContagemResultadoItem_RA[0] ) || ( StringUtil.StrCmp(Z779ContagemResultadoItem_CP, T002K2_A779ContagemResultadoItem_CP[0]) != 0 ) || ( Z780ContagemResultadoItem_PFB != T002K2_A780ContagemResultadoItem_PFB[0] ) || ( Z781ContagemResultadoItem_PFL != T002K2_A781ContagemResultadoItem_PFL[0] ) || ( Z789ContagemResultadoItem_ValidadoFM != T002K2_A789ContagemResultadoItem_ValidadoFM[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z790ContagemResultadoItem_TipoFM, T002K2_A790ContagemResultadoItem_TipoFM[0]) != 0 ) || ( StringUtil.StrCmp(Z786ContagemResultadoItem_DeflatorFM, T002K2_A786ContagemResultadoItem_DeflatorFM[0]) != 0 ) || ( Z787ContagemResultadoItem_DERFM != T002K2_A787ContagemResultadoItem_DERFM[0] ) || ( Z791ContagemResultadoItem_RAFM != T002K2_A791ContagemResultadoItem_RAFM[0] ) || ( StringUtil.StrCmp(Z795ContagemResultadoItem_CPFM, T002K2_A795ContagemResultadoItem_CPFM[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z784ContagemResultadoItem_PFBFM != T002K2_A784ContagemResultadoItem_PFBFM[0] ) || ( Z785ContagemResultadoItem_PFLFM != T002K2_A785ContagemResultadoItem_PFLFM[0] ) )
            {
               if ( Z773ContagemResultadoItem_ContagemCod != T002K2_A773ContagemResultadoItem_ContagemCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_ContagemCod");
                  GXUtil.WriteLogRaw("Old: ",Z773ContagemResultadoItem_ContagemCod);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A773ContagemResultadoItem_ContagemCod[0]);
               }
               if ( StringUtil.StrCmp(Z793ContagemResultadoItem_Demanda, T002K2_A793ContagemResultadoItem_Demanda[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_Demanda");
                  GXUtil.WriteLogRaw("Old: ",Z793ContagemResultadoItem_Demanda);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A793ContagemResultadoItem_Demanda[0]);
               }
               if ( StringUtil.StrCmp(Z809ContagemResultadoItem_DemandaCorrigida, T002K2_A809ContagemResultadoItem_DemandaCorrigida[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_DemandaCorrigida");
                  GXUtil.WriteLogRaw("Old: ",Z809ContagemResultadoItem_DemandaCorrigida);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A809ContagemResultadoItem_DemandaCorrigida[0]);
               }
               if ( StringUtil.StrCmp(Z794ContagemResultadoItem_CFPS, T002K2_A794ContagemResultadoItem_CFPS[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_CFPS");
                  GXUtil.WriteLogRaw("Old: ",Z794ContagemResultadoItem_CFPS);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A794ContagemResultadoItem_CFPS[0]);
               }
               if ( Z792ContagemResultadoItem_DataCnt != T002K2_A792ContagemResultadoItem_DataCnt[0] )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_DataCnt");
                  GXUtil.WriteLogRaw("Old: ",Z792ContagemResultadoItem_DataCnt);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A792ContagemResultadoItem_DataCnt[0]);
               }
               if ( Z796ContagemResultadoItem_PFBTot != T002K2_A796ContagemResultadoItem_PFBTot[0] )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_PFBTot");
                  GXUtil.WriteLogRaw("Old: ",Z796ContagemResultadoItem_PFBTot);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A796ContagemResultadoItem_PFBTot[0]);
               }
               if ( Z797ContagemResultadoItem_PFLTot != T002K2_A797ContagemResultadoItem_PFLTot[0] )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_PFLTot");
                  GXUtil.WriteLogRaw("Old: ",Z797ContagemResultadoItem_PFLTot);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A797ContagemResultadoItem_PFLTot[0]);
               }
               if ( StringUtil.StrCmp(Z775ContagemResultadoItem_Tipo, T002K2_A775ContagemResultadoItem_Tipo[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z775ContagemResultadoItem_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A775ContagemResultadoItem_Tipo[0]);
               }
               if ( StringUtil.StrCmp(Z776ContagemResultadoItem_Deflator, T002K2_A776ContagemResultadoItem_Deflator[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_Deflator");
                  GXUtil.WriteLogRaw("Old: ",Z776ContagemResultadoItem_Deflator);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A776ContagemResultadoItem_Deflator[0]);
               }
               if ( Z777ContagemResultadoItem_DER != T002K2_A777ContagemResultadoItem_DER[0] )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_DER");
                  GXUtil.WriteLogRaw("Old: ",Z777ContagemResultadoItem_DER);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A777ContagemResultadoItem_DER[0]);
               }
               if ( Z778ContagemResultadoItem_RA != T002K2_A778ContagemResultadoItem_RA[0] )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_RA");
                  GXUtil.WriteLogRaw("Old: ",Z778ContagemResultadoItem_RA);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A778ContagemResultadoItem_RA[0]);
               }
               if ( StringUtil.StrCmp(Z779ContagemResultadoItem_CP, T002K2_A779ContagemResultadoItem_CP[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_CP");
                  GXUtil.WriteLogRaw("Old: ",Z779ContagemResultadoItem_CP);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A779ContagemResultadoItem_CP[0]);
               }
               if ( Z780ContagemResultadoItem_PFB != T002K2_A780ContagemResultadoItem_PFB[0] )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_PFB");
                  GXUtil.WriteLogRaw("Old: ",Z780ContagemResultadoItem_PFB);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A780ContagemResultadoItem_PFB[0]);
               }
               if ( Z781ContagemResultadoItem_PFL != T002K2_A781ContagemResultadoItem_PFL[0] )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_PFL");
                  GXUtil.WriteLogRaw("Old: ",Z781ContagemResultadoItem_PFL);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A781ContagemResultadoItem_PFL[0]);
               }
               if ( Z789ContagemResultadoItem_ValidadoFM != T002K2_A789ContagemResultadoItem_ValidadoFM[0] )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_ValidadoFM");
                  GXUtil.WriteLogRaw("Old: ",Z789ContagemResultadoItem_ValidadoFM);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A789ContagemResultadoItem_ValidadoFM[0]);
               }
               if ( StringUtil.StrCmp(Z790ContagemResultadoItem_TipoFM, T002K2_A790ContagemResultadoItem_TipoFM[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_TipoFM");
                  GXUtil.WriteLogRaw("Old: ",Z790ContagemResultadoItem_TipoFM);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A790ContagemResultadoItem_TipoFM[0]);
               }
               if ( StringUtil.StrCmp(Z786ContagemResultadoItem_DeflatorFM, T002K2_A786ContagemResultadoItem_DeflatorFM[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_DeflatorFM");
                  GXUtil.WriteLogRaw("Old: ",Z786ContagemResultadoItem_DeflatorFM);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A786ContagemResultadoItem_DeflatorFM[0]);
               }
               if ( Z787ContagemResultadoItem_DERFM != T002K2_A787ContagemResultadoItem_DERFM[0] )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_DERFM");
                  GXUtil.WriteLogRaw("Old: ",Z787ContagemResultadoItem_DERFM);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A787ContagemResultadoItem_DERFM[0]);
               }
               if ( Z791ContagemResultadoItem_RAFM != T002K2_A791ContagemResultadoItem_RAFM[0] )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_RAFM");
                  GXUtil.WriteLogRaw("Old: ",Z791ContagemResultadoItem_RAFM);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A791ContagemResultadoItem_RAFM[0]);
               }
               if ( StringUtil.StrCmp(Z795ContagemResultadoItem_CPFM, T002K2_A795ContagemResultadoItem_CPFM[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_CPFM");
                  GXUtil.WriteLogRaw("Old: ",Z795ContagemResultadoItem_CPFM);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A795ContagemResultadoItem_CPFM[0]);
               }
               if ( Z784ContagemResultadoItem_PFBFM != T002K2_A784ContagemResultadoItem_PFBFM[0] )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_PFBFM");
                  GXUtil.WriteLogRaw("Old: ",Z784ContagemResultadoItem_PFBFM);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A784ContagemResultadoItem_PFBFM[0]);
               }
               if ( Z785ContagemResultadoItem_PFLFM != T002K2_A785ContagemResultadoItem_PFLFM[0] )
               {
                  GXUtil.WriteLog("contagemresultadoitem:[seudo value changed for attri]"+"ContagemResultadoItem_PFLFM");
                  GXUtil.WriteLogRaw("Old: ",Z785ContagemResultadoItem_PFLFM);
                  GXUtil.WriteLogRaw("Current: ",T002K2_A785ContagemResultadoItem_PFLFM[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoItem"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2K100( )
      {
         BeforeValidate2K100( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2K100( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2K100( 0) ;
            CheckOptimisticConcurrency2K100( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2K100( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2K100( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002K8 */
                     pr_default.execute(6, new Object[] {n773ContagemResultadoItem_ContagemCod, A773ContagemResultadoItem_ContagemCod, A793ContagemResultadoItem_Demanda, n809ContagemResultadoItem_DemandaCorrigida, A809ContagemResultadoItem_DemandaCorrigida, n794ContagemResultadoItem_CFPS, A794ContagemResultadoItem_CFPS, n792ContagemResultadoItem_DataCnt, A792ContagemResultadoItem_DataCnt, n796ContagemResultadoItem_PFBTot, A796ContagemResultadoItem_PFBTot, n797ContagemResultadoItem_PFLTot, A797ContagemResultadoItem_PFLTot, n774ContagemResultadoItem_Descricao, A774ContagemResultadoItem_Descricao, n775ContagemResultadoItem_Tipo, A775ContagemResultadoItem_Tipo, n776ContagemResultadoItem_Deflator, A776ContagemResultadoItem_Deflator, n777ContagemResultadoItem_DER, A777ContagemResultadoItem_DER, n778ContagemResultadoItem_RA, A778ContagemResultadoItem_RA, n779ContagemResultadoItem_CP, A779ContagemResultadoItem_CP, n780ContagemResultadoItem_PFB, A780ContagemResultadoItem_PFB, n781ContagemResultadoItem_PFL, A781ContagemResultadoItem_PFL, n782ContagemResultadoItem_Evidencia, A782ContagemResultadoItem_Evidencia, n789ContagemResultadoItem_ValidadoFM, A789ContagemResultadoItem_ValidadoFM, n790ContagemResultadoItem_TipoFM, A790ContagemResultadoItem_TipoFM, n786ContagemResultadoItem_DeflatorFM, A786ContagemResultadoItem_DeflatorFM, n787ContagemResultadoItem_DERFM, A787ContagemResultadoItem_DERFM, n791ContagemResultadoItem_RAFM, A791ContagemResultadoItem_RAFM, n795ContagemResultadoItem_CPFM, A795ContagemResultadoItem_CPFM, n784ContagemResultadoItem_PFBFM, A784ContagemResultadoItem_PFBFM, n785ContagemResultadoItem_PFLFM, A785ContagemResultadoItem_PFLFM});
                     A772ContagemResultadoItem_Codigo = T002K8_A772ContagemResultadoItem_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A772ContagemResultadoItem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A772ContagemResultadoItem_Codigo), 6, 0)));
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoItem") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2K0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2K100( ) ;
            }
            EndLevel2K100( ) ;
         }
         CloseExtendedTableCursors2K100( ) ;
      }

      protected void Update2K100( )
      {
         BeforeValidate2K100( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2K100( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2K100( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2K100( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2K100( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002K9 */
                     pr_default.execute(7, new Object[] {n773ContagemResultadoItem_ContagemCod, A773ContagemResultadoItem_ContagemCod, A793ContagemResultadoItem_Demanda, n809ContagemResultadoItem_DemandaCorrigida, A809ContagemResultadoItem_DemandaCorrigida, n794ContagemResultadoItem_CFPS, A794ContagemResultadoItem_CFPS, n792ContagemResultadoItem_DataCnt, A792ContagemResultadoItem_DataCnt, n796ContagemResultadoItem_PFBTot, A796ContagemResultadoItem_PFBTot, n797ContagemResultadoItem_PFLTot, A797ContagemResultadoItem_PFLTot, n774ContagemResultadoItem_Descricao, A774ContagemResultadoItem_Descricao, n775ContagemResultadoItem_Tipo, A775ContagemResultadoItem_Tipo, n776ContagemResultadoItem_Deflator, A776ContagemResultadoItem_Deflator, n777ContagemResultadoItem_DER, A777ContagemResultadoItem_DER, n778ContagemResultadoItem_RA, A778ContagemResultadoItem_RA, n779ContagemResultadoItem_CP, A779ContagemResultadoItem_CP, n780ContagemResultadoItem_PFB, A780ContagemResultadoItem_PFB, n781ContagemResultadoItem_PFL, A781ContagemResultadoItem_PFL, n782ContagemResultadoItem_Evidencia, A782ContagemResultadoItem_Evidencia, n789ContagemResultadoItem_ValidadoFM, A789ContagemResultadoItem_ValidadoFM, n790ContagemResultadoItem_TipoFM, A790ContagemResultadoItem_TipoFM, n786ContagemResultadoItem_DeflatorFM, A786ContagemResultadoItem_DeflatorFM, n787ContagemResultadoItem_DERFM, A787ContagemResultadoItem_DERFM, n791ContagemResultadoItem_RAFM, A791ContagemResultadoItem_RAFM, n795ContagemResultadoItem_CPFM, A795ContagemResultadoItem_CPFM, n784ContagemResultadoItem_PFBFM, A784ContagemResultadoItem_PFBFM, n785ContagemResultadoItem_PFLFM, A785ContagemResultadoItem_PFLFM, A772ContagemResultadoItem_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoItem") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoItem"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2K100( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption2K0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2K100( ) ;
         }
         CloseExtendedTableCursors2K100( ) ;
      }

      protected void DeferredUpdate2K100( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate2K100( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2K100( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2K100( ) ;
            AfterConfirm2K100( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2K100( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002K10 */
                  pr_default.execute(8, new Object[] {A772ContagemResultadoItem_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoItem") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound100 == 0 )
                        {
                           InitAll2K100( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption2K0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode100 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel2K100( ) ;
         Gx_mode = sMode100;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls2K100( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel2K100( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2K100( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "ContagemResultadoItem");
            if ( AnyError == 0 )
            {
               ConfirmValues2K0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "ContagemResultadoItem");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2K100( )
      {
         /* Using cursor T002K11 */
         pr_default.execute(9);
         RcdFound100 = 0;
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound100 = 1;
            A772ContagemResultadoItem_Codigo = T002K11_A772ContagemResultadoItem_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A772ContagemResultadoItem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A772ContagemResultadoItem_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2K100( )
      {
         /* Scan next routine */
         pr_default.readNext(9);
         RcdFound100 = 0;
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound100 = 1;
            A772ContagemResultadoItem_Codigo = T002K11_A772ContagemResultadoItem_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A772ContagemResultadoItem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A772ContagemResultadoItem_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2K100( )
      {
         pr_default.close(9);
      }

      protected void AfterConfirm2K100( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2K100( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2K100( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2K100( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2K100( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2K100( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2K100( )
      {
         edtContagemResultadoItem_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_Codigo_Enabled), 5, 0)));
         edtContagemResultadoItem_ContagemCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_ContagemCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_ContagemCod_Enabled), 5, 0)));
         edtContagemResultadoItem_Demanda_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_Demanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_Demanda_Enabled), 5, 0)));
         edtContagemResultadoItem_DemandaCorrigida_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_DemandaCorrigida_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_DemandaCorrigida_Enabled), 5, 0)));
         edtContagemResultadoItem_CFPS_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_CFPS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_CFPS_Enabled), 5, 0)));
         edtContagemResultadoItem_DataCnt_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_DataCnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_DataCnt_Enabled), 5, 0)));
         edtContagemResultadoItem_PFBTot_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_PFBTot_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_PFBTot_Enabled), 5, 0)));
         edtContagemResultadoItem_PFLTot_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_PFLTot_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_PFLTot_Enabled), 5, 0)));
         edtContagemResultadoItem_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_Descricao_Enabled), 5, 0)));
         edtContagemResultadoItem_Tipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_Tipo_Enabled), 5, 0)));
         edtContagemResultadoItem_Deflator_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_Deflator_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_Deflator_Enabled), 5, 0)));
         edtContagemResultadoItem_DER_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_DER_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_DER_Enabled), 5, 0)));
         edtContagemResultadoItem_RA_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_RA_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_RA_Enabled), 5, 0)));
         edtContagemResultadoItem_CP_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_CP_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_CP_Enabled), 5, 0)));
         edtContagemResultadoItem_PFB_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_PFB_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_PFB_Enabled), 5, 0)));
         edtContagemResultadoItem_PFL_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_PFL_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_PFL_Enabled), 5, 0)));
         edtContagemResultadoItem_Evidencia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_Evidencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_Evidencia_Enabled), 5, 0)));
         chkContagemResultadoItem_ValidadoFM.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContagemResultadoItem_ValidadoFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContagemResultadoItem_ValidadoFM.Enabled), 5, 0)));
         edtContagemResultadoItem_TipoFM_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_TipoFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_TipoFM_Enabled), 5, 0)));
         edtContagemResultadoItem_DeflatorFM_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_DeflatorFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_DeflatorFM_Enabled), 5, 0)));
         edtContagemResultadoItem_DERFM_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_DERFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_DERFM_Enabled), 5, 0)));
         edtContagemResultadoItem_RAFM_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_RAFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_RAFM_Enabled), 5, 0)));
         edtContagemResultadoItem_CPFM_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_CPFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_CPFM_Enabled), 5, 0)));
         edtContagemResultadoItem_PFBFM_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_PFBFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_PFBFM_Enabled), 5, 0)));
         edtContagemResultadoItem_PFLFM_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoItem_PFLFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoItem_PFLFM_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2K0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117231970");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadoitem.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z772ContagemResultadoItem_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z772ContagemResultadoItem_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z773ContagemResultadoItem_ContagemCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z773ContagemResultadoItem_ContagemCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z793ContagemResultadoItem_Demanda", Z793ContagemResultadoItem_Demanda);
         GxWebStd.gx_hidden_field( context, "Z809ContagemResultadoItem_DemandaCorrigida", Z809ContagemResultadoItem_DemandaCorrigida);
         GxWebStd.gx_hidden_field( context, "Z794ContagemResultadoItem_CFPS", Z794ContagemResultadoItem_CFPS);
         GxWebStd.gx_hidden_field( context, "Z792ContagemResultadoItem_DataCnt", context.localUtil.DToC( Z792ContagemResultadoItem_DataCnt, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z796ContagemResultadoItem_PFBTot", StringUtil.LTrim( StringUtil.NToC( Z796ContagemResultadoItem_PFBTot, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z797ContagemResultadoItem_PFLTot", StringUtil.LTrim( StringUtil.NToC( Z797ContagemResultadoItem_PFLTot, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z775ContagemResultadoItem_Tipo", Z775ContagemResultadoItem_Tipo);
         GxWebStd.gx_hidden_field( context, "Z776ContagemResultadoItem_Deflator", Z776ContagemResultadoItem_Deflator);
         GxWebStd.gx_hidden_field( context, "Z777ContagemResultadoItem_DER", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z777ContagemResultadoItem_DER), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z778ContagemResultadoItem_RA", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z778ContagemResultadoItem_RA), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z779ContagemResultadoItem_CP", StringUtil.RTrim( Z779ContagemResultadoItem_CP));
         GxWebStd.gx_hidden_field( context, "Z780ContagemResultadoItem_PFB", StringUtil.LTrim( StringUtil.NToC( Z780ContagemResultadoItem_PFB, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z781ContagemResultadoItem_PFL", StringUtil.LTrim( StringUtil.NToC( Z781ContagemResultadoItem_PFL, 14, 5, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z789ContagemResultadoItem_ValidadoFM", Z789ContagemResultadoItem_ValidadoFM);
         GxWebStd.gx_hidden_field( context, "Z790ContagemResultadoItem_TipoFM", StringUtil.RTrim( Z790ContagemResultadoItem_TipoFM));
         GxWebStd.gx_hidden_field( context, "Z786ContagemResultadoItem_DeflatorFM", Z786ContagemResultadoItem_DeflatorFM);
         GxWebStd.gx_hidden_field( context, "Z787ContagemResultadoItem_DERFM", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z787ContagemResultadoItem_DERFM), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z791ContagemResultadoItem_RAFM", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z791ContagemResultadoItem_RAFM), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z795ContagemResultadoItem_CPFM", StringUtil.RTrim( Z795ContagemResultadoItem_CPFM));
         GxWebStd.gx_hidden_field( context, "Z784ContagemResultadoItem_PFBFM", StringUtil.LTrim( StringUtil.NToC( Z784ContagemResultadoItem_PFBFM, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z785ContagemResultadoItem_PFLFM", StringUtil.LTrim( StringUtil.NToC( Z785ContagemResultadoItem_PFLFM, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemresultadoitem.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoItem" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Item" ;
      }

      protected void InitializeNonKey2K100( )
      {
         A773ContagemResultadoItem_ContagemCod = 0;
         n773ContagemResultadoItem_ContagemCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A773ContagemResultadoItem_ContagemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A773ContagemResultadoItem_ContagemCod), 6, 0)));
         n773ContagemResultadoItem_ContagemCod = ((0==A773ContagemResultadoItem_ContagemCod) ? true : false);
         A793ContagemResultadoItem_Demanda = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A793ContagemResultadoItem_Demanda", A793ContagemResultadoItem_Demanda);
         A809ContagemResultadoItem_DemandaCorrigida = "";
         n809ContagemResultadoItem_DemandaCorrigida = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A809ContagemResultadoItem_DemandaCorrigida", A809ContagemResultadoItem_DemandaCorrigida);
         n809ContagemResultadoItem_DemandaCorrigida = (String.IsNullOrEmpty(StringUtil.RTrim( A809ContagemResultadoItem_DemandaCorrigida)) ? true : false);
         A794ContagemResultadoItem_CFPS = "";
         n794ContagemResultadoItem_CFPS = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A794ContagemResultadoItem_CFPS", A794ContagemResultadoItem_CFPS);
         n794ContagemResultadoItem_CFPS = (String.IsNullOrEmpty(StringUtil.RTrim( A794ContagemResultadoItem_CFPS)) ? true : false);
         A792ContagemResultadoItem_DataCnt = DateTime.MinValue;
         n792ContagemResultadoItem_DataCnt = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A792ContagemResultadoItem_DataCnt", context.localUtil.Format(A792ContagemResultadoItem_DataCnt, "99/99/99"));
         n792ContagemResultadoItem_DataCnt = ((DateTime.MinValue==A792ContagemResultadoItem_DataCnt) ? true : false);
         A796ContagemResultadoItem_PFBTot = 0;
         n796ContagemResultadoItem_PFBTot = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A796ContagemResultadoItem_PFBTot", StringUtil.LTrim( StringUtil.Str( A796ContagemResultadoItem_PFBTot, 14, 5)));
         n796ContagemResultadoItem_PFBTot = ((Convert.ToDecimal(0)==A796ContagemResultadoItem_PFBTot) ? true : false);
         A797ContagemResultadoItem_PFLTot = 0;
         n797ContagemResultadoItem_PFLTot = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A797ContagemResultadoItem_PFLTot", StringUtil.LTrim( StringUtil.Str( A797ContagemResultadoItem_PFLTot, 14, 5)));
         n797ContagemResultadoItem_PFLTot = ((Convert.ToDecimal(0)==A797ContagemResultadoItem_PFLTot) ? true : false);
         A774ContagemResultadoItem_Descricao = "";
         n774ContagemResultadoItem_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A774ContagemResultadoItem_Descricao", A774ContagemResultadoItem_Descricao);
         n774ContagemResultadoItem_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A774ContagemResultadoItem_Descricao)) ? true : false);
         A775ContagemResultadoItem_Tipo = "";
         n775ContagemResultadoItem_Tipo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A775ContagemResultadoItem_Tipo", A775ContagemResultadoItem_Tipo);
         n775ContagemResultadoItem_Tipo = (String.IsNullOrEmpty(StringUtil.RTrim( A775ContagemResultadoItem_Tipo)) ? true : false);
         A776ContagemResultadoItem_Deflator = "";
         n776ContagemResultadoItem_Deflator = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A776ContagemResultadoItem_Deflator", A776ContagemResultadoItem_Deflator);
         n776ContagemResultadoItem_Deflator = (String.IsNullOrEmpty(StringUtil.RTrim( A776ContagemResultadoItem_Deflator)) ? true : false);
         A777ContagemResultadoItem_DER = 0;
         n777ContagemResultadoItem_DER = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A777ContagemResultadoItem_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A777ContagemResultadoItem_DER), 4, 0)));
         n777ContagemResultadoItem_DER = ((0==A777ContagemResultadoItem_DER) ? true : false);
         A778ContagemResultadoItem_RA = 0;
         n778ContagemResultadoItem_RA = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A778ContagemResultadoItem_RA", StringUtil.LTrim( StringUtil.Str( (decimal)(A778ContagemResultadoItem_RA), 4, 0)));
         n778ContagemResultadoItem_RA = ((0==A778ContagemResultadoItem_RA) ? true : false);
         A779ContagemResultadoItem_CP = "";
         n779ContagemResultadoItem_CP = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A779ContagemResultadoItem_CP", A779ContagemResultadoItem_CP);
         n779ContagemResultadoItem_CP = (String.IsNullOrEmpty(StringUtil.RTrim( A779ContagemResultadoItem_CP)) ? true : false);
         A780ContagemResultadoItem_PFB = 0;
         n780ContagemResultadoItem_PFB = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A780ContagemResultadoItem_PFB", StringUtil.LTrim( StringUtil.Str( A780ContagemResultadoItem_PFB, 14, 5)));
         n780ContagemResultadoItem_PFB = ((Convert.ToDecimal(0)==A780ContagemResultadoItem_PFB) ? true : false);
         A781ContagemResultadoItem_PFL = 0;
         n781ContagemResultadoItem_PFL = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A781ContagemResultadoItem_PFL", StringUtil.LTrim( StringUtil.Str( A781ContagemResultadoItem_PFL, 14, 5)));
         n781ContagemResultadoItem_PFL = ((Convert.ToDecimal(0)==A781ContagemResultadoItem_PFL) ? true : false);
         A782ContagemResultadoItem_Evidencia = "";
         n782ContagemResultadoItem_Evidencia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A782ContagemResultadoItem_Evidencia", A782ContagemResultadoItem_Evidencia);
         n782ContagemResultadoItem_Evidencia = (String.IsNullOrEmpty(StringUtil.RTrim( A782ContagemResultadoItem_Evidencia)) ? true : false);
         A789ContagemResultadoItem_ValidadoFM = false;
         n789ContagemResultadoItem_ValidadoFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A789ContagemResultadoItem_ValidadoFM", A789ContagemResultadoItem_ValidadoFM);
         n789ContagemResultadoItem_ValidadoFM = ((false==A789ContagemResultadoItem_ValidadoFM) ? true : false);
         A790ContagemResultadoItem_TipoFM = "";
         n790ContagemResultadoItem_TipoFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A790ContagemResultadoItem_TipoFM", A790ContagemResultadoItem_TipoFM);
         n790ContagemResultadoItem_TipoFM = (String.IsNullOrEmpty(StringUtil.RTrim( A790ContagemResultadoItem_TipoFM)) ? true : false);
         A786ContagemResultadoItem_DeflatorFM = "";
         n786ContagemResultadoItem_DeflatorFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A786ContagemResultadoItem_DeflatorFM", A786ContagemResultadoItem_DeflatorFM);
         n786ContagemResultadoItem_DeflatorFM = (String.IsNullOrEmpty(StringUtil.RTrim( A786ContagemResultadoItem_DeflatorFM)) ? true : false);
         A787ContagemResultadoItem_DERFM = 0;
         n787ContagemResultadoItem_DERFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A787ContagemResultadoItem_DERFM", StringUtil.LTrim( StringUtil.Str( (decimal)(A787ContagemResultadoItem_DERFM), 4, 0)));
         n787ContagemResultadoItem_DERFM = ((0==A787ContagemResultadoItem_DERFM) ? true : false);
         A791ContagemResultadoItem_RAFM = 0;
         n791ContagemResultadoItem_RAFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A791ContagemResultadoItem_RAFM", StringUtil.LTrim( StringUtil.Str( (decimal)(A791ContagemResultadoItem_RAFM), 4, 0)));
         n791ContagemResultadoItem_RAFM = ((0==A791ContagemResultadoItem_RAFM) ? true : false);
         A795ContagemResultadoItem_CPFM = "";
         n795ContagemResultadoItem_CPFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A795ContagemResultadoItem_CPFM", A795ContagemResultadoItem_CPFM);
         n795ContagemResultadoItem_CPFM = (String.IsNullOrEmpty(StringUtil.RTrim( A795ContagemResultadoItem_CPFM)) ? true : false);
         A784ContagemResultadoItem_PFBFM = 0;
         n784ContagemResultadoItem_PFBFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A784ContagemResultadoItem_PFBFM", StringUtil.LTrim( StringUtil.Str( A784ContagemResultadoItem_PFBFM, 14, 5)));
         n784ContagemResultadoItem_PFBFM = ((Convert.ToDecimal(0)==A784ContagemResultadoItem_PFBFM) ? true : false);
         A785ContagemResultadoItem_PFLFM = 0;
         n785ContagemResultadoItem_PFLFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A785ContagemResultadoItem_PFLFM", StringUtil.LTrim( StringUtil.Str( A785ContagemResultadoItem_PFLFM, 14, 5)));
         n785ContagemResultadoItem_PFLFM = ((Convert.ToDecimal(0)==A785ContagemResultadoItem_PFLFM) ? true : false);
         Z773ContagemResultadoItem_ContagemCod = 0;
         Z793ContagemResultadoItem_Demanda = "";
         Z809ContagemResultadoItem_DemandaCorrigida = "";
         Z794ContagemResultadoItem_CFPS = "";
         Z792ContagemResultadoItem_DataCnt = DateTime.MinValue;
         Z796ContagemResultadoItem_PFBTot = 0;
         Z797ContagemResultadoItem_PFLTot = 0;
         Z775ContagemResultadoItem_Tipo = "";
         Z776ContagemResultadoItem_Deflator = "";
         Z777ContagemResultadoItem_DER = 0;
         Z778ContagemResultadoItem_RA = 0;
         Z779ContagemResultadoItem_CP = "";
         Z780ContagemResultadoItem_PFB = 0;
         Z781ContagemResultadoItem_PFL = 0;
         Z789ContagemResultadoItem_ValidadoFM = false;
         Z790ContagemResultadoItem_TipoFM = "";
         Z786ContagemResultadoItem_DeflatorFM = "";
         Z787ContagemResultadoItem_DERFM = 0;
         Z791ContagemResultadoItem_RAFM = 0;
         Z795ContagemResultadoItem_CPFM = "";
         Z784ContagemResultadoItem_PFBFM = 0;
         Z785ContagemResultadoItem_PFLFM = 0;
      }

      protected void InitAll2K100( )
      {
         A772ContagemResultadoItem_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A772ContagemResultadoItem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A772ContagemResultadoItem_Codigo), 6, 0)));
         InitializeNonKey2K100( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117231997");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemresultadoitem.js", "?20203117231998");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockcontagemresultadoitem_codigo_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_CODIGO";
         edtContagemResultadoItem_Codigo_Internalname = "CONTAGEMRESULTADOITEM_CODIGO";
         lblTextblockcontagemresultadoitem_contagemcod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_CONTAGEMCOD";
         edtContagemResultadoItem_ContagemCod_Internalname = "CONTAGEMRESULTADOITEM_CONTAGEMCOD";
         lblTextblockcontagemresultadoitem_demanda_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_DEMANDA";
         edtContagemResultadoItem_Demanda_Internalname = "CONTAGEMRESULTADOITEM_DEMANDA";
         lblTextblockcontagemresultadoitem_demandacorrigida_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_DEMANDACORRIGIDA";
         edtContagemResultadoItem_DemandaCorrigida_Internalname = "CONTAGEMRESULTADOITEM_DEMANDACORRIGIDA";
         lblTextblockcontagemresultadoitem_cfps_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_CFPS";
         edtContagemResultadoItem_CFPS_Internalname = "CONTAGEMRESULTADOITEM_CFPS";
         lblTextblockcontagemresultadoitem_datacnt_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_DATACNT";
         edtContagemResultadoItem_DataCnt_Internalname = "CONTAGEMRESULTADOITEM_DATACNT";
         lblTextblockcontagemresultadoitem_pfbtot_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_PFBTOT";
         edtContagemResultadoItem_PFBTot_Internalname = "CONTAGEMRESULTADOITEM_PFBTOT";
         lblTextblockcontagemresultadoitem_pfltot_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_PFLTOT";
         edtContagemResultadoItem_PFLTot_Internalname = "CONTAGEMRESULTADOITEM_PFLTOT";
         lblTextblockcontagemresultadoitem_descricao_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_DESCRICAO";
         edtContagemResultadoItem_Descricao_Internalname = "CONTAGEMRESULTADOITEM_DESCRICAO";
         lblTextblockcontagemresultadoitem_tipo_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_TIPO";
         edtContagemResultadoItem_Tipo_Internalname = "CONTAGEMRESULTADOITEM_TIPO";
         lblTextblockcontagemresultadoitem_deflator_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_DEFLATOR";
         edtContagemResultadoItem_Deflator_Internalname = "CONTAGEMRESULTADOITEM_DEFLATOR";
         lblTextblockcontagemresultadoitem_der_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_DER";
         edtContagemResultadoItem_DER_Internalname = "CONTAGEMRESULTADOITEM_DER";
         lblTextblockcontagemresultadoitem_ra_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_RA";
         edtContagemResultadoItem_RA_Internalname = "CONTAGEMRESULTADOITEM_RA";
         lblTextblockcontagemresultadoitem_cp_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_CP";
         edtContagemResultadoItem_CP_Internalname = "CONTAGEMRESULTADOITEM_CP";
         lblTextblockcontagemresultadoitem_pfb_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_PFB";
         edtContagemResultadoItem_PFB_Internalname = "CONTAGEMRESULTADOITEM_PFB";
         lblTextblockcontagemresultadoitem_pfl_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_PFL";
         edtContagemResultadoItem_PFL_Internalname = "CONTAGEMRESULTADOITEM_PFL";
         lblTextblockcontagemresultadoitem_evidencia_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_EVIDENCIA";
         edtContagemResultadoItem_Evidencia_Internalname = "CONTAGEMRESULTADOITEM_EVIDENCIA";
         lblTextblockcontagemresultadoitem_validadofm_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_VALIDADOFM";
         chkContagemResultadoItem_ValidadoFM_Internalname = "CONTAGEMRESULTADOITEM_VALIDADOFM";
         lblTextblockcontagemresultadoitem_tipofm_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_TIPOFM";
         edtContagemResultadoItem_TipoFM_Internalname = "CONTAGEMRESULTADOITEM_TIPOFM";
         lblTextblockcontagemresultadoitem_deflatorfm_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_DEFLATORFM";
         edtContagemResultadoItem_DeflatorFM_Internalname = "CONTAGEMRESULTADOITEM_DEFLATORFM";
         lblTextblockcontagemresultadoitem_derfm_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_DERFM";
         edtContagemResultadoItem_DERFM_Internalname = "CONTAGEMRESULTADOITEM_DERFM";
         lblTextblockcontagemresultadoitem_rafm_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_RAFM";
         edtContagemResultadoItem_RAFM_Internalname = "CONTAGEMRESULTADOITEM_RAFM";
         lblTextblockcontagemresultadoitem_cpfm_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_CPFM";
         edtContagemResultadoItem_CPFM_Internalname = "CONTAGEMRESULTADOITEM_CPFM";
         lblTextblockcontagemresultadoitem_pfbfm_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_PFBFM";
         edtContagemResultadoItem_PFBFM_Internalname = "CONTAGEMRESULTADOITEM_PFBFM";
         lblTextblockcontagemresultadoitem_pflfm_Internalname = "TEXTBLOCKCONTAGEMRESULTADOITEM_PFLFM";
         edtContagemResultadoItem_PFLFM_Internalname = "CONTAGEMRESULTADOITEM_PFLFM";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem Resultado Item";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtContagemResultadoItem_PFLFM_Jsonclick = "";
         edtContagemResultadoItem_PFLFM_Enabled = 1;
         edtContagemResultadoItem_PFBFM_Jsonclick = "";
         edtContagemResultadoItem_PFBFM_Enabled = 1;
         edtContagemResultadoItem_CPFM_Jsonclick = "";
         edtContagemResultadoItem_CPFM_Enabled = 1;
         edtContagemResultadoItem_RAFM_Jsonclick = "";
         edtContagemResultadoItem_RAFM_Enabled = 1;
         edtContagemResultadoItem_DERFM_Jsonclick = "";
         edtContagemResultadoItem_DERFM_Enabled = 1;
         edtContagemResultadoItem_DeflatorFM_Jsonclick = "";
         edtContagemResultadoItem_DeflatorFM_Enabled = 1;
         edtContagemResultadoItem_TipoFM_Jsonclick = "";
         edtContagemResultadoItem_TipoFM_Enabled = 1;
         chkContagemResultadoItem_ValidadoFM.Enabled = 1;
         edtContagemResultadoItem_Evidencia_Enabled = 1;
         edtContagemResultadoItem_PFL_Jsonclick = "";
         edtContagemResultadoItem_PFL_Enabled = 1;
         edtContagemResultadoItem_PFB_Jsonclick = "";
         edtContagemResultadoItem_PFB_Enabled = 1;
         edtContagemResultadoItem_CP_Jsonclick = "";
         edtContagemResultadoItem_CP_Enabled = 1;
         edtContagemResultadoItem_RA_Jsonclick = "";
         edtContagemResultadoItem_RA_Enabled = 1;
         edtContagemResultadoItem_DER_Jsonclick = "";
         edtContagemResultadoItem_DER_Enabled = 1;
         edtContagemResultadoItem_Deflator_Jsonclick = "";
         edtContagemResultadoItem_Deflator_Enabled = 1;
         edtContagemResultadoItem_Tipo_Jsonclick = "";
         edtContagemResultadoItem_Tipo_Enabled = 1;
         edtContagemResultadoItem_Descricao_Enabled = 1;
         edtContagemResultadoItem_PFLTot_Jsonclick = "";
         edtContagemResultadoItem_PFLTot_Enabled = 1;
         edtContagemResultadoItem_PFBTot_Jsonclick = "";
         edtContagemResultadoItem_PFBTot_Enabled = 1;
         edtContagemResultadoItem_DataCnt_Jsonclick = "";
         edtContagemResultadoItem_DataCnt_Enabled = 1;
         edtContagemResultadoItem_CFPS_Jsonclick = "";
         edtContagemResultadoItem_CFPS_Enabled = 1;
         edtContagemResultadoItem_DemandaCorrigida_Jsonclick = "";
         edtContagemResultadoItem_DemandaCorrigida_Enabled = 1;
         edtContagemResultadoItem_Demanda_Jsonclick = "";
         edtContagemResultadoItem_Demanda_Enabled = 1;
         edtContagemResultadoItem_ContagemCod_Jsonclick = "";
         edtContagemResultadoItem_ContagemCod_Enabled = 1;
         edtContagemResultadoItem_Codigo_Jsonclick = "";
         edtContagemResultadoItem_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         chkContagemResultadoItem_ValidadoFM.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtContagemResultadoItem_ContagemCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contagemresultadoitem_codigo( int GX_Parm1 ,
                                                      int GX_Parm2 ,
                                                      String GX_Parm3 ,
                                                      String GX_Parm4 ,
                                                      String GX_Parm5 ,
                                                      DateTime GX_Parm6 ,
                                                      decimal GX_Parm7 ,
                                                      decimal GX_Parm8 ,
                                                      String GX_Parm9 ,
                                                      String GX_Parm10 ,
                                                      String GX_Parm11 ,
                                                      short GX_Parm12 ,
                                                      short GX_Parm13 ,
                                                      String GX_Parm14 ,
                                                      decimal GX_Parm15 ,
                                                      decimal GX_Parm16 ,
                                                      String GX_Parm17 ,
                                                      bool GX_Parm18 ,
                                                      String GX_Parm19 ,
                                                      String GX_Parm20 ,
                                                      short GX_Parm21 ,
                                                      short GX_Parm22 ,
                                                      String GX_Parm23 ,
                                                      decimal GX_Parm24 ,
                                                      decimal GX_Parm25 )
      {
         A772ContagemResultadoItem_Codigo = GX_Parm1;
         A773ContagemResultadoItem_ContagemCod = GX_Parm2;
         n773ContagemResultadoItem_ContagemCod = false;
         A793ContagemResultadoItem_Demanda = GX_Parm3;
         A809ContagemResultadoItem_DemandaCorrigida = GX_Parm4;
         n809ContagemResultadoItem_DemandaCorrigida = false;
         A794ContagemResultadoItem_CFPS = GX_Parm5;
         n794ContagemResultadoItem_CFPS = false;
         A792ContagemResultadoItem_DataCnt = GX_Parm6;
         n792ContagemResultadoItem_DataCnt = false;
         A796ContagemResultadoItem_PFBTot = GX_Parm7;
         n796ContagemResultadoItem_PFBTot = false;
         A797ContagemResultadoItem_PFLTot = GX_Parm8;
         n797ContagemResultadoItem_PFLTot = false;
         A774ContagemResultadoItem_Descricao = GX_Parm9;
         n774ContagemResultadoItem_Descricao = false;
         A775ContagemResultadoItem_Tipo = GX_Parm10;
         n775ContagemResultadoItem_Tipo = false;
         A776ContagemResultadoItem_Deflator = GX_Parm11;
         n776ContagemResultadoItem_Deflator = false;
         A777ContagemResultadoItem_DER = GX_Parm12;
         n777ContagemResultadoItem_DER = false;
         A778ContagemResultadoItem_RA = GX_Parm13;
         n778ContagemResultadoItem_RA = false;
         A779ContagemResultadoItem_CP = GX_Parm14;
         n779ContagemResultadoItem_CP = false;
         A780ContagemResultadoItem_PFB = GX_Parm15;
         n780ContagemResultadoItem_PFB = false;
         A781ContagemResultadoItem_PFL = GX_Parm16;
         n781ContagemResultadoItem_PFL = false;
         A782ContagemResultadoItem_Evidencia = GX_Parm17;
         n782ContagemResultadoItem_Evidencia = false;
         A789ContagemResultadoItem_ValidadoFM = GX_Parm18;
         n789ContagemResultadoItem_ValidadoFM = false;
         A790ContagemResultadoItem_TipoFM = GX_Parm19;
         n790ContagemResultadoItem_TipoFM = false;
         A786ContagemResultadoItem_DeflatorFM = GX_Parm20;
         n786ContagemResultadoItem_DeflatorFM = false;
         A787ContagemResultadoItem_DERFM = GX_Parm21;
         n787ContagemResultadoItem_DERFM = false;
         A791ContagemResultadoItem_RAFM = GX_Parm22;
         n791ContagemResultadoItem_RAFM = false;
         A795ContagemResultadoItem_CPFM = GX_Parm23;
         n795ContagemResultadoItem_CPFM = false;
         A784ContagemResultadoItem_PFBFM = GX_Parm24;
         n784ContagemResultadoItem_PFBFM = false;
         A785ContagemResultadoItem_PFLFM = GX_Parm25;
         n785ContagemResultadoItem_PFLFM = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A773ContagemResultadoItem_ContagemCod), 6, 0, ".", "")));
         isValidOutput.Add(A793ContagemResultadoItem_Demanda);
         isValidOutput.Add(A809ContagemResultadoItem_DemandaCorrigida);
         isValidOutput.Add(A794ContagemResultadoItem_CFPS);
         isValidOutput.Add(context.localUtil.Format(A792ContagemResultadoItem_DataCnt, "99/99/99"));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A796ContagemResultadoItem_PFBTot, 14, 5, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A797ContagemResultadoItem_PFLTot, 14, 5, ".", "")));
         isValidOutput.Add(A774ContagemResultadoItem_Descricao);
         isValidOutput.Add(A775ContagemResultadoItem_Tipo);
         isValidOutput.Add(A776ContagemResultadoItem_Deflator);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A777ContagemResultadoItem_DER), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A778ContagemResultadoItem_RA), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A779ContagemResultadoItem_CP));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A780ContagemResultadoItem_PFB, 14, 5, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A781ContagemResultadoItem_PFL, 14, 5, ".", "")));
         isValidOutput.Add(A782ContagemResultadoItem_Evidencia);
         isValidOutput.Add(A789ContagemResultadoItem_ValidadoFM);
         isValidOutput.Add(StringUtil.RTrim( A790ContagemResultadoItem_TipoFM));
         isValidOutput.Add(A786ContagemResultadoItem_DeflatorFM);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A787ContagemResultadoItem_DERFM), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A791ContagemResultadoItem_RAFM), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A795ContagemResultadoItem_CPFM));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A784ContagemResultadoItem_PFBFM, 14, 5, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A785ContagemResultadoItem_PFLFM, 14, 5, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z772ContagemResultadoItem_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z773ContagemResultadoItem_ContagemCod), 6, 0, ",", "")));
         isValidOutput.Add(Z793ContagemResultadoItem_Demanda);
         isValidOutput.Add(Z809ContagemResultadoItem_DemandaCorrigida);
         isValidOutput.Add(Z794ContagemResultadoItem_CFPS);
         isValidOutput.Add(context.localUtil.DToC( Z792ContagemResultadoItem_DataCnt, 0, "/"));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z796ContagemResultadoItem_PFBTot, 14, 5, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z797ContagemResultadoItem_PFLTot, 14, 5, ",", "")));
         isValidOutput.Add(Z774ContagemResultadoItem_Descricao);
         isValidOutput.Add(Z775ContagemResultadoItem_Tipo);
         isValidOutput.Add(Z776ContagemResultadoItem_Deflator);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z777ContagemResultadoItem_DER), 4, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z778ContagemResultadoItem_RA), 4, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z779ContagemResultadoItem_CP));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z780ContagemResultadoItem_PFB, 14, 5, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z781ContagemResultadoItem_PFL, 14, 5, ",", "")));
         isValidOutput.Add(Z782ContagemResultadoItem_Evidencia);
         isValidOutput.Add(Z789ContagemResultadoItem_ValidadoFM);
         isValidOutput.Add(StringUtil.RTrim( Z790ContagemResultadoItem_TipoFM));
         isValidOutput.Add(Z786ContagemResultadoItem_DeflatorFM);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z787ContagemResultadoItem_DERFM), 4, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z791ContagemResultadoItem_RAFM), 4, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z795ContagemResultadoItem_CPFM));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z784ContagemResultadoItem_PFBFM, 14, 5, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z785ContagemResultadoItem_PFLFM, 14, 5, ",", "")));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z793ContagemResultadoItem_Demanda = "";
         Z809ContagemResultadoItem_DemandaCorrigida = "";
         Z794ContagemResultadoItem_CFPS = "";
         Z792ContagemResultadoItem_DataCnt = DateTime.MinValue;
         Z775ContagemResultadoItem_Tipo = "";
         Z776ContagemResultadoItem_Deflator = "";
         Z779ContagemResultadoItem_CP = "";
         Z790ContagemResultadoItem_TipoFM = "";
         Z786ContagemResultadoItem_DeflatorFM = "";
         Z795ContagemResultadoItem_CPFM = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockcontagemresultadoitem_codigo_Jsonclick = "";
         lblTextblockcontagemresultadoitem_contagemcod_Jsonclick = "";
         lblTextblockcontagemresultadoitem_demanda_Jsonclick = "";
         A793ContagemResultadoItem_Demanda = "";
         lblTextblockcontagemresultadoitem_demandacorrigida_Jsonclick = "";
         A809ContagemResultadoItem_DemandaCorrigida = "";
         lblTextblockcontagemresultadoitem_cfps_Jsonclick = "";
         A794ContagemResultadoItem_CFPS = "";
         lblTextblockcontagemresultadoitem_datacnt_Jsonclick = "";
         A792ContagemResultadoItem_DataCnt = DateTime.MinValue;
         lblTextblockcontagemresultadoitem_pfbtot_Jsonclick = "";
         lblTextblockcontagemresultadoitem_pfltot_Jsonclick = "";
         lblTextblockcontagemresultadoitem_descricao_Jsonclick = "";
         A774ContagemResultadoItem_Descricao = "";
         lblTextblockcontagemresultadoitem_tipo_Jsonclick = "";
         A775ContagemResultadoItem_Tipo = "";
         lblTextblockcontagemresultadoitem_deflator_Jsonclick = "";
         A776ContagemResultadoItem_Deflator = "";
         lblTextblockcontagemresultadoitem_der_Jsonclick = "";
         lblTextblockcontagemresultadoitem_ra_Jsonclick = "";
         lblTextblockcontagemresultadoitem_cp_Jsonclick = "";
         A779ContagemResultadoItem_CP = "";
         lblTextblockcontagemresultadoitem_pfb_Jsonclick = "";
         lblTextblockcontagemresultadoitem_pfl_Jsonclick = "";
         lblTextblockcontagemresultadoitem_evidencia_Jsonclick = "";
         A782ContagemResultadoItem_Evidencia = "";
         lblTextblockcontagemresultadoitem_validadofm_Jsonclick = "";
         lblTextblockcontagemresultadoitem_tipofm_Jsonclick = "";
         A790ContagemResultadoItem_TipoFM = "";
         lblTextblockcontagemresultadoitem_deflatorfm_Jsonclick = "";
         A786ContagemResultadoItem_DeflatorFM = "";
         lblTextblockcontagemresultadoitem_derfm_Jsonclick = "";
         lblTextblockcontagemresultadoitem_rafm_Jsonclick = "";
         lblTextblockcontagemresultadoitem_cpfm_Jsonclick = "";
         A795ContagemResultadoItem_CPFM = "";
         lblTextblockcontagemresultadoitem_pfbfm_Jsonclick = "";
         lblTextblockcontagemresultadoitem_pflfm_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z774ContagemResultadoItem_Descricao = "";
         Z782ContagemResultadoItem_Evidencia = "";
         T002K4_A772ContagemResultadoItem_Codigo = new int[1] ;
         T002K4_A773ContagemResultadoItem_ContagemCod = new int[1] ;
         T002K4_n773ContagemResultadoItem_ContagemCod = new bool[] {false} ;
         T002K4_A793ContagemResultadoItem_Demanda = new String[] {""} ;
         T002K4_A809ContagemResultadoItem_DemandaCorrigida = new String[] {""} ;
         T002K4_n809ContagemResultadoItem_DemandaCorrigida = new bool[] {false} ;
         T002K4_A794ContagemResultadoItem_CFPS = new String[] {""} ;
         T002K4_n794ContagemResultadoItem_CFPS = new bool[] {false} ;
         T002K4_A792ContagemResultadoItem_DataCnt = new DateTime[] {DateTime.MinValue} ;
         T002K4_n792ContagemResultadoItem_DataCnt = new bool[] {false} ;
         T002K4_A796ContagemResultadoItem_PFBTot = new decimal[1] ;
         T002K4_n796ContagemResultadoItem_PFBTot = new bool[] {false} ;
         T002K4_A797ContagemResultadoItem_PFLTot = new decimal[1] ;
         T002K4_n797ContagemResultadoItem_PFLTot = new bool[] {false} ;
         T002K4_A774ContagemResultadoItem_Descricao = new String[] {""} ;
         T002K4_n774ContagemResultadoItem_Descricao = new bool[] {false} ;
         T002K4_A775ContagemResultadoItem_Tipo = new String[] {""} ;
         T002K4_n775ContagemResultadoItem_Tipo = new bool[] {false} ;
         T002K4_A776ContagemResultadoItem_Deflator = new String[] {""} ;
         T002K4_n776ContagemResultadoItem_Deflator = new bool[] {false} ;
         T002K4_A777ContagemResultadoItem_DER = new short[1] ;
         T002K4_n777ContagemResultadoItem_DER = new bool[] {false} ;
         T002K4_A778ContagemResultadoItem_RA = new short[1] ;
         T002K4_n778ContagemResultadoItem_RA = new bool[] {false} ;
         T002K4_A779ContagemResultadoItem_CP = new String[] {""} ;
         T002K4_n779ContagemResultadoItem_CP = new bool[] {false} ;
         T002K4_A780ContagemResultadoItem_PFB = new decimal[1] ;
         T002K4_n780ContagemResultadoItem_PFB = new bool[] {false} ;
         T002K4_A781ContagemResultadoItem_PFL = new decimal[1] ;
         T002K4_n781ContagemResultadoItem_PFL = new bool[] {false} ;
         T002K4_A782ContagemResultadoItem_Evidencia = new String[] {""} ;
         T002K4_n782ContagemResultadoItem_Evidencia = new bool[] {false} ;
         T002K4_A789ContagemResultadoItem_ValidadoFM = new bool[] {false} ;
         T002K4_n789ContagemResultadoItem_ValidadoFM = new bool[] {false} ;
         T002K4_A790ContagemResultadoItem_TipoFM = new String[] {""} ;
         T002K4_n790ContagemResultadoItem_TipoFM = new bool[] {false} ;
         T002K4_A786ContagemResultadoItem_DeflatorFM = new String[] {""} ;
         T002K4_n786ContagemResultadoItem_DeflatorFM = new bool[] {false} ;
         T002K4_A787ContagemResultadoItem_DERFM = new short[1] ;
         T002K4_n787ContagemResultadoItem_DERFM = new bool[] {false} ;
         T002K4_A791ContagemResultadoItem_RAFM = new short[1] ;
         T002K4_n791ContagemResultadoItem_RAFM = new bool[] {false} ;
         T002K4_A795ContagemResultadoItem_CPFM = new String[] {""} ;
         T002K4_n795ContagemResultadoItem_CPFM = new bool[] {false} ;
         T002K4_A784ContagemResultadoItem_PFBFM = new decimal[1] ;
         T002K4_n784ContagemResultadoItem_PFBFM = new bool[] {false} ;
         T002K4_A785ContagemResultadoItem_PFLFM = new decimal[1] ;
         T002K4_n785ContagemResultadoItem_PFLFM = new bool[] {false} ;
         T002K5_A772ContagemResultadoItem_Codigo = new int[1] ;
         T002K3_A772ContagemResultadoItem_Codigo = new int[1] ;
         T002K3_A773ContagemResultadoItem_ContagemCod = new int[1] ;
         T002K3_n773ContagemResultadoItem_ContagemCod = new bool[] {false} ;
         T002K3_A793ContagemResultadoItem_Demanda = new String[] {""} ;
         T002K3_A809ContagemResultadoItem_DemandaCorrigida = new String[] {""} ;
         T002K3_n809ContagemResultadoItem_DemandaCorrigida = new bool[] {false} ;
         T002K3_A794ContagemResultadoItem_CFPS = new String[] {""} ;
         T002K3_n794ContagemResultadoItem_CFPS = new bool[] {false} ;
         T002K3_A792ContagemResultadoItem_DataCnt = new DateTime[] {DateTime.MinValue} ;
         T002K3_n792ContagemResultadoItem_DataCnt = new bool[] {false} ;
         T002K3_A796ContagemResultadoItem_PFBTot = new decimal[1] ;
         T002K3_n796ContagemResultadoItem_PFBTot = new bool[] {false} ;
         T002K3_A797ContagemResultadoItem_PFLTot = new decimal[1] ;
         T002K3_n797ContagemResultadoItem_PFLTot = new bool[] {false} ;
         T002K3_A774ContagemResultadoItem_Descricao = new String[] {""} ;
         T002K3_n774ContagemResultadoItem_Descricao = new bool[] {false} ;
         T002K3_A775ContagemResultadoItem_Tipo = new String[] {""} ;
         T002K3_n775ContagemResultadoItem_Tipo = new bool[] {false} ;
         T002K3_A776ContagemResultadoItem_Deflator = new String[] {""} ;
         T002K3_n776ContagemResultadoItem_Deflator = new bool[] {false} ;
         T002K3_A777ContagemResultadoItem_DER = new short[1] ;
         T002K3_n777ContagemResultadoItem_DER = new bool[] {false} ;
         T002K3_A778ContagemResultadoItem_RA = new short[1] ;
         T002K3_n778ContagemResultadoItem_RA = new bool[] {false} ;
         T002K3_A779ContagemResultadoItem_CP = new String[] {""} ;
         T002K3_n779ContagemResultadoItem_CP = new bool[] {false} ;
         T002K3_A780ContagemResultadoItem_PFB = new decimal[1] ;
         T002K3_n780ContagemResultadoItem_PFB = new bool[] {false} ;
         T002K3_A781ContagemResultadoItem_PFL = new decimal[1] ;
         T002K3_n781ContagemResultadoItem_PFL = new bool[] {false} ;
         T002K3_A782ContagemResultadoItem_Evidencia = new String[] {""} ;
         T002K3_n782ContagemResultadoItem_Evidencia = new bool[] {false} ;
         T002K3_A789ContagemResultadoItem_ValidadoFM = new bool[] {false} ;
         T002K3_n789ContagemResultadoItem_ValidadoFM = new bool[] {false} ;
         T002K3_A790ContagemResultadoItem_TipoFM = new String[] {""} ;
         T002K3_n790ContagemResultadoItem_TipoFM = new bool[] {false} ;
         T002K3_A786ContagemResultadoItem_DeflatorFM = new String[] {""} ;
         T002K3_n786ContagemResultadoItem_DeflatorFM = new bool[] {false} ;
         T002K3_A787ContagemResultadoItem_DERFM = new short[1] ;
         T002K3_n787ContagemResultadoItem_DERFM = new bool[] {false} ;
         T002K3_A791ContagemResultadoItem_RAFM = new short[1] ;
         T002K3_n791ContagemResultadoItem_RAFM = new bool[] {false} ;
         T002K3_A795ContagemResultadoItem_CPFM = new String[] {""} ;
         T002K3_n795ContagemResultadoItem_CPFM = new bool[] {false} ;
         T002K3_A784ContagemResultadoItem_PFBFM = new decimal[1] ;
         T002K3_n784ContagemResultadoItem_PFBFM = new bool[] {false} ;
         T002K3_A785ContagemResultadoItem_PFLFM = new decimal[1] ;
         T002K3_n785ContagemResultadoItem_PFLFM = new bool[] {false} ;
         sMode100 = "";
         T002K6_A772ContagemResultadoItem_Codigo = new int[1] ;
         T002K7_A772ContagemResultadoItem_Codigo = new int[1] ;
         T002K2_A772ContagemResultadoItem_Codigo = new int[1] ;
         T002K2_A773ContagemResultadoItem_ContagemCod = new int[1] ;
         T002K2_n773ContagemResultadoItem_ContagemCod = new bool[] {false} ;
         T002K2_A793ContagemResultadoItem_Demanda = new String[] {""} ;
         T002K2_A809ContagemResultadoItem_DemandaCorrigida = new String[] {""} ;
         T002K2_n809ContagemResultadoItem_DemandaCorrigida = new bool[] {false} ;
         T002K2_A794ContagemResultadoItem_CFPS = new String[] {""} ;
         T002K2_n794ContagemResultadoItem_CFPS = new bool[] {false} ;
         T002K2_A792ContagemResultadoItem_DataCnt = new DateTime[] {DateTime.MinValue} ;
         T002K2_n792ContagemResultadoItem_DataCnt = new bool[] {false} ;
         T002K2_A796ContagemResultadoItem_PFBTot = new decimal[1] ;
         T002K2_n796ContagemResultadoItem_PFBTot = new bool[] {false} ;
         T002K2_A797ContagemResultadoItem_PFLTot = new decimal[1] ;
         T002K2_n797ContagemResultadoItem_PFLTot = new bool[] {false} ;
         T002K2_A774ContagemResultadoItem_Descricao = new String[] {""} ;
         T002K2_n774ContagemResultadoItem_Descricao = new bool[] {false} ;
         T002K2_A775ContagemResultadoItem_Tipo = new String[] {""} ;
         T002K2_n775ContagemResultadoItem_Tipo = new bool[] {false} ;
         T002K2_A776ContagemResultadoItem_Deflator = new String[] {""} ;
         T002K2_n776ContagemResultadoItem_Deflator = new bool[] {false} ;
         T002K2_A777ContagemResultadoItem_DER = new short[1] ;
         T002K2_n777ContagemResultadoItem_DER = new bool[] {false} ;
         T002K2_A778ContagemResultadoItem_RA = new short[1] ;
         T002K2_n778ContagemResultadoItem_RA = new bool[] {false} ;
         T002K2_A779ContagemResultadoItem_CP = new String[] {""} ;
         T002K2_n779ContagemResultadoItem_CP = new bool[] {false} ;
         T002K2_A780ContagemResultadoItem_PFB = new decimal[1] ;
         T002K2_n780ContagemResultadoItem_PFB = new bool[] {false} ;
         T002K2_A781ContagemResultadoItem_PFL = new decimal[1] ;
         T002K2_n781ContagemResultadoItem_PFL = new bool[] {false} ;
         T002K2_A782ContagemResultadoItem_Evidencia = new String[] {""} ;
         T002K2_n782ContagemResultadoItem_Evidencia = new bool[] {false} ;
         T002K2_A789ContagemResultadoItem_ValidadoFM = new bool[] {false} ;
         T002K2_n789ContagemResultadoItem_ValidadoFM = new bool[] {false} ;
         T002K2_A790ContagemResultadoItem_TipoFM = new String[] {""} ;
         T002K2_n790ContagemResultadoItem_TipoFM = new bool[] {false} ;
         T002K2_A786ContagemResultadoItem_DeflatorFM = new String[] {""} ;
         T002K2_n786ContagemResultadoItem_DeflatorFM = new bool[] {false} ;
         T002K2_A787ContagemResultadoItem_DERFM = new short[1] ;
         T002K2_n787ContagemResultadoItem_DERFM = new bool[] {false} ;
         T002K2_A791ContagemResultadoItem_RAFM = new short[1] ;
         T002K2_n791ContagemResultadoItem_RAFM = new bool[] {false} ;
         T002K2_A795ContagemResultadoItem_CPFM = new String[] {""} ;
         T002K2_n795ContagemResultadoItem_CPFM = new bool[] {false} ;
         T002K2_A784ContagemResultadoItem_PFBFM = new decimal[1] ;
         T002K2_n784ContagemResultadoItem_PFBFM = new bool[] {false} ;
         T002K2_A785ContagemResultadoItem_PFLFM = new decimal[1] ;
         T002K2_n785ContagemResultadoItem_PFLFM = new bool[] {false} ;
         T002K8_A772ContagemResultadoItem_Codigo = new int[1] ;
         T002K11_A772ContagemResultadoItem_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadoitem__default(),
            new Object[][] {
                new Object[] {
               T002K2_A772ContagemResultadoItem_Codigo, T002K2_A773ContagemResultadoItem_ContagemCod, T002K2_n773ContagemResultadoItem_ContagemCod, T002K2_A793ContagemResultadoItem_Demanda, T002K2_A809ContagemResultadoItem_DemandaCorrigida, T002K2_n809ContagemResultadoItem_DemandaCorrigida, T002K2_A794ContagemResultadoItem_CFPS, T002K2_n794ContagemResultadoItem_CFPS, T002K2_A792ContagemResultadoItem_DataCnt, T002K2_n792ContagemResultadoItem_DataCnt,
               T002K2_A796ContagemResultadoItem_PFBTot, T002K2_n796ContagemResultadoItem_PFBTot, T002K2_A797ContagemResultadoItem_PFLTot, T002K2_n797ContagemResultadoItem_PFLTot, T002K2_A774ContagemResultadoItem_Descricao, T002K2_n774ContagemResultadoItem_Descricao, T002K2_A775ContagemResultadoItem_Tipo, T002K2_n775ContagemResultadoItem_Tipo, T002K2_A776ContagemResultadoItem_Deflator, T002K2_n776ContagemResultadoItem_Deflator,
               T002K2_A777ContagemResultadoItem_DER, T002K2_n777ContagemResultadoItem_DER, T002K2_A778ContagemResultadoItem_RA, T002K2_n778ContagemResultadoItem_RA, T002K2_A779ContagemResultadoItem_CP, T002K2_n779ContagemResultadoItem_CP, T002K2_A780ContagemResultadoItem_PFB, T002K2_n780ContagemResultadoItem_PFB, T002K2_A781ContagemResultadoItem_PFL, T002K2_n781ContagemResultadoItem_PFL,
               T002K2_A782ContagemResultadoItem_Evidencia, T002K2_n782ContagemResultadoItem_Evidencia, T002K2_A789ContagemResultadoItem_ValidadoFM, T002K2_n789ContagemResultadoItem_ValidadoFM, T002K2_A790ContagemResultadoItem_TipoFM, T002K2_n790ContagemResultadoItem_TipoFM, T002K2_A786ContagemResultadoItem_DeflatorFM, T002K2_n786ContagemResultadoItem_DeflatorFM, T002K2_A787ContagemResultadoItem_DERFM, T002K2_n787ContagemResultadoItem_DERFM,
               T002K2_A791ContagemResultadoItem_RAFM, T002K2_n791ContagemResultadoItem_RAFM, T002K2_A795ContagemResultadoItem_CPFM, T002K2_n795ContagemResultadoItem_CPFM, T002K2_A784ContagemResultadoItem_PFBFM, T002K2_n784ContagemResultadoItem_PFBFM, T002K2_A785ContagemResultadoItem_PFLFM, T002K2_n785ContagemResultadoItem_PFLFM
               }
               , new Object[] {
               T002K3_A772ContagemResultadoItem_Codigo, T002K3_A773ContagemResultadoItem_ContagemCod, T002K3_n773ContagemResultadoItem_ContagemCod, T002K3_A793ContagemResultadoItem_Demanda, T002K3_A809ContagemResultadoItem_DemandaCorrigida, T002K3_n809ContagemResultadoItem_DemandaCorrigida, T002K3_A794ContagemResultadoItem_CFPS, T002K3_n794ContagemResultadoItem_CFPS, T002K3_A792ContagemResultadoItem_DataCnt, T002K3_n792ContagemResultadoItem_DataCnt,
               T002K3_A796ContagemResultadoItem_PFBTot, T002K3_n796ContagemResultadoItem_PFBTot, T002K3_A797ContagemResultadoItem_PFLTot, T002K3_n797ContagemResultadoItem_PFLTot, T002K3_A774ContagemResultadoItem_Descricao, T002K3_n774ContagemResultadoItem_Descricao, T002K3_A775ContagemResultadoItem_Tipo, T002K3_n775ContagemResultadoItem_Tipo, T002K3_A776ContagemResultadoItem_Deflator, T002K3_n776ContagemResultadoItem_Deflator,
               T002K3_A777ContagemResultadoItem_DER, T002K3_n777ContagemResultadoItem_DER, T002K3_A778ContagemResultadoItem_RA, T002K3_n778ContagemResultadoItem_RA, T002K3_A779ContagemResultadoItem_CP, T002K3_n779ContagemResultadoItem_CP, T002K3_A780ContagemResultadoItem_PFB, T002K3_n780ContagemResultadoItem_PFB, T002K3_A781ContagemResultadoItem_PFL, T002K3_n781ContagemResultadoItem_PFL,
               T002K3_A782ContagemResultadoItem_Evidencia, T002K3_n782ContagemResultadoItem_Evidencia, T002K3_A789ContagemResultadoItem_ValidadoFM, T002K3_n789ContagemResultadoItem_ValidadoFM, T002K3_A790ContagemResultadoItem_TipoFM, T002K3_n790ContagemResultadoItem_TipoFM, T002K3_A786ContagemResultadoItem_DeflatorFM, T002K3_n786ContagemResultadoItem_DeflatorFM, T002K3_A787ContagemResultadoItem_DERFM, T002K3_n787ContagemResultadoItem_DERFM,
               T002K3_A791ContagemResultadoItem_RAFM, T002K3_n791ContagemResultadoItem_RAFM, T002K3_A795ContagemResultadoItem_CPFM, T002K3_n795ContagemResultadoItem_CPFM, T002K3_A784ContagemResultadoItem_PFBFM, T002K3_n784ContagemResultadoItem_PFBFM, T002K3_A785ContagemResultadoItem_PFLFM, T002K3_n785ContagemResultadoItem_PFLFM
               }
               , new Object[] {
               T002K4_A772ContagemResultadoItem_Codigo, T002K4_A773ContagemResultadoItem_ContagemCod, T002K4_n773ContagemResultadoItem_ContagemCod, T002K4_A793ContagemResultadoItem_Demanda, T002K4_A809ContagemResultadoItem_DemandaCorrigida, T002K4_n809ContagemResultadoItem_DemandaCorrigida, T002K4_A794ContagemResultadoItem_CFPS, T002K4_n794ContagemResultadoItem_CFPS, T002K4_A792ContagemResultadoItem_DataCnt, T002K4_n792ContagemResultadoItem_DataCnt,
               T002K4_A796ContagemResultadoItem_PFBTot, T002K4_n796ContagemResultadoItem_PFBTot, T002K4_A797ContagemResultadoItem_PFLTot, T002K4_n797ContagemResultadoItem_PFLTot, T002K4_A774ContagemResultadoItem_Descricao, T002K4_n774ContagemResultadoItem_Descricao, T002K4_A775ContagemResultadoItem_Tipo, T002K4_n775ContagemResultadoItem_Tipo, T002K4_A776ContagemResultadoItem_Deflator, T002K4_n776ContagemResultadoItem_Deflator,
               T002K4_A777ContagemResultadoItem_DER, T002K4_n777ContagemResultadoItem_DER, T002K4_A778ContagemResultadoItem_RA, T002K4_n778ContagemResultadoItem_RA, T002K4_A779ContagemResultadoItem_CP, T002K4_n779ContagemResultadoItem_CP, T002K4_A780ContagemResultadoItem_PFB, T002K4_n780ContagemResultadoItem_PFB, T002K4_A781ContagemResultadoItem_PFL, T002K4_n781ContagemResultadoItem_PFL,
               T002K4_A782ContagemResultadoItem_Evidencia, T002K4_n782ContagemResultadoItem_Evidencia, T002K4_A789ContagemResultadoItem_ValidadoFM, T002K4_n789ContagemResultadoItem_ValidadoFM, T002K4_A790ContagemResultadoItem_TipoFM, T002K4_n790ContagemResultadoItem_TipoFM, T002K4_A786ContagemResultadoItem_DeflatorFM, T002K4_n786ContagemResultadoItem_DeflatorFM, T002K4_A787ContagemResultadoItem_DERFM, T002K4_n787ContagemResultadoItem_DERFM,
               T002K4_A791ContagemResultadoItem_RAFM, T002K4_n791ContagemResultadoItem_RAFM, T002K4_A795ContagemResultadoItem_CPFM, T002K4_n795ContagemResultadoItem_CPFM, T002K4_A784ContagemResultadoItem_PFBFM, T002K4_n784ContagemResultadoItem_PFBFM, T002K4_A785ContagemResultadoItem_PFLFM, T002K4_n785ContagemResultadoItem_PFLFM
               }
               , new Object[] {
               T002K5_A772ContagemResultadoItem_Codigo
               }
               , new Object[] {
               T002K6_A772ContagemResultadoItem_Codigo
               }
               , new Object[] {
               T002K7_A772ContagemResultadoItem_Codigo
               }
               , new Object[] {
               T002K8_A772ContagemResultadoItem_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002K11_A772ContagemResultadoItem_Codigo
               }
            }
         );
      }

      private short Z777ContagemResultadoItem_DER ;
      private short Z778ContagemResultadoItem_RA ;
      private short Z787ContagemResultadoItem_DERFM ;
      private short Z791ContagemResultadoItem_RAFM ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A777ContagemResultadoItem_DER ;
      private short A778ContagemResultadoItem_RA ;
      private short A787ContagemResultadoItem_DERFM ;
      private short A791ContagemResultadoItem_RAFM ;
      private short GX_JID ;
      private short RcdFound100 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z772ContagemResultadoItem_Codigo ;
      private int Z773ContagemResultadoItem_ContagemCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int A772ContagemResultadoItem_Codigo ;
      private int edtContagemResultadoItem_Codigo_Enabled ;
      private int A773ContagemResultadoItem_ContagemCod ;
      private int edtContagemResultadoItem_ContagemCod_Enabled ;
      private int edtContagemResultadoItem_Demanda_Enabled ;
      private int edtContagemResultadoItem_DemandaCorrigida_Enabled ;
      private int edtContagemResultadoItem_CFPS_Enabled ;
      private int edtContagemResultadoItem_DataCnt_Enabled ;
      private int edtContagemResultadoItem_PFBTot_Enabled ;
      private int edtContagemResultadoItem_PFLTot_Enabled ;
      private int edtContagemResultadoItem_Descricao_Enabled ;
      private int edtContagemResultadoItem_Tipo_Enabled ;
      private int edtContagemResultadoItem_Deflator_Enabled ;
      private int edtContagemResultadoItem_DER_Enabled ;
      private int edtContagemResultadoItem_RA_Enabled ;
      private int edtContagemResultadoItem_CP_Enabled ;
      private int edtContagemResultadoItem_PFB_Enabled ;
      private int edtContagemResultadoItem_PFL_Enabled ;
      private int edtContagemResultadoItem_Evidencia_Enabled ;
      private int edtContagemResultadoItem_TipoFM_Enabled ;
      private int edtContagemResultadoItem_DeflatorFM_Enabled ;
      private int edtContagemResultadoItem_DERFM_Enabled ;
      private int edtContagemResultadoItem_RAFM_Enabled ;
      private int edtContagemResultadoItem_CPFM_Enabled ;
      private int edtContagemResultadoItem_PFBFM_Enabled ;
      private int edtContagemResultadoItem_PFLFM_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private decimal Z796ContagemResultadoItem_PFBTot ;
      private decimal Z797ContagemResultadoItem_PFLTot ;
      private decimal Z780ContagemResultadoItem_PFB ;
      private decimal Z781ContagemResultadoItem_PFL ;
      private decimal Z784ContagemResultadoItem_PFBFM ;
      private decimal Z785ContagemResultadoItem_PFLFM ;
      private decimal A796ContagemResultadoItem_PFBTot ;
      private decimal A797ContagemResultadoItem_PFLTot ;
      private decimal A780ContagemResultadoItem_PFB ;
      private decimal A781ContagemResultadoItem_PFL ;
      private decimal A784ContagemResultadoItem_PFBFM ;
      private decimal A785ContagemResultadoItem_PFLFM ;
      private String sPrefix ;
      private String Z779ContagemResultadoItem_CP ;
      private String Z790ContagemResultadoItem_TipoFM ;
      private String Z795ContagemResultadoItem_CPFM ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String chkContagemResultadoItem_ValidadoFM_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemResultadoItem_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockcontagemresultadoitem_codigo_Internalname ;
      private String lblTextblockcontagemresultadoitem_codigo_Jsonclick ;
      private String edtContagemResultadoItem_Codigo_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_contagemcod_Internalname ;
      private String lblTextblockcontagemresultadoitem_contagemcod_Jsonclick ;
      private String edtContagemResultadoItem_ContagemCod_Internalname ;
      private String edtContagemResultadoItem_ContagemCod_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_demanda_Internalname ;
      private String lblTextblockcontagemresultadoitem_demanda_Jsonclick ;
      private String edtContagemResultadoItem_Demanda_Internalname ;
      private String edtContagemResultadoItem_Demanda_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_demandacorrigida_Internalname ;
      private String lblTextblockcontagemresultadoitem_demandacorrigida_Jsonclick ;
      private String edtContagemResultadoItem_DemandaCorrigida_Internalname ;
      private String edtContagemResultadoItem_DemandaCorrigida_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_cfps_Internalname ;
      private String lblTextblockcontagemresultadoitem_cfps_Jsonclick ;
      private String edtContagemResultadoItem_CFPS_Internalname ;
      private String edtContagemResultadoItem_CFPS_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_datacnt_Internalname ;
      private String lblTextblockcontagemresultadoitem_datacnt_Jsonclick ;
      private String edtContagemResultadoItem_DataCnt_Internalname ;
      private String edtContagemResultadoItem_DataCnt_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_pfbtot_Internalname ;
      private String lblTextblockcontagemresultadoitem_pfbtot_Jsonclick ;
      private String edtContagemResultadoItem_PFBTot_Internalname ;
      private String edtContagemResultadoItem_PFBTot_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_pfltot_Internalname ;
      private String lblTextblockcontagemresultadoitem_pfltot_Jsonclick ;
      private String edtContagemResultadoItem_PFLTot_Internalname ;
      private String edtContagemResultadoItem_PFLTot_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_descricao_Internalname ;
      private String lblTextblockcontagemresultadoitem_descricao_Jsonclick ;
      private String edtContagemResultadoItem_Descricao_Internalname ;
      private String lblTextblockcontagemresultadoitem_tipo_Internalname ;
      private String lblTextblockcontagemresultadoitem_tipo_Jsonclick ;
      private String edtContagemResultadoItem_Tipo_Internalname ;
      private String edtContagemResultadoItem_Tipo_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_deflator_Internalname ;
      private String lblTextblockcontagemresultadoitem_deflator_Jsonclick ;
      private String edtContagemResultadoItem_Deflator_Internalname ;
      private String edtContagemResultadoItem_Deflator_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_der_Internalname ;
      private String lblTextblockcontagemresultadoitem_der_Jsonclick ;
      private String edtContagemResultadoItem_DER_Internalname ;
      private String edtContagemResultadoItem_DER_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_ra_Internalname ;
      private String lblTextblockcontagemresultadoitem_ra_Jsonclick ;
      private String edtContagemResultadoItem_RA_Internalname ;
      private String edtContagemResultadoItem_RA_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_cp_Internalname ;
      private String lblTextblockcontagemresultadoitem_cp_Jsonclick ;
      private String edtContagemResultadoItem_CP_Internalname ;
      private String A779ContagemResultadoItem_CP ;
      private String edtContagemResultadoItem_CP_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_pfb_Internalname ;
      private String lblTextblockcontagemresultadoitem_pfb_Jsonclick ;
      private String edtContagemResultadoItem_PFB_Internalname ;
      private String edtContagemResultadoItem_PFB_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_pfl_Internalname ;
      private String lblTextblockcontagemresultadoitem_pfl_Jsonclick ;
      private String edtContagemResultadoItem_PFL_Internalname ;
      private String edtContagemResultadoItem_PFL_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_evidencia_Internalname ;
      private String lblTextblockcontagemresultadoitem_evidencia_Jsonclick ;
      private String edtContagemResultadoItem_Evidencia_Internalname ;
      private String lblTextblockcontagemresultadoitem_validadofm_Internalname ;
      private String lblTextblockcontagemresultadoitem_validadofm_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_tipofm_Internalname ;
      private String lblTextblockcontagemresultadoitem_tipofm_Jsonclick ;
      private String edtContagemResultadoItem_TipoFM_Internalname ;
      private String A790ContagemResultadoItem_TipoFM ;
      private String edtContagemResultadoItem_TipoFM_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_deflatorfm_Internalname ;
      private String lblTextblockcontagemresultadoitem_deflatorfm_Jsonclick ;
      private String edtContagemResultadoItem_DeflatorFM_Internalname ;
      private String edtContagemResultadoItem_DeflatorFM_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_derfm_Internalname ;
      private String lblTextblockcontagemresultadoitem_derfm_Jsonclick ;
      private String edtContagemResultadoItem_DERFM_Internalname ;
      private String edtContagemResultadoItem_DERFM_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_rafm_Internalname ;
      private String lblTextblockcontagemresultadoitem_rafm_Jsonclick ;
      private String edtContagemResultadoItem_RAFM_Internalname ;
      private String edtContagemResultadoItem_RAFM_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_cpfm_Internalname ;
      private String lblTextblockcontagemresultadoitem_cpfm_Jsonclick ;
      private String edtContagemResultadoItem_CPFM_Internalname ;
      private String A795ContagemResultadoItem_CPFM ;
      private String edtContagemResultadoItem_CPFM_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_pfbfm_Internalname ;
      private String lblTextblockcontagemresultadoitem_pfbfm_Jsonclick ;
      private String edtContagemResultadoItem_PFBFM_Internalname ;
      private String edtContagemResultadoItem_PFBFM_Jsonclick ;
      private String lblTextblockcontagemresultadoitem_pflfm_Internalname ;
      private String lblTextblockcontagemresultadoitem_pflfm_Jsonclick ;
      private String edtContagemResultadoItem_PFLFM_Internalname ;
      private String edtContagemResultadoItem_PFLFM_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode100 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private DateTime Z792ContagemResultadoItem_DataCnt ;
      private DateTime A792ContagemResultadoItem_DataCnt ;
      private bool Z789ContagemResultadoItem_ValidadoFM ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A789ContagemResultadoItem_ValidadoFM ;
      private bool n773ContagemResultadoItem_ContagemCod ;
      private bool n809ContagemResultadoItem_DemandaCorrigida ;
      private bool n794ContagemResultadoItem_CFPS ;
      private bool n792ContagemResultadoItem_DataCnt ;
      private bool n796ContagemResultadoItem_PFBTot ;
      private bool n797ContagemResultadoItem_PFLTot ;
      private bool n774ContagemResultadoItem_Descricao ;
      private bool n775ContagemResultadoItem_Tipo ;
      private bool n776ContagemResultadoItem_Deflator ;
      private bool n777ContagemResultadoItem_DER ;
      private bool n778ContagemResultadoItem_RA ;
      private bool n779ContagemResultadoItem_CP ;
      private bool n780ContagemResultadoItem_PFB ;
      private bool n781ContagemResultadoItem_PFL ;
      private bool n782ContagemResultadoItem_Evidencia ;
      private bool n789ContagemResultadoItem_ValidadoFM ;
      private bool n790ContagemResultadoItem_TipoFM ;
      private bool n786ContagemResultadoItem_DeflatorFM ;
      private bool n787ContagemResultadoItem_DERFM ;
      private bool n791ContagemResultadoItem_RAFM ;
      private bool n795ContagemResultadoItem_CPFM ;
      private bool n784ContagemResultadoItem_PFBFM ;
      private bool n785ContagemResultadoItem_PFLFM ;
      private bool Gx_longc ;
      private String A774ContagemResultadoItem_Descricao ;
      private String A782ContagemResultadoItem_Evidencia ;
      private String Z774ContagemResultadoItem_Descricao ;
      private String Z782ContagemResultadoItem_Evidencia ;
      private String Z793ContagemResultadoItem_Demanda ;
      private String Z809ContagemResultadoItem_DemandaCorrigida ;
      private String Z794ContagemResultadoItem_CFPS ;
      private String Z775ContagemResultadoItem_Tipo ;
      private String Z776ContagemResultadoItem_Deflator ;
      private String Z786ContagemResultadoItem_DeflatorFM ;
      private String A793ContagemResultadoItem_Demanda ;
      private String A809ContagemResultadoItem_DemandaCorrigida ;
      private String A794ContagemResultadoItem_CFPS ;
      private String A775ContagemResultadoItem_Tipo ;
      private String A776ContagemResultadoItem_Deflator ;
      private String A786ContagemResultadoItem_DeflatorFM ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkContagemResultadoItem_ValidadoFM ;
      private IDataStoreProvider pr_default ;
      private int[] T002K4_A772ContagemResultadoItem_Codigo ;
      private int[] T002K4_A773ContagemResultadoItem_ContagemCod ;
      private bool[] T002K4_n773ContagemResultadoItem_ContagemCod ;
      private String[] T002K4_A793ContagemResultadoItem_Demanda ;
      private String[] T002K4_A809ContagemResultadoItem_DemandaCorrigida ;
      private bool[] T002K4_n809ContagemResultadoItem_DemandaCorrigida ;
      private String[] T002K4_A794ContagemResultadoItem_CFPS ;
      private bool[] T002K4_n794ContagemResultadoItem_CFPS ;
      private DateTime[] T002K4_A792ContagemResultadoItem_DataCnt ;
      private bool[] T002K4_n792ContagemResultadoItem_DataCnt ;
      private decimal[] T002K4_A796ContagemResultadoItem_PFBTot ;
      private bool[] T002K4_n796ContagemResultadoItem_PFBTot ;
      private decimal[] T002K4_A797ContagemResultadoItem_PFLTot ;
      private bool[] T002K4_n797ContagemResultadoItem_PFLTot ;
      private String[] T002K4_A774ContagemResultadoItem_Descricao ;
      private bool[] T002K4_n774ContagemResultadoItem_Descricao ;
      private String[] T002K4_A775ContagemResultadoItem_Tipo ;
      private bool[] T002K4_n775ContagemResultadoItem_Tipo ;
      private String[] T002K4_A776ContagemResultadoItem_Deflator ;
      private bool[] T002K4_n776ContagemResultadoItem_Deflator ;
      private short[] T002K4_A777ContagemResultadoItem_DER ;
      private bool[] T002K4_n777ContagemResultadoItem_DER ;
      private short[] T002K4_A778ContagemResultadoItem_RA ;
      private bool[] T002K4_n778ContagemResultadoItem_RA ;
      private String[] T002K4_A779ContagemResultadoItem_CP ;
      private bool[] T002K4_n779ContagemResultadoItem_CP ;
      private decimal[] T002K4_A780ContagemResultadoItem_PFB ;
      private bool[] T002K4_n780ContagemResultadoItem_PFB ;
      private decimal[] T002K4_A781ContagemResultadoItem_PFL ;
      private bool[] T002K4_n781ContagemResultadoItem_PFL ;
      private String[] T002K4_A782ContagemResultadoItem_Evidencia ;
      private bool[] T002K4_n782ContagemResultadoItem_Evidencia ;
      private bool[] T002K4_A789ContagemResultadoItem_ValidadoFM ;
      private bool[] T002K4_n789ContagemResultadoItem_ValidadoFM ;
      private String[] T002K4_A790ContagemResultadoItem_TipoFM ;
      private bool[] T002K4_n790ContagemResultadoItem_TipoFM ;
      private String[] T002K4_A786ContagemResultadoItem_DeflatorFM ;
      private bool[] T002K4_n786ContagemResultadoItem_DeflatorFM ;
      private short[] T002K4_A787ContagemResultadoItem_DERFM ;
      private bool[] T002K4_n787ContagemResultadoItem_DERFM ;
      private short[] T002K4_A791ContagemResultadoItem_RAFM ;
      private bool[] T002K4_n791ContagemResultadoItem_RAFM ;
      private String[] T002K4_A795ContagemResultadoItem_CPFM ;
      private bool[] T002K4_n795ContagemResultadoItem_CPFM ;
      private decimal[] T002K4_A784ContagemResultadoItem_PFBFM ;
      private bool[] T002K4_n784ContagemResultadoItem_PFBFM ;
      private decimal[] T002K4_A785ContagemResultadoItem_PFLFM ;
      private bool[] T002K4_n785ContagemResultadoItem_PFLFM ;
      private int[] T002K5_A772ContagemResultadoItem_Codigo ;
      private int[] T002K3_A772ContagemResultadoItem_Codigo ;
      private int[] T002K3_A773ContagemResultadoItem_ContagemCod ;
      private bool[] T002K3_n773ContagemResultadoItem_ContagemCod ;
      private String[] T002K3_A793ContagemResultadoItem_Demanda ;
      private String[] T002K3_A809ContagemResultadoItem_DemandaCorrigida ;
      private bool[] T002K3_n809ContagemResultadoItem_DemandaCorrigida ;
      private String[] T002K3_A794ContagemResultadoItem_CFPS ;
      private bool[] T002K3_n794ContagemResultadoItem_CFPS ;
      private DateTime[] T002K3_A792ContagemResultadoItem_DataCnt ;
      private bool[] T002K3_n792ContagemResultadoItem_DataCnt ;
      private decimal[] T002K3_A796ContagemResultadoItem_PFBTot ;
      private bool[] T002K3_n796ContagemResultadoItem_PFBTot ;
      private decimal[] T002K3_A797ContagemResultadoItem_PFLTot ;
      private bool[] T002K3_n797ContagemResultadoItem_PFLTot ;
      private String[] T002K3_A774ContagemResultadoItem_Descricao ;
      private bool[] T002K3_n774ContagemResultadoItem_Descricao ;
      private String[] T002K3_A775ContagemResultadoItem_Tipo ;
      private bool[] T002K3_n775ContagemResultadoItem_Tipo ;
      private String[] T002K3_A776ContagemResultadoItem_Deflator ;
      private bool[] T002K3_n776ContagemResultadoItem_Deflator ;
      private short[] T002K3_A777ContagemResultadoItem_DER ;
      private bool[] T002K3_n777ContagemResultadoItem_DER ;
      private short[] T002K3_A778ContagemResultadoItem_RA ;
      private bool[] T002K3_n778ContagemResultadoItem_RA ;
      private String[] T002K3_A779ContagemResultadoItem_CP ;
      private bool[] T002K3_n779ContagemResultadoItem_CP ;
      private decimal[] T002K3_A780ContagemResultadoItem_PFB ;
      private bool[] T002K3_n780ContagemResultadoItem_PFB ;
      private decimal[] T002K3_A781ContagemResultadoItem_PFL ;
      private bool[] T002K3_n781ContagemResultadoItem_PFL ;
      private String[] T002K3_A782ContagemResultadoItem_Evidencia ;
      private bool[] T002K3_n782ContagemResultadoItem_Evidencia ;
      private bool[] T002K3_A789ContagemResultadoItem_ValidadoFM ;
      private bool[] T002K3_n789ContagemResultadoItem_ValidadoFM ;
      private String[] T002K3_A790ContagemResultadoItem_TipoFM ;
      private bool[] T002K3_n790ContagemResultadoItem_TipoFM ;
      private String[] T002K3_A786ContagemResultadoItem_DeflatorFM ;
      private bool[] T002K3_n786ContagemResultadoItem_DeflatorFM ;
      private short[] T002K3_A787ContagemResultadoItem_DERFM ;
      private bool[] T002K3_n787ContagemResultadoItem_DERFM ;
      private short[] T002K3_A791ContagemResultadoItem_RAFM ;
      private bool[] T002K3_n791ContagemResultadoItem_RAFM ;
      private String[] T002K3_A795ContagemResultadoItem_CPFM ;
      private bool[] T002K3_n795ContagemResultadoItem_CPFM ;
      private decimal[] T002K3_A784ContagemResultadoItem_PFBFM ;
      private bool[] T002K3_n784ContagemResultadoItem_PFBFM ;
      private decimal[] T002K3_A785ContagemResultadoItem_PFLFM ;
      private bool[] T002K3_n785ContagemResultadoItem_PFLFM ;
      private int[] T002K6_A772ContagemResultadoItem_Codigo ;
      private int[] T002K7_A772ContagemResultadoItem_Codigo ;
      private int[] T002K2_A772ContagemResultadoItem_Codigo ;
      private int[] T002K2_A773ContagemResultadoItem_ContagemCod ;
      private bool[] T002K2_n773ContagemResultadoItem_ContagemCod ;
      private String[] T002K2_A793ContagemResultadoItem_Demanda ;
      private String[] T002K2_A809ContagemResultadoItem_DemandaCorrigida ;
      private bool[] T002K2_n809ContagemResultadoItem_DemandaCorrigida ;
      private String[] T002K2_A794ContagemResultadoItem_CFPS ;
      private bool[] T002K2_n794ContagemResultadoItem_CFPS ;
      private DateTime[] T002K2_A792ContagemResultadoItem_DataCnt ;
      private bool[] T002K2_n792ContagemResultadoItem_DataCnt ;
      private decimal[] T002K2_A796ContagemResultadoItem_PFBTot ;
      private bool[] T002K2_n796ContagemResultadoItem_PFBTot ;
      private decimal[] T002K2_A797ContagemResultadoItem_PFLTot ;
      private bool[] T002K2_n797ContagemResultadoItem_PFLTot ;
      private String[] T002K2_A774ContagemResultadoItem_Descricao ;
      private bool[] T002K2_n774ContagemResultadoItem_Descricao ;
      private String[] T002K2_A775ContagemResultadoItem_Tipo ;
      private bool[] T002K2_n775ContagemResultadoItem_Tipo ;
      private String[] T002K2_A776ContagemResultadoItem_Deflator ;
      private bool[] T002K2_n776ContagemResultadoItem_Deflator ;
      private short[] T002K2_A777ContagemResultadoItem_DER ;
      private bool[] T002K2_n777ContagemResultadoItem_DER ;
      private short[] T002K2_A778ContagemResultadoItem_RA ;
      private bool[] T002K2_n778ContagemResultadoItem_RA ;
      private String[] T002K2_A779ContagemResultadoItem_CP ;
      private bool[] T002K2_n779ContagemResultadoItem_CP ;
      private decimal[] T002K2_A780ContagemResultadoItem_PFB ;
      private bool[] T002K2_n780ContagemResultadoItem_PFB ;
      private decimal[] T002K2_A781ContagemResultadoItem_PFL ;
      private bool[] T002K2_n781ContagemResultadoItem_PFL ;
      private String[] T002K2_A782ContagemResultadoItem_Evidencia ;
      private bool[] T002K2_n782ContagemResultadoItem_Evidencia ;
      private bool[] T002K2_A789ContagemResultadoItem_ValidadoFM ;
      private bool[] T002K2_n789ContagemResultadoItem_ValidadoFM ;
      private String[] T002K2_A790ContagemResultadoItem_TipoFM ;
      private bool[] T002K2_n790ContagemResultadoItem_TipoFM ;
      private String[] T002K2_A786ContagemResultadoItem_DeflatorFM ;
      private bool[] T002K2_n786ContagemResultadoItem_DeflatorFM ;
      private short[] T002K2_A787ContagemResultadoItem_DERFM ;
      private bool[] T002K2_n787ContagemResultadoItem_DERFM ;
      private short[] T002K2_A791ContagemResultadoItem_RAFM ;
      private bool[] T002K2_n791ContagemResultadoItem_RAFM ;
      private String[] T002K2_A795ContagemResultadoItem_CPFM ;
      private bool[] T002K2_n795ContagemResultadoItem_CPFM ;
      private decimal[] T002K2_A784ContagemResultadoItem_PFBFM ;
      private bool[] T002K2_n784ContagemResultadoItem_PFBFM ;
      private decimal[] T002K2_A785ContagemResultadoItem_PFLFM ;
      private bool[] T002K2_n785ContagemResultadoItem_PFLFM ;
      private int[] T002K8_A772ContagemResultadoItem_Codigo ;
      private int[] T002K11_A772ContagemResultadoItem_Codigo ;
      private GXWebForm Form ;
   }

   public class contagemresultadoitem__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002K4 ;
          prmT002K4 = new Object[] {
          new Object[] {"@ContagemResultadoItem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002K5 ;
          prmT002K5 = new Object[] {
          new Object[] {"@ContagemResultadoItem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002K3 ;
          prmT002K3 = new Object[] {
          new Object[] {"@ContagemResultadoItem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002K6 ;
          prmT002K6 = new Object[] {
          new Object[] {"@ContagemResultadoItem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002K7 ;
          prmT002K7 = new Object[] {
          new Object[] {"@ContagemResultadoItem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002K2 ;
          prmT002K2 = new Object[] {
          new Object[] {"@ContagemResultadoItem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002K8 ;
          prmT002K8 = new Object[] {
          new Object[] {"@ContagemResultadoItem_ContagemCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoItem_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@ContagemResultadoItem_DemandaCorrigida",SqlDbType.VarChar,30,0} ,
          new Object[] {"@ContagemResultadoItem_CFPS",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContagemResultadoItem_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoItem_PFBTot",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultadoItem_PFLTot",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultadoItem_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultadoItem_Tipo",SqlDbType.VarChar,3,0} ,
          new Object[] {"@ContagemResultadoItem_Deflator",SqlDbType.VarChar,5,2} ,
          new Object[] {"@ContagemResultadoItem_DER",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoItem_RA",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoItem_CP",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultadoItem_PFB",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultadoItem_PFL",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultadoItem_Evidencia",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultadoItem_ValidadoFM",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoItem_TipoFM",SqlDbType.Char,3,0} ,
          new Object[] {"@ContagemResultadoItem_DeflatorFM",SqlDbType.VarChar,5,2} ,
          new Object[] {"@ContagemResultadoItem_DERFM",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoItem_RAFM",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoItem_CPFM",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultadoItem_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultadoItem_PFLFM",SqlDbType.Decimal,14,5}
          } ;
          Object[] prmT002K9 ;
          prmT002K9 = new Object[] {
          new Object[] {"@ContagemResultadoItem_ContagemCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoItem_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@ContagemResultadoItem_DemandaCorrigida",SqlDbType.VarChar,30,0} ,
          new Object[] {"@ContagemResultadoItem_CFPS",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContagemResultadoItem_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoItem_PFBTot",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultadoItem_PFLTot",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultadoItem_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultadoItem_Tipo",SqlDbType.VarChar,3,0} ,
          new Object[] {"@ContagemResultadoItem_Deflator",SqlDbType.VarChar,5,2} ,
          new Object[] {"@ContagemResultadoItem_DER",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoItem_RA",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoItem_CP",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultadoItem_PFB",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultadoItem_PFL",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultadoItem_Evidencia",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultadoItem_ValidadoFM",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoItem_TipoFM",SqlDbType.Char,3,0} ,
          new Object[] {"@ContagemResultadoItem_DeflatorFM",SqlDbType.VarChar,5,2} ,
          new Object[] {"@ContagemResultadoItem_DERFM",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoItem_RAFM",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoItem_CPFM",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultadoItem_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultadoItem_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultadoItem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002K10 ;
          prmT002K10 = new Object[] {
          new Object[] {"@ContagemResultadoItem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002K11 ;
          prmT002K11 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T002K2", "SELECT [ContagemResultadoItem_Codigo], [ContagemResultadoItem_ContagemCod], [ContagemResultadoItem_Demanda], [ContagemResultadoItem_DemandaCorrigida], [ContagemResultadoItem_CFPS], [ContagemResultadoItem_DataCnt], [ContagemResultadoItem_PFBTot], [ContagemResultadoItem_PFLTot], [ContagemResultadoItem_Descricao], [ContagemResultadoItem_Tipo], [ContagemResultadoItem_Deflator], [ContagemResultadoItem_DER], [ContagemResultadoItem_RA], [ContagemResultadoItem_CP], [ContagemResultadoItem_PFB], [ContagemResultadoItem_PFL], [ContagemResultadoItem_Evidencia], [ContagemResultadoItem_ValidadoFM], [ContagemResultadoItem_TipoFM], [ContagemResultadoItem_DeflatorFM], [ContagemResultadoItem_DERFM], [ContagemResultadoItem_RAFM], [ContagemResultadoItem_CPFM], [ContagemResultadoItem_PFBFM], [ContagemResultadoItem_PFLFM] FROM [ContagemResultadoItem] WITH (UPDLOCK) WHERE [ContagemResultadoItem_Codigo] = @ContagemResultadoItem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002K2,1,0,true,false )
             ,new CursorDef("T002K3", "SELECT [ContagemResultadoItem_Codigo], [ContagemResultadoItem_ContagemCod], [ContagemResultadoItem_Demanda], [ContagemResultadoItem_DemandaCorrigida], [ContagemResultadoItem_CFPS], [ContagemResultadoItem_DataCnt], [ContagemResultadoItem_PFBTot], [ContagemResultadoItem_PFLTot], [ContagemResultadoItem_Descricao], [ContagemResultadoItem_Tipo], [ContagemResultadoItem_Deflator], [ContagemResultadoItem_DER], [ContagemResultadoItem_RA], [ContagemResultadoItem_CP], [ContagemResultadoItem_PFB], [ContagemResultadoItem_PFL], [ContagemResultadoItem_Evidencia], [ContagemResultadoItem_ValidadoFM], [ContagemResultadoItem_TipoFM], [ContagemResultadoItem_DeflatorFM], [ContagemResultadoItem_DERFM], [ContagemResultadoItem_RAFM], [ContagemResultadoItem_CPFM], [ContagemResultadoItem_PFBFM], [ContagemResultadoItem_PFLFM] FROM [ContagemResultadoItem] WITH (NOLOCK) WHERE [ContagemResultadoItem_Codigo] = @ContagemResultadoItem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002K3,1,0,true,false )
             ,new CursorDef("T002K4", "SELECT TM1.[ContagemResultadoItem_Codigo], TM1.[ContagemResultadoItem_ContagemCod], TM1.[ContagemResultadoItem_Demanda], TM1.[ContagemResultadoItem_DemandaCorrigida], TM1.[ContagemResultadoItem_CFPS], TM1.[ContagemResultadoItem_DataCnt], TM1.[ContagemResultadoItem_PFBTot], TM1.[ContagemResultadoItem_PFLTot], TM1.[ContagemResultadoItem_Descricao], TM1.[ContagemResultadoItem_Tipo], TM1.[ContagemResultadoItem_Deflator], TM1.[ContagemResultadoItem_DER], TM1.[ContagemResultadoItem_RA], TM1.[ContagemResultadoItem_CP], TM1.[ContagemResultadoItem_PFB], TM1.[ContagemResultadoItem_PFL], TM1.[ContagemResultadoItem_Evidencia], TM1.[ContagemResultadoItem_ValidadoFM], TM1.[ContagemResultadoItem_TipoFM], TM1.[ContagemResultadoItem_DeflatorFM], TM1.[ContagemResultadoItem_DERFM], TM1.[ContagemResultadoItem_RAFM], TM1.[ContagemResultadoItem_CPFM], TM1.[ContagemResultadoItem_PFBFM], TM1.[ContagemResultadoItem_PFLFM] FROM [ContagemResultadoItem] TM1 WITH (NOLOCK) WHERE TM1.[ContagemResultadoItem_Codigo] = @ContagemResultadoItem_Codigo ORDER BY TM1.[ContagemResultadoItem_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002K4,100,0,true,false )
             ,new CursorDef("T002K5", "SELECT [ContagemResultadoItem_Codigo] FROM [ContagemResultadoItem] WITH (NOLOCK) WHERE [ContagemResultadoItem_Codigo] = @ContagemResultadoItem_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002K5,1,0,true,false )
             ,new CursorDef("T002K6", "SELECT TOP 1 [ContagemResultadoItem_Codigo] FROM [ContagemResultadoItem] WITH (NOLOCK) WHERE ( [ContagemResultadoItem_Codigo] > @ContagemResultadoItem_Codigo) ORDER BY [ContagemResultadoItem_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002K6,1,0,true,true )
             ,new CursorDef("T002K7", "SELECT TOP 1 [ContagemResultadoItem_Codigo] FROM [ContagemResultadoItem] WITH (NOLOCK) WHERE ( [ContagemResultadoItem_Codigo] < @ContagemResultadoItem_Codigo) ORDER BY [ContagemResultadoItem_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002K7,1,0,true,true )
             ,new CursorDef("T002K8", "INSERT INTO [ContagemResultadoItem]([ContagemResultadoItem_ContagemCod], [ContagemResultadoItem_Demanda], [ContagemResultadoItem_DemandaCorrigida], [ContagemResultadoItem_CFPS], [ContagemResultadoItem_DataCnt], [ContagemResultadoItem_PFBTot], [ContagemResultadoItem_PFLTot], [ContagemResultadoItem_Descricao], [ContagemResultadoItem_Tipo], [ContagemResultadoItem_Deflator], [ContagemResultadoItem_DER], [ContagemResultadoItem_RA], [ContagemResultadoItem_CP], [ContagemResultadoItem_PFB], [ContagemResultadoItem_PFL], [ContagemResultadoItem_Evidencia], [ContagemResultadoItem_ValidadoFM], [ContagemResultadoItem_TipoFM], [ContagemResultadoItem_DeflatorFM], [ContagemResultadoItem_DERFM], [ContagemResultadoItem_RAFM], [ContagemResultadoItem_CPFM], [ContagemResultadoItem_PFBFM], [ContagemResultadoItem_PFLFM]) VALUES(@ContagemResultadoItem_ContagemCod, @ContagemResultadoItem_Demanda, @ContagemResultadoItem_DemandaCorrigida, @ContagemResultadoItem_CFPS, @ContagemResultadoItem_DataCnt, @ContagemResultadoItem_PFBTot, @ContagemResultadoItem_PFLTot, @ContagemResultadoItem_Descricao, @ContagemResultadoItem_Tipo, @ContagemResultadoItem_Deflator, @ContagemResultadoItem_DER, @ContagemResultadoItem_RA, @ContagemResultadoItem_CP, @ContagemResultadoItem_PFB, @ContagemResultadoItem_PFL, @ContagemResultadoItem_Evidencia, @ContagemResultadoItem_ValidadoFM, @ContagemResultadoItem_TipoFM, @ContagemResultadoItem_DeflatorFM, @ContagemResultadoItem_DERFM, @ContagemResultadoItem_RAFM, @ContagemResultadoItem_CPFM, @ContagemResultadoItem_PFBFM, @ContagemResultadoItem_PFLFM); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002K8)
             ,new CursorDef("T002K9", "UPDATE [ContagemResultadoItem] SET [ContagemResultadoItem_ContagemCod]=@ContagemResultadoItem_ContagemCod, [ContagemResultadoItem_Demanda]=@ContagemResultadoItem_Demanda, [ContagemResultadoItem_DemandaCorrigida]=@ContagemResultadoItem_DemandaCorrigida, [ContagemResultadoItem_CFPS]=@ContagemResultadoItem_CFPS, [ContagemResultadoItem_DataCnt]=@ContagemResultadoItem_DataCnt, [ContagemResultadoItem_PFBTot]=@ContagemResultadoItem_PFBTot, [ContagemResultadoItem_PFLTot]=@ContagemResultadoItem_PFLTot, [ContagemResultadoItem_Descricao]=@ContagemResultadoItem_Descricao, [ContagemResultadoItem_Tipo]=@ContagemResultadoItem_Tipo, [ContagemResultadoItem_Deflator]=@ContagemResultadoItem_Deflator, [ContagemResultadoItem_DER]=@ContagemResultadoItem_DER, [ContagemResultadoItem_RA]=@ContagemResultadoItem_RA, [ContagemResultadoItem_CP]=@ContagemResultadoItem_CP, [ContagemResultadoItem_PFB]=@ContagemResultadoItem_PFB, [ContagemResultadoItem_PFL]=@ContagemResultadoItem_PFL, [ContagemResultadoItem_Evidencia]=@ContagemResultadoItem_Evidencia, [ContagemResultadoItem_ValidadoFM]=@ContagemResultadoItem_ValidadoFM, [ContagemResultadoItem_TipoFM]=@ContagemResultadoItem_TipoFM, [ContagemResultadoItem_DeflatorFM]=@ContagemResultadoItem_DeflatorFM, [ContagemResultadoItem_DERFM]=@ContagemResultadoItem_DERFM, [ContagemResultadoItem_RAFM]=@ContagemResultadoItem_RAFM, [ContagemResultadoItem_CPFM]=@ContagemResultadoItem_CPFM, [ContagemResultadoItem_PFBFM]=@ContagemResultadoItem_PFBFM, [ContagemResultadoItem_PFLFM]=@ContagemResultadoItem_PFLFM  WHERE [ContagemResultadoItem_Codigo] = @ContagemResultadoItem_Codigo", GxErrorMask.GX_NOMASK,prmT002K9)
             ,new CursorDef("T002K10", "DELETE FROM [ContagemResultadoItem]  WHERE [ContagemResultadoItem_Codigo] = @ContagemResultadoItem_Codigo", GxErrorMask.GX_NOMASK,prmT002K10)
             ,new CursorDef("T002K11", "SELECT [ContagemResultadoItem_Codigo] FROM [ContagemResultadoItem] WITH (NOLOCK) ORDER BY [ContagemResultadoItem_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002K11,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((short[]) buf[20])[0] = rslt.getShort(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((short[]) buf[22])[0] = rslt.getShort(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getString(14, 1) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((decimal[]) buf[26])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((decimal[]) buf[28])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getLongVarchar(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((bool[]) buf[32])[0] = rslt.getBool(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((String[]) buf[34])[0] = rslt.getString(19, 3) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                ((String[]) buf[36])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                ((short[]) buf[38])[0] = rslt.getShort(21) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                ((short[]) buf[40])[0] = rslt.getShort(22) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(22);
                ((String[]) buf[42])[0] = rslt.getString(23, 1) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(23);
                ((decimal[]) buf[44])[0] = rslt.getDecimal(24) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(24);
                ((decimal[]) buf[46])[0] = rslt.getDecimal(25) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(25);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((short[]) buf[20])[0] = rslt.getShort(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((short[]) buf[22])[0] = rslt.getShort(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getString(14, 1) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((decimal[]) buf[26])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((decimal[]) buf[28])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getLongVarchar(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((bool[]) buf[32])[0] = rslt.getBool(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((String[]) buf[34])[0] = rslt.getString(19, 3) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                ((String[]) buf[36])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                ((short[]) buf[38])[0] = rslt.getShort(21) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                ((short[]) buf[40])[0] = rslt.getShort(22) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(22);
                ((String[]) buf[42])[0] = rslt.getString(23, 1) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(23);
                ((decimal[]) buf[44])[0] = rslt.getDecimal(24) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(24);
                ((decimal[]) buf[46])[0] = rslt.getDecimal(25) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(25);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((short[]) buf[20])[0] = rslt.getShort(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((short[]) buf[22])[0] = rslt.getShort(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getString(14, 1) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((decimal[]) buf[26])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((decimal[]) buf[28])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getLongVarchar(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((bool[]) buf[32])[0] = rslt.getBool(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((String[]) buf[34])[0] = rslt.getString(19, 3) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                ((String[]) buf[36])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                ((short[]) buf[38])[0] = rslt.getShort(21) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                ((short[]) buf[40])[0] = rslt.getShort(22) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(22);
                ((String[]) buf[42])[0] = rslt.getString(23, 1) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(23);
                ((decimal[]) buf[44])[0] = rslt.getDecimal(24) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(24);
                ((decimal[]) buf[46])[0] = rslt.getDecimal(25) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(25);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(5, (DateTime)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 12 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(12, (short)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 13 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(13, (String)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (decimal)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 16 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 17 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(17, (bool)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 18 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 19 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 20 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(20, (short)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 21 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(21, (short)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 22 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(22, (String)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 23 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(23, (decimal)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 24 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(24, (decimal)parms[46]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(5, (DateTime)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 12 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(12, (short)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 13 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(13, (String)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (decimal)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 16 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 17 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(17, (bool)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 18 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 19 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 20 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(20, (short)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 21 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(21, (short)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 22 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(22, (String)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 23 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(23, (decimal)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 24 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(24, (decimal)parms[46]);
                }
                stmt.SetParameter(25, (int)parms[47]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
