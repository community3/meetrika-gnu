/*
               File: PromptContrato
        Description: Selecione Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:12:28.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontrato : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontrato( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontrato( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContrato_Codigo ,
                           ref String aP1_InOutContrato_NumeroAta )
      {
         this.AV7InOutContrato_Codigo = aP0_InOutContrato_Codigo;
         this.AV45InOutContrato_NumeroAta = aP1_InOutContrato_NumeroAta;
         executePrivate();
         aP0_InOutContrato_Codigo=this.AV7InOutContrato_Codigo;
         aP1_InOutContrato_NumeroAta=this.AV45InOutContrato_NumeroAta;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkContrato_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_113 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_113_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_113_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV39Contrato_Numero1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Contrato_Numero1", AV39Contrato_Numero1);
               AV42Contrato_NumeroAta1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contrato_NumeroAta1", AV42Contrato_NumeroAta1);
               AV77Contrato_PrepostoNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Contrato_PrepostoNom1", AV77Contrato_PrepostoNom1);
               AV18Contratada_PessoaNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_PessoaNom1", AV18Contratada_PessoaNom1);
               AV36Contrato_Ano1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contrato_Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contrato_Ano1), 4, 0)));
               AV33Contrato_Ano_To1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contrato_Ano_To1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Contrato_Ano_To1), 4, 0)));
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
               AV40Contrato_Numero2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contrato_Numero2", AV40Contrato_Numero2);
               AV43Contrato_NumeroAta2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Contrato_NumeroAta2", AV43Contrato_NumeroAta2);
               AV78Contrato_PrepostoNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78Contrato_PrepostoNom2", AV78Contrato_PrepostoNom2);
               AV23Contratada_PessoaNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratada_PessoaNom2", AV23Contratada_PessoaNom2);
               AV37Contrato_Ano2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Contrato_Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37Contrato_Ano2), 4, 0)));
               AV34Contrato_Ano_To2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contrato_Ano_To2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contrato_Ano_To2), 4, 0)));
               AV25DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
               AV41Contrato_Numero3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Contrato_Numero3", AV41Contrato_Numero3);
               AV44Contrato_NumeroAta3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Contrato_NumeroAta3", AV44Contrato_NumeroAta3);
               AV79Contrato_PrepostoNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79Contrato_PrepostoNom3", AV79Contrato_PrepostoNom3);
               AV28Contratada_PessoaNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Contratada_PessoaNom3", AV28Contratada_PessoaNom3);
               AV38Contrato_Ano3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Contrato_Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Contrato_Ano3), 4, 0)));
               AV35Contrato_Ano_To3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Contrato_Ano_To3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Contrato_Ano_To3), 4, 0)));
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV24DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
               AV67TFContrato_Valor = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContrato_Valor", StringUtil.LTrim( StringUtil.Str( AV67TFContrato_Valor, 18, 5)));
               AV68TFContrato_Valor_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFContrato_Valor_To", StringUtil.LTrim( StringUtil.Str( AV68TFContrato_Valor_To, 18, 5)));
               AV71TFContrato_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContrato_Ativo_Sel", StringUtil.Str( (decimal)(AV71TFContrato_Ativo_Sel), 1, 0));
               AV69ddo_Contrato_ValorTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_Contrato_ValorTitleControlIdToReplace", AV69ddo_Contrato_ValorTitleControlIdToReplace);
               AV72ddo_Contrato_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Contrato_AtivoTitleControlIdToReplace", AV72ddo_Contrato_AtivoTitleControlIdToReplace);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV39Contrato_Numero1, AV42Contrato_NumeroAta1, AV77Contrato_PrepostoNom1, AV18Contratada_PessoaNom1, AV36Contrato_Ano1, AV33Contrato_Ano_To1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV40Contrato_Numero2, AV43Contrato_NumeroAta2, AV78Contrato_PrepostoNom2, AV23Contratada_PessoaNom2, AV37Contrato_Ano2, AV34Contrato_Ano_To2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV41Contrato_Numero3, AV44Contrato_NumeroAta3, AV79Contrato_PrepostoNom3, AV28Contratada_PessoaNom3, AV38Contrato_Ano3, AV35Contrato_Ano_To3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV67TFContrato_Valor, AV68TFContrato_Valor_To, AV71TFContrato_Ativo_Sel, AV69ddo_Contrato_ValorTitleControlIdToReplace, AV72ddo_Contrato_AtivoTitleControlIdToReplace) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContrato_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContrato_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV45InOutContrato_NumeroAta = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45InOutContrato_NumeroAta", AV45InOutContrato_NumeroAta);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA662( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS662( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE662( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031221122934");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontrato.aspx") + "?" + UrlEncode("" +AV7InOutContrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV45InOutContrato_NumeroAta))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_NUMERO1", StringUtil.RTrim( AV39Contrato_Numero1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_NUMEROATA1", StringUtil.RTrim( AV42Contrato_NumeroAta1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_PREPOSTONOM1", StringUtil.RTrim( AV77Contrato_PrepostoNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADA_PESSOANOM1", StringUtil.RTrim( AV18Contratada_PessoaNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_ANO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36Contrato_Ano1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_ANO_TO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33Contrato_Ano_To1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_NUMERO2", StringUtil.RTrim( AV40Contrato_Numero2));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_NUMEROATA2", StringUtil.RTrim( AV43Contrato_NumeroAta2));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_PREPOSTONOM2", StringUtil.RTrim( AV78Contrato_PrepostoNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADA_PESSOANOM2", StringUtil.RTrim( AV23Contratada_PessoaNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_ANO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37Contrato_Ano2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_ANO_TO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34Contrato_Ano_To2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV25DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_NUMERO3", StringUtil.RTrim( AV41Contrato_Numero3));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_NUMEROATA3", StringUtil.RTrim( AV44Contrato_NumeroAta3));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_PREPOSTONOM3", StringUtil.RTrim( AV79Contrato_PrepostoNom3));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADA_PESSOANOM3", StringUtil.RTrim( AV28Contratada_PessoaNom3));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_ANO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38Contrato_Ano3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_ANO_TO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35Contrato_Ano_To3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV24DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_VALOR", StringUtil.LTrim( StringUtil.NToC( AV67TFContrato_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_VALOR_TO", StringUtil.LTrim( StringUtil.NToC( AV68TFContrato_Valor_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV71TFContrato_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_113", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_113), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV75GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV73DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV73DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_VALORTITLEFILTERDATA", AV66Contrato_ValorTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_VALORTITLEFILTERDATA", AV66Contrato_ValorTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_ATIVOTITLEFILTERDATA", AV70Contrato_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_ATIVOTITLEFILTERDATA", AV70Contrato_AtivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATO_NUMEROATA", StringUtil.RTrim( AV45InOutContrato_NumeroAta));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Caption", StringUtil.RTrim( Ddo_contrato_valor_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Tooltip", StringUtil.RTrim( Ddo_contrato_valor_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Cls", StringUtil.RTrim( Ddo_contrato_valor_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_valor_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Filteredtextto_set", StringUtil.RTrim( Ddo_contrato_valor_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_valor_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_valor_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_valor_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_valor_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Sortedstatus", StringUtil.RTrim( Ddo_contrato_valor_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Includefilter", StringUtil.BoolToStr( Ddo_contrato_valor_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Filtertype", StringUtil.RTrim( Ddo_contrato_valor_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_valor_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_valor_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Sortasc", StringUtil.RTrim( Ddo_contrato_valor_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Sortdsc", StringUtil.RTrim( Ddo_contrato_valor_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Cleanfilter", StringUtil.RTrim( Ddo_contrato_valor_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Rangefilterfrom", StringUtil.RTrim( Ddo_contrato_valor_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Rangefilterto", StringUtil.RTrim( Ddo_contrato_valor_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_valor_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Caption", StringUtil.RTrim( Ddo_contrato_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Tooltip", StringUtil.RTrim( Ddo_contrato_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Cls", StringUtil.RTrim( Ddo_contrato_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_contrato_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_contrato_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contrato_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Sortasc", StringUtil.RTrim( Ddo_contrato_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_contrato_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Activeeventkey", StringUtil.RTrim( Ddo_contrato_valor_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_valor_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALOR_Filteredtextto_get", StringUtil.RTrim( Ddo_contrato_valor_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_contrato_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm662( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContrato" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Contrato" ;
      }

      protected void WB660( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_662( true) ;
         }
         else
         {
            wb_table1_2_662( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_662e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_113_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(129, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,129);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_113_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV24DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(130, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,130);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_valor_Internalname, StringUtil.LTrim( StringUtil.NToC( AV67TFContrato_Valor, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV67TFContrato_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,131);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_valor_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_valor_Visible, 1, 0, "text", "", 100, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_valor_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV68TFContrato_Valor_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV68TFContrato_Valor_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,132);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_valor_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_valor_to_Visible, 1, 0, "text", "", 100, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV71TFContrato_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV71TFContrato_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,133);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContrato.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_VALORContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_113_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_valortitlecontrolidtoreplace_Internalname, AV69ddo_Contrato_ValorTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"", 0, edtavDdo_contrato_valortitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContrato.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_113_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_ativotitlecontrolidtoreplace_Internalname, AV72ddo_Contrato_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,137);\"", 0, edtavDdo_contrato_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContrato.htm");
         }
         wbLoad = true;
      }

      protected void START662( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Contrato", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP660( ) ;
      }

      protected void WS662( )
      {
         START662( ) ;
         EVT662( ) ;
      }

      protected void EVT662( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11662 */
                           E11662 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_VALOR.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12662 */
                           E12662 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_ATIVO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13662 */
                           E13662 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14662 */
                           E14662 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15662 */
                           E15662 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16662 */
                           E16662 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17662 */
                           E17662 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18662 */
                           E18662 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19662 */
                           E19662 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20662 */
                           E20662 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21662 */
                           E21662 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22662 */
                           E22662 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23662 */
                           E23662 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_113_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_113_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_113_idx), 4, 0)), 4, "0");
                           SubsflControlProps_1132( ) ;
                           AV31Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV82Select_GXI : context.convertURL( context.PathToRelativeUrl( AV31Select))));
                           A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
                           A75Contrato_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtContrato_AreaTrabalhoCod_Internalname), ",", "."));
                           A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratada_Codigo_Internalname), ",", "."));
                           A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
                           A78Contrato_NumeroAta = cgiGet( edtContrato_NumeroAta_Internalname);
                           n78Contrato_NumeroAta = false;
                           A79Contrato_Ano = (short)(context.localUtil.CToN( cgiGet( edtContrato_Ano_Internalname), ",", "."));
                           A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
                           n41Contratada_PessoaNom = false;
                           A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
                           n42Contratada_PessoaCNPJ = false;
                           A89Contrato_Valor = context.localUtil.CToN( cgiGet( edtContrato_Valor_Internalname), ",", ".");
                           A92Contrato_Ativo = StringUtil.StrToBool( cgiGet( chkContrato_Ativo_Internalname));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E24662 */
                                 E24662 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E25662 */
                                 E25662 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E26662 */
                                 E26662 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_numero1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO1"), AV39Contrato_Numero1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_numeroata1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMEROATA1"), AV42Contrato_NumeroAta1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_prepostonom1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_PREPOSTONOM1"), AV77Contrato_PrepostoNom1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratada_pessoanom1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM1"), AV18Contratada_PessoaNom1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_ano1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO1"), ",", ".") != Convert.ToDecimal( AV36Contrato_Ano1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_ano_to1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO_TO1"), ",", ".") != Convert.ToDecimal( AV33Contrato_Ano_To1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_numero2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO2"), AV40Contrato_Numero2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_numeroata2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMEROATA2"), AV43Contrato_NumeroAta2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_prepostonom2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_PREPOSTONOM2"), AV78Contrato_PrepostoNom2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratada_pessoanom2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM2"), AV23Contratada_PessoaNom2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_ano2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO2"), ",", ".") != Convert.ToDecimal( AV37Contrato_Ano2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_ano_to2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO_TO2"), ",", ".") != Convert.ToDecimal( AV34Contrato_Ano_To2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_numero3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO3"), AV41Contrato_Numero3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_numeroata3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMEROATA3"), AV44Contrato_NumeroAta3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_prepostonom3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_PREPOSTONOM3"), AV79Contrato_PrepostoNom3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratada_pessoanom3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM3"), AV28Contratada_PessoaNom3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_ano3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO3"), ",", ".") != Convert.ToDecimal( AV38Contrato_Ano3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contrato_ano_to3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO_TO3"), ",", ".") != Convert.ToDecimal( AV35Contrato_Ano_To3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_valor Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_VALOR"), ",", ".") != AV67TFContrato_Valor )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_valor_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_VALOR_TO"), ",", ".") != AV68TFContrato_Valor_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_ativo_sel Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV71TFContrato_Ativo_Sel )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E27662 */
                                       E27662 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE662( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm662( ) ;
            }
         }
      }

      protected void PA662( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATO_NUMERO", "N� de Contrato", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATO_NUMEROATA", "N�mero da Ata", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATO_PREPOSTONOM", "Preposto", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATADA_PESSOANOM", "Contratada", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATO_ANO", "Ano", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATO_NUMERO", "N� de Contrato", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATO_NUMEROATA", "N�mero da Ata", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATO_PREPOSTONOM", "Preposto", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATADA_PESSOANOM", "Contratada", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATO_ANO", "Ano", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATO_NUMERO", "N� de Contrato", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATO_NUMEROATA", "N�mero da Ata", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATO_PREPOSTONOM", "Preposto", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATADA_PESSOANOM", "Contratada", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATO_ANO", "Ano", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "CONTRATO_ATIVO_" + sGXsfl_113_idx;
            chkContrato_Ativo.Name = GXCCtl;
            chkContrato_Ativo.WebTags = "";
            chkContrato_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContrato_Ativo_Internalname, "TitleCaption", chkContrato_Ativo.Caption);
            chkContrato_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1132( ) ;
         while ( nGXsfl_113_idx <= nRC_GXsfl_113 )
         {
            sendrow_1132( ) ;
            nGXsfl_113_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_113_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_113_idx+1));
            sGXsfl_113_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_113_idx), 4, 0)), 4, "0");
            SubsflControlProps_1132( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV39Contrato_Numero1 ,
                                       String AV42Contrato_NumeroAta1 ,
                                       String AV77Contrato_PrepostoNom1 ,
                                       String AV18Contratada_PessoaNom1 ,
                                       short AV36Contrato_Ano1 ,
                                       short AV33Contrato_Ano_To1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       String AV40Contrato_Numero2 ,
                                       String AV43Contrato_NumeroAta2 ,
                                       String AV78Contrato_PrepostoNom2 ,
                                       String AV23Contratada_PessoaNom2 ,
                                       short AV37Contrato_Ano2 ,
                                       short AV34Contrato_Ano_To2 ,
                                       String AV25DynamicFiltersSelector3 ,
                                       short AV26DynamicFiltersOperator3 ,
                                       String AV41Contrato_Numero3 ,
                                       String AV44Contrato_NumeroAta3 ,
                                       String AV79Contrato_PrepostoNom3 ,
                                       String AV28Contratada_PessoaNom3 ,
                                       short AV38Contrato_Ano3 ,
                                       short AV35Contrato_Ano_To3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV24DynamicFiltersEnabled3 ,
                                       decimal AV67TFContrato_Valor ,
                                       decimal AV68TFContrato_Valor_To ,
                                       short AV71TFContrato_Ativo_Sel ,
                                       String AV69ddo_Contrato_ValorTitleControlIdToReplace ,
                                       String AV72ddo_Contrato_AtivoTitleControlIdToReplace )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF662( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A75Contrato_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATO_NUMERO", StringUtil.RTrim( A77Contrato_Numero));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_NUMEROATA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATO_NUMEROATA", StringUtil.RTrim( A78Contrato_NumeroAta));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_ANO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_ANO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_VALOR", GetSecureSignedToken( "", context.localUtil.Format( A89Contrato_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_VALOR", StringUtil.LTrim( StringUtil.NToC( A89Contrato_Valor, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_ATIVO", GetSecureSignedToken( "", A92Contrato_Ativo));
         GxWebStd.gx_hidden_field( context, "CONTRATO_ATIVO", StringUtil.BoolToStr( A92Contrato_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF662( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF662( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 113;
         /* Execute user event: E25662 */
         E25662 ();
         nGXsfl_113_idx = 1;
         sGXsfl_113_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_113_idx), 4, 0)), 4, "0");
         SubsflControlProps_1132( ) ;
         nGXsfl_113_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1132( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV39Contrato_Numero1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV42Contrato_NumeroAta1 ,
                                                 AV77Contrato_PrepostoNom1 ,
                                                 AV18Contratada_PessoaNom1 ,
                                                 AV36Contrato_Ano1 ,
                                                 AV33Contrato_Ano_To1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV40Contrato_Numero2 ,
                                                 AV21DynamicFiltersOperator2 ,
                                                 AV43Contrato_NumeroAta2 ,
                                                 AV78Contrato_PrepostoNom2 ,
                                                 AV23Contratada_PessoaNom2 ,
                                                 AV37Contrato_Ano2 ,
                                                 AV34Contrato_Ano_To2 ,
                                                 AV24DynamicFiltersEnabled3 ,
                                                 AV25DynamicFiltersSelector3 ,
                                                 AV41Contrato_Numero3 ,
                                                 AV26DynamicFiltersOperator3 ,
                                                 AV44Contrato_NumeroAta3 ,
                                                 AV79Contrato_PrepostoNom3 ,
                                                 AV28Contratada_PessoaNom3 ,
                                                 AV38Contrato_Ano3 ,
                                                 AV35Contrato_Ano_To3 ,
                                                 AV67TFContrato_Valor ,
                                                 AV68TFContrato_Valor_To ,
                                                 AV71TFContrato_Ativo_Sel ,
                                                 A77Contrato_Numero ,
                                                 A78Contrato_NumeroAta ,
                                                 A1015Contrato_PrepostoNom ,
                                                 A41Contratada_PessoaNom ,
                                                 A79Contrato_Ano ,
                                                 A89Contrato_Valor ,
                                                 A92Contrato_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN
                                                 }
            });
            lV39Contrato_Numero1 = StringUtil.PadR( StringUtil.RTrim( AV39Contrato_Numero1), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Contrato_Numero1", AV39Contrato_Numero1);
            lV42Contrato_NumeroAta1 = StringUtil.PadR( StringUtil.RTrim( AV42Contrato_NumeroAta1), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contrato_NumeroAta1", AV42Contrato_NumeroAta1);
            lV42Contrato_NumeroAta1 = StringUtil.PadR( StringUtil.RTrim( AV42Contrato_NumeroAta1), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contrato_NumeroAta1", AV42Contrato_NumeroAta1);
            lV77Contrato_PrepostoNom1 = StringUtil.PadR( StringUtil.RTrim( AV77Contrato_PrepostoNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Contrato_PrepostoNom1", AV77Contrato_PrepostoNom1);
            lV77Contrato_PrepostoNom1 = StringUtil.PadR( StringUtil.RTrim( AV77Contrato_PrepostoNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Contrato_PrepostoNom1", AV77Contrato_PrepostoNom1);
            lV18Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV18Contratada_PessoaNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_PessoaNom1", AV18Contratada_PessoaNom1);
            lV18Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV18Contratada_PessoaNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_PessoaNom1", AV18Contratada_PessoaNom1);
            lV40Contrato_Numero2 = StringUtil.PadR( StringUtil.RTrim( AV40Contrato_Numero2), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contrato_Numero2", AV40Contrato_Numero2);
            lV43Contrato_NumeroAta2 = StringUtil.PadR( StringUtil.RTrim( AV43Contrato_NumeroAta2), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Contrato_NumeroAta2", AV43Contrato_NumeroAta2);
            lV43Contrato_NumeroAta2 = StringUtil.PadR( StringUtil.RTrim( AV43Contrato_NumeroAta2), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Contrato_NumeroAta2", AV43Contrato_NumeroAta2);
            lV78Contrato_PrepostoNom2 = StringUtil.PadR( StringUtil.RTrim( AV78Contrato_PrepostoNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78Contrato_PrepostoNom2", AV78Contrato_PrepostoNom2);
            lV78Contrato_PrepostoNom2 = StringUtil.PadR( StringUtil.RTrim( AV78Contrato_PrepostoNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78Contrato_PrepostoNom2", AV78Contrato_PrepostoNom2);
            lV23Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV23Contratada_PessoaNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratada_PessoaNom2", AV23Contratada_PessoaNom2);
            lV23Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV23Contratada_PessoaNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratada_PessoaNom2", AV23Contratada_PessoaNom2);
            lV41Contrato_Numero3 = StringUtil.PadR( StringUtil.RTrim( AV41Contrato_Numero3), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Contrato_Numero3", AV41Contrato_Numero3);
            lV44Contrato_NumeroAta3 = StringUtil.PadR( StringUtil.RTrim( AV44Contrato_NumeroAta3), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Contrato_NumeroAta3", AV44Contrato_NumeroAta3);
            lV44Contrato_NumeroAta3 = StringUtil.PadR( StringUtil.RTrim( AV44Contrato_NumeroAta3), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Contrato_NumeroAta3", AV44Contrato_NumeroAta3);
            lV79Contrato_PrepostoNom3 = StringUtil.PadR( StringUtil.RTrim( AV79Contrato_PrepostoNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79Contrato_PrepostoNom3", AV79Contrato_PrepostoNom3);
            lV79Contrato_PrepostoNom3 = StringUtil.PadR( StringUtil.RTrim( AV79Contrato_PrepostoNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79Contrato_PrepostoNom3", AV79Contrato_PrepostoNom3);
            lV28Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV28Contratada_PessoaNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Contratada_PessoaNom3", AV28Contratada_PessoaNom3);
            lV28Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV28Contratada_PessoaNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Contratada_PessoaNom3", AV28Contratada_PessoaNom3);
            /* Using cursor H00662 */
            pr_default.execute(0, new Object[] {lV39Contrato_Numero1, lV42Contrato_NumeroAta1, lV42Contrato_NumeroAta1, lV77Contrato_PrepostoNom1, lV77Contrato_PrepostoNom1, lV18Contratada_PessoaNom1, lV18Contratada_PessoaNom1, AV36Contrato_Ano1, AV33Contrato_Ano_To1, lV40Contrato_Numero2, lV43Contrato_NumeroAta2, lV43Contrato_NumeroAta2, lV78Contrato_PrepostoNom2, lV78Contrato_PrepostoNom2, lV23Contratada_PessoaNom2, lV23Contratada_PessoaNom2, AV37Contrato_Ano2, AV34Contrato_Ano_To2, lV41Contrato_Numero3, lV44Contrato_NumeroAta3, lV44Contrato_NumeroAta3, lV79Contrato_PrepostoNom3, lV79Contrato_PrepostoNom3, lV28Contratada_PessoaNom3, lV28Contratada_PessoaNom3, AV38Contrato_Ano3, AV35Contrato_Ano_To3, AV67TFContrato_Valor, AV68TFContrato_Valor_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_113_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A40Contratada_PessoaCod = H00662_A40Contratada_PessoaCod[0];
               A1013Contrato_PrepostoCod = H00662_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = H00662_n1013Contrato_PrepostoCod[0];
               A1016Contrato_PrepostoPesCod = H00662_A1016Contrato_PrepostoPesCod[0];
               n1016Contrato_PrepostoPesCod = H00662_n1016Contrato_PrepostoPesCod[0];
               A1015Contrato_PrepostoNom = H00662_A1015Contrato_PrepostoNom[0];
               n1015Contrato_PrepostoNom = H00662_n1015Contrato_PrepostoNom[0];
               A92Contrato_Ativo = H00662_A92Contrato_Ativo[0];
               A89Contrato_Valor = H00662_A89Contrato_Valor[0];
               A42Contratada_PessoaCNPJ = H00662_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H00662_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H00662_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00662_n41Contratada_PessoaNom[0];
               A79Contrato_Ano = H00662_A79Contrato_Ano[0];
               A78Contrato_NumeroAta = H00662_A78Contrato_NumeroAta[0];
               n78Contrato_NumeroAta = H00662_n78Contrato_NumeroAta[0];
               A77Contrato_Numero = H00662_A77Contrato_Numero[0];
               A39Contratada_Codigo = H00662_A39Contratada_Codigo[0];
               A75Contrato_AreaTrabalhoCod = H00662_A75Contrato_AreaTrabalhoCod[0];
               A74Contrato_Codigo = H00662_A74Contrato_Codigo[0];
               A1016Contrato_PrepostoPesCod = H00662_A1016Contrato_PrepostoPesCod[0];
               n1016Contrato_PrepostoPesCod = H00662_n1016Contrato_PrepostoPesCod[0];
               A1015Contrato_PrepostoNom = H00662_A1015Contrato_PrepostoNom[0];
               n1015Contrato_PrepostoNom = H00662_n1015Contrato_PrepostoNom[0];
               A40Contratada_PessoaCod = H00662_A40Contratada_PessoaCod[0];
               A42Contratada_PessoaCNPJ = H00662_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H00662_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H00662_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00662_n41Contratada_PessoaNom[0];
               /* Execute user event: E26662 */
               E26662 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 113;
            WB660( ) ;
         }
         nGXsfl_113_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV39Contrato_Numero1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV42Contrato_NumeroAta1 ,
                                              AV77Contrato_PrepostoNom1 ,
                                              AV18Contratada_PessoaNom1 ,
                                              AV36Contrato_Ano1 ,
                                              AV33Contrato_Ano_To1 ,
                                              AV19DynamicFiltersEnabled2 ,
                                              AV20DynamicFiltersSelector2 ,
                                              AV40Contrato_Numero2 ,
                                              AV21DynamicFiltersOperator2 ,
                                              AV43Contrato_NumeroAta2 ,
                                              AV78Contrato_PrepostoNom2 ,
                                              AV23Contratada_PessoaNom2 ,
                                              AV37Contrato_Ano2 ,
                                              AV34Contrato_Ano_To2 ,
                                              AV24DynamicFiltersEnabled3 ,
                                              AV25DynamicFiltersSelector3 ,
                                              AV41Contrato_Numero3 ,
                                              AV26DynamicFiltersOperator3 ,
                                              AV44Contrato_NumeroAta3 ,
                                              AV79Contrato_PrepostoNom3 ,
                                              AV28Contratada_PessoaNom3 ,
                                              AV38Contrato_Ano3 ,
                                              AV35Contrato_Ano_To3 ,
                                              AV67TFContrato_Valor ,
                                              AV68TFContrato_Valor_To ,
                                              AV71TFContrato_Ativo_Sel ,
                                              A77Contrato_Numero ,
                                              A78Contrato_NumeroAta ,
                                              A1015Contrato_PrepostoNom ,
                                              A41Contratada_PessoaNom ,
                                              A79Contrato_Ano ,
                                              A89Contrato_Valor ,
                                              A92Contrato_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV39Contrato_Numero1 = StringUtil.PadR( StringUtil.RTrim( AV39Contrato_Numero1), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Contrato_Numero1", AV39Contrato_Numero1);
         lV42Contrato_NumeroAta1 = StringUtil.PadR( StringUtil.RTrim( AV42Contrato_NumeroAta1), 10, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contrato_NumeroAta1", AV42Contrato_NumeroAta1);
         lV42Contrato_NumeroAta1 = StringUtil.PadR( StringUtil.RTrim( AV42Contrato_NumeroAta1), 10, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contrato_NumeroAta1", AV42Contrato_NumeroAta1);
         lV77Contrato_PrepostoNom1 = StringUtil.PadR( StringUtil.RTrim( AV77Contrato_PrepostoNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Contrato_PrepostoNom1", AV77Contrato_PrepostoNom1);
         lV77Contrato_PrepostoNom1 = StringUtil.PadR( StringUtil.RTrim( AV77Contrato_PrepostoNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Contrato_PrepostoNom1", AV77Contrato_PrepostoNom1);
         lV18Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV18Contratada_PessoaNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_PessoaNom1", AV18Contratada_PessoaNom1);
         lV18Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV18Contratada_PessoaNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_PessoaNom1", AV18Contratada_PessoaNom1);
         lV40Contrato_Numero2 = StringUtil.PadR( StringUtil.RTrim( AV40Contrato_Numero2), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contrato_Numero2", AV40Contrato_Numero2);
         lV43Contrato_NumeroAta2 = StringUtil.PadR( StringUtil.RTrim( AV43Contrato_NumeroAta2), 10, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Contrato_NumeroAta2", AV43Contrato_NumeroAta2);
         lV43Contrato_NumeroAta2 = StringUtil.PadR( StringUtil.RTrim( AV43Contrato_NumeroAta2), 10, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Contrato_NumeroAta2", AV43Contrato_NumeroAta2);
         lV78Contrato_PrepostoNom2 = StringUtil.PadR( StringUtil.RTrim( AV78Contrato_PrepostoNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78Contrato_PrepostoNom2", AV78Contrato_PrepostoNom2);
         lV78Contrato_PrepostoNom2 = StringUtil.PadR( StringUtil.RTrim( AV78Contrato_PrepostoNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78Contrato_PrepostoNom2", AV78Contrato_PrepostoNom2);
         lV23Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV23Contratada_PessoaNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratada_PessoaNom2", AV23Contratada_PessoaNom2);
         lV23Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV23Contratada_PessoaNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratada_PessoaNom2", AV23Contratada_PessoaNom2);
         lV41Contrato_Numero3 = StringUtil.PadR( StringUtil.RTrim( AV41Contrato_Numero3), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Contrato_Numero3", AV41Contrato_Numero3);
         lV44Contrato_NumeroAta3 = StringUtil.PadR( StringUtil.RTrim( AV44Contrato_NumeroAta3), 10, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Contrato_NumeroAta3", AV44Contrato_NumeroAta3);
         lV44Contrato_NumeroAta3 = StringUtil.PadR( StringUtil.RTrim( AV44Contrato_NumeroAta3), 10, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Contrato_NumeroAta3", AV44Contrato_NumeroAta3);
         lV79Contrato_PrepostoNom3 = StringUtil.PadR( StringUtil.RTrim( AV79Contrato_PrepostoNom3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79Contrato_PrepostoNom3", AV79Contrato_PrepostoNom3);
         lV79Contrato_PrepostoNom3 = StringUtil.PadR( StringUtil.RTrim( AV79Contrato_PrepostoNom3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79Contrato_PrepostoNom3", AV79Contrato_PrepostoNom3);
         lV28Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV28Contratada_PessoaNom3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Contratada_PessoaNom3", AV28Contratada_PessoaNom3);
         lV28Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV28Contratada_PessoaNom3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Contratada_PessoaNom3", AV28Contratada_PessoaNom3);
         /* Using cursor H00663 */
         pr_default.execute(1, new Object[] {lV39Contrato_Numero1, lV42Contrato_NumeroAta1, lV42Contrato_NumeroAta1, lV77Contrato_PrepostoNom1, lV77Contrato_PrepostoNom1, lV18Contratada_PessoaNom1, lV18Contratada_PessoaNom1, AV36Contrato_Ano1, AV33Contrato_Ano_To1, lV40Contrato_Numero2, lV43Contrato_NumeroAta2, lV43Contrato_NumeroAta2, lV78Contrato_PrepostoNom2, lV78Contrato_PrepostoNom2, lV23Contratada_PessoaNom2, lV23Contratada_PessoaNom2, AV37Contrato_Ano2, AV34Contrato_Ano_To2, lV41Contrato_Numero3, lV44Contrato_NumeroAta3, lV44Contrato_NumeroAta3, lV79Contrato_PrepostoNom3, lV79Contrato_PrepostoNom3, lV28Contratada_PessoaNom3, lV28Contratada_PessoaNom3, AV38Contrato_Ano3, AV35Contrato_Ano_To3, AV67TFContrato_Valor, AV68TFContrato_Valor_To});
         GRID_nRecordCount = H00663_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV39Contrato_Numero1, AV42Contrato_NumeroAta1, AV77Contrato_PrepostoNom1, AV18Contratada_PessoaNom1, AV36Contrato_Ano1, AV33Contrato_Ano_To1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV40Contrato_Numero2, AV43Contrato_NumeroAta2, AV78Contrato_PrepostoNom2, AV23Contratada_PessoaNom2, AV37Contrato_Ano2, AV34Contrato_Ano_To2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV41Contrato_Numero3, AV44Contrato_NumeroAta3, AV79Contrato_PrepostoNom3, AV28Contratada_PessoaNom3, AV38Contrato_Ano3, AV35Contrato_Ano_To3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV67TFContrato_Valor, AV68TFContrato_Valor_To, AV71TFContrato_Ativo_Sel, AV69ddo_Contrato_ValorTitleControlIdToReplace, AV72ddo_Contrato_AtivoTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV39Contrato_Numero1, AV42Contrato_NumeroAta1, AV77Contrato_PrepostoNom1, AV18Contratada_PessoaNom1, AV36Contrato_Ano1, AV33Contrato_Ano_To1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV40Contrato_Numero2, AV43Contrato_NumeroAta2, AV78Contrato_PrepostoNom2, AV23Contratada_PessoaNom2, AV37Contrato_Ano2, AV34Contrato_Ano_To2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV41Contrato_Numero3, AV44Contrato_NumeroAta3, AV79Contrato_PrepostoNom3, AV28Contratada_PessoaNom3, AV38Contrato_Ano3, AV35Contrato_Ano_To3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV67TFContrato_Valor, AV68TFContrato_Valor_To, AV71TFContrato_Ativo_Sel, AV69ddo_Contrato_ValorTitleControlIdToReplace, AV72ddo_Contrato_AtivoTitleControlIdToReplace) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV39Contrato_Numero1, AV42Contrato_NumeroAta1, AV77Contrato_PrepostoNom1, AV18Contratada_PessoaNom1, AV36Contrato_Ano1, AV33Contrato_Ano_To1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV40Contrato_Numero2, AV43Contrato_NumeroAta2, AV78Contrato_PrepostoNom2, AV23Contratada_PessoaNom2, AV37Contrato_Ano2, AV34Contrato_Ano_To2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV41Contrato_Numero3, AV44Contrato_NumeroAta3, AV79Contrato_PrepostoNom3, AV28Contratada_PessoaNom3, AV38Contrato_Ano3, AV35Contrato_Ano_To3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV67TFContrato_Valor, AV68TFContrato_Valor_To, AV71TFContrato_Ativo_Sel, AV69ddo_Contrato_ValorTitleControlIdToReplace, AV72ddo_Contrato_AtivoTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV39Contrato_Numero1, AV42Contrato_NumeroAta1, AV77Contrato_PrepostoNom1, AV18Contratada_PessoaNom1, AV36Contrato_Ano1, AV33Contrato_Ano_To1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV40Contrato_Numero2, AV43Contrato_NumeroAta2, AV78Contrato_PrepostoNom2, AV23Contratada_PessoaNom2, AV37Contrato_Ano2, AV34Contrato_Ano_To2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV41Contrato_Numero3, AV44Contrato_NumeroAta3, AV79Contrato_PrepostoNom3, AV28Contratada_PessoaNom3, AV38Contrato_Ano3, AV35Contrato_Ano_To3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV67TFContrato_Valor, AV68TFContrato_Valor_To, AV71TFContrato_Ativo_Sel, AV69ddo_Contrato_ValorTitleControlIdToReplace, AV72ddo_Contrato_AtivoTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV39Contrato_Numero1, AV42Contrato_NumeroAta1, AV77Contrato_PrepostoNom1, AV18Contratada_PessoaNom1, AV36Contrato_Ano1, AV33Contrato_Ano_To1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV40Contrato_Numero2, AV43Contrato_NumeroAta2, AV78Contrato_PrepostoNom2, AV23Contratada_PessoaNom2, AV37Contrato_Ano2, AV34Contrato_Ano_To2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV41Contrato_Numero3, AV44Contrato_NumeroAta3, AV79Contrato_PrepostoNom3, AV28Contratada_PessoaNom3, AV38Contrato_Ano3, AV35Contrato_Ano_To3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV67TFContrato_Valor, AV68TFContrato_Valor_To, AV71TFContrato_Ativo_Sel, AV69ddo_Contrato_ValorTitleControlIdToReplace, AV72ddo_Contrato_AtivoTitleControlIdToReplace) ;
         }
         return (int)(0) ;
      }

      protected void STRUP660( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E24662 */
         E24662 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV73DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_VALORTITLEFILTERDATA"), AV66Contrato_ValorTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_ATIVOTITLEFILTERDATA"), AV70Contrato_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV39Contrato_Numero1 = cgiGet( edtavContrato_numero1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Contrato_Numero1", AV39Contrato_Numero1);
            AV42Contrato_NumeroAta1 = cgiGet( edtavContrato_numeroata1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contrato_NumeroAta1", AV42Contrato_NumeroAta1);
            AV77Contrato_PrepostoNom1 = StringUtil.Upper( cgiGet( edtavContrato_prepostonom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Contrato_PrepostoNom1", AV77Contrato_PrepostoNom1);
            AV18Contratada_PessoaNom1 = StringUtil.Upper( cgiGet( edtavContratada_pessoanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_PessoaNom1", AV18Contratada_PessoaNom1);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano1_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_ANO1");
               GX_FocusControl = edtavContrato_ano1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36Contrato_Ano1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contrato_Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contrato_Ano1), 4, 0)));
            }
            else
            {
               AV36Contrato_Ano1 = (short)(context.localUtil.CToN( cgiGet( edtavContrato_ano1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contrato_Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contrato_Ano1), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano_to1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano_to1_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_ANO_TO1");
               GX_FocusControl = edtavContrato_ano_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33Contrato_Ano_To1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contrato_Ano_To1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Contrato_Ano_To1), 4, 0)));
            }
            else
            {
               AV33Contrato_Ano_To1 = (short)(context.localUtil.CToN( cgiGet( edtavContrato_ano_to1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contrato_Ano_To1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Contrato_Ano_To1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            AV40Contrato_Numero2 = cgiGet( edtavContrato_numero2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contrato_Numero2", AV40Contrato_Numero2);
            AV43Contrato_NumeroAta2 = cgiGet( edtavContrato_numeroata2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Contrato_NumeroAta2", AV43Contrato_NumeroAta2);
            AV78Contrato_PrepostoNom2 = StringUtil.Upper( cgiGet( edtavContrato_prepostonom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78Contrato_PrepostoNom2", AV78Contrato_PrepostoNom2);
            AV23Contratada_PessoaNom2 = StringUtil.Upper( cgiGet( edtavContratada_pessoanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratada_PessoaNom2", AV23Contratada_PessoaNom2);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano2_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_ANO2");
               GX_FocusControl = edtavContrato_ano2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37Contrato_Ano2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Contrato_Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37Contrato_Ano2), 4, 0)));
            }
            else
            {
               AV37Contrato_Ano2 = (short)(context.localUtil.CToN( cgiGet( edtavContrato_ano2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Contrato_Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37Contrato_Ano2), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano_to2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano_to2_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_ANO_TO2");
               GX_FocusControl = edtavContrato_ano_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34Contrato_Ano_To2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contrato_Ano_To2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contrato_Ano_To2), 4, 0)));
            }
            else
            {
               AV34Contrato_Ano_To2 = (short)(context.localUtil.CToN( cgiGet( edtavContrato_ano_to2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contrato_Ano_To2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contrato_Ano_To2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV25DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            AV41Contrato_Numero3 = cgiGet( edtavContrato_numero3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Contrato_Numero3", AV41Contrato_Numero3);
            AV44Contrato_NumeroAta3 = cgiGet( edtavContrato_numeroata3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Contrato_NumeroAta3", AV44Contrato_NumeroAta3);
            AV79Contrato_PrepostoNom3 = StringUtil.Upper( cgiGet( edtavContrato_prepostonom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79Contrato_PrepostoNom3", AV79Contrato_PrepostoNom3);
            AV28Contratada_PessoaNom3 = StringUtil.Upper( cgiGet( edtavContratada_pessoanom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Contratada_PessoaNom3", AV28Contratada_PessoaNom3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano3_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_ANO3");
               GX_FocusControl = edtavContrato_ano3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38Contrato_Ano3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Contrato_Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Contrato_Ano3), 4, 0)));
            }
            else
            {
               AV38Contrato_Ano3 = (short)(context.localUtil.CToN( cgiGet( edtavContrato_ano3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Contrato_Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Contrato_Ano3), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano_to3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano_to3_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_ANO_TO3");
               GX_FocusControl = edtavContrato_ano_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35Contrato_Ano_To3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Contrato_Ano_To3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Contrato_Ano_To3), 4, 0)));
            }
            else
            {
               AV35Contrato_Ano_To3 = (short)(context.localUtil.CToN( cgiGet( edtavContrato_ano_to3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Contrato_Ano_To3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Contrato_Ano_To3), 4, 0)));
            }
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV24DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATO_VALOR");
               GX_FocusControl = edtavTfcontrato_valor_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV67TFContrato_Valor = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContrato_Valor", StringUtil.LTrim( StringUtil.Str( AV67TFContrato_Valor, 18, 5)));
            }
            else
            {
               AV67TFContrato_Valor = context.localUtil.CToN( cgiGet( edtavTfcontrato_valor_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContrato_Valor", StringUtil.LTrim( StringUtil.Str( AV67TFContrato_Valor, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_valor_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_valor_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATO_VALOR_TO");
               GX_FocusControl = edtavTfcontrato_valor_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV68TFContrato_Valor_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFContrato_Valor_To", StringUtil.LTrim( StringUtil.Str( AV68TFContrato_Valor_To, 18, 5)));
            }
            else
            {
               AV68TFContrato_Valor_To = context.localUtil.CToN( cgiGet( edtavTfcontrato_valor_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFContrato_Valor_To", StringUtil.LTrim( StringUtil.Str( AV68TFContrato_Valor_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATO_ATIVO_SEL");
               GX_FocusControl = edtavTfcontrato_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV71TFContrato_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContrato_Ativo_Sel", StringUtil.Str( (decimal)(AV71TFContrato_Ativo_Sel), 1, 0));
            }
            else
            {
               AV71TFContrato_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfcontrato_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContrato_Ativo_Sel", StringUtil.Str( (decimal)(AV71TFContrato_Ativo_Sel), 1, 0));
            }
            AV69ddo_Contrato_ValorTitleControlIdToReplace = cgiGet( edtavDdo_contrato_valortitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_Contrato_ValorTitleControlIdToReplace", AV69ddo_Contrato_ValorTitleControlIdToReplace);
            AV72ddo_Contrato_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_contrato_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Contrato_AtivoTitleControlIdToReplace", AV72ddo_Contrato_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_113 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_113"), ",", "."));
            AV75GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV76GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contrato_valor_Caption = cgiGet( "DDO_CONTRATO_VALOR_Caption");
            Ddo_contrato_valor_Tooltip = cgiGet( "DDO_CONTRATO_VALOR_Tooltip");
            Ddo_contrato_valor_Cls = cgiGet( "DDO_CONTRATO_VALOR_Cls");
            Ddo_contrato_valor_Filteredtext_set = cgiGet( "DDO_CONTRATO_VALOR_Filteredtext_set");
            Ddo_contrato_valor_Filteredtextto_set = cgiGet( "DDO_CONTRATO_VALOR_Filteredtextto_set");
            Ddo_contrato_valor_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_VALOR_Dropdownoptionstype");
            Ddo_contrato_valor_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_VALOR_Titlecontrolidtoreplace");
            Ddo_contrato_valor_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_VALOR_Includesortasc"));
            Ddo_contrato_valor_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_VALOR_Includesortdsc"));
            Ddo_contrato_valor_Sortedstatus = cgiGet( "DDO_CONTRATO_VALOR_Sortedstatus");
            Ddo_contrato_valor_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_VALOR_Includefilter"));
            Ddo_contrato_valor_Filtertype = cgiGet( "DDO_CONTRATO_VALOR_Filtertype");
            Ddo_contrato_valor_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_VALOR_Filterisrange"));
            Ddo_contrato_valor_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_VALOR_Includedatalist"));
            Ddo_contrato_valor_Sortasc = cgiGet( "DDO_CONTRATO_VALOR_Sortasc");
            Ddo_contrato_valor_Sortdsc = cgiGet( "DDO_CONTRATO_VALOR_Sortdsc");
            Ddo_contrato_valor_Cleanfilter = cgiGet( "DDO_CONTRATO_VALOR_Cleanfilter");
            Ddo_contrato_valor_Rangefilterfrom = cgiGet( "DDO_CONTRATO_VALOR_Rangefilterfrom");
            Ddo_contrato_valor_Rangefilterto = cgiGet( "DDO_CONTRATO_VALOR_Rangefilterto");
            Ddo_contrato_valor_Searchbuttontext = cgiGet( "DDO_CONTRATO_VALOR_Searchbuttontext");
            Ddo_contrato_ativo_Caption = cgiGet( "DDO_CONTRATO_ATIVO_Caption");
            Ddo_contrato_ativo_Tooltip = cgiGet( "DDO_CONTRATO_ATIVO_Tooltip");
            Ddo_contrato_ativo_Cls = cgiGet( "DDO_CONTRATO_ATIVO_Cls");
            Ddo_contrato_ativo_Selectedvalue_set = cgiGet( "DDO_CONTRATO_ATIVO_Selectedvalue_set");
            Ddo_contrato_ativo_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_ATIVO_Dropdownoptionstype");
            Ddo_contrato_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_ATIVO_Titlecontrolidtoreplace");
            Ddo_contrato_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_ATIVO_Includesortasc"));
            Ddo_contrato_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_ATIVO_Includesortdsc"));
            Ddo_contrato_ativo_Sortedstatus = cgiGet( "DDO_CONTRATO_ATIVO_Sortedstatus");
            Ddo_contrato_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_ATIVO_Includefilter"));
            Ddo_contrato_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_ATIVO_Includedatalist"));
            Ddo_contrato_ativo_Datalisttype = cgiGet( "DDO_CONTRATO_ATIVO_Datalisttype");
            Ddo_contrato_ativo_Datalistfixedvalues = cgiGet( "DDO_CONTRATO_ATIVO_Datalistfixedvalues");
            Ddo_contrato_ativo_Sortasc = cgiGet( "DDO_CONTRATO_ATIVO_Sortasc");
            Ddo_contrato_ativo_Sortdsc = cgiGet( "DDO_CONTRATO_ATIVO_Sortdsc");
            Ddo_contrato_ativo_Cleanfilter = cgiGet( "DDO_CONTRATO_ATIVO_Cleanfilter");
            Ddo_contrato_ativo_Searchbuttontext = cgiGet( "DDO_CONTRATO_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contrato_valor_Activeeventkey = cgiGet( "DDO_CONTRATO_VALOR_Activeeventkey");
            Ddo_contrato_valor_Filteredtext_get = cgiGet( "DDO_CONTRATO_VALOR_Filteredtext_get");
            Ddo_contrato_valor_Filteredtextto_get = cgiGet( "DDO_CONTRATO_VALOR_Filteredtextto_get");
            Ddo_contrato_ativo_Activeeventkey = cgiGet( "DDO_CONTRATO_ATIVO_Activeeventkey");
            Ddo_contrato_ativo_Selectedvalue_get = cgiGet( "DDO_CONTRATO_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO1"), AV39Contrato_Numero1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMEROATA1"), AV42Contrato_NumeroAta1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_PREPOSTONOM1"), AV77Contrato_PrepostoNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM1"), AV18Contratada_PessoaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO1"), ",", ".") != Convert.ToDecimal( AV36Contrato_Ano1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO_TO1"), ",", ".") != Convert.ToDecimal( AV33Contrato_Ano_To1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO2"), AV40Contrato_Numero2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMEROATA2"), AV43Contrato_NumeroAta2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_PREPOSTONOM2"), AV78Contrato_PrepostoNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM2"), AV23Contratada_PessoaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO2"), ",", ".") != Convert.ToDecimal( AV37Contrato_Ano2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO_TO2"), ",", ".") != Convert.ToDecimal( AV34Contrato_Ano_To2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO3"), AV41Contrato_Numero3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMEROATA3"), AV44Contrato_NumeroAta3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_PREPOSTONOM3"), AV79Contrato_PrepostoNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM3"), AV28Contratada_PessoaNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO3"), ",", ".") != Convert.ToDecimal( AV38Contrato_Ano3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO_TO3"), ",", ".") != Convert.ToDecimal( AV35Contrato_Ano_To3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_VALOR"), ",", ".") != AV67TFContrato_Valor )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_VALOR_TO"), ",", ".") != AV68TFContrato_Valor_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV71TFContrato_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E24662 */
         E24662 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24662( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTRATO_NUMERO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "CONTRATO_NUMERO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersSelector3 = "CONTRATO_NUMERO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontrato_valor_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_valor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_valor_Visible), 5, 0)));
         edtavTfcontrato_valor_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_valor_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_valor_to_Visible), 5, 0)));
         edtavTfcontrato_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_ativo_sel_Visible), 5, 0)));
         Ddo_contrato_valor_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Valor";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_valor_Internalname, "TitleControlIdToReplace", Ddo_contrato_valor_Titlecontrolidtoreplace);
         AV69ddo_Contrato_ValorTitleControlIdToReplace = Ddo_contrato_valor_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_Contrato_ValorTitleControlIdToReplace", AV69ddo_Contrato_ValorTitleControlIdToReplace);
         edtavDdo_contrato_valortitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_valortitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_valortitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contrato_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_ativo_Internalname, "TitleControlIdToReplace", Ddo_contrato_ativo_Titlecontrolidtoreplace);
         AV72ddo_Contrato_AtivoTitleControlIdToReplace = Ddo_contrato_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Contrato_AtivoTitleControlIdToReplace", AV72ddo_Contrato_AtivoTitleControlIdToReplace);
         edtavDdo_contrato_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione um Contrato";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "N� de Contrato", 0);
         cmbavOrderedby.addItem("2", "N�mero da Ata", 0);
         cmbavOrderedby.addItem("3", "Contratada", 0);
         cmbavOrderedby.addItem("4", "Ano", 0);
         cmbavOrderedby.addItem("5", "CNPJ", 0);
         cmbavOrderedby.addItem("6", "Valor", 0);
         cmbavOrderedby.addItem("7", "Ativo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV73DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV73DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E25662( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV66Contrato_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV70Contrato_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_PREPOSTONOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Cont�m", 0);
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_PREPOSTONOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "Cont�m", 0);
            }
            if ( AV24DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_PREPOSTONOM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "Cont�m", 0);
               }
            }
         }
         edtContrato_Numero_Titleformat = 2;
         edtContrato_Numero_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV13OrderedBy==1) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "N� Contrato", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Title", edtContrato_Numero_Title);
         edtContrato_NumeroAta_Titleformat = 2;
         edtContrato_NumeroAta_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV13OrderedBy==2) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "N� Ata", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_NumeroAta_Internalname, "Title", edtContrato_NumeroAta_Title);
         edtContrato_Ano_Titleformat = 2;
         edtContrato_Ano_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV13OrderedBy==4) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Ano", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Ano_Internalname, "Title", edtContrato_Ano_Title);
         edtContratada_PessoaNom_Titleformat = 2;
         edtContratada_PessoaNom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV13OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Contratada", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Title", edtContratada_PessoaNom_Title);
         edtContratada_PessoaCNPJ_Titleformat = 2;
         edtContratada_PessoaCNPJ_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV13OrderedBy==5) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "CNPJ", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCNPJ_Internalname, "Title", edtContratada_PessoaCNPJ_Title);
         edtContrato_Valor_Titleformat = 2;
         edtContrato_Valor_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Valor", AV69ddo_Contrato_ValorTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Valor_Internalname, "Title", edtContrato_Valor_Title);
         chkContrato_Ativo_Titleformat = 2;
         chkContrato_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV72ddo_Contrato_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContrato_Ativo_Internalname, "Title", chkContrato_Ativo.Title.Text);
         AV75GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75GridCurrentPage), 10, 0)));
         AV76GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV66Contrato_ValorTitleFilterData", AV66Contrato_ValorTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV70Contrato_AtivoTitleFilterData", AV70Contrato_AtivoTitleFilterData);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E11662( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV74PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV74PageToGo) ;
         }
      }

      protected void E12662( )
      {
         /* Ddo_contrato_valor_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_valor_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_valor_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_valor_Internalname, "SortedStatus", Ddo_contrato_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_valor_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_valor_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_valor_Internalname, "SortedStatus", Ddo_contrato_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_valor_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV67TFContrato_Valor = NumberUtil.Val( Ddo_contrato_valor_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContrato_Valor", StringUtil.LTrim( StringUtil.Str( AV67TFContrato_Valor, 18, 5)));
            AV68TFContrato_Valor_To = NumberUtil.Val( Ddo_contrato_valor_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFContrato_Valor_To", StringUtil.LTrim( StringUtil.Str( AV68TFContrato_Valor_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13662( )
      {
         /* Ddo_contrato_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_ativo_Internalname, "SortedStatus", Ddo_contrato_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_ativo_Internalname, "SortedStatus", Ddo_contrato_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV71TFContrato_Ativo_Sel = (short)(NumberUtil.Val( Ddo_contrato_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContrato_Ativo_Sel", StringUtil.Str( (decimal)(AV71TFContrato_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E26662( )
      {
         /* Grid_Load Routine */
         AV31Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV31Select);
         AV82Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 113;
         }
         sendrow_1132( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_113_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(113, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E27662 */
         E27662 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E27662( )
      {
         /* Enter Routine */
         AV7InOutContrato_Codigo = A74Contrato_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContrato_Codigo), 6, 0)));
         AV45InOutContrato_NumeroAta = A78Contrato_NumeroAta;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45InOutContrato_NumeroAta", AV45InOutContrato_NumeroAta);
         context.setWebReturnParms(new Object[] {(int)AV7InOutContrato_Codigo,(String)AV45InOutContrato_NumeroAta});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E14662( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E19662( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E15662( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV39Contrato_Numero1, AV42Contrato_NumeroAta1, AV77Contrato_PrepostoNom1, AV18Contratada_PessoaNom1, AV36Contrato_Ano1, AV33Contrato_Ano_To1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV40Contrato_Numero2, AV43Contrato_NumeroAta2, AV78Contrato_PrepostoNom2, AV23Contratada_PessoaNom2, AV37Contrato_Ano2, AV34Contrato_Ano_To2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV41Contrato_Numero3, AV44Contrato_NumeroAta3, AV79Contrato_PrepostoNom3, AV28Contratada_PessoaNom3, AV38Contrato_Ano3, AV35Contrato_Ano_To3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV67TFContrato_Valor, AV68TFContrato_Valor_To, AV71TFContrato_Ativo_Sel, AV69ddo_Contrato_ValorTitleControlIdToReplace, AV72ddo_Contrato_AtivoTitleControlIdToReplace) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E20662( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E21662( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV24DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E16662( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV39Contrato_Numero1, AV42Contrato_NumeroAta1, AV77Contrato_PrepostoNom1, AV18Contratada_PessoaNom1, AV36Contrato_Ano1, AV33Contrato_Ano_To1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV40Contrato_Numero2, AV43Contrato_NumeroAta2, AV78Contrato_PrepostoNom2, AV23Contratada_PessoaNom2, AV37Contrato_Ano2, AV34Contrato_Ano_To2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV41Contrato_Numero3, AV44Contrato_NumeroAta3, AV79Contrato_PrepostoNom3, AV28Contratada_PessoaNom3, AV38Contrato_Ano3, AV35Contrato_Ano_To3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV67TFContrato_Valor, AV68TFContrato_Valor_To, AV71TFContrato_Ativo_Sel, AV69ddo_Contrato_ValorTitleControlIdToReplace, AV72ddo_Contrato_AtivoTitleControlIdToReplace) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E22662( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E17662( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV39Contrato_Numero1, AV42Contrato_NumeroAta1, AV77Contrato_PrepostoNom1, AV18Contratada_PessoaNom1, AV36Contrato_Ano1, AV33Contrato_Ano_To1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV40Contrato_Numero2, AV43Contrato_NumeroAta2, AV78Contrato_PrepostoNom2, AV23Contratada_PessoaNom2, AV37Contrato_Ano2, AV34Contrato_Ano_To2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV41Contrato_Numero3, AV44Contrato_NumeroAta3, AV79Contrato_PrepostoNom3, AV28Contratada_PessoaNom3, AV38Contrato_Ano3, AV35Contrato_Ano_To3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV67TFContrato_Valor, AV68TFContrato_Valor_To, AV71TFContrato_Ativo_Sel, AV69ddo_Contrato_ValorTitleControlIdToReplace, AV72ddo_Contrato_AtivoTitleControlIdToReplace) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E23662( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV26DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E18662( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contrato_valor_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_valor_Internalname, "SortedStatus", Ddo_contrato_valor_Sortedstatus);
         Ddo_contrato_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_ativo_Internalname, "SortedStatus", Ddo_contrato_ativo_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 6 )
         {
            Ddo_contrato_valor_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_valor_Internalname, "SortedStatus", Ddo_contrato_valor_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_contrato_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_ativo_Internalname, "SortedStatus", Ddo_contrato_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContrato_numero1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numero1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numero1_Visible), 5, 0)));
         edtavContrato_numeroata1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numeroata1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numeroata1_Visible), 5, 0)));
         edtavContrato_prepostonom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_prepostonom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_prepostonom1_Visible), 5, 0)));
         edtavContratada_pessoanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom1_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontrato_ano1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontrato_ano1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_ano1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 )
         {
            edtavContrato_numero1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numero1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numero1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 )
         {
            edtavContrato_numeroata1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numeroata1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numeroata1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_PREPOSTONOM") == 0 )
         {
            edtavContrato_prepostonom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_prepostonom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_prepostonom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
         {
            edtavContratada_pessoanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_ANO") == 0 )
         {
            tblTablemergeddynamicfilterscontrato_ano1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontrato_ano1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_ano1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContrato_numero2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numero2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numero2_Visible), 5, 0)));
         edtavContrato_numeroata2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numeroata2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numeroata2_Visible), 5, 0)));
         edtavContrato_prepostonom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_prepostonom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_prepostonom2_Visible), 5, 0)));
         edtavContratada_pessoanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom2_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontrato_ano2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontrato_ano2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_ano2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 )
         {
            edtavContrato_numero2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numero2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numero2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 )
         {
            edtavContrato_numeroata2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numeroata2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numeroata2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_PREPOSTONOM") == 0 )
         {
            edtavContrato_prepostonom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_prepostonom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_prepostonom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
         {
            edtavContratada_pessoanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_ANO") == 0 )
         {
            tblTablemergeddynamicfilterscontrato_ano2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontrato_ano2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_ano2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContrato_numero3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numero3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numero3_Visible), 5, 0)));
         edtavContrato_numeroata3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numeroata3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numeroata3_Visible), 5, 0)));
         edtavContrato_prepostonom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_prepostonom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_prepostonom3_Visible), 5, 0)));
         edtavContratada_pessoanom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom3_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontrato_ano3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontrato_ano3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_ano3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 )
         {
            edtavContrato_numero3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numero3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numero3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 )
         {
            edtavContrato_numeroata3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numeroata3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numeroata3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_PREPOSTONOM") == 0 )
         {
            edtavContrato_prepostonom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_prepostonom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_prepostonom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 )
         {
            edtavContratada_pessoanom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_ANO") == 0 )
         {
            tblTablemergeddynamicfilterscontrato_ano3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontrato_ano3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_ano3_Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "CONTRATO_NUMERO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV40Contrato_Numero2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contrato_Numero2", AV40Contrato_Numero2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         AV25DynamicFiltersSelector3 = "CONTRATO_NUMERO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         AV41Contrato_Numero3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Contrato_Numero3", AV41Contrato_Numero3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'CLEANFILTERS' Routine */
         AV67TFContrato_Valor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContrato_Valor", StringUtil.LTrim( StringUtil.Str( AV67TFContrato_Valor, 18, 5)));
         Ddo_contrato_valor_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_valor_Internalname, "FilteredText_set", Ddo_contrato_valor_Filteredtext_set);
         AV68TFContrato_Valor_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFContrato_Valor_To", StringUtil.LTrim( StringUtil.Str( AV68TFContrato_Valor_To, 18, 5)));
         Ddo_contrato_valor_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_valor_Internalname, "FilteredTextTo_set", Ddo_contrato_valor_Filteredtextto_set);
         AV71TFContrato_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContrato_Ativo_Sel", StringUtil.Str( (decimal)(AV71TFContrato_Ativo_Sel), 1, 0));
         Ddo_contrato_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_ativo_Internalname, "SelectedValue_set", Ddo_contrato_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "CONTRATO_NUMERO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV39Contrato_Numero1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Contrato_Numero1", AV39Contrato_Numero1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 )
            {
               AV39Contrato_Numero1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Contrato_Numero1", AV39Contrato_Numero1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV42Contrato_NumeroAta1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contrato_NumeroAta1", AV42Contrato_NumeroAta1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_PREPOSTONOM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV77Contrato_PrepostoNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Contrato_PrepostoNom1", AV77Contrato_PrepostoNom1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV18Contratada_PessoaNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_PessoaNom1", AV18Contratada_PessoaNom1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_ANO") == 0 )
            {
               AV36Contrato_Ano1 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Contrato_Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Contrato_Ano1), 4, 0)));
               AV33Contrato_Ano_To1 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contrato_Ano_To1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Contrato_Ano_To1), 4, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 )
               {
                  AV40Contrato_Numero2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contrato_Numero2", AV40Contrato_Numero2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV43Contrato_NumeroAta2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Contrato_NumeroAta2", AV43Contrato_NumeroAta2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_PREPOSTONOM") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV78Contrato_PrepostoNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78Contrato_PrepostoNom2", AV78Contrato_PrepostoNom2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV23Contratada_PessoaNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratada_PessoaNom2", AV23Contratada_PessoaNom2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_ANO") == 0 )
               {
                  AV37Contrato_Ano2 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Contrato_Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37Contrato_Ano2), 4, 0)));
                  AV34Contrato_Ano_To2 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Valueto, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contrato_Ano_To2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contrato_Ano_To2), 4, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV24DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV25DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 )
                  {
                     AV41Contrato_Numero3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Contrato_Numero3", AV41Contrato_Numero3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV44Contrato_NumeroAta3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Contrato_NumeroAta3", AV44Contrato_NumeroAta3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_PREPOSTONOM") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV79Contrato_PrepostoNom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79Contrato_PrepostoNom3", AV79Contrato_PrepostoNom3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV28Contratada_PessoaNom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Contratada_PessoaNom3", AV28Contratada_PessoaNom3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_ANO") == 0 )
                  {
                     AV38Contrato_Ano3 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Contrato_Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Contrato_Ano3), 4, 0)));
                     AV35Contrato_Ano_To3 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Valueto, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Contrato_Ano_To3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Contrato_Ano_To3), 4, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Contrato_Numero1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV39Contrato_Numero1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Contrato_NumeroAta1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV42Contrato_NumeroAta1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_PREPOSTONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV77Contrato_PrepostoNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV77Contrato_PrepostoNom1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratada_PessoaNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18Contratada_PessoaNom1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ! ( (0==AV36Contrato_Ano1) && (0==AV33Contrato_Ano_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV36Contrato_Ano1), 4, 0);
               AV12GridStateDynamicFilter.gxTpr_Valueto = StringUtil.Str( (decimal)(AV33Contrato_Ano_To1), 4, 0);
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Contrato_Numero2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV40Contrato_Numero2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Contrato_NumeroAta2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV43Contrato_NumeroAta2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_PREPOSTONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV78Contrato_PrepostoNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV78Contrato_PrepostoNom2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Contratada_PessoaNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23Contratada_PessoaNom2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ! ( (0==AV37Contrato_Ano2) && (0==AV34Contrato_Ano_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV37Contrato_Ano2), 4, 0);
               AV12GridStateDynamicFilter.gxTpr_Valueto = StringUtil.Str( (decimal)(AV34Contrato_Ano_To2), 4, 0);
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV24DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Contrato_Numero3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV41Contrato_Numero3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Contrato_NumeroAta3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV44Contrato_NumeroAta3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_PREPOSTONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV79Contrato_PrepostoNom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV79Contrato_PrepostoNom3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Contratada_PessoaNom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV28Contratada_PessoaNom3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ! ( (0==AV38Contrato_Ano3) && (0==AV35Contrato_Ano_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV38Contrato_Ano3), 4, 0);
               AV12GridStateDynamicFilter.gxTpr_Valueto = StringUtil.Str( (decimal)(AV35Contrato_Ano_To3), 4, 0);
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_662( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_662( true) ;
         }
         else
         {
            wb_table2_5_662( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_662e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_107_662( true) ;
         }
         else
         {
            wb_table3_107_662( false) ;
         }
         return  ;
      }

      protected void wb_table3_107_662e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_662e( true) ;
         }
         else
         {
            wb_table1_2_662e( false) ;
         }
      }

      protected void wb_table3_107_662( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_110_662( true) ;
         }
         else
         {
            wb_table4_110_662( false) ;
         }
         return  ;
      }

      protected void wb_table4_110_662e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_107_662e( true) ;
         }
         else
         {
            wb_table3_107_662e( false) ;
         }
      }

      protected void wb_table4_110_662( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"113\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contrato") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�d. �rea Trabalho") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contratada") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(60), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_NumeroAta_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_NumeroAta_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_NumeroAta_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(34), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Ano_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Ano_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Ano_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaCNPJ_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaCNPJ_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaCNPJ_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(118), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Valor_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Valor_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Valor_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkContrato_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkContrato_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkContrato_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A77Contrato_Numero));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A78Contrato_NumeroAta));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_NumeroAta_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_NumeroAta_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Ano_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Ano_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A41Contratada_PessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A42Contratada_PessoaCNPJ);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaCNPJ_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaCNPJ_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A89Contrato_Valor, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Valor_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Valor_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A92Contrato_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkContrato_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkContrato_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 113 )
         {
            wbEnd = 0;
            nRC_GXsfl_113 = (short)(nGXsfl_113_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_110_662e( true) ;
         }
         else
         {
            wb_table4_110_662e( false) ;
         }
      }

      protected void wb_table2_5_662( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContrato.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_14_662( true) ;
         }
         else
         {
            wb_table5_14_662( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_662e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_662e( true) ;
         }
         else
         {
            wb_table2_5_662e( false) ;
         }
      }

      protected void wb_table5_14_662( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_662( true) ;
         }
         else
         {
            wb_table6_19_662( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_662e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_662e( true) ;
         }
         else
         {
            wb_table5_14_662e( false) ;
         }
      }

      protected void wb_table6_19_662( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContrato.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_662( true) ;
         }
         else
         {
            wb_table7_28_662( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_662e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContrato.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_PromptContrato.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_56_662( true) ;
         }
         else
         {
            wb_table8_56_662( false) ;
         }
         return  ;
      }

      protected void wb_table8_56_662e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContrato.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"", "", true, "HLP_PromptContrato.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_84_662( true) ;
         }
         else
         {
            wb_table9_84_662( false) ;
         }
         return  ;
      }

      protected void wb_table9_84_662e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_662e( true) ;
         }
         else
         {
            wb_table6_19_662e( false) ;
         }
      }

      protected void wb_table9_84_662( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,87);\"", "", true, "HLP_PromptContrato.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numero3_Internalname, StringUtil.RTrim( AV41Contrato_Numero3), StringUtil.RTrim( context.localUtil.Format( AV41Contrato_Numero3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numero3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_numero3_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numeroata3_Internalname, StringUtil.RTrim( AV44Contrato_NumeroAta3), StringUtil.RTrim( context.localUtil.Format( AV44Contrato_NumeroAta3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numeroata3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_numeroata3_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_prepostonom3_Internalname, StringUtil.RTrim( AV79Contrato_PrepostoNom3), StringUtil.RTrim( context.localUtil.Format( AV79Contrato_PrepostoNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_prepostonom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_prepostonom3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoanom3_Internalname, StringUtil.RTrim( AV28Contratada_PessoaNom3), StringUtil.RTrim( context.localUtil.Format( AV28Contratada_PessoaNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoanom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_pessoanom3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContrato.htm");
            wb_table10_93_662( true) ;
         }
         else
         {
            wb_table10_93_662( false) ;
         }
         return  ;
      }

      protected void wb_table10_93_662e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_84_662e( true) ;
         }
         else
         {
            wb_table9_84_662e( false) ;
         }
      }

      protected void wb_table10_93_662( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontrato_ano3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontrato_ano3_Internalname, tblTablemergeddynamicfilterscontrato_ano3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_ano3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38Contrato_Ano3), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV38Contrato_Ano3), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_ano3_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontrato_ano_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontrato_ano_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_ano_to3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35Contrato_Ano_To3), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35Contrato_Ano_To3), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_ano_to3_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_93_662e( true) ;
         }
         else
         {
            wb_table10_93_662e( false) ;
         }
      }

      protected void wb_table8_56_662( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_PromptContrato.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numero2_Internalname, StringUtil.RTrim( AV40Contrato_Numero2), StringUtil.RTrim( context.localUtil.Format( AV40Contrato_Numero2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numero2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_numero2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numeroata2_Internalname, StringUtil.RTrim( AV43Contrato_NumeroAta2), StringUtil.RTrim( context.localUtil.Format( AV43Contrato_NumeroAta2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numeroata2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_numeroata2_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_prepostonom2_Internalname, StringUtil.RTrim( AV78Contrato_PrepostoNom2), StringUtil.RTrim( context.localUtil.Format( AV78Contrato_PrepostoNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_prepostonom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_prepostonom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoanom2_Internalname, StringUtil.RTrim( AV23Contratada_PessoaNom2), StringUtil.RTrim( context.localUtil.Format( AV23Contratada_PessoaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_pessoanom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContrato.htm");
            wb_table11_65_662( true) ;
         }
         else
         {
            wb_table11_65_662( false) ;
         }
         return  ;
      }

      protected void wb_table11_65_662e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_56_662e( true) ;
         }
         else
         {
            wb_table8_56_662e( false) ;
         }
      }

      protected void wb_table11_65_662( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontrato_ano2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontrato_ano2_Internalname, tblTablemergeddynamicfilterscontrato_ano2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_ano2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37Contrato_Ano2), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV37Contrato_Ano2), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_ano2_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontrato_ano_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontrato_ano_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_ano_to2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34Contrato_Ano_To2), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV34Contrato_Ano_To2), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_ano_to2_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_65_662e( true) ;
         }
         else
         {
            wb_table11_65_662e( false) ;
         }
      }

      protected void wb_table7_28_662( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_113_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptContrato.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numero1_Internalname, StringUtil.RTrim( AV39Contrato_Numero1), StringUtil.RTrim( context.localUtil.Format( AV39Contrato_Numero1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numero1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_numero1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numeroata1_Internalname, StringUtil.RTrim( AV42Contrato_NumeroAta1), StringUtil.RTrim( context.localUtil.Format( AV42Contrato_NumeroAta1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numeroata1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_numeroata1_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_prepostonom1_Internalname, StringUtil.RTrim( AV77Contrato_PrepostoNom1), StringUtil.RTrim( context.localUtil.Format( AV77Contrato_PrepostoNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_prepostonom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_prepostonom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoanom1_Internalname, StringUtil.RTrim( AV18Contratada_PessoaNom1), StringUtil.RTrim( context.localUtil.Format( AV18Contratada_PessoaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_pessoanom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContrato.htm");
            wb_table12_37_662( true) ;
         }
         else
         {
            wb_table12_37_662( false) ;
         }
         return  ;
      }

      protected void wb_table12_37_662e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_662e( true) ;
         }
         else
         {
            wb_table7_28_662e( false) ;
         }
      }

      protected void wb_table12_37_662( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontrato_ano1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontrato_ano1_Internalname, tblTablemergeddynamicfilterscontrato_ano1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_ano1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36Contrato_Ano1), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36Contrato_Ano1), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_ano1_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontrato_ano_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontrato_ano_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_113_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_ano_to1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33Contrato_Ano_To1), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV33Contrato_Ano_To1), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_ano_to1_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_37_662e( true) ;
         }
         else
         {
            wb_table12_37_662e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContrato_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContrato_Codigo), 6, 0)));
         AV45InOutContrato_NumeroAta = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45InOutContrato_NumeroAta", AV45InOutContrato_NumeroAta);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA662( ) ;
         WS662( ) ;
         WE662( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031221123720");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontrato.js", "?202031221123721");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1132( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_113_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_113_idx;
         edtContrato_AreaTrabalhoCod_Internalname = "CONTRATO_AREATRABALHOCOD_"+sGXsfl_113_idx;
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO_"+sGXsfl_113_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_113_idx;
         edtContrato_NumeroAta_Internalname = "CONTRATO_NUMEROATA_"+sGXsfl_113_idx;
         edtContrato_Ano_Internalname = "CONTRATO_ANO_"+sGXsfl_113_idx;
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM_"+sGXsfl_113_idx;
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ_"+sGXsfl_113_idx;
         edtContrato_Valor_Internalname = "CONTRATO_VALOR_"+sGXsfl_113_idx;
         chkContrato_Ativo_Internalname = "CONTRATO_ATIVO_"+sGXsfl_113_idx;
      }

      protected void SubsflControlProps_fel_1132( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_113_fel_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_113_fel_idx;
         edtContrato_AreaTrabalhoCod_Internalname = "CONTRATO_AREATRABALHOCOD_"+sGXsfl_113_fel_idx;
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO_"+sGXsfl_113_fel_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_113_fel_idx;
         edtContrato_NumeroAta_Internalname = "CONTRATO_NUMEROATA_"+sGXsfl_113_fel_idx;
         edtContrato_Ano_Internalname = "CONTRATO_ANO_"+sGXsfl_113_fel_idx;
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM_"+sGXsfl_113_fel_idx;
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ_"+sGXsfl_113_fel_idx;
         edtContrato_Valor_Internalname = "CONTRATO_VALOR_"+sGXsfl_113_fel_idx;
         chkContrato_Ativo_Internalname = "CONTRATO_ATIVO_"+sGXsfl_113_fel_idx;
      }

      protected void sendrow_1132( )
      {
         SubsflControlProps_1132( ) ;
         WB660( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_113_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_113_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_113_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 114,'',false,'',113)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV31Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV82Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV82Select_GXI : context.PathToRelativeUrl( AV31Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_113_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV31Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_AreaTrabalhoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A75Contrato_AreaTrabalhoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_AreaTrabalhoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Numero_Internalname,StringUtil.RTrim( A77Contrato_Numero),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroContrato",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_NumeroAta_Internalname,StringUtil.RTrim( A78Contrato_NumeroAta),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_NumeroAta_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)60,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroAta",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Ano_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Ano_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)34,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)0,(bool)true,(String)"Ano",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaNom_Internalname,StringUtil.RTrim( A41Contratada_PessoaNom),StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaCNPJ_Internalname,(String)A42Contratada_PessoaCNPJ,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaCNPJ_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Valor_Internalname,StringUtil.LTrim( StringUtil.NToC( A89Contrato_Valor, 18, 5, ",", "")),context.localUtil.Format( A89Contrato_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Valor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)118,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)113,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkContrato_Ativo_Internalname,StringUtil.BoolToStr( A92Contrato_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_CODIGO"+"_"+sGXsfl_113_idx, GetSecureSignedToken( sGXsfl_113_idx, context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_AREATRABALHOCOD"+"_"+sGXsfl_113_idx, GetSecureSignedToken( sGXsfl_113_idx, context.localUtil.Format( (decimal)(A75Contrato_AreaTrabalhoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_CODIGO"+"_"+sGXsfl_113_idx, GetSecureSignedToken( sGXsfl_113_idx, context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_NUMERO"+"_"+sGXsfl_113_idx, GetSecureSignedToken( sGXsfl_113_idx, StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_NUMEROATA"+"_"+sGXsfl_113_idx, GetSecureSignedToken( sGXsfl_113_idx, StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_ANO"+"_"+sGXsfl_113_idx, GetSecureSignedToken( sGXsfl_113_idx, context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_VALOR"+"_"+sGXsfl_113_idx, GetSecureSignedToken( sGXsfl_113_idx, context.localUtil.Format( A89Contrato_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_ATIVO"+"_"+sGXsfl_113_idx, GetSecureSignedToken( sGXsfl_113_idx, A92Contrato_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_113_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_113_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_113_idx+1));
            sGXsfl_113_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_113_idx), 4, 0)), 4, "0");
            SubsflControlProps_1132( ) ;
         }
         /* End function sendrow_1132 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContrato_numero1_Internalname = "vCONTRATO_NUMERO1";
         edtavContrato_numeroata1_Internalname = "vCONTRATO_NUMEROATA1";
         edtavContrato_prepostonom1_Internalname = "vCONTRATO_PREPOSTONOM1";
         edtavContratada_pessoanom1_Internalname = "vCONTRATADA_PESSOANOM1";
         edtavContrato_ano1_Internalname = "vCONTRATO_ANO1";
         lblDynamicfilterscontrato_ano_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTRATO_ANO_RANGEMIDDLETEXT1";
         edtavContrato_ano_to1_Internalname = "vCONTRATO_ANO_TO1";
         tblTablemergeddynamicfilterscontrato_ano1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContrato_numero2_Internalname = "vCONTRATO_NUMERO2";
         edtavContrato_numeroata2_Internalname = "vCONTRATO_NUMEROATA2";
         edtavContrato_prepostonom2_Internalname = "vCONTRATO_PREPOSTONOM2";
         edtavContratada_pessoanom2_Internalname = "vCONTRATADA_PESSOANOM2";
         edtavContrato_ano2_Internalname = "vCONTRATO_ANO2";
         lblDynamicfilterscontrato_ano_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTRATO_ANO_RANGEMIDDLETEXT2";
         edtavContrato_ano_to2_Internalname = "vCONTRATO_ANO_TO2";
         tblTablemergeddynamicfilterscontrato_ano2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavContrato_numero3_Internalname = "vCONTRATO_NUMERO3";
         edtavContrato_numeroata3_Internalname = "vCONTRATO_NUMEROATA3";
         edtavContrato_prepostonom3_Internalname = "vCONTRATO_PREPOSTONOM3";
         edtavContratada_pessoanom3_Internalname = "vCONTRATADA_PESSOANOM3";
         edtavContrato_ano3_Internalname = "vCONTRATO_ANO3";
         lblDynamicfilterscontrato_ano_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTRATO_ANO_RANGEMIDDLETEXT3";
         edtavContrato_ano_to3_Internalname = "vCONTRATO_ANO_TO3";
         tblTablemergeddynamicfilterscontrato_ano3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO";
         edtContrato_AreaTrabalhoCod_Internalname = "CONTRATO_AREATRABALHOCOD";
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         edtContrato_NumeroAta_Internalname = "CONTRATO_NUMEROATA";
         edtContrato_Ano_Internalname = "CONTRATO_ANO";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ";
         edtContrato_Valor_Internalname = "CONTRATO_VALOR";
         chkContrato_Ativo_Internalname = "CONTRATO_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontrato_valor_Internalname = "vTFCONTRATO_VALOR";
         edtavTfcontrato_valor_to_Internalname = "vTFCONTRATO_VALOR_TO";
         edtavTfcontrato_ativo_sel_Internalname = "vTFCONTRATO_ATIVO_SEL";
         Ddo_contrato_valor_Internalname = "DDO_CONTRATO_VALOR";
         edtavDdo_contrato_valortitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_VALORTITLECONTROLIDTOREPLACE";
         Ddo_contrato_ativo_Internalname = "DDO_CONTRATO_ATIVO";
         edtavDdo_contrato_ativotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContrato_Valor_Jsonclick = "";
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaNom_Jsonclick = "";
         edtContrato_Ano_Jsonclick = "";
         edtContrato_NumeroAta_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtContratada_Codigo_Jsonclick = "";
         edtContrato_AreaTrabalhoCod_Jsonclick = "";
         edtContrato_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavContrato_ano_to1_Jsonclick = "";
         edtavContrato_ano1_Jsonclick = "";
         edtavContratada_pessoanom1_Jsonclick = "";
         edtavContrato_prepostonom1_Jsonclick = "";
         edtavContrato_numeroata1_Jsonclick = "";
         edtavContrato_numero1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContrato_ano_to2_Jsonclick = "";
         edtavContrato_ano2_Jsonclick = "";
         edtavContratada_pessoanom2_Jsonclick = "";
         edtavContrato_prepostonom2_Jsonclick = "";
         edtavContrato_numeroata2_Jsonclick = "";
         edtavContrato_numero2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContrato_ano_to3_Jsonclick = "";
         edtavContrato_ano3_Jsonclick = "";
         edtavContratada_pessoanom3_Jsonclick = "";
         edtavContrato_prepostonom3_Jsonclick = "";
         edtavContrato_numeroata3_Jsonclick = "";
         edtavContrato_numero3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         chkContrato_Ativo_Titleformat = 0;
         edtContrato_Valor_Titleformat = 0;
         edtContratada_PessoaCNPJ_Titleformat = 0;
         edtContratada_PessoaNom_Titleformat = 0;
         edtContrato_Ano_Titleformat = 0;
         edtContrato_NumeroAta_Titleformat = 0;
         edtContrato_Numero_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         tblTablemergeddynamicfilterscontrato_ano3_Visible = 1;
         edtavContratada_pessoanom3_Visible = 1;
         edtavContrato_prepostonom3_Visible = 1;
         edtavContrato_numeroata3_Visible = 1;
         edtavContrato_numero3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         tblTablemergeddynamicfilterscontrato_ano2_Visible = 1;
         edtavContratada_pessoanom2_Visible = 1;
         edtavContrato_prepostonom2_Visible = 1;
         edtavContrato_numeroata2_Visible = 1;
         edtavContrato_numero2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         tblTablemergeddynamicfilterscontrato_ano1_Visible = 1;
         edtavContratada_pessoanom1_Visible = 1;
         edtavContrato_prepostonom1_Visible = 1;
         edtavContrato_numeroata1_Visible = 1;
         edtavContrato_numero1_Visible = 1;
         chkContrato_Ativo.Title.Text = "Ativo";
         edtContrato_Valor_Title = "Valor";
         edtContratada_PessoaCNPJ_Title = "CNPJ";
         edtContratada_PessoaNom_Title = "Contratada";
         edtContrato_Ano_Title = "Ano";
         edtContrato_NumeroAta_Title = "N� Ata";
         edtContrato_Numero_Title = "N� Contrato";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkContrato_Ativo.Caption = "";
         edtavDdo_contrato_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_valortitlecontrolidtoreplace_Visible = 1;
         edtavTfcontrato_ativo_sel_Jsonclick = "";
         edtavTfcontrato_ativo_sel_Visible = 1;
         edtavTfcontrato_valor_to_Jsonclick = "";
         edtavTfcontrato_valor_to_Visible = 1;
         edtavTfcontrato_valor_Jsonclick = "";
         edtavTfcontrato_valor_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contrato_ativo_Searchbuttontext = "Pesquisar";
         Ddo_contrato_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_contrato_ativo_Datalisttype = "FixedValues";
         Ddo_contrato_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contrato_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_contrato_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_ativo_Titlecontrolidtoreplace = "";
         Ddo_contrato_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_ativo_Cls = "ColumnSettings";
         Ddo_contrato_ativo_Tooltip = "Op��es";
         Ddo_contrato_ativo_Caption = "";
         Ddo_contrato_valor_Searchbuttontext = "Pesquisar";
         Ddo_contrato_valor_Rangefilterto = "At�";
         Ddo_contrato_valor_Rangefilterfrom = "Desde";
         Ddo_contrato_valor_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_valor_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_valor_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_valor_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contrato_valor_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contrato_valor_Filtertype = "Numeric";
         Ddo_contrato_valor_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_valor_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_valor_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_valor_Titlecontrolidtoreplace = "";
         Ddo_contrato_valor_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_valor_Cls = "ColumnSettings";
         Ddo_contrato_valor_Tooltip = "Op��es";
         Ddo_contrato_valor_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Contrato";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV77Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV36Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV33Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV43Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV78Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV37Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV34Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV44Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV79Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV38Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV67TFContrato_Valor',fld:'vTFCONTRATO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFContrato_Valor_To',fld:'vTFCONTRATO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV71TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV69ddo_Contrato_ValorTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''}],oparms:[{av:'AV66Contrato_ValorTitleFilterData',fld:'vCONTRATO_VALORTITLEFILTERDATA',pic:'',nv:null},{av:'AV70Contrato_AtivoTitleFilterData',fld:'vCONTRATO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtContrato_Numero_Titleformat',ctrl:'CONTRATO_NUMERO',prop:'Titleformat'},{av:'edtContrato_Numero_Title',ctrl:'CONTRATO_NUMERO',prop:'Title'},{av:'edtContrato_NumeroAta_Titleformat',ctrl:'CONTRATO_NUMEROATA',prop:'Titleformat'},{av:'edtContrato_NumeroAta_Title',ctrl:'CONTRATO_NUMEROATA',prop:'Title'},{av:'edtContrato_Ano_Titleformat',ctrl:'CONTRATO_ANO',prop:'Titleformat'},{av:'edtContrato_Ano_Title',ctrl:'CONTRATO_ANO',prop:'Title'},{av:'edtContratada_PessoaNom_Titleformat',ctrl:'CONTRATADA_PESSOANOM',prop:'Titleformat'},{av:'edtContratada_PessoaNom_Title',ctrl:'CONTRATADA_PESSOANOM',prop:'Title'},{av:'edtContratada_PessoaCNPJ_Titleformat',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Titleformat'},{av:'edtContratada_PessoaCNPJ_Title',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Title'},{av:'edtContrato_Valor_Titleformat',ctrl:'CONTRATO_VALOR',prop:'Titleformat'},{av:'edtContrato_Valor_Title',ctrl:'CONTRATO_VALOR',prop:'Title'},{av:'chkContrato_Ativo_Titleformat',ctrl:'CONTRATO_ATIVO',prop:'Titleformat'},{av:'chkContrato_Ativo.Title.Text',ctrl:'CONTRATO_ATIVO',prop:'Title'},{av:'AV75GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV76GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11662',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV77Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV36Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV33Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV43Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV78Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV37Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV34Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV44Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV79Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV38Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV67TFContrato_Valor',fld:'vTFCONTRATO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFContrato_Valor_To',fld:'vTFCONTRATO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV71TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV69ddo_Contrato_ValorTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATO_VALOR.ONOPTIONCLICKED","{handler:'E12662',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV77Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV36Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV33Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV43Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV78Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV37Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV34Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV44Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV79Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV38Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV67TFContrato_Valor',fld:'vTFCONTRATO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFContrato_Valor_To',fld:'vTFCONTRATO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV71TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV69ddo_Contrato_ValorTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_contrato_valor_Activeeventkey',ctrl:'DDO_CONTRATO_VALOR',prop:'ActiveEventKey'},{av:'Ddo_contrato_valor_Filteredtext_get',ctrl:'DDO_CONTRATO_VALOR',prop:'FilteredText_get'},{av:'Ddo_contrato_valor_Filteredtextto_get',ctrl:'DDO_CONTRATO_VALOR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_valor_Sortedstatus',ctrl:'DDO_CONTRATO_VALOR',prop:'SortedStatus'},{av:'AV67TFContrato_Valor',fld:'vTFCONTRATO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFContrato_Valor_To',fld:'vTFCONTRATO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contrato_ativo_Sortedstatus',ctrl:'DDO_CONTRATO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATO_ATIVO.ONOPTIONCLICKED","{handler:'E13662',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV77Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV36Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV33Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV43Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV78Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV37Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV34Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV44Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV79Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV38Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV67TFContrato_Valor',fld:'vTFCONTRATO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFContrato_Valor_To',fld:'vTFCONTRATO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV71TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV69ddo_Contrato_ValorTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_contrato_ativo_Activeeventkey',ctrl:'DDO_CONTRATO_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_contrato_ativo_Selectedvalue_get',ctrl:'DDO_CONTRATO_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_ativo_Sortedstatus',ctrl:'DDO_CONTRATO_ATIVO',prop:'SortedStatus'},{av:'AV71TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_contrato_valor_Sortedstatus',ctrl:'DDO_CONTRATO_VALOR',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E26662',iparms:[],oparms:[{av:'AV31Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E27662',iparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A78Contrato_NumeroAta',fld:'CONTRATO_NUMEROATA',pic:'',hsh:true,nv:''}],oparms:[{av:'AV7InOutContrato_Codigo',fld:'vINOUTCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45InOutContrato_NumeroAta',fld:'vINOUTCONTRATO_NUMEROATA',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E14662',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV77Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV36Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV33Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV43Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV78Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV37Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV34Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV44Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV79Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV38Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV67TFContrato_Valor',fld:'vTFCONTRATO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFContrato_Valor_To',fld:'vTFCONTRATO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV71TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV69ddo_Contrato_ValorTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E19662',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E15662',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV77Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV36Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV33Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV43Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV78Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV37Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV34Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV44Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV79Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV38Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV67TFContrato_Valor',fld:'vTFCONTRATO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFContrato_Valor_To',fld:'vTFCONTRATO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV71TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV69ddo_Contrato_ValorTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV77Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV36Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV33Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV43Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV78Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV37Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV34Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV44Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV79Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV38Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'edtavContrato_numero2_Visible',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavContrato_numeroata2_Visible',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'edtavContrato_prepostonom2_Visible',ctrl:'vCONTRATO_PREPOSTONOM2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{av:'edtavContrato_numero3_Visible',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavContrato_numeroata3_Visible',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'edtavContrato_prepostonom3_Visible',ctrl:'vCONTRATO_PREPOSTONOM3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'},{av:'edtavContrato_numero1_Visible',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavContrato_numeroata1_Visible',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'edtavContrato_prepostonom1_Visible',ctrl:'vCONTRATO_PREPOSTONOM1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E20662',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavContrato_numero1_Visible',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavContrato_numeroata1_Visible',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'edtavContrato_prepostonom1_Visible',ctrl:'vCONTRATO_PREPOSTONOM1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E21662',iparms:[],oparms:[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E16662',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV77Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV36Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV33Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV43Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV78Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV37Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV34Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV44Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV79Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV38Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV67TFContrato_Valor',fld:'vTFCONTRATO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFContrato_Valor_To',fld:'vTFCONTRATO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV71TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV69ddo_Contrato_ValorTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV77Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV36Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV33Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV43Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV78Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV37Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV34Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV44Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV79Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV38Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'edtavContrato_numero2_Visible',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavContrato_numeroata2_Visible',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'edtavContrato_prepostonom2_Visible',ctrl:'vCONTRATO_PREPOSTONOM2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{av:'edtavContrato_numero3_Visible',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavContrato_numeroata3_Visible',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'edtavContrato_prepostonom3_Visible',ctrl:'vCONTRATO_PREPOSTONOM3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'},{av:'edtavContrato_numero1_Visible',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavContrato_numeroata1_Visible',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'edtavContrato_prepostonom1_Visible',ctrl:'vCONTRATO_PREPOSTONOM1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E22662',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavContrato_numero2_Visible',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavContrato_numeroata2_Visible',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'edtavContrato_prepostonom2_Visible',ctrl:'vCONTRATO_PREPOSTONOM2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E17662',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV77Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV36Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV33Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV43Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV78Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV37Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV34Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV44Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV79Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV38Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV67TFContrato_Valor',fld:'vTFCONTRATO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFContrato_Valor_To',fld:'vTFCONTRATO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV71TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV69ddo_Contrato_ValorTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV77Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV36Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV33Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV43Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV78Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV37Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV34Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV44Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV79Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV38Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'edtavContrato_numero2_Visible',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavContrato_numeroata2_Visible',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'edtavContrato_prepostonom2_Visible',ctrl:'vCONTRATO_PREPOSTONOM2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{av:'edtavContrato_numero3_Visible',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavContrato_numeroata3_Visible',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'edtavContrato_prepostonom3_Visible',ctrl:'vCONTRATO_PREPOSTONOM3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'},{av:'edtavContrato_numero1_Visible',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavContrato_numeroata1_Visible',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'edtavContrato_prepostonom1_Visible',ctrl:'vCONTRATO_PREPOSTONOM1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E23662',iparms:[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavContrato_numero3_Visible',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavContrato_numeroata3_Visible',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'edtavContrato_prepostonom3_Visible',ctrl:'vCONTRATO_PREPOSTONOM3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E18662',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV77Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV36Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV33Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV43Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV78Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV37Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV34Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV44Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV79Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV38Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV67TFContrato_Valor',fld:'vTFCONTRATO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFContrato_Valor_To',fld:'vTFCONTRATO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV71TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV69ddo_Contrato_ValorTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV67TFContrato_Valor',fld:'vTFCONTRATO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contrato_valor_Filteredtext_set',ctrl:'DDO_CONTRATO_VALOR',prop:'FilteredText_set'},{av:'AV68TFContrato_Valor_To',fld:'vTFCONTRATO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contrato_valor_Filteredtextto_set',ctrl:'DDO_CONTRATO_VALOR',prop:'FilteredTextTo_set'},{av:'AV71TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_contrato_ativo_Selectedvalue_set',ctrl:'DDO_CONTRATO_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContrato_numero1_Visible',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavContrato_numeroata1_Visible',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'edtavContrato_prepostonom1_Visible',ctrl:'vCONTRATO_PREPOSTONOM1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV77Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV36Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV33Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV43Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV78Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV37Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV34Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV44Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV79Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV38Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'edtavContrato_numero2_Visible',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavContrato_numeroata2_Visible',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'edtavContrato_prepostonom2_Visible',ctrl:'vCONTRATO_PREPOSTONOM2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{av:'edtavContrato_numero3_Visible',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavContrato_numeroata3_Visible',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'edtavContrato_prepostonom3_Visible',ctrl:'vCONTRATO_PREPOSTONOM3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV45InOutContrato_NumeroAta = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_contrato_valor_Activeeventkey = "";
         Ddo_contrato_valor_Filteredtext_get = "";
         Ddo_contrato_valor_Filteredtextto_get = "";
         Ddo_contrato_ativo_Activeeventkey = "";
         Ddo_contrato_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV39Contrato_Numero1 = "";
         AV42Contrato_NumeroAta1 = "";
         AV77Contrato_PrepostoNom1 = "";
         AV18Contratada_PessoaNom1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV40Contrato_Numero2 = "";
         AV43Contrato_NumeroAta2 = "";
         AV78Contrato_PrepostoNom2 = "";
         AV23Contratada_PessoaNom2 = "";
         AV25DynamicFiltersSelector3 = "";
         AV41Contrato_Numero3 = "";
         AV44Contrato_NumeroAta3 = "";
         AV79Contrato_PrepostoNom3 = "";
         AV28Contratada_PessoaNom3 = "";
         AV69ddo_Contrato_ValorTitleControlIdToReplace = "";
         AV72ddo_Contrato_AtivoTitleControlIdToReplace = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV73DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV66Contrato_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV70Contrato_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         Ddo_contrato_valor_Filteredtext_set = "";
         Ddo_contrato_valor_Filteredtextto_set = "";
         Ddo_contrato_valor_Sortedstatus = "";
         Ddo_contrato_ativo_Selectedvalue_set = "";
         Ddo_contrato_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Select = "";
         AV82Select_GXI = "";
         A77Contrato_Numero = "";
         A78Contrato_NumeroAta = "";
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV39Contrato_Numero1 = "";
         lV42Contrato_NumeroAta1 = "";
         lV77Contrato_PrepostoNom1 = "";
         lV18Contratada_PessoaNom1 = "";
         lV40Contrato_Numero2 = "";
         lV43Contrato_NumeroAta2 = "";
         lV78Contrato_PrepostoNom2 = "";
         lV23Contratada_PessoaNom2 = "";
         lV41Contrato_Numero3 = "";
         lV44Contrato_NumeroAta3 = "";
         lV79Contrato_PrepostoNom3 = "";
         lV28Contratada_PessoaNom3 = "";
         A1015Contrato_PrepostoNom = "";
         H00662_A40Contratada_PessoaCod = new int[1] ;
         H00662_A1013Contrato_PrepostoCod = new int[1] ;
         H00662_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00662_A1016Contrato_PrepostoPesCod = new int[1] ;
         H00662_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         H00662_A1015Contrato_PrepostoNom = new String[] {""} ;
         H00662_n1015Contrato_PrepostoNom = new bool[] {false} ;
         H00662_A92Contrato_Ativo = new bool[] {false} ;
         H00662_A89Contrato_Valor = new decimal[1] ;
         H00662_A42Contratada_PessoaCNPJ = new String[] {""} ;
         H00662_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         H00662_A41Contratada_PessoaNom = new String[] {""} ;
         H00662_n41Contratada_PessoaNom = new bool[] {false} ;
         H00662_A79Contrato_Ano = new short[1] ;
         H00662_A78Contrato_NumeroAta = new String[] {""} ;
         H00662_n78Contrato_NumeroAta = new bool[] {false} ;
         H00662_A77Contrato_Numero = new String[] {""} ;
         H00662_A39Contratada_Codigo = new int[1] ;
         H00662_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00662_A74Contrato_Codigo = new int[1] ;
         H00663_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterscontrato_ano_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontrato_ano_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontrato_ano_rangemiddletext1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontrato__default(),
            new Object[][] {
                new Object[] {
               H00662_A40Contratada_PessoaCod, H00662_A1013Contrato_PrepostoCod, H00662_n1013Contrato_PrepostoCod, H00662_A1016Contrato_PrepostoPesCod, H00662_n1016Contrato_PrepostoPesCod, H00662_A1015Contrato_PrepostoNom, H00662_n1015Contrato_PrepostoNom, H00662_A92Contrato_Ativo, H00662_A89Contrato_Valor, H00662_A42Contratada_PessoaCNPJ,
               H00662_n42Contratada_PessoaCNPJ, H00662_A41Contratada_PessoaNom, H00662_n41Contratada_PessoaNom, H00662_A79Contrato_Ano, H00662_A78Contrato_NumeroAta, H00662_n78Contrato_NumeroAta, H00662_A77Contrato_Numero, H00662_A39Contratada_Codigo, H00662_A75Contrato_AreaTrabalhoCod, H00662_A74Contrato_Codigo
               }
               , new Object[] {
               H00663_AGRID_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_113 ;
      private short nGXsfl_113_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV36Contrato_Ano1 ;
      private short AV33Contrato_Ano_To1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV37Contrato_Ano2 ;
      private short AV34Contrato_Ano_To2 ;
      private short AV26DynamicFiltersOperator3 ;
      private short AV38Contrato_Ano3 ;
      private short AV35Contrato_Ano_To3 ;
      private short AV71TFContrato_Ativo_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A79Contrato_Ano ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_113_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContrato_Numero_Titleformat ;
      private short edtContrato_NumeroAta_Titleformat ;
      private short edtContrato_Ano_Titleformat ;
      private short edtContratada_PessoaNom_Titleformat ;
      private short edtContratada_PessoaCNPJ_Titleformat ;
      private short edtContrato_Valor_Titleformat ;
      private short chkContrato_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContrato_Codigo ;
      private int wcpOAV7InOutContrato_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtavTfcontrato_valor_Visible ;
      private int edtavTfcontrato_valor_to_Visible ;
      private int edtavTfcontrato_ativo_sel_Visible ;
      private int edtavDdo_contrato_valortitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contrato_ativotitlecontrolidtoreplace_Visible ;
      private int A74Contrato_Codigo ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A39Contratada_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A40Contratada_PessoaCod ;
      private int A1013Contrato_PrepostoCod ;
      private int A1016Contrato_PrepostoPesCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV74PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContrato_numero1_Visible ;
      private int edtavContrato_numeroata1_Visible ;
      private int edtavContrato_prepostonom1_Visible ;
      private int edtavContratada_pessoanom1_Visible ;
      private int tblTablemergeddynamicfilterscontrato_ano1_Visible ;
      private int edtavContrato_numero2_Visible ;
      private int edtavContrato_numeroata2_Visible ;
      private int edtavContrato_prepostonom2_Visible ;
      private int edtavContratada_pessoanom2_Visible ;
      private int tblTablemergeddynamicfilterscontrato_ano2_Visible ;
      private int edtavContrato_numero3_Visible ;
      private int edtavContrato_numeroata3_Visible ;
      private int edtavContrato_prepostonom3_Visible ;
      private int edtavContratada_pessoanom3_Visible ;
      private int tblTablemergeddynamicfilterscontrato_ano3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV75GridCurrentPage ;
      private long AV76GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV67TFContrato_Valor ;
      private decimal AV68TFContrato_Valor_To ;
      private decimal A89Contrato_Valor ;
      private String AV45InOutContrato_NumeroAta ;
      private String wcpOAV45InOutContrato_NumeroAta ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contrato_valor_Activeeventkey ;
      private String Ddo_contrato_valor_Filteredtext_get ;
      private String Ddo_contrato_valor_Filteredtextto_get ;
      private String Ddo_contrato_ativo_Activeeventkey ;
      private String Ddo_contrato_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_113_idx="0001" ;
      private String AV39Contrato_Numero1 ;
      private String AV42Contrato_NumeroAta1 ;
      private String AV77Contrato_PrepostoNom1 ;
      private String AV18Contratada_PessoaNom1 ;
      private String AV40Contrato_Numero2 ;
      private String AV43Contrato_NumeroAta2 ;
      private String AV78Contrato_PrepostoNom2 ;
      private String AV23Contratada_PessoaNom2 ;
      private String AV41Contrato_Numero3 ;
      private String AV44Contrato_NumeroAta3 ;
      private String AV79Contrato_PrepostoNom3 ;
      private String AV28Contratada_PessoaNom3 ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contrato_valor_Caption ;
      private String Ddo_contrato_valor_Tooltip ;
      private String Ddo_contrato_valor_Cls ;
      private String Ddo_contrato_valor_Filteredtext_set ;
      private String Ddo_contrato_valor_Filteredtextto_set ;
      private String Ddo_contrato_valor_Dropdownoptionstype ;
      private String Ddo_contrato_valor_Titlecontrolidtoreplace ;
      private String Ddo_contrato_valor_Sortedstatus ;
      private String Ddo_contrato_valor_Filtertype ;
      private String Ddo_contrato_valor_Sortasc ;
      private String Ddo_contrato_valor_Sortdsc ;
      private String Ddo_contrato_valor_Cleanfilter ;
      private String Ddo_contrato_valor_Rangefilterfrom ;
      private String Ddo_contrato_valor_Rangefilterto ;
      private String Ddo_contrato_valor_Searchbuttontext ;
      private String Ddo_contrato_ativo_Caption ;
      private String Ddo_contrato_ativo_Tooltip ;
      private String Ddo_contrato_ativo_Cls ;
      private String Ddo_contrato_ativo_Selectedvalue_set ;
      private String Ddo_contrato_ativo_Dropdownoptionstype ;
      private String Ddo_contrato_ativo_Titlecontrolidtoreplace ;
      private String Ddo_contrato_ativo_Sortedstatus ;
      private String Ddo_contrato_ativo_Datalisttype ;
      private String Ddo_contrato_ativo_Datalistfixedvalues ;
      private String Ddo_contrato_ativo_Sortasc ;
      private String Ddo_contrato_ativo_Sortdsc ;
      private String Ddo_contrato_ativo_Cleanfilter ;
      private String Ddo_contrato_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontrato_valor_Internalname ;
      private String edtavTfcontrato_valor_Jsonclick ;
      private String edtavTfcontrato_valor_to_Internalname ;
      private String edtavTfcontrato_valor_to_Jsonclick ;
      private String edtavTfcontrato_ativo_sel_Internalname ;
      private String edtavTfcontrato_ativo_sel_Jsonclick ;
      private String edtavDdo_contrato_valortitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contrato_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContrato_Codigo_Internalname ;
      private String edtContrato_AreaTrabalhoCod_Internalname ;
      private String edtContratada_Codigo_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Internalname ;
      private String A78Contrato_NumeroAta ;
      private String edtContrato_NumeroAta_Internalname ;
      private String edtContrato_Ano_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Internalname ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String edtContrato_Valor_Internalname ;
      private String chkContrato_Ativo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV39Contrato_Numero1 ;
      private String lV42Contrato_NumeroAta1 ;
      private String lV77Contrato_PrepostoNom1 ;
      private String lV18Contratada_PessoaNom1 ;
      private String lV40Contrato_Numero2 ;
      private String lV43Contrato_NumeroAta2 ;
      private String lV78Contrato_PrepostoNom2 ;
      private String lV23Contratada_PessoaNom2 ;
      private String lV41Contrato_Numero3 ;
      private String lV44Contrato_NumeroAta3 ;
      private String lV79Contrato_PrepostoNom3 ;
      private String lV28Contratada_PessoaNom3 ;
      private String A1015Contrato_PrepostoNom ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContrato_numero1_Internalname ;
      private String edtavContrato_numeroata1_Internalname ;
      private String edtavContrato_prepostonom1_Internalname ;
      private String edtavContratada_pessoanom1_Internalname ;
      private String edtavContrato_ano1_Internalname ;
      private String edtavContrato_ano_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContrato_numero2_Internalname ;
      private String edtavContrato_numeroata2_Internalname ;
      private String edtavContrato_prepostonom2_Internalname ;
      private String edtavContratada_pessoanom2_Internalname ;
      private String edtavContrato_ano2_Internalname ;
      private String edtavContrato_ano_to2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContrato_numero3_Internalname ;
      private String edtavContrato_numeroata3_Internalname ;
      private String edtavContrato_prepostonom3_Internalname ;
      private String edtavContratada_pessoanom3_Internalname ;
      private String edtavContrato_ano3_Internalname ;
      private String edtavContrato_ano_to3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contrato_valor_Internalname ;
      private String Ddo_contrato_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContrato_Numero_Title ;
      private String edtContrato_NumeroAta_Title ;
      private String edtContrato_Ano_Title ;
      private String edtContratada_PessoaNom_Title ;
      private String edtContratada_PessoaCNPJ_Title ;
      private String edtContrato_Valor_Title ;
      private String edtavSelect_Tooltiptext ;
      private String tblTablemergeddynamicfilterscontrato_ano1_Internalname ;
      private String tblTablemergeddynamicfilterscontrato_ano2_Internalname ;
      private String tblTablemergeddynamicfilterscontrato_ano3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContrato_numero3_Jsonclick ;
      private String edtavContrato_numeroata3_Jsonclick ;
      private String edtavContrato_prepostonom3_Jsonclick ;
      private String edtavContratada_pessoanom3_Jsonclick ;
      private String edtavContrato_ano3_Jsonclick ;
      private String lblDynamicfilterscontrato_ano_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontrato_ano_rangemiddletext3_Jsonclick ;
      private String edtavContrato_ano_to3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContrato_numero2_Jsonclick ;
      private String edtavContrato_numeroata2_Jsonclick ;
      private String edtavContrato_prepostonom2_Jsonclick ;
      private String edtavContratada_pessoanom2_Jsonclick ;
      private String edtavContrato_ano2_Jsonclick ;
      private String lblDynamicfilterscontrato_ano_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontrato_ano_rangemiddletext2_Jsonclick ;
      private String edtavContrato_ano_to2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContrato_numero1_Jsonclick ;
      private String edtavContrato_numeroata1_Jsonclick ;
      private String edtavContrato_prepostonom1_Jsonclick ;
      private String edtavContratada_pessoanom1_Jsonclick ;
      private String edtavContrato_ano1_Jsonclick ;
      private String lblDynamicfilterscontrato_ano_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontrato_ano_rangemiddletext1_Jsonclick ;
      private String edtavContrato_ano_to1_Jsonclick ;
      private String sGXsfl_113_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContrato_Codigo_Jsonclick ;
      private String edtContrato_AreaTrabalhoCod_Jsonclick ;
      private String edtContratada_Codigo_Jsonclick ;
      private String edtContrato_Numero_Jsonclick ;
      private String edtContrato_NumeroAta_Jsonclick ;
      private String edtContrato_Ano_Jsonclick ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String edtContrato_Valor_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersEnabled3 ;
      private bool toggleJsOutput ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contrato_valor_Includesortasc ;
      private bool Ddo_contrato_valor_Includesortdsc ;
      private bool Ddo_contrato_valor_Includefilter ;
      private bool Ddo_contrato_valor_Filterisrange ;
      private bool Ddo_contrato_valor_Includedatalist ;
      private bool Ddo_contrato_ativo_Includesortasc ;
      private bool Ddo_contrato_ativo_Includesortdsc ;
      private bool Ddo_contrato_ativo_Includefilter ;
      private bool Ddo_contrato_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n78Contrato_NumeroAta ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool A92Contrato_Ativo ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n1016Contrato_PrepostoPesCod ;
      private bool n1015Contrato_PrepostoNom ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV25DynamicFiltersSelector3 ;
      private String AV69ddo_Contrato_ValorTitleControlIdToReplace ;
      private String AV72ddo_Contrato_AtivoTitleControlIdToReplace ;
      private String AV82Select_GXI ;
      private String A42Contratada_PessoaCNPJ ;
      private String AV31Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContrato_Codigo ;
      private String aP1_InOutContrato_NumeroAta ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkContrato_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00662_A40Contratada_PessoaCod ;
      private int[] H00662_A1013Contrato_PrepostoCod ;
      private bool[] H00662_n1013Contrato_PrepostoCod ;
      private int[] H00662_A1016Contrato_PrepostoPesCod ;
      private bool[] H00662_n1016Contrato_PrepostoPesCod ;
      private String[] H00662_A1015Contrato_PrepostoNom ;
      private bool[] H00662_n1015Contrato_PrepostoNom ;
      private bool[] H00662_A92Contrato_Ativo ;
      private decimal[] H00662_A89Contrato_Valor ;
      private String[] H00662_A42Contratada_PessoaCNPJ ;
      private bool[] H00662_n42Contratada_PessoaCNPJ ;
      private String[] H00662_A41Contratada_PessoaNom ;
      private bool[] H00662_n41Contratada_PessoaNom ;
      private short[] H00662_A79Contrato_Ano ;
      private String[] H00662_A78Contrato_NumeroAta ;
      private bool[] H00662_n78Contrato_NumeroAta ;
      private String[] H00662_A77Contrato_Numero ;
      private int[] H00662_A39Contratada_Codigo ;
      private int[] H00662_A75Contrato_AreaTrabalhoCod ;
      private int[] H00662_A74Contrato_Codigo ;
      private long[] H00663_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV66Contrato_ValorTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV70Contrato_AtivoTitleFilterData ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV73DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontrato__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00662( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV39Contrato_Numero1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV42Contrato_NumeroAta1 ,
                                             String AV77Contrato_PrepostoNom1 ,
                                             String AV18Contratada_PessoaNom1 ,
                                             short AV36Contrato_Ano1 ,
                                             short AV33Contrato_Ano_To1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             String AV40Contrato_Numero2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV43Contrato_NumeroAta2 ,
                                             String AV78Contrato_PrepostoNom2 ,
                                             String AV23Contratada_PessoaNom2 ,
                                             short AV37Contrato_Ano2 ,
                                             short AV34Contrato_Ano_To2 ,
                                             bool AV24DynamicFiltersEnabled3 ,
                                             String AV25DynamicFiltersSelector3 ,
                                             String AV41Contrato_Numero3 ,
                                             short AV26DynamicFiltersOperator3 ,
                                             String AV44Contrato_NumeroAta3 ,
                                             String AV79Contrato_PrepostoNom3 ,
                                             String AV28Contratada_PessoaNom3 ,
                                             short AV38Contrato_Ano3 ,
                                             short AV35Contrato_Ano_To3 ,
                                             decimal AV67TFContrato_Valor ,
                                             decimal AV68TFContrato_Valor_To ,
                                             short AV71TFContrato_Ativo_Sel ,
                                             String A77Contrato_Numero ,
                                             String A78Contrato_NumeroAta ,
                                             String A1015Contrato_PrepostoNom ,
                                             String A41Contratada_PessoaNom ,
                                             short A79Contrato_Ano ,
                                             decimal A89Contrato_Valor ,
                                             bool A92Contrato_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [34] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T2.[Usuario_PessoaCod] AS Contrato_PrepostoPesCod, T3.[Pessoa_Nome] AS Contrato_PrepostoNom, T1.[Contrato_Ativo], T1.[Contrato_Valor], T5.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T5.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contrato_Ano], T1.[Contrato_NumeroAta], T1.[Contrato_Numero], T1.[Contratada_Codigo], T1.[Contrato_AreaTrabalhoCod], T1.[Contrato_Codigo]";
         sFromString = " FROM (((([Contrato] T1 WITH (NOLOCK) LEFT JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Contrato_Numero1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV39Contrato_Numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV39Contrato_Numero1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Contrato_NumeroAta1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV42Contrato_NumeroAta1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV42Contrato_NumeroAta1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Contrato_NumeroAta1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV42Contrato_NumeroAta1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like '%' + @lV42Contrato_NumeroAta1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77Contrato_PrepostoNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV77Contrato_PrepostoNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV77Contrato_PrepostoNom1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77Contrato_PrepostoNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV77Contrato_PrepostoNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV77Contrato_PrepostoNom1)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratada_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV18Contratada_PessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV18Contratada_PessoaNom1)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratada_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV18Contratada_PessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV18Contratada_PessoaNom1)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV36Contrato_Ano1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV36Contrato_Ano1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV36Contrato_Ano1)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV33Contrato_Ano_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV33Contrato_Ano_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV33Contrato_Ano_To1)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Contrato_Numero2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV40Contrato_Numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV40Contrato_Numero2)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 0 ) || ( AV21DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Contrato_NumeroAta2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV43Contrato_NumeroAta2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV43Contrato_NumeroAta2)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 1 ) || ( AV21DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Contrato_NumeroAta2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV43Contrato_NumeroAta2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like '%' + @lV43Contrato_NumeroAta2)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78Contrato_PrepostoNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV78Contrato_PrepostoNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV78Contrato_PrepostoNom2)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78Contrato_PrepostoNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV78Contrato_PrepostoNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV78Contrato_PrepostoNom2)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 0 ) || ( AV21DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Contratada_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV23Contratada_PessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV23Contratada_PessoaNom2)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 1 ) || ( AV21DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Contratada_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV23Contratada_PessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV23Contratada_PessoaNom2)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV37Contrato_Ano2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV37Contrato_Ano2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV37Contrato_Ano2)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV34Contrato_Ano_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV34Contrato_Ano_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV34Contrato_Ano_To2)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Contrato_Numero3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV41Contrato_Numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV41Contrato_Numero3)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 0 ) || ( AV26DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Contrato_NumeroAta3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV44Contrato_NumeroAta3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV44Contrato_NumeroAta3)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 1 ) || ( AV26DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Contrato_NumeroAta3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV44Contrato_NumeroAta3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like '%' + @lV44Contrato_NumeroAta3)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV26DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79Contrato_PrepostoNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV79Contrato_PrepostoNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV79Contrato_PrepostoNom3)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV26DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79Contrato_PrepostoNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV79Contrato_PrepostoNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV79Contrato_PrepostoNom3)";
            }
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 0 ) || ( AV26DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Contratada_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV28Contratada_PessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV28Contratada_PessoaNom3)";
            }
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 1 ) || ( AV26DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Contratada_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV28Contratada_PessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV28Contratada_PessoaNom3)";
            }
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV38Contrato_Ano3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV38Contrato_Ano3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV38Contrato_Ano3)";
            }
         }
         else
         {
            GXv_int2[25] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV35Contrato_Ano_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV35Contrato_Ano_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV35Contrato_Ano_To3)";
            }
         }
         else
         {
            GXv_int2[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV67TFContrato_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Valor] >= @AV67TFContrato_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Valor] >= @AV67TFContrato_Valor)";
            }
         }
         else
         {
            GXv_int2[27] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV68TFContrato_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Valor] <= @AV68TFContrato_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Valor] <= @AV68TFContrato_Valor_To)";
            }
         }
         else
         {
            GXv_int2[28] = 1;
         }
         if ( AV71TFContrato_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ativo] = 1)";
            }
         }
         if ( AV71TFContrato_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Numero]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_NumeroAta]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_NumeroAta] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Ano]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Ano] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Pessoa_Docto]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Pessoa_Docto] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Valor]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Valor] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Ativo]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00663( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV39Contrato_Numero1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV42Contrato_NumeroAta1 ,
                                             String AV77Contrato_PrepostoNom1 ,
                                             String AV18Contratada_PessoaNom1 ,
                                             short AV36Contrato_Ano1 ,
                                             short AV33Contrato_Ano_To1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             String AV40Contrato_Numero2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV43Contrato_NumeroAta2 ,
                                             String AV78Contrato_PrepostoNom2 ,
                                             String AV23Contratada_PessoaNom2 ,
                                             short AV37Contrato_Ano2 ,
                                             short AV34Contrato_Ano_To2 ,
                                             bool AV24DynamicFiltersEnabled3 ,
                                             String AV25DynamicFiltersSelector3 ,
                                             String AV41Contrato_Numero3 ,
                                             short AV26DynamicFiltersOperator3 ,
                                             String AV44Contrato_NumeroAta3 ,
                                             String AV79Contrato_PrepostoNom3 ,
                                             String AV28Contratada_PessoaNom3 ,
                                             short AV38Contrato_Ano3 ,
                                             short AV35Contrato_Ano_To3 ,
                                             decimal AV67TFContrato_Valor ,
                                             decimal AV68TFContrato_Valor_To ,
                                             short AV71TFContrato_Ativo_Sel ,
                                             String A77Contrato_Numero ,
                                             String A78Contrato_NumeroAta ,
                                             String A1015Contrato_PrepostoNom ,
                                             String A41Contratada_PessoaNom ,
                                             short A79Contrato_Ano ,
                                             decimal A89Contrato_Valor ,
                                             bool A92Contrato_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [29] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (((([Contrato] T1 WITH (NOLOCK) LEFT JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Contrato_Numero1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV39Contrato_Numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV39Contrato_Numero1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Contrato_NumeroAta1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV42Contrato_NumeroAta1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV42Contrato_NumeroAta1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Contrato_NumeroAta1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV42Contrato_NumeroAta1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like '%' + @lV42Contrato_NumeroAta1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77Contrato_PrepostoNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV77Contrato_PrepostoNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV77Contrato_PrepostoNom1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77Contrato_PrepostoNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV77Contrato_PrepostoNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV77Contrato_PrepostoNom1)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 0 ) || ( AV16DynamicFiltersOperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratada_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV18Contratada_PessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV18Contratada_PessoaNom1)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ( AV16DynamicFiltersOperator1 == 1 ) || ( AV16DynamicFiltersOperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratada_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV18Contratada_PessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV18Contratada_PessoaNom1)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV36Contrato_Ano1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV36Contrato_Ano1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV36Contrato_Ano1)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV33Contrato_Ano_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV33Contrato_Ano_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV33Contrato_Ano_To1)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Contrato_Numero2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV40Contrato_Numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV40Contrato_Numero2)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 0 ) || ( AV21DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Contrato_NumeroAta2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV43Contrato_NumeroAta2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV43Contrato_NumeroAta2)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 1 ) || ( AV21DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Contrato_NumeroAta2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV43Contrato_NumeroAta2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like '%' + @lV43Contrato_NumeroAta2)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78Contrato_PrepostoNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV78Contrato_PrepostoNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV78Contrato_PrepostoNom2)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78Contrato_PrepostoNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV78Contrato_PrepostoNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV78Contrato_PrepostoNom2)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 0 ) || ( AV21DynamicFiltersOperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Contratada_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV23Contratada_PessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV23Contratada_PessoaNom2)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ( AV21DynamicFiltersOperator2 == 1 ) || ( AV21DynamicFiltersOperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Contratada_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV23Contratada_PessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV23Contratada_PessoaNom2)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV37Contrato_Ano2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV37Contrato_Ano2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV37Contrato_Ano2)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV34Contrato_Ano_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV34Contrato_Ano_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV34Contrato_Ano_To2)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Contrato_Numero3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV41Contrato_Numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV41Contrato_Numero3)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 0 ) || ( AV26DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Contrato_NumeroAta3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV44Contrato_NumeroAta3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV44Contrato_NumeroAta3)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 1 ) || ( AV26DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Contrato_NumeroAta3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV44Contrato_NumeroAta3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like '%' + @lV44Contrato_NumeroAta3)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV26DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79Contrato_PrepostoNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV79Contrato_PrepostoNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV79Contrato_PrepostoNom3)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV26DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79Contrato_PrepostoNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV79Contrato_PrepostoNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV79Contrato_PrepostoNom3)";
            }
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 0 ) || ( AV26DynamicFiltersOperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Contratada_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV28Contratada_PessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV28Contratada_PessoaNom3)";
            }
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( ( AV26DynamicFiltersOperator3 == 1 ) || ( AV26DynamicFiltersOperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Contratada_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV28Contratada_PessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV28Contratada_PessoaNom3)";
            }
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV38Contrato_Ano3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV38Contrato_Ano3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV38Contrato_Ano3)";
            }
         }
         else
         {
            GXv_int4[25] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV35Contrato_Ano_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV35Contrato_Ano_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV35Contrato_Ano_To3)";
            }
         }
         else
         {
            GXv_int4[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV67TFContrato_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Valor] >= @AV67TFContrato_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Valor] >= @AV67TFContrato_Valor)";
            }
         }
         else
         {
            GXv_int4[27] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV68TFContrato_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Valor] <= @AV68TFContrato_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Valor] <= @AV68TFContrato_Valor_To)";
            }
         }
         else
         {
            GXv_int4[28] = 1;
         }
         if ( AV71TFContrato_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ativo] = 1)";
            }
         }
         if ( AV71TFContrato_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00662(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (short)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (short)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (short)dynConstraints[24] , (short)dynConstraints[25] , (decimal)dynConstraints[26] , (decimal)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (short)dynConstraints[33] , (decimal)dynConstraints[34] , (bool)dynConstraints[35] , (short)dynConstraints[36] , (bool)dynConstraints[37] );
               case 1 :
                     return conditional_H00663(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (short)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (short)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (short)dynConstraints[24] , (short)dynConstraints[25] , (decimal)dynConstraints[26] , (decimal)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (short)dynConstraints[33] , (decimal)dynConstraints[34] , (bool)dynConstraints[35] , (short)dynConstraints[36] , (bool)dynConstraints[37] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00662 ;
          prmH00662 = new Object[] {
          new Object[] {"@lV39Contrato_Numero1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV42Contrato_NumeroAta1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV42Contrato_NumeroAta1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV77Contrato_PrepostoNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV77Contrato_PrepostoNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV18Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV18Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV36Contrato_Ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV33Contrato_Ano_To1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV40Contrato_Numero2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV43Contrato_NumeroAta2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV43Contrato_NumeroAta2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV78Contrato_PrepostoNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV78Contrato_PrepostoNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV23Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV23Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV37Contrato_Ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV34Contrato_Ano_To2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV41Contrato_Numero3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV44Contrato_NumeroAta3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV44Contrato_NumeroAta3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV79Contrato_PrepostoNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV79Contrato_PrepostoNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV28Contratada_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV28Contratada_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV38Contrato_Ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV35Contrato_Ano_To3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV67TFContrato_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV68TFContrato_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00663 ;
          prmH00663 = new Object[] {
          new Object[] {"@lV39Contrato_Numero1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV42Contrato_NumeroAta1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV42Contrato_NumeroAta1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV77Contrato_PrepostoNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV77Contrato_PrepostoNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV18Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV18Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV36Contrato_Ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV33Contrato_Ano_To1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV40Contrato_Numero2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV43Contrato_NumeroAta2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV43Contrato_NumeroAta2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV78Contrato_PrepostoNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV78Contrato_PrepostoNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV23Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV23Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV37Contrato_Ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV34Contrato_Ano_To2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV41Contrato_Numero3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV44Contrato_NumeroAta3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV44Contrato_NumeroAta3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV79Contrato_PrepostoNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV79Contrato_PrepostoNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV28Contratada_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV28Contratada_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV38Contrato_Ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV35Contrato_Ano_To3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV67TFContrato_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV68TFContrato_Valor_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00662", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00662,11,0,true,false )
             ,new CursorDef("H00663", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00663,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((short[]) buf[13])[0] = rslt.getShort(9) ;
                ((String[]) buf[14])[0] = rslt.getString(10, 10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 20) ;
                ((int[]) buf[17])[0] = rslt.getInt(12) ;
                ((int[]) buf[18])[0] = rslt.getInt(13) ;
                ((int[]) buf[19])[0] = rslt.getInt(14) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[41]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[42]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[50]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[51]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[59]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[60]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[61]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[62]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[63]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[64]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[65]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[66]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[36]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[37]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[45]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[46]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[54]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[55]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[56]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[57]);
                }
                return;
       }
    }

 }

}
