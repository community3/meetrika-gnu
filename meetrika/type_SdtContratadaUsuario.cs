/*
               File: type_SdtContratadaUsuario
        Description: Contratada Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:16:7.52
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContratadaUsuario" )]
   [XmlType(TypeName =  "ContratadaUsuario" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtContratadaUsuario : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratadaUsuario( )
      {
         /* Constructor for serialization */
         gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc = "";
         gxTv_SdtContratadaUsuario_Usuario_nome = "";
         gxTv_SdtContratadaUsuario_Usuario_usergamguid = "";
         gxTv_SdtContratadaUsuario_Mode = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_Z = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_Z = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_Z = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_Z = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_Z = "";
         gxTv_SdtContratadaUsuario_Usuario_nome_Z = "";
         gxTv_SdtContratadaUsuario_Usuario_usergamguid_Z = "";
      }

      public SdtContratadaUsuario( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV66ContratadaUsuario_ContratadaCod ,
                        int AV69ContratadaUsuario_UsuarioCod )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV66ContratadaUsuario_ContratadaCod,(int)AV69ContratadaUsuario_UsuarioCod});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContratadaUsuario_ContratadaCod", typeof(int)}, new Object[]{"ContratadaUsuario_UsuarioCod", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContratadaUsuario");
         metadata.Set("BT", "ContratadaUsuario");
         metadata.Set("PK", "[ \"ContratadaUsuario_ContratadaCod\",\"ContratadaUsuario_UsuarioCod\" ]");
         metadata.Set("PKAssigned", "[ \"ContratadaUsuario_ContratadaCod\",\"ContratadaUsuario_UsuarioCod\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Contratada_Codigo\" ],\"FKMap\":[ \"ContratadaUsuario_ContratadaCod-Contratada_Codigo\" ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"ContratadaUsuario_UsuarioCod-Usuario_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_contratadacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_areatrabalhocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_areatrabalhodes_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_contratadapessoacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_contratadapessoanom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_contratadapessoacnpj_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_usuariocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_usuariopessoacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_usuariopessoanom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_usuariopessoadoc_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_usuarioehcontratada_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_usuarioativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_usergamguid_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_cstuntprdnrm_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_cstuntprdext_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_tmpestanl_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_tmpestexc_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_tmpestcrr_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_areatrabalhocod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_areatrabalhodes_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_contratadapessoacod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_contratadapessoanom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_contratadapessoacnpj_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_usuariopessoacod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_usuariopessoanom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_usuariopessoadoc_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_usuarioehcontratada_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_usuarioativo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_nome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_usergamguid_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_cstuntprdnrm_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_cstuntprdext_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_tmpestanl_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_tmpestexc_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratadausuario_tmpestcrr_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContratadaUsuario deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContratadaUsuario)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContratadaUsuario obj ;
         obj = this;
         obj.gxTpr_Contratadausuario_contratadacod = deserialized.gxTpr_Contratadausuario_contratadacod;
         obj.gxTpr_Contratadausuario_areatrabalhocod = deserialized.gxTpr_Contratadausuario_areatrabalhocod;
         obj.gxTpr_Contratadausuario_areatrabalhodes = deserialized.gxTpr_Contratadausuario_areatrabalhodes;
         obj.gxTpr_Contratadausuario_contratadapessoacod = deserialized.gxTpr_Contratadausuario_contratadapessoacod;
         obj.gxTpr_Contratadausuario_contratadapessoanom = deserialized.gxTpr_Contratadausuario_contratadapessoanom;
         obj.gxTpr_Contratadausuario_contratadapessoacnpj = deserialized.gxTpr_Contratadausuario_contratadapessoacnpj;
         obj.gxTpr_Contratadausuario_usuariocod = deserialized.gxTpr_Contratadausuario_usuariocod;
         obj.gxTpr_Contratadausuario_usuariopessoacod = deserialized.gxTpr_Contratadausuario_usuariopessoacod;
         obj.gxTpr_Contratadausuario_usuariopessoanom = deserialized.gxTpr_Contratadausuario_usuariopessoanom;
         obj.gxTpr_Contratadausuario_usuariopessoadoc = deserialized.gxTpr_Contratadausuario_usuariopessoadoc;
         obj.gxTpr_Contratadausuario_usuarioehcontratada = deserialized.gxTpr_Contratadausuario_usuarioehcontratada;
         obj.gxTpr_Contratadausuario_usuarioativo = deserialized.gxTpr_Contratadausuario_usuarioativo;
         obj.gxTpr_Usuario_nome = deserialized.gxTpr_Usuario_nome;
         obj.gxTpr_Usuario_usergamguid = deserialized.gxTpr_Usuario_usergamguid;
         obj.gxTpr_Contratadausuario_cstuntprdnrm = deserialized.gxTpr_Contratadausuario_cstuntprdnrm;
         obj.gxTpr_Contratadausuario_cstuntprdext = deserialized.gxTpr_Contratadausuario_cstuntprdext;
         obj.gxTpr_Contratadausuario_tmpestanl = deserialized.gxTpr_Contratadausuario_tmpestanl;
         obj.gxTpr_Contratadausuario_tmpestexc = deserialized.gxTpr_Contratadausuario_tmpestexc;
         obj.gxTpr_Contratadausuario_tmpestcrr = deserialized.gxTpr_Contratadausuario_tmpestcrr;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contratadausuario_contratadacod_Z = deserialized.gxTpr_Contratadausuario_contratadacod_Z;
         obj.gxTpr_Contratadausuario_areatrabalhocod_Z = deserialized.gxTpr_Contratadausuario_areatrabalhocod_Z;
         obj.gxTpr_Contratadausuario_areatrabalhodes_Z = deserialized.gxTpr_Contratadausuario_areatrabalhodes_Z;
         obj.gxTpr_Contratadausuario_contratadapessoacod_Z = deserialized.gxTpr_Contratadausuario_contratadapessoacod_Z;
         obj.gxTpr_Contratadausuario_contratadapessoanom_Z = deserialized.gxTpr_Contratadausuario_contratadapessoanom_Z;
         obj.gxTpr_Contratadausuario_contratadapessoacnpj_Z = deserialized.gxTpr_Contratadausuario_contratadapessoacnpj_Z;
         obj.gxTpr_Contratadausuario_usuariocod_Z = deserialized.gxTpr_Contratadausuario_usuariocod_Z;
         obj.gxTpr_Contratadausuario_usuariopessoacod_Z = deserialized.gxTpr_Contratadausuario_usuariopessoacod_Z;
         obj.gxTpr_Contratadausuario_usuariopessoanom_Z = deserialized.gxTpr_Contratadausuario_usuariopessoanom_Z;
         obj.gxTpr_Contratadausuario_usuariopessoadoc_Z = deserialized.gxTpr_Contratadausuario_usuariopessoadoc_Z;
         obj.gxTpr_Contratadausuario_usuarioehcontratada_Z = deserialized.gxTpr_Contratadausuario_usuarioehcontratada_Z;
         obj.gxTpr_Contratadausuario_usuarioativo_Z = deserialized.gxTpr_Contratadausuario_usuarioativo_Z;
         obj.gxTpr_Usuario_nome_Z = deserialized.gxTpr_Usuario_nome_Z;
         obj.gxTpr_Usuario_usergamguid_Z = deserialized.gxTpr_Usuario_usergamguid_Z;
         obj.gxTpr_Contratadausuario_cstuntprdnrm_Z = deserialized.gxTpr_Contratadausuario_cstuntprdnrm_Z;
         obj.gxTpr_Contratadausuario_cstuntprdext_Z = deserialized.gxTpr_Contratadausuario_cstuntprdext_Z;
         obj.gxTpr_Contratadausuario_tmpestanl_Z = deserialized.gxTpr_Contratadausuario_tmpestanl_Z;
         obj.gxTpr_Contratadausuario_tmpestexc_Z = deserialized.gxTpr_Contratadausuario_tmpestexc_Z;
         obj.gxTpr_Contratadausuario_tmpestcrr_Z = deserialized.gxTpr_Contratadausuario_tmpestcrr_Z;
         obj.gxTpr_Contratadausuario_areatrabalhocod_N = deserialized.gxTpr_Contratadausuario_areatrabalhocod_N;
         obj.gxTpr_Contratadausuario_areatrabalhodes_N = deserialized.gxTpr_Contratadausuario_areatrabalhodes_N;
         obj.gxTpr_Contratadausuario_contratadapessoacod_N = deserialized.gxTpr_Contratadausuario_contratadapessoacod_N;
         obj.gxTpr_Contratadausuario_contratadapessoanom_N = deserialized.gxTpr_Contratadausuario_contratadapessoanom_N;
         obj.gxTpr_Contratadausuario_contratadapessoacnpj_N = deserialized.gxTpr_Contratadausuario_contratadapessoacnpj_N;
         obj.gxTpr_Contratadausuario_usuariopessoacod_N = deserialized.gxTpr_Contratadausuario_usuariopessoacod_N;
         obj.gxTpr_Contratadausuario_usuariopessoanom_N = deserialized.gxTpr_Contratadausuario_usuariopessoanom_N;
         obj.gxTpr_Contratadausuario_usuariopessoadoc_N = deserialized.gxTpr_Contratadausuario_usuariopessoadoc_N;
         obj.gxTpr_Contratadausuario_usuarioehcontratada_N = deserialized.gxTpr_Contratadausuario_usuarioehcontratada_N;
         obj.gxTpr_Contratadausuario_usuarioativo_N = deserialized.gxTpr_Contratadausuario_usuarioativo_N;
         obj.gxTpr_Usuario_nome_N = deserialized.gxTpr_Usuario_nome_N;
         obj.gxTpr_Usuario_usergamguid_N = deserialized.gxTpr_Usuario_usergamguid_N;
         obj.gxTpr_Contratadausuario_cstuntprdnrm_N = deserialized.gxTpr_Contratadausuario_cstuntprdnrm_N;
         obj.gxTpr_Contratadausuario_cstuntprdext_N = deserialized.gxTpr_Contratadausuario_cstuntprdext_N;
         obj.gxTpr_Contratadausuario_tmpestanl_N = deserialized.gxTpr_Contratadausuario_tmpestanl_N;
         obj.gxTpr_Contratadausuario_tmpestexc_N = deserialized.gxTpr_Contratadausuario_tmpestexc_N;
         obj.gxTpr_Contratadausuario_tmpestcrr_N = deserialized.gxTpr_Contratadausuario_tmpestcrr_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_ContratadaCod") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_contratadacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_AreaTrabalhoCod") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_AreaTrabalhoDes") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_ContratadaPessoaCod") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_ContratadaPessoaNom") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_ContratadaPessoaCNPJ") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_UsuarioCod") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_usuariocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_UsuarioPessoaCod") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_UsuarioPessoaNom") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_UsuarioPessoaDoc") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_UsuarioEhContratada") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_UsuarioAtivo") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Nome") )
               {
                  gxTv_SdtContratadaUsuario_Usuario_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_UserGamGuid") )
               {
                  gxTv_SdtContratadaUsuario_Usuario_usergamguid = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_CstUntPrdNrm") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_CstUntPrdExt") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_TmpEstAnl") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_TmpEstExc") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_TmpEstCrr") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContratadaUsuario_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContratadaUsuario_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_ContratadaCod_Z") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_contratadacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_AreaTrabalhoCod_Z") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_AreaTrabalhoDes_Z") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_ContratadaPessoaCod_Z") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_ContratadaPessoaNom_Z") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_ContratadaPessoaCNPJ_Z") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_UsuarioCod_Z") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_usuariocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_UsuarioPessoaCod_Z") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_UsuarioPessoaNom_Z") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_UsuarioPessoaDoc_Z") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_UsuarioEhContratada_Z") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_UsuarioAtivo_Z") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Nome_Z") )
               {
                  gxTv_SdtContratadaUsuario_Usuario_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_UserGamGuid_Z") )
               {
                  gxTv_SdtContratadaUsuario_Usuario_usergamguid_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_CstUntPrdNrm_Z") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_CstUntPrdExt_Z") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_TmpEstAnl_Z") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_TmpEstExc_Z") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_TmpEstCrr_Z") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_AreaTrabalhoCod_N") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_AreaTrabalhoDes_N") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_ContratadaPessoaCod_N") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_ContratadaPessoaNom_N") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_ContratadaPessoaCNPJ_N") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_UsuarioPessoaCod_N") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_UsuarioPessoaNom_N") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_UsuarioPessoaDoc_N") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_UsuarioEhContratada_N") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_UsuarioAtivo_N") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Nome_N") )
               {
                  gxTv_SdtContratadaUsuario_Usuario_nome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_UserGamGuid_N") )
               {
                  gxTv_SdtContratadaUsuario_Usuario_usergamguid_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_CstUntPrdNrm_N") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_CstUntPrdExt_N") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_TmpEstAnl_N") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_TmpEstExc_N") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaUsuario_TmpEstCrr_N") )
               {
                  gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContratadaUsuario";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContratadaUsuario_ContratadaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_contratadacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaUsuario_AreaTrabalhoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaUsuario_AreaTrabalhoDes", StringUtil.RTrim( gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaUsuario_ContratadaPessoaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaUsuario_ContratadaPessoaNom", StringUtil.RTrim( gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaUsuario_ContratadaPessoaCNPJ", StringUtil.RTrim( gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaUsuario_UsuarioCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_usuariocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaUsuario_UsuarioPessoaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaUsuario_UsuarioPessoaNom", StringUtil.RTrim( gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaUsuario_UsuarioPessoaDoc", StringUtil.RTrim( gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaUsuario_UsuarioEhContratada", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaUsuario_UsuarioAtivo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_Nome", StringUtil.RTrim( gxTv_SdtContratadaUsuario_Usuario_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_UserGamGuid", StringUtil.RTrim( gxTv_SdtContratadaUsuario_Usuario_usergamguid));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaUsuario_CstUntPrdNrm", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaUsuario_CstUntPrdExt", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaUsuario_TmpEstAnl", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaUsuario_TmpEstExc", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaUsuario_TmpEstCrr", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContratadaUsuario_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_ContratadaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_contratadacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_AreaTrabalhoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_AreaTrabalhoDes_Z", StringUtil.RTrim( gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_ContratadaPessoaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_ContratadaPessoaNom_Z", StringUtil.RTrim( gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_ContratadaPessoaCNPJ_Z", StringUtil.RTrim( gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_UsuarioCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_usuariocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_UsuarioPessoaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_UsuarioPessoaNom_Z", StringUtil.RTrim( gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_UsuarioPessoaDoc_Z", StringUtil.RTrim( gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_UsuarioEhContratada_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_UsuarioAtivo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Usuario_Nome_Z", StringUtil.RTrim( gxTv_SdtContratadaUsuario_Usuario_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Usuario_UserGamGuid_Z", StringUtil.RTrim( gxTv_SdtContratadaUsuario_Usuario_usergamguid_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_CstUntPrdNrm_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_Z, 18, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_CstUntPrdExt_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_Z, 18, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_TmpEstAnl_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_Z), 8, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_TmpEstExc_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_Z), 8, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_TmpEstCrr_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_Z), 8, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_AreaTrabalhoCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_AreaTrabalhoDes_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_ContratadaPessoaCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_ContratadaPessoaNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_ContratadaPessoaCNPJ_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_UsuarioPessoaCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_UsuarioPessoaNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_UsuarioPessoaDoc_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_UsuarioEhContratada_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_UsuarioAtivo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Usuario_Nome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Usuario_nome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Usuario_UserGamGuid_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Usuario_usergamguid_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_CstUntPrdNrm_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_CstUntPrdExt_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_TmpEstAnl_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_TmpEstExc_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratadaUsuario_TmpEstCrr_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContratadaUsuario_ContratadaCod", gxTv_SdtContratadaUsuario_Contratadausuario_contratadacod, false);
         AddObjectProperty("ContratadaUsuario_AreaTrabalhoCod", gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod, false);
         AddObjectProperty("ContratadaUsuario_AreaTrabalhoDes", gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes, false);
         AddObjectProperty("ContratadaUsuario_ContratadaPessoaCod", gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod, false);
         AddObjectProperty("ContratadaUsuario_ContratadaPessoaNom", gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom, false);
         AddObjectProperty("ContratadaUsuario_ContratadaPessoaCNPJ", gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj, false);
         AddObjectProperty("ContratadaUsuario_UsuarioCod", gxTv_SdtContratadaUsuario_Contratadausuario_usuariocod, false);
         AddObjectProperty("ContratadaUsuario_UsuarioPessoaCod", gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod, false);
         AddObjectProperty("ContratadaUsuario_UsuarioPessoaNom", gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom, false);
         AddObjectProperty("ContratadaUsuario_UsuarioPessoaDoc", gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc, false);
         AddObjectProperty("ContratadaUsuario_UsuarioEhContratada", gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada, false);
         AddObjectProperty("ContratadaUsuario_UsuarioAtivo", gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo, false);
         AddObjectProperty("Usuario_Nome", gxTv_SdtContratadaUsuario_Usuario_nome, false);
         AddObjectProperty("Usuario_UserGamGuid", gxTv_SdtContratadaUsuario_Usuario_usergamguid, false);
         AddObjectProperty("ContratadaUsuario_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm, 18, 5)), false);
         AddObjectProperty("ContratadaUsuario_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext, 18, 5)), false);
         AddObjectProperty("ContratadaUsuario_TmpEstAnl", gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl, false);
         AddObjectProperty("ContratadaUsuario_TmpEstExc", gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc, false);
         AddObjectProperty("ContratadaUsuario_TmpEstCrr", gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContratadaUsuario_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContratadaUsuario_Initialized, false);
            AddObjectProperty("ContratadaUsuario_ContratadaCod_Z", gxTv_SdtContratadaUsuario_Contratadausuario_contratadacod_Z, false);
            AddObjectProperty("ContratadaUsuario_AreaTrabalhoCod_Z", gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_Z, false);
            AddObjectProperty("ContratadaUsuario_AreaTrabalhoDes_Z", gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_Z, false);
            AddObjectProperty("ContratadaUsuario_ContratadaPessoaCod_Z", gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_Z, false);
            AddObjectProperty("ContratadaUsuario_ContratadaPessoaNom_Z", gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_Z, false);
            AddObjectProperty("ContratadaUsuario_ContratadaPessoaCNPJ_Z", gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_Z, false);
            AddObjectProperty("ContratadaUsuario_UsuarioCod_Z", gxTv_SdtContratadaUsuario_Contratadausuario_usuariocod_Z, false);
            AddObjectProperty("ContratadaUsuario_UsuarioPessoaCod_Z", gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_Z, false);
            AddObjectProperty("ContratadaUsuario_UsuarioPessoaNom_Z", gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_Z, false);
            AddObjectProperty("ContratadaUsuario_UsuarioPessoaDoc_Z", gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_Z, false);
            AddObjectProperty("ContratadaUsuario_UsuarioEhContratada_Z", gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_Z, false);
            AddObjectProperty("ContratadaUsuario_UsuarioAtivo_Z", gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_Z, false);
            AddObjectProperty("Usuario_Nome_Z", gxTv_SdtContratadaUsuario_Usuario_nome_Z, false);
            AddObjectProperty("Usuario_UserGamGuid_Z", gxTv_SdtContratadaUsuario_Usuario_usergamguid_Z, false);
            AddObjectProperty("ContratadaUsuario_CstUntPrdNrm_Z", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_Z, 18, 5)), false);
            AddObjectProperty("ContratadaUsuario_CstUntPrdExt_Z", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_Z, 18, 5)), false);
            AddObjectProperty("ContratadaUsuario_TmpEstAnl_Z", gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_Z, false);
            AddObjectProperty("ContratadaUsuario_TmpEstExc_Z", gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_Z, false);
            AddObjectProperty("ContratadaUsuario_TmpEstCrr_Z", gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_Z, false);
            AddObjectProperty("ContratadaUsuario_AreaTrabalhoCod_N", gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_N, false);
            AddObjectProperty("ContratadaUsuario_AreaTrabalhoDes_N", gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_N, false);
            AddObjectProperty("ContratadaUsuario_ContratadaPessoaCod_N", gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_N, false);
            AddObjectProperty("ContratadaUsuario_ContratadaPessoaNom_N", gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_N, false);
            AddObjectProperty("ContratadaUsuario_ContratadaPessoaCNPJ_N", gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_N, false);
            AddObjectProperty("ContratadaUsuario_UsuarioPessoaCod_N", gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_N, false);
            AddObjectProperty("ContratadaUsuario_UsuarioPessoaNom_N", gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_N, false);
            AddObjectProperty("ContratadaUsuario_UsuarioPessoaDoc_N", gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_N, false);
            AddObjectProperty("ContratadaUsuario_UsuarioEhContratada_N", gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_N, false);
            AddObjectProperty("ContratadaUsuario_UsuarioAtivo_N", gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_N, false);
            AddObjectProperty("Usuario_Nome_N", gxTv_SdtContratadaUsuario_Usuario_nome_N, false);
            AddObjectProperty("Usuario_UserGamGuid_N", gxTv_SdtContratadaUsuario_Usuario_usergamguid_N, false);
            AddObjectProperty("ContratadaUsuario_CstUntPrdNrm_N", gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_N, false);
            AddObjectProperty("ContratadaUsuario_CstUntPrdExt_N", gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_N, false);
            AddObjectProperty("ContratadaUsuario_TmpEstAnl_N", gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_N, false);
            AddObjectProperty("ContratadaUsuario_TmpEstExc_N", gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_N, false);
            AddObjectProperty("ContratadaUsuario_TmpEstCrr_N", gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_ContratadaCod" )]
      [  XmlElement( ElementName = "ContratadaUsuario_ContratadaCod"   )]
      public int gxTpr_Contratadausuario_contratadacod
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_contratadacod ;
         }

         set {
            if ( gxTv_SdtContratadaUsuario_Contratadausuario_contratadacod != value )
            {
               gxTv_SdtContratadaUsuario_Mode = "INS";
               this.gxTv_SdtContratadaUsuario_Contratadausuario_contratadacod_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_usuariocod_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Usuario_nome_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Usuario_usergamguid_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_Z_SetNull( );
            }
            gxTv_SdtContratadaUsuario_Contratadausuario_contratadacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratadaUsuario_AreaTrabalhoCod" )]
      [  XmlElement( ElementName = "ContratadaUsuario_AreaTrabalhoCod"   )]
      public int gxTpr_Contratadausuario_areatrabalhocod
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_N = 0;
            gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod = (int)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_N = 1;
         gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_AreaTrabalhoDes" )]
      [  XmlElement( ElementName = "ContratadaUsuario_AreaTrabalhoDes"   )]
      public String gxTpr_Contratadausuario_areatrabalhodes
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_N = 0;
            gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes = (String)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_N = 1;
         gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes = "";
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_ContratadaPessoaCod" )]
      [  XmlElement( ElementName = "ContratadaUsuario_ContratadaPessoaCod"   )]
      public int gxTpr_Contratadausuario_contratadapessoacod
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_N = 0;
            gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod = (int)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_N = 1;
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_ContratadaPessoaNom" )]
      [  XmlElement( ElementName = "ContratadaUsuario_ContratadaPessoaNom"   )]
      public String gxTpr_Contratadausuario_contratadapessoanom
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_N = 0;
            gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom = (String)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_N = 1;
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom = "";
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_ContratadaPessoaCNPJ" )]
      [  XmlElement( ElementName = "ContratadaUsuario_ContratadaPessoaCNPJ"   )]
      public String gxTpr_Contratadausuario_contratadapessoacnpj
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_N = 0;
            gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj = (String)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_N = 1;
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj = "";
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_UsuarioCod" )]
      [  XmlElement( ElementName = "ContratadaUsuario_UsuarioCod"   )]
      public int gxTpr_Contratadausuario_usuariocod
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_usuariocod ;
         }

         set {
            if ( gxTv_SdtContratadaUsuario_Contratadausuario_usuariocod != value )
            {
               gxTv_SdtContratadaUsuario_Mode = "INS";
               this.gxTv_SdtContratadaUsuario_Contratadausuario_contratadacod_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_usuariocod_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Usuario_nome_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Usuario_usergamguid_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_Z_SetNull( );
               this.gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_Z_SetNull( );
            }
            gxTv_SdtContratadaUsuario_Contratadausuario_usuariocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratadaUsuario_UsuarioPessoaCod" )]
      [  XmlElement( ElementName = "ContratadaUsuario_UsuarioPessoaCod"   )]
      public int gxTpr_Contratadausuario_usuariopessoacod
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_N = 0;
            gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod = (int)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_N = 1;
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_UsuarioPessoaNom" )]
      [  XmlElement( ElementName = "ContratadaUsuario_UsuarioPessoaNom"   )]
      public String gxTpr_Contratadausuario_usuariopessoanom
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_N = 0;
            gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom = (String)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_N = 1;
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom = "";
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_UsuarioPessoaDoc" )]
      [  XmlElement( ElementName = "ContratadaUsuario_UsuarioPessoaDoc"   )]
      public String gxTpr_Contratadausuario_usuariopessoadoc
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_N = 0;
            gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc = (String)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_N = 1;
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc = "";
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_UsuarioEhContratada" )]
      [  XmlElement( ElementName = "ContratadaUsuario_UsuarioEhContratada"   )]
      public bool gxTpr_Contratadausuario_usuarioehcontratada
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_N = 0;
            gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada = value;
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_N = 1;
         gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada = false;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_UsuarioAtivo" )]
      [  XmlElement( ElementName = "ContratadaUsuario_UsuarioAtivo"   )]
      public bool gxTpr_Contratadausuario_usuarioativo
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_N = 0;
            gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo = value;
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_N = 1;
         gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo = false;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Nome" )]
      [  XmlElement( ElementName = "Usuario_Nome"   )]
      public String gxTpr_Usuario_nome
      {
         get {
            return gxTv_SdtContratadaUsuario_Usuario_nome ;
         }

         set {
            gxTv_SdtContratadaUsuario_Usuario_nome_N = 0;
            gxTv_SdtContratadaUsuario_Usuario_nome = (String)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Usuario_nome_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Usuario_nome_N = 1;
         gxTv_SdtContratadaUsuario_Usuario_nome = "";
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Usuario_nome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_UserGamGuid" )]
      [  XmlElement( ElementName = "Usuario_UserGamGuid"   )]
      public String gxTpr_Usuario_usergamguid
      {
         get {
            return gxTv_SdtContratadaUsuario_Usuario_usergamguid ;
         }

         set {
            gxTv_SdtContratadaUsuario_Usuario_usergamguid_N = 0;
            gxTv_SdtContratadaUsuario_Usuario_usergamguid = (String)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Usuario_usergamguid_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Usuario_usergamguid_N = 1;
         gxTv_SdtContratadaUsuario_Usuario_usergamguid = "";
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Usuario_usergamguid_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_CstUntPrdNrm" )]
      [  XmlElement( ElementName = "ContratadaUsuario_CstUntPrdNrm"   )]
      public double gxTpr_Contratadausuario_cstuntprdnrm_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm) ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_N = 0;
            gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratadausuario_cstuntprdnrm
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_N = 0;
            gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm = (decimal)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_N = 1;
         gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_CstUntPrdExt" )]
      [  XmlElement( ElementName = "ContratadaUsuario_CstUntPrdExt"   )]
      public double gxTpr_Contratadausuario_cstuntprdext_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext) ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_N = 0;
            gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratadausuario_cstuntprdext
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_N = 0;
            gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext = (decimal)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_N = 1;
         gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_TmpEstAnl" )]
      [  XmlElement( ElementName = "ContratadaUsuario_TmpEstAnl"   )]
      public int gxTpr_Contratadausuario_tmpestanl
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_N = 0;
            gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl = (int)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_N = 1;
         gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_TmpEstExc" )]
      [  XmlElement( ElementName = "ContratadaUsuario_TmpEstExc"   )]
      public int gxTpr_Contratadausuario_tmpestexc
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_N = 0;
            gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc = (int)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_N = 1;
         gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_TmpEstCrr" )]
      [  XmlElement( ElementName = "ContratadaUsuario_TmpEstCrr"   )]
      public int gxTpr_Contratadausuario_tmpestcrr
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_N = 0;
            gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr = (int)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_N = 1;
         gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContratadaUsuario_Mode ;
         }

         set {
            gxTv_SdtContratadaUsuario_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Mode_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContratadaUsuario_Initialized ;
         }

         set {
            gxTv_SdtContratadaUsuario_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Initialized_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_ContratadaCod_Z" )]
      [  XmlElement( ElementName = "ContratadaUsuario_ContratadaCod_Z"   )]
      public int gxTpr_Contratadausuario_contratadacod_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_contratadacod_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_contratadacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_contratadacod_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_contratadacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_AreaTrabalhoCod_Z" )]
      [  XmlElement( ElementName = "ContratadaUsuario_AreaTrabalhoCod_Z"   )]
      public int gxTpr_Contratadausuario_areatrabalhocod_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_AreaTrabalhoDes_Z" )]
      [  XmlElement( ElementName = "ContratadaUsuario_AreaTrabalhoDes_Z"   )]
      public String gxTpr_Contratadausuario_areatrabalhodes_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_ContratadaPessoaCod_Z" )]
      [  XmlElement( ElementName = "ContratadaUsuario_ContratadaPessoaCod_Z"   )]
      public int gxTpr_Contratadausuario_contratadapessoacod_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_ContratadaPessoaNom_Z" )]
      [  XmlElement( ElementName = "ContratadaUsuario_ContratadaPessoaNom_Z"   )]
      public String gxTpr_Contratadausuario_contratadapessoanom_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_ContratadaPessoaCNPJ_Z" )]
      [  XmlElement( ElementName = "ContratadaUsuario_ContratadaPessoaCNPJ_Z"   )]
      public String gxTpr_Contratadausuario_contratadapessoacnpj_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_UsuarioCod_Z" )]
      [  XmlElement( ElementName = "ContratadaUsuario_UsuarioCod_Z"   )]
      public int gxTpr_Contratadausuario_usuariocod_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_usuariocod_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_usuariocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_usuariocod_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_usuariocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_UsuarioPessoaCod_Z" )]
      [  XmlElement( ElementName = "ContratadaUsuario_UsuarioPessoaCod_Z"   )]
      public int gxTpr_Contratadausuario_usuariopessoacod_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_UsuarioPessoaNom_Z" )]
      [  XmlElement( ElementName = "ContratadaUsuario_UsuarioPessoaNom_Z"   )]
      public String gxTpr_Contratadausuario_usuariopessoanom_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_UsuarioPessoaDoc_Z" )]
      [  XmlElement( ElementName = "ContratadaUsuario_UsuarioPessoaDoc_Z"   )]
      public String gxTpr_Contratadausuario_usuariopessoadoc_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_UsuarioEhContratada_Z" )]
      [  XmlElement( ElementName = "ContratadaUsuario_UsuarioEhContratada_Z"   )]
      public bool gxTpr_Contratadausuario_usuarioehcontratada_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_Z = value;
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_UsuarioAtivo_Z" )]
      [  XmlElement( ElementName = "ContratadaUsuario_UsuarioAtivo_Z"   )]
      public bool gxTpr_Contratadausuario_usuarioativo_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_Z = value;
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Nome_Z" )]
      [  XmlElement( ElementName = "Usuario_Nome_Z"   )]
      public String gxTpr_Usuario_nome_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Usuario_nome_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Usuario_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Usuario_nome_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Usuario_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Usuario_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_UserGamGuid_Z" )]
      [  XmlElement( ElementName = "Usuario_UserGamGuid_Z"   )]
      public String gxTpr_Usuario_usergamguid_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Usuario_usergamguid_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Usuario_usergamguid_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Usuario_usergamguid_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Usuario_usergamguid_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Usuario_usergamguid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_CstUntPrdNrm_Z" )]
      [  XmlElement( ElementName = "ContratadaUsuario_CstUntPrdNrm_Z"   )]
      public double gxTpr_Contratadausuario_cstuntprdnrm_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_Z) ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratadausuario_cstuntprdnrm_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_CstUntPrdExt_Z" )]
      [  XmlElement( ElementName = "ContratadaUsuario_CstUntPrdExt_Z"   )]
      public double gxTpr_Contratadausuario_cstuntprdext_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_Z) ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contratadausuario_cstuntprdext_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_TmpEstAnl_Z" )]
      [  XmlElement( ElementName = "ContratadaUsuario_TmpEstAnl_Z"   )]
      public int gxTpr_Contratadausuario_tmpestanl_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_TmpEstExc_Z" )]
      [  XmlElement( ElementName = "ContratadaUsuario_TmpEstExc_Z"   )]
      public int gxTpr_Contratadausuario_tmpestexc_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_TmpEstCrr_Z" )]
      [  XmlElement( ElementName = "ContratadaUsuario_TmpEstCrr_Z"   )]
      public int gxTpr_Contratadausuario_tmpestcrr_Z
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_Z ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_Z_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_AreaTrabalhoCod_N" )]
      [  XmlElement( ElementName = "ContratadaUsuario_AreaTrabalhoCod_N"   )]
      public short gxTpr_Contratadausuario_areatrabalhocod_N
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_N ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_N = (short)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_N_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_AreaTrabalhoDes_N" )]
      [  XmlElement( ElementName = "ContratadaUsuario_AreaTrabalhoDes_N"   )]
      public short gxTpr_Contratadausuario_areatrabalhodes_N
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_N ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_N = (short)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_N_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_ContratadaPessoaCod_N" )]
      [  XmlElement( ElementName = "ContratadaUsuario_ContratadaPessoaCod_N"   )]
      public short gxTpr_Contratadausuario_contratadapessoacod_N
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_N ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_N = (short)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_N_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_ContratadaPessoaNom_N" )]
      [  XmlElement( ElementName = "ContratadaUsuario_ContratadaPessoaNom_N"   )]
      public short gxTpr_Contratadausuario_contratadapessoanom_N
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_N ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_N = (short)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_N_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_ContratadaPessoaCNPJ_N" )]
      [  XmlElement( ElementName = "ContratadaUsuario_ContratadaPessoaCNPJ_N"   )]
      public short gxTpr_Contratadausuario_contratadapessoacnpj_N
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_N ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_N = (short)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_N_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_UsuarioPessoaCod_N" )]
      [  XmlElement( ElementName = "ContratadaUsuario_UsuarioPessoaCod_N"   )]
      public short gxTpr_Contratadausuario_usuariopessoacod_N
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_N ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_N = (short)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_N_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_UsuarioPessoaNom_N" )]
      [  XmlElement( ElementName = "ContratadaUsuario_UsuarioPessoaNom_N"   )]
      public short gxTpr_Contratadausuario_usuariopessoanom_N
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_N ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_N = (short)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_N_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_UsuarioPessoaDoc_N" )]
      [  XmlElement( ElementName = "ContratadaUsuario_UsuarioPessoaDoc_N"   )]
      public short gxTpr_Contratadausuario_usuariopessoadoc_N
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_N ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_N = (short)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_N_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_UsuarioEhContratada_N" )]
      [  XmlElement( ElementName = "ContratadaUsuario_UsuarioEhContratada_N"   )]
      public short gxTpr_Contratadausuario_usuarioehcontratada_N
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_N ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_N = (short)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_N_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_UsuarioAtivo_N" )]
      [  XmlElement( ElementName = "ContratadaUsuario_UsuarioAtivo_N"   )]
      public short gxTpr_Contratadausuario_usuarioativo_N
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_N ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_N = (short)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_N_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Nome_N" )]
      [  XmlElement( ElementName = "Usuario_Nome_N"   )]
      public short gxTpr_Usuario_nome_N
      {
         get {
            return gxTv_SdtContratadaUsuario_Usuario_nome_N ;
         }

         set {
            gxTv_SdtContratadaUsuario_Usuario_nome_N = (short)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Usuario_nome_N_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Usuario_nome_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Usuario_nome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_UserGamGuid_N" )]
      [  XmlElement( ElementName = "Usuario_UserGamGuid_N"   )]
      public short gxTpr_Usuario_usergamguid_N
      {
         get {
            return gxTv_SdtContratadaUsuario_Usuario_usergamguid_N ;
         }

         set {
            gxTv_SdtContratadaUsuario_Usuario_usergamguid_N = (short)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Usuario_usergamguid_N_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Usuario_usergamguid_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Usuario_usergamguid_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_CstUntPrdNrm_N" )]
      [  XmlElement( ElementName = "ContratadaUsuario_CstUntPrdNrm_N"   )]
      public short gxTpr_Contratadausuario_cstuntprdnrm_N
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_N ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_N = (short)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_N_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_CstUntPrdExt_N" )]
      [  XmlElement( ElementName = "ContratadaUsuario_CstUntPrdExt_N"   )]
      public short gxTpr_Contratadausuario_cstuntprdext_N
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_N ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_N = (short)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_N_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_TmpEstAnl_N" )]
      [  XmlElement( ElementName = "ContratadaUsuario_TmpEstAnl_N"   )]
      public short gxTpr_Contratadausuario_tmpestanl_N
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_N ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_N = (short)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_N_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_TmpEstExc_N" )]
      [  XmlElement( ElementName = "ContratadaUsuario_TmpEstExc_N"   )]
      public short gxTpr_Contratadausuario_tmpestexc_N
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_N ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_N = (short)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_N_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratadaUsuario_TmpEstCrr_N" )]
      [  XmlElement( ElementName = "ContratadaUsuario_TmpEstCrr_N"   )]
      public short gxTpr_Contratadausuario_tmpestcrr_N
      {
         get {
            return gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_N ;
         }

         set {
            gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_N = (short)(value);
         }

      }

      public void gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_N_SetNull( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc = "";
         gxTv_SdtContratadaUsuario_Usuario_nome = "";
         gxTv_SdtContratadaUsuario_Usuario_usergamguid = "";
         gxTv_SdtContratadaUsuario_Mode = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_Z = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_Z = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_Z = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_Z = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_Z = "";
         gxTv_SdtContratadaUsuario_Usuario_nome_Z = "";
         gxTv_SdtContratadaUsuario_Usuario_usergamguid_Z = "";
         gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo = true;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contratadausuario", "GeneXus.Programs.contratadausuario_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContratadaUsuario_Initialized ;
      private short gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_N ;
      private short gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_N ;
      private short gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_N ;
      private short gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_N ;
      private short gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_N ;
      private short gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_N ;
      private short gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_N ;
      private short gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_N ;
      private short gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_N ;
      private short gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_N ;
      private short gxTv_SdtContratadaUsuario_Usuario_nome_N ;
      private short gxTv_SdtContratadaUsuario_Usuario_usergamguid_N ;
      private short gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_N ;
      private short gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_N ;
      private short gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_N ;
      private short gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_N ;
      private short gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContratadaUsuario_Contratadausuario_contratadacod ;
      private int gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod ;
      private int gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod ;
      private int gxTv_SdtContratadaUsuario_Contratadausuario_usuariocod ;
      private int gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod ;
      private int gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl ;
      private int gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc ;
      private int gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr ;
      private int gxTv_SdtContratadaUsuario_Contratadausuario_contratadacod_Z ;
      private int gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhocod_Z ;
      private int gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacod_Z ;
      private int gxTv_SdtContratadaUsuario_Contratadausuario_usuariocod_Z ;
      private int gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoacod_Z ;
      private int gxTv_SdtContratadaUsuario_Contratadausuario_tmpestanl_Z ;
      private int gxTv_SdtContratadaUsuario_Contratadausuario_tmpestexc_Z ;
      private int gxTv_SdtContratadaUsuario_Contratadausuario_tmpestcrr_Z ;
      private decimal gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm ;
      private decimal gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext ;
      private decimal gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdnrm_Z ;
      private decimal gxTv_SdtContratadaUsuario_Contratadausuario_cstuntprdext_Z ;
      private String gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom ;
      private String gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom ;
      private String gxTv_SdtContratadaUsuario_Usuario_nome ;
      private String gxTv_SdtContratadaUsuario_Usuario_usergamguid ;
      private String gxTv_SdtContratadaUsuario_Mode ;
      private String gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoanom_Z ;
      private String gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoanom_Z ;
      private String gxTv_SdtContratadaUsuario_Usuario_nome_Z ;
      private String gxTv_SdtContratadaUsuario_Usuario_usergamguid_Z ;
      private String sTagName ;
      private bool gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada ;
      private bool gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo ;
      private bool gxTv_SdtContratadaUsuario_Contratadausuario_usuarioehcontratada_Z ;
      private bool gxTv_SdtContratadaUsuario_Contratadausuario_usuarioativo_Z ;
      private String gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes ;
      private String gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj ;
      private String gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc ;
      private String gxTv_SdtContratadaUsuario_Contratadausuario_areatrabalhodes_Z ;
      private String gxTv_SdtContratadaUsuario_Contratadausuario_contratadapessoacnpj_Z ;
      private String gxTv_SdtContratadaUsuario_Contratadausuario_usuariopessoadoc_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContratadaUsuario", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtContratadaUsuario_RESTInterface : GxGenericCollectionItem<SdtContratadaUsuario>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratadaUsuario_RESTInterface( ) : base()
      {
      }

      public SdtContratadaUsuario_RESTInterface( SdtContratadaUsuario psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContratadaUsuario_ContratadaCod" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratadausuario_contratadacod
      {
         get {
            return sdt.gxTpr_Contratadausuario_contratadacod ;
         }

         set {
            sdt.gxTpr_Contratadausuario_contratadacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratadaUsuario_AreaTrabalhoCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratadausuario_areatrabalhocod
      {
         get {
            return sdt.gxTpr_Contratadausuario_areatrabalhocod ;
         }

         set {
            sdt.gxTpr_Contratadausuario_areatrabalhocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratadaUsuario_AreaTrabalhoDes" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contratadausuario_areatrabalhodes
      {
         get {
            return sdt.gxTpr_Contratadausuario_areatrabalhodes ;
         }

         set {
            sdt.gxTpr_Contratadausuario_areatrabalhodes = (String)(value);
         }

      }

      [DataMember( Name = "ContratadaUsuario_ContratadaPessoaCod" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratadausuario_contratadapessoacod
      {
         get {
            return sdt.gxTpr_Contratadausuario_contratadapessoacod ;
         }

         set {
            sdt.gxTpr_Contratadausuario_contratadapessoacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratadaUsuario_ContratadaPessoaNom" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Contratadausuario_contratadapessoanom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratadausuario_contratadapessoanom) ;
         }

         set {
            sdt.gxTpr_Contratadausuario_contratadapessoanom = (String)(value);
         }

      }

      [DataMember( Name = "ContratadaUsuario_ContratadaPessoaCNPJ" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Contratadausuario_contratadapessoacnpj
      {
         get {
            return sdt.gxTpr_Contratadausuario_contratadapessoacnpj ;
         }

         set {
            sdt.gxTpr_Contratadausuario_contratadapessoacnpj = (String)(value);
         }

      }

      [DataMember( Name = "ContratadaUsuario_UsuarioCod" , Order = 6 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratadausuario_usuariocod
      {
         get {
            return sdt.gxTpr_Contratadausuario_usuariocod ;
         }

         set {
            sdt.gxTpr_Contratadausuario_usuariocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratadaUsuario_UsuarioPessoaCod" , Order = 7 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratadausuario_usuariopessoacod
      {
         get {
            return sdt.gxTpr_Contratadausuario_usuariopessoacod ;
         }

         set {
            sdt.gxTpr_Contratadausuario_usuariopessoacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratadaUsuario_UsuarioPessoaNom" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Contratadausuario_usuariopessoanom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratadausuario_usuariopessoanom) ;
         }

         set {
            sdt.gxTpr_Contratadausuario_usuariopessoanom = (String)(value);
         }

      }

      [DataMember( Name = "ContratadaUsuario_UsuarioPessoaDoc" , Order = 9 )]
      [GxSeudo()]
      public String gxTpr_Contratadausuario_usuariopessoadoc
      {
         get {
            return sdt.gxTpr_Contratadausuario_usuariopessoadoc ;
         }

         set {
            sdt.gxTpr_Contratadausuario_usuariopessoadoc = (String)(value);
         }

      }

      [DataMember( Name = "ContratadaUsuario_UsuarioEhContratada" , Order = 10 )]
      [GxSeudo()]
      public bool gxTpr_Contratadausuario_usuarioehcontratada
      {
         get {
            return sdt.gxTpr_Contratadausuario_usuarioehcontratada ;
         }

         set {
            sdt.gxTpr_Contratadausuario_usuarioehcontratada = value;
         }

      }

      [DataMember( Name = "ContratadaUsuario_UsuarioAtivo" , Order = 11 )]
      [GxSeudo()]
      public bool gxTpr_Contratadausuario_usuarioativo
      {
         get {
            return sdt.gxTpr_Contratadausuario_usuarioativo ;
         }

         set {
            sdt.gxTpr_Contratadausuario_usuarioativo = value;
         }

      }

      [DataMember( Name = "Usuario_Nome" , Order = 12 )]
      [GxSeudo()]
      public String gxTpr_Usuario_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_nome) ;
         }

         set {
            sdt.gxTpr_Usuario_nome = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_UserGamGuid" , Order = 13 )]
      [GxSeudo()]
      public String gxTpr_Usuario_usergamguid
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_usergamguid) ;
         }

         set {
            sdt.gxTpr_Usuario_usergamguid = (String)(value);
         }

      }

      [DataMember( Name = "ContratadaUsuario_CstUntPrdNrm" , Order = 14 )]
      [GxSeudo()]
      public String gxTpr_Contratadausuario_cstuntprdnrm
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contratadausuario_cstuntprdnrm, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Contratadausuario_cstuntprdnrm = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContratadaUsuario_CstUntPrdExt" , Order = 15 )]
      [GxSeudo()]
      public String gxTpr_Contratadausuario_cstuntprdext
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contratadausuario_cstuntprdext, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Contratadausuario_cstuntprdext = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContratadaUsuario_TmpEstAnl" , Order = 16 )]
      [GxSeudo()]
      public String gxTpr_Contratadausuario_tmpestanl
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contratadausuario_tmpestanl), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Contratadausuario_tmpestanl = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ContratadaUsuario_TmpEstExc" , Order = 17 )]
      [GxSeudo()]
      public String gxTpr_Contratadausuario_tmpestexc
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contratadausuario_tmpestexc), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Contratadausuario_tmpestexc = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ContratadaUsuario_TmpEstCrr" , Order = 18 )]
      [GxSeudo()]
      public String gxTpr_Contratadausuario_tmpestcrr
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contratadausuario_tmpestcrr), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Contratadausuario_tmpestcrr = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      public SdtContratadaUsuario sdt
      {
         get {
            return (SdtContratadaUsuario)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContratadaUsuario() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 57 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
