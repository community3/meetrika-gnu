/*
               File: Baseline
        Description: Baseline
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:22:42.34
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class baseline : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_5") == 0 )
         {
            A721Baseline_UserCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A721Baseline_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A721Baseline_UserCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_5( A721Baseline_UserCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
         {
            A192Contagem_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_4( A192Contagem_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_6") == 0 )
         {
            A735Baseline_ProjetoMelCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n735Baseline_ProjetoMelCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A735Baseline_ProjetoMelCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A735Baseline_ProjetoMelCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_6( A735Baseline_ProjetoMelCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Baseline", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtBaseline_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public baseline( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public baseline( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2E92( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2E92e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2E92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2E92( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2E92e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Baseline", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_Baseline.htm");
            wb_table3_28_2E92( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_2E92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2E92e( true) ;
         }
         else
         {
            wb_table1_2_2E92e( false) ;
         }
      }

      protected void wb_table3_28_2E92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_2E92( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_2E92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Baseline.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Baseline.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_2E92e( true) ;
         }
         else
         {
            wb_table3_28_2E92e( false) ;
         }
      }

      protected void wb_table4_34_2E92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockbaseline_codigo_Internalname, "Codigo", "", "", lblTextblockbaseline_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtBaseline_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A722Baseline_Codigo), 6, 0, ",", "")), ((edtBaseline_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A722Baseline_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A722Baseline_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtBaseline_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtBaseline_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockbaseline_datahomologacao_Internalname, "de Homologa��o", "", "", lblTextblockbaseline_datahomologacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtBaseline_DataHomologacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtBaseline_DataHomologacao_Internalname, context.localUtil.TToC( A1164Baseline_DataHomologacao, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1164Baseline_DataHomologacao, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtBaseline_DataHomologacao_Jsonclick, 0, "Attribute", "", "", "", 1, edtBaseline_DataHomologacao_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Baseline.htm");
            GxWebStd.gx_bitmap( context, edtBaseline_DataHomologacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtBaseline_DataHomologacao_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Baseline.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockbaseline_usercod_Internalname, "Usuario", "", "", lblTextblockbaseline_usercod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtBaseline_UserCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A721Baseline_UserCod), 6, 0, ",", "")), ((edtBaseline_UserCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A721Baseline_UserCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A721Baseline_UserCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtBaseline_UserCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtBaseline_UserCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_codigo_Internalname, "C�digo", "", "", lblTextblockcontagem_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ",", "")), ((edtContagem_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagem_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_sistemacod_Internalname, "Sistema", "", "", lblTextblockcontagem_sistemacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_SistemaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A940Contagem_SistemaCod), 6, 0, ",", "")), ((edtContagem_SistemaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A940Contagem_SistemaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A940Contagem_SistemaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_SistemaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagem_SistemaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockbaseline_dataatualizacao_Internalname, "de Atualiza��o", "", "", lblTextblockbaseline_dataatualizacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtBaseline_DataAtualizacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtBaseline_DataAtualizacao_Internalname, context.localUtil.TToC( A1165Baseline_DataAtualizacao, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1165Baseline_DataAtualizacao, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtBaseline_DataAtualizacao_Jsonclick, 0, "Attribute", "", "", "", 1, edtBaseline_DataAtualizacao_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Baseline.htm");
            GxWebStd.gx_bitmap( context, edtBaseline_DataAtualizacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtBaseline_DataAtualizacao_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Baseline.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockbaseline_pfbantes_Internalname, "Antes", "", "", lblTextblockbaseline_pfbantes_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtBaseline_PFBAntes_Internalname, StringUtil.LTrim( StringUtil.NToC( A726Baseline_PFBAntes, 14, 5, ",", "")), ((edtBaseline_PFBAntes_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A726Baseline_PFBAntes, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A726Baseline_PFBAntes, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtBaseline_PFBAntes_Jsonclick, 0, "Attribute", "", "", "", 1, edtBaseline_PFBAntes_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockbaseline_pfbdepois_Internalname, "Depois", "", "", lblTextblockbaseline_pfbdepois_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtBaseline_PFBDepois_Internalname, StringUtil.LTrim( StringUtil.NToC( A727Baseline_PFBDepois, 14, 5, ",", "")), ((edtBaseline_PFBDepois_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A727Baseline_PFBDepois, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A727Baseline_PFBDepois, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtBaseline_PFBDepois_Jsonclick, 0, "Attribute", "", "", "", 1, edtBaseline_PFBDepois_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockbaseline_projetomelcod_Internalname, "Projeto", "", "", lblTextblockbaseline_projetomelcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtBaseline_ProjetoMelCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A735Baseline_ProjetoMelCod), 6, 0, ",", "")), ((edtBaseline_ProjetoMelCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A735Baseline_ProjetoMelCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A735Baseline_ProjetoMelCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtBaseline_ProjetoMelCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtBaseline_ProjetoMelCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockbaseline_antes_Internalname, "do Update", "", "", lblTextblockbaseline_antes_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtBaseline_Antes_Internalname, A1161Baseline_Antes, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"", 0, 1, edtBaseline_Antes_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockbaseline_depois_Internalname, "do Update", "", "", lblTextblockbaseline_depois_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtBaseline_Depois_Internalname, A1162Baseline_Depois, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,89);\"", 0, 1, edtBaseline_Depois_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_Baseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_2E92e( true) ;
         }
         else
         {
            wb_table4_34_2E92e( false) ;
         }
      }

      protected void wb_table2_5_2E92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Baseline.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Baseline.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Baseline.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Baseline.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Baseline.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Baseline.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Baseline.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Baseline.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Baseline.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Baseline.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Baseline.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Baseline.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Baseline.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Baseline.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Baseline.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Baseline.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2E92e( true) ;
         }
         else
         {
            wb_table2_5_2E92e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtBaseline_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtBaseline_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "BASELINE_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtBaseline_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A722Baseline_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A722Baseline_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A722Baseline_Codigo), 6, 0)));
               }
               else
               {
                  A722Baseline_Codigo = (int)(context.localUtil.CToN( cgiGet( edtBaseline_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A722Baseline_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A722Baseline_Codigo), 6, 0)));
               }
               if ( context.localUtil.VCDateTime( cgiGet( edtBaseline_DataHomologacao_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data de Homologa��o"}), 1, "BASELINE_DATAHOMOLOGACAO");
                  AnyError = 1;
                  GX_FocusControl = edtBaseline_DataHomologacao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1164Baseline_DataHomologacao = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1164Baseline_DataHomologacao", context.localUtil.TToC( A1164Baseline_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1164Baseline_DataHomologacao = context.localUtil.CToT( cgiGet( edtBaseline_DataHomologacao_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1164Baseline_DataHomologacao", context.localUtil.TToC( A1164Baseline_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtBaseline_UserCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtBaseline_UserCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "BASELINE_USERCOD");
                  AnyError = 1;
                  GX_FocusControl = edtBaseline_UserCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A721Baseline_UserCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A721Baseline_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A721Baseline_UserCod), 6, 0)));
               }
               else
               {
                  A721Baseline_UserCod = (int)(context.localUtil.CToN( cgiGet( edtBaseline_UserCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A721Baseline_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A721Baseline_UserCod), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A192Contagem_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
               }
               else
               {
                  A192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagem_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
               }
               A940Contagem_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_SistemaCod_Internalname), ",", "."));
               n940Contagem_SistemaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
               if ( context.localUtil.VCDateTime( cgiGet( edtBaseline_DataAtualizacao_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data de Atualiza��o"}), 1, "BASELINE_DATAATUALIZACAO");
                  AnyError = 1;
                  GX_FocusControl = edtBaseline_DataAtualizacao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1165Baseline_DataAtualizacao = (DateTime)(DateTime.MinValue);
                  n1165Baseline_DataAtualizacao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1165Baseline_DataAtualizacao", context.localUtil.TToC( A1165Baseline_DataAtualizacao, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1165Baseline_DataAtualizacao = context.localUtil.CToT( cgiGet( edtBaseline_DataAtualizacao_Internalname));
                  n1165Baseline_DataAtualizacao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1165Baseline_DataAtualizacao", context.localUtil.TToC( A1165Baseline_DataAtualizacao, 8, 5, 0, 3, "/", ":", " "));
               }
               n1165Baseline_DataAtualizacao = ((DateTime.MinValue==A1165Baseline_DataAtualizacao) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtBaseline_PFBAntes_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtBaseline_PFBAntes_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "BASELINE_PFBANTES");
                  AnyError = 1;
                  GX_FocusControl = edtBaseline_PFBAntes_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A726Baseline_PFBAntes = 0;
                  n726Baseline_PFBAntes = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A726Baseline_PFBAntes", StringUtil.LTrim( StringUtil.Str( A726Baseline_PFBAntes, 14, 5)));
               }
               else
               {
                  A726Baseline_PFBAntes = context.localUtil.CToN( cgiGet( edtBaseline_PFBAntes_Internalname), ",", ".");
                  n726Baseline_PFBAntes = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A726Baseline_PFBAntes", StringUtil.LTrim( StringUtil.Str( A726Baseline_PFBAntes, 14, 5)));
               }
               n726Baseline_PFBAntes = ((Convert.ToDecimal(0)==A726Baseline_PFBAntes) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtBaseline_PFBDepois_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtBaseline_PFBDepois_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "BASELINE_PFBDEPOIS");
                  AnyError = 1;
                  GX_FocusControl = edtBaseline_PFBDepois_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A727Baseline_PFBDepois = 0;
                  n727Baseline_PFBDepois = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A727Baseline_PFBDepois", StringUtil.LTrim( StringUtil.Str( A727Baseline_PFBDepois, 14, 5)));
               }
               else
               {
                  A727Baseline_PFBDepois = context.localUtil.CToN( cgiGet( edtBaseline_PFBDepois_Internalname), ",", ".");
                  n727Baseline_PFBDepois = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A727Baseline_PFBDepois", StringUtil.LTrim( StringUtil.Str( A727Baseline_PFBDepois, 14, 5)));
               }
               n727Baseline_PFBDepois = ((Convert.ToDecimal(0)==A727Baseline_PFBDepois) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtBaseline_ProjetoMelCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtBaseline_ProjetoMelCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "BASELINE_PROJETOMELCOD");
                  AnyError = 1;
                  GX_FocusControl = edtBaseline_ProjetoMelCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A735Baseline_ProjetoMelCod = 0;
                  n735Baseline_ProjetoMelCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A735Baseline_ProjetoMelCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A735Baseline_ProjetoMelCod), 6, 0)));
               }
               else
               {
                  A735Baseline_ProjetoMelCod = (int)(context.localUtil.CToN( cgiGet( edtBaseline_ProjetoMelCod_Internalname), ",", "."));
                  n735Baseline_ProjetoMelCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A735Baseline_ProjetoMelCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A735Baseline_ProjetoMelCod), 6, 0)));
               }
               n735Baseline_ProjetoMelCod = ((0==A735Baseline_ProjetoMelCod) ? true : false);
               A1161Baseline_Antes = cgiGet( edtBaseline_Antes_Internalname);
               n1161Baseline_Antes = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1161Baseline_Antes", A1161Baseline_Antes);
               n1161Baseline_Antes = (String.IsNullOrEmpty(StringUtil.RTrim( A1161Baseline_Antes)) ? true : false);
               A1162Baseline_Depois = cgiGet( edtBaseline_Depois_Internalname);
               n1162Baseline_Depois = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1162Baseline_Depois", A1162Baseline_Depois);
               n1162Baseline_Depois = (String.IsNullOrEmpty(StringUtil.RTrim( A1162Baseline_Depois)) ? true : false);
               /* Read saved values. */
               Z722Baseline_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z722Baseline_Codigo"), ",", "."));
               Z1164Baseline_DataHomologacao = context.localUtil.CToT( cgiGet( "Z1164Baseline_DataHomologacao"), 0);
               Z1165Baseline_DataAtualizacao = context.localUtil.CToT( cgiGet( "Z1165Baseline_DataAtualizacao"), 0);
               n1165Baseline_DataAtualizacao = ((DateTime.MinValue==A1165Baseline_DataAtualizacao) ? true : false);
               Z726Baseline_PFBAntes = context.localUtil.CToN( cgiGet( "Z726Baseline_PFBAntes"), ",", ".");
               n726Baseline_PFBAntes = ((Convert.ToDecimal(0)==A726Baseline_PFBAntes) ? true : false);
               Z727Baseline_PFBDepois = context.localUtil.CToN( cgiGet( "Z727Baseline_PFBDepois"), ",", ".");
               n727Baseline_PFBDepois = ((Convert.ToDecimal(0)==A727Baseline_PFBDepois) ? true : false);
               Z192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z192Contagem_Codigo"), ",", "."));
               Z721Baseline_UserCod = (int)(context.localUtil.CToN( cgiGet( "Z721Baseline_UserCod"), ",", "."));
               Z735Baseline_ProjetoMelCod = (int)(context.localUtil.CToN( cgiGet( "Z735Baseline_ProjetoMelCod"), ",", "."));
               n735Baseline_ProjetoMelCod = ((0==A735Baseline_ProjetoMelCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A722Baseline_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A722Baseline_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A722Baseline_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2E92( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes2E92( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption2E0( )
      {
      }

      protected void ZM2E92( short GX_JID )
      {
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1164Baseline_DataHomologacao = T002E3_A1164Baseline_DataHomologacao[0];
               Z1165Baseline_DataAtualizacao = T002E3_A1165Baseline_DataAtualizacao[0];
               Z726Baseline_PFBAntes = T002E3_A726Baseline_PFBAntes[0];
               Z727Baseline_PFBDepois = T002E3_A727Baseline_PFBDepois[0];
               Z192Contagem_Codigo = T002E3_A192Contagem_Codigo[0];
               Z721Baseline_UserCod = T002E3_A721Baseline_UserCod[0];
               Z735Baseline_ProjetoMelCod = T002E3_A735Baseline_ProjetoMelCod[0];
            }
            else
            {
               Z1164Baseline_DataHomologacao = A1164Baseline_DataHomologacao;
               Z1165Baseline_DataAtualizacao = A1165Baseline_DataAtualizacao;
               Z726Baseline_PFBAntes = A726Baseline_PFBAntes;
               Z727Baseline_PFBDepois = A727Baseline_PFBDepois;
               Z192Contagem_Codigo = A192Contagem_Codigo;
               Z721Baseline_UserCod = A721Baseline_UserCod;
               Z735Baseline_ProjetoMelCod = A735Baseline_ProjetoMelCod;
            }
         }
         if ( GX_JID == -3 )
         {
            Z722Baseline_Codigo = A722Baseline_Codigo;
            Z1164Baseline_DataHomologacao = A1164Baseline_DataHomologacao;
            Z1165Baseline_DataAtualizacao = A1165Baseline_DataAtualizacao;
            Z726Baseline_PFBAntes = A726Baseline_PFBAntes;
            Z727Baseline_PFBDepois = A727Baseline_PFBDepois;
            Z1161Baseline_Antes = A1161Baseline_Antes;
            Z1162Baseline_Depois = A1162Baseline_Depois;
            Z192Contagem_Codigo = A192Contagem_Codigo;
            Z721Baseline_UserCod = A721Baseline_UserCod;
            Z735Baseline_ProjetoMelCod = A735Baseline_ProjetoMelCod;
            Z940Contagem_SistemaCod = A940Contagem_SistemaCod;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load2E92( )
      {
         /* Using cursor T002E7 */
         pr_default.execute(5, new Object[] {A722Baseline_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound92 = 1;
            A1164Baseline_DataHomologacao = T002E7_A1164Baseline_DataHomologacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1164Baseline_DataHomologacao", context.localUtil.TToC( A1164Baseline_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
            A1165Baseline_DataAtualizacao = T002E7_A1165Baseline_DataAtualizacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1165Baseline_DataAtualizacao", context.localUtil.TToC( A1165Baseline_DataAtualizacao, 8, 5, 0, 3, "/", ":", " "));
            n1165Baseline_DataAtualizacao = T002E7_n1165Baseline_DataAtualizacao[0];
            A726Baseline_PFBAntes = T002E7_A726Baseline_PFBAntes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A726Baseline_PFBAntes", StringUtil.LTrim( StringUtil.Str( A726Baseline_PFBAntes, 14, 5)));
            n726Baseline_PFBAntes = T002E7_n726Baseline_PFBAntes[0];
            A727Baseline_PFBDepois = T002E7_A727Baseline_PFBDepois[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A727Baseline_PFBDepois", StringUtil.LTrim( StringUtil.Str( A727Baseline_PFBDepois, 14, 5)));
            n727Baseline_PFBDepois = T002E7_n727Baseline_PFBDepois[0];
            A1161Baseline_Antes = T002E7_A1161Baseline_Antes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1161Baseline_Antes", A1161Baseline_Antes);
            n1161Baseline_Antes = T002E7_n1161Baseline_Antes[0];
            A1162Baseline_Depois = T002E7_A1162Baseline_Depois[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1162Baseline_Depois", A1162Baseline_Depois);
            n1162Baseline_Depois = T002E7_n1162Baseline_Depois[0];
            A192Contagem_Codigo = T002E7_A192Contagem_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
            A721Baseline_UserCod = T002E7_A721Baseline_UserCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A721Baseline_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A721Baseline_UserCod), 6, 0)));
            A735Baseline_ProjetoMelCod = T002E7_A735Baseline_ProjetoMelCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A735Baseline_ProjetoMelCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A735Baseline_ProjetoMelCod), 6, 0)));
            n735Baseline_ProjetoMelCod = T002E7_n735Baseline_ProjetoMelCod[0];
            A940Contagem_SistemaCod = T002E7_A940Contagem_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
            n940Contagem_SistemaCod = T002E7_n940Contagem_SistemaCod[0];
            ZM2E92( -3) ;
         }
         pr_default.close(5);
         OnLoadActions2E92( ) ;
      }

      protected void OnLoadActions2E92( )
      {
      }

      protected void CheckExtendedTable2E92( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A1164Baseline_DataHomologacao) || ( A1164Baseline_DataHomologacao >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data de Homologa��o fora do intervalo", "OutOfRange", 1, "BASELINE_DATAHOMOLOGACAO");
            AnyError = 1;
            GX_FocusControl = edtBaseline_DataHomologacao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T002E5 */
         pr_default.execute(3, new Object[] {A721Baseline_UserCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Baseline_Usuario'.", "ForeignKeyNotFound", 1, "BASELINE_USERCOD");
            AnyError = 1;
            GX_FocusControl = edtBaseline_UserCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(3);
         /* Using cursor T002E4 */
         pr_default.execute(2, new Object[] {A192Contagem_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEM_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagem_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A940Contagem_SistemaCod = T002E4_A940Contagem_SistemaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
         n940Contagem_SistemaCod = T002E4_n940Contagem_SistemaCod[0];
         pr_default.close(2);
         if ( ! ( (DateTime.MinValue==A1165Baseline_DataAtualizacao) || ( A1165Baseline_DataAtualizacao >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data de Atualiza��o fora do intervalo", "OutOfRange", 1, "BASELINE_DATAATUALIZACAO");
            AnyError = 1;
            GX_FocusControl = edtBaseline_DataAtualizacao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T002E6 */
         pr_default.execute(4, new Object[] {n735Baseline_ProjetoMelCod, A735Baseline_ProjetoMelCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A735Baseline_ProjetoMelCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Baseline_Projeto Melhoria'.", "ForeignKeyNotFound", 1, "BASELINE_PROJETOMELCOD");
               AnyError = 1;
               GX_FocusControl = edtBaseline_ProjetoMelCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors2E92( )
      {
         pr_default.close(3);
         pr_default.close(2);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_5( int A721Baseline_UserCod )
      {
         /* Using cursor T002E8 */
         pr_default.execute(6, new Object[] {A721Baseline_UserCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Baseline_Usuario'.", "ForeignKeyNotFound", 1, "BASELINE_USERCOD");
            AnyError = 1;
            GX_FocusControl = edtBaseline_UserCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void gxLoad_4( int A192Contagem_Codigo )
      {
         /* Using cursor T002E9 */
         pr_default.execute(7, new Object[] {A192Contagem_Codigo});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEM_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagem_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A940Contagem_SistemaCod = T002E9_A940Contagem_SistemaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
         n940Contagem_SistemaCod = T002E9_n940Contagem_SistemaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A940Contagem_SistemaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_6( int A735Baseline_ProjetoMelCod )
      {
         /* Using cursor T002E10 */
         pr_default.execute(8, new Object[] {n735Baseline_ProjetoMelCod, A735Baseline_ProjetoMelCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            if ( ! ( (0==A735Baseline_ProjetoMelCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Baseline_Projeto Melhoria'.", "ForeignKeyNotFound", 1, "BASELINE_PROJETOMELCOD");
               AnyError = 1;
               GX_FocusControl = edtBaseline_ProjetoMelCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void GetKey2E92( )
      {
         /* Using cursor T002E11 */
         pr_default.execute(9, new Object[] {A722Baseline_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound92 = 1;
         }
         else
         {
            RcdFound92 = 0;
         }
         pr_default.close(9);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002E3 */
         pr_default.execute(1, new Object[] {A722Baseline_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2E92( 3) ;
            RcdFound92 = 1;
            A722Baseline_Codigo = T002E3_A722Baseline_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A722Baseline_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A722Baseline_Codigo), 6, 0)));
            A1164Baseline_DataHomologacao = T002E3_A1164Baseline_DataHomologacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1164Baseline_DataHomologacao", context.localUtil.TToC( A1164Baseline_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
            A1165Baseline_DataAtualizacao = T002E3_A1165Baseline_DataAtualizacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1165Baseline_DataAtualizacao", context.localUtil.TToC( A1165Baseline_DataAtualizacao, 8, 5, 0, 3, "/", ":", " "));
            n1165Baseline_DataAtualizacao = T002E3_n1165Baseline_DataAtualizacao[0];
            A726Baseline_PFBAntes = T002E3_A726Baseline_PFBAntes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A726Baseline_PFBAntes", StringUtil.LTrim( StringUtil.Str( A726Baseline_PFBAntes, 14, 5)));
            n726Baseline_PFBAntes = T002E3_n726Baseline_PFBAntes[0];
            A727Baseline_PFBDepois = T002E3_A727Baseline_PFBDepois[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A727Baseline_PFBDepois", StringUtil.LTrim( StringUtil.Str( A727Baseline_PFBDepois, 14, 5)));
            n727Baseline_PFBDepois = T002E3_n727Baseline_PFBDepois[0];
            A1161Baseline_Antes = T002E3_A1161Baseline_Antes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1161Baseline_Antes", A1161Baseline_Antes);
            n1161Baseline_Antes = T002E3_n1161Baseline_Antes[0];
            A1162Baseline_Depois = T002E3_A1162Baseline_Depois[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1162Baseline_Depois", A1162Baseline_Depois);
            n1162Baseline_Depois = T002E3_n1162Baseline_Depois[0];
            A192Contagem_Codigo = T002E3_A192Contagem_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
            A721Baseline_UserCod = T002E3_A721Baseline_UserCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A721Baseline_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A721Baseline_UserCod), 6, 0)));
            A735Baseline_ProjetoMelCod = T002E3_A735Baseline_ProjetoMelCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A735Baseline_ProjetoMelCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A735Baseline_ProjetoMelCod), 6, 0)));
            n735Baseline_ProjetoMelCod = T002E3_n735Baseline_ProjetoMelCod[0];
            Z722Baseline_Codigo = A722Baseline_Codigo;
            sMode92 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load2E92( ) ;
            if ( AnyError == 1 )
            {
               RcdFound92 = 0;
               InitializeNonKey2E92( ) ;
            }
            Gx_mode = sMode92;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound92 = 0;
            InitializeNonKey2E92( ) ;
            sMode92 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode92;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2E92( ) ;
         if ( RcdFound92 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound92 = 0;
         /* Using cursor T002E12 */
         pr_default.execute(10, new Object[] {A722Baseline_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T002E12_A722Baseline_Codigo[0] < A722Baseline_Codigo ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T002E12_A722Baseline_Codigo[0] > A722Baseline_Codigo ) ) )
            {
               A722Baseline_Codigo = T002E12_A722Baseline_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A722Baseline_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A722Baseline_Codigo), 6, 0)));
               RcdFound92 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void move_previous( )
      {
         RcdFound92 = 0;
         /* Using cursor T002E13 */
         pr_default.execute(11, new Object[] {A722Baseline_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T002E13_A722Baseline_Codigo[0] > A722Baseline_Codigo ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T002E13_A722Baseline_Codigo[0] < A722Baseline_Codigo ) ) )
            {
               A722Baseline_Codigo = T002E13_A722Baseline_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A722Baseline_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A722Baseline_Codigo), 6, 0)));
               RcdFound92 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2E92( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtBaseline_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2E92( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound92 == 1 )
            {
               if ( A722Baseline_Codigo != Z722Baseline_Codigo )
               {
                  A722Baseline_Codigo = Z722Baseline_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A722Baseline_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A722Baseline_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "BASELINE_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtBaseline_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtBaseline_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update2E92( ) ;
                  GX_FocusControl = edtBaseline_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A722Baseline_Codigo != Z722Baseline_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtBaseline_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2E92( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "BASELINE_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtBaseline_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtBaseline_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2E92( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A722Baseline_Codigo != Z722Baseline_Codigo )
         {
            A722Baseline_Codigo = Z722Baseline_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A722Baseline_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A722Baseline_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "BASELINE_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtBaseline_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtBaseline_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound92 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "BASELINE_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtBaseline_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtBaseline_DataHomologacao_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart2E92( ) ;
         if ( RcdFound92 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtBaseline_DataHomologacao_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd2E92( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound92 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtBaseline_DataHomologacao_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound92 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtBaseline_DataHomologacao_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart2E92( ) ;
         if ( RcdFound92 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound92 != 0 )
            {
               ScanNext2E92( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtBaseline_DataHomologacao_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd2E92( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency2E92( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002E2 */
            pr_default.execute(0, new Object[] {A722Baseline_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Baseline"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z1164Baseline_DataHomologacao != T002E2_A1164Baseline_DataHomologacao[0] ) || ( Z1165Baseline_DataAtualizacao != T002E2_A1165Baseline_DataAtualizacao[0] ) || ( Z726Baseline_PFBAntes != T002E2_A726Baseline_PFBAntes[0] ) || ( Z727Baseline_PFBDepois != T002E2_A727Baseline_PFBDepois[0] ) || ( Z192Contagem_Codigo != T002E2_A192Contagem_Codigo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z721Baseline_UserCod != T002E2_A721Baseline_UserCod[0] ) || ( Z735Baseline_ProjetoMelCod != T002E2_A735Baseline_ProjetoMelCod[0] ) )
            {
               if ( Z1164Baseline_DataHomologacao != T002E2_A1164Baseline_DataHomologacao[0] )
               {
                  GXUtil.WriteLog("baseline:[seudo value changed for attri]"+"Baseline_DataHomologacao");
                  GXUtil.WriteLogRaw("Old: ",Z1164Baseline_DataHomologacao);
                  GXUtil.WriteLogRaw("Current: ",T002E2_A1164Baseline_DataHomologacao[0]);
               }
               if ( Z1165Baseline_DataAtualizacao != T002E2_A1165Baseline_DataAtualizacao[0] )
               {
                  GXUtil.WriteLog("baseline:[seudo value changed for attri]"+"Baseline_DataAtualizacao");
                  GXUtil.WriteLogRaw("Old: ",Z1165Baseline_DataAtualizacao);
                  GXUtil.WriteLogRaw("Current: ",T002E2_A1165Baseline_DataAtualizacao[0]);
               }
               if ( Z726Baseline_PFBAntes != T002E2_A726Baseline_PFBAntes[0] )
               {
                  GXUtil.WriteLog("baseline:[seudo value changed for attri]"+"Baseline_PFBAntes");
                  GXUtil.WriteLogRaw("Old: ",Z726Baseline_PFBAntes);
                  GXUtil.WriteLogRaw("Current: ",T002E2_A726Baseline_PFBAntes[0]);
               }
               if ( Z727Baseline_PFBDepois != T002E2_A727Baseline_PFBDepois[0] )
               {
                  GXUtil.WriteLog("baseline:[seudo value changed for attri]"+"Baseline_PFBDepois");
                  GXUtil.WriteLogRaw("Old: ",Z727Baseline_PFBDepois);
                  GXUtil.WriteLogRaw("Current: ",T002E2_A727Baseline_PFBDepois[0]);
               }
               if ( Z192Contagem_Codigo != T002E2_A192Contagem_Codigo[0] )
               {
                  GXUtil.WriteLog("baseline:[seudo value changed for attri]"+"Contagem_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z192Contagem_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T002E2_A192Contagem_Codigo[0]);
               }
               if ( Z721Baseline_UserCod != T002E2_A721Baseline_UserCod[0] )
               {
                  GXUtil.WriteLog("baseline:[seudo value changed for attri]"+"Baseline_UserCod");
                  GXUtil.WriteLogRaw("Old: ",Z721Baseline_UserCod);
                  GXUtil.WriteLogRaw("Current: ",T002E2_A721Baseline_UserCod[0]);
               }
               if ( Z735Baseline_ProjetoMelCod != T002E2_A735Baseline_ProjetoMelCod[0] )
               {
                  GXUtil.WriteLog("baseline:[seudo value changed for attri]"+"Baseline_ProjetoMelCod");
                  GXUtil.WriteLogRaw("Old: ",Z735Baseline_ProjetoMelCod);
                  GXUtil.WriteLogRaw("Current: ",T002E2_A735Baseline_ProjetoMelCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Baseline"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2E92( )
      {
         BeforeValidate2E92( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2E92( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2E92( 0) ;
            CheckOptimisticConcurrency2E92( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2E92( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2E92( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002E14 */
                     pr_default.execute(12, new Object[] {A1164Baseline_DataHomologacao, n1165Baseline_DataAtualizacao, A1165Baseline_DataAtualizacao, n726Baseline_PFBAntes, A726Baseline_PFBAntes, n727Baseline_PFBDepois, A727Baseline_PFBDepois, n1161Baseline_Antes, A1161Baseline_Antes, n1162Baseline_Depois, A1162Baseline_Depois, A192Contagem_Codigo, A721Baseline_UserCod, n735Baseline_ProjetoMelCod, A735Baseline_ProjetoMelCod});
                     A722Baseline_Codigo = T002E14_A722Baseline_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A722Baseline_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A722Baseline_Codigo), 6, 0)));
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("Baseline") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2E0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2E92( ) ;
            }
            EndLevel2E92( ) ;
         }
         CloseExtendedTableCursors2E92( ) ;
      }

      protected void Update2E92( )
      {
         BeforeValidate2E92( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2E92( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2E92( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2E92( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2E92( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002E15 */
                     pr_default.execute(13, new Object[] {A1164Baseline_DataHomologacao, n1165Baseline_DataAtualizacao, A1165Baseline_DataAtualizacao, n726Baseline_PFBAntes, A726Baseline_PFBAntes, n727Baseline_PFBDepois, A727Baseline_PFBDepois, n1161Baseline_Antes, A1161Baseline_Antes, n1162Baseline_Depois, A1162Baseline_Depois, A192Contagem_Codigo, A721Baseline_UserCod, n735Baseline_ProjetoMelCod, A735Baseline_ProjetoMelCod, A722Baseline_Codigo});
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("Baseline") ;
                     if ( (pr_default.getStatus(13) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Baseline"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2E92( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption2E0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2E92( ) ;
         }
         CloseExtendedTableCursors2E92( ) ;
      }

      protected void DeferredUpdate2E92( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate2E92( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2E92( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2E92( ) ;
            AfterConfirm2E92( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2E92( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002E16 */
                  pr_default.execute(14, new Object[] {A722Baseline_Codigo});
                  pr_default.close(14);
                  dsDefault.SmartCacheProvider.SetUpdated("Baseline") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound92 == 0 )
                        {
                           InitAll2E92( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption2E0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode92 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel2E92( ) ;
         Gx_mode = sMode92;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls2E92( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002E17 */
            pr_default.execute(15, new Object[] {A192Contagem_Codigo});
            A940Contagem_SistemaCod = T002E17_A940Contagem_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
            n940Contagem_SistemaCod = T002E17_n940Contagem_SistemaCod[0];
            pr_default.close(15);
         }
      }

      protected void EndLevel2E92( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2E92( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(15);
            context.CommitDataStores( "Baseline");
            if ( AnyError == 0 )
            {
               ConfirmValues2E0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(15);
            context.RollbackDataStores( "Baseline");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2E92( )
      {
         /* Using cursor T002E18 */
         pr_default.execute(16);
         RcdFound92 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound92 = 1;
            A722Baseline_Codigo = T002E18_A722Baseline_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A722Baseline_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A722Baseline_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2E92( )
      {
         /* Scan next routine */
         pr_default.readNext(16);
         RcdFound92 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound92 = 1;
            A722Baseline_Codigo = T002E18_A722Baseline_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A722Baseline_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A722Baseline_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2E92( )
      {
         pr_default.close(16);
      }

      protected void AfterConfirm2E92( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2E92( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2E92( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2E92( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2E92( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2E92( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2E92( )
      {
         edtBaseline_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBaseline_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBaseline_Codigo_Enabled), 5, 0)));
         edtBaseline_DataHomologacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBaseline_DataHomologacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBaseline_DataHomologacao_Enabled), 5, 0)));
         edtBaseline_UserCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBaseline_UserCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBaseline_UserCod_Enabled), 5, 0)));
         edtContagem_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_Codigo_Enabled), 5, 0)));
         edtContagem_SistemaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_SistemaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_SistemaCod_Enabled), 5, 0)));
         edtBaseline_DataAtualizacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBaseline_DataAtualizacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBaseline_DataAtualizacao_Enabled), 5, 0)));
         edtBaseline_PFBAntes_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBaseline_PFBAntes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBaseline_PFBAntes_Enabled), 5, 0)));
         edtBaseline_PFBDepois_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBaseline_PFBDepois_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBaseline_PFBDepois_Enabled), 5, 0)));
         edtBaseline_ProjetoMelCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBaseline_ProjetoMelCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBaseline_ProjetoMelCod_Enabled), 5, 0)));
         edtBaseline_Antes_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBaseline_Antes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBaseline_Antes_Enabled), 5, 0)));
         edtBaseline_Depois_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtBaseline_Depois_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtBaseline_Depois_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2E0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117224413");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("baseline.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z722Baseline_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z722Baseline_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1164Baseline_DataHomologacao", context.localUtil.TToC( Z1164Baseline_DataHomologacao, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1165Baseline_DataAtualizacao", context.localUtil.TToC( Z1165Baseline_DataAtualizacao, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z726Baseline_PFBAntes", StringUtil.LTrim( StringUtil.NToC( Z726Baseline_PFBAntes, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z727Baseline_PFBDepois", StringUtil.LTrim( StringUtil.NToC( Z727Baseline_PFBDepois, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z192Contagem_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z192Contagem_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z721Baseline_UserCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z721Baseline_UserCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z735Baseline_ProjetoMelCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z735Baseline_ProjetoMelCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("baseline.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "Baseline" ;
      }

      public override String GetPgmdesc( )
      {
         return "Baseline" ;
      }

      protected void InitializeNonKey2E92( )
      {
         A1164Baseline_DataHomologacao = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1164Baseline_DataHomologacao", context.localUtil.TToC( A1164Baseline_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
         A721Baseline_UserCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A721Baseline_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A721Baseline_UserCod), 6, 0)));
         A192Contagem_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
         A940Contagem_SistemaCod = 0;
         n940Contagem_SistemaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
         A1165Baseline_DataAtualizacao = (DateTime)(DateTime.MinValue);
         n1165Baseline_DataAtualizacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1165Baseline_DataAtualizacao", context.localUtil.TToC( A1165Baseline_DataAtualizacao, 8, 5, 0, 3, "/", ":", " "));
         n1165Baseline_DataAtualizacao = ((DateTime.MinValue==A1165Baseline_DataAtualizacao) ? true : false);
         A726Baseline_PFBAntes = 0;
         n726Baseline_PFBAntes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A726Baseline_PFBAntes", StringUtil.LTrim( StringUtil.Str( A726Baseline_PFBAntes, 14, 5)));
         n726Baseline_PFBAntes = ((Convert.ToDecimal(0)==A726Baseline_PFBAntes) ? true : false);
         A727Baseline_PFBDepois = 0;
         n727Baseline_PFBDepois = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A727Baseline_PFBDepois", StringUtil.LTrim( StringUtil.Str( A727Baseline_PFBDepois, 14, 5)));
         n727Baseline_PFBDepois = ((Convert.ToDecimal(0)==A727Baseline_PFBDepois) ? true : false);
         A735Baseline_ProjetoMelCod = 0;
         n735Baseline_ProjetoMelCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A735Baseline_ProjetoMelCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A735Baseline_ProjetoMelCod), 6, 0)));
         n735Baseline_ProjetoMelCod = ((0==A735Baseline_ProjetoMelCod) ? true : false);
         A1161Baseline_Antes = "";
         n1161Baseline_Antes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1161Baseline_Antes", A1161Baseline_Antes);
         n1161Baseline_Antes = (String.IsNullOrEmpty(StringUtil.RTrim( A1161Baseline_Antes)) ? true : false);
         A1162Baseline_Depois = "";
         n1162Baseline_Depois = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1162Baseline_Depois", A1162Baseline_Depois);
         n1162Baseline_Depois = (String.IsNullOrEmpty(StringUtil.RTrim( A1162Baseline_Depois)) ? true : false);
         Z1164Baseline_DataHomologacao = (DateTime)(DateTime.MinValue);
         Z1165Baseline_DataAtualizacao = (DateTime)(DateTime.MinValue);
         Z726Baseline_PFBAntes = 0;
         Z727Baseline_PFBDepois = 0;
         Z192Contagem_Codigo = 0;
         Z721Baseline_UserCod = 0;
         Z735Baseline_ProjetoMelCod = 0;
      }

      protected void InitAll2E92( )
      {
         A722Baseline_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A722Baseline_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A722Baseline_Codigo), 6, 0)));
         InitializeNonKey2E92( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117224423");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("baseline.js", "?20203117224423");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockbaseline_codigo_Internalname = "TEXTBLOCKBASELINE_CODIGO";
         edtBaseline_Codigo_Internalname = "BASELINE_CODIGO";
         lblTextblockbaseline_datahomologacao_Internalname = "TEXTBLOCKBASELINE_DATAHOMOLOGACAO";
         edtBaseline_DataHomologacao_Internalname = "BASELINE_DATAHOMOLOGACAO";
         lblTextblockbaseline_usercod_Internalname = "TEXTBLOCKBASELINE_USERCOD";
         edtBaseline_UserCod_Internalname = "BASELINE_USERCOD";
         lblTextblockcontagem_codigo_Internalname = "TEXTBLOCKCONTAGEM_CODIGO";
         edtContagem_Codigo_Internalname = "CONTAGEM_CODIGO";
         lblTextblockcontagem_sistemacod_Internalname = "TEXTBLOCKCONTAGEM_SISTEMACOD";
         edtContagem_SistemaCod_Internalname = "CONTAGEM_SISTEMACOD";
         lblTextblockbaseline_dataatualizacao_Internalname = "TEXTBLOCKBASELINE_DATAATUALIZACAO";
         edtBaseline_DataAtualizacao_Internalname = "BASELINE_DATAATUALIZACAO";
         lblTextblockbaseline_pfbantes_Internalname = "TEXTBLOCKBASELINE_PFBANTES";
         edtBaseline_PFBAntes_Internalname = "BASELINE_PFBANTES";
         lblTextblockbaseline_pfbdepois_Internalname = "TEXTBLOCKBASELINE_PFBDEPOIS";
         edtBaseline_PFBDepois_Internalname = "BASELINE_PFBDEPOIS";
         lblTextblockbaseline_projetomelcod_Internalname = "TEXTBLOCKBASELINE_PROJETOMELCOD";
         edtBaseline_ProjetoMelCod_Internalname = "BASELINE_PROJETOMELCOD";
         lblTextblockbaseline_antes_Internalname = "TEXTBLOCKBASELINE_ANTES";
         edtBaseline_Antes_Internalname = "BASELINE_ANTES";
         lblTextblockbaseline_depois_Internalname = "TEXTBLOCKBASELINE_DEPOIS";
         edtBaseline_Depois_Internalname = "BASELINE_DEPOIS";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Baseline";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtBaseline_Depois_Enabled = 1;
         edtBaseline_Antes_Enabled = 1;
         edtBaseline_ProjetoMelCod_Jsonclick = "";
         edtBaseline_ProjetoMelCod_Enabled = 1;
         edtBaseline_PFBDepois_Jsonclick = "";
         edtBaseline_PFBDepois_Enabled = 1;
         edtBaseline_PFBAntes_Jsonclick = "";
         edtBaseline_PFBAntes_Enabled = 1;
         edtBaseline_DataAtualizacao_Jsonclick = "";
         edtBaseline_DataAtualizacao_Enabled = 1;
         edtContagem_SistemaCod_Jsonclick = "";
         edtContagem_SistemaCod_Enabled = 0;
         edtContagem_Codigo_Jsonclick = "";
         edtContagem_Codigo_Enabled = 1;
         edtBaseline_UserCod_Jsonclick = "";
         edtBaseline_UserCod_Enabled = 1;
         edtBaseline_DataHomologacao_Jsonclick = "";
         edtBaseline_DataHomologacao_Enabled = 1;
         edtBaseline_Codigo_Jsonclick = "";
         edtBaseline_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtBaseline_DataHomologacao_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Baseline_codigo( int GX_Parm1 ,
                                         DateTime GX_Parm2 ,
                                         DateTime GX_Parm3 ,
                                         decimal GX_Parm4 ,
                                         decimal GX_Parm5 ,
                                         String GX_Parm6 ,
                                         String GX_Parm7 ,
                                         int GX_Parm8 ,
                                         int GX_Parm9 ,
                                         int GX_Parm10 )
      {
         A722Baseline_Codigo = GX_Parm1;
         A1164Baseline_DataHomologacao = GX_Parm2;
         A1165Baseline_DataAtualizacao = GX_Parm3;
         n1165Baseline_DataAtualizacao = false;
         A726Baseline_PFBAntes = GX_Parm4;
         n726Baseline_PFBAntes = false;
         A727Baseline_PFBDepois = GX_Parm5;
         n727Baseline_PFBDepois = false;
         A1161Baseline_Antes = GX_Parm6;
         n1161Baseline_Antes = false;
         A1162Baseline_Depois = GX_Parm7;
         n1162Baseline_Depois = false;
         A192Contagem_Codigo = GX_Parm8;
         A721Baseline_UserCod = GX_Parm9;
         A735Baseline_ProjetoMelCod = GX_Parm10;
         n735Baseline_ProjetoMelCod = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A940Contagem_SistemaCod = 0;
            n940Contagem_SistemaCod = false;
         }
         isValidOutput.Add(context.localUtil.TToC( A1164Baseline_DataHomologacao, 10, 8, 0, 3, "/", ":", " "));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A721Baseline_UserCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ".", "")));
         isValidOutput.Add(context.localUtil.TToC( A1165Baseline_DataAtualizacao, 10, 8, 0, 3, "/", ":", " "));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A726Baseline_PFBAntes, 14, 5, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A727Baseline_PFBDepois, 14, 5, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A735Baseline_ProjetoMelCod), 6, 0, ".", "")));
         isValidOutput.Add(A1161Baseline_Antes);
         isValidOutput.Add(A1162Baseline_Depois);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A940Contagem_SistemaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z722Baseline_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(context.localUtil.TToC( Z1164Baseline_DataHomologacao, 10, 8, 0, 0, "/", ":", " "));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z721Baseline_UserCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z192Contagem_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(context.localUtil.TToC( Z1165Baseline_DataAtualizacao, 10, 8, 0, 0, "/", ":", " "));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z726Baseline_PFBAntes, 14, 5, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z727Baseline_PFBDepois, 14, 5, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z735Baseline_ProjetoMelCod), 6, 0, ",", "")));
         isValidOutput.Add(Z1161Baseline_Antes);
         isValidOutput.Add(Z1162Baseline_Depois);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z940Contagem_SistemaCod), 6, 0, ".", "")));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Baseline_usercod( int GX_Parm1 )
      {
         A721Baseline_UserCod = GX_Parm1;
         /* Using cursor T002E19 */
         pr_default.execute(17, new Object[] {A721Baseline_UserCod});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Baseline_Usuario'.", "ForeignKeyNotFound", 1, "BASELINE_USERCOD");
            AnyError = 1;
            GX_FocusControl = edtBaseline_UserCod_Internalname;
         }
         pr_default.close(17);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagem_codigo( int GX_Parm1 ,
                                         int GX_Parm2 )
      {
         A192Contagem_Codigo = GX_Parm1;
         A940Contagem_SistemaCod = GX_Parm2;
         n940Contagem_SistemaCod = false;
         /* Using cursor T002E17 */
         pr_default.execute(15, new Object[] {A192Contagem_Codigo});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEM_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagem_Codigo_Internalname;
         }
         A940Contagem_SistemaCod = T002E17_A940Contagem_SistemaCod[0];
         n940Contagem_SistemaCod = T002E17_n940Contagem_SistemaCod[0];
         pr_default.close(15);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A940Contagem_SistemaCod = 0;
            n940Contagem_SistemaCod = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A940Contagem_SistemaCod), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Baseline_projetomelcod( int GX_Parm1 )
      {
         A735Baseline_ProjetoMelCod = GX_Parm1;
         n735Baseline_ProjetoMelCod = false;
         /* Using cursor T002E20 */
         pr_default.execute(18, new Object[] {n735Baseline_ProjetoMelCod, A735Baseline_ProjetoMelCod});
         if ( (pr_default.getStatus(18) == 101) )
         {
            if ( ! ( (0==A735Baseline_ProjetoMelCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Baseline_Projeto Melhoria'.", "ForeignKeyNotFound", 1, "BASELINE_PROJETOMELCOD");
               AnyError = 1;
               GX_FocusControl = edtBaseline_ProjetoMelCod_Internalname;
            }
         }
         pr_default.close(18);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
         pr_default.close(17);
         pr_default.close(18);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z1164Baseline_DataHomologacao = (DateTime)(DateTime.MinValue);
         Z1165Baseline_DataAtualizacao = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockbaseline_codigo_Jsonclick = "";
         lblTextblockbaseline_datahomologacao_Jsonclick = "";
         A1164Baseline_DataHomologacao = (DateTime)(DateTime.MinValue);
         lblTextblockbaseline_usercod_Jsonclick = "";
         lblTextblockcontagem_codigo_Jsonclick = "";
         lblTextblockcontagem_sistemacod_Jsonclick = "";
         lblTextblockbaseline_dataatualizacao_Jsonclick = "";
         A1165Baseline_DataAtualizacao = (DateTime)(DateTime.MinValue);
         lblTextblockbaseline_pfbantes_Jsonclick = "";
         lblTextblockbaseline_pfbdepois_Jsonclick = "";
         lblTextblockbaseline_projetomelcod_Jsonclick = "";
         lblTextblockbaseline_antes_Jsonclick = "";
         A1161Baseline_Antes = "";
         lblTextblockbaseline_depois_Jsonclick = "";
         A1162Baseline_Depois = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z1161Baseline_Antes = "";
         Z1162Baseline_Depois = "";
         T002E7_A722Baseline_Codigo = new int[1] ;
         T002E7_A1164Baseline_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         T002E7_A1165Baseline_DataAtualizacao = new DateTime[] {DateTime.MinValue} ;
         T002E7_n1165Baseline_DataAtualizacao = new bool[] {false} ;
         T002E7_A726Baseline_PFBAntes = new decimal[1] ;
         T002E7_n726Baseline_PFBAntes = new bool[] {false} ;
         T002E7_A727Baseline_PFBDepois = new decimal[1] ;
         T002E7_n727Baseline_PFBDepois = new bool[] {false} ;
         T002E7_A1161Baseline_Antes = new String[] {""} ;
         T002E7_n1161Baseline_Antes = new bool[] {false} ;
         T002E7_A1162Baseline_Depois = new String[] {""} ;
         T002E7_n1162Baseline_Depois = new bool[] {false} ;
         T002E7_A192Contagem_Codigo = new int[1] ;
         T002E7_A721Baseline_UserCod = new int[1] ;
         T002E7_A735Baseline_ProjetoMelCod = new int[1] ;
         T002E7_n735Baseline_ProjetoMelCod = new bool[] {false} ;
         T002E7_A940Contagem_SistemaCod = new int[1] ;
         T002E7_n940Contagem_SistemaCod = new bool[] {false} ;
         T002E5_A721Baseline_UserCod = new int[1] ;
         T002E4_A940Contagem_SistemaCod = new int[1] ;
         T002E4_n940Contagem_SistemaCod = new bool[] {false} ;
         T002E6_A735Baseline_ProjetoMelCod = new int[1] ;
         T002E6_n735Baseline_ProjetoMelCod = new bool[] {false} ;
         T002E8_A721Baseline_UserCod = new int[1] ;
         T002E9_A940Contagem_SistemaCod = new int[1] ;
         T002E9_n940Contagem_SistemaCod = new bool[] {false} ;
         T002E10_A735Baseline_ProjetoMelCod = new int[1] ;
         T002E10_n735Baseline_ProjetoMelCod = new bool[] {false} ;
         T002E11_A722Baseline_Codigo = new int[1] ;
         T002E3_A722Baseline_Codigo = new int[1] ;
         T002E3_A1164Baseline_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         T002E3_A1165Baseline_DataAtualizacao = new DateTime[] {DateTime.MinValue} ;
         T002E3_n1165Baseline_DataAtualizacao = new bool[] {false} ;
         T002E3_A726Baseline_PFBAntes = new decimal[1] ;
         T002E3_n726Baseline_PFBAntes = new bool[] {false} ;
         T002E3_A727Baseline_PFBDepois = new decimal[1] ;
         T002E3_n727Baseline_PFBDepois = new bool[] {false} ;
         T002E3_A1161Baseline_Antes = new String[] {""} ;
         T002E3_n1161Baseline_Antes = new bool[] {false} ;
         T002E3_A1162Baseline_Depois = new String[] {""} ;
         T002E3_n1162Baseline_Depois = new bool[] {false} ;
         T002E3_A192Contagem_Codigo = new int[1] ;
         T002E3_A721Baseline_UserCod = new int[1] ;
         T002E3_A735Baseline_ProjetoMelCod = new int[1] ;
         T002E3_n735Baseline_ProjetoMelCod = new bool[] {false} ;
         sMode92 = "";
         T002E12_A722Baseline_Codigo = new int[1] ;
         T002E13_A722Baseline_Codigo = new int[1] ;
         T002E2_A722Baseline_Codigo = new int[1] ;
         T002E2_A1164Baseline_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         T002E2_A1165Baseline_DataAtualizacao = new DateTime[] {DateTime.MinValue} ;
         T002E2_n1165Baseline_DataAtualizacao = new bool[] {false} ;
         T002E2_A726Baseline_PFBAntes = new decimal[1] ;
         T002E2_n726Baseline_PFBAntes = new bool[] {false} ;
         T002E2_A727Baseline_PFBDepois = new decimal[1] ;
         T002E2_n727Baseline_PFBDepois = new bool[] {false} ;
         T002E2_A1161Baseline_Antes = new String[] {""} ;
         T002E2_n1161Baseline_Antes = new bool[] {false} ;
         T002E2_A1162Baseline_Depois = new String[] {""} ;
         T002E2_n1162Baseline_Depois = new bool[] {false} ;
         T002E2_A192Contagem_Codigo = new int[1] ;
         T002E2_A721Baseline_UserCod = new int[1] ;
         T002E2_A735Baseline_ProjetoMelCod = new int[1] ;
         T002E2_n735Baseline_ProjetoMelCod = new bool[] {false} ;
         T002E14_A722Baseline_Codigo = new int[1] ;
         T002E17_A940Contagem_SistemaCod = new int[1] ;
         T002E17_n940Contagem_SistemaCod = new bool[] {false} ;
         T002E18_A722Baseline_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         T002E19_A721Baseline_UserCod = new int[1] ;
         T002E20_A735Baseline_ProjetoMelCod = new int[1] ;
         T002E20_n735Baseline_ProjetoMelCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.baseline__default(),
            new Object[][] {
                new Object[] {
               T002E2_A722Baseline_Codigo, T002E2_A1164Baseline_DataHomologacao, T002E2_A1165Baseline_DataAtualizacao, T002E2_n1165Baseline_DataAtualizacao, T002E2_A726Baseline_PFBAntes, T002E2_n726Baseline_PFBAntes, T002E2_A727Baseline_PFBDepois, T002E2_n727Baseline_PFBDepois, T002E2_A1161Baseline_Antes, T002E2_n1161Baseline_Antes,
               T002E2_A1162Baseline_Depois, T002E2_n1162Baseline_Depois, T002E2_A192Contagem_Codigo, T002E2_A721Baseline_UserCod, T002E2_A735Baseline_ProjetoMelCod, T002E2_n735Baseline_ProjetoMelCod
               }
               , new Object[] {
               T002E3_A722Baseline_Codigo, T002E3_A1164Baseline_DataHomologacao, T002E3_A1165Baseline_DataAtualizacao, T002E3_n1165Baseline_DataAtualizacao, T002E3_A726Baseline_PFBAntes, T002E3_n726Baseline_PFBAntes, T002E3_A727Baseline_PFBDepois, T002E3_n727Baseline_PFBDepois, T002E3_A1161Baseline_Antes, T002E3_n1161Baseline_Antes,
               T002E3_A1162Baseline_Depois, T002E3_n1162Baseline_Depois, T002E3_A192Contagem_Codigo, T002E3_A721Baseline_UserCod, T002E3_A735Baseline_ProjetoMelCod, T002E3_n735Baseline_ProjetoMelCod
               }
               , new Object[] {
               T002E4_A940Contagem_SistemaCod, T002E4_n940Contagem_SistemaCod
               }
               , new Object[] {
               T002E5_A721Baseline_UserCod
               }
               , new Object[] {
               T002E6_A735Baseline_ProjetoMelCod
               }
               , new Object[] {
               T002E7_A722Baseline_Codigo, T002E7_A1164Baseline_DataHomologacao, T002E7_A1165Baseline_DataAtualizacao, T002E7_n1165Baseline_DataAtualizacao, T002E7_A726Baseline_PFBAntes, T002E7_n726Baseline_PFBAntes, T002E7_A727Baseline_PFBDepois, T002E7_n727Baseline_PFBDepois, T002E7_A1161Baseline_Antes, T002E7_n1161Baseline_Antes,
               T002E7_A1162Baseline_Depois, T002E7_n1162Baseline_Depois, T002E7_A192Contagem_Codigo, T002E7_A721Baseline_UserCod, T002E7_A735Baseline_ProjetoMelCod, T002E7_n735Baseline_ProjetoMelCod, T002E7_A940Contagem_SistemaCod, T002E7_n940Contagem_SistemaCod
               }
               , new Object[] {
               T002E8_A721Baseline_UserCod
               }
               , new Object[] {
               T002E9_A940Contagem_SistemaCod, T002E9_n940Contagem_SistemaCod
               }
               , new Object[] {
               T002E10_A735Baseline_ProjetoMelCod
               }
               , new Object[] {
               T002E11_A722Baseline_Codigo
               }
               , new Object[] {
               T002E12_A722Baseline_Codigo
               }
               , new Object[] {
               T002E13_A722Baseline_Codigo
               }
               , new Object[] {
               T002E14_A722Baseline_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002E17_A940Contagem_SistemaCod, T002E17_n940Contagem_SistemaCod
               }
               , new Object[] {
               T002E18_A722Baseline_Codigo
               }
               , new Object[] {
               T002E19_A721Baseline_UserCod
               }
               , new Object[] {
               T002E20_A735Baseline_ProjetoMelCod
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound92 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z722Baseline_Codigo ;
      private int Z192Contagem_Codigo ;
      private int Z721Baseline_UserCod ;
      private int Z735Baseline_ProjetoMelCod ;
      private int A721Baseline_UserCod ;
      private int A192Contagem_Codigo ;
      private int A735Baseline_ProjetoMelCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int A722Baseline_Codigo ;
      private int edtBaseline_Codigo_Enabled ;
      private int edtBaseline_DataHomologacao_Enabled ;
      private int edtBaseline_UserCod_Enabled ;
      private int edtContagem_Codigo_Enabled ;
      private int A940Contagem_SistemaCod ;
      private int edtContagem_SistemaCod_Enabled ;
      private int edtBaseline_DataAtualizacao_Enabled ;
      private int edtBaseline_PFBAntes_Enabled ;
      private int edtBaseline_PFBDepois_Enabled ;
      private int edtBaseline_ProjetoMelCod_Enabled ;
      private int edtBaseline_Antes_Enabled ;
      private int edtBaseline_Depois_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int Z940Contagem_SistemaCod ;
      private int idxLst ;
      private decimal Z726Baseline_PFBAntes ;
      private decimal Z727Baseline_PFBDepois ;
      private decimal A726Baseline_PFBAntes ;
      private decimal A727Baseline_PFBDepois ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtBaseline_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockbaseline_codigo_Internalname ;
      private String lblTextblockbaseline_codigo_Jsonclick ;
      private String edtBaseline_Codigo_Jsonclick ;
      private String lblTextblockbaseline_datahomologacao_Internalname ;
      private String lblTextblockbaseline_datahomologacao_Jsonclick ;
      private String edtBaseline_DataHomologacao_Internalname ;
      private String edtBaseline_DataHomologacao_Jsonclick ;
      private String lblTextblockbaseline_usercod_Internalname ;
      private String lblTextblockbaseline_usercod_Jsonclick ;
      private String edtBaseline_UserCod_Internalname ;
      private String edtBaseline_UserCod_Jsonclick ;
      private String lblTextblockcontagem_codigo_Internalname ;
      private String lblTextblockcontagem_codigo_Jsonclick ;
      private String edtContagem_Codigo_Internalname ;
      private String edtContagem_Codigo_Jsonclick ;
      private String lblTextblockcontagem_sistemacod_Internalname ;
      private String lblTextblockcontagem_sistemacod_Jsonclick ;
      private String edtContagem_SistemaCod_Internalname ;
      private String edtContagem_SistemaCod_Jsonclick ;
      private String lblTextblockbaseline_dataatualizacao_Internalname ;
      private String lblTextblockbaseline_dataatualizacao_Jsonclick ;
      private String edtBaseline_DataAtualizacao_Internalname ;
      private String edtBaseline_DataAtualizacao_Jsonclick ;
      private String lblTextblockbaseline_pfbantes_Internalname ;
      private String lblTextblockbaseline_pfbantes_Jsonclick ;
      private String edtBaseline_PFBAntes_Internalname ;
      private String edtBaseline_PFBAntes_Jsonclick ;
      private String lblTextblockbaseline_pfbdepois_Internalname ;
      private String lblTextblockbaseline_pfbdepois_Jsonclick ;
      private String edtBaseline_PFBDepois_Internalname ;
      private String edtBaseline_PFBDepois_Jsonclick ;
      private String lblTextblockbaseline_projetomelcod_Internalname ;
      private String lblTextblockbaseline_projetomelcod_Jsonclick ;
      private String edtBaseline_ProjetoMelCod_Internalname ;
      private String edtBaseline_ProjetoMelCod_Jsonclick ;
      private String lblTextblockbaseline_antes_Internalname ;
      private String lblTextblockbaseline_antes_Jsonclick ;
      private String edtBaseline_Antes_Internalname ;
      private String lblTextblockbaseline_depois_Internalname ;
      private String lblTextblockbaseline_depois_Jsonclick ;
      private String edtBaseline_Depois_Internalname ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode92 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private DateTime Z1164Baseline_DataHomologacao ;
      private DateTime Z1165Baseline_DataAtualizacao ;
      private DateTime A1164Baseline_DataHomologacao ;
      private DateTime A1165Baseline_DataAtualizacao ;
      private bool entryPointCalled ;
      private bool n735Baseline_ProjetoMelCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n940Contagem_SistemaCod ;
      private bool n1165Baseline_DataAtualizacao ;
      private bool n726Baseline_PFBAntes ;
      private bool n727Baseline_PFBDepois ;
      private bool n1161Baseline_Antes ;
      private bool n1162Baseline_Depois ;
      private bool Gx_longc ;
      private String A1161Baseline_Antes ;
      private String A1162Baseline_Depois ;
      private String Z1161Baseline_Antes ;
      private String Z1162Baseline_Depois ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T002E7_A722Baseline_Codigo ;
      private DateTime[] T002E7_A1164Baseline_DataHomologacao ;
      private DateTime[] T002E7_A1165Baseline_DataAtualizacao ;
      private bool[] T002E7_n1165Baseline_DataAtualizacao ;
      private decimal[] T002E7_A726Baseline_PFBAntes ;
      private bool[] T002E7_n726Baseline_PFBAntes ;
      private decimal[] T002E7_A727Baseline_PFBDepois ;
      private bool[] T002E7_n727Baseline_PFBDepois ;
      private String[] T002E7_A1161Baseline_Antes ;
      private bool[] T002E7_n1161Baseline_Antes ;
      private String[] T002E7_A1162Baseline_Depois ;
      private bool[] T002E7_n1162Baseline_Depois ;
      private int[] T002E7_A192Contagem_Codigo ;
      private int[] T002E7_A721Baseline_UserCod ;
      private int[] T002E7_A735Baseline_ProjetoMelCod ;
      private bool[] T002E7_n735Baseline_ProjetoMelCod ;
      private int[] T002E7_A940Contagem_SistemaCod ;
      private bool[] T002E7_n940Contagem_SistemaCod ;
      private int[] T002E5_A721Baseline_UserCod ;
      private int[] T002E4_A940Contagem_SistemaCod ;
      private bool[] T002E4_n940Contagem_SistemaCod ;
      private int[] T002E6_A735Baseline_ProjetoMelCod ;
      private bool[] T002E6_n735Baseline_ProjetoMelCod ;
      private int[] T002E8_A721Baseline_UserCod ;
      private int[] T002E9_A940Contagem_SistemaCod ;
      private bool[] T002E9_n940Contagem_SistemaCod ;
      private int[] T002E10_A735Baseline_ProjetoMelCod ;
      private bool[] T002E10_n735Baseline_ProjetoMelCod ;
      private int[] T002E11_A722Baseline_Codigo ;
      private int[] T002E3_A722Baseline_Codigo ;
      private DateTime[] T002E3_A1164Baseline_DataHomologacao ;
      private DateTime[] T002E3_A1165Baseline_DataAtualizacao ;
      private bool[] T002E3_n1165Baseline_DataAtualizacao ;
      private decimal[] T002E3_A726Baseline_PFBAntes ;
      private bool[] T002E3_n726Baseline_PFBAntes ;
      private decimal[] T002E3_A727Baseline_PFBDepois ;
      private bool[] T002E3_n727Baseline_PFBDepois ;
      private String[] T002E3_A1161Baseline_Antes ;
      private bool[] T002E3_n1161Baseline_Antes ;
      private String[] T002E3_A1162Baseline_Depois ;
      private bool[] T002E3_n1162Baseline_Depois ;
      private int[] T002E3_A192Contagem_Codigo ;
      private int[] T002E3_A721Baseline_UserCod ;
      private int[] T002E3_A735Baseline_ProjetoMelCod ;
      private bool[] T002E3_n735Baseline_ProjetoMelCod ;
      private int[] T002E12_A722Baseline_Codigo ;
      private int[] T002E13_A722Baseline_Codigo ;
      private int[] T002E2_A722Baseline_Codigo ;
      private DateTime[] T002E2_A1164Baseline_DataHomologacao ;
      private DateTime[] T002E2_A1165Baseline_DataAtualizacao ;
      private bool[] T002E2_n1165Baseline_DataAtualizacao ;
      private decimal[] T002E2_A726Baseline_PFBAntes ;
      private bool[] T002E2_n726Baseline_PFBAntes ;
      private decimal[] T002E2_A727Baseline_PFBDepois ;
      private bool[] T002E2_n727Baseline_PFBDepois ;
      private String[] T002E2_A1161Baseline_Antes ;
      private bool[] T002E2_n1161Baseline_Antes ;
      private String[] T002E2_A1162Baseline_Depois ;
      private bool[] T002E2_n1162Baseline_Depois ;
      private int[] T002E2_A192Contagem_Codigo ;
      private int[] T002E2_A721Baseline_UserCod ;
      private int[] T002E2_A735Baseline_ProjetoMelCod ;
      private bool[] T002E2_n735Baseline_ProjetoMelCod ;
      private int[] T002E14_A722Baseline_Codigo ;
      private int[] T002E17_A940Contagem_SistemaCod ;
      private bool[] T002E17_n940Contagem_SistemaCod ;
      private int[] T002E18_A722Baseline_Codigo ;
      private int[] T002E19_A721Baseline_UserCod ;
      private int[] T002E20_A735Baseline_ProjetoMelCod ;
      private bool[] T002E20_n735Baseline_ProjetoMelCod ;
      private GXWebForm Form ;
   }

   public class baseline__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002E7 ;
          prmT002E7 = new Object[] {
          new Object[] {"@Baseline_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002E5 ;
          prmT002E5 = new Object[] {
          new Object[] {"@Baseline_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002E4 ;
          prmT002E4 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002E6 ;
          prmT002E6 = new Object[] {
          new Object[] {"@Baseline_ProjetoMelCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002E8 ;
          prmT002E8 = new Object[] {
          new Object[] {"@Baseline_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002E9 ;
          prmT002E9 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002E10 ;
          prmT002E10 = new Object[] {
          new Object[] {"@Baseline_ProjetoMelCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002E11 ;
          prmT002E11 = new Object[] {
          new Object[] {"@Baseline_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002E3 ;
          prmT002E3 = new Object[] {
          new Object[] {"@Baseline_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002E12 ;
          prmT002E12 = new Object[] {
          new Object[] {"@Baseline_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002E13 ;
          prmT002E13 = new Object[] {
          new Object[] {"@Baseline_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002E2 ;
          prmT002E2 = new Object[] {
          new Object[] {"@Baseline_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002E14 ;
          prmT002E14 = new Object[] {
          new Object[] {"@Baseline_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Baseline_DataAtualizacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Baseline_PFBAntes",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Baseline_PFBDepois",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Baseline_Antes",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Baseline_Depois",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Baseline_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Baseline_ProjetoMelCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002E15 ;
          prmT002E15 = new Object[] {
          new Object[] {"@Baseline_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Baseline_DataAtualizacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Baseline_PFBAntes",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Baseline_PFBDepois",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Baseline_Antes",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Baseline_Depois",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Baseline_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Baseline_ProjetoMelCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Baseline_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002E16 ;
          prmT002E16 = new Object[] {
          new Object[] {"@Baseline_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002E18 ;
          prmT002E18 = new Object[] {
          } ;
          Object[] prmT002E19 ;
          prmT002E19 = new Object[] {
          new Object[] {"@Baseline_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002E17 ;
          prmT002E17 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002E20 ;
          prmT002E20 = new Object[] {
          new Object[] {"@Baseline_ProjetoMelCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002E2", "SELECT [Baseline_Codigo], [Baseline_DataHomologacao], [Baseline_DataAtualizacao], [Baseline_PFBAntes], [Baseline_PFBDepois], [Baseline_Antes], [Baseline_Depois], [Contagem_Codigo], [Baseline_UserCod] AS Baseline_UserCod, [Baseline_ProjetoMelCod] AS Baseline_ProjetoMelCod FROM [Baseline] WITH (UPDLOCK) WHERE [Baseline_Codigo] = @Baseline_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002E2,1,0,true,false )
             ,new CursorDef("T002E3", "SELECT [Baseline_Codigo], [Baseline_DataHomologacao], [Baseline_DataAtualizacao], [Baseline_PFBAntes], [Baseline_PFBDepois], [Baseline_Antes], [Baseline_Depois], [Contagem_Codigo], [Baseline_UserCod] AS Baseline_UserCod, [Baseline_ProjetoMelCod] AS Baseline_ProjetoMelCod FROM [Baseline] WITH (NOLOCK) WHERE [Baseline_Codigo] = @Baseline_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002E3,1,0,true,false )
             ,new CursorDef("T002E4", "SELECT [Contagem_SistemaCod] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_Codigo] = @Contagem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002E4,1,0,true,false )
             ,new CursorDef("T002E5", "SELECT [Usuario_Codigo] AS Baseline_UserCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Baseline_UserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002E5,1,0,true,false )
             ,new CursorDef("T002E6", "SELECT [ProjetoMelhoria_Codigo] AS Baseline_ProjetoMelCod FROM [ProjetoMelhoria] WITH (NOLOCK) WHERE [ProjetoMelhoria_Codigo] = @Baseline_ProjetoMelCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002E6,1,0,true,false )
             ,new CursorDef("T002E7", "SELECT TM1.[Baseline_Codigo], TM1.[Baseline_DataHomologacao], TM1.[Baseline_DataAtualizacao], TM1.[Baseline_PFBAntes], TM1.[Baseline_PFBDepois], TM1.[Baseline_Antes], TM1.[Baseline_Depois], TM1.[Contagem_Codigo], TM1.[Baseline_UserCod] AS Baseline_UserCod, TM1.[Baseline_ProjetoMelCod] AS Baseline_ProjetoMelCod, T2.[Contagem_SistemaCod] FROM ([Baseline] TM1 WITH (NOLOCK) INNER JOIN [Contagem] T2 WITH (NOLOCK) ON T2.[Contagem_Codigo] = TM1.[Contagem_Codigo]) WHERE TM1.[Baseline_Codigo] = @Baseline_Codigo ORDER BY TM1.[Baseline_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002E7,100,0,true,false )
             ,new CursorDef("T002E8", "SELECT [Usuario_Codigo] AS Baseline_UserCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Baseline_UserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002E8,1,0,true,false )
             ,new CursorDef("T002E9", "SELECT [Contagem_SistemaCod] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_Codigo] = @Contagem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002E9,1,0,true,false )
             ,new CursorDef("T002E10", "SELECT [ProjetoMelhoria_Codigo] AS Baseline_ProjetoMelCod FROM [ProjetoMelhoria] WITH (NOLOCK) WHERE [ProjetoMelhoria_Codigo] = @Baseline_ProjetoMelCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002E10,1,0,true,false )
             ,new CursorDef("T002E11", "SELECT [Baseline_Codigo] FROM [Baseline] WITH (NOLOCK) WHERE [Baseline_Codigo] = @Baseline_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002E11,1,0,true,false )
             ,new CursorDef("T002E12", "SELECT TOP 1 [Baseline_Codigo] FROM [Baseline] WITH (NOLOCK) WHERE ( [Baseline_Codigo] > @Baseline_Codigo) ORDER BY [Baseline_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002E12,1,0,true,true )
             ,new CursorDef("T002E13", "SELECT TOP 1 [Baseline_Codigo] FROM [Baseline] WITH (NOLOCK) WHERE ( [Baseline_Codigo] < @Baseline_Codigo) ORDER BY [Baseline_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002E13,1,0,true,true )
             ,new CursorDef("T002E14", "INSERT INTO [Baseline]([Baseline_DataHomologacao], [Baseline_DataAtualizacao], [Baseline_PFBAntes], [Baseline_PFBDepois], [Baseline_Antes], [Baseline_Depois], [Contagem_Codigo], [Baseline_UserCod], [Baseline_ProjetoMelCod]) VALUES(@Baseline_DataHomologacao, @Baseline_DataAtualizacao, @Baseline_PFBAntes, @Baseline_PFBDepois, @Baseline_Antes, @Baseline_Depois, @Contagem_Codigo, @Baseline_UserCod, @Baseline_ProjetoMelCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002E14)
             ,new CursorDef("T002E15", "UPDATE [Baseline] SET [Baseline_DataHomologacao]=@Baseline_DataHomologacao, [Baseline_DataAtualizacao]=@Baseline_DataAtualizacao, [Baseline_PFBAntes]=@Baseline_PFBAntes, [Baseline_PFBDepois]=@Baseline_PFBDepois, [Baseline_Antes]=@Baseline_Antes, [Baseline_Depois]=@Baseline_Depois, [Contagem_Codigo]=@Contagem_Codigo, [Baseline_UserCod]=@Baseline_UserCod, [Baseline_ProjetoMelCod]=@Baseline_ProjetoMelCod  WHERE [Baseline_Codigo] = @Baseline_Codigo", GxErrorMask.GX_NOMASK,prmT002E15)
             ,new CursorDef("T002E16", "DELETE FROM [Baseline]  WHERE [Baseline_Codigo] = @Baseline_Codigo", GxErrorMask.GX_NOMASK,prmT002E16)
             ,new CursorDef("T002E17", "SELECT [Contagem_SistemaCod] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_Codigo] = @Contagem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002E17,1,0,true,false )
             ,new CursorDef("T002E18", "SELECT [Baseline_Codigo] FROM [Baseline] WITH (NOLOCK) ORDER BY [Baseline_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002E18,100,0,true,false )
             ,new CursorDef("T002E19", "SELECT [Usuario_Codigo] AS Baseline_UserCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Baseline_UserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002E19,1,0,true,false )
             ,new CursorDef("T002E20", "SELECT [ProjetoMelhoria_Codigo] AS Baseline_ProjetoMelCod FROM [ProjetoMelhoria] WITH (NOLOCK) WHERE [ProjetoMelhoria_Codigo] = @Baseline_ProjetoMelCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002E20,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[10]);
                }
                stmt.SetParameter(7, (int)parms[11]);
                stmt.SetParameter(8, (int)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[14]);
                }
                return;
             case 13 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[10]);
                }
                stmt.SetParameter(7, (int)parms[11]);
                stmt.SetParameter(8, (int)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[14]);
                }
                stmt.SetParameter(10, (int)parms[15]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
