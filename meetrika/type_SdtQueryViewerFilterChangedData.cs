/*
               File: type_SdtQueryViewerFilterChangedData
        Description: QueryViewerFilterChangedData
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:57.82
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "QueryViewerFilterChangedData" )]
   [XmlType(TypeName =  "QueryViewerFilterChangedData" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtQueryViewerFilterChangedData : GxUserType
   {
      public SdtQueryViewerFilterChangedData( )
      {
         /* Constructor for serialization */
         gxTv_SdtQueryViewerFilterChangedData_Name = "";
      }

      public SdtQueryViewerFilterChangedData( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtQueryViewerFilterChangedData deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtQueryViewerFilterChangedData)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtQueryViewerFilterChangedData obj ;
         obj = this;
         obj.gxTpr_Name = deserialized.gxTpr_Name;
         obj.gxTpr_Selectedvalues = deserialized.gxTpr_Selectedvalues;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Name") )
               {
                  gxTv_SdtQueryViewerFilterChangedData_Name = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SelectedValues") )
               {
                  if ( gxTv_SdtQueryViewerFilterChangedData_Selectedvalues == null )
                  {
                     gxTv_SdtQueryViewerFilterChangedData_Selectedvalues = new GxSimpleCollection();
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtQueryViewerFilterChangedData_Selectedvalues.readxmlcollection(oReader, "SelectedValues", "Item");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "QueryViewerFilterChangedData";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Name", StringUtil.RTrim( gxTv_SdtQueryViewerFilterChangedData_Name));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( gxTv_SdtQueryViewerFilterChangedData_Selectedvalues != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_Meetrika";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_Meetrika";
            }
            gxTv_SdtQueryViewerFilterChangedData_Selectedvalues.writexmlcollection(oWriter, "SelectedValues", sNameSpace1, "Item", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Name", gxTv_SdtQueryViewerFilterChangedData_Name, false);
         if ( gxTv_SdtQueryViewerFilterChangedData_Selectedvalues != null )
         {
            AddObjectProperty("SelectedValues", gxTv_SdtQueryViewerFilterChangedData_Selectedvalues, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Name" )]
      [  XmlElement( ElementName = "Name"   )]
      public String gxTpr_Name
      {
         get {
            return gxTv_SdtQueryViewerFilterChangedData_Name ;
         }

         set {
            gxTv_SdtQueryViewerFilterChangedData_Name = (String)(value);
         }

      }

      [  SoapElement( ElementName = "SelectedValues" )]
      [  XmlArray( ElementName = "SelectedValues"  )]
      [  XmlArrayItemAttribute( Type= typeof( String ), ElementName= "Item"  , IsNullable=false)]
      public GxSimpleCollection gxTpr_Selectedvalues_GxSimpleCollection
      {
         get {
            if ( gxTv_SdtQueryViewerFilterChangedData_Selectedvalues == null )
            {
               gxTv_SdtQueryViewerFilterChangedData_Selectedvalues = new GxSimpleCollection();
            }
            return (GxSimpleCollection)gxTv_SdtQueryViewerFilterChangedData_Selectedvalues ;
         }

         set {
            if ( gxTv_SdtQueryViewerFilterChangedData_Selectedvalues == null )
            {
               gxTv_SdtQueryViewerFilterChangedData_Selectedvalues = new GxSimpleCollection();
            }
            gxTv_SdtQueryViewerFilterChangedData_Selectedvalues = (GxSimpleCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Selectedvalues
      {
         get {
            if ( gxTv_SdtQueryViewerFilterChangedData_Selectedvalues == null )
            {
               gxTv_SdtQueryViewerFilterChangedData_Selectedvalues = new GxSimpleCollection();
            }
            return gxTv_SdtQueryViewerFilterChangedData_Selectedvalues ;
         }

         set {
            gxTv_SdtQueryViewerFilterChangedData_Selectedvalues = value;
         }

      }

      public void gxTv_SdtQueryViewerFilterChangedData_Selectedvalues_SetNull( )
      {
         gxTv_SdtQueryViewerFilterChangedData_Selectedvalues = null;
         return  ;
      }

      public bool gxTv_SdtQueryViewerFilterChangedData_Selectedvalues_IsNull( )
      {
         if ( gxTv_SdtQueryViewerFilterChangedData_Selectedvalues == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtQueryViewerFilterChangedData_Name = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtQueryViewerFilterChangedData_Name ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( String ))]
      protected IGxCollection gxTv_SdtQueryViewerFilterChangedData_Selectedvalues=null ;
   }

   [DataContract(Name = @"QueryViewerFilterChangedData", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtQueryViewerFilterChangedData_RESTInterface : GxGenericCollectionItem<SdtQueryViewerFilterChangedData>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtQueryViewerFilterChangedData_RESTInterface( ) : base()
      {
      }

      public SdtQueryViewerFilterChangedData_RESTInterface( SdtQueryViewerFilterChangedData psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Name" , Order = 0 )]
      public String gxTpr_Name
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Name) ;
         }

         set {
            sdt.gxTpr_Name = (String)(value);
         }

      }

      [DataMember( Name = "SelectedValues" , Order = 1 )]
      public GxSimpleCollection gxTpr_Selectedvalues
      {
         get {
            return (GxSimpleCollection)(sdt.gxTpr_Selectedvalues) ;
         }

         set {
            sdt.gxTpr_Selectedvalues = value;
         }

      }

      public SdtQueryViewerFilterChangedData sdt
      {
         get {
            return (SdtQueryViewerFilterChangedData)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtQueryViewerFilterChangedData() ;
         }
      }

   }

}
