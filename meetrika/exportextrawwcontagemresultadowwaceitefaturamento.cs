/*
               File: ExportExtraWWContagemResultadoWWAceiteFaturamento
        Description: Stub for ExportExtraWWContagemResultadoWWAceiteFaturamento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 22:58:31.3
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class exportextrawwcontagemresultadowwaceitefaturamento : GXProcedure
   {
      public exportextrawwcontagemresultadowwaceitefaturamento( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public exportextrawwcontagemresultadowwaceitefaturamento( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           int aP1_Contratada_Codigo ,
                           short aP2_ContagemResultado_StatusCnt ,
                           String aP3_ContagemResultado_StatusDmn ,
                           String aP4_TFContagemResultado_LoteAceite ,
                           String aP5_TFContagemResultado_LoteAceite_Sel ,
                           String aP6_TFContagemResultado_OsFsOsFm ,
                           String aP7_TFContagemResultado_OsFsOsFm_Sel ,
                           String aP8_TFContagemResultado_Descricao ,
                           String aP9_TFContagemResultado_Descricao_Sel ,
                           DateTime aP10_TFContagemResultado_DataAceite ,
                           DateTime aP11_TFContagemResultado_DataAceite_To ,
                           DateTime aP12_TFContagemResultado_DataUltCnt ,
                           DateTime aP13_TFContagemResultado_DataUltCnt_To ,
                           String aP14_TFContagemResultado_ContratadaOrigemSigla ,
                           String aP15_TFContagemResultado_ContratadaOrigemSigla_Sel ,
                           String aP16_TFContagemrResultado_SistemaSigla ,
                           String aP17_TFContagemrResultado_SistemaSigla_Sel ,
                           String aP18_TFContagemResultado_StatusDmn_SelsJson ,
                           short aP19_TFContagemResultado_Baseline_Sel ,
                           String aP20_TFContagemResultado_ServicoSigla ,
                           String aP21_TFContagemResultado_ServicoSigla_Sel ,
                           decimal aP22_TFContagemResultado_PFFinal ,
                           decimal aP23_TFContagemResultado_PFFinal_To ,
                           decimal aP24_TFContagemResultado_ValorPF ,
                           decimal aP25_TFContagemResultado_ValorPF_To ,
                           short aP26_OrderedBy ,
                           String aP27_GridStateXML ,
                           out String aP28_Filename ,
                           out String aP29_ErrorMessage )
      {
         this.AV2Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV3Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV4ContagemResultado_StatusCnt = aP2_ContagemResultado_StatusCnt;
         this.AV5ContagemResultado_StatusDmn = aP3_ContagemResultado_StatusDmn;
         this.AV6TFContagemResultado_LoteAceite = aP4_TFContagemResultado_LoteAceite;
         this.AV7TFContagemResultado_LoteAceite_Sel = aP5_TFContagemResultado_LoteAceite_Sel;
         this.AV8TFContagemResultado_OsFsOsFm = aP6_TFContagemResultado_OsFsOsFm;
         this.AV9TFContagemResultado_OsFsOsFm_Sel = aP7_TFContagemResultado_OsFsOsFm_Sel;
         this.AV10TFContagemResultado_Descricao = aP8_TFContagemResultado_Descricao;
         this.AV11TFContagemResultado_Descricao_Sel = aP9_TFContagemResultado_Descricao_Sel;
         this.AV12TFContagemResultado_DataAceite = aP10_TFContagemResultado_DataAceite;
         this.AV13TFContagemResultado_DataAceite_To = aP11_TFContagemResultado_DataAceite_To;
         this.AV14TFContagemResultado_DataUltCnt = aP12_TFContagemResultado_DataUltCnt;
         this.AV15TFContagemResultado_DataUltCnt_To = aP13_TFContagemResultado_DataUltCnt_To;
         this.AV16TFContagemResultado_ContratadaOrigemSigla = aP14_TFContagemResultado_ContratadaOrigemSigla;
         this.AV17TFContagemResultado_ContratadaOrigemSigla_Sel = aP15_TFContagemResultado_ContratadaOrigemSigla_Sel;
         this.AV18TFContagemrResultado_SistemaSigla = aP16_TFContagemrResultado_SistemaSigla;
         this.AV19TFContagemrResultado_SistemaSigla_Sel = aP17_TFContagemrResultado_SistemaSigla_Sel;
         this.AV20TFContagemResultado_StatusDmn_SelsJson = aP18_TFContagemResultado_StatusDmn_SelsJson;
         this.AV21TFContagemResultado_Baseline_Sel = aP19_TFContagemResultado_Baseline_Sel;
         this.AV22TFContagemResultado_ServicoSigla = aP20_TFContagemResultado_ServicoSigla;
         this.AV23TFContagemResultado_ServicoSigla_Sel = aP21_TFContagemResultado_ServicoSigla_Sel;
         this.AV24TFContagemResultado_PFFinal = aP22_TFContagemResultado_PFFinal;
         this.AV25TFContagemResultado_PFFinal_To = aP23_TFContagemResultado_PFFinal_To;
         this.AV26TFContagemResultado_ValorPF = aP24_TFContagemResultado_ValorPF;
         this.AV27TFContagemResultado_ValorPF_To = aP25_TFContagemResultado_ValorPF_To;
         this.AV28OrderedBy = aP26_OrderedBy;
         this.AV29GridStateXML = aP27_GridStateXML;
         this.AV30Filename = "" ;
         this.AV31ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP28_Filename=this.AV30Filename;
         aP29_ErrorMessage=this.AV31ErrorMessage;
      }

      public String executeUdp( int aP0_Contratada_AreaTrabalhoCod ,
                                int aP1_Contratada_Codigo ,
                                short aP2_ContagemResultado_StatusCnt ,
                                String aP3_ContagemResultado_StatusDmn ,
                                String aP4_TFContagemResultado_LoteAceite ,
                                String aP5_TFContagemResultado_LoteAceite_Sel ,
                                String aP6_TFContagemResultado_OsFsOsFm ,
                                String aP7_TFContagemResultado_OsFsOsFm_Sel ,
                                String aP8_TFContagemResultado_Descricao ,
                                String aP9_TFContagemResultado_Descricao_Sel ,
                                DateTime aP10_TFContagemResultado_DataAceite ,
                                DateTime aP11_TFContagemResultado_DataAceite_To ,
                                DateTime aP12_TFContagemResultado_DataUltCnt ,
                                DateTime aP13_TFContagemResultado_DataUltCnt_To ,
                                String aP14_TFContagemResultado_ContratadaOrigemSigla ,
                                String aP15_TFContagemResultado_ContratadaOrigemSigla_Sel ,
                                String aP16_TFContagemrResultado_SistemaSigla ,
                                String aP17_TFContagemrResultado_SistemaSigla_Sel ,
                                String aP18_TFContagemResultado_StatusDmn_SelsJson ,
                                short aP19_TFContagemResultado_Baseline_Sel ,
                                String aP20_TFContagemResultado_ServicoSigla ,
                                String aP21_TFContagemResultado_ServicoSigla_Sel ,
                                decimal aP22_TFContagemResultado_PFFinal ,
                                decimal aP23_TFContagemResultado_PFFinal_To ,
                                decimal aP24_TFContagemResultado_ValorPF ,
                                decimal aP25_TFContagemResultado_ValorPF_To ,
                                short aP26_OrderedBy ,
                                String aP27_GridStateXML ,
                                out String aP28_Filename )
      {
         this.AV2Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV3Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV4ContagemResultado_StatusCnt = aP2_ContagemResultado_StatusCnt;
         this.AV5ContagemResultado_StatusDmn = aP3_ContagemResultado_StatusDmn;
         this.AV6TFContagemResultado_LoteAceite = aP4_TFContagemResultado_LoteAceite;
         this.AV7TFContagemResultado_LoteAceite_Sel = aP5_TFContagemResultado_LoteAceite_Sel;
         this.AV8TFContagemResultado_OsFsOsFm = aP6_TFContagemResultado_OsFsOsFm;
         this.AV9TFContagemResultado_OsFsOsFm_Sel = aP7_TFContagemResultado_OsFsOsFm_Sel;
         this.AV10TFContagemResultado_Descricao = aP8_TFContagemResultado_Descricao;
         this.AV11TFContagemResultado_Descricao_Sel = aP9_TFContagemResultado_Descricao_Sel;
         this.AV12TFContagemResultado_DataAceite = aP10_TFContagemResultado_DataAceite;
         this.AV13TFContagemResultado_DataAceite_To = aP11_TFContagemResultado_DataAceite_To;
         this.AV14TFContagemResultado_DataUltCnt = aP12_TFContagemResultado_DataUltCnt;
         this.AV15TFContagemResultado_DataUltCnt_To = aP13_TFContagemResultado_DataUltCnt_To;
         this.AV16TFContagemResultado_ContratadaOrigemSigla = aP14_TFContagemResultado_ContratadaOrigemSigla;
         this.AV17TFContagemResultado_ContratadaOrigemSigla_Sel = aP15_TFContagemResultado_ContratadaOrigemSigla_Sel;
         this.AV18TFContagemrResultado_SistemaSigla = aP16_TFContagemrResultado_SistemaSigla;
         this.AV19TFContagemrResultado_SistemaSigla_Sel = aP17_TFContagemrResultado_SistemaSigla_Sel;
         this.AV20TFContagemResultado_StatusDmn_SelsJson = aP18_TFContagemResultado_StatusDmn_SelsJson;
         this.AV21TFContagemResultado_Baseline_Sel = aP19_TFContagemResultado_Baseline_Sel;
         this.AV22TFContagemResultado_ServicoSigla = aP20_TFContagemResultado_ServicoSigla;
         this.AV23TFContagemResultado_ServicoSigla_Sel = aP21_TFContagemResultado_ServicoSigla_Sel;
         this.AV24TFContagemResultado_PFFinal = aP22_TFContagemResultado_PFFinal;
         this.AV25TFContagemResultado_PFFinal_To = aP23_TFContagemResultado_PFFinal_To;
         this.AV26TFContagemResultado_ValorPF = aP24_TFContagemResultado_ValorPF;
         this.AV27TFContagemResultado_ValorPF_To = aP25_TFContagemResultado_ValorPF_To;
         this.AV28OrderedBy = aP26_OrderedBy;
         this.AV29GridStateXML = aP27_GridStateXML;
         this.AV30Filename = "" ;
         this.AV31ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP28_Filename=this.AV30Filename;
         aP29_ErrorMessage=this.AV31ErrorMessage;
         return AV31ErrorMessage ;
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 int aP1_Contratada_Codigo ,
                                 short aP2_ContagemResultado_StatusCnt ,
                                 String aP3_ContagemResultado_StatusDmn ,
                                 String aP4_TFContagemResultado_LoteAceite ,
                                 String aP5_TFContagemResultado_LoteAceite_Sel ,
                                 String aP6_TFContagemResultado_OsFsOsFm ,
                                 String aP7_TFContagemResultado_OsFsOsFm_Sel ,
                                 String aP8_TFContagemResultado_Descricao ,
                                 String aP9_TFContagemResultado_Descricao_Sel ,
                                 DateTime aP10_TFContagemResultado_DataAceite ,
                                 DateTime aP11_TFContagemResultado_DataAceite_To ,
                                 DateTime aP12_TFContagemResultado_DataUltCnt ,
                                 DateTime aP13_TFContagemResultado_DataUltCnt_To ,
                                 String aP14_TFContagemResultado_ContratadaOrigemSigla ,
                                 String aP15_TFContagemResultado_ContratadaOrigemSigla_Sel ,
                                 String aP16_TFContagemrResultado_SistemaSigla ,
                                 String aP17_TFContagemrResultado_SistemaSigla_Sel ,
                                 String aP18_TFContagemResultado_StatusDmn_SelsJson ,
                                 short aP19_TFContagemResultado_Baseline_Sel ,
                                 String aP20_TFContagemResultado_ServicoSigla ,
                                 String aP21_TFContagemResultado_ServicoSigla_Sel ,
                                 decimal aP22_TFContagemResultado_PFFinal ,
                                 decimal aP23_TFContagemResultado_PFFinal_To ,
                                 decimal aP24_TFContagemResultado_ValorPF ,
                                 decimal aP25_TFContagemResultado_ValorPF_To ,
                                 short aP26_OrderedBy ,
                                 String aP27_GridStateXML ,
                                 out String aP28_Filename ,
                                 out String aP29_ErrorMessage )
      {
         exportextrawwcontagemresultadowwaceitefaturamento objexportextrawwcontagemresultadowwaceitefaturamento;
         objexportextrawwcontagemresultadowwaceitefaturamento = new exportextrawwcontagemresultadowwaceitefaturamento();
         objexportextrawwcontagemresultadowwaceitefaturamento.AV2Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV3Contratada_Codigo = aP1_Contratada_Codigo;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV4ContagemResultado_StatusCnt = aP2_ContagemResultado_StatusCnt;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV5ContagemResultado_StatusDmn = aP3_ContagemResultado_StatusDmn;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV6TFContagemResultado_LoteAceite = aP4_TFContagemResultado_LoteAceite;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV7TFContagemResultado_LoteAceite_Sel = aP5_TFContagemResultado_LoteAceite_Sel;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV8TFContagemResultado_OsFsOsFm = aP6_TFContagemResultado_OsFsOsFm;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV9TFContagemResultado_OsFsOsFm_Sel = aP7_TFContagemResultado_OsFsOsFm_Sel;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV10TFContagemResultado_Descricao = aP8_TFContagemResultado_Descricao;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV11TFContagemResultado_Descricao_Sel = aP9_TFContagemResultado_Descricao_Sel;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV12TFContagemResultado_DataAceite = aP10_TFContagemResultado_DataAceite;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV13TFContagemResultado_DataAceite_To = aP11_TFContagemResultado_DataAceite_To;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV14TFContagemResultado_DataUltCnt = aP12_TFContagemResultado_DataUltCnt;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV15TFContagemResultado_DataUltCnt_To = aP13_TFContagemResultado_DataUltCnt_To;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV16TFContagemResultado_ContratadaOrigemSigla = aP14_TFContagemResultado_ContratadaOrigemSigla;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV17TFContagemResultado_ContratadaOrigemSigla_Sel = aP15_TFContagemResultado_ContratadaOrigemSigla_Sel;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV18TFContagemrResultado_SistemaSigla = aP16_TFContagemrResultado_SistemaSigla;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV19TFContagemrResultado_SistemaSigla_Sel = aP17_TFContagemrResultado_SistemaSigla_Sel;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV20TFContagemResultado_StatusDmn_SelsJson = aP18_TFContagemResultado_StatusDmn_SelsJson;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV21TFContagemResultado_Baseline_Sel = aP19_TFContagemResultado_Baseline_Sel;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV22TFContagemResultado_ServicoSigla = aP20_TFContagemResultado_ServicoSigla;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV23TFContagemResultado_ServicoSigla_Sel = aP21_TFContagemResultado_ServicoSigla_Sel;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV24TFContagemResultado_PFFinal = aP22_TFContagemResultado_PFFinal;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV25TFContagemResultado_PFFinal_To = aP23_TFContagemResultado_PFFinal_To;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV26TFContagemResultado_ValorPF = aP24_TFContagemResultado_ValorPF;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV27TFContagemResultado_ValorPF_To = aP25_TFContagemResultado_ValorPF_To;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV28OrderedBy = aP26_OrderedBy;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV29GridStateXML = aP27_GridStateXML;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV30Filename = "" ;
         objexportextrawwcontagemresultadowwaceitefaturamento.AV31ErrorMessage = "" ;
         objexportextrawwcontagemresultadowwaceitefaturamento.context.SetSubmitInitialConfig(context);
         objexportextrawwcontagemresultadowwaceitefaturamento.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objexportextrawwcontagemresultadowwaceitefaturamento);
         aP28_Filename=this.AV30Filename;
         aP29_ErrorMessage=this.AV31ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((exportextrawwcontagemresultadowwaceitefaturamento)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(int)AV2Contratada_AreaTrabalhoCod,(int)AV3Contratada_Codigo,(short)AV4ContagemResultado_StatusCnt,(String)AV5ContagemResultado_StatusDmn,(String)AV6TFContagemResultado_LoteAceite,(String)AV7TFContagemResultado_LoteAceite_Sel,(String)AV8TFContagemResultado_OsFsOsFm,(String)AV9TFContagemResultado_OsFsOsFm_Sel,(String)AV10TFContagemResultado_Descricao,(String)AV11TFContagemResultado_Descricao_Sel,(DateTime)AV12TFContagemResultado_DataAceite,(DateTime)AV13TFContagemResultado_DataAceite_To,(DateTime)AV14TFContagemResultado_DataUltCnt,(DateTime)AV15TFContagemResultado_DataUltCnt_To,(String)AV16TFContagemResultado_ContratadaOrigemSigla,(String)AV17TFContagemResultado_ContratadaOrigemSigla_Sel,(String)AV18TFContagemrResultado_SistemaSigla,(String)AV19TFContagemrResultado_SistemaSigla_Sel,(String)AV20TFContagemResultado_StatusDmn_SelsJson,(short)AV21TFContagemResultado_Baseline_Sel,(String)AV22TFContagemResultado_ServicoSigla,(String)AV23TFContagemResultado_ServicoSigla_Sel,(decimal)AV24TFContagemResultado_PFFinal,(decimal)AV25TFContagemResultado_PFFinal_To,(decimal)AV26TFContagemResultado_ValorPF,(decimal)AV27TFContagemResultado_ValorPF_To,(short)AV28OrderedBy,(String)AV29GridStateXML,(String)AV30Filename,(String)AV31ErrorMessage} ;
         ClassLoader.Execute("aexportextrawwcontagemresultadowwaceitefaturamento","GeneXus.Programs.aexportextrawwcontagemresultadowwaceitefaturamento", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 30 ) )
         {
            AV30Filename = (String)(args[28]) ;
            AV31ErrorMessage = (String)(args[29]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV4ContagemResultado_StatusCnt ;
      private short AV21TFContagemResultado_Baseline_Sel ;
      private short AV28OrderedBy ;
      private int AV2Contratada_AreaTrabalhoCod ;
      private int AV3Contratada_Codigo ;
      private decimal AV24TFContagemResultado_PFFinal ;
      private decimal AV25TFContagemResultado_PFFinal_To ;
      private decimal AV26TFContagemResultado_ValorPF ;
      private decimal AV27TFContagemResultado_ValorPF_To ;
      private String AV5ContagemResultado_StatusDmn ;
      private String AV6TFContagemResultado_LoteAceite ;
      private String AV7TFContagemResultado_LoteAceite_Sel ;
      private String AV16TFContagemResultado_ContratadaOrigemSigla ;
      private String AV17TFContagemResultado_ContratadaOrigemSigla_Sel ;
      private String AV18TFContagemrResultado_SistemaSigla ;
      private String AV19TFContagemrResultado_SistemaSigla_Sel ;
      private String AV22TFContagemResultado_ServicoSigla ;
      private String AV23TFContagemResultado_ServicoSigla_Sel ;
      private DateTime AV12TFContagemResultado_DataAceite ;
      private DateTime AV13TFContagemResultado_DataAceite_To ;
      private DateTime AV14TFContagemResultado_DataUltCnt ;
      private DateTime AV15TFContagemResultado_DataUltCnt_To ;
      private String AV20TFContagemResultado_StatusDmn_SelsJson ;
      private String AV29GridStateXML ;
      private String AV8TFContagemResultado_OsFsOsFm ;
      private String AV9TFContagemResultado_OsFsOsFm_Sel ;
      private String AV10TFContagemResultado_Descricao ;
      private String AV11TFContagemResultado_Descricao_Sel ;
      private String AV31ErrorMessage ;
      private String AV30Filename ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
      private String aP28_Filename ;
      private String aP29_ErrorMessage ;
   }

}
