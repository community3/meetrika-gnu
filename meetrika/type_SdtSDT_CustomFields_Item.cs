/*
               File: type_SdtSDT_CustomFields_Item
        Description: SDT_CustomFields
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:59.14
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_CustomFields.Item" )]
   [XmlType(TypeName =  "SDT_CustomFields.Item" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSDT_CustomFields_Item : GxUserType
   {
      public SdtSDT_CustomFields_Item( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_CustomFields_Item_Name = "";
         gxTv_SdtSDT_CustomFields_Item_Value = "";
         gxTv_SdtSDT_CustomFields_Item_Para = "";
      }

      public SdtSDT_CustomFields_Item( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_CustomFields_Item deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_CustomFields_Item)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_CustomFields_Item obj ;
         obj = this;
         obj.gxTpr_Id = deserialized.gxTpr_Id;
         obj.gxTpr_Name = deserialized.gxTpr_Name;
         obj.gxTpr_Value = deserialized.gxTpr_Value;
         obj.gxTpr_Para = deserialized.gxTpr_Para;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Id") )
               {
                  gxTv_SdtSDT_CustomFields_Item_Id = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Name") )
               {
                  gxTv_SdtSDT_CustomFields_Item_Name = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Value") )
               {
                  gxTv_SdtSDT_CustomFields_Item_Value = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Para") )
               {
                  gxTv_SdtSDT_CustomFields_Item_Para = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_CustomFields.Item";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Id", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_CustomFields_Item_Id), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Name", StringUtil.RTrim( gxTv_SdtSDT_CustomFields_Item_Name));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Value", StringUtil.RTrim( gxTv_SdtSDT_CustomFields_Item_Value));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Para", StringUtil.RTrim( gxTv_SdtSDT_CustomFields_Item_Para));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Id", gxTv_SdtSDT_CustomFields_Item_Id, false);
         AddObjectProperty("Name", gxTv_SdtSDT_CustomFields_Item_Name, false);
         AddObjectProperty("Value", gxTv_SdtSDT_CustomFields_Item_Value, false);
         AddObjectProperty("Para", gxTv_SdtSDT_CustomFields_Item_Para, false);
         return  ;
      }

      [  SoapElement( ElementName = "Id" )]
      [  XmlElement( ElementName = "Id"   )]
      public short gxTpr_Id
      {
         get {
            return gxTv_SdtSDT_CustomFields_Item_Id ;
         }

         set {
            gxTv_SdtSDT_CustomFields_Item_Id = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Name" )]
      [  XmlElement( ElementName = "Name"   )]
      public String gxTpr_Name
      {
         get {
            return gxTv_SdtSDT_CustomFields_Item_Name ;
         }

         set {
            gxTv_SdtSDT_CustomFields_Item_Name = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Value" )]
      [  XmlElement( ElementName = "Value"   )]
      public String gxTpr_Value
      {
         get {
            return gxTv_SdtSDT_CustomFields_Item_Value ;
         }

         set {
            gxTv_SdtSDT_CustomFields_Item_Value = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Para" )]
      [  XmlElement( ElementName = "Para"   )]
      public String gxTpr_Para
      {
         get {
            return gxTv_SdtSDT_CustomFields_Item_Para ;
         }

         set {
            gxTv_SdtSDT_CustomFields_Item_Para = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_CustomFields_Item_Name = "";
         gxTv_SdtSDT_CustomFields_Item_Value = "";
         gxTv_SdtSDT_CustomFields_Item_Para = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_CustomFields_Item_Id ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_CustomFields_Item_Name ;
      protected String gxTv_SdtSDT_CustomFields_Item_Value ;
      protected String gxTv_SdtSDT_CustomFields_Item_Para ;
      protected String sTagName ;
   }

   [DataContract(Name = @"SDT_CustomFields.Item", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSDT_CustomFields_Item_RESTInterface : GxGenericCollectionItem<SdtSDT_CustomFields_Item>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_CustomFields_Item_RESTInterface( ) : base()
      {
      }

      public SdtSDT_CustomFields_Item_RESTInterface( SdtSDT_CustomFields_Item psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Id" , Order = 0 )]
      public Nullable<short> gxTpr_Id
      {
         get {
            return sdt.gxTpr_Id ;
         }

         set {
            sdt.gxTpr_Id = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Name" , Order = 1 )]
      public String gxTpr_Name
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Name) ;
         }

         set {
            sdt.gxTpr_Name = (String)(value);
         }

      }

      [DataMember( Name = "Value" , Order = 2 )]
      public String gxTpr_Value
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Value) ;
         }

         set {
            sdt.gxTpr_Value = (String)(value);
         }

      }

      [DataMember( Name = "Para" , Order = 3 )]
      public String gxTpr_Para
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Para) ;
         }

         set {
            sdt.gxTpr_Para = (String)(value);
         }

      }

      public SdtSDT_CustomFields_Item sdt
      {
         get {
            return (SdtSDT_CustomFields_Item)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_CustomFields_Item() ;
         }
      }

   }

}
