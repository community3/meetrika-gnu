/*
               File: REL_ContagemOS
        Description: Relatorio Contagem OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:14:14.93
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class arel_contagemos : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV45ContagemResultado_StatusCnt = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV77GridStateXML = GetNextPar( );
                  AV8ContagemResultado_LoteAceite = (int)(NumberUtil.Val( GetNextPar( ), "."));
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public arel_contagemos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public arel_contagemos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           short aP1_ContagemResultado_StatusCnt ,
                           String aP2_GridStateXML ,
                           int aP3_ContagemResultado_LoteAceite )
      {
         this.AV52Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV45ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         this.AV77GridStateXML = aP2_GridStateXML;
         this.AV8ContagemResultado_LoteAceite = aP3_ContagemResultado_LoteAceite;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 short aP1_ContagemResultado_StatusCnt ,
                                 String aP2_GridStateXML ,
                                 int aP3_ContagemResultado_LoteAceite )
      {
         arel_contagemos objarel_contagemos;
         objarel_contagemos = new arel_contagemos();
         objarel_contagemos.AV52Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objarel_contagemos.AV45ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         objarel_contagemos.AV77GridStateXML = aP2_GridStateXML;
         objarel_contagemos.AV8ContagemResultado_LoteAceite = aP3_ContagemResultado_LoteAceite;
         objarel_contagemos.context.SetSubmitInitialConfig(context);
         objarel_contagemos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objarel_contagemos);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((arel_contagemos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 4;
         P_lines = (int)(60-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 9, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*4));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV10WWPContext) ;
            AV126Paginas = (short)(AV126Paginas+1);
            if ( (0==AV8ContagemResultado_LoteAceite) )
            {
               AV84Lote = "";
               /* Execute user subroutine: 'PRINTFILTERS' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
               /* Execute user subroutine: 'PRINTDATA' */
               S121 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
            else
            {
               /* Using cursor P003G2 */
               pr_default.execute(0, new Object[] {AV8ContagemResultado_LoteAceite});
               while ( (pr_default.getStatus(0) != 101) )
               {
                  A596Lote_Codigo = P003G2_A596Lote_Codigo[0];
                  A562Lote_Numero = P003G2_A562Lote_Numero[0];
                  A844Lote_DataContrato = P003G2_A844Lote_DataContrato[0];
                  n844Lote_DataContrato = P003G2_n844Lote_DataContrato[0];
                  AV127Length = (short)(StringUtil.Len( A562Lote_Numero)-6);
                  AV188Lote_Numero = A562Lote_Numero;
                  AV84Lote = StringUtil.Substring( A562Lote_Numero, StringUtil.Len( A562Lote_Numero)-3, 4) + "/" + StringUtil.PadL( StringUtil.Substring( StringUtil.Trim( A562Lote_Numero), 1, AV127Length), 3, "0");
                  AV55Contrato_DataVigenciaInicio = A844Lote_DataContrato;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(0);
               /* Execute user subroutine: 'DADOSCONTRATO' */
               S135 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
               /* Execute user subroutine: 'PRINTDATALOTE' */
               S151 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H3G0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PRINTFILTERS' Routine */
         AV75GridState.gxTpr_Dynamicfilters.FromXml(AV77GridStateXML, "");
         if ( AV75GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV76GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV75GridState.gxTpr_Dynamicfilters.Item(1));
            AV65DynamicFiltersSelector1 = AV76GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 )
            {
               AV19ContagemResultado_DataCnt1 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Value, 2);
               AV16ContagemResultado_DataCnt_To1 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV47ContagemResultado_StatusDmn1 = AV76GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV225DynamicFiltersOperator1 = AV76GridStateDynamicFilter.gxTpr_Operator;
               AV29ContagemResultado_OsFsOsFm1 = AV76GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV296ContagemResultado_DataDmn1 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Value, 2);
               AV291ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
            {
               AV307ContagemResultado_DataPrevista1 = context.localUtil.CToT( AV76GridStateDynamicFilter.gxTpr_Value, 2);
               AV302ContagemResultado_DataPrevista_To1 = context.localUtil.CToT( AV76GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV133ContagemResultado_Servico1 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
            {
               AV172ContagemResultado_ContadorFM1 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV38ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV190ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
            {
               AV141ContagemResultado_ServicoGrupo1 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_BASELINE") == 0 )
            {
               AV137ContagemResultado_Baseline1 = AV76GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
            {
               AV25ContagemResultado_NaoCnfDmnCod1 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_PFBFM") == 0 )
            {
               AV177ContagemResultado_PFBFM1 = NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, ".");
            }
            else if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_PFBFS") == 0 )
            {
               AV182ContagemResultado_PFBFS1 = NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, ".");
            }
            else if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
            {
               AV196ContagemResultado_Agrupador1 = AV76GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
            {
               AV212ContagemResultado_Descricao1 = AV76GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_OWNER") == 0 )
            {
               AV218ContagemResultado_Owner1 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
            {
               AV233ContagemResultado_TemPndHmlg1 = AV76GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV65DynamicFiltersSelector1, "CONTAGEMRESULTADO_CNTCOD") == 0 )
            {
               AV317ContagemResultado_CntCod1 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV75GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV63DynamicFiltersEnabled2 = true;
               AV76GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV75GridState.gxTpr_Dynamicfilters.Item(2));
               AV66DynamicFiltersSelector2 = AV76GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 )
               {
                  AV20ContagemResultado_DataCnt2 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Value, 2);
                  AV17ContagemResultado_DataCnt_To2 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV48ContagemResultado_StatusDmn2 = AV76GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
               {
                  AV226DynamicFiltersOperator2 = AV76GridStateDynamicFilter.gxTpr_Operator;
                  AV30ContagemResultado_OsFsOsFm2 = AV76GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV297ContagemResultado_DataDmn2 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Value, 2);
                  AV292ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
               {
                  AV308ContagemResultado_DataPrevista2 = context.localUtil.CToT( AV76GridStateDynamicFilter.gxTpr_Value, 2);
                  AV303ContagemResultado_DataPrevista_To2 = context.localUtil.CToT( AV76GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICO") == 0 )
               {
                  AV135ContagemResultado_Servico2 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
               {
                  AV173ContagemResultado_ContadorFM2 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV39ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
               {
                  AV191ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
               {
                  AV142ContagemResultado_ServicoGrupo2 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_BASELINE") == 0 )
               {
                  AV138ContagemResultado_Baseline2 = AV76GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
               {
                  AV26ContagemResultado_NaoCnfDmnCod2 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_PFBFM") == 0 )
               {
                  AV178ContagemResultado_PFBFM2 = NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, ".");
               }
               else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_PFBFS") == 0 )
               {
                  AV183ContagemResultado_PFBFS2 = NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, ".");
               }
               else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
               {
                  AV197ContagemResultado_Agrupador2 = AV76GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
               {
                  AV213ContagemResultado_Descricao2 = AV76GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_OWNER") == 0 )
               {
                  AV219ContagemResultado_Owner2 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
               {
                  AV234ContagemResultado_TemPndHmlg2 = AV76GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV66DynamicFiltersSelector2, "CONTAGEMRESULTADO_CNTCOD") == 0 )
               {
                  AV318ContagemResultado_CntCod2 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV75GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV64DynamicFiltersEnabled3 = true;
                  AV76GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV75GridState.gxTpr_Dynamicfilters.Item(3));
                  AV67DynamicFiltersSelector3 = AV76GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 )
                  {
                     AV21ContagemResultado_DataCnt3 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Value, 2);
                     AV18ContagemResultado_DataCnt_To3 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV49ContagemResultado_StatusDmn3 = AV76GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                  {
                     AV227DynamicFiltersOperator3 = AV76GridStateDynamicFilter.gxTpr_Operator;
                     AV31ContagemResultado_OsFsOsFm3 = AV76GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV298ContagemResultado_DataDmn3 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Value, 2);
                     AV293ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                  {
                     AV309ContagemResultado_DataPrevista3 = context.localUtil.CToT( AV76GridStateDynamicFilter.gxTpr_Value, 2);
                     AV304ContagemResultado_DataPrevista_To3 = context.localUtil.CToT( AV76GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICO") == 0 )
                  {
                     AV136ContagemResultado_Servico3 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                  {
                     AV174ContagemResultado_ContadorFM3 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV40ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                  {
                     AV192ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
                  {
                     AV143ContagemResultado_ServicoGrupo3 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_BASELINE") == 0 )
                  {
                     AV139ContagemResultado_Baseline3 = AV76GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                  {
                     AV27ContagemResultado_NaoCnfDmnCod3 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_PFBFM") == 0 )
                  {
                     AV179ContagemResultado_PFBFM3 = NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, ".");
                  }
                  else if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_PFBFS") == 0 )
                  {
                     AV184ContagemResultado_PFBFS3 = NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, ".");
                  }
                  else if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                  {
                     AV198ContagemResultado_Agrupador3 = AV76GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                  {
                     AV214ContagemResultado_Descricao3 = AV76GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_OWNER") == 0 )
                  {
                     AV220ContagemResultado_Owner3 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
                  {
                     AV235ContagemResultado_TemPndHmlg3 = AV76GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "CONTAGEMRESULTADO_CNTCOD") == 0 )
                  {
                     AV319ContagemResultado_CntCod3 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  if ( AV75GridState.gxTpr_Dynamicfilters.Count >= 4 )
                  {
                     AV146DynamicFiltersEnabled4 = true;
                     AV76GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV75GridState.gxTpr_Dynamicfilters.Item(4));
                     AV148DynamicFiltersSelector4 = AV76GridStateDynamicFilter.gxTpr_Selected;
                     if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATACNT") == 0 )
                     {
                        AV155ContagemResultado_DataCnt4 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Value, 2);
                        AV154ContagemResultado_DataCnt_To4 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Valueto, 2);
                     }
                     else if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                     {
                        AV164ContagemResultado_StatusDmn4 = AV76GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                     {
                        AV228DynamicFiltersOperator4 = AV76GridStateDynamicFilter.gxTpr_Operator;
                        AV159ContagemResultado_OsFsOsFm4 = AV76GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATADMN") == 0 )
                     {
                        AV299ContagemResultado_DataDmn4 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Value, 2);
                        AV294ContagemResultado_DataDmn_To4 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Valueto, 2);
                     }
                     else if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                     {
                        AV310ContagemResultado_DataPrevista4 = context.localUtil.CToT( AV76GridStateDynamicFilter.gxTpr_Value, 2);
                        AV305ContagemResultado_DataPrevista_To4 = context.localUtil.CToT( AV76GridStateDynamicFilter.gxTpr_Valueto, 2);
                     }
                     else if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_SERVICO") == 0 )
                     {
                        AV160ContagemResultado_Servico4 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                     {
                        AV175ContagemResultado_ContadorFM4 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                     {
                        AV162ContagemResultado_SistemaCod4 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                     {
                        AV193ContagemResultado_ContratadaCod4 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
                     {
                        AV161ContagemResultado_ServicoGrupo4 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_BASELINE") == 0 )
                     {
                        AV150ContagemResultado_Baseline4 = AV76GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                     {
                        AV158ContagemResultado_NaoCnfDmnCod4 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_PFBFM") == 0 )
                     {
                        AV180ContagemResultado_PFBFM4 = NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, ".");
                     }
                     else if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_PFBFS") == 0 )
                     {
                        AV185ContagemResultado_PFBFS4 = NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, ".");
                     }
                     else if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                     {
                        AV199ContagemResultado_Agrupador4 = AV76GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                     {
                        AV215ContagemResultado_Descricao4 = AV76GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_OWNER") == 0 )
                     {
                        AV221ContagemResultado_Owner4 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
                     {
                        AV236ContagemResultado_TemPndHmlg4 = AV76GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV148DynamicFiltersSelector4, "CONTAGEMRESULTADO_CNTCOD") == 0 )
                     {
                        AV320ContagemResultado_CntCod4 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     if ( AV75GridState.gxTpr_Dynamicfilters.Count >= 5 )
                     {
                        AV147DynamicFiltersEnabled5 = true;
                        AV76GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV75GridState.gxTpr_Dynamicfilters.Item(5));
                        AV149DynamicFiltersSelector5 = AV76GridStateDynamicFilter.gxTpr_Selected;
                        if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATACNT") == 0 )
                        {
                           AV157ContagemResultado_DataCnt5 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Value, 2);
                           AV156ContagemResultado_DataCnt_To5 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Valueto, 2);
                        }
                        else if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                        {
                           AV171ContagemResultado_StatusDmn5 = AV76GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                        {
                           AV229DynamicFiltersOperator5 = AV76GridStateDynamicFilter.gxTpr_Operator;
                           AV166ContagemResultado_OsFsOsFm5 = AV76GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATADMN") == 0 )
                        {
                           AV300ContagemResultado_DataDmn5 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Value, 2);
                           AV295ContagemResultado_DataDmn_To5 = context.localUtil.CToD( AV76GridStateDynamicFilter.gxTpr_Valueto, 2);
                        }
                        else if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                        {
                           AV311ContagemResultado_DataPrevista5 = context.localUtil.CToT( AV76GridStateDynamicFilter.gxTpr_Value, 2);
                           AV306ContagemResultado_DataPrevista_To5 = context.localUtil.CToT( AV76GridStateDynamicFilter.gxTpr_Valueto, 2);
                        }
                        else if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_SERVICO") == 0 )
                        {
                           AV167ContagemResultado_Servico5 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                        {
                           AV176ContagemResultado_ContadorFM5 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                        {
                           AV169ContagemResultado_SistemaCod5 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                        {
                           AV194ContagemResultado_ContratadaCod5 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
                        {
                           AV168ContagemResultado_ServicoGrupo5 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_BASELINE") == 0 )
                        {
                           AV151ContagemResultado_Baseline5 = AV76GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                        {
                           AV165ContagemResultado_NaoCnfDmnCod5 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_PFBFM") == 0 )
                        {
                           AV181ContagemResultado_PFBFM5 = NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, ".");
                        }
                        else if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_PFBFS") == 0 )
                        {
                           AV186ContagemResultado_PFBFS5 = NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, ".");
                        }
                        else if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                        {
                           AV200ContagemResultado_Agrupador5 = AV76GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                        {
                           AV216ContagemResultado_Descricao5 = AV76GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_OWNER") == 0 )
                        {
                           AV222ContagemResultado_Owner5 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
                        {
                           AV237ContagemResultado_TemPndHmlg5 = AV76GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV149DynamicFiltersSelector5, "CONTAGEMRESULTADO_CNTCOD") == 0 )
                        {
                           AV321ContagemResultado_CntCod5 = (int)(NumberUtil.Val( AV76GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                     }
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'PRINTDATA' Routine */
         AV327ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod = AV52Contratada_AreaTrabalhoCod;
         AV328ExtraWWContagemResultadoExtraSelectionDS_2_Contagemresultado_statuscnt = AV45ContagemResultado_StatusCnt;
         AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = AV65DynamicFiltersSelector1;
         AV330ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 = AV225DynamicFiltersOperator1;
         AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 = AV19ContagemResultado_DataCnt1;
         AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 = AV16ContagemResultado_DataCnt_To1;
         AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 = AV47ContagemResultado_StatusDmn1;
         AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = AV29ContagemResultado_OsFsOsFm1;
         AV335ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 = AV296ContagemResultado_DataDmn1;
         AV336ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 = AV291ContagemResultado_DataDmn_To1;
         AV337ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 = DateTimeUtil.ResetTime(AV307ContagemResultado_DataPrevista1);
         AV338ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 = DateTimeUtil.ResetTime(AV302ContagemResultado_DataPrevista_To1);
         AV339ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 = AV133ContagemResultado_Servico1;
         AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 = AV172ContagemResultado_ContadorFM1;
         AV341ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 = AV38ContagemResultado_SistemaCod1;
         AV342ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 = AV190ContagemResultado_ContratadaCod1;
         AV343ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 = AV141ContagemResultado_ServicoGrupo1;
         AV344ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 = AV137ContagemResultado_Baseline1;
         AV345ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 = AV25ContagemResultado_NaoCnfDmnCod1;
         AV346ExtraWWContagemResultadoExtraSelectionDS_20_Contagemresultado_pfbfm1 = AV177ContagemResultado_PFBFM1;
         AV347ExtraWWContagemResultadoExtraSelectionDS_21_Contagemresultado_pfbfs1 = AV182ContagemResultado_PFBFS1;
         AV348ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 = AV196ContagemResultado_Agrupador1;
         AV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 = AV212ContagemResultado_Descricao1;
         AV350ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 = AV218ContagemResultado_Owner1;
         AV351ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 = AV233ContagemResultado_TemPndHmlg1;
         AV352ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 = AV317ContagemResultado_CntCod1;
         AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = AV63DynamicFiltersEnabled2;
         AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = AV66DynamicFiltersSelector2;
         AV355ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 = AV226DynamicFiltersOperator2;
         AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 = AV20ContagemResultado_DataCnt2;
         AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 = AV17ContagemResultado_DataCnt_To2;
         AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 = AV48ContagemResultado_StatusDmn2;
         AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = AV30ContagemResultado_OsFsOsFm2;
         AV360ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 = AV297ContagemResultado_DataDmn2;
         AV361ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 = AV292ContagemResultado_DataDmn_To2;
         AV362ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 = DateTimeUtil.ResetTime(AV308ContagemResultado_DataPrevista2);
         AV363ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 = DateTimeUtil.ResetTime(AV303ContagemResultado_DataPrevista_To2);
         AV364ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 = AV135ContagemResultado_Servico2;
         AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 = AV173ContagemResultado_ContadorFM2;
         AV366ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 = AV39ContagemResultado_SistemaCod2;
         AV367ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 = AV191ContagemResultado_ContratadaCod2;
         AV368ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 = AV142ContagemResultado_ServicoGrupo2;
         AV369ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 = AV138ContagemResultado_Baseline2;
         AV370ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 = AV26ContagemResultado_NaoCnfDmnCod2;
         AV371ExtraWWContagemResultadoExtraSelectionDS_45_Contagemresultado_pfbfm2 = AV178ContagemResultado_PFBFM2;
         AV372ExtraWWContagemResultadoExtraSelectionDS_46_Contagemresultado_pfbfs2 = AV183ContagemResultado_PFBFS2;
         AV373ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 = AV197ContagemResultado_Agrupador2;
         AV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 = AV213ContagemResultado_Descricao2;
         AV375ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 = AV219ContagemResultado_Owner2;
         AV376ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 = AV234ContagemResultado_TemPndHmlg2;
         AV377ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 = AV318ContagemResultado_CntCod2;
         AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = AV64DynamicFiltersEnabled3;
         AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = AV67DynamicFiltersSelector3;
         AV380ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 = AV227DynamicFiltersOperator3;
         AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 = AV21ContagemResultado_DataCnt3;
         AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 = AV18ContagemResultado_DataCnt_To3;
         AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 = AV49ContagemResultado_StatusDmn3;
         AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = AV31ContagemResultado_OsFsOsFm3;
         AV385ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 = AV298ContagemResultado_DataDmn3;
         AV386ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 = AV293ContagemResultado_DataDmn_To3;
         AV387ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 = DateTimeUtil.ResetTime(AV309ContagemResultado_DataPrevista3);
         AV388ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 = DateTimeUtil.ResetTime(AV304ContagemResultado_DataPrevista_To3);
         AV389ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 = AV136ContagemResultado_Servico3;
         AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 = AV174ContagemResultado_ContadorFM3;
         AV391ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 = AV40ContagemResultado_SistemaCod3;
         AV392ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 = AV192ContagemResultado_ContratadaCod3;
         AV393ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 = AV143ContagemResultado_ServicoGrupo3;
         AV394ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 = AV139ContagemResultado_Baseline3;
         AV395ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 = AV27ContagemResultado_NaoCnfDmnCod3;
         AV396ExtraWWContagemResultadoExtraSelectionDS_70_Contagemresultado_pfbfm3 = AV179ContagemResultado_PFBFM3;
         AV397ExtraWWContagemResultadoExtraSelectionDS_71_Contagemresultado_pfbfs3 = AV184ContagemResultado_PFBFS3;
         AV398ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 = AV198ContagemResultado_Agrupador3;
         AV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 = AV214ContagemResultado_Descricao3;
         AV400ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 = AV220ContagemResultado_Owner3;
         AV401ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 = AV235ContagemResultado_TemPndHmlg3;
         AV402ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 = AV319ContagemResultado_CntCod3;
         AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = AV146DynamicFiltersEnabled4;
         AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = AV148DynamicFiltersSelector4;
         AV405ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 = AV228DynamicFiltersOperator4;
         AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 = AV155ContagemResultado_DataCnt4;
         AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 = AV154ContagemResultado_DataCnt_To4;
         AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 = AV164ContagemResultado_StatusDmn4;
         AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = AV159ContagemResultado_OsFsOsFm4;
         AV410ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 = AV299ContagemResultado_DataDmn4;
         AV411ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 = AV294ContagemResultado_DataDmn_To4;
         AV412ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 = DateTimeUtil.ResetTime(AV310ContagemResultado_DataPrevista4);
         AV413ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 = DateTimeUtil.ResetTime(AV305ContagemResultado_DataPrevista_To4);
         AV414ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 = AV160ContagemResultado_Servico4;
         AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 = AV175ContagemResultado_ContadorFM4;
         AV416ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 = AV162ContagemResultado_SistemaCod4;
         AV417ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 = AV193ContagemResultado_ContratadaCod4;
         AV418ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 = AV161ContagemResultado_ServicoGrupo4;
         AV419ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 = AV150ContagemResultado_Baseline4;
         AV420ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 = AV158ContagemResultado_NaoCnfDmnCod4;
         AV421ExtraWWContagemResultadoExtraSelectionDS_95_Contagemresultado_pfbfm4 = AV180ContagemResultado_PFBFM4;
         AV422ExtraWWContagemResultadoExtraSelectionDS_96_Contagemresultado_pfbfs4 = AV185ContagemResultado_PFBFS4;
         AV423ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 = AV199ContagemResultado_Agrupador4;
         AV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 = AV215ContagemResultado_Descricao4;
         AV425ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 = AV221ContagemResultado_Owner4;
         AV426ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 = AV236ContagemResultado_TemPndHmlg4;
         AV427ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 = AV320ContagemResultado_CntCod4;
         AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = AV147DynamicFiltersEnabled5;
         AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = AV149DynamicFiltersSelector5;
         AV430ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 = AV229DynamicFiltersOperator5;
         AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 = AV157ContagemResultado_DataCnt5;
         AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 = AV156ContagemResultado_DataCnt_To5;
         AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 = AV171ContagemResultado_StatusDmn5;
         AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = AV166ContagemResultado_OsFsOsFm5;
         AV435ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 = AV300ContagemResultado_DataDmn5;
         AV436ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 = AV295ContagemResultado_DataDmn_To5;
         AV437ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 = DateTimeUtil.ResetTime(AV311ContagemResultado_DataPrevista5);
         AV438ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 = DateTimeUtil.ResetTime(AV306ContagemResultado_DataPrevista_To5);
         AV439ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 = AV167ContagemResultado_Servico5;
         AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 = AV176ContagemResultado_ContadorFM5;
         AV441ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 = AV169ContagemResultado_SistemaCod5;
         AV442ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 = AV194ContagemResultado_ContratadaCod5;
         AV443ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 = AV168ContagemResultado_ServicoGrupo5;
         AV444ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 = AV151ContagemResultado_Baseline5;
         AV445ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 = AV165ContagemResultado_NaoCnfDmnCod5;
         AV446ExtraWWContagemResultadoExtraSelectionDS_120_Contagemresultado_pfbfm5 = AV181ContagemResultado_PFBFM5;
         AV447ExtraWWContagemResultadoExtraSelectionDS_121_Contagemresultado_pfbfs5 = AV186ContagemResultado_PFBFS5;
         AV448ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 = AV200ContagemResultado_Agrupador5;
         AV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 = AV216ContagemResultado_Descricao5;
         AV450ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 = AV222ContagemResultado_Owner5;
         AV451ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 = AV237ContagemResultado_TemPndHmlg5;
         AV452ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 = AV321ContagemResultado_CntCod5;
         AV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador = AV238TFContagemResultado_Agrupador;
         AV454ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel = AV239TFContagemResultado_Agrupador_Sel;
         AV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm = AV247TFContagemResultado_DemandaFM;
         AV456ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel = AV248TFContagemResultado_DemandaFM_Sel;
         AV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda = AV245TFContagemResultado_Demanda;
         AV458ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel = AV246TFContagemResultado_Demanda_Sel;
         AV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao = AV249TFContagemResultado_Descricao;
         AV460ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel = AV250TFContagemResultado_Descricao_Sel;
         AV461ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista = AV314TFContagemResultado_DataPrevista;
         AV462ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to = AV315TFContagemResultado_DataPrevista_To;
         AV463ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn = AV312TFContagemResultado_DataDmn;
         AV464ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to = AV313TFContagemResultado_DataDmn_To;
         AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt = AV243TFContagemResultado_DataUltCnt;
         AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to = AV244TFContagemResultado_DataUltCnt_To;
         AV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla = AV241TFContagemResultado_ContratadaSigla;
         AV468ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel = AV242TFContagemResultado_ContratadaSigla_Sel;
         AV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord = AV255TFContagemResultado_SistemaCoord;
         AV470ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel = AV256TFContagemResultado_SistemaCoord_Sel;
         AV471ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels = AV259TFContagemResultado_StatusDmn_Sels;
         AV472ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel = AV240TFContagemResultado_Baseline_Sel;
         AV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla = AV253TFContagemResultado_ServicoSigla;
         AV474ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel = AV254TFContagemResultado_ServicoSigla_Sel;
         AV475ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal = AV251TFContagemResultado_PFFinal;
         AV476ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to = AV252TFContagemResultado_PFFinal_To;
         AV477ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf = AV261TFContagemResultado_ValorPF;
         AV478ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to = AV262TFContagemResultado_ValorPF_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A484ContagemResultado_StatusDmn ,
                                              AV471ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels ,
                                              AV10WWPContext.gxTpr_Contratada_codigo ,
                                              AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 ,
                                              AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 ,
                                              AV330ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 ,
                                              AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 ,
                                              AV335ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 ,
                                              AV336ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 ,
                                              AV337ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 ,
                                              AV338ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 ,
                                              AV339ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 ,
                                              AV341ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 ,
                                              AV342ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 ,
                                              AV327ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod ,
                                              AV343ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 ,
                                              AV344ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 ,
                                              AV345ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 ,
                                              AV348ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 ,
                                              AV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 ,
                                              AV350ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 ,
                                              AV352ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 ,
                                              AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 ,
                                              AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 ,
                                              AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 ,
                                              AV355ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 ,
                                              AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 ,
                                              AV360ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 ,
                                              AV361ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 ,
                                              AV362ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 ,
                                              AV363ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 ,
                                              AV364ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 ,
                                              AV366ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 ,
                                              AV367ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 ,
                                              AV368ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 ,
                                              AV369ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 ,
                                              AV370ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 ,
                                              AV373ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 ,
                                              AV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 ,
                                              AV375ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 ,
                                              AV377ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 ,
                                              AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 ,
                                              AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 ,
                                              AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 ,
                                              AV380ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 ,
                                              AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 ,
                                              AV385ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 ,
                                              AV386ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 ,
                                              AV387ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 ,
                                              AV388ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 ,
                                              AV389ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 ,
                                              AV391ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 ,
                                              AV392ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 ,
                                              AV393ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 ,
                                              AV394ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 ,
                                              AV395ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 ,
                                              AV398ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 ,
                                              AV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 ,
                                              AV400ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 ,
                                              AV402ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 ,
                                              AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 ,
                                              AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 ,
                                              AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 ,
                                              AV405ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 ,
                                              AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 ,
                                              AV410ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 ,
                                              AV411ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 ,
                                              AV412ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 ,
                                              AV413ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 ,
                                              AV414ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 ,
                                              AV416ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 ,
                                              AV417ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 ,
                                              AV418ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 ,
                                              AV419ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 ,
                                              AV420ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 ,
                                              AV423ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 ,
                                              AV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 ,
                                              AV425ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 ,
                                              AV427ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 ,
                                              AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 ,
                                              AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 ,
                                              AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 ,
                                              AV430ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 ,
                                              AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 ,
                                              AV435ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 ,
                                              AV436ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 ,
                                              AV437ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 ,
                                              AV438ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 ,
                                              AV439ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 ,
                                              AV441ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 ,
                                              AV442ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 ,
                                              AV443ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 ,
                                              AV444ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 ,
                                              AV445ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 ,
                                              AV448ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 ,
                                              AV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 ,
                                              AV450ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 ,
                                              AV452ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 ,
                                              AV454ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel ,
                                              AV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador ,
                                              AV456ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel ,
                                              AV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm ,
                                              AV458ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel ,
                                              AV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda ,
                                              AV460ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel ,
                                              AV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao ,
                                              AV461ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista ,
                                              AV462ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to ,
                                              AV463ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn ,
                                              AV464ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to ,
                                              AV468ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel ,
                                              AV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla ,
                                              AV470ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel ,
                                              AV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord ,
                                              AV471ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV472ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel ,
                                              AV474ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel ,
                                              AV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla ,
                                              AV477ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf ,
                                              AV478ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A1351ContagemResultado_DataPrevista ,
                                              A601ContagemResultado_Servico ,
                                              A489ContagemResultado_SistemaCod ,
                                              A764ContagemResultado_ServicoGrupo ,
                                              A598ContagemResultado_Baseline ,
                                              A468ContagemResultado_NaoCnfDmnCod ,
                                              A1046ContagemResultado_Agrupador ,
                                              A494ContagemResultado_Descricao ,
                                              A508ContagemResultado_Owner ,
                                              A1603ContagemResultado_CntCod ,
                                              A803ContagemResultado_ContratadaSigla ,
                                              A515ContagemResultado_SistemaCoord ,
                                              A801ContagemResultado_ServicoSigla ,
                                              A512ContagemResultado_ValorPF ,
                                              A531ContagemResultado_StatusUltCnt ,
                                              A1854ContagemResultado_VlrCnc ,
                                              AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 ,
                                              AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 ,
                                              A584ContagemResultado_ContadorFM ,
                                              A684ContagemResultado_PFBFSUltima ,
                                              A682ContagemResultado_PFBFMUltima ,
                                              A685ContagemResultado_PFLFSUltima ,
                                              A683ContagemResultado_PFLFMUltima ,
                                              AV351ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 ,
                                              A1802ContagemResultado_TemPndHmlg ,
                                              AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 ,
                                              AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 ,
                                              AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 ,
                                              AV376ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 ,
                                              AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 ,
                                              AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 ,
                                              AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 ,
                                              AV401ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 ,
                                              AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 ,
                                              AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 ,
                                              AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 ,
                                              AV426ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 ,
                                              AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 ,
                                              AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 ,
                                              AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 ,
                                              AV451ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 ,
                                              AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt ,
                                              AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to ,
                                              AV475ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ,
                                              A574ContagemResultado_PFFinal ,
                                              AV476ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV10WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
         lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
         lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
         lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
         lV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1), "%", "");
         lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
         lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
         lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
         lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
         lV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2), "%", "");
         lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
         lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
         lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
         lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
         lV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3), "%", "");
         lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
         lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
         lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
         lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
         lV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 = StringUtil.Concat( StringUtil.RTrim( AV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4), "%", "");
         lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
         lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
         lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
         lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
         lV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 = StringUtil.Concat( StringUtil.RTrim( AV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5), "%", "");
         lV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador = StringUtil.PadR( StringUtil.RTrim( AV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador), 15, "%");
         lV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm), "%", "");
         lV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda = StringUtil.Concat( StringUtil.RTrim( AV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda), "%", "");
         lV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao = StringUtil.Concat( StringUtil.RTrim( AV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao), "%", "");
         lV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla = StringUtil.PadR( StringUtil.RTrim( AV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla), 15, "%");
         lV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord = StringUtil.Concat( StringUtil.RTrim( AV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord), "%", "");
         lV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P003G4 */
         pr_default.execute(1, new Object[] {AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1, AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1, AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1, AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1, AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1, AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1, AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1, AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2, AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2, AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2, AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2, AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2, AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2, AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2, AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3, AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3, AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3, AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3, AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3, AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3, AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3, AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4, AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4, AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4, AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4, AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4, AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4, AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4, AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5, AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5, AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5, AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5, AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5, AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5, AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5, AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt, AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt, AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to, AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to, AV10WWPContext.gxTpr_Areatrabalho_codigo, AV10WWPContext.gxTpr_Contratada_codigo, AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, AV335ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1, AV336ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1, AV337ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1, AV338ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1, AV339ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1, AV341ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1, AV342ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1, AV343ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1, AV345ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1, AV348ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1, lV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1, AV350ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1, AV352ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1, AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, AV360ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2, AV361ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2, AV362ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2, AV363ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2,
         AV364ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2, AV366ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2, AV367ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2, AV368ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2, AV370ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2, AV373ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2, lV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2, AV375ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2, AV377ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2, AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, AV385ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3, AV386ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3, AV387ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3, AV388ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3, AV389ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3, AV391ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3, AV392ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3, AV393ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3, AV395ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3, AV398ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3, lV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3, AV400ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3, AV402ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3, AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, AV410ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4, AV411ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4, AV412ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4, AV413ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4, AV414ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4, AV416ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4, AV417ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4, AV418ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4, AV420ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4, AV423ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4, lV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4, AV425ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4, AV427ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4, AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, AV435ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5, AV436ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5, AV437ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5, AV438ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5, AV439ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5, AV441ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5, AV442ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5, AV443ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5, AV445ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5, AV448ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5, lV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5, AV450ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5, AV452ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5, lV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador, AV454ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel, lV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm, AV456ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel, lV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda, AV458ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel, lV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao, AV460ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel, AV461ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista, AV462ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to, AV463ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn, AV464ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to, lV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla, AV468ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel, lV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord, AV470ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel, lV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla, AV474ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel, AV477ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf, AV478ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRK3G5 = false;
            A1553ContagemResultado_CntSrvCod = P003G4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P003G4_n1553ContagemResultado_CntSrvCod[0];
            A52Contratada_AreaTrabalhoCod = P003G4_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P003G4_n52Contratada_AreaTrabalhoCod[0];
            A484ContagemResultado_StatusDmn = P003G4_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P003G4_n484ContagemResultado_StatusDmn[0];
            A1452ContagemResultado_SS = P003G4_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = P003G4_n1452ContagemResultado_SS[0];
            A457ContagemResultado_Demanda = P003G4_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P003G4_n457ContagemResultado_Demanda[0];
            A512ContagemResultado_ValorPF = P003G4_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P003G4_n512ContagemResultado_ValorPF[0];
            A801ContagemResultado_ServicoSigla = P003G4_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P003G4_n801ContagemResultado_ServicoSigla[0];
            A515ContagemResultado_SistemaCoord = P003G4_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P003G4_n515ContagemResultado_SistemaCoord[0];
            A803ContagemResultado_ContratadaSigla = P003G4_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P003G4_n803ContagemResultado_ContratadaSigla[0];
            A1603ContagemResultado_CntCod = P003G4_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P003G4_n1603ContagemResultado_CntCod[0];
            A494ContagemResultado_Descricao = P003G4_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P003G4_n494ContagemResultado_Descricao[0];
            A1046ContagemResultado_Agrupador = P003G4_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P003G4_n1046ContagemResultado_Agrupador[0];
            A468ContagemResultado_NaoCnfDmnCod = P003G4_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P003G4_n468ContagemResultado_NaoCnfDmnCod[0];
            A598ContagemResultado_Baseline = P003G4_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P003G4_n598ContagemResultado_Baseline[0];
            A764ContagemResultado_ServicoGrupo = P003G4_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P003G4_n764ContagemResultado_ServicoGrupo[0];
            A489ContagemResultado_SistemaCod = P003G4_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P003G4_n489ContagemResultado_SistemaCod[0];
            A508ContagemResultado_Owner = P003G4_A508ContagemResultado_Owner[0];
            A601ContagemResultado_Servico = P003G4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P003G4_n601ContagemResultado_Servico[0];
            A1351ContagemResultado_DataPrevista = P003G4_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P003G4_n1351ContagemResultado_DataPrevista[0];
            A471ContagemResultado_DataDmn = P003G4_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P003G4_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P003G4_n493ContagemResultado_DemandaFM[0];
            A1854ContagemResultado_VlrCnc = P003G4_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P003G4_n1854ContagemResultado_VlrCnc[0];
            A490ContagemResultado_ContratadaCod = P003G4_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P003G4_n490ContagemResultado_ContratadaCod[0];
            A509ContagemrResultado_SistemaSigla = P003G4_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P003G4_n509ContagemrResultado_SistemaSigla[0];
            A683ContagemResultado_PFLFMUltima = P003G4_A683ContagemResultado_PFLFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P003G4_A685ContagemResultado_PFLFSUltima[0];
            A682ContagemResultado_PFBFMUltima = P003G4_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = P003G4_A684ContagemResultado_PFBFSUltima[0];
            A584ContagemResultado_ContadorFM = P003G4_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P003G4_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P003G4_n566ContagemResultado_DataUltCnt[0];
            A531ContagemResultado_StatusUltCnt = P003G4_A531ContagemResultado_StatusUltCnt[0];
            A456ContagemResultado_Codigo = P003G4_A456ContagemResultado_Codigo[0];
            A473ContagemResultado_DataCnt = P003G4_A473ContagemResultado_DataCnt[0];
            A511ContagemResultado_HoraCnt = P003G4_A511ContagemResultado_HoraCnt[0];
            A1553ContagemResultado_CntSrvCod = P003G4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P003G4_n1553ContagemResultado_CntSrvCod[0];
            A484ContagemResultado_StatusDmn = P003G4_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P003G4_n484ContagemResultado_StatusDmn[0];
            A1452ContagemResultado_SS = P003G4_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = P003G4_n1452ContagemResultado_SS[0];
            A457ContagemResultado_Demanda = P003G4_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P003G4_n457ContagemResultado_Demanda[0];
            A512ContagemResultado_ValorPF = P003G4_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P003G4_n512ContagemResultado_ValorPF[0];
            A494ContagemResultado_Descricao = P003G4_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P003G4_n494ContagemResultado_Descricao[0];
            A1046ContagemResultado_Agrupador = P003G4_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P003G4_n1046ContagemResultado_Agrupador[0];
            A468ContagemResultado_NaoCnfDmnCod = P003G4_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P003G4_n468ContagemResultado_NaoCnfDmnCod[0];
            A598ContagemResultado_Baseline = P003G4_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P003G4_n598ContagemResultado_Baseline[0];
            A489ContagemResultado_SistemaCod = P003G4_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P003G4_n489ContagemResultado_SistemaCod[0];
            A508ContagemResultado_Owner = P003G4_A508ContagemResultado_Owner[0];
            A1351ContagemResultado_DataPrevista = P003G4_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P003G4_n1351ContagemResultado_DataPrevista[0];
            A471ContagemResultado_DataDmn = P003G4_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P003G4_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P003G4_n493ContagemResultado_DemandaFM[0];
            A1854ContagemResultado_VlrCnc = P003G4_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P003G4_n1854ContagemResultado_VlrCnc[0];
            A490ContagemResultado_ContratadaCod = P003G4_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P003G4_n490ContagemResultado_ContratadaCod[0];
            A1603ContagemResultado_CntCod = P003G4_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P003G4_n1603ContagemResultado_CntCod[0];
            A601ContagemResultado_Servico = P003G4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P003G4_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P003G4_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P003G4_n801ContagemResultado_ServicoSigla[0];
            A764ContagemResultado_ServicoGrupo = P003G4_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P003G4_n764ContagemResultado_ServicoGrupo[0];
            A515ContagemResultado_SistemaCoord = P003G4_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P003G4_n515ContagemResultado_SistemaCoord[0];
            A509ContagemrResultado_SistemaSigla = P003G4_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P003G4_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = P003G4_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P003G4_n52Contratada_AreaTrabalhoCod[0];
            A803ContagemResultado_ContratadaSigla = P003G4_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P003G4_n803ContagemResultado_ContratadaSigla[0];
            A683ContagemResultado_PFLFMUltima = P003G4_A683ContagemResultado_PFLFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P003G4_A685ContagemResultado_PFLFSUltima[0];
            A682ContagemResultado_PFBFMUltima = P003G4_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = P003G4_A684ContagemResultado_PFBFSUltima[0];
            A584ContagemResultado_ContadorFM = P003G4_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P003G4_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P003G4_n566ContagemResultado_DataUltCnt[0];
            A531ContagemResultado_StatusUltCnt = P003G4_A531ContagemResultado_StatusUltCnt[0];
            GXt_boolean1 = A1802ContagemResultado_TemPndHmlg;
            new prc_tempndparahomologar(context ).execute( ref  A456ContagemResultado_Codigo, out  GXt_boolean1) ;
            A1802ContagemResultado_TemPndHmlg = GXt_boolean1;
            if ( ! ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
            {
               if ( ! ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
               {
                  if ( ! ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                  {
                     if ( ! ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                     {
                        if ( ! ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                        {
                           if ( ! ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                           {
                              if ( ! ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                              {
                                 if ( ! ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                                 {
                                    if ( ! ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV451ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                                    {
                                       if ( ! ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV451ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                                       {
                                          GXt_decimal2 = A574ContagemResultado_PFFinal;
                                          new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal2) ;
                                          A574ContagemResultado_PFFinal = GXt_decimal2;
                                          if ( (Convert.ToDecimal(0)==AV475ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal) || ( ( A574ContagemResultado_PFFinal >= AV475ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ) ) )
                                          {
                                             if ( (Convert.ToDecimal(0)==AV476ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to) || ( ( A574ContagemResultado_PFFinal <= AV476ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ) ) )
                                             {
                                                OV224ContagemResultado_SS = AV224ContagemResultado_SS;
                                                if ( String.IsNullOrEmpty(StringUtil.RTrim( AV56Contrato_Numero)) )
                                                {
                                                   AV230Codigo = A456ContagemResultado_Codigo;
                                                   /* Execute user subroutine: 'DADOSCONTRATO' */
                                                   S135 ();
                                                   if ( returnInSub )
                                                   {
                                                      pr_default.close(1);
                                                      returnInSub = true;
                                                      if (true) return;
                                                   }
                                                }
                                                AV9Contratada_OS = (int)(AV9Contratada_OS+1);
                                                AV102Sistema_Sigla = A509ContagemrResultado_SistemaSigla;
                                                AV101Sistema_Coordenacao = A515ContagemResultado_SistemaCoord;
                                                AV224ContagemResultado_SS = A1452ContagemResultado_SS;
                                                while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P003G4_A484ContagemResultado_StatusDmn[0], A484ContagemResultado_StatusDmn) == 0 ) && ( P003G4_A1452ContagemResultado_SS[0] == A1452ContagemResultado_SS ) )
                                                {
                                                   BRK3G5 = false;
                                                   A457ContagemResultado_Demanda = P003G4_A457ContagemResultado_Demanda[0];
                                                   n457ContagemResultado_Demanda = P003G4_n457ContagemResultado_Demanda[0];
                                                   A456ContagemResultado_Codigo = P003G4_A456ContagemResultado_Codigo[0];
                                                   A473ContagemResultado_DataCnt = P003G4_A473ContagemResultado_DataCnt[0];
                                                   A511ContagemResultado_HoraCnt = P003G4_A511ContagemResultado_HoraCnt[0];
                                                   A457ContagemResultado_Demanda = P003G4_A457ContagemResultado_Demanda[0];
                                                   n457ContagemResultado_Demanda = P003G4_n457ContagemResultado_Demanda[0];
                                                   AV78i = (short)(AV78i+1);
                                                   BRK3G5 = true;
                                                   pr_default.readNext(1);
                                                }
                                                /* Execute user subroutine: 'CONTAGENSDOSISTEMA' */
                                                S145 ();
                                                if ( returnInSub )
                                                {
                                                   pr_default.close(1);
                                                   returnInSub = true;
                                                   if (true) return;
                                                }
                                                AV206ValorIni = (decimal)(AV203PFInicial*A512ContagemResultado_ValorPF);
                                                AV105Valor = (decimal)(AV98PFFinal*A512ContagemResultado_ValorPF);
                                                AV207ValorTotal = (decimal)(AV105Valor+AV206ValorIni);
                                                if ( ( AV105Valor == Convert.ToDecimal( 0 )) )
                                                {
                                                   AV105Valor = NumberUtil.Round( AV98PFFinal*A512ContagemResultado_ValorPF, 2);
                                                   AV202strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV105Valor, 14, 2), " ", "");
                                                }
                                                else if ( AV105Valor < 0.001m )
                                                {
                                                   AV105Valor = NumberUtil.Round( AV98PFFinal*A512ContagemResultado_ValorPF, 4);
                                                   AV202strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV105Valor, 14, 4), " ", "");
                                                }
                                                else if ( AV105Valor < 0.01m )
                                                {
                                                   AV105Valor = NumberUtil.Round( AV98PFFinal*A512ContagemResultado_ValorPF, 3);
                                                   AV202strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV105Valor, 14, 3), " ", "");
                                                }
                                                else
                                                {
                                                   AV105Valor = NumberUtil.Round( AV98PFFinal*A512ContagemResultado_ValorPF, 2);
                                                   AV202strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV105Valor, 14, 2), " ", "");
                                                }
                                                if ( ( AV206ValorIni == Convert.ToDecimal( 0 )) )
                                                {
                                                   AV206ValorIni = NumberUtil.Round( AV203PFInicial*A512ContagemResultado_ValorPF, 2);
                                                   AV205strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV206ValorIni, 14, 2), " ", "");
                                                }
                                                else if ( AV206ValorIni < 0.001m )
                                                {
                                                   AV206ValorIni = NumberUtil.Round( AV203PFInicial*A512ContagemResultado_ValorPF, 4);
                                                   AV205strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV206ValorIni, 14, 4), " ", "");
                                                }
                                                else if ( AV206ValorIni < 0.01m )
                                                {
                                                   AV206ValorIni = NumberUtil.Round( AV203PFInicial*A512ContagemResultado_ValorPF, 3);
                                                   AV205strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV206ValorIni, 14, 3), " ", "");
                                                }
                                                else
                                                {
                                                   AV206ValorIni = NumberUtil.Round( AV203PFInicial*A512ContagemResultado_ValorPF, 2);
                                                   AV205strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV206ValorIni, 14, 2), " ", "");
                                                }
                                                if ( AV207ValorTotal < 0.001m )
                                                {
                                                   AV208strValorTotal = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV207ValorTotal, 14, 4), " ", "");
                                                }
                                                else if ( AV207ValorTotal < 0.01m )
                                                {
                                                   AV208strValorTotal = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV207ValorTotal, 14, 3), " ", "");
                                                }
                                                else
                                                {
                                                   AV208strValorTotal = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV207ValorTotal, 14, 2), " ", "");
                                                }
                                                if ( AV203PFInicial + AV98PFFinal < 0.001m )
                                                {
                                                   AV210strPFTotal = StringUtil.Str( AV203PFInicial+AV98PFFinal, 14, 4);
                                                }
                                                else
                                                {
                                                   AV210strPFTotal = StringUtil.Str( AV203PFInicial+AV98PFFinal, 14, 3);
                                                }
                                                AV82ImprimindoOS = true;
                                                H3G0( false, 390) ;
                                                getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("1.  Identifica��o da Empresa Contratada ", 17, Gx_line+13, 359, Gx_line+32, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("2. Informa��es sobre os Servi�os a serem Realizados", 17, Gx_line+186, 442, Gx_line+205, 0, 0, 0, 0) ;
                                                getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV102Sistema_Sigla, "@!")), 92, Gx_line+204, 275, Gx_line+221, 0+256, 0, 0, 0) ;
                                                getPrinter().GxDrawText(context.localUtil.Format( AV60DataInicio, "99/99/99"), 258, Gx_line+313, 328, Gx_line+330, 2, 0, 0, 0) ;
                                                getPrinter().GxDrawText(context.localUtil.Format( AV59DataFim, "99/99/99"), 358, Gx_line+313, 428, Gx_line+330, 2, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54Contratada_PessoaNom, "@!")), 159, Gx_line+40, 889, Gx_line+57, 0+256, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV53Contratada_PessoaCNPJ, "")), 76, Gx_line+61, 186, Gx_line+78, 0+256, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV93Pessoa_Endereco, "")), 17, Gx_line+100, 747, Gx_line+117, 0+256, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV96Pessoa_Telefone, "")), 367, Gx_line+150, 477, Gx_line+167, 0+256, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV92Pessoa_CEP, "")), 84, Gx_line+150, 158, Gx_line+167, 0+256, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV94Pessoa_Fax, "")), 576, Gx_line+150, 686, Gx_line+167, 0+256, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV97Pessoa_UF, "@!")), 634, Gx_line+118, 660, Gx_line+135, 1+256, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV85Municipio_Nome, "@!")), 84, Gx_line+122, 450, Gx_line+139, 0+256, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV95Pessoa_IE, "99.999.999/999-99999")), 551, Gx_line+68, 698, Gx_line+85, 1+256, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV106Volume, "")), 17, Gx_line+363, 495, Gx_line+380, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV79Identificacao1, "")), 17, Gx_line+254, 747, Gx_line+271, 0+256, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV80Identificacao2, "")), 17, Gx_line+271, 747, Gx_line+288, 0+256, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV81Identificacao3, "")), 17, Gx_line+287, 747, Gx_line+304, 0+256, 0, 0, 0) ;
                                                getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("PF", 619, Gx_line+362, 636, Gx_line+381, 0+256, 0, 0, 0) ;
                                                getPrinter().GxDrawText("a", 350, Gx_line+313, 358, Gx_line+332, 0+256, 0, 0, 0) ;
                                                getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("Nome da Empresa:", 17, Gx_line+40, 146, Gx_line+58, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Inscri��o Estadual:", 409, Gx_line+68, 538, Gx_line+86, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("CNPJ:", 17, Gx_line+61, 63, Gx_line+79, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Endere�o:", 16, Gx_line+82, 93, Gx_line+100, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Cidade:", 17, Gx_line+122, 77, Gx_line+140, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("UF:", 592, Gx_line+118, 621, Gx_line+136, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("CEP:", 17, Gx_line+150, 68, Gx_line+168, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Telefone:", 276, Gx_line+150, 355, Gx_line+168, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Fax:", 517, Gx_line+150, 566, Gx_line+168, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Sistema:", 17, Gx_line+204, 86, Gx_line+222, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Identifica��o do Servi�o:", 17, Gx_line+237, 184, Gx_line+256, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Volume de Servi�o:", 17, Gx_line+347, 150, Gx_line+366, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Per�odo de Execu��o do Servi�o:", 17, Gx_line+313, 234, Gx_line+332, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV210strPFTotal, "")), 494, Gx_line+361, 612, Gx_line+381, 2+256, 0, 0, 0) ;
                                                getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+162, 781, Gx_line+177, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+0, 781, Gx_line+15, 0, 0, 0, 0) ;
                                                Gx_OldLine = Gx_line;
                                                Gx_line = (int)(Gx_line+390);
                                                H3G0( false, 354) ;
                                                getPrinter().GxDrawLine(275, Gx_line+317, 558, Gx_line+317, 1, 0, 0, 0, 0) ;
                                                getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("5. Solicitante", 17, Gx_line+150, 192, Gx_line+169, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("4. Gestor da Ordem de Servi�o", 17, Gx_line+100, 308, Gx_line+119, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("3. Local de Execu��o do Servi�o", 17, Gx_line+50, 325, Gx_line+69, 0, 0, 0, 0) ;
                                                getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("Relat�rios T�cnicos de Contagem de Pontos de Fun��o", 17, Gx_line+17, 326, Gx_line+35, 0+256, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Minist�rio da Sa�de", 287, Gx_line+67, 420, Gx_line+85, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("DATASUS - Minist�rio da Sa�de", 100, Gx_line+167, 300, Gx_line+185, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Cristiane Sousa de Oliveira", 17, Gx_line+117, 192, Gx_line+135, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV101Sistema_Coordenacao, "@!")), 473, Gx_line+202, 782, Gx_line+219, 0, 0, 0, 0) ;
                                                getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("Gestor Contratual", 328, Gx_line+317, 505, Gx_line+335, 1, 0, 0, 0) ;
                                                getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("Bras�lia, ___ de ___________ de ____.", 275, Gx_line+241, 517, Gx_line+260, 0, 0, 0, 0) ;
                                                getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("Unidade:", 17, Gx_line+167, 87, Gx_line+185, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Identifica��o do Local de Execu��o:", 17, Gx_line+67, 275, Gx_line+85, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Produtos a Serem Entregues:", 17, Gx_line+0, 217, Gx_line+19, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Respons�vel pela solicita��o: (�rg�o, Secretaria, Coordena��o):", 17, Gx_line+200, 467, Gx_line+219, 0, 0, 0, 0) ;
                                                getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+29, 781, Gx_line+46, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+79, 781, Gx_line+96, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+129, 781, Gx_line+146, 0, 0, 0, 0) ;
                                                Gx_OldLine = Gx_line;
                                                Gx_line = (int)(Gx_line+354);
                                                H3G0( false, 306) ;
                                                getPrinter().GxDrawLine(19, Gx_line+188, 782, Gx_line+188, 1, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawLine(511, Gx_line+52, 511, Gx_line+235, 1, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawLine(657, Gx_line+51, 657, Gx_line+235, 1, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawLine(19, Gx_line+52, 19, Gx_line+188, 1, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawLine(187, Gx_line+51, 187, Gx_line+189, 1, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawLine(782, Gx_line+51, 782, Gx_line+235, 1, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawLine(512, Gx_line+235, 782, Gx_line+235, 1, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawLine(19, Gx_line+128, 782, Gx_line+128, 1, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawLine(20, Gx_line+80, 782, Gx_line+80, 1, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawLine(19, Gx_line+51, 782, Gx_line+51, 1, 0, 0, 0, 0) ;
                                                getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("7. Ciente da Contratada", 17, Gx_line+260, 234, Gx_line+279, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("6. Custo da Ordem de Servi�o", 17, Gx_line+7, 267, Gx_line+26, 0, 0, 0, 0) ;
                                                getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("Contagem de PF Final", 27, Gx_line+145, 169, Gx_line+163, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("(Parcela 2)", 61, Gx_line+162, 128, Gx_line+180, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("100%", 569, Gx_line+150, 602, Gx_line+168, 1, 0, 0, 0) ;
                                                getPrinter().GxDrawText("50%", 569, Gx_line+96, 594, Gx_line+114, 1, 0, 0, 0) ;
                                                getPrinter().GxDrawText("(Parcela 1)", 69, Gx_line+102, 136, Gx_line+120, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Contagem de PF Inicial", 27, Gx_line+85, 177, Gx_line+103, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("O quadro a seguir descreve o custo dos servi�os a serem executados:", 17, Gx_line+31, 413, Gx_line+49, 0+256, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV202strValor, "")), 675, Gx_line+150, 767, Gx_line+167, 2, 0, 0, 1) ;
                                                getPrinter().GxDrawText("(*)", 392, Gx_line+96, 417, Gx_line+114, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("(*) Este valor j� esta l�quido conforme % ao lado.", 17, Gx_line+217, 325, Gx_line+235, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV205strValorIni, "")), 671, Gx_line+96, 767, Gx_line+113, 2, 0, 0, 0) ;
                                                getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("VALOR", 694, Gx_line+57, 752, Gx_line+75, 1, 0, 0, 0) ;
                                                getPrinter().GxDrawText("% FASE", 550, Gx_line+57, 617, Gx_line+75, 1, 0, 0, 0) ;
                                                getPrinter().GxDrawText("QUANTIDADE DE PONTOS DE FUN��O", 200, Gx_line+57, 475, Gx_line+75, 1, 0, 0, 0) ;
                                                getPrinter().GxDrawText("SERVI�O", 58, Gx_line+57, 125, Gx_line+75, 1, 0, 0, 0) ;
                                                getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("Declaramos nossa ci�ncia e concord�ncia com rela��o aos servi�os discriminados  no  item 02.", 17, Gx_line+277, 700, Gx_line+296, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV201strPFFinal, "")), 273, Gx_line+150, 391, Gx_line+170, 1, 0, 0, 1) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV208strValorTotal, "")), 675, Gx_line+203, 768, Gx_line+223, 2, 0, 0, 1) ;
                                                getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV204strPFInicial, "")), 273, Gx_line+95, 391, Gx_line+115, 1, 0, 0, 1) ;
                                                getPrinter().GxAttris("Calibri", 12, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("TOTAL", 542, Gx_line+203, 642, Gx_line+223, 0, 0, 0, 0) ;
                                                getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+244, 781, Gx_line+259, 0, 0, 0, 0) ;
                                                Gx_OldLine = Gx_line;
                                                Gx_line = (int)(Gx_line+306);
                                                H3G0( false, 162) ;
                                                getPrinter().GxDrawLine(258, Gx_line+74, 541, Gx_line+74, 1, 0, 0, 0, 0) ;
                                                getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("Respons�vel Legal/T�cnico", 308, Gx_line+74, 491, Gx_line+92, 1, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Pedro Augusto de Ara�jo, CFPS", 292, Gx_line+91, 509, Gx_line+109, 1, 0, 0, 0) ;
                                                getPrinter().GxDrawText("EFIC�CIA ORGANIZA��O LTDA", 292, Gx_line+107, 509, Gx_line+125, 1, 0, 0, 0) ;
                                                getPrinter().GxDrawText("CNPJ: 00.665.620/0001-40", 308, Gx_line+124, 491, Gx_line+142, 1, 0, 0, 0) ;
                                                getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("Bras�lia, ___ de ___________ de ____.", 267, Gx_line+17, 541, Gx_line+36, 0, 0, 0, 0) ;
                                                getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+141, 781, Gx_line+158, 0, 0, 0, 0) ;
                                                Gx_OldLine = Gx_line;
                                                Gx_line = (int)(Gx_line+162);
                                                H3G0( false, 120) ;
                                                getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("8. Cancelamento da Ordem de Servi�o", 17, Gx_line+0, 317, Gx_line+19, 0, 0, 0, 0) ;
                                                getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                                getPrinter().GxDrawText("Motivo do Cancelamento:", 17, Gx_line+17, 192, Gx_line+35, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Assinatura do Gestor da Ordem de Servi�o:", 150, Gx_line+50, 433, Gx_line+68, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Assinatura do Gestor de Contrato:", 492, Gx_line+50, 725, Gx_line+68, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Data:", 25, Gx_line+50, 67, Gx_line+68, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Ci�ncia do Preposto da Contratada:", 400, Gx_line+100, 642, Gx_line+118, 0, 0, 0, 0) ;
                                                getPrinter().GxDrawText("Assinatura do Coordenador Geral Respons�vel:", 25, Gx_line+100, 342, Gx_line+118, 0, 0, 0, 0) ;
                                                Gx_OldLine = Gx_line;
                                                Gx_line = (int)(Gx_line+120);
                                                /* Eject command */
                                                Gx_OldLine = Gx_line;
                                                Gx_line = (int)(P_lines+1);
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
            if ( ! BRK3G5 )
            {
               BRK3G5 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S145( )
      {
         /* 'CONTAGENSDOSISTEMA' Routine */
         AV203PFInicial = 0;
         AV98PFFinal = 0;
         AV327ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod = AV52Contratada_AreaTrabalhoCod;
         AV328ExtraWWContagemResultadoExtraSelectionDS_2_Contagemresultado_statuscnt = AV45ContagemResultado_StatusCnt;
         AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = AV65DynamicFiltersSelector1;
         AV330ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 = AV225DynamicFiltersOperator1;
         AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 = AV19ContagemResultado_DataCnt1;
         AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 = AV16ContagemResultado_DataCnt_To1;
         AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 = AV47ContagemResultado_StatusDmn1;
         AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = AV29ContagemResultado_OsFsOsFm1;
         AV335ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 = AV296ContagemResultado_DataDmn1;
         AV336ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 = AV291ContagemResultado_DataDmn_To1;
         AV337ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 = DateTimeUtil.ResetTime(AV307ContagemResultado_DataPrevista1);
         AV338ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 = DateTimeUtil.ResetTime(AV302ContagemResultado_DataPrevista_To1);
         AV339ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 = AV133ContagemResultado_Servico1;
         AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 = AV172ContagemResultado_ContadorFM1;
         AV341ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 = AV38ContagemResultado_SistemaCod1;
         AV342ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 = AV190ContagemResultado_ContratadaCod1;
         AV343ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 = AV141ContagemResultado_ServicoGrupo1;
         AV344ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 = AV137ContagemResultado_Baseline1;
         AV345ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 = AV25ContagemResultado_NaoCnfDmnCod1;
         AV346ExtraWWContagemResultadoExtraSelectionDS_20_Contagemresultado_pfbfm1 = AV177ContagemResultado_PFBFM1;
         AV347ExtraWWContagemResultadoExtraSelectionDS_21_Contagemresultado_pfbfs1 = AV182ContagemResultado_PFBFS1;
         AV348ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 = AV196ContagemResultado_Agrupador1;
         AV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 = AV212ContagemResultado_Descricao1;
         AV350ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 = AV218ContagemResultado_Owner1;
         AV351ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 = AV233ContagemResultado_TemPndHmlg1;
         AV352ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 = AV317ContagemResultado_CntCod1;
         AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = AV63DynamicFiltersEnabled2;
         AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = AV66DynamicFiltersSelector2;
         AV355ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 = AV226DynamicFiltersOperator2;
         AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 = AV20ContagemResultado_DataCnt2;
         AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 = AV17ContagemResultado_DataCnt_To2;
         AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 = AV48ContagemResultado_StatusDmn2;
         AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = AV30ContagemResultado_OsFsOsFm2;
         AV360ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 = AV297ContagemResultado_DataDmn2;
         AV361ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 = AV292ContagemResultado_DataDmn_To2;
         AV362ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 = DateTimeUtil.ResetTime(AV308ContagemResultado_DataPrevista2);
         AV363ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 = DateTimeUtil.ResetTime(AV303ContagemResultado_DataPrevista_To2);
         AV364ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 = AV135ContagemResultado_Servico2;
         AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 = AV173ContagemResultado_ContadorFM2;
         AV366ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 = AV39ContagemResultado_SistemaCod2;
         AV367ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 = AV191ContagemResultado_ContratadaCod2;
         AV368ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 = AV142ContagemResultado_ServicoGrupo2;
         AV369ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 = AV138ContagemResultado_Baseline2;
         AV370ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 = AV26ContagemResultado_NaoCnfDmnCod2;
         AV371ExtraWWContagemResultadoExtraSelectionDS_45_Contagemresultado_pfbfm2 = AV178ContagemResultado_PFBFM2;
         AV372ExtraWWContagemResultadoExtraSelectionDS_46_Contagemresultado_pfbfs2 = AV183ContagemResultado_PFBFS2;
         AV373ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 = AV197ContagemResultado_Agrupador2;
         AV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 = AV213ContagemResultado_Descricao2;
         AV375ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 = AV219ContagemResultado_Owner2;
         AV376ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 = AV234ContagemResultado_TemPndHmlg2;
         AV377ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 = AV318ContagemResultado_CntCod2;
         AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = AV64DynamicFiltersEnabled3;
         AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = AV67DynamicFiltersSelector3;
         AV380ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 = AV227DynamicFiltersOperator3;
         AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 = AV21ContagemResultado_DataCnt3;
         AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 = AV18ContagemResultado_DataCnt_To3;
         AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 = AV49ContagemResultado_StatusDmn3;
         AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = AV31ContagemResultado_OsFsOsFm3;
         AV385ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 = AV298ContagemResultado_DataDmn3;
         AV386ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 = AV293ContagemResultado_DataDmn_To3;
         AV387ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 = DateTimeUtil.ResetTime(AV309ContagemResultado_DataPrevista3);
         AV388ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 = DateTimeUtil.ResetTime(AV304ContagemResultado_DataPrevista_To3);
         AV389ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 = AV136ContagemResultado_Servico3;
         AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 = AV174ContagemResultado_ContadorFM3;
         AV391ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 = AV40ContagemResultado_SistemaCod3;
         AV392ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 = AV192ContagemResultado_ContratadaCod3;
         AV393ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 = AV143ContagemResultado_ServicoGrupo3;
         AV394ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 = AV139ContagemResultado_Baseline3;
         AV395ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 = AV27ContagemResultado_NaoCnfDmnCod3;
         AV396ExtraWWContagemResultadoExtraSelectionDS_70_Contagemresultado_pfbfm3 = AV179ContagemResultado_PFBFM3;
         AV397ExtraWWContagemResultadoExtraSelectionDS_71_Contagemresultado_pfbfs3 = AV184ContagemResultado_PFBFS3;
         AV398ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 = AV198ContagemResultado_Agrupador3;
         AV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 = AV214ContagemResultado_Descricao3;
         AV400ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 = AV220ContagemResultado_Owner3;
         AV401ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 = AV235ContagemResultado_TemPndHmlg3;
         AV402ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 = AV319ContagemResultado_CntCod3;
         AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = AV146DynamicFiltersEnabled4;
         AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = AV148DynamicFiltersSelector4;
         AV405ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 = AV228DynamicFiltersOperator4;
         AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 = AV155ContagemResultado_DataCnt4;
         AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 = AV154ContagemResultado_DataCnt_To4;
         AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 = AV164ContagemResultado_StatusDmn4;
         AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = AV159ContagemResultado_OsFsOsFm4;
         AV410ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 = AV299ContagemResultado_DataDmn4;
         AV411ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 = AV294ContagemResultado_DataDmn_To4;
         AV412ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 = DateTimeUtil.ResetTime(AV310ContagemResultado_DataPrevista4);
         AV413ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 = DateTimeUtil.ResetTime(AV305ContagemResultado_DataPrevista_To4);
         AV414ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 = AV160ContagemResultado_Servico4;
         AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 = AV175ContagemResultado_ContadorFM4;
         AV416ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 = AV162ContagemResultado_SistemaCod4;
         AV417ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 = AV193ContagemResultado_ContratadaCod4;
         AV418ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 = AV161ContagemResultado_ServicoGrupo4;
         AV419ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 = AV150ContagemResultado_Baseline4;
         AV420ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 = AV158ContagemResultado_NaoCnfDmnCod4;
         AV421ExtraWWContagemResultadoExtraSelectionDS_95_Contagemresultado_pfbfm4 = AV180ContagemResultado_PFBFM4;
         AV422ExtraWWContagemResultadoExtraSelectionDS_96_Contagemresultado_pfbfs4 = AV185ContagemResultado_PFBFS4;
         AV423ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 = AV199ContagemResultado_Agrupador4;
         AV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 = AV215ContagemResultado_Descricao4;
         AV425ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 = AV221ContagemResultado_Owner4;
         AV426ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 = AV236ContagemResultado_TemPndHmlg4;
         AV427ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 = AV320ContagemResultado_CntCod4;
         AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = AV147DynamicFiltersEnabled5;
         AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = AV149DynamicFiltersSelector5;
         AV430ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 = AV229DynamicFiltersOperator5;
         AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 = AV157ContagemResultado_DataCnt5;
         AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 = AV156ContagemResultado_DataCnt_To5;
         AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 = AV171ContagemResultado_StatusDmn5;
         AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = AV166ContagemResultado_OsFsOsFm5;
         AV435ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 = AV300ContagemResultado_DataDmn5;
         AV436ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 = AV295ContagemResultado_DataDmn_To5;
         AV437ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 = DateTimeUtil.ResetTime(AV311ContagemResultado_DataPrevista5);
         AV438ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 = DateTimeUtil.ResetTime(AV306ContagemResultado_DataPrevista_To5);
         AV439ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 = AV167ContagemResultado_Servico5;
         AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 = AV176ContagemResultado_ContadorFM5;
         AV441ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 = AV169ContagemResultado_SistemaCod5;
         AV442ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 = AV194ContagemResultado_ContratadaCod5;
         AV443ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 = AV168ContagemResultado_ServicoGrupo5;
         AV444ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 = AV151ContagemResultado_Baseline5;
         AV445ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 = AV165ContagemResultado_NaoCnfDmnCod5;
         AV446ExtraWWContagemResultadoExtraSelectionDS_120_Contagemresultado_pfbfm5 = AV181ContagemResultado_PFBFM5;
         AV447ExtraWWContagemResultadoExtraSelectionDS_121_Contagemresultado_pfbfs5 = AV186ContagemResultado_PFBFS5;
         AV448ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 = AV200ContagemResultado_Agrupador5;
         AV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 = AV216ContagemResultado_Descricao5;
         AV450ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 = AV222ContagemResultado_Owner5;
         AV451ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 = AV237ContagemResultado_TemPndHmlg5;
         AV452ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 = AV321ContagemResultado_CntCod5;
         AV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador = AV238TFContagemResultado_Agrupador;
         AV454ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel = AV239TFContagemResultado_Agrupador_Sel;
         AV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm = AV247TFContagemResultado_DemandaFM;
         AV456ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel = AV248TFContagemResultado_DemandaFM_Sel;
         AV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda = AV245TFContagemResultado_Demanda;
         AV458ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel = AV246TFContagemResultado_Demanda_Sel;
         AV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao = AV249TFContagemResultado_Descricao;
         AV460ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel = AV250TFContagemResultado_Descricao_Sel;
         AV461ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista = AV314TFContagemResultado_DataPrevista;
         AV462ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to = AV315TFContagemResultado_DataPrevista_To;
         AV463ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn = AV312TFContagemResultado_DataDmn;
         AV464ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to = AV313TFContagemResultado_DataDmn_To;
         AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt = AV243TFContagemResultado_DataUltCnt;
         AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to = AV244TFContagemResultado_DataUltCnt_To;
         AV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla = AV241TFContagemResultado_ContratadaSigla;
         AV468ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel = AV242TFContagemResultado_ContratadaSigla_Sel;
         AV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord = AV255TFContagemResultado_SistemaCoord;
         AV470ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel = AV256TFContagemResultado_SistemaCoord_Sel;
         AV471ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels = AV259TFContagemResultado_StatusDmn_Sels;
         AV472ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel = AV240TFContagemResultado_Baseline_Sel;
         AV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla = AV253TFContagemResultado_ServicoSigla;
         AV474ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel = AV254TFContagemResultado_ServicoSigla_Sel;
         AV475ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal = AV251TFContagemResultado_PFFinal;
         AV476ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to = AV252TFContagemResultado_PFFinal_To;
         AV477ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf = AV261TFContagemResultado_ValorPF;
         AV478ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to = AV262TFContagemResultado_ValorPF_To;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A484ContagemResultado_StatusDmn ,
                                              AV471ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels ,
                                              AV10WWPContext.gxTpr_Contratada_codigo ,
                                              AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 ,
                                              AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 ,
                                              AV330ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 ,
                                              AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 ,
                                              AV335ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 ,
                                              AV336ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 ,
                                              AV337ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 ,
                                              AV338ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 ,
                                              AV339ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 ,
                                              AV341ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 ,
                                              AV342ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 ,
                                              AV327ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod ,
                                              AV343ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 ,
                                              AV344ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 ,
                                              AV345ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 ,
                                              AV348ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 ,
                                              AV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 ,
                                              AV350ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 ,
                                              AV352ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 ,
                                              AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 ,
                                              AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 ,
                                              AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 ,
                                              AV355ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 ,
                                              AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 ,
                                              AV360ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 ,
                                              AV361ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 ,
                                              AV362ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 ,
                                              AV363ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 ,
                                              AV364ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 ,
                                              AV366ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 ,
                                              AV367ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 ,
                                              AV368ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 ,
                                              AV369ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 ,
                                              AV370ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 ,
                                              AV373ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 ,
                                              AV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 ,
                                              AV375ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 ,
                                              AV377ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 ,
                                              AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 ,
                                              AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 ,
                                              AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 ,
                                              AV380ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 ,
                                              AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 ,
                                              AV385ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 ,
                                              AV386ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 ,
                                              AV387ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 ,
                                              AV388ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 ,
                                              AV389ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 ,
                                              AV391ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 ,
                                              AV392ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 ,
                                              AV393ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 ,
                                              AV394ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 ,
                                              AV395ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 ,
                                              AV398ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 ,
                                              AV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 ,
                                              AV400ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 ,
                                              AV402ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 ,
                                              AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 ,
                                              AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 ,
                                              AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 ,
                                              AV405ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 ,
                                              AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 ,
                                              AV410ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 ,
                                              AV411ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 ,
                                              AV412ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 ,
                                              AV413ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 ,
                                              AV414ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 ,
                                              AV416ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 ,
                                              AV417ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 ,
                                              AV418ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 ,
                                              AV419ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 ,
                                              AV420ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 ,
                                              AV423ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 ,
                                              AV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 ,
                                              AV425ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 ,
                                              AV427ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 ,
                                              AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 ,
                                              AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 ,
                                              AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 ,
                                              AV430ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 ,
                                              AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 ,
                                              AV435ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 ,
                                              AV436ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 ,
                                              AV437ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 ,
                                              AV438ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 ,
                                              AV439ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 ,
                                              AV441ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 ,
                                              AV442ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 ,
                                              AV443ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 ,
                                              AV444ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 ,
                                              AV445ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 ,
                                              AV448ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 ,
                                              AV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 ,
                                              AV450ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 ,
                                              AV452ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 ,
                                              AV454ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel ,
                                              AV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador ,
                                              AV456ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel ,
                                              AV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm ,
                                              AV458ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel ,
                                              AV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda ,
                                              AV460ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel ,
                                              AV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao ,
                                              AV461ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista ,
                                              AV462ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to ,
                                              AV463ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn ,
                                              AV464ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to ,
                                              AV468ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel ,
                                              AV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla ,
                                              AV470ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel ,
                                              AV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord ,
                                              AV471ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV472ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel ,
                                              AV474ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel ,
                                              AV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla ,
                                              AV477ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf ,
                                              AV478ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A1351ContagemResultado_DataPrevista ,
                                              A601ContagemResultado_Servico ,
                                              A489ContagemResultado_SistemaCod ,
                                              A764ContagemResultado_ServicoGrupo ,
                                              A598ContagemResultado_Baseline ,
                                              A468ContagemResultado_NaoCnfDmnCod ,
                                              A1046ContagemResultado_Agrupador ,
                                              A494ContagemResultado_Descricao ,
                                              A508ContagemResultado_Owner ,
                                              A1603ContagemResultado_CntCod ,
                                              A803ContagemResultado_ContratadaSigla ,
                                              A515ContagemResultado_SistemaCoord ,
                                              A801ContagemResultado_ServicoSigla ,
                                              A512ContagemResultado_ValorPF ,
                                              A531ContagemResultado_StatusUltCnt ,
                                              A1854ContagemResultado_VlrCnc ,
                                              AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 ,
                                              AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 ,
                                              A584ContagemResultado_ContadorFM ,
                                              A684ContagemResultado_PFBFSUltima ,
                                              A682ContagemResultado_PFBFMUltima ,
                                              A685ContagemResultado_PFLFSUltima ,
                                              A683ContagemResultado_PFLFMUltima ,
                                              AV351ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 ,
                                              A1802ContagemResultado_TemPndHmlg ,
                                              AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 ,
                                              AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 ,
                                              AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 ,
                                              AV376ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 ,
                                              AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 ,
                                              AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 ,
                                              AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 ,
                                              AV401ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 ,
                                              AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 ,
                                              AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 ,
                                              AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 ,
                                              AV426ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 ,
                                              AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 ,
                                              AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 ,
                                              AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 ,
                                              AV451ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 ,
                                              AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt ,
                                              AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to ,
                                              AV475ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ,
                                              A574ContagemResultado_PFFinal ,
                                              AV476ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV10WWPContext.gxTpr_Areatrabalho_codigo ,
                                              AV224ContagemResultado_SS ,
                                              A1452ContagemResultado_SS },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
         lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
         lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
         lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1), "%", "");
         lV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1), "%", "");
         lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
         lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
         lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
         lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2), "%", "");
         lV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2), "%", "");
         lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
         lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
         lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
         lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3), "%", "");
         lV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3), "%", "");
         lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
         lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
         lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
         lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4), "%", "");
         lV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 = StringUtil.Concat( StringUtil.RTrim( AV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4), "%", "");
         lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
         lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
         lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
         lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5), "%", "");
         lV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 = StringUtil.Concat( StringUtil.RTrim( AV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5), "%", "");
         lV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador = StringUtil.PadR( StringUtil.RTrim( AV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador), 15, "%");
         lV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm), "%", "");
         lV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda = StringUtil.Concat( StringUtil.RTrim( AV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda), "%", "");
         lV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao = StringUtil.Concat( StringUtil.RTrim( AV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao), "%", "");
         lV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla = StringUtil.PadR( StringUtil.RTrim( AV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla), 15, "%");
         lV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord = StringUtil.Concat( StringUtil.RTrim( AV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord), "%", "");
         lV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P003G6 */
         pr_default.execute(2, new Object[] {AV224ContagemResultado_SS, AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1, AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1, AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1, AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1, AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1, AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1, AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1, AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2, AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2, AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2, AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2, AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2, AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2, AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2, AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2, AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3, AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3, AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3, AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3, AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3, AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3, AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3, AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3, AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4, AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4, AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4, AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4, AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4, AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4, AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4, AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4, AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5, AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5, AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5, AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5, AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5, AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5, AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5, AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5, AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt, AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt, AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to, AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to, AV10WWPContext.gxTpr_Areatrabalho_codigo, AV10WWPContext.gxTpr_Contratada_codigo, AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1, AV335ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1, AV336ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1, AV337ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1, AV338ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1, AV339ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1, AV341ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1, AV342ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1, AV343ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1, AV345ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1, AV348ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1, lV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1, AV350ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1, AV352ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1, AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2, AV360ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2, AV361ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2, AV362ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2,
         AV363ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2, AV364ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2, AV366ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2, AV367ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2, AV368ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2, AV370ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2, AV373ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2, lV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2, AV375ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2, AV377ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2, AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3, AV385ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3, AV386ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3, AV387ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3, AV388ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3, AV389ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3, AV391ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3, AV392ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3, AV393ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3, AV395ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3, AV398ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3, lV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3, AV400ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3, AV402ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3, AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4, AV410ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4, AV411ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4, AV412ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4, AV413ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4, AV414ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4, AV416ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4, AV417ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4, AV418ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4, AV420ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4, AV423ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4, lV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4, AV425ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4, AV427ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4, AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5, AV435ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5, AV436ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5, AV437ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5, AV438ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5, AV439ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5, AV441ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5, AV442ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5, AV443ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5, AV445ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5, AV448ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5, lV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5, AV450ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5, AV452ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5, lV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador, AV454ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel, lV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm, AV456ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel, lV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda, AV458ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel, lV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao, AV460ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel, AV461ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista, AV462ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to, AV463ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn, AV464ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to, lV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla, AV468ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel, lV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord, AV470ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel, lV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla, AV474ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel, AV477ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf, AV478ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P003G6_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P003G6_n1553ContagemResultado_CntSrvCod[0];
            A52Contratada_AreaTrabalhoCod = P003G6_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P003G6_n52Contratada_AreaTrabalhoCod[0];
            A484ContagemResultado_StatusDmn = P003G6_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P003G6_n484ContagemResultado_StatusDmn[0];
            A1452ContagemResultado_SS = P003G6_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = P003G6_n1452ContagemResultado_SS[0];
            A512ContagemResultado_ValorPF = P003G6_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P003G6_n512ContagemResultado_ValorPF[0];
            A801ContagemResultado_ServicoSigla = P003G6_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P003G6_n801ContagemResultado_ServicoSigla[0];
            A515ContagemResultado_SistemaCoord = P003G6_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P003G6_n515ContagemResultado_SistemaCoord[0];
            A803ContagemResultado_ContratadaSigla = P003G6_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P003G6_n803ContagemResultado_ContratadaSigla[0];
            A1603ContagemResultado_CntCod = P003G6_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P003G6_n1603ContagemResultado_CntCod[0];
            A494ContagemResultado_Descricao = P003G6_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P003G6_n494ContagemResultado_Descricao[0];
            A1046ContagemResultado_Agrupador = P003G6_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P003G6_n1046ContagemResultado_Agrupador[0];
            A468ContagemResultado_NaoCnfDmnCod = P003G6_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P003G6_n468ContagemResultado_NaoCnfDmnCod[0];
            A598ContagemResultado_Baseline = P003G6_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P003G6_n598ContagemResultado_Baseline[0];
            A764ContagemResultado_ServicoGrupo = P003G6_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P003G6_n764ContagemResultado_ServicoGrupo[0];
            A489ContagemResultado_SistemaCod = P003G6_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P003G6_n489ContagemResultado_SistemaCod[0];
            A508ContagemResultado_Owner = P003G6_A508ContagemResultado_Owner[0];
            A601ContagemResultado_Servico = P003G6_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P003G6_n601ContagemResultado_Servico[0];
            A1351ContagemResultado_DataPrevista = P003G6_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P003G6_n1351ContagemResultado_DataPrevista[0];
            A471ContagemResultado_DataDmn = P003G6_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P003G6_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P003G6_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P003G6_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P003G6_n457ContagemResultado_Demanda[0];
            A1854ContagemResultado_VlrCnc = P003G6_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P003G6_n1854ContagemResultado_VlrCnc[0];
            A490ContagemResultado_ContratadaCod = P003G6_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P003G6_n490ContagemResultado_ContratadaCod[0];
            A1623ContagemResultado_CntSrvMmn = P003G6_A1623ContagemResultado_CntSrvMmn[0];
            n1623ContagemResultado_CntSrvMmn = P003G6_n1623ContagemResultado_CntSrvMmn[0];
            A1855ContagemResultado_PFCnc = P003G6_A1855ContagemResultado_PFCnc[0];
            n1855ContagemResultado_PFCnc = P003G6_n1855ContagemResultado_PFCnc[0];
            A683ContagemResultado_PFLFMUltima = P003G6_A683ContagemResultado_PFLFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P003G6_A685ContagemResultado_PFLFSUltima[0];
            A682ContagemResultado_PFBFMUltima = P003G6_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = P003G6_A684ContagemResultado_PFBFSUltima[0];
            A584ContagemResultado_ContadorFM = P003G6_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P003G6_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P003G6_n566ContagemResultado_DataUltCnt[0];
            A531ContagemResultado_StatusUltCnt = P003G6_A531ContagemResultado_StatusUltCnt[0];
            A456ContagemResultado_Codigo = P003G6_A456ContagemResultado_Codigo[0];
            A473ContagemResultado_DataCnt = P003G6_A473ContagemResultado_DataCnt[0];
            A511ContagemResultado_HoraCnt = P003G6_A511ContagemResultado_HoraCnt[0];
            A1553ContagemResultado_CntSrvCod = P003G6_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P003G6_n1553ContagemResultado_CntSrvCod[0];
            A484ContagemResultado_StatusDmn = P003G6_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P003G6_n484ContagemResultado_StatusDmn[0];
            A1452ContagemResultado_SS = P003G6_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = P003G6_n1452ContagemResultado_SS[0];
            A512ContagemResultado_ValorPF = P003G6_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P003G6_n512ContagemResultado_ValorPF[0];
            A494ContagemResultado_Descricao = P003G6_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P003G6_n494ContagemResultado_Descricao[0];
            A1046ContagemResultado_Agrupador = P003G6_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P003G6_n1046ContagemResultado_Agrupador[0];
            A468ContagemResultado_NaoCnfDmnCod = P003G6_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P003G6_n468ContagemResultado_NaoCnfDmnCod[0];
            A598ContagemResultado_Baseline = P003G6_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P003G6_n598ContagemResultado_Baseline[0];
            A489ContagemResultado_SistemaCod = P003G6_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P003G6_n489ContagemResultado_SistemaCod[0];
            A508ContagemResultado_Owner = P003G6_A508ContagemResultado_Owner[0];
            A1351ContagemResultado_DataPrevista = P003G6_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P003G6_n1351ContagemResultado_DataPrevista[0];
            A471ContagemResultado_DataDmn = P003G6_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P003G6_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P003G6_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P003G6_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P003G6_n457ContagemResultado_Demanda[0];
            A1854ContagemResultado_VlrCnc = P003G6_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P003G6_n1854ContagemResultado_VlrCnc[0];
            A490ContagemResultado_ContratadaCod = P003G6_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P003G6_n490ContagemResultado_ContratadaCod[0];
            A1855ContagemResultado_PFCnc = P003G6_A1855ContagemResultado_PFCnc[0];
            n1855ContagemResultado_PFCnc = P003G6_n1855ContagemResultado_PFCnc[0];
            A1603ContagemResultado_CntCod = P003G6_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P003G6_n1603ContagemResultado_CntCod[0];
            A601ContagemResultado_Servico = P003G6_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P003G6_n601ContagemResultado_Servico[0];
            A1623ContagemResultado_CntSrvMmn = P003G6_A1623ContagemResultado_CntSrvMmn[0];
            n1623ContagemResultado_CntSrvMmn = P003G6_n1623ContagemResultado_CntSrvMmn[0];
            A801ContagemResultado_ServicoSigla = P003G6_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P003G6_n801ContagemResultado_ServicoSigla[0];
            A764ContagemResultado_ServicoGrupo = P003G6_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P003G6_n764ContagemResultado_ServicoGrupo[0];
            A515ContagemResultado_SistemaCoord = P003G6_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P003G6_n515ContagemResultado_SistemaCoord[0];
            A52Contratada_AreaTrabalhoCod = P003G6_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P003G6_n52Contratada_AreaTrabalhoCod[0];
            A803ContagemResultado_ContratadaSigla = P003G6_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P003G6_n803ContagemResultado_ContratadaSigla[0];
            A683ContagemResultado_PFLFMUltima = P003G6_A683ContagemResultado_PFLFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P003G6_A685ContagemResultado_PFLFSUltima[0];
            A682ContagemResultado_PFBFMUltima = P003G6_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = P003G6_A684ContagemResultado_PFBFSUltima[0];
            A584ContagemResultado_ContadorFM = P003G6_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P003G6_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P003G6_n566ContagemResultado_DataUltCnt[0];
            A531ContagemResultado_StatusUltCnt = P003G6_A531ContagemResultado_StatusUltCnt[0];
            GXt_boolean1 = A1802ContagemResultado_TemPndHmlg;
            new prc_tempndparahomologar(context ).execute( ref  A456ContagemResultado_Codigo, out  GXt_boolean1) ;
            A1802ContagemResultado_TemPndHmlg = GXt_boolean1;
            if ( ! ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
            {
               if ( ! ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV351ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
               {
                  if ( ! ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                  {
                     if ( ! ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV376ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                     {
                        if ( ! ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                        {
                           if ( ! ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV401ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                           {
                              if ( ! ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                              {
                                 if ( ! ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV426ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                                 {
                                    if ( ! ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV451ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                                    {
                                       if ( ! ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV451ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                                       {
                                          GXt_decimal2 = A574ContagemResultado_PFFinal;
                                          new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal2) ;
                                          A574ContagemResultado_PFFinal = GXt_decimal2;
                                          if ( (Convert.ToDecimal(0)==AV475ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal) || ( ( A574ContagemResultado_PFFinal >= AV475ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ) ) )
                                          {
                                             if ( (Convert.ToDecimal(0)==AV476ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to) || ( ( A574ContagemResultado_PFFinal <= AV476ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ) ) )
                                             {
                                                if ( StringUtil.StrCmp(A1623ContagemResultado_CntSrvMmn, "I") == 0 )
                                                {
                                                   if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
                                                   {
                                                      AV203PFInicial = (decimal)(AV203PFInicial+A1855ContagemResultado_PFCnc);
                                                   }
                                                   else
                                                   {
                                                      AV203PFInicial = (decimal)(AV203PFInicial+A574ContagemResultado_PFFinal);
                                                   }
                                                }
                                                else
                                                {
                                                   if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
                                                   {
                                                      AV98PFFinal = (decimal)(AV98PFFinal+A1855ContagemResultado_PFCnc);
                                                   }
                                                   else
                                                   {
                                                      AV98PFFinal = (decimal)(AV98PFFinal+A574ContagemResultado_PFFinal);
                                                   }
                                                }
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
         if ( ( AV98PFFinal > Convert.ToDecimal( 0 )) && ( AV98PFFinal < 0.001m ) )
         {
            AV201strPFFinal = StringUtil.Str( AV98PFFinal, 14, 4);
         }
         else
         {
            AV201strPFFinal = StringUtil.Str( AV98PFFinal, 14, 3);
         }
         if ( ( AV203PFInicial > Convert.ToDecimal( 0 )) && ( AV203PFInicial < 0.001m ) )
         {
            AV204strPFInicial = StringUtil.Str( AV203PFInicial, 14, 4);
         }
         else
         {
            AV204strPFInicial = StringUtil.Str( AV203PFInicial, 14, 3);
         }
      }

      protected void S151( )
      {
         /* 'PRINTDATALOTE' Routine */
         AV115x = (short)(AV115x+1);
         /* Execute user subroutine: 'PERIODO' */
         S161 ();
         if (returnInSub) return;
         if ( AV132Contratante_OSAutomatica )
         {
            /* Using cursor P003G7 */
            pr_default.execute(3, new Object[] {AV8ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(3) != 101) )
            {
               BRK3G8 = false;
               A597ContagemResultado_LoteAceiteCod = P003G7_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P003G7_n597ContagemResultado_LoteAceiteCod[0];
               A1452ContagemResultado_SS = P003G7_A1452ContagemResultado_SS[0];
               n1452ContagemResultado_SS = P003G7_n1452ContagemResultado_SS[0];
               A457ContagemResultado_Demanda = P003G7_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P003G7_n457ContagemResultado_Demanda[0];
               A489ContagemResultado_SistemaCod = P003G7_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P003G7_n489ContagemResultado_SistemaCod[0];
               A509ContagemrResultado_SistemaSigla = P003G7_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P003G7_n509ContagemrResultado_SistemaSigla[0];
               A515ContagemResultado_SistemaCoord = P003G7_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P003G7_n515ContagemResultado_SistemaCoord[0];
               A512ContagemResultado_ValorPF = P003G7_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = P003G7_n512ContagemResultado_ValorPF[0];
               A456ContagemResultado_Codigo = P003G7_A456ContagemResultado_Codigo[0];
               A509ContagemrResultado_SistemaSigla = P003G7_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P003G7_n509ContagemrResultado_SistemaSigla[0];
               A515ContagemResultado_SistemaCoord = P003G7_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P003G7_n515ContagemResultado_SistemaCoord[0];
               OV224ContagemResultado_SS = AV224ContagemResultado_SS;
               AV121Os_Header = StringUtil.Substring( AV188Lote_Numero, StringUtil.Len( AV188Lote_Numero)-3, 4) + "/" + "000" + StringUtil.Trim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0));
               AV108Sistema_Codigo = A489ContagemResultado_SistemaCod;
               AV102Sistema_Sigla = A509ContagemrResultado_SistemaSigla;
               AV101Sistema_Coordenacao = A515ContagemResultado_SistemaCoord;
               AV224ContagemResultado_SS = A1452ContagemResultado_SS;
               while ( (pr_default.getStatus(3) != 101) && ( P003G7_A597ContagemResultado_LoteAceiteCod[0] == A597ContagemResultado_LoteAceiteCod ) && ( P003G7_A1452ContagemResultado_SS[0] == A1452ContagemResultado_SS ) )
               {
                  BRK3G8 = false;
                  A457ContagemResultado_Demanda = P003G7_A457ContagemResultado_Demanda[0];
                  n457ContagemResultado_Demanda = P003G7_n457ContagemResultado_Demanda[0];
                  A456ContagemResultado_Codigo = P003G7_A456ContagemResultado_Codigo[0];
                  AV78i = (short)(AV78i+1);
                  BRK3G8 = true;
                  pr_default.readNext(3);
               }
               /* Execute user subroutine: 'CONTAGENSDOSISTEMALOTE' */
               S178 ();
               if ( returnInSub )
               {
                  pr_default.close(3);
                  returnInSub = true;
                  if (true) return;
               }
               AV206ValorIni = (decimal)(AV203PFInicial*A512ContagemResultado_ValorPF);
               AV105Valor = (decimal)(AV98PFFinal*A512ContagemResultado_ValorPF);
               AV207ValorTotal = (decimal)(AV105Valor+AV206ValorIni);
               if ( ( AV105Valor == Convert.ToDecimal( 0 )) )
               {
                  AV105Valor = NumberUtil.Round( AV98PFFinal*A512ContagemResultado_ValorPF, 2);
                  AV202strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV105Valor, 14, 2), " ", "");
               }
               else if ( AV105Valor < 0.001m )
               {
                  AV105Valor = NumberUtil.Round( AV98PFFinal*A512ContagemResultado_ValorPF, 4);
                  AV202strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV105Valor, 14, 4), " ", "");
               }
               else if ( AV105Valor < 0.01m )
               {
                  AV105Valor = NumberUtil.Round( AV98PFFinal*A512ContagemResultado_ValorPF, 3);
                  AV202strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV105Valor, 14, 3), " ", "");
               }
               else
               {
                  AV105Valor = NumberUtil.Round( AV98PFFinal*A512ContagemResultado_ValorPF, 2);
                  AV202strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV105Valor, 14, 2), " ", "");
               }
               if ( ( AV206ValorIni == Convert.ToDecimal( 0 )) )
               {
                  AV206ValorIni = NumberUtil.Round( AV203PFInicial*A512ContagemResultado_ValorPF, 2);
                  AV205strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV206ValorIni, 14, 2), " ", "");
               }
               else if ( AV206ValorIni < 0.001m )
               {
                  AV206ValorIni = NumberUtil.Round( AV203PFInicial*A512ContagemResultado_ValorPF, 4);
                  AV205strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV206ValorIni, 14, 4), " ", "");
               }
               else if ( AV206ValorIni < 0.01m )
               {
                  AV206ValorIni = NumberUtil.Round( AV203PFInicial*A512ContagemResultado_ValorPF, 3);
                  AV205strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV206ValorIni, 14, 3), " ", "");
               }
               else
               {
                  AV206ValorIni = NumberUtil.Round( AV203PFInicial*A512ContagemResultado_ValorPF, 2);
                  AV205strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV206ValorIni, 14, 2), " ", "");
               }
               if ( AV207ValorTotal < 0.001m )
               {
                  AV208strValorTotal = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV207ValorTotal, 14, 4), " ", "");
               }
               else if ( AV207ValorTotal < 0.01m )
               {
                  AV208strValorTotal = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV207ValorTotal, 14, 3), " ", "");
               }
               else
               {
                  AV208strValorTotal = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV207ValorTotal, 14, 2), " ", "");
               }
               if ( AV203PFInicial + AV98PFFinal < 0.001m )
               {
                  AV210strPFTotal = StringUtil.Str( AV203PFInicial+AV98PFFinal, 14, 4);
               }
               else
               {
                  AV210strPFTotal = StringUtil.Str( AV203PFInicial+AV98PFFinal, 14, 3);
               }
               AV82ImprimindoOS = true;
               H3G0( false, 390) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("1.  Identifica��o da Empresa Contratada ", 17, Gx_line+13, 359, Gx_line+32, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("2. Informa��es sobre os Servi�os a serem Realizados", 17, Gx_line+186, 442, Gx_line+205, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV102Sistema_Sigla, "@!")), 92, Gx_line+204, 275, Gx_line+221, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV60DataInicio, "99/99/99"), 258, Gx_line+313, 328, Gx_line+330, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV59DataFim, "99/99/99"), 358, Gx_line+313, 428, Gx_line+330, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54Contratada_PessoaNom, "@!")), 159, Gx_line+40, 889, Gx_line+57, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV53Contratada_PessoaCNPJ, "")), 76, Gx_line+61, 186, Gx_line+78, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV93Pessoa_Endereco, "")), 17, Gx_line+100, 747, Gx_line+117, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV96Pessoa_Telefone, "")), 367, Gx_line+150, 477, Gx_line+167, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV92Pessoa_CEP, "")), 84, Gx_line+150, 158, Gx_line+167, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV94Pessoa_Fax, "")), 576, Gx_line+150, 686, Gx_line+167, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV97Pessoa_UF, "@!")), 634, Gx_line+118, 660, Gx_line+135, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV85Municipio_Nome, "@!")), 84, Gx_line+122, 450, Gx_line+139, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV95Pessoa_IE, "99.999.999/999-99999")), 551, Gx_line+68, 698, Gx_line+85, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV106Volume, "")), 17, Gx_line+363, 495, Gx_line+380, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV79Identificacao1, "")), 17, Gx_line+254, 747, Gx_line+271, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV80Identificacao2, "")), 17, Gx_line+271, 747, Gx_line+288, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV81Identificacao3, "")), 17, Gx_line+287, 747, Gx_line+304, 0+256, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("PF", 619, Gx_line+362, 636, Gx_line+381, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("a", 350, Gx_line+313, 358, Gx_line+332, 0+256, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Nome da Empresa:", 17, Gx_line+40, 146, Gx_line+58, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Inscri��o Estadual:", 409, Gx_line+68, 538, Gx_line+86, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("CNPJ:", 17, Gx_line+61, 63, Gx_line+79, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Endere�o:", 16, Gx_line+82, 93, Gx_line+100, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Cidade:", 17, Gx_line+122, 77, Gx_line+140, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("UF:", 592, Gx_line+118, 621, Gx_line+136, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("CEP:", 17, Gx_line+150, 68, Gx_line+168, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Telefone:", 276, Gx_line+150, 355, Gx_line+168, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Fax:", 517, Gx_line+150, 566, Gx_line+168, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Sistema:", 17, Gx_line+204, 86, Gx_line+222, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Identifica��o do Servi�o:", 17, Gx_line+237, 184, Gx_line+256, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Volume de Servi�o:", 17, Gx_line+347, 150, Gx_line+366, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Per�odo de Execu��o do Servi�o:", 17, Gx_line+313, 234, Gx_line+332, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV210strPFTotal, "")), 494, Gx_line+361, 612, Gx_line+381, 2+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+162, 781, Gx_line+177, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+0, 781, Gx_line+15, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+390);
               H3G0( false, 354) ;
               getPrinter().GxDrawLine(275, Gx_line+317, 558, Gx_line+317, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("5. Solicitante", 17, Gx_line+150, 192, Gx_line+169, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("4. Gestor da Ordem de Servi�o", 17, Gx_line+100, 308, Gx_line+119, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("3. Local de Execu��o do Servi�o", 17, Gx_line+50, 325, Gx_line+69, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Relat�rios T�cnicos de Contagem de Pontos de Fun��o", 17, Gx_line+17, 326, Gx_line+35, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("Minist�rio da Sa�de", 287, Gx_line+67, 420, Gx_line+85, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("DATASUS - Minist�rio da Sa�de", 100, Gx_line+167, 300, Gx_line+185, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Cristiane Sousa de Oliveira", 17, Gx_line+117, 192, Gx_line+135, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV101Sistema_Coordenacao, "@!")), 473, Gx_line+202, 782, Gx_line+219, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Gestor Contratual", 328, Gx_line+317, 505, Gx_line+335, 1, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Bras�lia, ___ de ___________ de ____.", 275, Gx_line+241, 517, Gx_line+260, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Unidade:", 17, Gx_line+167, 87, Gx_line+185, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Identifica��o do Local de Execu��o:", 17, Gx_line+67, 275, Gx_line+85, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Produtos a Serem Entregues:", 17, Gx_line+0, 217, Gx_line+19, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Respons�vel pela solicita��o: (�rg�o, Secretaria, Coordena��o):", 17, Gx_line+200, 467, Gx_line+219, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+29, 781, Gx_line+46, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+79, 781, Gx_line+96, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+129, 781, Gx_line+146, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+354);
               H3G0( false, 306) ;
               getPrinter().GxDrawLine(19, Gx_line+188, 782, Gx_line+188, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(511, Gx_line+52, 511, Gx_line+235, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(657, Gx_line+51, 657, Gx_line+235, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(19, Gx_line+52, 19, Gx_line+188, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(187, Gx_line+51, 187, Gx_line+189, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(782, Gx_line+51, 782, Gx_line+235, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(512, Gx_line+235, 782, Gx_line+235, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(19, Gx_line+128, 782, Gx_line+128, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(20, Gx_line+80, 782, Gx_line+80, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(19, Gx_line+51, 782, Gx_line+51, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("7. Ciente da Contratada", 17, Gx_line+260, 234, Gx_line+279, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("6. Custo da Ordem de Servi�o", 17, Gx_line+7, 267, Gx_line+26, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Contagem de PF Final", 27, Gx_line+145, 169, Gx_line+163, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("(Parcela 2)", 61, Gx_line+162, 128, Gx_line+180, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("100%", 569, Gx_line+150, 602, Gx_line+168, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("50%", 569, Gx_line+96, 594, Gx_line+114, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("(Parcela 1)", 69, Gx_line+102, 136, Gx_line+120, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Contagem de PF Inicial", 27, Gx_line+85, 177, Gx_line+103, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("O quadro a seguir descreve o custo dos servi�os a serem executados:", 17, Gx_line+31, 413, Gx_line+49, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV202strValor, "")), 675, Gx_line+150, 767, Gx_line+167, 2, 0, 0, 1) ;
               getPrinter().GxDrawText("(*)", 392, Gx_line+96, 417, Gx_line+114, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("(*) Este valor j� esta l�quido conforme % ao lado.", 17, Gx_line+217, 325, Gx_line+235, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV205strValorIni, "")), 671, Gx_line+96, 767, Gx_line+113, 2, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("VALOR", 694, Gx_line+57, 752, Gx_line+75, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("% FASE", 550, Gx_line+57, 617, Gx_line+75, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("QUANTIDADE DE PONTOS DE FUN��O", 200, Gx_line+57, 475, Gx_line+75, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("SERVI�O", 58, Gx_line+57, 125, Gx_line+75, 1, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Declaramos nossa ci�ncia e concord�ncia com rela��o aos servi�os discriminados  no  item 02.", 17, Gx_line+277, 700, Gx_line+296, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV201strPFFinal, "")), 273, Gx_line+150, 391, Gx_line+170, 1, 0, 0, 1) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV208strValorTotal, "")), 675, Gx_line+203, 768, Gx_line+223, 2, 0, 0, 1) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV204strPFInicial, "")), 273, Gx_line+95, 391, Gx_line+115, 1, 0, 0, 1) ;
               getPrinter().GxAttris("Calibri", 12, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("TOTAL", 542, Gx_line+203, 642, Gx_line+223, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+244, 781, Gx_line+259, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+306);
               H3G0( false, 162) ;
               getPrinter().GxDrawLine(258, Gx_line+74, 541, Gx_line+74, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Respons�vel Legal/T�cnico", 308, Gx_line+74, 491, Gx_line+92, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("Pedro Augusto de Ara�jo, CFPS", 292, Gx_line+91, 509, Gx_line+109, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("EFIC�CIA ORGANIZA��O LTDA", 292, Gx_line+107, 509, Gx_line+125, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("CNPJ: 00.665.620/0001-40", 308, Gx_line+124, 491, Gx_line+142, 1, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Bras�lia, ___ de ___________ de ____.", 267, Gx_line+17, 541, Gx_line+36, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+141, 781, Gx_line+158, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+162);
               H3G0( false, 120) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("8. Cancelamento da Ordem de Servi�o", 17, Gx_line+0, 317, Gx_line+19, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Motivo do Cancelamento:", 17, Gx_line+17, 192, Gx_line+35, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Assinatura do Gestor da Ordem de Servi�o:", 150, Gx_line+50, 433, Gx_line+68, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Assinatura do Gestor de Contrato:", 492, Gx_line+50, 725, Gx_line+68, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Data:", 25, Gx_line+50, 67, Gx_line+68, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Ci�ncia do Preposto da Contratada:", 400, Gx_line+100, 642, Gx_line+118, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Assinatura do Coordenador Geral Respons�vel:", 25, Gx_line+100, 342, Gx_line+118, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+120);
               /* Eject command */
               Gx_OldLine = Gx_line;
               Gx_line = (int)(P_lines+1);
               if ( ! BRK3G8 )
               {
                  BRK3G8 = true;
                  pr_default.readNext(3);
               }
            }
            pr_default.close(3);
         }
         else
         {
            /* Using cursor P003G8 */
            pr_default.execute(4, new Object[] {AV8ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(4) != 101) )
            {
               BRK3G10 = false;
               A597ContagemResultado_LoteAceiteCod = P003G8_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P003G8_n597ContagemResultado_LoteAceiteCod[0];
               A1452ContagemResultado_SS = P003G8_A1452ContagemResultado_SS[0];
               n1452ContagemResultado_SS = P003G8_n1452ContagemResultado_SS[0];
               A457ContagemResultado_Demanda = P003G8_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P003G8_n457ContagemResultado_Demanda[0];
               A489ContagemResultado_SistemaCod = P003G8_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P003G8_n489ContagemResultado_SistemaCod[0];
               A509ContagemrResultado_SistemaSigla = P003G8_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P003G8_n509ContagemrResultado_SistemaSigla[0];
               A515ContagemResultado_SistemaCoord = P003G8_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P003G8_n515ContagemResultado_SistemaCoord[0];
               A512ContagemResultado_ValorPF = P003G8_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = P003G8_n512ContagemResultado_ValorPF[0];
               A456ContagemResultado_Codigo = P003G8_A456ContagemResultado_Codigo[0];
               A509ContagemrResultado_SistemaSigla = P003G8_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P003G8_n509ContagemrResultado_SistemaSigla[0];
               A515ContagemResultado_SistemaCoord = P003G8_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P003G8_n515ContagemResultado_SistemaCoord[0];
               OV224ContagemResultado_SS = AV224ContagemResultado_SS;
               AV121Os_Header = StringUtil.Substring( AV188Lote_Numero, StringUtil.Len( AV188Lote_Numero)-3, 4) + "/" + "000" + StringUtil.Trim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0));
               AV108Sistema_Codigo = A489ContagemResultado_SistemaCod;
               AV102Sistema_Sigla = A509ContagemrResultado_SistemaSigla;
               AV101Sistema_Coordenacao = A515ContagemResultado_SistemaCoord;
               AV224ContagemResultado_SS = A1452ContagemResultado_SS;
               while ( (pr_default.getStatus(4) != 101) && ( P003G8_A597ContagemResultado_LoteAceiteCod[0] == A597ContagemResultado_LoteAceiteCod ) && ( P003G8_A1452ContagemResultado_SS[0] == A1452ContagemResultado_SS ) )
               {
                  BRK3G10 = false;
                  A457ContagemResultado_Demanda = P003G8_A457ContagemResultado_Demanda[0];
                  n457ContagemResultado_Demanda = P003G8_n457ContagemResultado_Demanda[0];
                  A456ContagemResultado_Codigo = P003G8_A456ContagemResultado_Codigo[0];
                  AV78i = (short)(AV78i+1);
                  BRK3G10 = true;
                  pr_default.readNext(4);
               }
               /* Execute user subroutine: 'CONTAGENSDOSISTEMALOTE' */
               S178 ();
               if ( returnInSub )
               {
                  pr_default.close(4);
                  returnInSub = true;
                  if (true) return;
               }
               AV206ValorIni = (decimal)(AV203PFInicial*A512ContagemResultado_ValorPF);
               AV105Valor = (decimal)(AV98PFFinal*A512ContagemResultado_ValorPF);
               AV207ValorTotal = (decimal)(AV105Valor+AV206ValorIni);
               if ( ( AV105Valor == Convert.ToDecimal( 0 )) )
               {
                  AV105Valor = NumberUtil.Round( AV98PFFinal*A512ContagemResultado_ValorPF, 2);
                  AV202strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV105Valor, 14, 2), " ", "");
               }
               else if ( AV105Valor < 0.001m )
               {
                  AV105Valor = NumberUtil.Round( AV98PFFinal*A512ContagemResultado_ValorPF, 4);
                  AV202strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV105Valor, 14, 4), " ", "");
               }
               else if ( AV105Valor < 0.01m )
               {
                  AV105Valor = NumberUtil.Round( AV98PFFinal*A512ContagemResultado_ValorPF, 3);
                  AV202strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV105Valor, 14, 3), " ", "");
               }
               else
               {
                  AV105Valor = NumberUtil.Round( AV98PFFinal*A512ContagemResultado_ValorPF, 2);
                  AV202strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV105Valor, 14, 2), " ", "");
               }
               if ( ( AV206ValorIni == Convert.ToDecimal( 0 )) )
               {
                  AV206ValorIni = NumberUtil.Round( AV203PFInicial*A512ContagemResultado_ValorPF, 2);
                  AV205strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV206ValorIni, 14, 2), " ", "");
               }
               else if ( AV206ValorIni < 0.001m )
               {
                  AV206ValorIni = NumberUtil.Round( AV203PFInicial*A512ContagemResultado_ValorPF, 4);
                  AV205strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV206ValorIni, 14, 4), " ", "");
               }
               else if ( AV206ValorIni < 0.01m )
               {
                  AV206ValorIni = NumberUtil.Round( AV203PFInicial*A512ContagemResultado_ValorPF, 3);
                  AV205strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV206ValorIni, 14, 3), " ", "");
               }
               else
               {
                  AV206ValorIni = NumberUtil.Round( AV203PFInicial*A512ContagemResultado_ValorPF, 2);
                  AV205strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV206ValorIni, 14, 2), " ", "");
               }
               if ( AV207ValorTotal < 0.001m )
               {
                  AV208strValorTotal = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV207ValorTotal, 14, 4), " ", "");
               }
               else if ( AV207ValorTotal < 0.01m )
               {
                  AV208strValorTotal = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV207ValorTotal, 14, 3), " ", "");
               }
               else
               {
                  AV208strValorTotal = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV207ValorTotal, 14, 2), " ", "");
               }
               if ( AV203PFInicial + AV98PFFinal < 0.001m )
               {
                  AV210strPFTotal = StringUtil.Str( AV203PFInicial+AV98PFFinal, 14, 4);
               }
               else
               {
                  AV210strPFTotal = StringUtil.Str( AV203PFInicial+AV98PFFinal, 14, 3);
               }
               AV82ImprimindoOS = true;
               H3G0( false, 390) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("1.  Identifica��o da Empresa Contratada ", 17, Gx_line+13, 359, Gx_line+32, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("2. Informa��es sobre os Servi�os a serem Realizados", 17, Gx_line+186, 442, Gx_line+205, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV102Sistema_Sigla, "@!")), 92, Gx_line+204, 275, Gx_line+221, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV60DataInicio, "99/99/99"), 258, Gx_line+313, 328, Gx_line+330, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV59DataFim, "99/99/99"), 358, Gx_line+313, 428, Gx_line+330, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54Contratada_PessoaNom, "@!")), 159, Gx_line+40, 889, Gx_line+57, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV53Contratada_PessoaCNPJ, "")), 76, Gx_line+61, 186, Gx_line+78, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV93Pessoa_Endereco, "")), 17, Gx_line+100, 747, Gx_line+117, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV96Pessoa_Telefone, "")), 367, Gx_line+150, 477, Gx_line+167, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV92Pessoa_CEP, "")), 84, Gx_line+150, 158, Gx_line+167, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV94Pessoa_Fax, "")), 576, Gx_line+150, 686, Gx_line+167, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV97Pessoa_UF, "@!")), 634, Gx_line+118, 660, Gx_line+135, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV85Municipio_Nome, "@!")), 84, Gx_line+122, 450, Gx_line+139, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV95Pessoa_IE, "99.999.999/999-99999")), 551, Gx_line+68, 698, Gx_line+85, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV106Volume, "")), 17, Gx_line+363, 495, Gx_line+380, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV79Identificacao1, "")), 17, Gx_line+254, 747, Gx_line+271, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV80Identificacao2, "")), 17, Gx_line+271, 747, Gx_line+288, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV81Identificacao3, "")), 17, Gx_line+287, 747, Gx_line+304, 0+256, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("PF", 619, Gx_line+362, 636, Gx_line+381, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("a", 350, Gx_line+313, 358, Gx_line+332, 0+256, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Nome da Empresa:", 17, Gx_line+40, 146, Gx_line+58, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Inscri��o Estadual:", 409, Gx_line+68, 538, Gx_line+86, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("CNPJ:", 17, Gx_line+61, 63, Gx_line+79, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Endere�o:", 16, Gx_line+82, 93, Gx_line+100, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Cidade:", 17, Gx_line+122, 77, Gx_line+140, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("UF:", 592, Gx_line+118, 621, Gx_line+136, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("CEP:", 17, Gx_line+150, 68, Gx_line+168, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Telefone:", 276, Gx_line+150, 355, Gx_line+168, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Fax:", 517, Gx_line+150, 566, Gx_line+168, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Sistema:", 17, Gx_line+204, 86, Gx_line+222, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Identifica��o do Servi�o:", 17, Gx_line+237, 184, Gx_line+256, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Volume de Servi�o:", 17, Gx_line+347, 150, Gx_line+366, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Per�odo de Execu��o do Servi�o:", 17, Gx_line+313, 234, Gx_line+332, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV210strPFTotal, "")), 494, Gx_line+361, 612, Gx_line+381, 2+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+162, 781, Gx_line+177, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+0, 781, Gx_line+15, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+390);
               H3G0( false, 354) ;
               getPrinter().GxDrawLine(275, Gx_line+317, 558, Gx_line+317, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("5. Solicitante", 17, Gx_line+150, 192, Gx_line+169, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("4. Gestor da Ordem de Servi�o", 17, Gx_line+100, 308, Gx_line+119, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("3. Local de Execu��o do Servi�o", 17, Gx_line+50, 325, Gx_line+69, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Relat�rios T�cnicos de Contagem de Pontos de Fun��o", 17, Gx_line+17, 326, Gx_line+35, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("Minist�rio da Sa�de", 287, Gx_line+67, 420, Gx_line+85, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("DATASUS - Minist�rio da Sa�de", 100, Gx_line+167, 300, Gx_line+185, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Cristiane Sousa de Oliveira", 17, Gx_line+117, 192, Gx_line+135, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV101Sistema_Coordenacao, "@!")), 473, Gx_line+202, 782, Gx_line+219, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Gestor Contratual", 328, Gx_line+317, 505, Gx_line+335, 1, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Bras�lia, ___ de ___________ de ____.", 275, Gx_line+241, 517, Gx_line+260, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Unidade:", 17, Gx_line+167, 87, Gx_line+185, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Identifica��o do Local de Execu��o:", 17, Gx_line+67, 275, Gx_line+85, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Produtos a Serem Entregues:", 17, Gx_line+0, 217, Gx_line+19, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Respons�vel pela solicita��o: (�rg�o, Secretaria, Coordena��o):", 17, Gx_line+200, 467, Gx_line+219, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+29, 781, Gx_line+46, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+79, 781, Gx_line+96, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+129, 781, Gx_line+146, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+354);
               H3G0( false, 306) ;
               getPrinter().GxDrawLine(19, Gx_line+188, 782, Gx_line+188, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(511, Gx_line+52, 511, Gx_line+235, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(657, Gx_line+51, 657, Gx_line+235, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(19, Gx_line+52, 19, Gx_line+188, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(187, Gx_line+51, 187, Gx_line+189, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(782, Gx_line+51, 782, Gx_line+235, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(512, Gx_line+235, 782, Gx_line+235, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(19, Gx_line+128, 782, Gx_line+128, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(20, Gx_line+80, 782, Gx_line+80, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(19, Gx_line+51, 782, Gx_line+51, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("7. Ciente da Contratada", 17, Gx_line+260, 234, Gx_line+279, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("6. Custo da Ordem de Servi�o", 17, Gx_line+7, 267, Gx_line+26, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Contagem de PF Final", 27, Gx_line+145, 169, Gx_line+163, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("(Parcela 2)", 61, Gx_line+162, 128, Gx_line+180, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("100%", 569, Gx_line+150, 602, Gx_line+168, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("50%", 569, Gx_line+96, 594, Gx_line+114, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("(Parcela 1)", 69, Gx_line+102, 136, Gx_line+120, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Contagem de PF Inicial", 27, Gx_line+85, 177, Gx_line+103, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("O quadro a seguir descreve o custo dos servi�os a serem executados:", 17, Gx_line+31, 413, Gx_line+49, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV202strValor, "")), 675, Gx_line+150, 767, Gx_line+167, 2, 0, 0, 1) ;
               getPrinter().GxDrawText("(*)", 392, Gx_line+96, 417, Gx_line+114, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("(*) Este valor j� esta l�quido conforme % ao lado.", 17, Gx_line+217, 325, Gx_line+235, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV205strValorIni, "")), 671, Gx_line+96, 767, Gx_line+113, 2, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("VALOR", 694, Gx_line+57, 752, Gx_line+75, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("% FASE", 550, Gx_line+57, 617, Gx_line+75, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("QUANTIDADE DE PONTOS DE FUN��O", 200, Gx_line+57, 475, Gx_line+75, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("SERVI�O", 58, Gx_line+57, 125, Gx_line+75, 1, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Declaramos nossa ci�ncia e concord�ncia com rela��o aos servi�os discriminados  no  item 02.", 17, Gx_line+277, 700, Gx_line+296, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV201strPFFinal, "")), 273, Gx_line+150, 391, Gx_line+170, 1, 0, 0, 1) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV208strValorTotal, "")), 675, Gx_line+203, 768, Gx_line+223, 2, 0, 0, 1) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV204strPFInicial, "")), 273, Gx_line+95, 391, Gx_line+115, 1, 0, 0, 1) ;
               getPrinter().GxAttris("Calibri", 12, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("TOTAL", 542, Gx_line+203, 642, Gx_line+223, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+244, 781, Gx_line+259, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+306);
               H3G0( false, 162) ;
               getPrinter().GxDrawLine(258, Gx_line+74, 541, Gx_line+74, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Respons�vel Legal/T�cnico", 308, Gx_line+74, 491, Gx_line+92, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("Pedro Augusto de Ara�jo, CFPS", 292, Gx_line+91, 509, Gx_line+109, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("EFIC�CIA ORGANIZA��O LTDA", 292, Gx_line+107, 509, Gx_line+125, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("CNPJ: 00.665.620/0001-40", 308, Gx_line+124, 491, Gx_line+142, 1, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Bras�lia, ___ de ___________ de ____.", 267, Gx_line+17, 541, Gx_line+36, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+141, 781, Gx_line+158, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+162);
               H3G0( false, 120) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("8. Cancelamento da Ordem de Servi�o", 17, Gx_line+0, 317, Gx_line+19, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Motivo do Cancelamento:", 17, Gx_line+17, 192, Gx_line+35, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Assinatura do Gestor da Ordem de Servi�o:", 150, Gx_line+50, 433, Gx_line+68, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Assinatura do Gestor de Contrato:", 492, Gx_line+50, 725, Gx_line+68, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Data:", 25, Gx_line+50, 67, Gx_line+68, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Ci�ncia do Preposto da Contratada:", 400, Gx_line+100, 642, Gx_line+118, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Assinatura do Coordenador Geral Respons�vel:", 25, Gx_line+100, 342, Gx_line+118, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+120);
               /* Eject command */
               Gx_OldLine = Gx_line;
               Gx_line = (int)(P_lines+1);
               if ( ! BRK3G10 )
               {
                  BRK3G10 = true;
                  pr_default.readNext(4);
               }
            }
            pr_default.close(4);
         }
      }

      protected void S161( )
      {
         /* 'PERIODO' Routine */
         /* Using cursor P003G11 */
         pr_default.execute(5, new Object[] {AV8ContagemResultado_LoteAceite});
         if ( (pr_default.getStatus(5) != 101) )
         {
            A40000GXC1 = P003G11_A40000GXC1[0];
            A40001GXC2 = P003G11_A40001GXC2[0];
         }
         else
         {
            A40000GXC1 = DateTime.MinValue;
            A40001GXC2 = DateTime.MinValue;
         }
         pr_default.close(5);
         if ( StringUtil.StrCmp(AV188Lote_Numero, "2012016") == 0 )
         {
            AV60DataInicio = context.localUtil.CToD( "04/01/2016", 2);
            AV59DataFim = context.localUtil.CToD( "08/01/2016", 2);
         }
         else
         {
            AV60DataInicio = A40000GXC1;
            AV59DataFim = A40001GXC2;
         }
      }

      protected void S178( )
      {
         /* 'CONTAGENSDOSISTEMALOTE' Routine */
         AV203PFInicial = 0;
         AV98PFFinal = 0;
         /* Using cursor P003G12 */
         pr_default.execute(6, new Object[] {AV8ContagemResultado_LoteAceite, AV224ContagemResultado_SS});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A489ContagemResultado_SistemaCod = P003G12_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P003G12_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P003G12_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P003G12_n1553ContagemResultado_CntSrvCod[0];
            A597ContagemResultado_LoteAceiteCod = P003G12_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P003G12_n597ContagemResultado_LoteAceiteCod[0];
            A1452ContagemResultado_SS = P003G12_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = P003G12_n1452ContagemResultado_SS[0];
            A1623ContagemResultado_CntSrvMmn = P003G12_A1623ContagemResultado_CntSrvMmn[0];
            n1623ContagemResultado_CntSrvMmn = P003G12_n1623ContagemResultado_CntSrvMmn[0];
            A484ContagemResultado_StatusDmn = P003G12_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P003G12_n484ContagemResultado_StatusDmn[0];
            A1855ContagemResultado_PFCnc = P003G12_A1855ContagemResultado_PFCnc[0];
            n1855ContagemResultado_PFCnc = P003G12_n1855ContagemResultado_PFCnc[0];
            A509ContagemrResultado_SistemaSigla = P003G12_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P003G12_n509ContagemrResultado_SistemaSigla[0];
            A456ContagemResultado_Codigo = P003G12_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = P003G12_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P003G12_n509ContagemrResultado_SistemaSigla[0];
            A1623ContagemResultado_CntSrvMmn = P003G12_A1623ContagemResultado_CntSrvMmn[0];
            n1623ContagemResultado_CntSrvMmn = P003G12_n1623ContagemResultado_CntSrvMmn[0];
            GXt_decimal2 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal2) ;
            A574ContagemResultado_PFFinal = GXt_decimal2;
            if ( StringUtil.StrCmp(A1623ContagemResultado_CntSrvMmn, "I") == 0 )
            {
               if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
               {
                  AV203PFInicial = (decimal)(AV203PFInicial+A1855ContagemResultado_PFCnc);
               }
               else
               {
                  AV203PFInicial = (decimal)(AV203PFInicial+A574ContagemResultado_PFFinal);
               }
            }
            else
            {
               if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
               {
                  AV98PFFinal = (decimal)(AV98PFFinal+A1855ContagemResultado_PFCnc);
               }
               else
               {
                  AV98PFFinal = (decimal)(AV98PFFinal+A574ContagemResultado_PFFinal);
               }
            }
            pr_default.readNext(6);
         }
         pr_default.close(6);
         if ( ( AV98PFFinal > Convert.ToDecimal( 0 )) && ( AV98PFFinal < 0.001m ) )
         {
            AV201strPFFinal = StringUtil.Str( AV98PFFinal, 14, 4);
         }
         else
         {
            AV201strPFFinal = StringUtil.Str( AV98PFFinal, 14, 3);
         }
         if ( ( AV203PFInicial > Convert.ToDecimal( 0 )) && ( AV203PFInicial < 0.001m ) )
         {
            AV204strPFInicial = StringUtil.Str( AV203PFInicial, 14, 4);
         }
         else
         {
            AV204strPFInicial = StringUtil.Str( AV203PFInicial, 14, 3);
         }
      }

      protected void S135( )
      {
         /* 'DADOSCONTRATO' Routine */
         pr_default.dynParam(7, new Object[]{ new Object[]{
                                              AV8ContagemResultado_LoteAceite ,
                                              A456ContagemResultado_Codigo ,
                                              AV230Codigo ,
                                              A597ContagemResultado_LoteAceiteCod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P003G15 */
         pr_default.execute(7, new Object[] {AV230Codigo, AV8ContagemResultado_LoteAceite});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P003G15_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P003G15_n1553ContagemResultado_CntSrvCod[0];
            A1603ContagemResultado_CntCod = P003G15_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P003G15_n1603ContagemResultado_CntCod[0];
            A597ContagemResultado_LoteAceiteCod = P003G15_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P003G15_n597ContagemResultado_LoteAceiteCod[0];
            A456ContagemResultado_Codigo = P003G15_A456ContagemResultado_Codigo[0];
            A1612ContagemResultado_CntNum = P003G15_A1612ContagemResultado_CntNum[0];
            n1612ContagemResultado_CntNum = P003G15_n1612ContagemResultado_CntNum[0];
            A1624ContagemResultado_DatVgnInc = P003G15_A1624ContagemResultado_DatVgnInc[0];
            n1624ContagemResultado_DatVgnInc = P003G15_n1624ContagemResultado_DatVgnInc[0];
            A490ContagemResultado_ContratadaCod = P003G15_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P003G15_n490ContagemResultado_ContratadaCod[0];
            A40002GXC3 = P003G15_A40002GXC3[0];
            n40002GXC3 = P003G15_n40002GXC3[0];
            A1603ContagemResultado_CntCod = P003G15_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P003G15_n1603ContagemResultado_CntCod[0];
            A1612ContagemResultado_CntNum = P003G15_A1612ContagemResultado_CntNum[0];
            n1612ContagemResultado_CntNum = P003G15_n1612ContagemResultado_CntNum[0];
            A1624ContagemResultado_DatVgnInc = P003G15_A1624ContagemResultado_DatVgnInc[0];
            n1624ContagemResultado_DatVgnInc = P003G15_n1624ContagemResultado_DatVgnInc[0];
            A40002GXC3 = P003G15_A40002GXC3[0];
            n40002GXC3 = P003G15_n40002GXC3[0];
            AV56Contrato_Numero = A1612ContagemResultado_CntNum;
            AV144Contrato_DataInicioTA = A40002GXC3;
            if ( (DateTime.MinValue==AV144Contrato_DataInicioTA) )
            {
               AV55Contrato_DataVigenciaInicio = A1624ContagemResultado_DatVgnInc;
            }
            AV231Contratada_Codigo = A490ContagemResultado_ContratadaCod;
            /* Execute user subroutine: 'DADOSCONTRTADA' */
            S1813 ();
            if ( returnInSub )
            {
               pr_default.close(7);
               returnInSub = true;
               if (true) return;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(7);
         }
         pr_default.close(7);
      }

      protected void S1813( )
      {
         /* 'DADOSCONTRTADA' Routine */
         /* Using cursor P003G16 */
         pr_default.execute(8, new Object[] {AV231Contratada_Codigo});
         while ( (pr_default.getStatus(8) != 101) )
         {
            A40Contratada_PessoaCod = P003G16_A40Contratada_PessoaCod[0];
            A503Pessoa_MunicipioCod = P003G16_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = P003G16_n503Pessoa_MunicipioCod[0];
            A52Contratada_AreaTrabalhoCod = P003G16_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P003G16_n52Contratada_AreaTrabalhoCod[0];
            A29Contratante_Codigo = P003G16_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P003G16_n29Contratante_Codigo[0];
            A39Contratada_Codigo = P003G16_A39Contratada_Codigo[0];
            A41Contratada_PessoaNom = P003G16_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P003G16_n41Contratada_PessoaNom[0];
            A524Contratada_OS = P003G16_A524Contratada_OS[0];
            n524Contratada_OS = P003G16_n524Contratada_OS[0];
            A42Contratada_PessoaCNPJ = P003G16_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P003G16_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = P003G16_A518Pessoa_IE[0];
            n518Pessoa_IE = P003G16_n518Pessoa_IE[0];
            A519Pessoa_Endereco = P003G16_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P003G16_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = P003G16_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P003G16_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = P003G16_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P003G16_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = P003G16_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P003G16_n523Pessoa_Fax[0];
            A520Pessoa_UF = P003G16_A520Pessoa_UF[0];
            n520Pessoa_UF = P003G16_n520Pessoa_UF[0];
            A26Municipio_Nome = P003G16_A26Municipio_Nome[0];
            n26Municipio_Nome = P003G16_n26Municipio_Nome[0];
            A593Contratante_OSAutomatica = P003G16_A593Contratante_OSAutomatica[0];
            A503Pessoa_MunicipioCod = P003G16_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = P003G16_n503Pessoa_MunicipioCod[0];
            A41Contratada_PessoaNom = P003G16_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P003G16_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P003G16_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P003G16_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = P003G16_A518Pessoa_IE[0];
            n518Pessoa_IE = P003G16_n518Pessoa_IE[0];
            A519Pessoa_Endereco = P003G16_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P003G16_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = P003G16_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P003G16_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = P003G16_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P003G16_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = P003G16_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P003G16_n523Pessoa_Fax[0];
            A520Pessoa_UF = P003G16_A520Pessoa_UF[0];
            n520Pessoa_UF = P003G16_n520Pessoa_UF[0];
            A26Municipio_Nome = P003G16_A26Municipio_Nome[0];
            n26Municipio_Nome = P003G16_n26Municipio_Nome[0];
            A29Contratante_Codigo = P003G16_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P003G16_n29Contratante_Codigo[0];
            A593Contratante_OSAutomatica = P003G16_A593Contratante_OSAutomatica[0];
            AV54Contratada_PessoaNom = A41Contratada_PessoaNom;
            AV9Contratada_OS = A524Contratada_OS;
            if ( StringUtil.StringSearch( A42Contratada_PessoaCNPJ, ".", 1) == 0 )
            {
               AV53Contratada_PessoaCNPJ = StringUtil.Substring( A42Contratada_PessoaCNPJ, 1, 2) + "." + StringUtil.Substring( A42Contratada_PessoaCNPJ, 3, 3) + "." + StringUtil.Substring( A42Contratada_PessoaCNPJ, 6, 3) + "/" + StringUtil.Substring( A42Contratada_PessoaCNPJ, 9, 4) + "-" + StringUtil.Substring( A42Contratada_PessoaCNPJ, 13, 2);
            }
            else
            {
               AV53Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
            }
            if ( StringUtil.StringSearch( A518Pessoa_IE, ".", 1) == 0 )
            {
               AV95Pessoa_IE = StringUtil.Substring( A518Pessoa_IE, 1, 2) + "." + StringUtil.Substring( A518Pessoa_IE, 3, 3) + "." + StringUtil.Substring( A518Pessoa_IE, 6, 3) + "/" + StringUtil.Substring( A518Pessoa_IE, 9, 3) + "-" + StringUtil.Substring( A518Pessoa_IE, 12, 2);
            }
            else
            {
               AV95Pessoa_IE = A518Pessoa_IE;
            }
            AV93Pessoa_Endereco = A519Pessoa_Endereco;
            AV92Pessoa_CEP = A521Pessoa_CEP;
            AV96Pessoa_Telefone = A522Pessoa_Telefone;
            AV94Pessoa_Fax = A523Pessoa_Fax;
            AV97Pessoa_UF = A520Pessoa_UF;
            AV85Municipio_Nome = A26Municipio_Nome;
            AV132Contratante_OSAutomatica = A593Contratante_OSAutomatica;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(8);
      }

      protected void H3G0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("P�gina", 647, Gx_line+4, 691, Gx_line+31, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV91Pagina), "ZZZ9")), 696, Gx_line+4, 729, Gx_line+33, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 730, Gx_line+4, 747, Gx_line+31, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV89Os, "")), 41, Gx_line+2, 166, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("SS", 17, Gx_line+2, 33, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("2", 755, Gx_line+4, 773, Gx_line+31, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+38);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               AV103SubTitulo = "ORDEM DE SERVI�O";
               AV68Emissao = AV60DataInicio;
               AV109EmissaoTA = AV60DataInicio;
               AV106Volume = "A previs�o de volume total de servi�o apresentado neste documento � de:";
               AV104TituloOS = "N� da SS";
               AV79Identificacao1 = "Esta Ordem de Servi�o tem como objetivo a presta��o de servi�os t�cnicos especializados para as atividades de";
               AV80Identificacao2 = "an�lise e mensura��o de software com base na t�cnica de contagem de pontos de fun��o do sistema acima";
               AV81Identificacao3 = "referenciado no �mbito do Minist�rio da Sa�de.";
               AV89Os = AV121Os_Header;
               if ( ( Gx_page /  ( decimal )( 2 ) != Convert.ToDecimal( NumberUtil.Int( (long)(Gx_page/ (decimal)(2))) )) )
               {
                  AV91Pagina = 1;
                  getPrinter().GxDrawBitMap(context.GetImagePath( "6ded99f5-9d9f-4337-aeda-7b9046b912b6", "", context.GetTheme( )), 349, Gx_line+0, 429, Gx_line+80) ;
                  getPrinter().GxDrawLine(314, Gx_line+201, 314, Gx_line+250, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(428, Gx_line+201, 428, Gx_line+250, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(604, Gx_line+201, 604, Gx_line+251, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(14, Gx_line+201, 778, Gx_line+201, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawRect(14, Gx_line+168, 778, Gx_line+251, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(176, Gx_line+201, 176, Gx_line+249, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(428, Gx_line+169, 428, Gx_line+202, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(778, Gx_line+251, 778, Gx_line+286, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(14, Gx_line+250, 14, Gx_line+287, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(15, Gx_line+287, 778, Gx_line+287, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(314, Gx_line+251, 314, Gx_line+287, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(604, Gx_line+251, 604, Gx_line+287, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(428, Gx_line+251, 428, Gx_line+287, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(176, Gx_line+251, 176, Gx_line+287, 1, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV104TituloOS, "")), 17, Gx_line+218, 174, Gx_line+235, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("DATASUS", 555, Gx_line+177, 664, Gx_line+195, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("MINIST�RIO DA SA�DE", 139, Gx_line+177, 306, Gx_line+195, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("DATA DO CONTRATO", 616, Gx_line+218, 765, Gx_line+236, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("N� DO CONTRATO", 443, Gx_line+218, 590, Gx_line+236, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("DATA DE EMISS�O", 179, Gx_line+218, 310, Gx_line+236, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("LOTE", 322, Gx_line+217, 422, Gx_line+235, 1, 0, 0, 1) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV55Contrato_DataVigenciaInicio, "99/99/99"), 617, Gx_line+259, 766, Gx_line+276, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV56Contrato_Numero, "")), 441, Gx_line+258, 588, Gx_line+275, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV121Os_Header, "")), 17, Gx_line+256, 174, Gx_line+273, 1, 0, 0, 0) ;
                  getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV68Emissao, "99/99/99"), 182, Gx_line+258, 301, Gx_line+275, 1, 0, 0, 0) ;
                  getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("MINIST�RIO DA SA�DE", 317, Gx_line+83, 462, Gx_line+102, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("SECRETARIA DE GEST�O ESTRAT�GICA E PARTICIPATIVA", 217, Gx_line+100, 562, Gx_line+119, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("DEPARTAMENTO DE INFORM�TICA DO SUS - DATASUS", 217, Gx_line+117, 556, Gx_line+136, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV103SubTitulo, "")), 217, Gx_line+138, 551, Gx_line+158, 1+256, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV84Lote, "")), 323, Gx_line+257, 421, Gx_line+274, 1, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+292);
               }
               else
               {
                  AV91Pagina = 2;
               }
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
         add_metrics3( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Arial", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Calibri", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Calibri", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics3( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV10WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV84Lote = "";
         scmdbuf = "";
         P003G2_A596Lote_Codigo = new int[1] ;
         P003G2_A562Lote_Numero = new String[] {""} ;
         P003G2_A844Lote_DataContrato = new DateTime[] {DateTime.MinValue} ;
         P003G2_n844Lote_DataContrato = new bool[] {false} ;
         A562Lote_Numero = "";
         A844Lote_DataContrato = DateTime.MinValue;
         AV188Lote_Numero = "";
         AV55Contrato_DataVigenciaInicio = DateTime.MinValue;
         AV75GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV76GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV65DynamicFiltersSelector1 = "";
         AV19ContagemResultado_DataCnt1 = DateTime.MinValue;
         AV16ContagemResultado_DataCnt_To1 = DateTime.MinValue;
         AV47ContagemResultado_StatusDmn1 = "";
         AV29ContagemResultado_OsFsOsFm1 = "";
         AV296ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV291ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV307ContagemResultado_DataPrevista1 = (DateTime)(DateTime.MinValue);
         AV302ContagemResultado_DataPrevista_To1 = (DateTime)(DateTime.MinValue);
         AV137ContagemResultado_Baseline1 = "";
         AV196ContagemResultado_Agrupador1 = "";
         AV212ContagemResultado_Descricao1 = "";
         AV233ContagemResultado_TemPndHmlg1 = "";
         AV66DynamicFiltersSelector2 = "";
         AV20ContagemResultado_DataCnt2 = DateTime.MinValue;
         AV17ContagemResultado_DataCnt_To2 = DateTime.MinValue;
         AV48ContagemResultado_StatusDmn2 = "";
         AV30ContagemResultado_OsFsOsFm2 = "";
         AV297ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV292ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV308ContagemResultado_DataPrevista2 = (DateTime)(DateTime.MinValue);
         AV303ContagemResultado_DataPrevista_To2 = (DateTime)(DateTime.MinValue);
         AV138ContagemResultado_Baseline2 = "";
         AV197ContagemResultado_Agrupador2 = "";
         AV213ContagemResultado_Descricao2 = "";
         AV234ContagemResultado_TemPndHmlg2 = "";
         AV67DynamicFiltersSelector3 = "";
         AV21ContagemResultado_DataCnt3 = DateTime.MinValue;
         AV18ContagemResultado_DataCnt_To3 = DateTime.MinValue;
         AV49ContagemResultado_StatusDmn3 = "";
         AV31ContagemResultado_OsFsOsFm3 = "";
         AV298ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV293ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV309ContagemResultado_DataPrevista3 = (DateTime)(DateTime.MinValue);
         AV304ContagemResultado_DataPrevista_To3 = (DateTime)(DateTime.MinValue);
         AV139ContagemResultado_Baseline3 = "";
         AV198ContagemResultado_Agrupador3 = "";
         AV214ContagemResultado_Descricao3 = "";
         AV235ContagemResultado_TemPndHmlg3 = "";
         AV148DynamicFiltersSelector4 = "";
         AV155ContagemResultado_DataCnt4 = DateTime.MinValue;
         AV154ContagemResultado_DataCnt_To4 = DateTime.MinValue;
         AV164ContagemResultado_StatusDmn4 = "";
         AV159ContagemResultado_OsFsOsFm4 = "";
         AV299ContagemResultado_DataDmn4 = DateTime.MinValue;
         AV294ContagemResultado_DataDmn_To4 = DateTime.MinValue;
         AV310ContagemResultado_DataPrevista4 = (DateTime)(DateTime.MinValue);
         AV305ContagemResultado_DataPrevista_To4 = (DateTime)(DateTime.MinValue);
         AV150ContagemResultado_Baseline4 = "";
         AV199ContagemResultado_Agrupador4 = "";
         AV215ContagemResultado_Descricao4 = "";
         AV236ContagemResultado_TemPndHmlg4 = "";
         AV149DynamicFiltersSelector5 = "";
         AV157ContagemResultado_DataCnt5 = DateTime.MinValue;
         AV156ContagemResultado_DataCnt_To5 = DateTime.MinValue;
         AV171ContagemResultado_StatusDmn5 = "";
         AV166ContagemResultado_OsFsOsFm5 = "";
         AV300ContagemResultado_DataDmn5 = DateTime.MinValue;
         AV295ContagemResultado_DataDmn_To5 = DateTime.MinValue;
         AV311ContagemResultado_DataPrevista5 = (DateTime)(DateTime.MinValue);
         AV306ContagemResultado_DataPrevista_To5 = (DateTime)(DateTime.MinValue);
         AV151ContagemResultado_Baseline5 = "";
         AV200ContagemResultado_Agrupador5 = "";
         AV216ContagemResultado_Descricao5 = "";
         AV237ContagemResultado_TemPndHmlg5 = "";
         AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = "";
         AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 = DateTime.MinValue;
         AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 = DateTime.MinValue;
         AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 = "";
         AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = "";
         AV335ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV336ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV337ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 = DateTime.MinValue;
         AV338ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 = DateTime.MinValue;
         AV344ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 = "";
         AV348ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 = "";
         AV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 = "";
         AV351ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 = "";
         AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = "";
         AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 = DateTime.MinValue;
         AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 = DateTime.MinValue;
         AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 = "";
         AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = "";
         AV360ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV361ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV362ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 = DateTime.MinValue;
         AV363ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 = DateTime.MinValue;
         AV369ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 = "";
         AV373ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 = "";
         AV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 = "";
         AV376ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 = "";
         AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = "";
         AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 = DateTime.MinValue;
         AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 = DateTime.MinValue;
         AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 = "";
         AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = "";
         AV385ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV386ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV387ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 = DateTime.MinValue;
         AV388ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 = DateTime.MinValue;
         AV394ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 = "";
         AV398ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 = "";
         AV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 = "";
         AV401ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 = "";
         AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = "";
         AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 = DateTime.MinValue;
         AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 = DateTime.MinValue;
         AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 = "";
         AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = "";
         AV410ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 = DateTime.MinValue;
         AV411ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 = DateTime.MinValue;
         AV412ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 = DateTime.MinValue;
         AV413ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 = DateTime.MinValue;
         AV419ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 = "";
         AV423ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 = "";
         AV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 = "";
         AV426ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 = "";
         AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = "";
         AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 = DateTime.MinValue;
         AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 = DateTime.MinValue;
         AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 = "";
         AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = "";
         AV435ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 = DateTime.MinValue;
         AV436ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 = DateTime.MinValue;
         AV437ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 = DateTime.MinValue;
         AV438ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 = DateTime.MinValue;
         AV444ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 = "";
         AV448ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 = "";
         AV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 = "";
         AV451ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 = "";
         AV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador = "";
         AV238TFContagemResultado_Agrupador = "";
         AV454ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel = "";
         AV239TFContagemResultado_Agrupador_Sel = "";
         AV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm = "";
         AV247TFContagemResultado_DemandaFM = "";
         AV456ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel = "";
         AV248TFContagemResultado_DemandaFM_Sel = "";
         AV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda = "";
         AV245TFContagemResultado_Demanda = "";
         AV458ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel = "";
         AV246TFContagemResultado_Demanda_Sel = "";
         AV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao = "";
         AV249TFContagemResultado_Descricao = "";
         AV460ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel = "";
         AV250TFContagemResultado_Descricao_Sel = "";
         AV461ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista = (DateTime)(DateTime.MinValue);
         AV314TFContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         AV462ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to = (DateTime)(DateTime.MinValue);
         AV315TFContagemResultado_DataPrevista_To = (DateTime)(DateTime.MinValue);
         AV463ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn = DateTime.MinValue;
         AV312TFContagemResultado_DataDmn = DateTime.MinValue;
         AV464ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to = DateTime.MinValue;
         AV313TFContagemResultado_DataDmn_To = DateTime.MinValue;
         AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt = DateTime.MinValue;
         AV243TFContagemResultado_DataUltCnt = DateTime.MinValue;
         AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to = DateTime.MinValue;
         AV244TFContagemResultado_DataUltCnt_To = DateTime.MinValue;
         AV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla = "";
         AV241TFContagemResultado_ContratadaSigla = "";
         AV468ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel = "";
         AV242TFContagemResultado_ContratadaSigla_Sel = "";
         AV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord = "";
         AV255TFContagemResultado_SistemaCoord = "";
         AV470ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel = "";
         AV256TFContagemResultado_SistemaCoord_Sel = "";
         AV471ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels = new GxSimpleCollection();
         AV259TFContagemResultado_StatusDmn_Sels = new GxSimpleCollection();
         AV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla = "";
         AV253TFContagemResultado_ServicoSigla = "";
         AV474ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel = "";
         AV254TFContagemResultado_ServicoSigla_Sel = "";
         lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 = "";
         lV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 = "";
         lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 = "";
         lV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 = "";
         lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 = "";
         lV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 = "";
         lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 = "";
         lV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 = "";
         lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 = "";
         lV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 = "";
         lV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador = "";
         lV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm = "";
         lV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda = "";
         lV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao = "";
         lV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla = "";
         lV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord = "";
         lV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla = "";
         A484ContagemResultado_StatusDmn = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A1046ContagemResultado_Agrupador = "";
         A494ContagemResultado_Descricao = "";
         A803ContagemResultado_ContratadaSigla = "";
         A515ContagemResultado_SistemaCoord = "";
         A801ContagemResultado_ServicoSigla = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         P003G4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P003G4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P003G4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P003G4_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P003G4_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003G4_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003G4_A1452ContagemResultado_SS = new int[1] ;
         P003G4_n1452ContagemResultado_SS = new bool[] {false} ;
         P003G4_A457ContagemResultado_Demanda = new String[] {""} ;
         P003G4_n457ContagemResultado_Demanda = new bool[] {false} ;
         P003G4_A512ContagemResultado_ValorPF = new decimal[1] ;
         P003G4_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P003G4_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P003G4_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P003G4_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P003G4_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P003G4_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P003G4_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P003G4_A1603ContagemResultado_CntCod = new int[1] ;
         P003G4_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P003G4_A494ContagemResultado_Descricao = new String[] {""} ;
         P003G4_n494ContagemResultado_Descricao = new bool[] {false} ;
         P003G4_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P003G4_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P003G4_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P003G4_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P003G4_A598ContagemResultado_Baseline = new bool[] {false} ;
         P003G4_n598ContagemResultado_Baseline = new bool[] {false} ;
         P003G4_A764ContagemResultado_ServicoGrupo = new int[1] ;
         P003G4_n764ContagemResultado_ServicoGrupo = new bool[] {false} ;
         P003G4_A489ContagemResultado_SistemaCod = new int[1] ;
         P003G4_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P003G4_A508ContagemResultado_Owner = new int[1] ;
         P003G4_A601ContagemResultado_Servico = new int[1] ;
         P003G4_n601ContagemResultado_Servico = new bool[] {false} ;
         P003G4_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P003G4_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P003G4_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P003G4_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P003G4_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P003G4_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P003G4_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P003G4_A490ContagemResultado_ContratadaCod = new int[1] ;
         P003G4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P003G4_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P003G4_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P003G4_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         P003G4_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         P003G4_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P003G4_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P003G4_A584ContagemResultado_ContadorFM = new int[1] ;
         P003G4_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P003G4_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         P003G4_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P003G4_A456ContagemResultado_Codigo = new int[1] ;
         P003G4_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P003G4_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A509ContagemrResultado_SistemaSigla = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         AV224ContagemResultado_SS = 0;
         AV56Contrato_Numero = "";
         AV102Sistema_Sigla = "";
         AV101Sistema_Coordenacao = "";
         AV202strValor = "";
         AV205strValorIni = "";
         AV208strValorTotal = "";
         AV210strPFTotal = "";
         AV60DataInicio = DateTime.MinValue;
         AV59DataFim = DateTime.MinValue;
         AV54Contratada_PessoaNom = "";
         AV53Contratada_PessoaCNPJ = "";
         AV93Pessoa_Endereco = "";
         AV96Pessoa_Telefone = "";
         AV92Pessoa_CEP = "";
         AV94Pessoa_Fax = "";
         AV97Pessoa_UF = "";
         AV85Municipio_Nome = "";
         AV95Pessoa_IE = "";
         AV106Volume = "";
         AV79Identificacao1 = "";
         AV80Identificacao2 = "";
         AV81Identificacao3 = "";
         AV201strPFFinal = "";
         AV204strPFInicial = "";
         P003G6_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P003G6_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P003G6_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P003G6_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P003G6_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003G6_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003G6_A1452ContagemResultado_SS = new int[1] ;
         P003G6_n1452ContagemResultado_SS = new bool[] {false} ;
         P003G6_A512ContagemResultado_ValorPF = new decimal[1] ;
         P003G6_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P003G6_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P003G6_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P003G6_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P003G6_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P003G6_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P003G6_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P003G6_A1603ContagemResultado_CntCod = new int[1] ;
         P003G6_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P003G6_A494ContagemResultado_Descricao = new String[] {""} ;
         P003G6_n494ContagemResultado_Descricao = new bool[] {false} ;
         P003G6_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P003G6_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P003G6_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P003G6_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P003G6_A598ContagemResultado_Baseline = new bool[] {false} ;
         P003G6_n598ContagemResultado_Baseline = new bool[] {false} ;
         P003G6_A764ContagemResultado_ServicoGrupo = new int[1] ;
         P003G6_n764ContagemResultado_ServicoGrupo = new bool[] {false} ;
         P003G6_A489ContagemResultado_SistemaCod = new int[1] ;
         P003G6_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P003G6_A508ContagemResultado_Owner = new int[1] ;
         P003G6_A601ContagemResultado_Servico = new int[1] ;
         P003G6_n601ContagemResultado_Servico = new bool[] {false} ;
         P003G6_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P003G6_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P003G6_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P003G6_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P003G6_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P003G6_A457ContagemResultado_Demanda = new String[] {""} ;
         P003G6_n457ContagemResultado_Demanda = new bool[] {false} ;
         P003G6_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P003G6_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P003G6_A490ContagemResultado_ContratadaCod = new int[1] ;
         P003G6_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P003G6_A1623ContagemResultado_CntSrvMmn = new String[] {""} ;
         P003G6_n1623ContagemResultado_CntSrvMmn = new bool[] {false} ;
         P003G6_A1855ContagemResultado_PFCnc = new decimal[1] ;
         P003G6_n1855ContagemResultado_PFCnc = new bool[] {false} ;
         P003G6_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         P003G6_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         P003G6_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P003G6_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P003G6_A584ContagemResultado_ContadorFM = new int[1] ;
         P003G6_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P003G6_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         P003G6_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P003G6_A456ContagemResultado_Codigo = new int[1] ;
         P003G6_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P003G6_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A1623ContagemResultado_CntSrvMmn = "";
         P003G7_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P003G7_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P003G7_A1452ContagemResultado_SS = new int[1] ;
         P003G7_n1452ContagemResultado_SS = new bool[] {false} ;
         P003G7_A457ContagemResultado_Demanda = new String[] {""} ;
         P003G7_n457ContagemResultado_Demanda = new bool[] {false} ;
         P003G7_A489ContagemResultado_SistemaCod = new int[1] ;
         P003G7_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P003G7_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P003G7_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P003G7_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P003G7_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P003G7_A512ContagemResultado_ValorPF = new decimal[1] ;
         P003G7_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P003G7_A456ContagemResultado_Codigo = new int[1] ;
         AV121Os_Header = "";
         P003G8_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P003G8_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P003G8_A1452ContagemResultado_SS = new int[1] ;
         P003G8_n1452ContagemResultado_SS = new bool[] {false} ;
         P003G8_A457ContagemResultado_Demanda = new String[] {""} ;
         P003G8_n457ContagemResultado_Demanda = new bool[] {false} ;
         P003G8_A489ContagemResultado_SistemaCod = new int[1] ;
         P003G8_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P003G8_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P003G8_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P003G8_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P003G8_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P003G8_A512ContagemResultado_ValorPF = new decimal[1] ;
         P003G8_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P003G8_A456ContagemResultado_Codigo = new int[1] ;
         P003G11_A40000GXC1 = new DateTime[] {DateTime.MinValue} ;
         P003G11_A40001GXC2 = new DateTime[] {DateTime.MinValue} ;
         A40000GXC1 = DateTime.MinValue;
         A40001GXC2 = DateTime.MinValue;
         P003G12_A489ContagemResultado_SistemaCod = new int[1] ;
         P003G12_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P003G12_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P003G12_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P003G12_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P003G12_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P003G12_A1452ContagemResultado_SS = new int[1] ;
         P003G12_n1452ContagemResultado_SS = new bool[] {false} ;
         P003G12_A1623ContagemResultado_CntSrvMmn = new String[] {""} ;
         P003G12_n1623ContagemResultado_CntSrvMmn = new bool[] {false} ;
         P003G12_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003G12_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003G12_A1855ContagemResultado_PFCnc = new decimal[1] ;
         P003G12_n1855ContagemResultado_PFCnc = new bool[] {false} ;
         P003G12_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P003G12_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P003G12_A456ContagemResultado_Codigo = new int[1] ;
         P003G15_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P003G15_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P003G15_A1603ContagemResultado_CntCod = new int[1] ;
         P003G15_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P003G15_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P003G15_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P003G15_A456ContagemResultado_Codigo = new int[1] ;
         P003G15_A1612ContagemResultado_CntNum = new String[] {""} ;
         P003G15_n1612ContagemResultado_CntNum = new bool[] {false} ;
         P003G15_A1624ContagemResultado_DatVgnInc = new DateTime[] {DateTime.MinValue} ;
         P003G15_n1624ContagemResultado_DatVgnInc = new bool[] {false} ;
         P003G15_A490ContagemResultado_ContratadaCod = new int[1] ;
         P003G15_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P003G15_A40002GXC3 = new DateTime[] {DateTime.MinValue} ;
         P003G15_n40002GXC3 = new bool[] {false} ;
         A1612ContagemResultado_CntNum = "";
         A1624ContagemResultado_DatVgnInc = DateTime.MinValue;
         A40002GXC3 = DateTime.MinValue;
         AV144Contrato_DataInicioTA = DateTime.MinValue;
         P003G16_A40Contratada_PessoaCod = new int[1] ;
         P003G16_A503Pessoa_MunicipioCod = new int[1] ;
         P003G16_n503Pessoa_MunicipioCod = new bool[] {false} ;
         P003G16_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P003G16_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P003G16_A29Contratante_Codigo = new int[1] ;
         P003G16_n29Contratante_Codigo = new bool[] {false} ;
         P003G16_A39Contratada_Codigo = new int[1] ;
         P003G16_A41Contratada_PessoaNom = new String[] {""} ;
         P003G16_n41Contratada_PessoaNom = new bool[] {false} ;
         P003G16_A524Contratada_OS = new int[1] ;
         P003G16_n524Contratada_OS = new bool[] {false} ;
         P003G16_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P003G16_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P003G16_A518Pessoa_IE = new String[] {""} ;
         P003G16_n518Pessoa_IE = new bool[] {false} ;
         P003G16_A519Pessoa_Endereco = new String[] {""} ;
         P003G16_n519Pessoa_Endereco = new bool[] {false} ;
         P003G16_A521Pessoa_CEP = new String[] {""} ;
         P003G16_n521Pessoa_CEP = new bool[] {false} ;
         P003G16_A522Pessoa_Telefone = new String[] {""} ;
         P003G16_n522Pessoa_Telefone = new bool[] {false} ;
         P003G16_A523Pessoa_Fax = new String[] {""} ;
         P003G16_n523Pessoa_Fax = new bool[] {false} ;
         P003G16_A520Pessoa_UF = new String[] {""} ;
         P003G16_n520Pessoa_UF = new bool[] {false} ;
         P003G16_A26Municipio_Nome = new String[] {""} ;
         P003G16_n26Municipio_Nome = new bool[] {false} ;
         P003G16_A593Contratante_OSAutomatica = new bool[] {false} ;
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         A518Pessoa_IE = "";
         A519Pessoa_Endereco = "";
         A521Pessoa_CEP = "";
         A522Pessoa_Telefone = "";
         A523Pessoa_Fax = "";
         A520Pessoa_UF = "";
         A26Municipio_Nome = "";
         AV89Os = "";
         AV103SubTitulo = "";
         AV68Emissao = DateTime.MinValue;
         AV109EmissaoTA = DateTime.MinValue;
         AV104TituloOS = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.arel_contagemos__default(),
            new Object[][] {
                new Object[] {
               P003G2_A596Lote_Codigo, P003G2_A562Lote_Numero, P003G2_A844Lote_DataContrato, P003G2_n844Lote_DataContrato
               }
               , new Object[] {
               P003G4_A1553ContagemResultado_CntSrvCod, P003G4_n1553ContagemResultado_CntSrvCod, P003G4_A52Contratada_AreaTrabalhoCod, P003G4_n52Contratada_AreaTrabalhoCod, P003G4_A484ContagemResultado_StatusDmn, P003G4_n484ContagemResultado_StatusDmn, P003G4_A1452ContagemResultado_SS, P003G4_n1452ContagemResultado_SS, P003G4_A457ContagemResultado_Demanda, P003G4_n457ContagemResultado_Demanda,
               P003G4_A512ContagemResultado_ValorPF, P003G4_n512ContagemResultado_ValorPF, P003G4_A801ContagemResultado_ServicoSigla, P003G4_n801ContagemResultado_ServicoSigla, P003G4_A515ContagemResultado_SistemaCoord, P003G4_n515ContagemResultado_SistemaCoord, P003G4_A803ContagemResultado_ContratadaSigla, P003G4_n803ContagemResultado_ContratadaSigla, P003G4_A1603ContagemResultado_CntCod, P003G4_n1603ContagemResultado_CntCod,
               P003G4_A494ContagemResultado_Descricao, P003G4_n494ContagemResultado_Descricao, P003G4_A1046ContagemResultado_Agrupador, P003G4_n1046ContagemResultado_Agrupador, P003G4_A468ContagemResultado_NaoCnfDmnCod, P003G4_n468ContagemResultado_NaoCnfDmnCod, P003G4_A598ContagemResultado_Baseline, P003G4_n598ContagemResultado_Baseline, P003G4_A764ContagemResultado_ServicoGrupo, P003G4_n764ContagemResultado_ServicoGrupo,
               P003G4_A489ContagemResultado_SistemaCod, P003G4_n489ContagemResultado_SistemaCod, P003G4_A508ContagemResultado_Owner, P003G4_A601ContagemResultado_Servico, P003G4_n601ContagemResultado_Servico, P003G4_A1351ContagemResultado_DataPrevista, P003G4_n1351ContagemResultado_DataPrevista, P003G4_A471ContagemResultado_DataDmn, P003G4_A493ContagemResultado_DemandaFM, P003G4_n493ContagemResultado_DemandaFM,
               P003G4_A1854ContagemResultado_VlrCnc, P003G4_n1854ContagemResultado_VlrCnc, P003G4_A490ContagemResultado_ContratadaCod, P003G4_n490ContagemResultado_ContratadaCod, P003G4_A509ContagemrResultado_SistemaSigla, P003G4_n509ContagemrResultado_SistemaSigla, P003G4_A683ContagemResultado_PFLFMUltima, P003G4_A685ContagemResultado_PFLFSUltima, P003G4_A682ContagemResultado_PFBFMUltima, P003G4_A684ContagemResultado_PFBFSUltima,
               P003G4_A584ContagemResultado_ContadorFM, P003G4_A566ContagemResultado_DataUltCnt, P003G4_A531ContagemResultado_StatusUltCnt, P003G4_A456ContagemResultado_Codigo, P003G4_A473ContagemResultado_DataCnt, P003G4_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               P003G6_A1553ContagemResultado_CntSrvCod, P003G6_n1553ContagemResultado_CntSrvCod, P003G6_A52Contratada_AreaTrabalhoCod, P003G6_n52Contratada_AreaTrabalhoCod, P003G6_A484ContagemResultado_StatusDmn, P003G6_n484ContagemResultado_StatusDmn, P003G6_A1452ContagemResultado_SS, P003G6_n1452ContagemResultado_SS, P003G6_A512ContagemResultado_ValorPF, P003G6_n512ContagemResultado_ValorPF,
               P003G6_A801ContagemResultado_ServicoSigla, P003G6_n801ContagemResultado_ServicoSigla, P003G6_A515ContagemResultado_SistemaCoord, P003G6_n515ContagemResultado_SistemaCoord, P003G6_A803ContagemResultado_ContratadaSigla, P003G6_n803ContagemResultado_ContratadaSigla, P003G6_A1603ContagemResultado_CntCod, P003G6_n1603ContagemResultado_CntCod, P003G6_A494ContagemResultado_Descricao, P003G6_n494ContagemResultado_Descricao,
               P003G6_A1046ContagemResultado_Agrupador, P003G6_n1046ContagemResultado_Agrupador, P003G6_A468ContagemResultado_NaoCnfDmnCod, P003G6_n468ContagemResultado_NaoCnfDmnCod, P003G6_A598ContagemResultado_Baseline, P003G6_n598ContagemResultado_Baseline, P003G6_A764ContagemResultado_ServicoGrupo, P003G6_n764ContagemResultado_ServicoGrupo, P003G6_A489ContagemResultado_SistemaCod, P003G6_n489ContagemResultado_SistemaCod,
               P003G6_A508ContagemResultado_Owner, P003G6_A601ContagemResultado_Servico, P003G6_n601ContagemResultado_Servico, P003G6_A1351ContagemResultado_DataPrevista, P003G6_n1351ContagemResultado_DataPrevista, P003G6_A471ContagemResultado_DataDmn, P003G6_A493ContagemResultado_DemandaFM, P003G6_n493ContagemResultado_DemandaFM, P003G6_A457ContagemResultado_Demanda, P003G6_n457ContagemResultado_Demanda,
               P003G6_A1854ContagemResultado_VlrCnc, P003G6_n1854ContagemResultado_VlrCnc, P003G6_A490ContagemResultado_ContratadaCod, P003G6_n490ContagemResultado_ContratadaCod, P003G6_A1623ContagemResultado_CntSrvMmn, P003G6_n1623ContagemResultado_CntSrvMmn, P003G6_A1855ContagemResultado_PFCnc, P003G6_n1855ContagemResultado_PFCnc, P003G6_A683ContagemResultado_PFLFMUltima, P003G6_A685ContagemResultado_PFLFSUltima,
               P003G6_A682ContagemResultado_PFBFMUltima, P003G6_A684ContagemResultado_PFBFSUltima, P003G6_A584ContagemResultado_ContadorFM, P003G6_A566ContagemResultado_DataUltCnt, P003G6_A531ContagemResultado_StatusUltCnt, P003G6_A456ContagemResultado_Codigo, P003G6_A473ContagemResultado_DataCnt, P003G6_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               P003G7_A597ContagemResultado_LoteAceiteCod, P003G7_n597ContagemResultado_LoteAceiteCod, P003G7_A1452ContagemResultado_SS, P003G7_n1452ContagemResultado_SS, P003G7_A457ContagemResultado_Demanda, P003G7_n457ContagemResultado_Demanda, P003G7_A489ContagemResultado_SistemaCod, P003G7_n489ContagemResultado_SistemaCod, P003G7_A509ContagemrResultado_SistemaSigla, P003G7_n509ContagemrResultado_SistemaSigla,
               P003G7_A515ContagemResultado_SistemaCoord, P003G7_n515ContagemResultado_SistemaCoord, P003G7_A512ContagemResultado_ValorPF, P003G7_n512ContagemResultado_ValorPF, P003G7_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P003G8_A597ContagemResultado_LoteAceiteCod, P003G8_n597ContagemResultado_LoteAceiteCod, P003G8_A1452ContagemResultado_SS, P003G8_n1452ContagemResultado_SS, P003G8_A457ContagemResultado_Demanda, P003G8_n457ContagemResultado_Demanda, P003G8_A489ContagemResultado_SistemaCod, P003G8_n489ContagemResultado_SistemaCod, P003G8_A509ContagemrResultado_SistemaSigla, P003G8_n509ContagemrResultado_SistemaSigla,
               P003G8_A515ContagemResultado_SistemaCoord, P003G8_n515ContagemResultado_SistemaCoord, P003G8_A512ContagemResultado_ValorPF, P003G8_n512ContagemResultado_ValorPF, P003G8_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P003G11_A40000GXC1, P003G11_A40001GXC2
               }
               , new Object[] {
               P003G12_A489ContagemResultado_SistemaCod, P003G12_n489ContagemResultado_SistemaCod, P003G12_A1553ContagemResultado_CntSrvCod, P003G12_n1553ContagemResultado_CntSrvCod, P003G12_A597ContagemResultado_LoteAceiteCod, P003G12_n597ContagemResultado_LoteAceiteCod, P003G12_A1452ContagemResultado_SS, P003G12_n1452ContagemResultado_SS, P003G12_A1623ContagemResultado_CntSrvMmn, P003G12_n1623ContagemResultado_CntSrvMmn,
               P003G12_A484ContagemResultado_StatusDmn, P003G12_n484ContagemResultado_StatusDmn, P003G12_A1855ContagemResultado_PFCnc, P003G12_n1855ContagemResultado_PFCnc, P003G12_A509ContagemrResultado_SistemaSigla, P003G12_n509ContagemrResultado_SistemaSigla, P003G12_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P003G15_A1553ContagemResultado_CntSrvCod, P003G15_n1553ContagemResultado_CntSrvCod, P003G15_A1603ContagemResultado_CntCod, P003G15_n1603ContagemResultado_CntCod, P003G15_A597ContagemResultado_LoteAceiteCod, P003G15_n597ContagemResultado_LoteAceiteCod, P003G15_A456ContagemResultado_Codigo, P003G15_A1612ContagemResultado_CntNum, P003G15_n1612ContagemResultado_CntNum, P003G15_A1624ContagemResultado_DatVgnInc,
               P003G15_n1624ContagemResultado_DatVgnInc, P003G15_A490ContagemResultado_ContratadaCod, P003G15_n490ContagemResultado_ContratadaCod, P003G15_A40002GXC3, P003G15_n40002GXC3
               }
               , new Object[] {
               P003G16_A40Contratada_PessoaCod, P003G16_A503Pessoa_MunicipioCod, P003G16_n503Pessoa_MunicipioCod, P003G16_A52Contratada_AreaTrabalhoCod, P003G16_A29Contratante_Codigo, P003G16_n29Contratante_Codigo, P003G16_A39Contratada_Codigo, P003G16_A41Contratada_PessoaNom, P003G16_n41Contratada_PessoaNom, P003G16_A524Contratada_OS,
               P003G16_n524Contratada_OS, P003G16_A42Contratada_PessoaCNPJ, P003G16_n42Contratada_PessoaCNPJ, P003G16_A518Pessoa_IE, P003G16_n518Pessoa_IE, P003G16_A519Pessoa_Endereco, P003G16_n519Pessoa_Endereco, P003G16_A521Pessoa_CEP, P003G16_n521Pessoa_CEP, P003G16_A522Pessoa_Telefone,
               P003G16_n522Pessoa_Telefone, P003G16_A523Pessoa_Fax, P003G16_n523Pessoa_Fax, P003G16_A520Pessoa_UF, P003G16_n520Pessoa_UF, P003G16_A26Municipio_Nome, P003G16_n26Municipio_Nome, P003G16_A593Contratante_OSAutomatica
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV45ContagemResultado_StatusCnt ;
      private short GxWebError ;
      private short AV126Paginas ;
      private short AV127Length ;
      private short AV225DynamicFiltersOperator1 ;
      private short AV226DynamicFiltersOperator2 ;
      private short AV227DynamicFiltersOperator3 ;
      private short AV228DynamicFiltersOperator4 ;
      private short AV229DynamicFiltersOperator5 ;
      private short AV328ExtraWWContagemResultadoExtraSelectionDS_2_Contagemresultado_statuscnt ;
      private short AV330ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 ;
      private short AV355ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 ;
      private short AV380ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 ;
      private short AV405ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 ;
      private short AV430ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 ;
      private short AV472ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel ;
      private short AV240TFContagemResultado_Baseline_Sel ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short AV78i ;
      private short AV115x ;
      private short AV91Pagina ;
      private int AV52Contratada_AreaTrabalhoCod ;
      private int AV8ContagemResultado_LoteAceite ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int A596Lote_Codigo ;
      private int AV133ContagemResultado_Servico1 ;
      private int AV172ContagemResultado_ContadorFM1 ;
      private int AV38ContagemResultado_SistemaCod1 ;
      private int AV190ContagemResultado_ContratadaCod1 ;
      private int AV141ContagemResultado_ServicoGrupo1 ;
      private int AV25ContagemResultado_NaoCnfDmnCod1 ;
      private int AV218ContagemResultado_Owner1 ;
      private int AV317ContagemResultado_CntCod1 ;
      private int AV135ContagemResultado_Servico2 ;
      private int AV173ContagemResultado_ContadorFM2 ;
      private int AV39ContagemResultado_SistemaCod2 ;
      private int AV191ContagemResultado_ContratadaCod2 ;
      private int AV142ContagemResultado_ServicoGrupo2 ;
      private int AV26ContagemResultado_NaoCnfDmnCod2 ;
      private int AV219ContagemResultado_Owner2 ;
      private int AV318ContagemResultado_CntCod2 ;
      private int AV136ContagemResultado_Servico3 ;
      private int AV174ContagemResultado_ContadorFM3 ;
      private int AV40ContagemResultado_SistemaCod3 ;
      private int AV192ContagemResultado_ContratadaCod3 ;
      private int AV143ContagemResultado_ServicoGrupo3 ;
      private int AV27ContagemResultado_NaoCnfDmnCod3 ;
      private int AV220ContagemResultado_Owner3 ;
      private int AV319ContagemResultado_CntCod3 ;
      private int AV160ContagemResultado_Servico4 ;
      private int AV175ContagemResultado_ContadorFM4 ;
      private int AV162ContagemResultado_SistemaCod4 ;
      private int AV193ContagemResultado_ContratadaCod4 ;
      private int AV161ContagemResultado_ServicoGrupo4 ;
      private int AV158ContagemResultado_NaoCnfDmnCod4 ;
      private int AV221ContagemResultado_Owner4 ;
      private int AV320ContagemResultado_CntCod4 ;
      private int AV167ContagemResultado_Servico5 ;
      private int AV176ContagemResultado_ContadorFM5 ;
      private int AV169ContagemResultado_SistemaCod5 ;
      private int AV194ContagemResultado_ContratadaCod5 ;
      private int AV168ContagemResultado_ServicoGrupo5 ;
      private int AV165ContagemResultado_NaoCnfDmnCod5 ;
      private int AV222ContagemResultado_Owner5 ;
      private int AV321ContagemResultado_CntCod5 ;
      private int AV327ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod ;
      private int AV339ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 ;
      private int AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 ;
      private int AV341ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 ;
      private int AV342ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 ;
      private int AV343ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 ;
      private int AV345ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 ;
      private int AV350ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 ;
      private int AV352ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 ;
      private int AV364ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 ;
      private int AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 ;
      private int AV366ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 ;
      private int AV367ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 ;
      private int AV368ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 ;
      private int AV370ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 ;
      private int AV375ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 ;
      private int AV377ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 ;
      private int AV389ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 ;
      private int AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 ;
      private int AV391ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 ;
      private int AV392ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 ;
      private int AV393ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 ;
      private int AV395ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 ;
      private int AV400ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 ;
      private int AV402ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 ;
      private int AV414ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 ;
      private int AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 ;
      private int AV416ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 ;
      private int AV417ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 ;
      private int AV418ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 ;
      private int AV420ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 ;
      private int AV425ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 ;
      private int AV427ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 ;
      private int AV439ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 ;
      private int AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 ;
      private int AV441ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 ;
      private int AV442ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 ;
      private int AV443ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 ;
      private int AV445ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 ;
      private int AV450ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 ;
      private int AV452ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 ;
      private int AV10WWPContext_gxTpr_Contratada_codigo ;
      private int AV471ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels_Count ;
      private int AV10WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int A489ContagemResultado_SistemaCod ;
      private int A764ContagemResultado_ServicoGrupo ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A508ContagemResultado_Owner ;
      private int A1603ContagemResultado_CntCod ;
      private int A584ContagemResultado_ContadorFM ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1452ContagemResultado_SS ;
      private int A456ContagemResultado_Codigo ;
      private int OV224ContagemResultado_SS ;
      private int AV224ContagemResultado_SS ;
      private int AV230Codigo ;
      private int AV9Contratada_OS ;
      private int Gx_OldLine ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int AV108Sistema_Codigo ;
      private int AV231Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A503Pessoa_MunicipioCod ;
      private int A29Contratante_Codigo ;
      private int A39Contratada_Codigo ;
      private int A524Contratada_OS ;
      private decimal AV177ContagemResultado_PFBFM1 ;
      private decimal AV182ContagemResultado_PFBFS1 ;
      private decimal AV178ContagemResultado_PFBFM2 ;
      private decimal AV183ContagemResultado_PFBFS2 ;
      private decimal AV179ContagemResultado_PFBFM3 ;
      private decimal AV184ContagemResultado_PFBFS3 ;
      private decimal AV180ContagemResultado_PFBFM4 ;
      private decimal AV185ContagemResultado_PFBFS4 ;
      private decimal AV181ContagemResultado_PFBFM5 ;
      private decimal AV186ContagemResultado_PFBFS5 ;
      private decimal AV346ExtraWWContagemResultadoExtraSelectionDS_20_Contagemresultado_pfbfm1 ;
      private decimal AV347ExtraWWContagemResultadoExtraSelectionDS_21_Contagemresultado_pfbfs1 ;
      private decimal AV371ExtraWWContagemResultadoExtraSelectionDS_45_Contagemresultado_pfbfm2 ;
      private decimal AV372ExtraWWContagemResultadoExtraSelectionDS_46_Contagemresultado_pfbfs2 ;
      private decimal AV396ExtraWWContagemResultadoExtraSelectionDS_70_Contagemresultado_pfbfm3 ;
      private decimal AV397ExtraWWContagemResultadoExtraSelectionDS_71_Contagemresultado_pfbfs3 ;
      private decimal AV421ExtraWWContagemResultadoExtraSelectionDS_95_Contagemresultado_pfbfm4 ;
      private decimal AV422ExtraWWContagemResultadoExtraSelectionDS_96_Contagemresultado_pfbfs4 ;
      private decimal AV446ExtraWWContagemResultadoExtraSelectionDS_120_Contagemresultado_pfbfm5 ;
      private decimal AV447ExtraWWContagemResultadoExtraSelectionDS_121_Contagemresultado_pfbfs5 ;
      private decimal AV475ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ;
      private decimal AV251TFContagemResultado_PFFinal ;
      private decimal AV476ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ;
      private decimal AV252TFContagemResultado_PFFinal_To ;
      private decimal AV477ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf ;
      private decimal AV261TFContagemResultado_ValorPF ;
      private decimal AV478ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to ;
      private decimal AV262TFContagemResultado_ValorPF_To ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A1854ContagemResultado_VlrCnc ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal A683ContagemResultado_PFLFMUltima ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal AV206ValorIni ;
      private decimal AV203PFInicial ;
      private decimal AV105Valor ;
      private decimal AV98PFFinal ;
      private decimal AV207ValorTotal ;
      private decimal A1855ContagemResultado_PFCnc ;
      private decimal GXt_decimal2 ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV84Lote ;
      private String scmdbuf ;
      private String A562Lote_Numero ;
      private String AV188Lote_Numero ;
      private String AV47ContagemResultado_StatusDmn1 ;
      private String AV137ContagemResultado_Baseline1 ;
      private String AV196ContagemResultado_Agrupador1 ;
      private String AV233ContagemResultado_TemPndHmlg1 ;
      private String AV48ContagemResultado_StatusDmn2 ;
      private String AV138ContagemResultado_Baseline2 ;
      private String AV197ContagemResultado_Agrupador2 ;
      private String AV234ContagemResultado_TemPndHmlg2 ;
      private String AV49ContagemResultado_StatusDmn3 ;
      private String AV139ContagemResultado_Baseline3 ;
      private String AV198ContagemResultado_Agrupador3 ;
      private String AV235ContagemResultado_TemPndHmlg3 ;
      private String AV164ContagemResultado_StatusDmn4 ;
      private String AV150ContagemResultado_Baseline4 ;
      private String AV199ContagemResultado_Agrupador4 ;
      private String AV236ContagemResultado_TemPndHmlg4 ;
      private String AV171ContagemResultado_StatusDmn5 ;
      private String AV151ContagemResultado_Baseline5 ;
      private String AV200ContagemResultado_Agrupador5 ;
      private String AV237ContagemResultado_TemPndHmlg5 ;
      private String AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 ;
      private String AV344ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 ;
      private String AV348ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 ;
      private String AV351ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 ;
      private String AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 ;
      private String AV369ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 ;
      private String AV373ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 ;
      private String AV376ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 ;
      private String AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 ;
      private String AV394ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 ;
      private String AV398ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 ;
      private String AV401ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 ;
      private String AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 ;
      private String AV419ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 ;
      private String AV423ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 ;
      private String AV426ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 ;
      private String AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 ;
      private String AV444ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 ;
      private String AV448ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 ;
      private String AV451ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 ;
      private String AV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador ;
      private String AV238TFContagemResultado_Agrupador ;
      private String AV454ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel ;
      private String AV239TFContagemResultado_Agrupador_Sel ;
      private String AV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla ;
      private String AV241TFContagemResultado_ContratadaSigla ;
      private String AV468ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel ;
      private String AV242TFContagemResultado_ContratadaSigla_Sel ;
      private String AV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla ;
      private String AV253TFContagemResultado_ServicoSigla ;
      private String AV474ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel ;
      private String AV254TFContagemResultado_ServicoSigla_Sel ;
      private String lV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador ;
      private String lV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla ;
      private String lV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1046ContagemResultado_Agrupador ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A511ContagemResultado_HoraCnt ;
      private String AV56Contrato_Numero ;
      private String AV102Sistema_Sigla ;
      private String AV202strValor ;
      private String AV205strValorIni ;
      private String AV208strValorTotal ;
      private String AV210strPFTotal ;
      private String AV54Contratada_PessoaNom ;
      private String AV96Pessoa_Telefone ;
      private String AV92Pessoa_CEP ;
      private String AV94Pessoa_Fax ;
      private String AV97Pessoa_UF ;
      private String AV85Municipio_Nome ;
      private String AV95Pessoa_IE ;
      private String AV106Volume ;
      private String AV79Identificacao1 ;
      private String AV80Identificacao2 ;
      private String AV81Identificacao3 ;
      private String AV201strPFFinal ;
      private String AV204strPFInicial ;
      private String A1623ContagemResultado_CntSrvMmn ;
      private String A1612ContagemResultado_CntNum ;
      private String A41Contratada_PessoaNom ;
      private String A518Pessoa_IE ;
      private String A521Pessoa_CEP ;
      private String A522Pessoa_Telefone ;
      private String A523Pessoa_Fax ;
      private String A520Pessoa_UF ;
      private String A26Municipio_Nome ;
      private String AV104TituloOS ;
      private DateTime AV307ContagemResultado_DataPrevista1 ;
      private DateTime AV302ContagemResultado_DataPrevista_To1 ;
      private DateTime AV308ContagemResultado_DataPrevista2 ;
      private DateTime AV303ContagemResultado_DataPrevista_To2 ;
      private DateTime AV309ContagemResultado_DataPrevista3 ;
      private DateTime AV304ContagemResultado_DataPrevista_To3 ;
      private DateTime AV310ContagemResultado_DataPrevista4 ;
      private DateTime AV305ContagemResultado_DataPrevista_To4 ;
      private DateTime AV311ContagemResultado_DataPrevista5 ;
      private DateTime AV306ContagemResultado_DataPrevista_To5 ;
      private DateTime AV461ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista ;
      private DateTime AV314TFContagemResultado_DataPrevista ;
      private DateTime AV462ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to ;
      private DateTime AV315TFContagemResultado_DataPrevista_To ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime A844Lote_DataContrato ;
      private DateTime AV55Contrato_DataVigenciaInicio ;
      private DateTime AV19ContagemResultado_DataCnt1 ;
      private DateTime AV16ContagemResultado_DataCnt_To1 ;
      private DateTime AV296ContagemResultado_DataDmn1 ;
      private DateTime AV291ContagemResultado_DataDmn_To1 ;
      private DateTime AV20ContagemResultado_DataCnt2 ;
      private DateTime AV17ContagemResultado_DataCnt_To2 ;
      private DateTime AV297ContagemResultado_DataDmn2 ;
      private DateTime AV292ContagemResultado_DataDmn_To2 ;
      private DateTime AV21ContagemResultado_DataCnt3 ;
      private DateTime AV18ContagemResultado_DataCnt_To3 ;
      private DateTime AV298ContagemResultado_DataDmn3 ;
      private DateTime AV293ContagemResultado_DataDmn_To3 ;
      private DateTime AV155ContagemResultado_DataCnt4 ;
      private DateTime AV154ContagemResultado_DataCnt_To4 ;
      private DateTime AV299ContagemResultado_DataDmn4 ;
      private DateTime AV294ContagemResultado_DataDmn_To4 ;
      private DateTime AV157ContagemResultado_DataCnt5 ;
      private DateTime AV156ContagemResultado_DataCnt_To5 ;
      private DateTime AV300ContagemResultado_DataDmn5 ;
      private DateTime AV295ContagemResultado_DataDmn_To5 ;
      private DateTime AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 ;
      private DateTime AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 ;
      private DateTime AV335ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 ;
      private DateTime AV336ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 ;
      private DateTime AV337ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 ;
      private DateTime AV338ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 ;
      private DateTime AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 ;
      private DateTime AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 ;
      private DateTime AV360ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 ;
      private DateTime AV361ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 ;
      private DateTime AV362ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 ;
      private DateTime AV363ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 ;
      private DateTime AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 ;
      private DateTime AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 ;
      private DateTime AV385ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 ;
      private DateTime AV386ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 ;
      private DateTime AV387ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 ;
      private DateTime AV388ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 ;
      private DateTime AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 ;
      private DateTime AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 ;
      private DateTime AV410ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 ;
      private DateTime AV411ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 ;
      private DateTime AV412ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 ;
      private DateTime AV413ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 ;
      private DateTime AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 ;
      private DateTime AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 ;
      private DateTime AV435ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 ;
      private DateTime AV436ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 ;
      private DateTime AV437ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 ;
      private DateTime AV438ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 ;
      private DateTime AV463ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn ;
      private DateTime AV312TFContagemResultado_DataDmn ;
      private DateTime AV464ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to ;
      private DateTime AV313TFContagemResultado_DataDmn_To ;
      private DateTime AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt ;
      private DateTime AV243TFContagemResultado_DataUltCnt ;
      private DateTime AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to ;
      private DateTime AV244TFContagemResultado_DataUltCnt_To ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime AV60DataInicio ;
      private DateTime AV59DataFim ;
      private DateTime A40000GXC1 ;
      private DateTime A40001GXC2 ;
      private DateTime A1624ContagemResultado_DatVgnInc ;
      private DateTime A40002GXC3 ;
      private DateTime AV144Contrato_DataInicioTA ;
      private DateTime AV68Emissao ;
      private DateTime AV109EmissaoTA ;
      private bool entryPointCalled ;
      private bool returnInSub ;
      private bool n844Lote_DataContrato ;
      private bool AV63DynamicFiltersEnabled2 ;
      private bool AV64DynamicFiltersEnabled3 ;
      private bool AV146DynamicFiltersEnabled4 ;
      private bool AV147DynamicFiltersEnabled5 ;
      private bool AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 ;
      private bool AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 ;
      private bool AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 ;
      private bool AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 ;
      private bool A598ContagemResultado_Baseline ;
      private bool A1802ContagemResultado_TemPndHmlg ;
      private bool BRK3G5 ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1452ContagemResultado_SS ;
      private bool n457ContagemResultado_Demanda ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n515ContagemResultado_SistemaCoord ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n494ContagemResultado_Descricao ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n598ContagemResultado_Baseline ;
      private bool n764ContagemResultado_ServicoGrupo ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n1854ContagemResultado_VlrCnc ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n566ContagemResultado_DataUltCnt ;
      private bool AV82ImprimindoOS ;
      private bool n1623ContagemResultado_CntSrvMmn ;
      private bool n1855ContagemResultado_PFCnc ;
      private bool GXt_boolean1 ;
      private bool AV132Contratante_OSAutomatica ;
      private bool BRK3G8 ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool BRK3G10 ;
      private bool n1612ContagemResultado_CntNum ;
      private bool n1624ContagemResultado_DatVgnInc ;
      private bool n40002GXC3 ;
      private bool n503Pessoa_MunicipioCod ;
      private bool n29Contratante_Codigo ;
      private bool n41Contratada_PessoaNom ;
      private bool n524Contratada_OS ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n518Pessoa_IE ;
      private bool n519Pessoa_Endereco ;
      private bool n521Pessoa_CEP ;
      private bool n522Pessoa_Telefone ;
      private bool n523Pessoa_Fax ;
      private bool n520Pessoa_UF ;
      private bool n26Municipio_Nome ;
      private bool A593Contratante_OSAutomatica ;
      private String AV77GridStateXML ;
      private String AV65DynamicFiltersSelector1 ;
      private String AV29ContagemResultado_OsFsOsFm1 ;
      private String AV212ContagemResultado_Descricao1 ;
      private String AV66DynamicFiltersSelector2 ;
      private String AV30ContagemResultado_OsFsOsFm2 ;
      private String AV213ContagemResultado_Descricao2 ;
      private String AV67DynamicFiltersSelector3 ;
      private String AV31ContagemResultado_OsFsOsFm3 ;
      private String AV214ContagemResultado_Descricao3 ;
      private String AV148DynamicFiltersSelector4 ;
      private String AV159ContagemResultado_OsFsOsFm4 ;
      private String AV215ContagemResultado_Descricao4 ;
      private String AV149DynamicFiltersSelector5 ;
      private String AV166ContagemResultado_OsFsOsFm5 ;
      private String AV216ContagemResultado_Descricao5 ;
      private String AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 ;
      private String AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 ;
      private String AV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 ;
      private String AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 ;
      private String AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 ;
      private String AV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 ;
      private String AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 ;
      private String AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 ;
      private String AV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 ;
      private String AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 ;
      private String AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 ;
      private String AV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 ;
      private String AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 ;
      private String AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 ;
      private String AV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 ;
      private String AV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm ;
      private String AV247TFContagemResultado_DemandaFM ;
      private String AV456ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel ;
      private String AV248TFContagemResultado_DemandaFM_Sel ;
      private String AV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda ;
      private String AV245TFContagemResultado_Demanda ;
      private String AV458ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel ;
      private String AV246TFContagemResultado_Demanda_Sel ;
      private String AV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao ;
      private String AV249TFContagemResultado_Descricao ;
      private String AV460ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel ;
      private String AV250TFContagemResultado_Descricao_Sel ;
      private String AV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord ;
      private String AV255TFContagemResultado_SistemaCoord ;
      private String AV470ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel ;
      private String AV256TFContagemResultado_SistemaCoord_Sel ;
      private String lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 ;
      private String lV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 ;
      private String lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 ;
      private String lV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 ;
      private String lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 ;
      private String lV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 ;
      private String lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 ;
      private String lV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 ;
      private String lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 ;
      private String lV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 ;
      private String lV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm ;
      private String lV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda ;
      private String lV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao ;
      private String lV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String A515ContagemResultado_SistemaCoord ;
      private String AV101Sistema_Coordenacao ;
      private String AV53Contratada_PessoaCNPJ ;
      private String AV93Pessoa_Endereco ;
      private String AV121Os_Header ;
      private String A42Contratada_PessoaCNPJ ;
      private String A519Pessoa_Endereco ;
      private String AV89Os ;
      private String AV103SubTitulo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P003G2_A596Lote_Codigo ;
      private String[] P003G2_A562Lote_Numero ;
      private DateTime[] P003G2_A844Lote_DataContrato ;
      private bool[] P003G2_n844Lote_DataContrato ;
      private int[] P003G4_A1553ContagemResultado_CntSrvCod ;
      private bool[] P003G4_n1553ContagemResultado_CntSrvCod ;
      private int[] P003G4_A52Contratada_AreaTrabalhoCod ;
      private bool[] P003G4_n52Contratada_AreaTrabalhoCod ;
      private String[] P003G4_A484ContagemResultado_StatusDmn ;
      private bool[] P003G4_n484ContagemResultado_StatusDmn ;
      private int[] P003G4_A1452ContagemResultado_SS ;
      private bool[] P003G4_n1452ContagemResultado_SS ;
      private String[] P003G4_A457ContagemResultado_Demanda ;
      private bool[] P003G4_n457ContagemResultado_Demanda ;
      private decimal[] P003G4_A512ContagemResultado_ValorPF ;
      private bool[] P003G4_n512ContagemResultado_ValorPF ;
      private String[] P003G4_A801ContagemResultado_ServicoSigla ;
      private bool[] P003G4_n801ContagemResultado_ServicoSigla ;
      private String[] P003G4_A515ContagemResultado_SistemaCoord ;
      private bool[] P003G4_n515ContagemResultado_SistemaCoord ;
      private String[] P003G4_A803ContagemResultado_ContratadaSigla ;
      private bool[] P003G4_n803ContagemResultado_ContratadaSigla ;
      private int[] P003G4_A1603ContagemResultado_CntCod ;
      private bool[] P003G4_n1603ContagemResultado_CntCod ;
      private String[] P003G4_A494ContagemResultado_Descricao ;
      private bool[] P003G4_n494ContagemResultado_Descricao ;
      private String[] P003G4_A1046ContagemResultado_Agrupador ;
      private bool[] P003G4_n1046ContagemResultado_Agrupador ;
      private int[] P003G4_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P003G4_n468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P003G4_A598ContagemResultado_Baseline ;
      private bool[] P003G4_n598ContagemResultado_Baseline ;
      private int[] P003G4_A764ContagemResultado_ServicoGrupo ;
      private bool[] P003G4_n764ContagemResultado_ServicoGrupo ;
      private int[] P003G4_A489ContagemResultado_SistemaCod ;
      private bool[] P003G4_n489ContagemResultado_SistemaCod ;
      private int[] P003G4_A508ContagemResultado_Owner ;
      private int[] P003G4_A601ContagemResultado_Servico ;
      private bool[] P003G4_n601ContagemResultado_Servico ;
      private DateTime[] P003G4_A1351ContagemResultado_DataPrevista ;
      private bool[] P003G4_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P003G4_A471ContagemResultado_DataDmn ;
      private String[] P003G4_A493ContagemResultado_DemandaFM ;
      private bool[] P003G4_n493ContagemResultado_DemandaFM ;
      private decimal[] P003G4_A1854ContagemResultado_VlrCnc ;
      private bool[] P003G4_n1854ContagemResultado_VlrCnc ;
      private int[] P003G4_A490ContagemResultado_ContratadaCod ;
      private bool[] P003G4_n490ContagemResultado_ContratadaCod ;
      private String[] P003G4_A509ContagemrResultado_SistemaSigla ;
      private bool[] P003G4_n509ContagemrResultado_SistemaSigla ;
      private decimal[] P003G4_A683ContagemResultado_PFLFMUltima ;
      private decimal[] P003G4_A685ContagemResultado_PFLFSUltima ;
      private decimal[] P003G4_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P003G4_A684ContagemResultado_PFBFSUltima ;
      private int[] P003G4_A584ContagemResultado_ContadorFM ;
      private DateTime[] P003G4_A566ContagemResultado_DataUltCnt ;
      private bool[] P003G4_n566ContagemResultado_DataUltCnt ;
      private short[] P003G4_A531ContagemResultado_StatusUltCnt ;
      private int[] P003G4_A456ContagemResultado_Codigo ;
      private DateTime[] P003G4_A473ContagemResultado_DataCnt ;
      private String[] P003G4_A511ContagemResultado_HoraCnt ;
      private int[] P003G6_A1553ContagemResultado_CntSrvCod ;
      private bool[] P003G6_n1553ContagemResultado_CntSrvCod ;
      private int[] P003G6_A52Contratada_AreaTrabalhoCod ;
      private bool[] P003G6_n52Contratada_AreaTrabalhoCod ;
      private String[] P003G6_A484ContagemResultado_StatusDmn ;
      private bool[] P003G6_n484ContagemResultado_StatusDmn ;
      private int[] P003G6_A1452ContagemResultado_SS ;
      private bool[] P003G6_n1452ContagemResultado_SS ;
      private decimal[] P003G6_A512ContagemResultado_ValorPF ;
      private bool[] P003G6_n512ContagemResultado_ValorPF ;
      private String[] P003G6_A801ContagemResultado_ServicoSigla ;
      private bool[] P003G6_n801ContagemResultado_ServicoSigla ;
      private String[] P003G6_A515ContagemResultado_SistemaCoord ;
      private bool[] P003G6_n515ContagemResultado_SistemaCoord ;
      private String[] P003G6_A803ContagemResultado_ContratadaSigla ;
      private bool[] P003G6_n803ContagemResultado_ContratadaSigla ;
      private int[] P003G6_A1603ContagemResultado_CntCod ;
      private bool[] P003G6_n1603ContagemResultado_CntCod ;
      private String[] P003G6_A494ContagemResultado_Descricao ;
      private bool[] P003G6_n494ContagemResultado_Descricao ;
      private String[] P003G6_A1046ContagemResultado_Agrupador ;
      private bool[] P003G6_n1046ContagemResultado_Agrupador ;
      private int[] P003G6_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P003G6_n468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P003G6_A598ContagemResultado_Baseline ;
      private bool[] P003G6_n598ContagemResultado_Baseline ;
      private int[] P003G6_A764ContagemResultado_ServicoGrupo ;
      private bool[] P003G6_n764ContagemResultado_ServicoGrupo ;
      private int[] P003G6_A489ContagemResultado_SistemaCod ;
      private bool[] P003G6_n489ContagemResultado_SistemaCod ;
      private int[] P003G6_A508ContagemResultado_Owner ;
      private int[] P003G6_A601ContagemResultado_Servico ;
      private bool[] P003G6_n601ContagemResultado_Servico ;
      private DateTime[] P003G6_A1351ContagemResultado_DataPrevista ;
      private bool[] P003G6_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P003G6_A471ContagemResultado_DataDmn ;
      private String[] P003G6_A493ContagemResultado_DemandaFM ;
      private bool[] P003G6_n493ContagemResultado_DemandaFM ;
      private String[] P003G6_A457ContagemResultado_Demanda ;
      private bool[] P003G6_n457ContagemResultado_Demanda ;
      private decimal[] P003G6_A1854ContagemResultado_VlrCnc ;
      private bool[] P003G6_n1854ContagemResultado_VlrCnc ;
      private int[] P003G6_A490ContagemResultado_ContratadaCod ;
      private bool[] P003G6_n490ContagemResultado_ContratadaCod ;
      private String[] P003G6_A1623ContagemResultado_CntSrvMmn ;
      private bool[] P003G6_n1623ContagemResultado_CntSrvMmn ;
      private decimal[] P003G6_A1855ContagemResultado_PFCnc ;
      private bool[] P003G6_n1855ContagemResultado_PFCnc ;
      private decimal[] P003G6_A683ContagemResultado_PFLFMUltima ;
      private decimal[] P003G6_A685ContagemResultado_PFLFSUltima ;
      private decimal[] P003G6_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P003G6_A684ContagemResultado_PFBFSUltima ;
      private int[] P003G6_A584ContagemResultado_ContadorFM ;
      private DateTime[] P003G6_A566ContagemResultado_DataUltCnt ;
      private bool[] P003G6_n566ContagemResultado_DataUltCnt ;
      private short[] P003G6_A531ContagemResultado_StatusUltCnt ;
      private int[] P003G6_A456ContagemResultado_Codigo ;
      private DateTime[] P003G6_A473ContagemResultado_DataCnt ;
      private String[] P003G6_A511ContagemResultado_HoraCnt ;
      private int[] P003G7_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P003G7_n597ContagemResultado_LoteAceiteCod ;
      private int[] P003G7_A1452ContagemResultado_SS ;
      private bool[] P003G7_n1452ContagemResultado_SS ;
      private String[] P003G7_A457ContagemResultado_Demanda ;
      private bool[] P003G7_n457ContagemResultado_Demanda ;
      private int[] P003G7_A489ContagemResultado_SistemaCod ;
      private bool[] P003G7_n489ContagemResultado_SistemaCod ;
      private String[] P003G7_A509ContagemrResultado_SistemaSigla ;
      private bool[] P003G7_n509ContagemrResultado_SistemaSigla ;
      private String[] P003G7_A515ContagemResultado_SistemaCoord ;
      private bool[] P003G7_n515ContagemResultado_SistemaCoord ;
      private decimal[] P003G7_A512ContagemResultado_ValorPF ;
      private bool[] P003G7_n512ContagemResultado_ValorPF ;
      private int[] P003G7_A456ContagemResultado_Codigo ;
      private int[] P003G8_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P003G8_n597ContagemResultado_LoteAceiteCod ;
      private int[] P003G8_A1452ContagemResultado_SS ;
      private bool[] P003G8_n1452ContagemResultado_SS ;
      private String[] P003G8_A457ContagemResultado_Demanda ;
      private bool[] P003G8_n457ContagemResultado_Demanda ;
      private int[] P003G8_A489ContagemResultado_SistemaCod ;
      private bool[] P003G8_n489ContagemResultado_SistemaCod ;
      private String[] P003G8_A509ContagemrResultado_SistemaSigla ;
      private bool[] P003G8_n509ContagemrResultado_SistemaSigla ;
      private String[] P003G8_A515ContagemResultado_SistemaCoord ;
      private bool[] P003G8_n515ContagemResultado_SistemaCoord ;
      private decimal[] P003G8_A512ContagemResultado_ValorPF ;
      private bool[] P003G8_n512ContagemResultado_ValorPF ;
      private int[] P003G8_A456ContagemResultado_Codigo ;
      private DateTime[] P003G11_A40000GXC1 ;
      private DateTime[] P003G11_A40001GXC2 ;
      private int[] P003G12_A489ContagemResultado_SistemaCod ;
      private bool[] P003G12_n489ContagemResultado_SistemaCod ;
      private int[] P003G12_A1553ContagemResultado_CntSrvCod ;
      private bool[] P003G12_n1553ContagemResultado_CntSrvCod ;
      private int[] P003G12_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P003G12_n597ContagemResultado_LoteAceiteCod ;
      private int[] P003G12_A1452ContagemResultado_SS ;
      private bool[] P003G12_n1452ContagemResultado_SS ;
      private String[] P003G12_A1623ContagemResultado_CntSrvMmn ;
      private bool[] P003G12_n1623ContagemResultado_CntSrvMmn ;
      private String[] P003G12_A484ContagemResultado_StatusDmn ;
      private bool[] P003G12_n484ContagemResultado_StatusDmn ;
      private decimal[] P003G12_A1855ContagemResultado_PFCnc ;
      private bool[] P003G12_n1855ContagemResultado_PFCnc ;
      private String[] P003G12_A509ContagemrResultado_SistemaSigla ;
      private bool[] P003G12_n509ContagemrResultado_SistemaSigla ;
      private int[] P003G12_A456ContagemResultado_Codigo ;
      private int[] P003G15_A1553ContagemResultado_CntSrvCod ;
      private bool[] P003G15_n1553ContagemResultado_CntSrvCod ;
      private int[] P003G15_A1603ContagemResultado_CntCod ;
      private bool[] P003G15_n1603ContagemResultado_CntCod ;
      private int[] P003G15_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P003G15_n597ContagemResultado_LoteAceiteCod ;
      private int[] P003G15_A456ContagemResultado_Codigo ;
      private String[] P003G15_A1612ContagemResultado_CntNum ;
      private bool[] P003G15_n1612ContagemResultado_CntNum ;
      private DateTime[] P003G15_A1624ContagemResultado_DatVgnInc ;
      private bool[] P003G15_n1624ContagemResultado_DatVgnInc ;
      private int[] P003G15_A490ContagemResultado_ContratadaCod ;
      private bool[] P003G15_n490ContagemResultado_ContratadaCod ;
      private DateTime[] P003G15_A40002GXC3 ;
      private bool[] P003G15_n40002GXC3 ;
      private int[] P003G16_A40Contratada_PessoaCod ;
      private int[] P003G16_A503Pessoa_MunicipioCod ;
      private bool[] P003G16_n503Pessoa_MunicipioCod ;
      private int[] P003G16_A52Contratada_AreaTrabalhoCod ;
      private bool[] P003G16_n52Contratada_AreaTrabalhoCod ;
      private int[] P003G16_A29Contratante_Codigo ;
      private bool[] P003G16_n29Contratante_Codigo ;
      private int[] P003G16_A39Contratada_Codigo ;
      private String[] P003G16_A41Contratada_PessoaNom ;
      private bool[] P003G16_n41Contratada_PessoaNom ;
      private int[] P003G16_A524Contratada_OS ;
      private bool[] P003G16_n524Contratada_OS ;
      private String[] P003G16_A42Contratada_PessoaCNPJ ;
      private bool[] P003G16_n42Contratada_PessoaCNPJ ;
      private String[] P003G16_A518Pessoa_IE ;
      private bool[] P003G16_n518Pessoa_IE ;
      private String[] P003G16_A519Pessoa_Endereco ;
      private bool[] P003G16_n519Pessoa_Endereco ;
      private String[] P003G16_A521Pessoa_CEP ;
      private bool[] P003G16_n521Pessoa_CEP ;
      private String[] P003G16_A522Pessoa_Telefone ;
      private bool[] P003G16_n522Pessoa_Telefone ;
      private String[] P003G16_A523Pessoa_Fax ;
      private bool[] P003G16_n523Pessoa_Fax ;
      private String[] P003G16_A520Pessoa_UF ;
      private bool[] P003G16_n520Pessoa_UF ;
      private String[] P003G16_A26Municipio_Nome ;
      private bool[] P003G16_n26Municipio_Nome ;
      private bool[] P003G16_A593Contratante_OSAutomatica ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV471ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV259TFContagemResultado_StatusDmn_Sels ;
      private wwpbaseobjects.SdtWWPGridState AV75GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV76GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPContext AV10WWPContext ;
   }

   public class arel_contagemos__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P003G4( IGxContext context ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV471ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels ,
                                             int AV10WWPContext_gxTpr_Contratada_codigo ,
                                             String AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 ,
                                             String AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 ,
                                             short AV330ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 ,
                                             String AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 ,
                                             DateTime AV335ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 ,
                                             DateTime AV336ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 ,
                                             DateTime AV337ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 ,
                                             DateTime AV338ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 ,
                                             int AV339ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 ,
                                             int AV341ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 ,
                                             int AV342ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 ,
                                             int AV327ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod ,
                                             int AV343ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 ,
                                             String AV344ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 ,
                                             int AV345ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 ,
                                             String AV348ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 ,
                                             String AV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 ,
                                             int AV350ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 ,
                                             int AV352ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 ,
                                             bool AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 ,
                                             String AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 ,
                                             String AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 ,
                                             short AV355ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 ,
                                             String AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 ,
                                             DateTime AV360ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 ,
                                             DateTime AV361ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 ,
                                             DateTime AV362ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 ,
                                             DateTime AV363ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 ,
                                             int AV364ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 ,
                                             int AV366ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 ,
                                             int AV367ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 ,
                                             int AV368ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 ,
                                             String AV369ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 ,
                                             int AV370ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 ,
                                             String AV373ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 ,
                                             String AV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 ,
                                             int AV375ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 ,
                                             int AV377ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 ,
                                             bool AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 ,
                                             String AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 ,
                                             String AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 ,
                                             short AV380ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 ,
                                             String AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 ,
                                             DateTime AV385ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 ,
                                             DateTime AV386ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 ,
                                             DateTime AV387ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 ,
                                             DateTime AV388ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 ,
                                             int AV389ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 ,
                                             int AV391ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 ,
                                             int AV392ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 ,
                                             int AV393ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 ,
                                             String AV394ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 ,
                                             int AV395ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 ,
                                             String AV398ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 ,
                                             String AV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 ,
                                             int AV400ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 ,
                                             int AV402ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 ,
                                             bool AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 ,
                                             String AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 ,
                                             String AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 ,
                                             short AV405ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 ,
                                             String AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 ,
                                             DateTime AV410ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 ,
                                             DateTime AV411ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 ,
                                             DateTime AV412ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 ,
                                             DateTime AV413ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 ,
                                             int AV414ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 ,
                                             int AV416ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 ,
                                             int AV417ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 ,
                                             int AV418ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 ,
                                             String AV419ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 ,
                                             int AV420ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 ,
                                             String AV423ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 ,
                                             String AV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 ,
                                             int AV425ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 ,
                                             int AV427ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 ,
                                             bool AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 ,
                                             String AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 ,
                                             String AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 ,
                                             short AV430ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 ,
                                             String AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 ,
                                             DateTime AV435ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 ,
                                             DateTime AV436ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 ,
                                             DateTime AV437ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 ,
                                             DateTime AV438ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 ,
                                             int AV439ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 ,
                                             int AV441ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 ,
                                             int AV442ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 ,
                                             int AV443ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 ,
                                             String AV444ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 ,
                                             int AV445ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 ,
                                             String AV448ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 ,
                                             String AV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 ,
                                             int AV450ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 ,
                                             int AV452ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 ,
                                             String AV454ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel ,
                                             String AV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador ,
                                             String AV456ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel ,
                                             String AV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm ,
                                             String AV458ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel ,
                                             String AV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda ,
                                             String AV460ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel ,
                                             String AV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao ,
                                             DateTime AV461ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista ,
                                             DateTime AV462ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to ,
                                             DateTime AV463ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn ,
                                             DateTime AV464ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to ,
                                             String AV468ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel ,
                                             String AV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla ,
                                             String AV470ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel ,
                                             String AV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord ,
                                             int AV471ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels_Count ,
                                             short AV472ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel ,
                                             String AV474ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel ,
                                             String AV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla ,
                                             decimal AV477ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf ,
                                             decimal AV478ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A1351ContagemResultado_DataPrevista ,
                                             int A601ContagemResultado_Servico ,
                                             int A489ContagemResultado_SistemaCod ,
                                             int A764ContagemResultado_ServicoGrupo ,
                                             bool A598ContagemResultado_Baseline ,
                                             int A468ContagemResultado_NaoCnfDmnCod ,
                                             String A1046ContagemResultado_Agrupador ,
                                             String A494ContagemResultado_Descricao ,
                                             int A508ContagemResultado_Owner ,
                                             int A1603ContagemResultado_CntCod ,
                                             String A803ContagemResultado_ContratadaSigla ,
                                             String A515ContagemResultado_SistemaCoord ,
                                             String A801ContagemResultado_ServicoSigla ,
                                             decimal A512ContagemResultado_ValorPF ,
                                             short A531ContagemResultado_StatusUltCnt ,
                                             decimal A1854ContagemResultado_VlrCnc ,
                                             DateTime AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             DateTime AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 ,
                                             int AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 ,
                                             int A584ContagemResultado_ContadorFM ,
                                             decimal A684ContagemResultado_PFBFSUltima ,
                                             decimal A682ContagemResultado_PFBFMUltima ,
                                             decimal A685ContagemResultado_PFLFSUltima ,
                                             decimal A683ContagemResultado_PFLFMUltima ,
                                             String AV351ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 ,
                                             bool A1802ContagemResultado_TemPndHmlg ,
                                             DateTime AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 ,
                                             DateTime AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 ,
                                             int AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 ,
                                             String AV376ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 ,
                                             DateTime AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 ,
                                             DateTime AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 ,
                                             int AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 ,
                                             String AV401ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 ,
                                             DateTime AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 ,
                                             DateTime AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 ,
                                             int AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 ,
                                             String AV426ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 ,
                                             DateTime AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 ,
                                             DateTime AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 ,
                                             int AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 ,
                                             String AV451ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 ,
                                             DateTime AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt ,
                                             DateTime AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to ,
                                             decimal AV475ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ,
                                             decimal A574ContagemResultado_PFFinal ,
                                             decimal AV476ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV10WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [211] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T6.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_SS], T2.[ContagemResultado_Demanda], T2.[ContagemResultado_ValorPF], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T5.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T6.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T3.[Contrato_Codigo] AS ContagemResultado_CntCod, T2.[ContagemResultado_Descricao], T2.[ContagemResultado_Agrupador], T2.[ContagemResultado_NaoCnfDmnCod], T2.[ContagemResultado_Baseline], T4.[ServicoGrupo_Codigo] AS ContagemResultado_ServicoGrupo, T2.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T2.[ContagemResultado_Owner], T3.[Servico_Codigo] AS ContagemResultado_Servico, T2.[ContagemResultado_DataPrevista], T2.[ContagemResultado_DataDmn], T2.[ContagemResultado_DemandaFM], T2.[ContagemResultado_VlrCnc], T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T5.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T7.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM";
         scmdbuf = scmdbuf + " (((((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) LEFT JOIN [Sistema] T5 WITH (NOLOCK) ON T5.[Sistema_Codigo] = T2.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T7 ON T7.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T2.[ContagemResultado_StatusDmn] = 'H')";
         scmdbuf = scmdbuf + " and (Not ( Not ( @AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 = 'N' or @AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 = 'N' or @AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 = 'N' or @AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 = 'N' or @AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 = 'N')) or ( COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) = 5 or T2.[ContagemResultado_VlrCnc] > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (@AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 <> 'CONTAGEMRESULTADO_PFBFM' or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (@AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 <> 'CONTAGEMRESULTADO_PFBFS' or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3)))";
         scmdbuf = scmdbuf + " and (Not ( @AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4))";
         scmdbuf = scmdbuf + " and (Not ( @AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4))";
         scmdbuf = scmdbuf + " and (Not ( @AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4)))";
         scmdbuf = scmdbuf + " and (Not ( @AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5))";
         scmdbuf = scmdbuf + " and (Not ( @AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5))";
         scmdbuf = scmdbuf + " and (Not ( @AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5)))";
         scmdbuf = scmdbuf + " and (Not ( @AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and ((@AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and (T6.[Contratada_AreaTrabalhoCod] = @AV10WWPC_1Areatrabalho_codigo)";
         if ( AV10WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV10WWPC_2Contratada_codigo)";
         }
         else
         {
            GXv_int3[90] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1)) && ! ( StringUtil.StrCmp(AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int3[91] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T2.[ContagemResultado_StatusDmn] = 'P' or T2.[ContagemResultado_StatusDmn] = 'L' or T2.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV330ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] = @AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[92] = 1;
            GXv_int3[93] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV330ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] like @lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[94] = 1;
            GXv_int3[95] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV330ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] like '%' + @lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[96] = 1;
            GXv_int3[97] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV335ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV335ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int3[98] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV336ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV336ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int3[99] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV337ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] >= @AV337ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1)";
         }
         else
         {
            GXv_int3[100] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV338ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV338ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1))";
         }
         else
         {
            GXv_int3[101] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV339ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV339ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int3[102] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV341ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV341ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int3[103] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV342ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 > 0 ) && ( AV327ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV342ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int3[104] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV343ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV343ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1)";
         }
         else
         {
            GXv_int3[105] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV344ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV344ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV345ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV345ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1)";
         }
         else
         {
            GXv_int3[106] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV348ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV348ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int3[107] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 + '%')";
         }
         else
         {
            GXv_int3[108] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV350ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Owner] = @AV350ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1)";
         }
         else
         {
            GXv_int3[109] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV352ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Codigo] = @AV352ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1)";
         }
         else
         {
            GXv_int3[110] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2)) && ! ( StringUtil.StrCmp(AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int3[111] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T2.[ContagemResultado_StatusDmn] = 'P' or T2.[ContagemResultado_StatusDmn] = 'L' or T2.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV355ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] = @AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[112] = 1;
            GXv_int3[113] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV355ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] like @lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[114] = 1;
            GXv_int3[115] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV355ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] like '%' + @lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[116] = 1;
            GXv_int3[117] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV360ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV360ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int3[118] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV361ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV361ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int3[119] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV362ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] >= @AV362ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2)";
         }
         else
         {
            GXv_int3[120] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV363ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV363ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2))";
         }
         else
         {
            GXv_int3[121] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV364ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV364ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int3[122] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV366ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV366ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int3[123] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV367ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 > 0 ) && ( AV327ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV367ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int3[124] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV368ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV368ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2)";
         }
         else
         {
            GXv_int3[125] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV369ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV369ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV370ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV370ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2)";
         }
         else
         {
            GXv_int3[126] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV373ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV373ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int3[127] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 + '%')";
         }
         else
         {
            GXv_int3[128] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV375ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Owner] = @AV375ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2)";
         }
         else
         {
            GXv_int3[129] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV377ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Codigo] = @AV377ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2)";
         }
         else
         {
            GXv_int3[130] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3)) && ! ( StringUtil.StrCmp(AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int3[131] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T2.[ContagemResultado_StatusDmn] = 'P' or T2.[ContagemResultado_StatusDmn] = 'L' or T2.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV380ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] = @AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[132] = 1;
            GXv_int3[133] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV380ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] like @lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[134] = 1;
            GXv_int3[135] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV380ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] like '%' + @lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[136] = 1;
            GXv_int3[137] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV385ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV385ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int3[138] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV386ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV386ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int3[139] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV387ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] >= @AV387ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3)";
         }
         else
         {
            GXv_int3[140] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV388ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV388ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3))";
         }
         else
         {
            GXv_int3[141] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV389ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV389ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int3[142] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV391ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV391ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int3[143] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV392ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 > 0 ) && ( AV327ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV392ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int3[144] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV393ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV393ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3)";
         }
         else
         {
            GXv_int3[145] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV394ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV394ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV395ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV395ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3)";
         }
         else
         {
            GXv_int3[146] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV398ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV398ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int3[147] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 + '%')";
         }
         else
         {
            GXv_int3[148] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV400ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Owner] = @AV400ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3)";
         }
         else
         {
            GXv_int3[149] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV402ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Codigo] = @AV402ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3)";
         }
         else
         {
            GXv_int3[150] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4)) && ! ( StringUtil.StrCmp(AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4)";
         }
         else
         {
            GXv_int3[151] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T2.[ContagemResultado_StatusDmn] = 'P' or T2.[ContagemResultado_StatusDmn] = 'L' or T2.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV405ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 or T2.[ContagemResultado_DemandaFM] = @AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int3[152] = 1;
            GXv_int3[153] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV405ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 or T2.[ContagemResultado_DemandaFM] like @lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int3[154] = 1;
            GXv_int3[155] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV405ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 or T2.[ContagemResultado_DemandaFM] like '%' + @lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int3[156] = 1;
            GXv_int3[157] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV410ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV410ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4)";
         }
         else
         {
            GXv_int3[158] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV411ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV411ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4)";
         }
         else
         {
            GXv_int3[159] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV412ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] >= @AV412ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4)";
         }
         else
         {
            GXv_int3[160] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV413ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV413ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4))";
         }
         else
         {
            GXv_int3[161] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV414ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV414ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4)";
         }
         else
         {
            GXv_int3[162] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV416ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV416ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4)";
         }
         else
         {
            GXv_int3[163] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV417ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 > 0 ) && ( AV327ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV417ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4)";
         }
         else
         {
            GXv_int3[164] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV418ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV418ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4)";
         }
         else
         {
            GXv_int3[165] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV419ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV419ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV420ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV420ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4)";
         }
         else
         {
            GXv_int3[166] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV423ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV423ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4)";
         }
         else
         {
            GXv_int3[167] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 + '%')";
         }
         else
         {
            GXv_int3[168] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV425ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Owner] = @AV425ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4)";
         }
         else
         {
            GXv_int3[169] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV427ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Codigo] = @AV427ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4)";
         }
         else
         {
            GXv_int3[170] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5)) && ! ( StringUtil.StrCmp(AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5)";
         }
         else
         {
            GXv_int3[171] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T2.[ContagemResultado_StatusDmn] = 'P' or T2.[ContagemResultado_StatusDmn] = 'L' or T2.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV430ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 or T2.[ContagemResultado_DemandaFM] = @AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int3[172] = 1;
            GXv_int3[173] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV430ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 or T2.[ContagemResultado_DemandaFM] like @lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int3[174] = 1;
            GXv_int3[175] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV430ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 or T2.[ContagemResultado_DemandaFM] like '%' + @lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int3[176] = 1;
            GXv_int3[177] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV435ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV435ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5)";
         }
         else
         {
            GXv_int3[178] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV436ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV436ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5)";
         }
         else
         {
            GXv_int3[179] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV437ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] >= @AV437ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5)";
         }
         else
         {
            GXv_int3[180] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV438ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV438ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5))";
         }
         else
         {
            GXv_int3[181] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV439ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV439ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5)";
         }
         else
         {
            GXv_int3[182] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV441ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV441ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5)";
         }
         else
         {
            GXv_int3[183] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV442ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 > 0 ) && ( AV327ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV442ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5)";
         }
         else
         {
            GXv_int3[184] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV443ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV443ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5)";
         }
         else
         {
            GXv_int3[185] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV444ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV444ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV445ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV445ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5)";
         }
         else
         {
            GXv_int3[186] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV448ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV448ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5)";
         }
         else
         {
            GXv_int3[187] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 + '%')";
         }
         else
         {
            GXv_int3[188] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV450ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Owner] = @AV450ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5)";
         }
         else
         {
            GXv_int3[189] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV452ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Codigo] = @AV452ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5)";
         }
         else
         {
            GXv_int3[190] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV454ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like @lV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador)";
         }
         else
         {
            GXv_int3[191] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV454ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV454ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel)";
         }
         else
         {
            GXv_int3[192] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV456ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm)";
         }
         else
         {
            GXv_int3[193] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV456ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV456ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel)";
         }
         else
         {
            GXv_int3[194] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV458ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda)";
         }
         else
         {
            GXv_int3[195] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV458ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV458ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel)";
         }
         else
         {
            GXv_int3[196] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV460ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like @lV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao)";
         }
         else
         {
            GXv_int3[197] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV460ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] = @AV460ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel)";
         }
         else
         {
            GXv_int3[198] = 1;
         }
         if ( ! (DateTime.MinValue==AV461ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] >= @AV461ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista)";
         }
         else
         {
            GXv_int3[199] = 1;
         }
         if ( ! (DateTime.MinValue==AV462ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] <= @AV462ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to)";
         }
         else
         {
            GXv_int3[200] = 1;
         }
         if ( ! (DateTime.MinValue==AV463ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV463ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int3[201] = 1;
         }
         if ( ! (DateTime.MinValue==AV464ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV464ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int3[202] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV468ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] like @lV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla)";
         }
         else
         {
            GXv_int3[203] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV468ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] = @AV468ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel)";
         }
         else
         {
            GXv_int3[204] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV470ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Coordenacao] like @lV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord)";
         }
         else
         {
            GXv_int3[205] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV470ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Coordenacao] = @AV470ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel)";
         }
         else
         {
            GXv_int3[206] = 1;
         }
         if ( AV471ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV471ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels, "T2.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV472ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel == 1 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV472ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel == 2 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 0)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV474ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Sigla] like @lV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int3[207] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV474ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Sigla] = @AV474ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int3[208] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV477ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ValorPF] >= @AV477ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf)";
         }
         else
         {
            GXv_int3[209] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV478ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ValorPF] <= @AV478ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to)";
         }
         else
         {
            GXv_int3[210] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_SS], T2.[ContagemResultado_Demanda]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P003G6( IGxContext context ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV471ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels ,
                                             int AV10WWPContext_gxTpr_Contratada_codigo ,
                                             String AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 ,
                                             String AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 ,
                                             short AV330ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 ,
                                             String AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 ,
                                             DateTime AV335ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1 ,
                                             DateTime AV336ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1 ,
                                             DateTime AV337ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1 ,
                                             DateTime AV338ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1 ,
                                             int AV339ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1 ,
                                             int AV341ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1 ,
                                             int AV342ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 ,
                                             int AV327ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod ,
                                             int AV343ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1 ,
                                             String AV344ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1 ,
                                             int AV345ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1 ,
                                             String AV348ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1 ,
                                             String AV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 ,
                                             int AV350ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1 ,
                                             int AV352ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1 ,
                                             bool AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 ,
                                             String AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 ,
                                             String AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 ,
                                             short AV355ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 ,
                                             String AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 ,
                                             DateTime AV360ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2 ,
                                             DateTime AV361ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2 ,
                                             DateTime AV362ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2 ,
                                             DateTime AV363ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2 ,
                                             int AV364ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2 ,
                                             int AV366ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2 ,
                                             int AV367ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 ,
                                             int AV368ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2 ,
                                             String AV369ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2 ,
                                             int AV370ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2 ,
                                             String AV373ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2 ,
                                             String AV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 ,
                                             int AV375ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2 ,
                                             int AV377ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2 ,
                                             bool AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 ,
                                             String AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 ,
                                             String AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 ,
                                             short AV380ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 ,
                                             String AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 ,
                                             DateTime AV385ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3 ,
                                             DateTime AV386ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3 ,
                                             DateTime AV387ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3 ,
                                             DateTime AV388ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3 ,
                                             int AV389ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3 ,
                                             int AV391ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3 ,
                                             int AV392ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 ,
                                             int AV393ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3 ,
                                             String AV394ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3 ,
                                             int AV395ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3 ,
                                             String AV398ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3 ,
                                             String AV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 ,
                                             int AV400ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3 ,
                                             int AV402ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3 ,
                                             bool AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 ,
                                             String AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 ,
                                             String AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 ,
                                             short AV405ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 ,
                                             String AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 ,
                                             DateTime AV410ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4 ,
                                             DateTime AV411ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4 ,
                                             DateTime AV412ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4 ,
                                             DateTime AV413ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4 ,
                                             int AV414ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4 ,
                                             int AV416ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4 ,
                                             int AV417ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 ,
                                             int AV418ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4 ,
                                             String AV419ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4 ,
                                             int AV420ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4 ,
                                             String AV423ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4 ,
                                             String AV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 ,
                                             int AV425ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4 ,
                                             int AV427ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4 ,
                                             bool AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 ,
                                             String AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 ,
                                             String AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 ,
                                             short AV430ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 ,
                                             String AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 ,
                                             DateTime AV435ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5 ,
                                             DateTime AV436ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5 ,
                                             DateTime AV437ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5 ,
                                             DateTime AV438ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5 ,
                                             int AV439ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5 ,
                                             int AV441ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5 ,
                                             int AV442ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 ,
                                             int AV443ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5 ,
                                             String AV444ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5 ,
                                             int AV445ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5 ,
                                             String AV448ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5 ,
                                             String AV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 ,
                                             int AV450ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5 ,
                                             int AV452ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5 ,
                                             String AV454ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel ,
                                             String AV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador ,
                                             String AV456ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel ,
                                             String AV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm ,
                                             String AV458ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel ,
                                             String AV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda ,
                                             String AV460ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel ,
                                             String AV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao ,
                                             DateTime AV461ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista ,
                                             DateTime AV462ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to ,
                                             DateTime AV463ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn ,
                                             DateTime AV464ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to ,
                                             String AV468ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel ,
                                             String AV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla ,
                                             String AV470ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel ,
                                             String AV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord ,
                                             int AV471ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels_Count ,
                                             short AV472ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel ,
                                             String AV474ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel ,
                                             String AV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla ,
                                             decimal AV477ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf ,
                                             decimal AV478ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A1351ContagemResultado_DataPrevista ,
                                             int A601ContagemResultado_Servico ,
                                             int A489ContagemResultado_SistemaCod ,
                                             int A764ContagemResultado_ServicoGrupo ,
                                             bool A598ContagemResultado_Baseline ,
                                             int A468ContagemResultado_NaoCnfDmnCod ,
                                             String A1046ContagemResultado_Agrupador ,
                                             String A494ContagemResultado_Descricao ,
                                             int A508ContagemResultado_Owner ,
                                             int A1603ContagemResultado_CntCod ,
                                             String A803ContagemResultado_ContratadaSigla ,
                                             String A515ContagemResultado_SistemaCoord ,
                                             String A801ContagemResultado_ServicoSigla ,
                                             decimal A512ContagemResultado_ValorPF ,
                                             short A531ContagemResultado_StatusUltCnt ,
                                             decimal A1854ContagemResultado_VlrCnc ,
                                             DateTime AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             DateTime AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 ,
                                             int AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 ,
                                             int A584ContagemResultado_ContadorFM ,
                                             decimal A684ContagemResultado_PFBFSUltima ,
                                             decimal A682ContagemResultado_PFBFMUltima ,
                                             decimal A685ContagemResultado_PFLFSUltima ,
                                             decimal A683ContagemResultado_PFLFMUltima ,
                                             String AV351ExtraWWContagemResultadoExtraSelectionDS_25_Contagemresultado_tempndhmlg1 ,
                                             bool A1802ContagemResultado_TemPndHmlg ,
                                             DateTime AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 ,
                                             DateTime AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 ,
                                             int AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 ,
                                             String AV376ExtraWWContagemResultadoExtraSelectionDS_50_Contagemresultado_tempndhmlg2 ,
                                             DateTime AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 ,
                                             DateTime AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 ,
                                             int AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 ,
                                             String AV401ExtraWWContagemResultadoExtraSelectionDS_75_Contagemresultado_tempndhmlg3 ,
                                             DateTime AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 ,
                                             DateTime AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 ,
                                             int AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 ,
                                             String AV426ExtraWWContagemResultadoExtraSelectionDS_100_Contagemresultado_tempndhmlg4 ,
                                             DateTime AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 ,
                                             DateTime AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 ,
                                             int AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 ,
                                             String AV451ExtraWWContagemResultadoExtraSelectionDS_125_Contagemresultado_tempndhmlg5 ,
                                             DateTime AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt ,
                                             DateTime AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to ,
                                             decimal AV475ExtraWWContagemResultadoExtraSelectionDS_149_Tfcontagemresultado_pffinal ,
                                             decimal A574ContagemResultado_PFFinal ,
                                             decimal AV476ExtraWWContagemResultadoExtraSelectionDS_150_Tfcontagemresultado_pffinal_to ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV10WWPContext_gxTpr_Areatrabalho_codigo ,
                                             int AV224ContagemResultado_SS ,
                                             int A1452ContagemResultado_SS )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [212] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T6.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_SS], T2.[ContagemResultado_ValorPF], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T5.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T6.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T3.[Contrato_Codigo] AS ContagemResultado_CntCod, T2.[ContagemResultado_Descricao], T2.[ContagemResultado_Agrupador], T2.[ContagemResultado_NaoCnfDmnCod], T2.[ContagemResultado_Baseline], T4.[ServicoGrupo_Codigo] AS ContagemResultado_ServicoGrupo, T2.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T2.[ContagemResultado_Owner], T3.[Servico_Codigo] AS ContagemResultado_Servico, T2.[ContagemResultado_DataPrevista], T2.[ContagemResultado_DataDmn], T2.[ContagemResultado_DemandaFM], T2.[ContagemResultado_Demanda], T2.[ContagemResultado_VlrCnc], T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T3.[ContratoServicos_Momento] AS ContagemResultado_CntSrvMmn, T2.[ContagemResultado_PFCnc], COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T7.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataCnt],";
         scmdbuf = scmdbuf + " T1.[ContagemResultado_HoraCnt] FROM (((((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) LEFT JOIN [Sistema] T5 WITH (NOLOCK) ON T5.[Sistema_Codigo] = T2.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T7 ON T7.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T2.[ContagemResultado_StatusDmn] = 'H' and T2.[ContagemResultado_SS] = @AV224ContagemResultado_SS)";
         scmdbuf = scmdbuf + " and (Not ( Not ( @AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1 = 'N' or @AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2 = 'N' or @AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3 = 'N' or @AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4 = 'N' or @AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5 = 'N')) or ( COALESCE( T7.[ContagemResultado_StatusUltCnt], 0) = 5 or T2.[ContagemResultado_VlrCnc] > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (@AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 <> 'CONTAGEMRESULTADO_PFBFM' or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (@AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1 <> 'CONTAGEMRESULTADO_PFBFS' or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 = 1 and @AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3)))";
         scmdbuf = scmdbuf + " and (Not ( @AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 = 1 and @AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4))";
         scmdbuf = scmdbuf + " and (Not ( @AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4))";
         scmdbuf = scmdbuf + " and (Not ( @AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4)))";
         scmdbuf = scmdbuf + " and (Not ( @AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 = 1 and @AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5))";
         scmdbuf = scmdbuf + " and (Not ( @AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5))";
         scmdbuf = scmdbuf + " and (Not ( @AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 = convert(int, 0)))) or ( COALESCE( T7.[ContagemResultado_ContadorFM], 0) = @AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5 or ( (COALESCE( T7.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5)))";
         scmdbuf = scmdbuf + " and (Not ( @AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 = 1 and @AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and ((@AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T7.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and (T6.[Contratada_AreaTrabalhoCod] = @AV10WWPC_1Areatrabalho_codigo)";
         if ( AV10WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV10WWPC_2Contratada_codigo)";
         }
         else
         {
            GXv_int5[91] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1)) && ! ( StringUtil.StrCmp(AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int5[92] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T2.[ContagemResultado_StatusDmn] = 'P' or T2.[ContagemResultado_StatusDmn] = 'L' or T2.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV330ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] = @AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int5[93] = 1;
            GXv_int5[94] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV330ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] like @lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int5[95] = 1;
            GXv_int5[96] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV330ExtraWWContagemResultadoExtraSelectionDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] like '%' + @lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int5[97] = 1;
            GXv_int5[98] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV335ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV335ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int5[99] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV336ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV336ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int5[100] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV337ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] >= @AV337ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1)";
         }
         else
         {
            GXv_int5[101] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV338ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV338ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1))";
         }
         else
         {
            GXv_int5[102] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV339ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV339ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int5[103] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV341ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV341ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int5[104] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV342ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1 > 0 ) && ( AV327ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV342ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int5[105] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV343ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV343ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1)";
         }
         else
         {
            GXv_int5[106] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV344ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV344ExtraWWContagemResultadoExtraSelectionDS_18_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV345ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV345ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1)";
         }
         else
         {
            GXv_int5[107] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV348ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV348ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int5[108] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1 + '%')";
         }
         else
         {
            GXv_int5[109] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV350ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Owner] = @AV350ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1)";
         }
         else
         {
            GXv_int5[110] = 1;
         }
         if ( ( StringUtil.StrCmp(AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV352ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Codigo] = @AV352ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1)";
         }
         else
         {
            GXv_int5[111] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2)) && ! ( StringUtil.StrCmp(AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int5[112] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T2.[ContagemResultado_StatusDmn] = 'P' or T2.[ContagemResultado_StatusDmn] = 'L' or T2.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV355ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] = @AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int5[113] = 1;
            GXv_int5[114] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV355ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] like @lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int5[115] = 1;
            GXv_int5[116] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV355ExtraWWContagemResultadoExtraSelectionDS_29_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] like '%' + @lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int5[117] = 1;
            GXv_int5[118] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV360ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV360ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int5[119] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV361ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV361ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int5[120] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV362ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] >= @AV362ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2)";
         }
         else
         {
            GXv_int5[121] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV363ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV363ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2))";
         }
         else
         {
            GXv_int5[122] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV364ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV364ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int5[123] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV366ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV366ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int5[124] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV367ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2 > 0 ) && ( AV327ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV367ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int5[125] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV368ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV368ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2)";
         }
         else
         {
            GXv_int5[126] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV369ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV369ExtraWWContagemResultadoExtraSelectionDS_43_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV370ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV370ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2)";
         }
         else
         {
            GXv_int5[127] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV373ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV373ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int5[128] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2 + '%')";
         }
         else
         {
            GXv_int5[129] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV375ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Owner] = @AV375ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2)";
         }
         else
         {
            GXv_int5[130] = 1;
         }
         if ( AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV377ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Codigo] = @AV377ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2)";
         }
         else
         {
            GXv_int5[131] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3)) && ! ( StringUtil.StrCmp(AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int5[132] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T2.[ContagemResultado_StatusDmn] = 'P' or T2.[ContagemResultado_StatusDmn] = 'L' or T2.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV380ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] = @AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int5[133] = 1;
            GXv_int5[134] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV380ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] like @lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int5[135] = 1;
            GXv_int5[136] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV380ExtraWWContagemResultadoExtraSelectionDS_54_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] like '%' + @lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int5[137] = 1;
            GXv_int5[138] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV385ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV385ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int5[139] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV386ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV386ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int5[140] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV387ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] >= @AV387ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3)";
         }
         else
         {
            GXv_int5[141] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV388ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV388ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3))";
         }
         else
         {
            GXv_int5[142] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV389ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV389ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int5[143] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV391ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV391ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int5[144] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV392ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3 > 0 ) && ( AV327ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV392ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int5[145] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV393ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV393ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3)";
         }
         else
         {
            GXv_int5[146] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV394ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV394ExtraWWContagemResultadoExtraSelectionDS_68_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV395ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV395ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3)";
         }
         else
         {
            GXv_int5[147] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV398ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV398ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int5[148] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3 + '%')";
         }
         else
         {
            GXv_int5[149] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV400ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Owner] = @AV400ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3)";
         }
         else
         {
            GXv_int5[150] = 1;
         }
         if ( AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV402ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Codigo] = @AV402ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3)";
         }
         else
         {
            GXv_int5[151] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4)) && ! ( StringUtil.StrCmp(AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4)";
         }
         else
         {
            GXv_int5[152] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T2.[ContagemResultado_StatusDmn] = 'P' or T2.[ContagemResultado_StatusDmn] = 'L' or T2.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV405ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 or T2.[ContagemResultado_DemandaFM] = @AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int5[153] = 1;
            GXv_int5[154] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV405ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 or T2.[ContagemResultado_DemandaFM] like @lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int5[155] = 1;
            GXv_int5[156] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV405ExtraWWContagemResultadoExtraSelectionDS_79_Dynamicfiltersoperator4 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4 or T2.[ContagemResultado_DemandaFM] like '%' + @lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int5[157] = 1;
            GXv_int5[158] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV410ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV410ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4)";
         }
         else
         {
            GXv_int5[159] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV411ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV411ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4)";
         }
         else
         {
            GXv_int5[160] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV412ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] >= @AV412ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4)";
         }
         else
         {
            GXv_int5[161] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV413ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV413ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4))";
         }
         else
         {
            GXv_int5[162] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV414ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV414ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4)";
         }
         else
         {
            GXv_int5[163] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV416ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV416ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4)";
         }
         else
         {
            GXv_int5[164] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV417ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4 > 0 ) && ( AV327ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV417ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4)";
         }
         else
         {
            GXv_int5[165] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV418ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV418ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4)";
         }
         else
         {
            GXv_int5[166] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV419ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV419ExtraWWContagemResultadoExtraSelectionDS_93_Contagemresultado_baseline4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV420ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV420ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4)";
         }
         else
         {
            GXv_int5[167] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV423ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV423ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4)";
         }
         else
         {
            GXv_int5[168] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4 + '%')";
         }
         else
         {
            GXv_int5[169] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV425ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Owner] = @AV425ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4)";
         }
         else
         {
            GXv_int5[170] = 1;
         }
         if ( AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV427ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Codigo] = @AV427ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4)";
         }
         else
         {
            GXv_int5[171] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5)) && ! ( StringUtil.StrCmp(AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5)";
         }
         else
         {
            GXv_int5[172] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T2.[ContagemResultado_StatusDmn] = 'P' or T2.[ContagemResultado_StatusDmn] = 'L' or T2.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV430ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 or T2.[ContagemResultado_DemandaFM] = @AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int5[173] = 1;
            GXv_int5[174] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV430ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 or T2.[ContagemResultado_DemandaFM] like @lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int5[175] = 1;
            GXv_int5[176] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV430ExtraWWContagemResultadoExtraSelectionDS_104_Dynamicfiltersoperator5 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5 or T2.[ContagemResultado_DemandaFM] like '%' + @lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int5[177] = 1;
            GXv_int5[178] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV435ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV435ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5)";
         }
         else
         {
            GXv_int5[179] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV436ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV436ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5)";
         }
         else
         {
            GXv_int5[180] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV437ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] >= @AV437ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5)";
         }
         else
         {
            GXv_int5[181] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV438ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV438ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5))";
         }
         else
         {
            GXv_int5[182] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV439ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV439ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5)";
         }
         else
         {
            GXv_int5[183] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV441ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV441ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5)";
         }
         else
         {
            GXv_int5[184] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV442ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5 > 0 ) && ( AV327ExtraWWContagemResultadoExtraSelectionDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV442ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5)";
         }
         else
         {
            GXv_int5[185] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV443ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5) ) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV443ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5)";
         }
         else
         {
            GXv_int5[186] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV444ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV444ExtraWWContagemResultadoExtraSelectionDS_118_Contagemresultado_baseline5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV445ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV445ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5)";
         }
         else
         {
            GXv_int5[187] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV448ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV448ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5)";
         }
         else
         {
            GXv_int5[188] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5 + '%')";
         }
         else
         {
            GXv_int5[189] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV450ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Owner] = @AV450ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5)";
         }
         else
         {
            GXv_int5[190] = 1;
         }
         if ( AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV452ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Codigo] = @AV452ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5)";
         }
         else
         {
            GXv_int5[191] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV454ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like @lV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador)";
         }
         else
         {
            GXv_int5[192] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV454ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV454ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel)";
         }
         else
         {
            GXv_int5[193] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV456ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm)";
         }
         else
         {
            GXv_int5[194] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV456ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV456ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel)";
         }
         else
         {
            GXv_int5[195] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV458ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda)";
         }
         else
         {
            GXv_int5[196] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV458ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV458ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel)";
         }
         else
         {
            GXv_int5[197] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV460ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like @lV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao)";
         }
         else
         {
            GXv_int5[198] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV460ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] = @AV460ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel)";
         }
         else
         {
            GXv_int5[199] = 1;
         }
         if ( ! (DateTime.MinValue==AV461ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] >= @AV461ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista)";
         }
         else
         {
            GXv_int5[200] = 1;
         }
         if ( ! (DateTime.MinValue==AV462ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataPrevista] <= @AV462ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to)";
         }
         else
         {
            GXv_int5[201] = 1;
         }
         if ( ! (DateTime.MinValue==AV463ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV463ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int5[202] = 1;
         }
         if ( ! (DateTime.MinValue==AV464ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV464ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int5[203] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV468ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] like @lV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla)";
         }
         else
         {
            GXv_int5[204] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV468ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] = @AV468ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel)";
         }
         else
         {
            GXv_int5[205] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV470ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Coordenacao] like @lV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord)";
         }
         else
         {
            GXv_int5[206] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV470ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Coordenacao] = @AV470ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel)";
         }
         else
         {
            GXv_int5[207] = 1;
         }
         if ( AV471ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV471ExtraWWContagemResultadoExtraSelectionDS_145_Tfcontagemresultado_statusdmn_sels, "T2.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV472ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel == 1 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV472ExtraWWContagemResultadoExtraSelectionDS_146_Tfcontagemresultado_baseline_sel == 2 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 0)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV474ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Sigla] like @lV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int5[208] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV474ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Sigla] = @AV474ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int5[209] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV477ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ValorPF] >= @AV477ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf)";
         }
         else
         {
            GXv_int5[210] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV478ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ValorPF] <= @AV478ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to)";
         }
         else
         {
            GXv_int5[211] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_SS], T2.[ContagemResultado_Demanda]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P003G15( IGxContext context ,
                                              int AV8ContagemResultado_LoteAceite ,
                                              int A456ContagemResultado_Codigo ,
                                              int AV230Codigo ,
                                              int A597ContagemResultado_LoteAceiteCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [2] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_Codigo], T3.[Contrato_Numero] AS ContagemResultado_CntNum, T3.[Contrato_DataVigenciaInicio] AS ContagemResultado_DatVgnInc, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, COALESCE( T4.[GXC3], convert( DATETIME, '17530101', 112 )) AS GXC3 FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN (SELECT T5.[ContratoTermoAditivo_DataInicio] AS GXC3, T7.[ContagemResultado_Codigo], T5.[ContratoTermoAditivo_Codigo], T6.[GXC7] AS GXC7, T5.[Contrato_Codigo], T8.[Contrato_Codigo] AS ContagemResultado_CntCod FROM [ContratoTermoAditivo] T5 WITH (NOLOCK),  (([ContagemResultado] T7 WITH (NOLOCK) INNER JOIN (SELECT MAX(T9.[ContratoTermoAditivo_Codigo]) AS GXC7, T10.[ContagemResultado_Codigo] FROM [ContratoTermoAditivo] T9 WITH (NOLOCK),  ([ContagemResultado] T10 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T11 WITH (NOLOCK) ON T11.[ContratoServicos_Codigo] = T10.[ContagemResultado_CntSrvCod]) WHERE (T11.[ContratoServicos_Codigo] = T10.[ContagemResultado_CntSrvCod]) AND (T9.[Contrato_Codigo] = T11.[Contrato_Codigo]) GROUP BY T10.[ContagemResultado_Codigo] ) T6 ON T6.[ContagemResultado_Codigo] = T7.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T8 WITH (NOLOCK) ON T8.[ContratoServicos_Codigo] = T7.[ContagemResultado_CntSrvCod]) WHERE (T6.[ContagemResultado_Codigo] = T7.[ContagemResultado_Codigo] AND T8.[ContratoServicos_Codigo] = T7.[ContagemResultado_CntSrvCod]) AND ((T5.[ContratoTermoAditivo_Codigo]";
         scmdbuf = scmdbuf + " = T6.[GXC7]) AND (T5.[Contrato_Codigo] = T8.[Contrato_Codigo])) ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         if ( (0==AV8ContagemResultado_LoteAceite) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = @AV230Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Codigo] = @AV230Codigo)";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ! (0==AV8ContagemResultado_LoteAceite) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_LoteAceiteCod] = @AV8ContagemResultado_LoteAceite)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_LoteAceiteCod] = @AV8ContagemResultado_LoteAceite)";
            }
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_P003G4(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (bool)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (short)dynConstraints[25] , (String)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (int)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (bool)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (short)dynConstraints[44] , (String)dynConstraints[45] , (DateTime)dynConstraints[46] , (DateTime)dynConstraints[47] , (DateTime)dynConstraints[48] , (DateTime)dynConstraints[49] , (int)dynConstraints[50] , (int)dynConstraints[51] , (int)dynConstraints[52] , (int)dynConstraints[53] , (String)dynConstraints[54] , (int)dynConstraints[55] , (String)dynConstraints[56] , (String)dynConstraints[57] , (int)dynConstraints[58] , (int)dynConstraints[59] , (bool)dynConstraints[60] , (String)dynConstraints[61] , (String)dynConstraints[62] , (short)dynConstraints[63] , (String)dynConstraints[64] , (DateTime)dynConstraints[65] , (DateTime)dynConstraints[66] , (DateTime)dynConstraints[67] , (DateTime)dynConstraints[68] , (int)dynConstraints[69] , (int)dynConstraints[70] , (int)dynConstraints[71] , (int)dynConstraints[72] , (String)dynConstraints[73] , (int)dynConstraints[74] , (String)dynConstraints[75] , (String)dynConstraints[76] , (int)dynConstraints[77] , (int)dynConstraints[78] , (bool)dynConstraints[79] , (String)dynConstraints[80] , (String)dynConstraints[81] , (short)dynConstraints[82] , (String)dynConstraints[83] , (DateTime)dynConstraints[84] , (DateTime)dynConstraints[85] , (DateTime)dynConstraints[86] , (DateTime)dynConstraints[87] , (int)dynConstraints[88] , (int)dynConstraints[89] , (int)dynConstraints[90] , (int)dynConstraints[91] , (String)dynConstraints[92] , (int)dynConstraints[93] , (String)dynConstraints[94] , (String)dynConstraints[95] , (int)dynConstraints[96] , (int)dynConstraints[97] , (String)dynConstraints[98] , (String)dynConstraints[99] , (String)dynConstraints[100] , (String)dynConstraints[101] , (String)dynConstraints[102] , (String)dynConstraints[103] , (String)dynConstraints[104] , (String)dynConstraints[105] , (DateTime)dynConstraints[106] , (DateTime)dynConstraints[107] , (DateTime)dynConstraints[108] , (DateTime)dynConstraints[109] , (String)dynConstraints[110] , (String)dynConstraints[111] , (String)dynConstraints[112] , (String)dynConstraints[113] , (int)dynConstraints[114] , (short)dynConstraints[115] , (String)dynConstraints[116] , (String)dynConstraints[117] , (decimal)dynConstraints[118] , (decimal)dynConstraints[119] , (int)dynConstraints[120] , (String)dynConstraints[121] , (String)dynConstraints[122] , (DateTime)dynConstraints[123] , (DateTime)dynConstraints[124] , (int)dynConstraints[125] , (int)dynConstraints[126] , (int)dynConstraints[127] , (bool)dynConstraints[128] , (int)dynConstraints[129] , (String)dynConstraints[130] , (String)dynConstraints[131] , (int)dynConstraints[132] , (int)dynConstraints[133] , (String)dynConstraints[134] , (String)dynConstraints[135] , (String)dynConstraints[136] , (decimal)dynConstraints[137] , (short)dynConstraints[138] , (decimal)dynConstraints[139] , (DateTime)dynConstraints[140] , (DateTime)dynConstraints[141] , (DateTime)dynConstraints[142] , (int)dynConstraints[143] , (int)dynConstraints[144] , (decimal)dynConstraints[145] , (decimal)dynConstraints[146] , (decimal)dynConstraints[147] , (decimal)dynConstraints[148] , (String)dynConstraints[149] , (bool)dynConstraints[150] , (DateTime)dynConstraints[151] , (DateTime)dynConstraints[152] , (int)dynConstraints[153] , (String)dynConstraints[154] , (DateTime)dynConstraints[155] , (DateTime)dynConstraints[156] , (int)dynConstraints[157] , (String)dynConstraints[158] , (DateTime)dynConstraints[159] , (DateTime)dynConstraints[160] , (int)dynConstraints[161] , (String)dynConstraints[162] , (DateTime)dynConstraints[163] , (DateTime)dynConstraints[164] , (int)dynConstraints[165] , (String)dynConstraints[166] , (DateTime)dynConstraints[167] , (DateTime)dynConstraints[168] , (decimal)dynConstraints[169] , (decimal)dynConstraints[170] , (decimal)dynConstraints[171] , (int)dynConstraints[172] , (int)dynConstraints[173] );
               case 2 :
                     return conditional_P003G6(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (bool)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (short)dynConstraints[25] , (String)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (int)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (bool)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (short)dynConstraints[44] , (String)dynConstraints[45] , (DateTime)dynConstraints[46] , (DateTime)dynConstraints[47] , (DateTime)dynConstraints[48] , (DateTime)dynConstraints[49] , (int)dynConstraints[50] , (int)dynConstraints[51] , (int)dynConstraints[52] , (int)dynConstraints[53] , (String)dynConstraints[54] , (int)dynConstraints[55] , (String)dynConstraints[56] , (String)dynConstraints[57] , (int)dynConstraints[58] , (int)dynConstraints[59] , (bool)dynConstraints[60] , (String)dynConstraints[61] , (String)dynConstraints[62] , (short)dynConstraints[63] , (String)dynConstraints[64] , (DateTime)dynConstraints[65] , (DateTime)dynConstraints[66] , (DateTime)dynConstraints[67] , (DateTime)dynConstraints[68] , (int)dynConstraints[69] , (int)dynConstraints[70] , (int)dynConstraints[71] , (int)dynConstraints[72] , (String)dynConstraints[73] , (int)dynConstraints[74] , (String)dynConstraints[75] , (String)dynConstraints[76] , (int)dynConstraints[77] , (int)dynConstraints[78] , (bool)dynConstraints[79] , (String)dynConstraints[80] , (String)dynConstraints[81] , (short)dynConstraints[82] , (String)dynConstraints[83] , (DateTime)dynConstraints[84] , (DateTime)dynConstraints[85] , (DateTime)dynConstraints[86] , (DateTime)dynConstraints[87] , (int)dynConstraints[88] , (int)dynConstraints[89] , (int)dynConstraints[90] , (int)dynConstraints[91] , (String)dynConstraints[92] , (int)dynConstraints[93] , (String)dynConstraints[94] , (String)dynConstraints[95] , (int)dynConstraints[96] , (int)dynConstraints[97] , (String)dynConstraints[98] , (String)dynConstraints[99] , (String)dynConstraints[100] , (String)dynConstraints[101] , (String)dynConstraints[102] , (String)dynConstraints[103] , (String)dynConstraints[104] , (String)dynConstraints[105] , (DateTime)dynConstraints[106] , (DateTime)dynConstraints[107] , (DateTime)dynConstraints[108] , (DateTime)dynConstraints[109] , (String)dynConstraints[110] , (String)dynConstraints[111] , (String)dynConstraints[112] , (String)dynConstraints[113] , (int)dynConstraints[114] , (short)dynConstraints[115] , (String)dynConstraints[116] , (String)dynConstraints[117] , (decimal)dynConstraints[118] , (decimal)dynConstraints[119] , (int)dynConstraints[120] , (String)dynConstraints[121] , (String)dynConstraints[122] , (DateTime)dynConstraints[123] , (DateTime)dynConstraints[124] , (int)dynConstraints[125] , (int)dynConstraints[126] , (int)dynConstraints[127] , (bool)dynConstraints[128] , (int)dynConstraints[129] , (String)dynConstraints[130] , (String)dynConstraints[131] , (int)dynConstraints[132] , (int)dynConstraints[133] , (String)dynConstraints[134] , (String)dynConstraints[135] , (String)dynConstraints[136] , (decimal)dynConstraints[137] , (short)dynConstraints[138] , (decimal)dynConstraints[139] , (DateTime)dynConstraints[140] , (DateTime)dynConstraints[141] , (DateTime)dynConstraints[142] , (int)dynConstraints[143] , (int)dynConstraints[144] , (decimal)dynConstraints[145] , (decimal)dynConstraints[146] , (decimal)dynConstraints[147] , (decimal)dynConstraints[148] , (String)dynConstraints[149] , (bool)dynConstraints[150] , (DateTime)dynConstraints[151] , (DateTime)dynConstraints[152] , (int)dynConstraints[153] , (String)dynConstraints[154] , (DateTime)dynConstraints[155] , (DateTime)dynConstraints[156] , (int)dynConstraints[157] , (String)dynConstraints[158] , (DateTime)dynConstraints[159] , (DateTime)dynConstraints[160] , (int)dynConstraints[161] , (String)dynConstraints[162] , (DateTime)dynConstraints[163] , (DateTime)dynConstraints[164] , (int)dynConstraints[165] , (String)dynConstraints[166] , (DateTime)dynConstraints[167] , (DateTime)dynConstraints[168] , (decimal)dynConstraints[169] , (decimal)dynConstraints[170] , (decimal)dynConstraints[171] , (int)dynConstraints[172] , (int)dynConstraints[173] , (int)dynConstraints[174] , (int)dynConstraints[175] );
               case 7 :
                     return conditional_P003G15(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP003G2 ;
          prmP003G2 = new Object[] {
          new Object[] {"@AV8ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003G7 ;
          prmP003G7 = new Object[] {
          new Object[] {"@AV8ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003G8 ;
          prmP003G8 = new Object[] {
          new Object[] {"@AV8ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003G11 ;
          prmP003G11 = new Object[] {
          new Object[] {"@AV8ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003G12 ;
          prmP003G12 = new Object[] {
          new Object[] {"@AV8ContagemResultado_LoteAceite",SqlDbType.Int,6,0} ,
          new Object[] {"@AV224ContagemResultado_SS",SqlDbType.Int,8,0}
          } ;
          Object[] prmP003G16 ;
          prmP003G16 = new Object[] {
          new Object[] {"@AV231Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003G4 ;
          prmP003G4 = new Object[] {
          new Object[] {"@AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV10WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10WWPC_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV335ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV336ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV337ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV338ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV339ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV341ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV342ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV343ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV345ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV348ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV350ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV352ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV360ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV361ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV362ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV363ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV364ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV366ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV367ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV368ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV370ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV373ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV375ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV377ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV385ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV386ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV387ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV388ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV389ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV391ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV392ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV393ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV395ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV398ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV400ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV402ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV410ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV411ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV412ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV413ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV414ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV416ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV417ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV418ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV420ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV423ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4",SqlDbType.Char,15,0} ,
          new Object[] {"@lV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV425ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV427ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV435ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV436ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV437ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV438ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV439ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV441ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV442ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV443ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV445ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV448ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5",SqlDbType.Char,15,0} ,
          new Object[] {"@lV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV450ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV452ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5",SqlDbType.Int,6,0} ,
          new Object[] {"@lV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV454ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV456ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV458ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV460ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV461ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV462ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV463ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV464ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV468ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV470ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV474ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV477ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV478ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP003G6 ;
          prmP003G6 = new Object[] {
          new Object[] {"@AV224ContagemResultado_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV331ExtraWWContagemResultadoExtraSelectionDS_5_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV332ExtraWWContagemResultadoExtraSelectionDS_6_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV340ExtraWWContagemResultadoExtraSelectionDS_14_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV329ExtraWWContagemResultadoExtraSelectionDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV356ExtraWWContagemResultadoExtraSelectionDS_30_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV357ExtraWWContagemResultadoExtraSelectionDS_31_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV365ExtraWWContagemResultadoExtraSelectionDS_39_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV353ExtraWWContagemResultadoExtraSelectionDS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV354ExtraWWContagemResultadoExtraSelectionDS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV381ExtraWWContagemResultadoExtraSelectionDS_55_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV382ExtraWWContagemResultadoExtraSelectionDS_56_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV390ExtraWWContagemResultadoExtraSelectionDS_64_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV378ExtraWWContagemResultadoExtraSelectionDS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV379ExtraWWContagemResultadoExtraSelectionDS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV406ExtraWWContagemResultadoExtraSelectionDS_80_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV407ExtraWWContagemResultadoExtraSelectionDS_81_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV415ExtraWWContagemResultadoExtraSelectionDS_89_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV403ExtraWWContagemResultadoExtraSelectionDS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV404ExtraWWContagemResultadoExtraSelectionDS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV431ExtraWWContagemResultadoExtraSelectionDS_105_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV432ExtraWWContagemResultadoExtraSelectionDS_106_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV440ExtraWWContagemResultadoExtraSelectionDS_114_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV428ExtraWWContagemResultadoExtraSelectionDS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV429ExtraWWContagemResultadoExtraSelectionDS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV465ExtraWWContagemResultadoExtraSelectionDS_139_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV466ExtraWWContagemResultadoExtraSelectionDS_140_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV10WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10WWPC_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV333ExtraWWContagemResultadoExtraSelectionDS_7_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV334ExtraWWContagemResultadoExtraSelectionDS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV335ExtraWWContagemResultadoExtraSelectionDS_9_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV336ExtraWWContagemResultadoExtraSelectionDS_10_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV337ExtraWWContagemResultadoExtraSelectionDS_11_Contagemresultado_dataprevista1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV338ExtraWWContagemResultadoExtraSelectionDS_12_Contagemresultado_dataprevista_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV339ExtraWWContagemResultadoExtraSelectionDS_13_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV341ExtraWWContagemResultadoExtraSelectionDS_15_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV342ExtraWWContagemResultadoExtraSelectionDS_16_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV343ExtraWWContagemResultadoExtraSelectionDS_17_Contagemresultado_servicogrupo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV345ExtraWWContagemResultadoExtraSelectionDS_19_Contagemresultado_naocnfdmncod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV348ExtraWWContagemResultadoExtraSelectionDS_22_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV349ExtraWWContagemResultadoExtraSelectionDS_23_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV350ExtraWWContagemResultadoExtraSelectionDS_24_Contagemresultado_owner1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV352ExtraWWContagemResultadoExtraSelectionDS_26_Contagemresultado_cntcod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV358ExtraWWContagemResultadoExtraSelectionDS_32_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV359ExtraWWContagemResultadoExtraSelectionDS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV360ExtraWWContagemResultadoExtraSelectionDS_34_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV361ExtraWWContagemResultadoExtraSelectionDS_35_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV362ExtraWWContagemResultadoExtraSelectionDS_36_Contagemresultado_dataprevista2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV363ExtraWWContagemResultadoExtraSelectionDS_37_Contagemresultado_dataprevista_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV364ExtraWWContagemResultadoExtraSelectionDS_38_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV366ExtraWWContagemResultadoExtraSelectionDS_40_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV367ExtraWWContagemResultadoExtraSelectionDS_41_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV368ExtraWWContagemResultadoExtraSelectionDS_42_Contagemresultado_servicogrupo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV370ExtraWWContagemResultadoExtraSelectionDS_44_Contagemresultado_naocnfdmncod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV373ExtraWWContagemResultadoExtraSelectionDS_47_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV374ExtraWWContagemResultadoExtraSelectionDS_48_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV375ExtraWWContagemResultadoExtraSelectionDS_49_Contagemresultado_owner2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV377ExtraWWContagemResultadoExtraSelectionDS_51_Contagemresultado_cntcod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV383ExtraWWContagemResultadoExtraSelectionDS_57_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV384ExtraWWContagemResultadoExtraSelectionDS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV385ExtraWWContagemResultadoExtraSelectionDS_59_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV386ExtraWWContagemResultadoExtraSelectionDS_60_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV387ExtraWWContagemResultadoExtraSelectionDS_61_Contagemresultado_dataprevista3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV388ExtraWWContagemResultadoExtraSelectionDS_62_Contagemresultado_dataprevista_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV389ExtraWWContagemResultadoExtraSelectionDS_63_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV391ExtraWWContagemResultadoExtraSelectionDS_65_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV392ExtraWWContagemResultadoExtraSelectionDS_66_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV393ExtraWWContagemResultadoExtraSelectionDS_67_Contagemresultado_servicogrupo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV395ExtraWWContagemResultadoExtraSelectionDS_69_Contagemresultado_naocnfdmncod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV398ExtraWWContagemResultadoExtraSelectionDS_72_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV399ExtraWWContagemResultadoExtraSelectionDS_73_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV400ExtraWWContagemResultadoExtraSelectionDS_74_Contagemresultado_owner3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV402ExtraWWContagemResultadoExtraSelectionDS_76_Contagemresultado_cntcod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV408ExtraWWContagemResultadoExtraSelectionDS_82_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV409ExtraWWContagemResultadoExtraSelectionDS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV410ExtraWWContagemResultadoExtraSelectionDS_84_Contagemresultado_datadmn4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV411ExtraWWContagemResultadoExtraSelectionDS_85_Contagemresultado_datadmn_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV412ExtraWWContagemResultadoExtraSelectionDS_86_Contagemresultado_dataprevista4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV413ExtraWWContagemResultadoExtraSelectionDS_87_Contagemresultado_dataprevista_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV414ExtraWWContagemResultadoExtraSelectionDS_88_Contagemresultado_servico4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV416ExtraWWContagemResultadoExtraSelectionDS_90_Contagemresultado_sistemacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV417ExtraWWContagemResultadoExtraSelectionDS_91_Contagemresultado_contratadacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV418ExtraWWContagemResultadoExtraSelectionDS_92_Contagemresultado_servicogrupo4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV420ExtraWWContagemResultadoExtraSelectionDS_94_Contagemresultado_naocnfdmncod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV423ExtraWWContagemResultadoExtraSelectionDS_97_Contagemresultado_agrupador4",SqlDbType.Char,15,0} ,
          new Object[] {"@lV424ExtraWWContagemResultadoExtraSelectionDS_98_Contagemresultado_descricao4",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV425ExtraWWContagemResultadoExtraSelectionDS_99_Contagemresultado_owner4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV427ExtraWWContagemResultadoExtraSelectionDS_101_Contagemresultado_cntcod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV433ExtraWWContagemResultadoExtraSelectionDS_107_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV434ExtraWWContagemResultadoExtraSelectionDS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV435ExtraWWContagemResultadoExtraSelectionDS_109_Contagemresultado_datadmn5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV436ExtraWWContagemResultadoExtraSelectionDS_110_Contagemresultado_datadmn_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV437ExtraWWContagemResultadoExtraSelectionDS_111_Contagemresultado_dataprevista5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV438ExtraWWContagemResultadoExtraSelectionDS_112_Contagemresultado_dataprevista_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV439ExtraWWContagemResultadoExtraSelectionDS_113_Contagemresultado_servico5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV441ExtraWWContagemResultadoExtraSelectionDS_115_Contagemresultado_sistemacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV442ExtraWWContagemResultadoExtraSelectionDS_116_Contagemresultado_contratadacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV443ExtraWWContagemResultadoExtraSelectionDS_117_Contagemresultado_servicogrupo5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV445ExtraWWContagemResultadoExtraSelectionDS_119_Contagemresultado_naocnfdmncod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV448ExtraWWContagemResultadoExtraSelectionDS_122_Contagemresultado_agrupador5",SqlDbType.Char,15,0} ,
          new Object[] {"@lV449ExtraWWContagemResultadoExtraSelectionDS_123_Contagemresultado_descricao5",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV450ExtraWWContagemResultadoExtraSelectionDS_124_Contagemresultado_owner5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV452ExtraWWContagemResultadoExtraSelectionDS_126_Contagemresultado_cntcod5",SqlDbType.Int,6,0} ,
          new Object[] {"@lV453ExtraWWContagemResultadoExtraSelectionDS_127_Tfcontagemresultado_agrupador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV454ExtraWWContagemResultadoExtraSelectionDS_128_Tfcontagemresultado_agrupador_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV455ExtraWWContagemResultadoExtraSelectionDS_129_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV456ExtraWWContagemResultadoExtraSelectionDS_130_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV457ExtraWWContagemResultadoExtraSelectionDS_131_Tfcontagemresultado_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV458ExtraWWContagemResultadoExtraSelectionDS_132_Tfcontagemresultado_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV459ExtraWWContagemResultadoExtraSelectionDS_133_Tfcontagemresultado_descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV460ExtraWWContagemResultadoExtraSelectionDS_134_Tfcontagemresultado_descricao_sel",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV461ExtraWWContagemResultadoExtraSelectionDS_135_Tfcontagemresultado_dataprevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV462ExtraWWContagemResultadoExtraSelectionDS_136_Tfcontagemresultado_dataprevista_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV463ExtraWWContagemResultadoExtraSelectionDS_137_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV464ExtraWWContagemResultadoExtraSelectionDS_138_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV467ExtraWWContagemResultadoExtraSelectionDS_141_Tfcontagemresultado_contratadasigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV468ExtraWWContagemResultadoExtraSelectionDS_142_Tfcontagemresultado_contratadasigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV469ExtraWWContagemResultadoExtraSelectionDS_143_Tfcontagemresultado_sistemacoord",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV470ExtraWWContagemResultadoExtraSelectionDS_144_Tfcontagemresultado_sistemacoord_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV473ExtraWWContagemResultadoExtraSelectionDS_147_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV474ExtraWWContagemResultadoExtraSelectionDS_148_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV477ExtraWWContagemResultadoExtraSelectionDS_151_Tfcontagemresultado_valorpf",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV478ExtraWWContagemResultadoExtraSelectionDS_152_Tfcontagemresultado_valorpf_to",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP003G15 ;
          prmP003G15 = new Object[] {
          new Object[] {"@AV230Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P003G2", "SELECT [Lote_Codigo], [Lote_Numero], [Lote_DataContrato] FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @AV8ContagemResultado_LoteAceite ORDER BY [Lote_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003G2,1,0,false,true )
             ,new CursorDef("P003G4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003G4,100,0,true,false )
             ,new CursorDef("P003G6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003G6,100,0,true,false )
             ,new CursorDef("P003G7", "SELECT T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T2.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T1.[ContagemResultado_ValorPF], T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @AV8ContagemResultado_LoteAceite ORDER BY T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS], T1.[ContagemResultado_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003G7,100,0,true,false )
             ,new CursorDef("P003G8", "SELECT T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T2.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T1.[ContagemResultado_ValorPF], T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @AV8ContagemResultado_LoteAceite ORDER BY T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS], T1.[ContagemResultado_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003G8,100,0,true,false )
             ,new CursorDef("P003G11", "SELECT COALESCE( T1.[GXC1], convert( DATETIME, '17530101', 112 )) AS GXC1, COALESCE( T1.[GXC2], convert( DATETIME, '17530101', 112 )) AS GXC2 FROM (SELECT MIN(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS GXC1, MAX(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS GXC2 FROM ([ContagemResultado] T2 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T2.[ContagemResultado_Codigo]) WHERE T2.[ContagemResultado_LoteAceiteCod] = @AV8ContagemResultado_LoteAceite ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003G11,1,0,true,true )
             ,new CursorDef("P003G12", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS], T3.[ContratoServicos_Momento] AS ContagemResultado_CntSrvMmn, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_PFCnc], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Codigo] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE (T1.[ContagemResultado_LoteAceiteCod] = @AV8ContagemResultado_LoteAceite) AND (T1.[ContagemResultado_SS] = @AV224ContagemResultado_SS) ORDER BY T1.[ContagemResultado_LoteAceiteCod], T2.[Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003G12,100,0,true,false )
             ,new CursorDef("P003G15", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003G15,1,0,true,true )
             ,new CursorDef("P003G16", "SELECT TOP 1 T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T4.[Contratante_Codigo], T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_OS], T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T2.[Pessoa_IE], T2.[Pessoa_Endereco], T2.[Pessoa_CEP], T2.[Pessoa_Telefone], T2.[Pessoa_Fax], T3.[Estado_UF] AS Pessoa_UF, T3.[Municipio_Nome], T5.[Contratante_OSAutomatica] FROM (((([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Pessoa_MunicipioCod]) INNER JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Contratante] T5 WITH (NOLOCK) ON T5.[Contratante_Codigo] = T4.[Contratante_Codigo]) WHERE T1.[Contratada_Codigo] = @AV231Contratada_Codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003G16,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((String[]) buf[16])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((int[]) buf[18])[0] = rslt.getInt(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((String[]) buf[20])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((String[]) buf[22])[0] = rslt.getString(12, 15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((int[]) buf[24])[0] = rslt.getInt(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((bool[]) buf[26])[0] = rslt.getBool(14) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((int[]) buf[28])[0] = rslt.getInt(15) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(15);
                ((int[]) buf[30])[0] = rslt.getInt(16) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(16);
                ((int[]) buf[32])[0] = rslt.getInt(17) ;
                ((int[]) buf[33])[0] = rslt.getInt(18) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                ((DateTime[]) buf[35])[0] = rslt.getGXDateTime(19) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(19);
                ((DateTime[]) buf[37])[0] = rslt.getGXDate(20) ;
                ((String[]) buf[38])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                ((decimal[]) buf[40])[0] = rslt.getDecimal(22) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(22);
                ((int[]) buf[42])[0] = rslt.getInt(23) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(23);
                ((String[]) buf[44])[0] = rslt.getString(24, 25) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(24);
                ((decimal[]) buf[46])[0] = rslt.getDecimal(25) ;
                ((decimal[]) buf[47])[0] = rslt.getDecimal(26) ;
                ((decimal[]) buf[48])[0] = rslt.getDecimal(27) ;
                ((decimal[]) buf[49])[0] = rslt.getDecimal(28) ;
                ((int[]) buf[50])[0] = rslt.getInt(29) ;
                ((DateTime[]) buf[51])[0] = rslt.getGXDate(30) ;
                ((short[]) buf[52])[0] = rslt.getShort(31) ;
                ((int[]) buf[53])[0] = rslt.getInt(32) ;
                ((DateTime[]) buf[54])[0] = rslt.getGXDate(33) ;
                ((String[]) buf[55])[0] = rslt.getString(34, 5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((String[]) buf[18])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((String[]) buf[20])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((int[]) buf[22])[0] = rslt.getInt(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((bool[]) buf[24])[0] = rslt.getBool(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((int[]) buf[26])[0] = rslt.getInt(14) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((int[]) buf[28])[0] = rslt.getInt(15) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(15);
                ((int[]) buf[30])[0] = rslt.getInt(16) ;
                ((int[]) buf[31])[0] = rslt.getInt(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((DateTime[]) buf[33])[0] = rslt.getGXDateTime(18) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                ((DateTime[]) buf[35])[0] = rslt.getGXDate(19) ;
                ((String[]) buf[36])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                ((String[]) buf[38])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                ((decimal[]) buf[40])[0] = rslt.getDecimal(22) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(22);
                ((int[]) buf[42])[0] = rslt.getInt(23) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(23);
                ((String[]) buf[44])[0] = rslt.getString(24, 1) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(24);
                ((decimal[]) buf[46])[0] = rslt.getDecimal(25) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(25);
                ((decimal[]) buf[48])[0] = rslt.getDecimal(26) ;
                ((decimal[]) buf[49])[0] = rslt.getDecimal(27) ;
                ((decimal[]) buf[50])[0] = rslt.getDecimal(28) ;
                ((decimal[]) buf[51])[0] = rslt.getDecimal(29) ;
                ((int[]) buf[52])[0] = rslt.getInt(30) ;
                ((DateTime[]) buf[53])[0] = rslt.getGXDate(31) ;
                ((short[]) buf[54])[0] = rslt.getShort(32) ;
                ((int[]) buf[55])[0] = rslt.getInt(33) ;
                ((DateTime[]) buf[56])[0] = rslt.getGXDate(34) ;
                ((String[]) buf[57])[0] = rslt.getString(35, 5) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 25) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 25) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                return;
             case 5 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 25) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 20) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((String[]) buf[7])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 2) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getString(15, 50) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((bool[]) buf[27])[0] = rslt.getBool(16) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[211]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[212]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[213]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[214]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[215]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[216]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[217]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[218]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[219]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[220]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[221]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[222]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[223]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[224]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[225]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[226]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[227]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[228]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[229]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[230]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[231]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[232]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[233]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[234]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[235]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[236]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[237]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[238]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[239]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[240]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[241]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[242]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[243]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[244]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[245]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[246]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[247]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[248]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[249]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[250]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[251]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[252]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[253]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[254]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[255]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[256]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[257]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[258]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[259]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[260]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[261]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[262]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[263]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[264]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[265]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[266]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[267]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[268]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[269]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[270]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[271]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[272]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[273]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[274]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[275]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[276]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[277]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[278]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[279]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[280]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[281]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[282]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[283]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[284]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[285]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[286]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[287]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[288]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[289]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[290]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[291]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[292]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[293]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[294]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[295]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[296]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[297]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[298]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[299]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[300]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[301]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[302]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[303]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[304]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[305]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[306]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[307]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[308]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[309]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[310]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[311]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[312]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[313]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[314]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[315]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[316]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[317]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[318]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[319]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[320]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[321]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[322]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[323]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[324]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[325]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[326]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[327]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[328]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[329]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[330]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[331]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[332]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[333]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[334]);
                }
                if ( (short)parms[124] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[335]);
                }
                if ( (short)parms[125] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[336]);
                }
                if ( (short)parms[126] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[337]);
                }
                if ( (short)parms[127] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[338]);
                }
                if ( (short)parms[128] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[339]);
                }
                if ( (short)parms[129] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[340]);
                }
                if ( (short)parms[130] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[341]);
                }
                if ( (short)parms[131] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[342]);
                }
                if ( (short)parms[132] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[343]);
                }
                if ( (short)parms[133] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[344]);
                }
                if ( (short)parms[134] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[345]);
                }
                if ( (short)parms[135] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[346]);
                }
                if ( (short)parms[136] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[347]);
                }
                if ( (short)parms[137] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[348]);
                }
                if ( (short)parms[138] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[349]);
                }
                if ( (short)parms[139] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[350]);
                }
                if ( (short)parms[140] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[351]);
                }
                if ( (short)parms[141] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[352]);
                }
                if ( (short)parms[142] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[353]);
                }
                if ( (short)parms[143] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[354]);
                }
                if ( (short)parms[144] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[355]);
                }
                if ( (short)parms[145] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[356]);
                }
                if ( (short)parms[146] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[357]);
                }
                if ( (short)parms[147] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[358]);
                }
                if ( (short)parms[148] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[359]);
                }
                if ( (short)parms[149] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[360]);
                }
                if ( (short)parms[150] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[361]);
                }
                if ( (short)parms[151] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[362]);
                }
                if ( (short)parms[152] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[363]);
                }
                if ( (short)parms[153] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[364]);
                }
                if ( (short)parms[154] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[365]);
                }
                if ( (short)parms[155] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[366]);
                }
                if ( (short)parms[156] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[367]);
                }
                if ( (short)parms[157] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[368]);
                }
                if ( (short)parms[158] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[369]);
                }
                if ( (short)parms[159] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[370]);
                }
                if ( (short)parms[160] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[371]);
                }
                if ( (short)parms[161] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[372]);
                }
                if ( (short)parms[162] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[373]);
                }
                if ( (short)parms[163] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[374]);
                }
                if ( (short)parms[164] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[375]);
                }
                if ( (short)parms[165] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[376]);
                }
                if ( (short)parms[166] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[377]);
                }
                if ( (short)parms[167] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[378]);
                }
                if ( (short)parms[168] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[379]);
                }
                if ( (short)parms[169] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[380]);
                }
                if ( (short)parms[170] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[381]);
                }
                if ( (short)parms[171] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[382]);
                }
                if ( (short)parms[172] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[383]);
                }
                if ( (short)parms[173] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[384]);
                }
                if ( (short)parms[174] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[385]);
                }
                if ( (short)parms[175] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[386]);
                }
                if ( (short)parms[176] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[387]);
                }
                if ( (short)parms[177] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[388]);
                }
                if ( (short)parms[178] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[389]);
                }
                if ( (short)parms[179] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[390]);
                }
                if ( (short)parms[180] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[391]);
                }
                if ( (short)parms[181] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[392]);
                }
                if ( (short)parms[182] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[393]);
                }
                if ( (short)parms[183] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[394]);
                }
                if ( (short)parms[184] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[395]);
                }
                if ( (short)parms[185] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[396]);
                }
                if ( (short)parms[186] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[397]);
                }
                if ( (short)parms[187] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[398]);
                }
                if ( (short)parms[188] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[399]);
                }
                if ( (short)parms[189] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[400]);
                }
                if ( (short)parms[190] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[401]);
                }
                if ( (short)parms[191] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[402]);
                }
                if ( (short)parms[192] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[403]);
                }
                if ( (short)parms[193] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[404]);
                }
                if ( (short)parms[194] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[405]);
                }
                if ( (short)parms[195] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[406]);
                }
                if ( (short)parms[196] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[407]);
                }
                if ( (short)parms[197] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[408]);
                }
                if ( (short)parms[198] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[409]);
                }
                if ( (short)parms[199] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[410]);
                }
                if ( (short)parms[200] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[411]);
                }
                if ( (short)parms[201] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[412]);
                }
                if ( (short)parms[202] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[413]);
                }
                if ( (short)parms[203] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[414]);
                }
                if ( (short)parms[204] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[415]);
                }
                if ( (short)parms[205] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[416]);
                }
                if ( (short)parms[206] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[417]);
                }
                if ( (short)parms[207] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[418]);
                }
                if ( (short)parms[208] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[419]);
                }
                if ( (short)parms[209] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[420]);
                }
                if ( (short)parms[210] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[421]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[212]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[213]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[214]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[215]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[216]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[217]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[218]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[219]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[220]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[221]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[222]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[223]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[224]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[225]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[226]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[227]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[228]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[229]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[230]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[231]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[232]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[233]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[234]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[235]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[236]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[237]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[238]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[239]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[240]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[241]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[242]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[243]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[244]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[245]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[246]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[247]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[248]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[249]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[250]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[251]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[252]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[253]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[254]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[255]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[256]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[257]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[258]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[259]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[260]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[261]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[262]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[263]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[264]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[265]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[266]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[267]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[268]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[269]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[270]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[271]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[272]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[273]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[274]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[275]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[276]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[277]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[278]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[279]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[280]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[281]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[282]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[283]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[284]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[285]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[286]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[287]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[288]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[289]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[290]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[291]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[292]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[293]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[294]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[295]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[296]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[297]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[298]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[299]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[300]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[301]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[302]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[303]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[304]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[305]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[306]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[307]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[308]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[309]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[310]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[311]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[312]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[313]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[314]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[315]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[316]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[317]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[318]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[319]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[320]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[321]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[322]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[323]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[324]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[325]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[326]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[327]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[328]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[329]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[330]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[331]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[332]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[333]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[334]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[335]);
                }
                if ( (short)parms[124] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[336]);
                }
                if ( (short)parms[125] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[337]);
                }
                if ( (short)parms[126] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[338]);
                }
                if ( (short)parms[127] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[339]);
                }
                if ( (short)parms[128] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[340]);
                }
                if ( (short)parms[129] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[341]);
                }
                if ( (short)parms[130] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[342]);
                }
                if ( (short)parms[131] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[343]);
                }
                if ( (short)parms[132] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[344]);
                }
                if ( (short)parms[133] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[345]);
                }
                if ( (short)parms[134] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[346]);
                }
                if ( (short)parms[135] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[347]);
                }
                if ( (short)parms[136] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[348]);
                }
                if ( (short)parms[137] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[349]);
                }
                if ( (short)parms[138] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[350]);
                }
                if ( (short)parms[139] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[351]);
                }
                if ( (short)parms[140] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[352]);
                }
                if ( (short)parms[141] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[353]);
                }
                if ( (short)parms[142] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[354]);
                }
                if ( (short)parms[143] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[355]);
                }
                if ( (short)parms[144] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[356]);
                }
                if ( (short)parms[145] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[357]);
                }
                if ( (short)parms[146] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[358]);
                }
                if ( (short)parms[147] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[359]);
                }
                if ( (short)parms[148] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[360]);
                }
                if ( (short)parms[149] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[361]);
                }
                if ( (short)parms[150] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[362]);
                }
                if ( (short)parms[151] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[363]);
                }
                if ( (short)parms[152] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[364]);
                }
                if ( (short)parms[153] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[365]);
                }
                if ( (short)parms[154] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[366]);
                }
                if ( (short)parms[155] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[367]);
                }
                if ( (short)parms[156] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[368]);
                }
                if ( (short)parms[157] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[369]);
                }
                if ( (short)parms[158] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[370]);
                }
                if ( (short)parms[159] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[371]);
                }
                if ( (short)parms[160] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[372]);
                }
                if ( (short)parms[161] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[373]);
                }
                if ( (short)parms[162] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[374]);
                }
                if ( (short)parms[163] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[375]);
                }
                if ( (short)parms[164] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[376]);
                }
                if ( (short)parms[165] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[377]);
                }
                if ( (short)parms[166] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[378]);
                }
                if ( (short)parms[167] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[379]);
                }
                if ( (short)parms[168] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[380]);
                }
                if ( (short)parms[169] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[381]);
                }
                if ( (short)parms[170] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[382]);
                }
                if ( (short)parms[171] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[383]);
                }
                if ( (short)parms[172] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[384]);
                }
                if ( (short)parms[173] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[385]);
                }
                if ( (short)parms[174] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[386]);
                }
                if ( (short)parms[175] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[387]);
                }
                if ( (short)parms[176] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[388]);
                }
                if ( (short)parms[177] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[389]);
                }
                if ( (short)parms[178] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[390]);
                }
                if ( (short)parms[179] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[391]);
                }
                if ( (short)parms[180] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[392]);
                }
                if ( (short)parms[181] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[393]);
                }
                if ( (short)parms[182] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[394]);
                }
                if ( (short)parms[183] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[395]);
                }
                if ( (short)parms[184] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[396]);
                }
                if ( (short)parms[185] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[397]);
                }
                if ( (short)parms[186] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[398]);
                }
                if ( (short)parms[187] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[399]);
                }
                if ( (short)parms[188] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[400]);
                }
                if ( (short)parms[189] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[401]);
                }
                if ( (short)parms[190] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[402]);
                }
                if ( (short)parms[191] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[403]);
                }
                if ( (short)parms[192] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[404]);
                }
                if ( (short)parms[193] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[405]);
                }
                if ( (short)parms[194] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[406]);
                }
                if ( (short)parms[195] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[407]);
                }
                if ( (short)parms[196] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[408]);
                }
                if ( (short)parms[197] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[409]);
                }
                if ( (short)parms[198] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[410]);
                }
                if ( (short)parms[199] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[411]);
                }
                if ( (short)parms[200] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[412]);
                }
                if ( (short)parms[201] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[413]);
                }
                if ( (short)parms[202] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[414]);
                }
                if ( (short)parms[203] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[415]);
                }
                if ( (short)parms[204] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[416]);
                }
                if ( (short)parms[205] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[417]);
                }
                if ( (short)parms[206] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[418]);
                }
                if ( (short)parms[207] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[419]);
                }
                if ( (short)parms[208] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[420]);
                }
                if ( (short)parms[209] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[421]);
                }
                if ( (short)parms[210] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[422]);
                }
                if ( (short)parms[211] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[423]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
