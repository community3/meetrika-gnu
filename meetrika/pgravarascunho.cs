/*
               File: PGravaRascunho
        Description: PGrava Rascunho
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:21:0.72
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class pgravarascunho : GXProcedure
   {
      public pgravarascunho( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public pgravarascunho( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contratada_codigo ,
                           ref int aP1_Servico_Codigo ,
                           ref int aP2_Sistema_Codigo ,
                           ref String aP3_Solicitacoes_Objetivo ,
                           ref String aP4_Solicitacoes_Novo_Projeto ,
                           ref int aP5_UserName ,
                           ref IGxCollection aP6_SDTSolicitacao ,
                           out String aP7_vrMsg )
      {
         this.AV8Contratada_codigo = aP0_Contratada_codigo;
         this.AV9Servico_Codigo = aP1_Servico_Codigo;
         this.AV10Sistema_Codigo = aP2_Sistema_Codigo;
         this.AV11Solicitacoes_Objetivo = aP3_Solicitacoes_Objetivo;
         this.AV14Solicitacoes_Novo_Projeto = aP4_Solicitacoes_Novo_Projeto;
         this.AV15UserName = aP5_UserName;
         this.AV12SDTSolicitacao = aP6_SDTSolicitacao;
         this.AV18vrMsg = "" ;
         initialize();
         executePrivate();
         aP0_Contratada_codigo=this.AV8Contratada_codigo;
         aP1_Servico_Codigo=this.AV9Servico_Codigo;
         aP2_Sistema_Codigo=this.AV10Sistema_Codigo;
         aP3_Solicitacoes_Objetivo=this.AV11Solicitacoes_Objetivo;
         aP4_Solicitacoes_Novo_Projeto=this.AV14Solicitacoes_Novo_Projeto;
         aP5_UserName=this.AV15UserName;
         aP6_SDTSolicitacao=this.AV12SDTSolicitacao;
         aP7_vrMsg=this.AV18vrMsg;
      }

      public String executeUdp( ref int aP0_Contratada_codigo ,
                                ref int aP1_Servico_Codigo ,
                                ref int aP2_Sistema_Codigo ,
                                ref String aP3_Solicitacoes_Objetivo ,
                                ref String aP4_Solicitacoes_Novo_Projeto ,
                                ref int aP5_UserName ,
                                ref IGxCollection aP6_SDTSolicitacao )
      {
         this.AV8Contratada_codigo = aP0_Contratada_codigo;
         this.AV9Servico_Codigo = aP1_Servico_Codigo;
         this.AV10Sistema_Codigo = aP2_Sistema_Codigo;
         this.AV11Solicitacoes_Objetivo = aP3_Solicitacoes_Objetivo;
         this.AV14Solicitacoes_Novo_Projeto = aP4_Solicitacoes_Novo_Projeto;
         this.AV15UserName = aP5_UserName;
         this.AV12SDTSolicitacao = aP6_SDTSolicitacao;
         this.AV18vrMsg = "" ;
         initialize();
         executePrivate();
         aP0_Contratada_codigo=this.AV8Contratada_codigo;
         aP1_Servico_Codigo=this.AV9Servico_Codigo;
         aP2_Sistema_Codigo=this.AV10Sistema_Codigo;
         aP3_Solicitacoes_Objetivo=this.AV11Solicitacoes_Objetivo;
         aP4_Solicitacoes_Novo_Projeto=this.AV14Solicitacoes_Novo_Projeto;
         aP5_UserName=this.AV15UserName;
         aP6_SDTSolicitacao=this.AV12SDTSolicitacao;
         aP7_vrMsg=this.AV18vrMsg;
         return AV18vrMsg ;
      }

      public void executeSubmit( ref int aP0_Contratada_codigo ,
                                 ref int aP1_Servico_Codigo ,
                                 ref int aP2_Sistema_Codigo ,
                                 ref String aP3_Solicitacoes_Objetivo ,
                                 ref String aP4_Solicitacoes_Novo_Projeto ,
                                 ref int aP5_UserName ,
                                 ref IGxCollection aP6_SDTSolicitacao ,
                                 out String aP7_vrMsg )
      {
         pgravarascunho objpgravarascunho;
         objpgravarascunho = new pgravarascunho();
         objpgravarascunho.AV8Contratada_codigo = aP0_Contratada_codigo;
         objpgravarascunho.AV9Servico_Codigo = aP1_Servico_Codigo;
         objpgravarascunho.AV10Sistema_Codigo = aP2_Sistema_Codigo;
         objpgravarascunho.AV11Solicitacoes_Objetivo = aP3_Solicitacoes_Objetivo;
         objpgravarascunho.AV14Solicitacoes_Novo_Projeto = aP4_Solicitacoes_Novo_Projeto;
         objpgravarascunho.AV15UserName = aP5_UserName;
         objpgravarascunho.AV12SDTSolicitacao = aP6_SDTSolicitacao;
         objpgravarascunho.AV18vrMsg = "" ;
         objpgravarascunho.context.SetSubmitInitialConfig(context);
         objpgravarascunho.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objpgravarascunho);
         aP0_Contratada_codigo=this.AV8Contratada_codigo;
         aP1_Servico_Codigo=this.AV9Servico_Codigo;
         aP2_Sistema_Codigo=this.AV10Sistema_Codigo;
         aP3_Solicitacoes_Objetivo=this.AV11Solicitacoes_Objetivo;
         aP4_Solicitacoes_Novo_Projeto=this.AV14Solicitacoes_Novo_Projeto;
         aP5_UserName=this.AV15UserName;
         aP6_SDTSolicitacao=this.AV12SDTSolicitacao;
         aP7_vrMsg=this.AV18vrMsg;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((pgravarascunho)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Contratada_codigo ;
      private int AV9Servico_Codigo ;
      private int AV10Sistema_Codigo ;
      private int AV15UserName ;
      private String AV14Solicitacoes_Novo_Projeto ;
      private String AV18vrMsg ;
      private String AV11Solicitacoes_Objetivo ;
      private int aP0_Contratada_codigo ;
      private int aP1_Servico_Codigo ;
      private int aP2_Sistema_Codigo ;
      private String aP3_Solicitacoes_Objetivo ;
      private String aP4_Solicitacoes_Novo_Projeto ;
      private int aP5_UserName ;
      private IGxCollection aP6_SDTSolicitacao ;
      private String aP7_vrMsg ;
      [ObjectCollection(ItemType=typeof( SdtSDTSolicitacao_SDTSolicitacaoItem ))]
      private IGxCollection AV12SDTSolicitacao ;
   }

}
