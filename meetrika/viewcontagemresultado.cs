/*
               File: ViewContagemResultado
        Description: View Contagem Resultado
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:56:4.82
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class viewcontagemresultado : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public viewcontagemresultado( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public viewcontagemresultado( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           String aP1_TabCode )
      {
         this.AV9ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV7TabCode = aP1_TabCode;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV9ContagemResultado_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ContagemResultado_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV7TabCode = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAB72( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTB72( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203242356494");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTABCODE", StringUtil.RTrim( AV7TabCode));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_OSFSOSFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ViewContagemResultado";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("viewcontagemresultado:[SendSecurityCheck value for]"+"ContagemResultado_Codigo:"+context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            WebComp_Tabbedview.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEB72( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTB72( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode)) ;
      }

      public override String GetPgmname( )
      {
         return "ViewContagemResultado" ;
      }

      public override String GetPgmdesc( )
      {
         return "View Contagem Resultado" ;
      }

      protected void WBB70( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_B72( true) ;
         }
         else
         {
            wb_table1_2_B72( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_B72e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTB72( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "View Contagem Resultado", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPB70( ) ;
      }

      protected void WSB72( )
      {
         STARTB72( ) ;
         EVTB72( ) ;
      }

      protected void EVTB72( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11B72 */
                              E11B72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12B72 */
                              E12B72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 59 )
                        {
                           OldTabbedview = cgiGet( "W0059");
                           if ( ( StringUtil.Len( OldTabbedview) == 0 ) || ( StringUtil.StrCmp(OldTabbedview, WebComp_Tabbedview_Component) != 0 ) )
                           {
                              WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", OldTabbedview, new Object[] {context} );
                              WebComp_Tabbedview.ComponentInit();
                              WebComp_Tabbedview.Name = "OldTabbedview";
                              WebComp_Tabbedview_Component = OldTabbedview;
                           }
                           if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
                           {
                              WebComp_Tabbedview.componentprocess("W0059", "", sEvt);
                           }
                           WebComp_Tabbedview_Component = OldTabbedview;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEB72( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAB72( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFB72( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFB72( )
      {
         initialize_formulas( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  WebComp_Tabbedview.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00B72 */
            pr_default.execute(0, new Object[] {AV9ContagemResultado_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A494ContagemResultado_Descricao = H00B72_A494ContagemResultado_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A494ContagemResultado_Descricao", A494ContagemResultado_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, ""))));
               n494ContagemResultado_Descricao = H00B72_n494ContagemResultado_Descricao[0];
               A456ContagemResultado_Codigo = H00B72_A456ContagemResultado_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
               A493ContagemResultado_DemandaFM = H00B72_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00B72_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = H00B72_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00B72_n457ContagemResultado_Demanda[0];
               A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_OSFSOSFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, ""))));
               /* Execute user event: E12B72 */
               E12B72 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBB70( ) ;
         }
      }

      protected void STRUPB70( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11B72 */
         E11B72 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
            A501ContagemResultado_OsFsOsFm = cgiGet( edtContagemResultado_OsFsOsFm_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_OSFSOSFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, ""))));
            A494ContagemResultado_Descricao = cgiGet( edtContagemResultado_Descricao_Internalname);
            n494ContagemResultado_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A494ContagemResultado_Descricao", A494ContagemResultado_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, ""))));
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "ViewContagemResultado";
            A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("viewcontagemresultado:[SecurityCheckFailed value for]"+"ContagemResultado_Codigo:"+context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11B72 */
         E11B72 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11B72( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         lblWorkwithlink_Link = formatLink("wwcontagemresultado.aspx") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Link", lblWorkwithlink_Link);
         AV19GXLvl9 = 0;
         /* Using cursor H00B73 */
         pr_default.execute(1, new Object[] {AV9ContagemResultado_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A456ContagemResultado_Codigo = H00B73_A456ContagemResultado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
            A457ContagemResultado_Demanda = H00B73_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = H00B73_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = H00B73_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = H00B73_n493ContagemResultado_DemandaFM[0];
            AV19GXLvl9 = 1;
            Form.Caption = "OS "+(String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? StringUtil.Trim( A457ContagemResultado_Demanda) : StringUtil.Trim( A493ContagemResultado_DemandaFM));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = true;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         if ( AV19GXLvl9 == 0 )
         {
            Form.Caption = "Registro n�o encontrado ";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = false;
         }
         if ( AV8Exists )
         {
            /* Execute user subroutine: 'LOADTABS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            /* Object Property */
            if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Tabbedview_Component), StringUtil.Lower( "WWPBaseObjects.WWPTabbedView")) != 0 )
            {
               WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", "wwpbaseobjects.wwptabbedview", new Object[] {context} );
               WebComp_Tabbedview.ComponentInit();
               WebComp_Tabbedview.Name = "WWPBaseObjects.WWPTabbedView";
               WebComp_Tabbedview_Component = "WWPBaseObjects.WWPTabbedView";
            }
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.setjustcreated();
               WebComp_Tabbedview.componentprepare(new Object[] {(String)"W0059",(String)"",(IGxCollection)AV10Tabs,(String)AV7TabCode});
               WebComp_Tabbedview.componentbind(new Object[] {(String)"",(String)""});
            }
         }
         if ( AV8Exists )
         {
            /* Using cursor H00B74 */
            pr_default.execute(2, new Object[] {AV9ContagemResultado_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A602ContagemResultado_OSVinculada = H00B74_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = H00B74_n602ContagemResultado_OSVinculada[0];
               A456ContagemResultado_Codigo = H00B74_A456ContagemResultado_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
               A1636ContagemResultado_ServicoSS = H00B74_A1636ContagemResultado_ServicoSS[0];
               n1636ContagemResultado_ServicoSS = H00B74_n1636ContagemResultado_ServicoSS[0];
               A1765ContagemResultado_CodSrvSSVnc = H00B74_A1765ContagemResultado_CodSrvSSVnc[0];
               n1765ContagemResultado_CodSrvSSVnc = H00B74_n1765ContagemResultado_CodSrvSSVnc[0];
               A1765ContagemResultado_CodSrvSSVnc = H00B74_A1765ContagemResultado_CodSrvSSVnc[0];
               n1765ContagemResultado_CodSrvSSVnc = H00B74_n1765ContagemResultado_CodSrvSSVnc[0];
               if ( A1765ContagemResultado_CodSrvSSVnc + A1636ContagemResultado_ServicoSS == 0 )
               {
                  lblViewtitle_Caption = "OS Ref.| OS N� ::";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblViewtitle_Internalname, "Caption", lblViewtitle_Caption);
               }
               else if ( A1636ContagemResultado_ServicoSS > 0 )
               {
                  lblViewtitle_Caption = "OS Ref.| SS N� ::";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblViewtitle_Internalname, "Caption", lblViewtitle_Caption);
               }
               else if ( A1765ContagemResultado_CodSrvSSVnc > 0 )
               {
                  lblViewtitle_Caption = "SS Ref.| OS N� ::";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblViewtitle_Internalname, "Caption", lblViewtitle_Caption);
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
         }
         lblWorkwithlink_Link = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Link", lblWorkwithlink_Link);
         lblWorkwithlink_Jsonclick = "window.history.back()";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Jsonclick", lblWorkwithlink_Jsonclick);
      }

      protected void nextLoad( )
      {
      }

      protected void E12B72( )
      {
         /* Load Routine */
      }

      protected void S112( )
      {
         /* 'LOADTABS' Routine */
         /* Using cursor H00B75 */
         pr_default.execute(3, new Object[] {AV9ContagemResultado_Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A456ContagemResultado_Codigo = H00B75_A456ContagemResultado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
            A602ContagemResultado_OSVinculada = H00B75_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00B75_n602ContagemResultado_OSVinculada[0];
            AV13ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
         AV10Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "General";
         AV11Tab.gxTpr_Description = "Demanda";
         AV11Tab.gxTpr_Webcomponent = formatLink("contagemresultadogeneral.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 0;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "Descri��o";
         AV11Tab.gxTpr_Description = "Descri��o complementar";
         AV11Tab.gxTpr_Webcomponent = formatLink("contagemresultadoobservacao.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 0;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContagemResultadoContagens";
         AV11Tab.gxTpr_Description = "Registro de Esfor�o";
         AV11Tab.gxTpr_Webcomponent = formatLink("contagemresultadocontagemresultadocontagenswc.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = true;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContagemResultadoEvidencia";
         AV11Tab.gxTpr_Description = "Evid�ncias";
         AV11Tab.gxTpr_Webcomponent = formatLink("contagemresultadoevidenciaswc.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 0;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "Vinculadas";
         AV11Tab.gxTpr_Description = "Demandas Vinculadas";
         AV11Tab.gxTpr_Webcomponent = formatLink("wc_demandasvinculadas.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo) + "," + UrlEncode("" +AV13ContagemResultado_OSVinculada);
         AV11Tab.gxTpr_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContagemResultadoChckLstLog";
         AV11Tab.gxTpr_Description = "Check List";
         AV11Tab.gxTpr_Webcomponent = formatLink("contagemresultadochcklstlogwc.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "Requisitos";
         AV11Tab.gxTpr_Description = "Requisitos";
         AV11Tab.gxTpr_Webcomponent = formatLink("contagemresultadorequisitoswc.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "Notificacoes";
         AV11Tab.gxTpr_Description = "Notifica��es";
         AV11Tab.gxTpr_Webcomponent = formatLink("demandanotificacoeswc.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "QA";
         AV11Tab.gxTpr_Description = "QA";
         AV11Tab.gxTpr_Webcomponent = formatLink("contagemresultado_qawc.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 0;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "LogResponsavel";
         AV11Tab.gxTpr_Description = "Hist�rico";
         AV11Tab.gxTpr_Webcomponent = formatLink("wclogresponsavel.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +AV9ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         /* Using cursor H00B76 */
         pr_default.execute(4, new Object[] {AV9ContagemResultado_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A456ContagemResultado_Codigo = H00B76_A456ContagemResultado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
            A514ContagemResultado_Observacao = H00B76_A514ContagemResultado_Observacao[0];
            n514ContagemResultado_Observacao = H00B76_n514ContagemResultado_Observacao[0];
            AV14Index = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Index", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Index), 4, 0)));
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A514ContagemResultado_Observacao)) || H00B76_n514ContagemResultado_Observacao[0] )
            {
               AV12Count = 0;
            }
            else
            {
               AV12Count = 1;
            }
            /* Optimized group. */
            /* Using cursor H00B77 */
            pr_default.execute(5, new Object[] {A456ContagemResultado_Codigo});
            cV12Count = H00B77_AV12Count[0];
            pr_default.close(5);
            AV12Count = (short)(AV12Count+cV12Count*1);
            /* End optimized group. */
            if ( (0==AV12Count) )
            {
               /* Execute user subroutine: 'REMOVEITEM' */
               S126 ();
               if ( returnInSub )
               {
                  pr_default.close(4);
                  returnInSub = true;
                  if (true) return;
               }
            }
            else
            {
               AV14Index = (short)(AV14Index+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Index", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Index), 4, 0)));
               AV12Count = 0;
            }
            /* Optimized group. */
            /* Using cursor H00B78 */
            pr_default.execute(6, new Object[] {A456ContagemResultado_Codigo});
            cV12Count = H00B78_AV12Count[0];
            pr_default.close(6);
            AV12Count = (short)(AV12Count+cV12Count*1);
            /* End optimized group. */
            if ( (0==AV12Count) )
            {
               /* Execute user subroutine: 'REMOVEITEM' */
               S126 ();
               if ( returnInSub )
               {
                  pr_default.close(4);
                  returnInSub = true;
                  if (true) return;
               }
            }
            else
            {
               AV14Index = (short)(AV14Index+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Index", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Index), 4, 0)));
               AV12Count = 0;
            }
            GXt_char1 = "";
            new prc_criarpastadeevd(context ).execute( ref  AV9ContagemResultado_Codigo, ref  AV12Count, out  GXt_char1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ContagemResultado_Codigo), "ZZZZZ9")));
            AV16Directory.Source = GXt_char1;
            AV14Index = (short)(AV14Index+2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Index", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Index), 4, 0)));
            /* Optimized group. */
            /* Using cursor H00B79 */
            pr_default.execute(7, new Object[] {A456ContagemResultado_Codigo});
            cV12Count = H00B79_AV12Count[0];
            pr_default.close(7);
            AV12Count = (short)(AV12Count+cV12Count*1);
            /* End optimized group. */
            if ( (0==AV12Count) )
            {
               /* Execute user subroutine: 'REMOVEITEM' */
               S126 ();
               if ( returnInSub )
               {
                  pr_default.close(4);
                  returnInSub = true;
                  if (true) return;
               }
            }
            else
            {
               AV14Index = (short)(AV14Index+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Index", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Index), 4, 0)));
               AV12Count = 0;
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(4);
      }

      protected void S126( )
      {
         /* 'REMOVEITEM' Routine */
         AV10Tabs.RemoveItem(AV14Index);
      }

      protected void wb_table1_2_B72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table100x100", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_B72( true) ;
         }
         else
         {
            wb_table2_5_B72( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_B72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\" class='TableTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblWorkwithlink_Internalname, "Voltar", lblWorkwithlink_Link, "", lblWorkwithlink_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockLink", 0, "", 1, 1, 0, "HLP_ViewContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"10\" >") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0059"+"", StringUtil.RTrim( WebComp_Tabbedview_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0059"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0059"+"");
                  }
                  WebComp_Tabbedview.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_B72e( true) ;
         }
         else
         {
            wb_table1_2_B72e( false) ;
         }
      }

      protected void wb_table2_5_B72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_8_B72( true) ;
         }
         else
         {
            wb_table3_8_B72( false) ;
         }
         return  ;
      }

      protected void wb_table3_8_B72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"10\"  class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Descricao_Internalname, A494ContagemResultado_Descricao, StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_Descricao_Jsonclick, 0, "ReadonlyAttSubTitle", "", "", "", 1, 0, 0, "text", "", 100, "%", 1, "row", 500, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ViewContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_B72e( true) ;
         }
         else
         {
            wb_table2_5_B72e( false) ;
         }
      }

      protected void wb_table3_8_B72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_11_B72( true) ;
         }
         else
         {
            wb_table4_11_B72( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_B72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_25_B72( true) ;
         }
         else
         {
            wb_table5_25_B72( false) ;
         }
         return  ;
      }

      protected void wb_table5_25_B72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_8_B72e( true) ;
         }
         else
         {
            wb_table3_8_B72e( false) ;
         }
      }

      protected void wb_table5_25_B72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedviewtitle_Internalname, tblTablemergedviewtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblViewtitle_Internalname, lblViewtitle_Caption, "", "", lblViewtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ViewContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_OsFsOsFm_Internalname, A501ContagemResultado_OsFsOsFm, StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_OsFsOsFm_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, 0, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ViewContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_25_B72e( true) ;
         }
         else
         {
            wb_table5_25_B72e( false) ;
         }
      }

      protected void wb_table4_11_B72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedabre_Internalname, tblTablemergedabre_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAbre_Internalname, "(ID:", "", "", lblAbre_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ViewContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_Codigo_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ViewContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFecha_Internalname, ")", "", "", lblFecha_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ViewContagemResultado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_B72e( true) ;
         }
         else
         {
            wb_table4_11_B72e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV9ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ContagemResultado_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9ContagemResultado_Codigo), "ZZZZZ9")));
         AV7TabCode = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAB72( ) ;
         WSB72( ) ;
         WEB72( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203242356549");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("viewcontagemresultado.js", "?20203242356549");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblAbre_Internalname = "ABRE";
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO";
         lblFecha_Internalname = "FECHA";
         tblTablemergedabre_Internalname = "TABLEMERGEDABRE";
         lblViewtitle_Internalname = "VIEWTITLE";
         edtContagemResultado_OsFsOsFm_Internalname = "CONTAGEMRESULTADO_OSFSOSFM";
         tblTablemergedviewtitle_Internalname = "TABLEMERGEDVIEWTITLE";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         edtContagemResultado_Descricao_Internalname = "CONTAGEMRESULTADO_DESCRICAO";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         lblWorkwithlink_Internalname = "WORKWITHLINK";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContagemResultado_Codigo_Jsonclick = "";
         edtContagemResultado_OsFsOsFm_Jsonclick = "";
         edtContagemResultado_Descricao_Jsonclick = "";
         lblWorkwithlink_Link = "";
         lblViewtitle_Caption = "OS Ref.| OS N� ::";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "View Contagem Resultado";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV7TabCode = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A501ContagemResultado_OsFsOsFm = "";
         A494ContagemResultado_Descricao = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         OldTabbedview = "";
         WebComp_Tabbedview_Component = "";
         scmdbuf = "";
         H00B72_A494ContagemResultado_Descricao = new String[] {""} ;
         H00B72_n494ContagemResultado_Descricao = new bool[] {false} ;
         H00B72_A456ContagemResultado_Codigo = new int[1] ;
         H00B72_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00B72_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00B72_A457ContagemResultado_Demanda = new String[] {""} ;
         H00B72_n457ContagemResultado_Demanda = new bool[] {false} ;
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         H00B73_A456ContagemResultado_Codigo = new int[1] ;
         H00B73_A457ContagemResultado_Demanda = new String[] {""} ;
         H00B73_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00B73_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00B73_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         AV10Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         H00B74_A602ContagemResultado_OSVinculada = new int[1] ;
         H00B74_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00B74_A456ContagemResultado_Codigo = new int[1] ;
         H00B74_A1636ContagemResultado_ServicoSS = new int[1] ;
         H00B74_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         H00B74_A1765ContagemResultado_CodSrvSSVnc = new int[1] ;
         H00B74_n1765ContagemResultado_CodSrvSSVnc = new bool[] {false} ;
         lblWorkwithlink_Jsonclick = "";
         H00B75_A456ContagemResultado_Codigo = new int[1] ;
         H00B75_A602ContagemResultado_OSVinculada = new int[1] ;
         H00B75_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         H00B76_A456ContagemResultado_Codigo = new int[1] ;
         H00B76_A514ContagemResultado_Observacao = new String[] {""} ;
         H00B76_n514ContagemResultado_Observacao = new bool[] {false} ;
         A514ContagemResultado_Observacao = "";
         H00B77_AV12Count = new short[1] ;
         H00B78_AV12Count = new short[1] ;
         AV16Directory = new GxDirectory(context.GetPhysicalPath());
         GXt_char1 = "";
         H00B79_AV12Count = new short[1] ;
         sStyleString = "";
         lblViewtitle_Jsonclick = "";
         lblAbre_Jsonclick = "";
         lblFecha_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.viewcontagemresultado__default(),
            new Object[][] {
                new Object[] {
               H00B72_A494ContagemResultado_Descricao, H00B72_n494ContagemResultado_Descricao, H00B72_A456ContagemResultado_Codigo, H00B72_A493ContagemResultado_DemandaFM, H00B72_n493ContagemResultado_DemandaFM, H00B72_A457ContagemResultado_Demanda, H00B72_n457ContagemResultado_Demanda
               }
               , new Object[] {
               H00B73_A456ContagemResultado_Codigo, H00B73_A457ContagemResultado_Demanda, H00B73_n457ContagemResultado_Demanda, H00B73_A493ContagemResultado_DemandaFM, H00B73_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               H00B74_A602ContagemResultado_OSVinculada, H00B74_n602ContagemResultado_OSVinculada, H00B74_A456ContagemResultado_Codigo, H00B74_A1636ContagemResultado_ServicoSS, H00B74_n1636ContagemResultado_ServicoSS, H00B74_A1765ContagemResultado_CodSrvSSVnc, H00B74_n1765ContagemResultado_CodSrvSSVnc
               }
               , new Object[] {
               H00B75_A456ContagemResultado_Codigo, H00B75_A602ContagemResultado_OSVinculada, H00B75_n602ContagemResultado_OSVinculada
               }
               , new Object[] {
               H00B76_A456ContagemResultado_Codigo, H00B76_A514ContagemResultado_Observacao, H00B76_n514ContagemResultado_Observacao
               }
               , new Object[] {
               H00B77_AV12Count
               }
               , new Object[] {
               H00B78_AV12Count
               }
               , new Object[] {
               H00B79_AV12Count
               }
            }
         );
         WebComp_Tabbedview = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV19GXLvl9 ;
      private short AV14Index ;
      private short AV12Count ;
      private short cV12Count ;
      private short nGXWrapped ;
      private int AV9ContagemResultado_Codigo ;
      private int wcpOAV9ContagemResultado_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private int A1636ContagemResultado_ServicoSS ;
      private int A1765ContagemResultado_CodSrvSSVnc ;
      private int AV13ContagemResultado_OSVinculada ;
      private int idxLst ;
      private String AV7TabCode ;
      private String wcpOAV7TabCode ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String OldTabbedview ;
      private String WebComp_Tabbedview_Component ;
      private String scmdbuf ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String edtContagemResultado_OsFsOsFm_Internalname ;
      private String edtContagemResultado_Descricao_Internalname ;
      private String hsh ;
      private String lblWorkwithlink_Link ;
      private String lblWorkwithlink_Internalname ;
      private String lblViewtitle_Caption ;
      private String lblViewtitle_Internalname ;
      private String lblWorkwithlink_Jsonclick ;
      private String GXt_char1 ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String edtContagemResultado_Descricao_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String tblTablemergedviewtitle_Internalname ;
      private String lblViewtitle_Jsonclick ;
      private String edtContagemResultado_OsFsOsFm_Jsonclick ;
      private String tblTablemergedabre_Internalname ;
      private String lblAbre_Internalname ;
      private String lblAbre_Jsonclick ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String lblFecha_Internalname ;
      private String lblFecha_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n494ContagemResultado_Descricao ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool returnInSub ;
      private bool AV8Exists ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n1636ContagemResultado_ServicoSS ;
      private bool n1765ContagemResultado_CodSrvSSVnc ;
      private bool n514ContagemResultado_Observacao ;
      private String A514ContagemResultado_Observacao ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String A494ContagemResultado_Descricao ;
      private GXWebComponent WebComp_Tabbedview ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] H00B72_A494ContagemResultado_Descricao ;
      private bool[] H00B72_n494ContagemResultado_Descricao ;
      private int[] H00B72_A456ContagemResultado_Codigo ;
      private String[] H00B72_A493ContagemResultado_DemandaFM ;
      private bool[] H00B72_n493ContagemResultado_DemandaFM ;
      private String[] H00B72_A457ContagemResultado_Demanda ;
      private bool[] H00B72_n457ContagemResultado_Demanda ;
      private int[] H00B73_A456ContagemResultado_Codigo ;
      private String[] H00B73_A457ContagemResultado_Demanda ;
      private bool[] H00B73_n457ContagemResultado_Demanda ;
      private String[] H00B73_A493ContagemResultado_DemandaFM ;
      private bool[] H00B73_n493ContagemResultado_DemandaFM ;
      private int[] H00B74_A602ContagemResultado_OSVinculada ;
      private bool[] H00B74_n602ContagemResultado_OSVinculada ;
      private int[] H00B74_A456ContagemResultado_Codigo ;
      private int[] H00B74_A1636ContagemResultado_ServicoSS ;
      private bool[] H00B74_n1636ContagemResultado_ServicoSS ;
      private int[] H00B74_A1765ContagemResultado_CodSrvSSVnc ;
      private bool[] H00B74_n1765ContagemResultado_CodSrvSSVnc ;
      private int[] H00B75_A456ContagemResultado_Codigo ;
      private int[] H00B75_A602ContagemResultado_OSVinculada ;
      private bool[] H00B75_n602ContagemResultado_OSVinculada ;
      private int[] H00B76_A456ContagemResultado_Codigo ;
      private String[] H00B76_A514ContagemResultado_Observacao ;
      private bool[] H00B76_n514ContagemResultado_Observacao ;
      private short[] H00B77_AV12Count ;
      private short[] H00B78_AV12Count ;
      private short[] H00B79_AV12Count ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem ))]
      private IGxCollection AV10Tabs ;
      private GxDirectory AV16Directory ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem AV11Tab ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class viewcontagemresultado__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00B72 ;
          prmH00B72 = new Object[] {
          new Object[] {"@AV9ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B73 ;
          prmH00B73 = new Object[] {
          new Object[] {"@AV9ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B74 ;
          prmH00B74 = new Object[] {
          new Object[] {"@AV9ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B75 ;
          prmH00B75 = new Object[] {
          new Object[] {"@AV9ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B76 ;
          prmH00B76 = new Object[] {
          new Object[] {"@AV9ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B77 ;
          prmH00B77 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B78 ;
          prmH00B78 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B79 ;
          prmH00B79 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00B72", "SELECT [ContagemResultado_Descricao], [ContagemResultado_Codigo], [ContagemResultado_DemandaFM], [ContagemResultado_Demanda] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV9ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B72,1,0,true,true )
             ,new CursorDef("H00B73", "SELECT [ContagemResultado_Codigo], [ContagemResultado_Demanda], [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV9ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B73,1,0,false,true )
             ,new CursorDef("H00B74", "SELECT TOP 1 T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ServicoSS], T2.[ContagemResultado_ServicoSS] AS ContagemResultado_CodSrvSSVnc FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada]) WHERE T1.[ContagemResultado_Codigo] = @AV9ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B74,1,0,false,true )
             ,new CursorDef("H00B75", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV9ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B75,1,0,false,true )
             ,new CursorDef("H00B76", "SELECT [ContagemResultado_Codigo], [ContagemResultado_Observacao] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV9ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B76,1,0,true,true )
             ,new CursorDef("H00B77", "SELECT COUNT(*) FROM [ContagemResultadoNotas] WITH (NOLOCK) WHERE [ContagemResultadoNota_DemandaCod] = @ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B77,1,0,true,false )
             ,new CursorDef("H00B78", "SELECT COUNT(*) FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B78,1,0,true,false )
             ,new CursorDef("H00B79", "SELECT COUNT(*) FROM [ContagemResultadoChckLstLog] WITH (NOLOCK) WHERE [ContagemResultadoChckLstLog_OSCodigo] = @ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B79,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 5 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 6 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 7 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
