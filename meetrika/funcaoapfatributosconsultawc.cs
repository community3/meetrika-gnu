/*
               File: FuncaoAPFAtributosConsultaWC
        Description: Consulta Atributos da FuncaoAPF
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:12.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcaoapfatributosconsultawc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public funcaoapfatributosconsultawc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public funcaoapfatributosconsultawc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoAPF_Codigo )
      {
         this.A165FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbAtributos_TipoDados = new GXCombobox();
         chkFuncoesAPFAtributos_Regra = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A165FuncaoAPF_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridatributos") == 0 )
               {
                  nRC_GXsfl_31 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_31_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_31_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGridatributos_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridatributos") == 0 )
               {
                  subGridatributos_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGridatributos_refresh( subGridatributos_Rows, A165FuncaoAPF_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAA52( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSA52( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Consulta Atributos da FuncaoAPF") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311771225");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("funcaoapfatributosconsultawc.aspx") + "?" + UrlEncode("" +A165FuncaoAPF_Codigo)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_31", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_31), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Width", StringUtil.RTrim( Dvpanel__Width));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Cls", StringUtil.RTrim( Dvpanel__Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Title", StringUtil.RTrim( Dvpanel__Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Collapsible", StringUtil.BoolToStr( Dvpanel__Collapsible));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Collapsed", StringUtil.BoolToStr( Dvpanel__Collapsed));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Autowidth", StringUtil.BoolToStr( Dvpanel__Autowidth));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Autoheight", StringUtil.BoolToStr( Dvpanel__Autoheight));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Showcollapseicon", StringUtil.BoolToStr( Dvpanel__Showcollapseicon));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Iconposition", StringUtil.RTrim( Dvpanel__Iconposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL__Autoscroll", StringUtil.BoolToStr( Dvpanel__Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormA52( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("funcaoapfatributosconsultawc.js", "?2020311771240");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "FuncaoAPFAtributosConsultaWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Consulta Atributos da FuncaoAPF" ;
      }

      protected void WBA50( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "funcaoapfatributosconsultawc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
            }
            wb_table1_2_A52( true) ;
         }
         else
         {
            wb_table1_2_A52( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_A52e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTA52( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Consulta Atributos da FuncaoAPF", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPA50( ) ;
            }
         }
      }

      protected void WSA52( )
      {
         STARTA52( ) ;
         EVTA52( ) ;
      }

      protected void EVTA52( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDATRIBUTOSPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA50( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDATRIBUTOSPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgridatributos_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgridatributos_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgridatributos_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgridatributos_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "GRIDATRIBUTOS.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA50( ) ;
                              }
                              nGXsfl_31_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_31_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_31_idx), 4, 0)), 4, "0");
                              SubsflControlProps_312( ) ;
                              if ( StringUtil.Len( sPrefix) == 0 )
                              {
                                 A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", "."));
                              }
                              A364FuncaoAPFAtributos_AtributosCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPFAtributos_AtributosCod_Internalname), ",", "."));
                              A365FuncaoAPFAtributos_AtributosNom = StringUtil.Upper( cgiGet( edtFuncaoAPFAtributos_AtributosNom_Internalname));
                              n365FuncaoAPFAtributos_AtributosNom = false;
                              cmbAtributos_TipoDados.Name = cmbAtributos_TipoDados_Internalname;
                              cmbAtributos_TipoDados.CurrentValue = cgiGet( cmbAtributos_TipoDados_Internalname);
                              A178Atributos_TipoDados = cgiGet( cmbAtributos_TipoDados_Internalname);
                              n178Atributos_TipoDados = false;
                              A389FuncoesAPFAtributos_Regra = StringUtil.StrToBool( cgiGet( chkFuncoesAPFAtributos_Regra_Internalname));
                              n389FuncoesAPFAtributos_Regra = false;
                              A383FuncoesAPFAtributos_Code = cgiGet( edtFuncoesAPFAtributos_Code_Internalname);
                              n383FuncoesAPFAtributos_Code = false;
                              A384FuncoesAPFAtributos_Nome = StringUtil.Upper( cgiGet( edtFuncoesAPFAtributos_Nome_Internalname));
                              n384FuncoesAPFAtributos_Nome = false;
                              A367FuncaoAPFAtributos_AtrTabelaNom = StringUtil.Upper( cgiGet( edtFuncaoAPFAtributos_AtrTabelaNom_Internalname));
                              n367FuncaoAPFAtributos_AtrTabelaNom = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E11A52 */
                                          E11A52 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDATRIBUTOS.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E12A52 */
                                          E12A52 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E13A52 */
                                          E13A52 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPA50( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEA52( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormA52( ) ;
            }
         }
      }

      protected void PAA52( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "ATRIBUTOS_TIPODADOS_" + sGXsfl_31_idx;
            cmbAtributos_TipoDados.Name = GXCCtl;
            cmbAtributos_TipoDados.WebTags = "";
            cmbAtributos_TipoDados.addItem("", "Desconhecido", 0);
            cmbAtributos_TipoDados.addItem("N", "Numeric", 0);
            cmbAtributos_TipoDados.addItem("C", "Character", 0);
            cmbAtributos_TipoDados.addItem("VC", "Varchar", 0);
            cmbAtributos_TipoDados.addItem("D", "Date", 0);
            cmbAtributos_TipoDados.addItem("DT", "Date Time", 0);
            cmbAtributos_TipoDados.addItem("Bool", "Boolean", 0);
            cmbAtributos_TipoDados.addItem("Blob", "Blob", 0);
            cmbAtributos_TipoDados.addItem("Outr", "Outros", 0);
            if ( cmbAtributos_TipoDados.ItemCount > 0 )
            {
               A178Atributos_TipoDados = cmbAtributos_TipoDados.getValidValue(A178Atributos_TipoDados);
               n178Atributos_TipoDados = false;
            }
            GXCCtl = "FUNCOESAPFATRIBUTOS_REGRA_" + sGXsfl_31_idx;
            chkFuncoesAPFAtributos_Regra.Name = GXCCtl;
            chkFuncoesAPFAtributos_Regra.WebTags = "";
            chkFuncoesAPFAtributos_Regra.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkFuncoesAPFAtributos_Regra_Internalname, "TitleCaption", chkFuncoesAPFAtributos_Regra.Caption);
            chkFuncoesAPFAtributos_Regra.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridatributos_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_312( ) ;
         while ( nGXsfl_31_idx <= nRC_GXsfl_31 )
         {
            sendrow_312( ) ;
            nGXsfl_31_idx = (short)(((subGridatributos_Islastpage==1)&&(nGXsfl_31_idx+1>subGridatributos_Recordsperpage( )) ? 1 : nGXsfl_31_idx+1));
            sGXsfl_31_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_31_idx), 4, 0)), 4, "0");
            SubsflControlProps_312( ) ;
         }
         context.GX_webresponse.AddString(GridatributosContainer.ToJavascriptSource());
         /* End function gxnrGridatributos_newrow */
      }

      protected void gxgrGridatributos_refresh( int subGridatributos_Rows ,
                                                int A165FuncaoAPF_Codigo ,
                                                String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
         GRIDATRIBUTOS_nCurrentRecord = 0;
         RFA52( ) ;
         /* End function gxgrGridatributos_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A364FuncaoAPFAtributos_AtributosCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCOESAPFATRIBUTOS_REGRA", GetSecureSignedToken( sPrefix, A389FuncoesAPFAtributos_Regra));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCOESAPFATRIBUTOS_REGRA", StringUtil.BoolToStr( A389FuncoesAPFAtributos_Regra));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCOESAPFATRIBUTOS_CODE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A383FuncoesAPFAtributos_Code, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCOESAPFATRIBUTOS_CODE", A383FuncoesAPFAtributos_Code);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCOESAPFATRIBUTOS_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A384FuncoesAPFAtributos_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCOESAPFATRIBUTOS_NOME", StringUtil.RTrim( A384FuncoesAPFAtributos_Nome));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFA52( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFA52( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridatributosContainer.ClearRows();
         }
         wbStart = 31;
         /* Execute user event: E13A52 */
         E13A52 ();
         nGXsfl_31_idx = 1;
         sGXsfl_31_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_31_idx), 4, 0)), 4, "0");
         SubsflControlProps_312( ) ;
         nGXsfl_31_Refreshing = 1;
         GridatributosContainer.AddObjectProperty("GridName", "Gridatributos");
         GridatributosContainer.AddObjectProperty("CmpContext", sPrefix);
         GridatributosContainer.AddObjectProperty("InMasterPage", "false");
         GridatributosContainer.AddObjectProperty("Class", "WorkWith");
         GridatributosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridatributosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridatributosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Backcolorstyle), 1, 0, ".", "")));
         GridatributosContainer.PageSize = subGridatributos_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_312( ) ;
            GXPagingFrom2 = (int)(((subGridatributos_Rows==0) ? 1 : GRIDATRIBUTOS_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGridatributos_Rows==0) ? 10000 : GRIDATRIBUTOS_nFirstRecordOnPage+subGridatributos_Recordsperpage( )+1));
            /* Using cursor H00A52 */
            pr_default.execute(0, new Object[] {A165FuncaoAPF_Codigo, GXPagingFrom2, GXPagingTo2});
            nGXsfl_31_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGridatributos_Rows == 0 ) || ( GRIDATRIBUTOS_nCurrentRecord < subGridatributos_Recordsperpage( ) ) ) ) )
            {
               A366FuncaoAPFAtributos_AtrTabelaCod = H00A52_A366FuncaoAPFAtributos_AtrTabelaCod[0];
               n366FuncaoAPFAtributos_AtrTabelaCod = H00A52_n366FuncaoAPFAtributos_AtrTabelaCod[0];
               A180Atributos_Ativo = H00A52_A180Atributos_Ativo[0];
               n180Atributos_Ativo = H00A52_n180Atributos_Ativo[0];
               A367FuncaoAPFAtributos_AtrTabelaNom = H00A52_A367FuncaoAPFAtributos_AtrTabelaNom[0];
               n367FuncaoAPFAtributos_AtrTabelaNom = H00A52_n367FuncaoAPFAtributos_AtrTabelaNom[0];
               A384FuncoesAPFAtributos_Nome = H00A52_A384FuncoesAPFAtributos_Nome[0];
               n384FuncoesAPFAtributos_Nome = H00A52_n384FuncoesAPFAtributos_Nome[0];
               A383FuncoesAPFAtributos_Code = H00A52_A383FuncoesAPFAtributos_Code[0];
               n383FuncoesAPFAtributos_Code = H00A52_n383FuncoesAPFAtributos_Code[0];
               A389FuncoesAPFAtributos_Regra = H00A52_A389FuncoesAPFAtributos_Regra[0];
               n389FuncoesAPFAtributos_Regra = H00A52_n389FuncoesAPFAtributos_Regra[0];
               A178Atributos_TipoDados = H00A52_A178Atributos_TipoDados[0];
               n178Atributos_TipoDados = H00A52_n178Atributos_TipoDados[0];
               A365FuncaoAPFAtributos_AtributosNom = H00A52_A365FuncaoAPFAtributos_AtributosNom[0];
               n365FuncaoAPFAtributos_AtributosNom = H00A52_n365FuncaoAPFAtributos_AtributosNom[0];
               A364FuncaoAPFAtributos_AtributosCod = H00A52_A364FuncaoAPFAtributos_AtributosCod[0];
               A366FuncaoAPFAtributos_AtrTabelaCod = H00A52_A366FuncaoAPFAtributos_AtrTabelaCod[0];
               n366FuncaoAPFAtributos_AtrTabelaCod = H00A52_n366FuncaoAPFAtributos_AtrTabelaCod[0];
               A180Atributos_Ativo = H00A52_A180Atributos_Ativo[0];
               n180Atributos_Ativo = H00A52_n180Atributos_Ativo[0];
               A178Atributos_TipoDados = H00A52_A178Atributos_TipoDados[0];
               n178Atributos_TipoDados = H00A52_n178Atributos_TipoDados[0];
               A365FuncaoAPFAtributos_AtributosNom = H00A52_A365FuncaoAPFAtributos_AtributosNom[0];
               n365FuncaoAPFAtributos_AtributosNom = H00A52_n365FuncaoAPFAtributos_AtributosNom[0];
               A367FuncaoAPFAtributos_AtrTabelaNom = H00A52_A367FuncaoAPFAtributos_AtrTabelaNom[0];
               n367FuncaoAPFAtributos_AtrTabelaNom = H00A52_n367FuncaoAPFAtributos_AtrTabelaNom[0];
               /* Execute user event: E12A52 */
               E12A52 ();
               pr_default.readNext(0);
            }
            GRIDATRIBUTOS_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 31;
            WBA50( ) ;
         }
         nGXsfl_31_Refreshing = 0;
      }

      protected int subGridatributos_Pagecount( )
      {
         GRIDATRIBUTOS_nRecordCount = subGridatributos_Recordcount( );
         if ( ((int)((GRIDATRIBUTOS_nRecordCount) % (subGridatributos_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDATRIBUTOS_nRecordCount/ (decimal)(subGridatributos_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDATRIBUTOS_nRecordCount/ (decimal)(subGridatributos_Recordsperpage( ))))+1) ;
      }

      protected int subGridatributos_Recordcount( )
      {
         /* Using cursor H00A53 */
         pr_default.execute(1, new Object[] {A165FuncaoAPF_Codigo});
         GRIDATRIBUTOS_nRecordCount = H00A53_AGRIDATRIBUTOS_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRIDATRIBUTOS_nRecordCount) ;
      }

      protected int subGridatributos_Recordsperpage( )
      {
         if ( subGridatributos_Rows > 0 )
         {
            return subGridatributos_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridatributos_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRIDATRIBUTOS_nFirstRecordOnPage/ (decimal)(subGridatributos_Recordsperpage( ))))+1) ;
      }

      protected short subgridatributos_firstpage( )
      {
         GRIDATRIBUTOS_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, A165FuncaoAPF_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridatributos_nextpage( )
      {
         GRIDATRIBUTOS_nRecordCount = subGridatributos_Recordcount( );
         if ( ( GRIDATRIBUTOS_nRecordCount >= subGridatributos_Recordsperpage( ) ) && ( GRIDATRIBUTOS_nEOF == 0 ) )
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nFirstRecordOnPage+subGridatributos_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, A165FuncaoAPF_Codigo, sPrefix) ;
         }
         return (short)(((GRIDATRIBUTOS_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridatributos_previouspage( )
      {
         if ( GRIDATRIBUTOS_nFirstRecordOnPage >= subGridatributos_Recordsperpage( ) )
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nFirstRecordOnPage-subGridatributos_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, A165FuncaoAPF_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridatributos_lastpage( )
      {
         GRIDATRIBUTOS_nRecordCount = subGridatributos_Recordcount( );
         if ( GRIDATRIBUTOS_nRecordCount > subGridatributos_Recordsperpage( ) )
         {
            if ( ((int)((GRIDATRIBUTOS_nRecordCount) % (subGridatributos_Recordsperpage( )))) == 0 )
            {
               GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nRecordCount-subGridatributos_Recordsperpage( ));
            }
            else
            {
               GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nRecordCount-((int)((GRIDATRIBUTOS_nRecordCount) % (subGridatributos_Recordsperpage( )))));
            }
         }
         else
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, A165FuncaoAPF_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgridatributos_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(subGridatributos_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, A165FuncaoAPF_Codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPA50( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11A52 */
         E11A52 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            nRC_GXsfl_31 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_31"), ",", "."));
            wcpOA165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA165FuncaoAPF_Codigo"), ",", "."));
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage"), ",", "."));
            GRIDATRIBUTOS_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOS_nEOF"), ",", "."));
            subGridatributos_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOS_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
            Dvpanel__Width = cgiGet( sPrefix+"DVPANEL__Width");
            Dvpanel__Cls = cgiGet( sPrefix+"DVPANEL__Cls");
            Dvpanel__Title = cgiGet( sPrefix+"DVPANEL__Title");
            Dvpanel__Collapsible = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL__Collapsible"));
            Dvpanel__Collapsed = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL__Collapsed"));
            Dvpanel__Autowidth = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL__Autowidth"));
            Dvpanel__Autoheight = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL__Autoheight"));
            Dvpanel__Showcollapseicon = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL__Showcollapseicon"));
            Dvpanel__Iconposition = cgiGet( sPrefix+"DVPANEL__Iconposition");
            Dvpanel__Autoscroll = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL__Autoscroll"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11A52 */
         E11A52 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11A52( )
      {
         /* Start Routine */
         subGridatributos_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
      }

      private void E12A52( )
      {
         /* Gridatributos_Load Routine */
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 31;
         }
         sendrow_312( ) ;
         GRIDATRIBUTOS_nCurrentRecord = (long)(GRIDATRIBUTOS_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_31_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(31, GridatributosRow);
         }
      }

      protected void E13A52( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV5Context) ;
      }

      protected void wb_table1_2_A52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table2_8_A52( true) ;
         }
         else
         {
            wb_table2_8_A52( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_A52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table100x100'>") ;
            wb_table3_20_A52( true) ;
         }
         else
         {
            wb_table3_20_A52( false) ;
         }
         return  ;
      }

      protected void wb_table3_20_A52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_A52e( true) ;
         }
         else
         {
            wb_table1_2_A52e( false) ;
         }
      }

      protected void wb_table3_20_A52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableSearch", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DVPANEL_Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"DVPANEL_Container"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_25_A52( true) ;
         }
         else
         {
            wb_table4_25_A52( false) ;
         }
         return  ;
      }

      protected void wb_table4_25_A52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_20_A52e( true) ;
         }
         else
         {
            wb_table3_20_A52e( false) ;
         }
      }

      protected void wb_table4_25_A52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Table'>") ;
            wb_table5_28_A52( true) ;
         }
         else
         {
            wb_table5_28_A52( false) ;
         }
         return  ;
      }

      protected void wb_table5_28_A52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_25_A52e( true) ;
         }
         else
         {
            wb_table4_25_A52e( false) ;
         }
      }

      protected void wb_table5_28_A52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable4_Internalname, tblTable4_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='TableContentNoMargin'>") ;
            /*  Grid Control  */
            GridatributosContainer.SetWrapped(nGXWrapped);
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridatributosContainer"+"DivS\" data-gxgridid=\"31\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridatributos_Internalname, subGridatributos_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridatributos_Backcolorstyle == 0 )
               {
                  subGridatributos_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridatributos_Class) > 0 )
                  {
                     subGridatributos_Linesclass = subGridatributos_Class+"Title";
                  }
               }
               else
               {
                  subGridatributos_Titlebackstyle = 1;
                  if ( subGridatributos_Backcolorstyle == 1 )
                  {
                     subGridatributos_Titlebackcolor = subGridatributos_Allbackcolor;
                     if ( StringUtil.Len( subGridatributos_Class) > 0 )
                     {
                        subGridatributos_Linesclass = subGridatributos_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridatributos_Class) > 0 )
                     {
                        subGridatributos_Linesclass = subGridatributos_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Fun��o de Transa��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Atributo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Regra?") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tabela") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridatributosContainer.AddObjectProperty("GridName", "Gridatributos");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridatributosContainer = new GXWebGrid( context);
               }
               else
               {
                  GridatributosContainer.Clear();
               }
               GridatributosContainer.SetWrapped(nGXWrapped);
               GridatributosContainer.AddObjectProperty("GridName", "Gridatributos");
               GridatributosContainer.AddObjectProperty("Class", "WorkWith");
               GridatributosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Backcolorstyle), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("CmpContext", sPrefix);
               GridatributosContainer.AddObjectProperty("InMasterPage", "false");
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.RTrim( A365FuncaoAPFAtributos_AtributosNom));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.RTrim( A178Atributos_TipoDados));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A389FuncoesAPFAtributos_Regra));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", A383FuncoesAPFAtributos_Code);
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.RTrim( A384FuncoesAPFAtributos_Nome));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.RTrim( A367FuncaoAPFAtributos_AtrTabelaNom));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Allowselection), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Selectioncolor), 9, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Allowhovering), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Hoveringcolor), 9, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Allowcollapsing), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 31 )
         {
            wbEnd = 0;
            nRC_GXsfl_31 = (short)(nGXsfl_31_idx-1);
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridatributosContainer.AddObjectProperty("GRIDATRIBUTOS_nEOF", GRIDATRIBUTOS_nEOF);
               GridatributosContainer.AddObjectProperty("GRIDATRIBUTOS_nFirstRecordOnPage", GRIDATRIBUTOS_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridatributosContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Gridatributos", GridatributosContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridatributosContainerData", GridatributosContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridatributosContainerData"+"V", GridatributosContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridatributosContainerData"+"V"+"\" value='"+GridatributosContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_28_A52e( true) ;
         }
         else
         {
            wb_table5_28_A52e( false) ;
         }
      }

      protected void wb_table2_8_A52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table6_11_A52( true) ;
         }
         else
         {
            wb_table6_11_A52( false) ;
         }
         return  ;
      }

      protected void wb_table6_11_A52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table7_15_A52( true) ;
         }
         else
         {
            wb_table7_15_A52( false) ;
         }
         return  ;
      }

      protected void wb_table7_15_A52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_A52e( true) ;
         }
         else
         {
            wb_table2_8_A52e( false) ;
         }
      }

      protected void wb_table7_15_A52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_15_A52e( true) ;
         }
         else
         {
            wb_table7_15_A52e( false) ;
         }
      }

      protected void wb_table6_11_A52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_11_A52e( true) ;
         }
         else
         {
            wb_table6_11_A52e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A165FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,0));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAA52( ) ;
         WSA52( ) ;
         WEA52( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA165FuncaoAPF_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAA52( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "funcaoapfatributosconsultawc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAA52( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A165FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,2));
         }
         wcpOA165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA165FuncaoAPF_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A165FuncaoAPF_Codigo != wcpOA165FuncaoAPF_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA165FuncaoAPF_Codigo = cgiGet( sPrefix+"A165FuncaoAPF_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA165FuncaoAPF_Codigo) > 0 )
         {
            A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA165FuncaoAPF_Codigo), ",", "."));
         }
         else
         {
            A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A165FuncaoAPF_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAA52( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSA52( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSA52( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A165FuncaoAPF_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA165FuncaoAPF_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A165FuncaoAPF_Codigo_CTRL", StringUtil.RTrim( sCtrlA165FuncaoAPF_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEA52( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031177130");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("funcaoapfatributosconsultawc.js", "?202031177130");
            context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_312( )
      {
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO_"+sGXsfl_31_idx;
         edtFuncaoAPFAtributos_AtributosCod_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_"+sGXsfl_31_idx;
         edtFuncaoAPFAtributos_AtributosNom_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_"+sGXsfl_31_idx;
         cmbAtributos_TipoDados_Internalname = sPrefix+"ATRIBUTOS_TIPODADOS_"+sGXsfl_31_idx;
         chkFuncoesAPFAtributos_Regra_Internalname = sPrefix+"FUNCOESAPFATRIBUTOS_REGRA_"+sGXsfl_31_idx;
         edtFuncoesAPFAtributos_Code_Internalname = sPrefix+"FUNCOESAPFATRIBUTOS_CODE_"+sGXsfl_31_idx;
         edtFuncoesAPFAtributos_Nome_Internalname = sPrefix+"FUNCOESAPFATRIBUTOS_NOME_"+sGXsfl_31_idx;
         edtFuncaoAPFAtributos_AtrTabelaNom_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRTABELANOM_"+sGXsfl_31_idx;
      }

      protected void SubsflControlProps_fel_312( )
      {
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO_"+sGXsfl_31_fel_idx;
         edtFuncaoAPFAtributos_AtributosCod_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_"+sGXsfl_31_fel_idx;
         edtFuncaoAPFAtributos_AtributosNom_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_"+sGXsfl_31_fel_idx;
         cmbAtributos_TipoDados_Internalname = sPrefix+"ATRIBUTOS_TIPODADOS_"+sGXsfl_31_fel_idx;
         chkFuncoesAPFAtributos_Regra_Internalname = sPrefix+"FUNCOESAPFATRIBUTOS_REGRA_"+sGXsfl_31_fel_idx;
         edtFuncoesAPFAtributos_Code_Internalname = sPrefix+"FUNCOESAPFATRIBUTOS_CODE_"+sGXsfl_31_fel_idx;
         edtFuncoesAPFAtributos_Nome_Internalname = sPrefix+"FUNCOESAPFATRIBUTOS_NOME_"+sGXsfl_31_fel_idx;
         edtFuncaoAPFAtributos_AtrTabelaNom_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRTABELANOM_"+sGXsfl_31_fel_idx;
      }

      protected void sendrow_312( )
      {
         SubsflControlProps_312( ) ;
         WBA50( ) ;
         if ( ( subGridatributos_Rows * 1 == 0 ) || ( nGXsfl_31_idx <= subGridatributos_Recordsperpage( ) * 1 ) )
         {
            GridatributosRow = GXWebRow.GetNew(context,GridatributosContainer);
            if ( subGridatributos_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridatributos_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
               {
                  subGridatributos_Linesclass = subGridatributos_Class+"Odd";
               }
            }
            else if ( subGridatributos_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridatributos_Backstyle = 0;
               subGridatributos_Backcolor = subGridatributos_Allbackcolor;
               if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
               {
                  subGridatributos_Linesclass = subGridatributos_Class+"Uniform";
               }
            }
            else if ( subGridatributos_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridatributos_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
               {
                  subGridatributos_Linesclass = subGridatributos_Class+"Odd";
               }
               subGridatributos_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridatributos_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridatributos_Backstyle = 1;
               if ( ((int)((nGXsfl_31_idx) % (2))) == 0 )
               {
                  subGridatributos_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
                  {
                     subGridatributos_Linesclass = subGridatributos_Class+"Even";
                  }
               }
               else
               {
                  subGridatributos_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
                  {
                     subGridatributos_Linesclass = subGridatributos_Class+"Odd";
                  }
               }
            }
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridatributos_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_31_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_Codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)31,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFAtributos_AtributosCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A364FuncaoAPFAtributos_AtributosCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFAtributos_AtributosCod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)31,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFAtributos_AtributosNom_Internalname,StringUtil.RTrim( A365FuncaoAPFAtributos_AtributosNom),StringUtil.RTrim( context.localUtil.Format( A365FuncaoAPFAtributos_AtributosNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFAtributos_AtributosNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)31,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "ATRIBUTOS_TIPODADOS_" + sGXsfl_31_idx;
            cmbAtributos_TipoDados.Name = GXCCtl;
            cmbAtributos_TipoDados.WebTags = "";
            cmbAtributos_TipoDados.addItem("", "Desconhecido", 0);
            cmbAtributos_TipoDados.addItem("N", "Numeric", 0);
            cmbAtributos_TipoDados.addItem("C", "Character", 0);
            cmbAtributos_TipoDados.addItem("VC", "Varchar", 0);
            cmbAtributos_TipoDados.addItem("D", "Date", 0);
            cmbAtributos_TipoDados.addItem("DT", "Date Time", 0);
            cmbAtributos_TipoDados.addItem("Bool", "Boolean", 0);
            cmbAtributos_TipoDados.addItem("Blob", "Blob", 0);
            cmbAtributos_TipoDados.addItem("Outr", "Outros", 0);
            if ( cmbAtributos_TipoDados.ItemCount > 0 )
            {
               A178Atributos_TipoDados = cmbAtributos_TipoDados.getValidValue(A178Atributos_TipoDados);
               n178Atributos_TipoDados = false;
            }
            /* ComboBox */
            GridatributosRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbAtributos_TipoDados,(String)cmbAtributos_TipoDados_Internalname,StringUtil.RTrim( A178Atributos_TipoDados),(short)1,(String)cmbAtributos_TipoDados_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbAtributos_TipoDados.CurrentValue = StringUtil.RTrim( A178Atributos_TipoDados);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbAtributos_TipoDados_Internalname, "Values", (String)(cmbAtributos_TipoDados.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "Attribute";
            StyleString = "";
            GridatributosRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkFuncoesAPFAtributos_Regra_Internalname,StringUtil.BoolToStr( A389FuncoesAPFAtributos_Regra),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncoesAPFAtributos_Code_Internalname,(String)A383FuncoesAPFAtributos_Code,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncoesAPFAtributos_Code_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)31,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncoesAPFAtributos_Nome_Internalname,StringUtil.RTrim( A384FuncoesAPFAtributos_Nome),StringUtil.RTrim( context.localUtil.Format( A384FuncoesAPFAtributos_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncoesAPFAtributos_Nome_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)31,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFAtributos_AtrTabelaNom_Internalname,StringUtil.RTrim( A367FuncaoAPFAtributos_AtrTabelaNom),StringUtil.RTrim( context.localUtil.Format( A367FuncaoAPFAtributos_AtrTabelaNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFAtributos_AtrTabelaNom_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)31,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD"+"_"+sGXsfl_31_idx, GetSecureSignedToken( sPrefix+sGXsfl_31_idx, context.localUtil.Format( (decimal)(A364FuncaoAPFAtributos_AtributosCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCOESAPFATRIBUTOS_REGRA"+"_"+sGXsfl_31_idx, GetSecureSignedToken( sPrefix+sGXsfl_31_idx, A389FuncoesAPFAtributos_Regra));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCOESAPFATRIBUTOS_CODE"+"_"+sGXsfl_31_idx, GetSecureSignedToken( sPrefix+sGXsfl_31_idx, StringUtil.RTrim( context.localUtil.Format( A383FuncoesAPFAtributos_Code, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCOESAPFATRIBUTOS_NOME"+"_"+sGXsfl_31_idx, GetSecureSignedToken( sPrefix+sGXsfl_31_idx, StringUtil.RTrim( context.localUtil.Format( A384FuncoesAPFAtributos_Nome, "@!"))));
            GridatributosContainer.AddRow(GridatributosRow);
            nGXsfl_31_idx = (short)(((subGridatributos_Islastpage==1)&&(nGXsfl_31_idx+1>subGridatributos_Recordsperpage( )) ? 1 : nGXsfl_31_idx+1));
            sGXsfl_31_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_31_idx), 4, 0)), 4, "0");
            SubsflControlProps_312( ) ;
         }
         /* End function sendrow_312 */
      }

      protected void init_default_properties( )
      {
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTableheader_Internalname = sPrefix+"TABLEHEADER";
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO";
         edtFuncaoAPFAtributos_AtributosCod_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD";
         edtFuncaoAPFAtributos_AtributosNom_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
         cmbAtributos_TipoDados_Internalname = sPrefix+"ATRIBUTOS_TIPODADOS";
         chkFuncoesAPFAtributos_Regra_Internalname = sPrefix+"FUNCOESAPFATRIBUTOS_REGRA";
         edtFuncoesAPFAtributos_Code_Internalname = sPrefix+"FUNCOESAPFATRIBUTOS_CODE";
         edtFuncoesAPFAtributos_Nome_Internalname = sPrefix+"FUNCOESAPFATRIBUTOS_NOME";
         edtFuncaoAPFAtributos_AtrTabelaNom_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRTABELANOM";
         tblTable4_Internalname = sPrefix+"TABLE4";
         tblTable2_Internalname = sPrefix+"TABLE2";
         Dvpanel__Internalname = sPrefix+"DVPANEL_";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         tblTable1_Internalname = sPrefix+"TABLE1";
         Form.Internalname = sPrefix+"FORM";
         subGridatributos_Internalname = sPrefix+"GRIDATRIBUTOS";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtFuncaoAPFAtributos_AtrTabelaNom_Jsonclick = "";
         edtFuncoesAPFAtributos_Nome_Jsonclick = "";
         edtFuncoesAPFAtributos_Code_Jsonclick = "";
         cmbAtributos_TipoDados_Jsonclick = "";
         edtFuncaoAPFAtributos_AtributosNom_Jsonclick = "";
         edtFuncaoAPFAtributos_AtributosCod_Jsonclick = "";
         edtFuncaoAPF_Codigo_Jsonclick = "";
         subGridatributos_Allowcollapsing = 0;
         subGridatributos_Allowselection = 0;
         subGridatributos_Class = "WorkWith";
         subGridatributos_Backcolorstyle = 3;
         chkFuncoesAPFAtributos_Regra.Caption = "";
         Dvpanel__Autoscroll = Convert.ToBoolean( 0);
         Dvpanel__Iconposition = "left";
         Dvpanel__Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel__Autoheight = Convert.ToBoolean( -1);
         Dvpanel__Autowidth = Convert.ToBoolean( 0);
         Dvpanel__Collapsed = Convert.ToBoolean( 0);
         Dvpanel__Collapsible = Convert.ToBoolean( 0);
         Dvpanel__Title = "Atributos";
         Dvpanel__Cls = "GXUI-DVelop-Panel";
         Dvpanel__Width = "100%";
         subGridatributos_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRIDATRIBUTOS.LOAD","{handler:'E12A52',iparms:[],oparms:[]}");
         setEventMetadata("GRIDATRIBUTOS_FIRSTPAGE","{handler:'subgridatributos_firstpage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRIDATRIBUTOS_PREVPAGE","{handler:'subgridatributos_previouspage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRIDATRIBUTOS_NEXTPAGE","{handler:'subgridatributos_nextpage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRIDATRIBUTOS_LASTPAGE","{handler:'subgridatributos_lastpage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A365FuncaoAPFAtributos_AtributosNom = "";
         A178Atributos_TipoDados = "";
         A383FuncoesAPFAtributos_Code = "";
         A384FuncoesAPFAtributos_Nome = "";
         A367FuncaoAPFAtributos_AtrTabelaNom = "";
         GXCCtl = "";
         GridatributosContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00A52_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         H00A52_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         H00A52_A165FuncaoAPF_Codigo = new int[1] ;
         H00A52_A180Atributos_Ativo = new bool[] {false} ;
         H00A52_n180Atributos_Ativo = new bool[] {false} ;
         H00A52_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         H00A52_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         H00A52_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         H00A52_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         H00A52_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         H00A52_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         H00A52_A389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         H00A52_n389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         H00A52_A178Atributos_TipoDados = new String[] {""} ;
         H00A52_n178Atributos_TipoDados = new bool[] {false} ;
         H00A52_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         H00A52_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         H00A52_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         H00A53_AGRIDATRIBUTOS_nRecordCount = new long[1] ;
         GridatributosRow = new GXWebRow();
         AV5Context = new wwpbaseobjects.SdtWWPContext(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         subGridatributos_Linesclass = "";
         GridatributosColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA165FuncaoAPF_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcaoapfatributosconsultawc__default(),
            new Object[][] {
                new Object[] {
               H00A52_A366FuncaoAPFAtributos_AtrTabelaCod, H00A52_n366FuncaoAPFAtributos_AtrTabelaCod, H00A52_A165FuncaoAPF_Codigo, H00A52_A180Atributos_Ativo, H00A52_n180Atributos_Ativo, H00A52_A367FuncaoAPFAtributos_AtrTabelaNom, H00A52_n367FuncaoAPFAtributos_AtrTabelaNom, H00A52_A384FuncoesAPFAtributos_Nome, H00A52_n384FuncoesAPFAtributos_Nome, H00A52_A383FuncoesAPFAtributos_Code,
               H00A52_n383FuncoesAPFAtributos_Code, H00A52_A389FuncoesAPFAtributos_Regra, H00A52_n389FuncoesAPFAtributos_Regra, H00A52_A178Atributos_TipoDados, H00A52_n178Atributos_TipoDados, H00A52_A365FuncaoAPFAtributos_AtributosNom, H00A52_n365FuncaoAPFAtributos_AtributosNom, H00A52_A364FuncaoAPFAtributos_AtributosCod
               }
               , new Object[] {
               H00A53_AGRIDATRIBUTOS_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_31 ;
      private short nGXsfl_31_idx=1 ;
      private short initialized ;
      private short nGXWrapped ;
      private short GRIDATRIBUTOS_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_31_Refreshing=0 ;
      private short subGridatributos_Backcolorstyle ;
      private short subGridatributos_Titlebackstyle ;
      private short subGridatributos_Allowselection ;
      private short subGridatributos_Allowhovering ;
      private short subGridatributos_Allowcollapsing ;
      private short subGridatributos_Collapsed ;
      private short subGridatributos_Backstyle ;
      private int A165FuncaoAPF_Codigo ;
      private int wcpOA165FuncaoAPF_Codigo ;
      private int subGridatributos_Rows ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private int subGridatributos_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A366FuncaoAPFAtributos_AtrTabelaCod ;
      private int subGridatributos_Titlebackcolor ;
      private int subGridatributos_Allbackcolor ;
      private int subGridatributos_Selectioncolor ;
      private int subGridatributos_Hoveringcolor ;
      private int idxLst ;
      private int subGridatributos_Backcolor ;
      private long GRIDATRIBUTOS_nFirstRecordOnPage ;
      private long GRIDATRIBUTOS_nCurrentRecord ;
      private long GRIDATRIBUTOS_nRecordCount ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_31_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel__Width ;
      private String Dvpanel__Cls ;
      private String Dvpanel__Title ;
      private String Dvpanel__Iconposition ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtFuncaoAPF_Codigo_Internalname ;
      private String edtFuncaoAPFAtributos_AtributosCod_Internalname ;
      private String A365FuncaoAPFAtributos_AtributosNom ;
      private String edtFuncaoAPFAtributos_AtributosNom_Internalname ;
      private String cmbAtributos_TipoDados_Internalname ;
      private String A178Atributos_TipoDados ;
      private String chkFuncoesAPFAtributos_Regra_Internalname ;
      private String edtFuncoesAPFAtributos_Code_Internalname ;
      private String A384FuncoesAPFAtributos_Nome ;
      private String edtFuncoesAPFAtributos_Nome_Internalname ;
      private String A367FuncaoAPFAtributos_AtrTabelaNom ;
      private String edtFuncaoAPFAtributos_AtrTabelaNom_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblTable2_Internalname ;
      private String tblTable4_Internalname ;
      private String subGridatributos_Internalname ;
      private String subGridatributos_Class ;
      private String subGridatributos_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String tblTableactions_Internalname ;
      private String sCtrlA165FuncaoAPF_Codigo ;
      private String sGXsfl_31_fel_idx="0001" ;
      private String ROClassString ;
      private String edtFuncaoAPF_Codigo_Jsonclick ;
      private String edtFuncaoAPFAtributos_AtributosCod_Jsonclick ;
      private String edtFuncaoAPFAtributos_AtributosNom_Jsonclick ;
      private String cmbAtributos_TipoDados_Jsonclick ;
      private String edtFuncoesAPFAtributos_Code_Jsonclick ;
      private String edtFuncoesAPFAtributos_Nome_Jsonclick ;
      private String edtFuncaoAPFAtributos_AtrTabelaNom_Jsonclick ;
      private String Dvpanel__Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Dvpanel__Collapsible ;
      private bool Dvpanel__Collapsed ;
      private bool Dvpanel__Autowidth ;
      private bool Dvpanel__Autoheight ;
      private bool Dvpanel__Showcollapseicon ;
      private bool Dvpanel__Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n365FuncaoAPFAtributos_AtributosNom ;
      private bool n178Atributos_TipoDados ;
      private bool A389FuncoesAPFAtributos_Regra ;
      private bool n389FuncoesAPFAtributos_Regra ;
      private bool n383FuncoesAPFAtributos_Code ;
      private bool n384FuncoesAPFAtributos_Nome ;
      private bool n367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool n366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool A180Atributos_Ativo ;
      private bool n180Atributos_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String A383FuncoesAPFAtributos_Code ;
      private GXWebGrid GridatributosContainer ;
      private GXWebRow GridatributosRow ;
      private GXWebColumn GridatributosColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbAtributos_TipoDados ;
      private GXCheckbox chkFuncoesAPFAtributos_Regra ;
      private IDataStoreProvider pr_default ;
      private int[] H00A52_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] H00A52_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private int[] H00A52_A165FuncaoAPF_Codigo ;
      private bool[] H00A52_A180Atributos_Ativo ;
      private bool[] H00A52_n180Atributos_Ativo ;
      private String[] H00A52_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] H00A52_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private String[] H00A52_A384FuncoesAPFAtributos_Nome ;
      private bool[] H00A52_n384FuncoesAPFAtributos_Nome ;
      private String[] H00A52_A383FuncoesAPFAtributos_Code ;
      private bool[] H00A52_n383FuncoesAPFAtributos_Code ;
      private bool[] H00A52_A389FuncoesAPFAtributos_Regra ;
      private bool[] H00A52_n389FuncoesAPFAtributos_Regra ;
      private String[] H00A52_A178Atributos_TipoDados ;
      private bool[] H00A52_n178Atributos_TipoDados ;
      private String[] H00A52_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] H00A52_n365FuncaoAPFAtributos_AtributosNom ;
      private int[] H00A52_A364FuncaoAPFAtributos_AtributosCod ;
      private long[] H00A53_AGRIDATRIBUTOS_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV5Context ;
   }

   public class funcaoapfatributosconsultawc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00A52 ;
          prmH00A52 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00A53 ;
          prmH00A53 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00A52", "SELECT * FROM (SELECT  T2.[Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, T1.[FuncaoAPF_Codigo], T2.[Atributos_Ativo], T3.[Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, T1.[FuncoesAPFAtributos_Nome], T1.[FuncoesAPFAtributos_Code], T1.[FuncoesAPFAtributos_Regra], T2.[Atributos_TipoDados], T2.[Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom, T1.[FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, ROW_NUMBER() OVER ( ORDER BY T2.[Atributos_Nome] ) AS GX_ROW_NUMBER FROM (([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod]) WHERE (T1.[FuncaoAPF_Codigo] = @FuncaoAPF_Codigo) AND (T2.[Atributos_Ativo] = 1)) AS GX_CTE WHERE GX_ROW_NUMBER BETWEEN @GXPagingFrom2 AND @GXPagingTo2 OR @GXPagingTo2 < @GXPagingFrom2 AND GX_ROW_NUMBER >= @GXPagingFrom2",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00A52,11,0,true,false )
             ,new CursorDef("H00A53", "SELECT COUNT(*) FROM (([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod]) WHERE (T1.[FuncaoAPF_Codigo] = @FuncaoAPF_Codigo) AND (T2.[Atributos_Ativo] = 1) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00A53,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((bool[]) buf[11])[0] = rslt.getBool(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 4) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
