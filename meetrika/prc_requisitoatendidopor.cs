/*
               File: PRC_RequisitoAtendidoPor
        Description: Requisito Atendido Por
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:8:44.1
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_requisitoatendidopor : GXProcedure
   {
      public prc_requisitoatendidopor( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_requisitoatendidopor( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref long aP0_SolicServicoReqNeg_ReqNeqCod ,
                           out String aP1_OS ,
                           out String aP2_Tooltiptext ,
                           ref bool aP3_OSVisible )
      {
         this.A1947SolicServicoReqNeg_ReqNeqCod = aP0_SolicServicoReqNeg_ReqNeqCod;
         this.AV9OS = "" ;
         this.AV10Tooltiptext = "" ;
         this.AV11OSVisible = aP3_OSVisible;
         initialize();
         executePrivate();
         aP0_SolicServicoReqNeg_ReqNeqCod=this.A1947SolicServicoReqNeg_ReqNeqCod;
         aP1_OS=this.AV9OS;
         aP2_Tooltiptext=this.AV10Tooltiptext;
         aP3_OSVisible=this.AV11OSVisible;
      }

      public bool executeUdp( ref long aP0_SolicServicoReqNeg_ReqNeqCod ,
                              out String aP1_OS ,
                              out String aP2_Tooltiptext )
      {
         this.A1947SolicServicoReqNeg_ReqNeqCod = aP0_SolicServicoReqNeg_ReqNeqCod;
         this.AV9OS = "" ;
         this.AV10Tooltiptext = "" ;
         this.AV11OSVisible = aP3_OSVisible;
         initialize();
         executePrivate();
         aP0_SolicServicoReqNeg_ReqNeqCod=this.A1947SolicServicoReqNeg_ReqNeqCod;
         aP1_OS=this.AV9OS;
         aP2_Tooltiptext=this.AV10Tooltiptext;
         aP3_OSVisible=this.AV11OSVisible;
         return AV11OSVisible ;
      }

      public void executeSubmit( ref long aP0_SolicServicoReqNeg_ReqNeqCod ,
                                 out String aP1_OS ,
                                 out String aP2_Tooltiptext ,
                                 ref bool aP3_OSVisible )
      {
         prc_requisitoatendidopor objprc_requisitoatendidopor;
         objprc_requisitoatendidopor = new prc_requisitoatendidopor();
         objprc_requisitoatendidopor.A1947SolicServicoReqNeg_ReqNeqCod = aP0_SolicServicoReqNeg_ReqNeqCod;
         objprc_requisitoatendidopor.AV9OS = "" ;
         objprc_requisitoatendidopor.AV10Tooltiptext = "" ;
         objprc_requisitoatendidopor.AV11OSVisible = aP3_OSVisible;
         objprc_requisitoatendidopor.context.SetSubmitInitialConfig(context);
         objprc_requisitoatendidopor.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_requisitoatendidopor);
         aP0_SolicServicoReqNeg_ReqNeqCod=this.A1947SolicServicoReqNeg_ReqNeqCod;
         aP1_OS=this.AV9OS;
         aP2_Tooltiptext=this.AV10Tooltiptext;
         aP3_OSVisible=this.AV11OSVisible;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_requisitoatendidopor)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Count = 0;
         AV9OS = "";
         AV10Tooltiptext = "";
         /* Using cursor P00V72 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2003ContagemResultadoRequisito_OSCod = P00V72_A2003ContagemResultadoRequisito_OSCod[0];
            A484ContagemResultado_StatusDmn = P00V72_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00V72_n484ContagemResultado_StatusDmn[0];
            A493ContagemResultado_DemandaFM = P00V72_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00V72_n493ContagemResultado_DemandaFM[0];
            A2005ContagemResultadoRequisito_Codigo = P00V72_A2005ContagemResultadoRequisito_Codigo[0];
            A484ContagemResultado_StatusDmn = P00V72_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00V72_n484ContagemResultado_StatusDmn[0];
            A493ContagemResultado_DemandaFM = P00V72_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00V72_n493ContagemResultado_DemandaFM[0];
            AV9OS = AV9OS + StringUtil.Trim( A493ContagemResultado_DemandaFM) + " (" + gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn) + "), ";
            AV8Count = (short)(AV8Count+1);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         AV9OS = AV9OS + ".";
         AV9OS = StringUtil.StringReplace( AV9OS, ", .", "");
         if ( AV8Count > 0 )
         {
            AV11OSVisible = true;
            AV10Tooltiptext = AV9OS;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00V72_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         P00V72_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00V72_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00V72_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00V72_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00V72_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         A484ContagemResultado_StatusDmn = "";
         A493ContagemResultado_DemandaFM = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_requisitoatendidopor__default(),
            new Object[][] {
                new Object[] {
               P00V72_A2003ContagemResultadoRequisito_OSCod, P00V72_A484ContagemResultado_StatusDmn, P00V72_n484ContagemResultado_StatusDmn, P00V72_A493ContagemResultado_DemandaFM, P00V72_n493ContagemResultado_DemandaFM, P00V72_A2005ContagemResultadoRequisito_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8Count ;
      private int A2003ContagemResultadoRequisito_OSCod ;
      private int A2005ContagemResultadoRequisito_Codigo ;
      private long A1947SolicServicoReqNeg_ReqNeqCod ;
      private String AV10Tooltiptext ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private bool AV11OSVisible ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n493ContagemResultado_DemandaFM ;
      private String AV9OS ;
      private String A493ContagemResultado_DemandaFM ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private long aP0_SolicServicoReqNeg_ReqNeqCod ;
      private bool aP3_OSVisible ;
      private IDataStoreProvider pr_default ;
      private int[] P00V72_A2003ContagemResultadoRequisito_OSCod ;
      private String[] P00V72_A484ContagemResultado_StatusDmn ;
      private bool[] P00V72_n484ContagemResultado_StatusDmn ;
      private String[] P00V72_A493ContagemResultado_DemandaFM ;
      private bool[] P00V72_n493ContagemResultado_DemandaFM ;
      private int[] P00V72_A2005ContagemResultadoRequisito_Codigo ;
      private String aP1_OS ;
      private String aP2_Tooltiptext ;
   }

   public class prc_requisitoatendidopor__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00V72 ;
          prmP00V72 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00V72", "SELECT T1.[ContagemResultadoRequisito_OSCod] AS ContagemResultadoRequisito_OSCod, T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_DemandaFM], T1.[ContagemResultadoRequisito_Codigo] FROM ([ContagemResultadoRequisito] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultadoRequisito_OSCod]) WHERE T2.[ContagemResultado_StatusDmn] <> 'X' ORDER BY T1.[ContagemResultadoRequisito_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00V72,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
