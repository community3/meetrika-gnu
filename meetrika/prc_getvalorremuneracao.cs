/*
               File: PRC_GetValorRemuneracao
        Description: Get Valor Remuneracao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:42.94
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_getvalorremuneracao : GXProcedure
   {
      public prc_getvalorremuneracao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_getvalorremuneracao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicos_Codigo ,
                           decimal aP1_Valor ,
                           out decimal aP2_ValorFinal )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV8Valor = aP1_Valor;
         this.AV9ValorFinal = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP2_ValorFinal=this.AV9ValorFinal;
      }

      public decimal executeUdp( ref int aP0_ContratoServicos_Codigo ,
                                 decimal aP1_Valor )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV8Valor = aP1_Valor;
         this.AV9ValorFinal = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP2_ValorFinal=this.AV9ValorFinal;
         return AV9ValorFinal ;
      }

      public void executeSubmit( ref int aP0_ContratoServicos_Codigo ,
                                 decimal aP1_Valor ,
                                 out decimal aP2_ValorFinal )
      {
         prc_getvalorremuneracao objprc_getvalorremuneracao;
         objprc_getvalorremuneracao = new prc_getvalorremuneracao();
         objprc_getvalorremuneracao.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_getvalorremuneracao.AV8Valor = aP1_Valor;
         objprc_getvalorremuneracao.AV9ValorFinal = 0 ;
         objprc_getvalorremuneracao.context.SetSubmitInitialConfig(context);
         objprc_getvalorremuneracao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_getvalorremuneracao);
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP2_ValorFinal=this.AV9ValorFinal;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_getvalorremuneracao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00X62 */
         pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo, AV8Valor});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2071ContratoServicosRmn_Fim = P00X62_A2071ContratoServicosRmn_Fim[0];
            A2070ContratoServicosRmn_Inicio = P00X62_A2070ContratoServicosRmn_Inicio[0];
            A2072ContratoServicosRmn_Valor = P00X62_A2072ContratoServicosRmn_Valor[0];
            A2069ContratoServicosRmn_Sequencial = P00X62_A2069ContratoServicosRmn_Sequencial[0];
            if ( (Convert.ToDecimal(0)==A2072ContratoServicosRmn_Valor) )
            {
               AV9ValorFinal = AV8Valor;
            }
            else
            {
               AV9ValorFinal = A2072ContratoServicosRmn_Valor;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00X62_A160ContratoServicos_Codigo = new int[1] ;
         P00X62_A2071ContratoServicosRmn_Fim = new decimal[1] ;
         P00X62_A2070ContratoServicosRmn_Inicio = new decimal[1] ;
         P00X62_A2072ContratoServicosRmn_Valor = new decimal[1] ;
         P00X62_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_getvalorremuneracao__default(),
            new Object[][] {
                new Object[] {
               P00X62_A160ContratoServicos_Codigo, P00X62_A2071ContratoServicosRmn_Fim, P00X62_A2070ContratoServicosRmn_Inicio, P00X62_A2072ContratoServicosRmn_Valor, P00X62_A2069ContratoServicosRmn_Sequencial
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A2069ContratoServicosRmn_Sequencial ;
      private int A160ContratoServicos_Codigo ;
      private decimal AV8Valor ;
      private decimal AV9ValorFinal ;
      private decimal A2071ContratoServicosRmn_Fim ;
      private decimal A2070ContratoServicosRmn_Inicio ;
      private decimal A2072ContratoServicosRmn_Valor ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicos_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00X62_A160ContratoServicos_Codigo ;
      private decimal[] P00X62_A2071ContratoServicosRmn_Fim ;
      private decimal[] P00X62_A2070ContratoServicosRmn_Inicio ;
      private decimal[] P00X62_A2072ContratoServicosRmn_Valor ;
      private short[] P00X62_A2069ContratoServicosRmn_Sequencial ;
      private decimal aP2_ValorFinal ;
   }

   public class prc_getvalorremuneracao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00X62 ;
          prmP00X62 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8Valor",SqlDbType.Decimal,14,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00X62", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosRmn_Fim], [ContratoServicosRmn_Inicio], [ContratoServicosRmn_Valor], [ContratoServicosRmn_Sequencial] FROM [ContratoServicosRmn] WITH (NOLOCK) WHERE ([ContratoServicos_Codigo] = @ContratoServicos_Codigo) AND (( [ContratoServicosRmn_Inicio] <= @AV8Valor and [ContratoServicosRmn_Fim] >= @AV8Valor) or [ContratoServicosRmn_Inicio] <= @AV8Valor and ([ContratoServicosRmn_Fim] = convert(int, 0))) ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00X62,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                return;
       }
    }

 }

}
