/*
               File: type_SdtSolicitacaoServicoReqNegocio
        Description: Requisitos de Neg�cio da Solicita��o de Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:45:31.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SolicitacaoServicoReqNegocio" )]
   [XmlType(TypeName =  "SolicitacaoServicoReqNegocio" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSolicitacaoServicoReqNegocio : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSolicitacaoServicoReqNegocio( )
      {
         /* Constructor for serialization */
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador = "";
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome = "";
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_descricao = "";
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador = "";
         gxTv_SdtSolicitacaoServicoReqNegocio_Mode = "";
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador_Z = "";
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome_Z = "";
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador_Z = "";
      }

      public SdtSolicitacaoServicoReqNegocio( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( long AV1895SolicServicoReqNeg_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(long)AV1895SolicServicoReqNeg_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"SolicServicoReqNeg_Codigo", typeof(long)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "SolicitacaoServicoReqNegocio");
         metadata.Set("BT", "SolicitacaoServicoReqNegocio");
         metadata.Set("PK", "[ \"SolicServicoReqNeg_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"SolicServicoReqNeg_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"ContagemResultado_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"SolicServicoReqNeg_Codigo\" ],\"FKMap\":[ \"SolicServicoReqNeg_ReqNeqCod-SolicServicoReqNeg_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicservicoreqneg_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicservicoreqneg_identificador_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicservicoreqneg_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicservicoreqneg_prioridade_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicservicoreqneg_agrupador_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicservicoreqneg_tamanho_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicservicoreqneg_situacao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicservicoreqneg_reqneqcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicservicoreqneg_reqneqcod_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSolicitacaoServicoReqNegocio deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSolicitacaoServicoReqNegocio)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSolicitacaoServicoReqNegocio obj ;
         obj = this;
         obj.gxTpr_Solicservicoreqneg_codigo = deserialized.gxTpr_Solicservicoreqneg_codigo;
         obj.gxTpr_Contagemresultado_codigo = deserialized.gxTpr_Contagemresultado_codigo;
         obj.gxTpr_Solicservicoreqneg_identificador = deserialized.gxTpr_Solicservicoreqneg_identificador;
         obj.gxTpr_Solicservicoreqneg_nome = deserialized.gxTpr_Solicservicoreqneg_nome;
         obj.gxTpr_Solicservicoreqneg_descricao = deserialized.gxTpr_Solicservicoreqneg_descricao;
         obj.gxTpr_Solicservicoreqneg_prioridade = deserialized.gxTpr_Solicservicoreqneg_prioridade;
         obj.gxTpr_Solicservicoreqneg_agrupador = deserialized.gxTpr_Solicservicoreqneg_agrupador;
         obj.gxTpr_Solicservicoreqneg_tamanho = deserialized.gxTpr_Solicservicoreqneg_tamanho;
         obj.gxTpr_Solicservicoreqneg_situacao = deserialized.gxTpr_Solicservicoreqneg_situacao;
         obj.gxTpr_Solicservicoreqneg_reqneqcod = deserialized.gxTpr_Solicservicoreqneg_reqneqcod;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Solicservicoreqneg_codigo_Z = deserialized.gxTpr_Solicservicoreqneg_codigo_Z;
         obj.gxTpr_Contagemresultado_codigo_Z = deserialized.gxTpr_Contagemresultado_codigo_Z;
         obj.gxTpr_Solicservicoreqneg_identificador_Z = deserialized.gxTpr_Solicservicoreqneg_identificador_Z;
         obj.gxTpr_Solicservicoreqneg_nome_Z = deserialized.gxTpr_Solicservicoreqneg_nome_Z;
         obj.gxTpr_Solicservicoreqneg_prioridade_Z = deserialized.gxTpr_Solicservicoreqneg_prioridade_Z;
         obj.gxTpr_Solicservicoreqneg_agrupador_Z = deserialized.gxTpr_Solicservicoreqneg_agrupador_Z;
         obj.gxTpr_Solicservicoreqneg_tamanho_Z = deserialized.gxTpr_Solicservicoreqneg_tamanho_Z;
         obj.gxTpr_Solicservicoreqneg_situacao_Z = deserialized.gxTpr_Solicservicoreqneg_situacao_Z;
         obj.gxTpr_Solicservicoreqneg_reqneqcod_Z = deserialized.gxTpr_Solicservicoreqneg_reqneqcod_Z;
         obj.gxTpr_Solicservicoreqneg_reqneqcod_N = deserialized.gxTpr_Solicservicoreqneg_reqneqcod_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_Codigo") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_codigo = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Codigo") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Contagemresultado_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_Identificador") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_Nome") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_Descricao") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_Prioridade") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_prioridade = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_Agrupador") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_Tamanho") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_Situacao") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_situacao = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_ReqNeqCod") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_Codigo_Z") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_codigo_Z = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Codigo_Z") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Contagemresultado_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_Identificador_Z") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_Nome_Z") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_Prioridade_Z") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_prioridade_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_Agrupador_Z") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_Tamanho_Z") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_Situacao_Z") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_situacao_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_ReqNeqCod_Z") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_Z = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicServicoReqNeg_ReqNeqCod_N") )
               {
                  gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SolicitacaoServicoReqNegocio";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("SolicServicoReqNeg_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_codigo), 10, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServicoReqNegocio_Contagemresultado_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("SolicServicoReqNeg_Identificador", StringUtil.RTrim( gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("SolicServicoReqNeg_Nome", StringUtil.RTrim( gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("SolicServicoReqNeg_Descricao", StringUtil.RTrim( gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("SolicServicoReqNeg_Prioridade", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_prioridade), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("SolicServicoReqNeg_Agrupador", StringUtil.RTrim( gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("SolicServicoReqNeg_Tamanho", StringUtil.Trim( StringUtil.Str( gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho, 7, 3)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("SolicServicoReqNeg_Situacao", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_situacao), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("SolicServicoReqNeg_ReqNeqCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod), 10, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtSolicitacaoServicoReqNegocio_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServicoReqNegocio_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("SolicServicoReqNeg_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_codigo_Z), 10, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultado_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServicoReqNegocio_Contagemresultado_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("SolicServicoReqNeg_Identificador_Z", StringUtil.RTrim( gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("SolicServicoReqNeg_Nome_Z", StringUtil.RTrim( gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("SolicServicoReqNeg_Prioridade_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_prioridade_Z), 2, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("SolicServicoReqNeg_Agrupador_Z", StringUtil.RTrim( gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("SolicServicoReqNeg_Tamanho_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho_Z, 7, 3)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("SolicServicoReqNeg_Situacao_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_situacao_Z), 2, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("SolicServicoReqNeg_ReqNeqCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_Z), 10, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("SolicServicoReqNeg_ReqNeqCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("SolicServicoReqNeg_Codigo", gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_codigo, false);
         AddObjectProperty("ContagemResultado_Codigo", gxTv_SdtSolicitacaoServicoReqNegocio_Contagemresultado_codigo, false);
         AddObjectProperty("SolicServicoReqNeg_Identificador", gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador, false);
         AddObjectProperty("SolicServicoReqNeg_Nome", gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome, false);
         AddObjectProperty("SolicServicoReqNeg_Descricao", gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_descricao, false);
         AddObjectProperty("SolicServicoReqNeg_Prioridade", gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_prioridade, false);
         AddObjectProperty("SolicServicoReqNeg_Agrupador", gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador, false);
         AddObjectProperty("SolicServicoReqNeg_Tamanho", gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho, false);
         AddObjectProperty("SolicServicoReqNeg_Situacao", gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_situacao, false);
         AddObjectProperty("SolicServicoReqNeg_ReqNeqCod", gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtSolicitacaoServicoReqNegocio_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtSolicitacaoServicoReqNegocio_Initialized, false);
            AddObjectProperty("SolicServicoReqNeg_Codigo_Z", gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_codigo_Z, false);
            AddObjectProperty("ContagemResultado_Codigo_Z", gxTv_SdtSolicitacaoServicoReqNegocio_Contagemresultado_codigo_Z, false);
            AddObjectProperty("SolicServicoReqNeg_Identificador_Z", gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador_Z, false);
            AddObjectProperty("SolicServicoReqNeg_Nome_Z", gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome_Z, false);
            AddObjectProperty("SolicServicoReqNeg_Prioridade_Z", gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_prioridade_Z, false);
            AddObjectProperty("SolicServicoReqNeg_Agrupador_Z", gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador_Z, false);
            AddObjectProperty("SolicServicoReqNeg_Tamanho_Z", gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho_Z, false);
            AddObjectProperty("SolicServicoReqNeg_Situacao_Z", gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_situacao_Z, false);
            AddObjectProperty("SolicServicoReqNeg_ReqNeqCod_Z", gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_Z, false);
            AddObjectProperty("SolicServicoReqNeg_ReqNeqCod_N", gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_Codigo" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_Codigo"   )]
      public long gxTpr_Solicservicoreqneg_codigo
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_codigo ;
         }

         set {
            if ( gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_codigo != value )
            {
               gxTv_SdtSolicitacaoServicoReqNegocio_Mode = "INS";
               this.gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_codigo_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServicoReqNegocio_Contagemresultado_codigo_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_prioridade_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_situacao_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_Z_SetNull( );
            }
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_codigo = (long)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Codigo" )]
      [  XmlElement( ElementName = "ContagemResultado_Codigo"   )]
      public int gxTpr_Contagemresultado_codigo
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Contagemresultado_codigo ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Contagemresultado_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_Identificador" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_Identificador"   )]
      public String gxTpr_Solicservicoreqneg_identificador
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador = (String)(value);
         }

      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_Nome" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_Nome"   )]
      public String gxTpr_Solicservicoreqneg_nome
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_Descricao" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_Descricao"   )]
      public String gxTpr_Solicservicoreqneg_descricao
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_descricao ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_Prioridade" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_Prioridade"   )]
      public short gxTpr_Solicservicoreqneg_prioridade
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_prioridade ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_prioridade = (short)(value);
         }

      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_Agrupador" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_Agrupador"   )]
      public String gxTpr_Solicservicoreqneg_agrupador
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador = (String)(value);
         }

      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_Tamanho" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_Tamanho"   )]
      public double gxTpr_Solicservicoreqneg_tamanho_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho) ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Solicservicoreqneg_tamanho
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_Situacao" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_Situacao"   )]
      public short gxTpr_Solicservicoreqneg_situacao
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_situacao ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_situacao = (short)(value);
         }

      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_ReqNeqCod" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_ReqNeqCod"   )]
      public long gxTpr_Solicservicoreqneg_reqneqcod
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_N = 0;
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod = (long)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_SetNull( )
      {
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_N = 1;
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Mode ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Mode = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServicoReqNegocio_Mode_SetNull( )
      {
         gxTv_SdtSolicitacaoServicoReqNegocio_Mode = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServicoReqNegocio_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Initialized ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServicoReqNegocio_Initialized_SetNull( )
      {
         gxTv_SdtSolicitacaoServicoReqNegocio_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServicoReqNegocio_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_Codigo_Z" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_Codigo_Z"   )]
      public long gxTpr_Solicservicoreqneg_codigo_Z
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_codigo_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_codigo_Z = (long)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_codigo_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Codigo_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Codigo_Z"   )]
      public int gxTpr_Contagemresultado_codigo_Z
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Contagemresultado_codigo_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Contagemresultado_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServicoReqNegocio_Contagemresultado_codigo_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServicoReqNegocio_Contagemresultado_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServicoReqNegocio_Contagemresultado_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_Identificador_Z" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_Identificador_Z"   )]
      public String gxTpr_Solicservicoreqneg_identificador_Z
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador_Z = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador_Z = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_Nome_Z" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_Nome_Z"   )]
      public String gxTpr_Solicservicoreqneg_nome_Z
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_Prioridade_Z" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_Prioridade_Z"   )]
      public short gxTpr_Solicservicoreqneg_prioridade_Z
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_prioridade_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_prioridade_Z = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_prioridade_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_prioridade_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_prioridade_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_Agrupador_Z" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_Agrupador_Z"   )]
      public String gxTpr_Solicservicoreqneg_agrupador_Z
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador_Z = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador_Z = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_Tamanho_Z" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_Tamanho_Z"   )]
      public double gxTpr_Solicservicoreqneg_tamanho_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho_Z) ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Solicservicoreqneg_tamanho_Z
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_Situacao_Z" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_Situacao_Z"   )]
      public short gxTpr_Solicservicoreqneg_situacao_Z
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_situacao_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_situacao_Z = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_situacao_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_situacao_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_situacao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_ReqNeqCod_Z" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_ReqNeqCod_Z"   )]
      public long gxTpr_Solicservicoreqneg_reqneqcod_Z
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_Z = (long)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SolicServicoReqNeg_ReqNeqCod_N" )]
      [  XmlElement( ElementName = "SolicServicoReqNeg_ReqNeqCod_N"   )]
      public short gxTpr_Solicservicoreqneg_reqneqcod_N
      {
         get {
            return gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_N ;
         }

         set {
            gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador = "";
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome = "";
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_descricao = "";
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador = "";
         gxTv_SdtSolicitacaoServicoReqNegocio_Mode = "";
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador_Z = "";
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome_Z = "";
         gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador_Z = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "solicitacaoservicoreqnegocio", "GeneXus.Programs.solicitacaoservicoreqnegocio_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_prioridade ;
      private short gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_situacao ;
      private short gxTv_SdtSolicitacaoServicoReqNegocio_Initialized ;
      private short gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_prioridade_Z ;
      private short gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_situacao_Z ;
      private short gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtSolicitacaoServicoReqNegocio_Contagemresultado_codigo ;
      private int gxTv_SdtSolicitacaoServicoReqNegocio_Contagemresultado_codigo_Z ;
      private long gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_codigo ;
      private long gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod ;
      private long gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_codigo_Z ;
      private long gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_reqneqcod_Z ;
      private decimal gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho ;
      private decimal gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_tamanho_Z ;
      private String gxTv_SdtSolicitacaoServicoReqNegocio_Mode ;
      private String sTagName ;
      private String gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_descricao ;
      private String gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador ;
      private String gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome ;
      private String gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador ;
      private String gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_identificador_Z ;
      private String gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_nome_Z ;
      private String gxTv_SdtSolicitacaoServicoReqNegocio_Solicservicoreqneg_agrupador_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"SolicitacaoServicoReqNegocio", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSolicitacaoServicoReqNegocio_RESTInterface : GxGenericCollectionItem<SdtSolicitacaoServicoReqNegocio>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSolicitacaoServicoReqNegocio_RESTInterface( ) : base()
      {
      }

      public SdtSolicitacaoServicoReqNegocio_RESTInterface( SdtSolicitacaoServicoReqNegocio psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "SolicServicoReqNeg_Codigo" , Order = 0 )]
      [GxSeudo()]
      public String gxTpr_Solicservicoreqneg_codigo
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Solicservicoreqneg_codigo), 10, 0)) ;
         }

         set {
            sdt.gxTpr_Solicservicoreqneg_codigo = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ContagemResultado_Codigo" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_codigo
      {
         get {
            return sdt.gxTpr_Contagemresultado_codigo ;
         }

         set {
            sdt.gxTpr_Contagemresultado_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "SolicServicoReqNeg_Identificador" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Solicservicoreqneg_identificador
      {
         get {
            return sdt.gxTpr_Solicservicoreqneg_identificador ;
         }

         set {
            sdt.gxTpr_Solicservicoreqneg_identificador = (String)(value);
         }

      }

      [DataMember( Name = "SolicServicoReqNeg_Nome" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Solicservicoreqneg_nome
      {
         get {
            return sdt.gxTpr_Solicservicoreqneg_nome ;
         }

         set {
            sdt.gxTpr_Solicservicoreqneg_nome = (String)(value);
         }

      }

      [DataMember( Name = "SolicServicoReqNeg_Descricao" , Order = 4 )]
      public String gxTpr_Solicservicoreqneg_descricao
      {
         get {
            return sdt.gxTpr_Solicservicoreqneg_descricao ;
         }

         set {
            sdt.gxTpr_Solicservicoreqneg_descricao = (String)(value);
         }

      }

      [DataMember( Name = "SolicServicoReqNeg_Prioridade" , Order = 5 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Solicservicoreqneg_prioridade
      {
         get {
            return sdt.gxTpr_Solicservicoreqneg_prioridade ;
         }

         set {
            sdt.gxTpr_Solicservicoreqneg_prioridade = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "SolicServicoReqNeg_Agrupador" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Solicservicoreqneg_agrupador
      {
         get {
            return sdt.gxTpr_Solicservicoreqneg_agrupador ;
         }

         set {
            sdt.gxTpr_Solicservicoreqneg_agrupador = (String)(value);
         }

      }

      [DataMember( Name = "SolicServicoReqNeg_Tamanho" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Solicservicoreqneg_tamanho
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Solicservicoreqneg_tamanho, 7, 3)) ;
         }

         set {
            sdt.gxTpr_Solicservicoreqneg_tamanho = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "SolicServicoReqNeg_Situacao" , Order = 8 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Solicservicoreqneg_situacao
      {
         get {
            return sdt.gxTpr_Solicservicoreqneg_situacao ;
         }

         set {
            sdt.gxTpr_Solicservicoreqneg_situacao = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "SolicServicoReqNeg_ReqNeqCod" , Order = 9 )]
      [GxSeudo()]
      public String gxTpr_Solicservicoreqneg_reqneqcod
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Solicservicoreqneg_reqneqcod), 10, 0)) ;
         }

         set {
            sdt.gxTpr_Solicservicoreqneg_reqneqcod = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      public SdtSolicitacaoServicoReqNegocio sdt
      {
         get {
            return (SdtSolicitacaoServicoReqNegocio)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSolicitacaoServicoReqNegocio() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 22 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
