/*
               File: GetControServicoIndicadoresWCFilterData
        Description: Get Contro Servico Indicadores WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/15/2020 23:16:56.13
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontroservicoindicadoreswcfilterdata : GXProcedure
   {
      public getcontroservicoindicadoreswcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontroservicoindicadoreswcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontroservicoindicadoreswcfilterdata objgetcontroservicoindicadoreswcfilterdata;
         objgetcontroservicoindicadoreswcfilterdata = new getcontroservicoindicadoreswcfilterdata();
         objgetcontroservicoindicadoreswcfilterdata.AV18DDOName = aP0_DDOName;
         objgetcontroservicoindicadoreswcfilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetcontroservicoindicadoreswcfilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetcontroservicoindicadoreswcfilterdata.AV22OptionsJson = "" ;
         objgetcontroservicoindicadoreswcfilterdata.AV25OptionsDescJson = "" ;
         objgetcontroservicoindicadoreswcfilterdata.AV27OptionIndexesJson = "" ;
         objgetcontroservicoindicadoreswcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontroservicoindicadoreswcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontroservicoindicadoreswcfilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontroservicoindicadoreswcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATOSERVICOSINDICADOR_INDICADOR") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSINDICADOR_INDICADOROPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("ControServicoIndicadoresWCGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ControServicoIndicadoresWCGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("ControServicoIndicadoresWCGridState"), "");
         }
         AV37GXV1 = 1;
         while ( AV37GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV37GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_NUMERO") == 0 )
            {
               AV10TFContratoServicosIndicador_Numero = (short)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
               AV11TFContratoServicosIndicador_Numero_To = (short)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_INDICADOR") == 0 )
            {
               AV12TFContratoServicosIndicador_Indicador = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_INDICADOR_SEL") == 0 )
            {
               AV13TFContratoServicosIndicador_Indicador_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADOR_QTDEFAIXAS") == 0 )
            {
               AV14TFContratoServicosIndicador_QtdeFaixas = (short)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
               AV15TFContratoServicosIndicador_QtdeFaixas_To = (short)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "PARM_&CONTRATOSERVICOSINDICADOR_CNTSRVCOD") == 0 )
            {
               AV34ContratoServicosIndicador_CntSrvCod = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
            }
            AV37GXV1 = (int)(AV37GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATOSERVICOSINDICADOR_INDICADOROPTIONS' Routine */
         AV12TFContratoServicosIndicador_Indicador = AV16SearchTxt;
         AV13TFContratoServicosIndicador_Indicador_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV10TFContratoServicosIndicador_Numero ,
                                              AV11TFContratoServicosIndicador_Numero_To ,
                                              AV13TFContratoServicosIndicador_Indicador_Sel ,
                                              AV12TFContratoServicosIndicador_Indicador ,
                                              A1271ContratoServicosIndicador_Numero ,
                                              A1274ContratoServicosIndicador_Indicador ,
                                              AV14TFContratoServicosIndicador_QtdeFaixas ,
                                              A1298ContratoServicosIndicador_QtdeFaixas ,
                                              AV15TFContratoServicosIndicador_QtdeFaixas_To ,
                                              AV34ContratoServicosIndicador_CntSrvCod ,
                                              A1270ContratoServicosIndicador_CntSrvCod },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV12TFContratoServicosIndicador_Indicador = StringUtil.Concat( StringUtil.RTrim( AV12TFContratoServicosIndicador_Indicador), "%", "");
         /* Using cursor P00JZ3 */
         pr_default.execute(0, new Object[] {AV34ContratoServicosIndicador_CntSrvCod, AV14TFContratoServicosIndicador_QtdeFaixas, AV14TFContratoServicosIndicador_QtdeFaixas, AV15TFContratoServicosIndicador_QtdeFaixas_To, AV15TFContratoServicosIndicador_QtdeFaixas_To, AV10TFContratoServicosIndicador_Numero, AV11TFContratoServicosIndicador_Numero_To, lV12TFContratoServicosIndicador_Indicador, AV13TFContratoServicosIndicador_Indicador_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKJZ2 = false;
            A1269ContratoServicosIndicador_Codigo = P00JZ3_A1269ContratoServicosIndicador_Codigo[0];
            A1270ContratoServicosIndicador_CntSrvCod = P00JZ3_A1270ContratoServicosIndicador_CntSrvCod[0];
            A1274ContratoServicosIndicador_Indicador = P00JZ3_A1274ContratoServicosIndicador_Indicador[0];
            A1271ContratoServicosIndicador_Numero = P00JZ3_A1271ContratoServicosIndicador_Numero[0];
            A1298ContratoServicosIndicador_QtdeFaixas = P00JZ3_A1298ContratoServicosIndicador_QtdeFaixas[0];
            n1298ContratoServicosIndicador_QtdeFaixas = P00JZ3_n1298ContratoServicosIndicador_QtdeFaixas[0];
            A1298ContratoServicosIndicador_QtdeFaixas = P00JZ3_A1298ContratoServicosIndicador_QtdeFaixas[0];
            n1298ContratoServicosIndicador_QtdeFaixas = P00JZ3_n1298ContratoServicosIndicador_QtdeFaixas[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00JZ3_A1270ContratoServicosIndicador_CntSrvCod[0] == A1270ContratoServicosIndicador_CntSrvCod ) && ( StringUtil.StrCmp(P00JZ3_A1274ContratoServicosIndicador_Indicador[0], A1274ContratoServicosIndicador_Indicador) == 0 ) )
            {
               BRKJZ2 = false;
               A1269ContratoServicosIndicador_Codigo = P00JZ3_A1269ContratoServicosIndicador_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKJZ2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1274ContratoServicosIndicador_Indicador)) )
            {
               AV20Option = A1274ContratoServicosIndicador_Indicador;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJZ2 )
            {
               BRKJZ2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFContratoServicosIndicador_Indicador = "";
         AV13TFContratoServicosIndicador_Indicador_Sel = "";
         scmdbuf = "";
         lV12TFContratoServicosIndicador_Indicador = "";
         A1274ContratoServicosIndicador_Indicador = "";
         P00JZ3_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P00JZ3_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         P00JZ3_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         P00JZ3_A1271ContratoServicosIndicador_Numero = new short[1] ;
         P00JZ3_A1298ContratoServicosIndicador_QtdeFaixas = new short[1] ;
         P00JZ3_n1298ContratoServicosIndicador_QtdeFaixas = new bool[] {false} ;
         AV20Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontroservicoindicadoreswcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00JZ3_A1269ContratoServicosIndicador_Codigo, P00JZ3_A1270ContratoServicosIndicador_CntSrvCod, P00JZ3_A1274ContratoServicosIndicador_Indicador, P00JZ3_A1271ContratoServicosIndicador_Numero, P00JZ3_A1298ContratoServicosIndicador_QtdeFaixas, P00JZ3_n1298ContratoServicosIndicador_QtdeFaixas
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV10TFContratoServicosIndicador_Numero ;
      private short AV11TFContratoServicosIndicador_Numero_To ;
      private short AV14TFContratoServicosIndicador_QtdeFaixas ;
      private short AV15TFContratoServicosIndicador_QtdeFaixas_To ;
      private short A1271ContratoServicosIndicador_Numero ;
      private short A1298ContratoServicosIndicador_QtdeFaixas ;
      private int AV37GXV1 ;
      private int AV34ContratoServicosIndicador_CntSrvCod ;
      private int A1270ContratoServicosIndicador_CntSrvCod ;
      private int A1269ContratoServicosIndicador_Codigo ;
      private long AV28count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool BRKJZ2 ;
      private bool n1298ContratoServicosIndicador_QtdeFaixas ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String A1274ContratoServicosIndicador_Indicador ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV12TFContratoServicosIndicador_Indicador ;
      private String AV13TFContratoServicosIndicador_Indicador_Sel ;
      private String lV12TFContratoServicosIndicador_Indicador ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00JZ3_A1269ContratoServicosIndicador_Codigo ;
      private int[] P00JZ3_A1270ContratoServicosIndicador_CntSrvCod ;
      private String[] P00JZ3_A1274ContratoServicosIndicador_Indicador ;
      private short[] P00JZ3_A1271ContratoServicosIndicador_Numero ;
      private short[] P00JZ3_A1298ContratoServicosIndicador_QtdeFaixas ;
      private bool[] P00JZ3_n1298ContratoServicosIndicador_QtdeFaixas ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
   }

   public class getcontroservicoindicadoreswcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00JZ3( IGxContext context ,
                                             short AV10TFContratoServicosIndicador_Numero ,
                                             short AV11TFContratoServicosIndicador_Numero_To ,
                                             String AV13TFContratoServicosIndicador_Indicador_Sel ,
                                             String AV12TFContratoServicosIndicador_Indicador ,
                                             short A1271ContratoServicosIndicador_Numero ,
                                             String A1274ContratoServicosIndicador_Indicador ,
                                             short AV14TFContratoServicosIndicador_QtdeFaixas ,
                                             short A1298ContratoServicosIndicador_QtdeFaixas ,
                                             short AV15TFContratoServicosIndicador_QtdeFaixas_To ,
                                             int AV34ContratoServicosIndicador_CntSrvCod ,
                                             int A1270ContratoServicosIndicador_CntSrvCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [9] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoServicosIndicador_Codigo], T1.[ContratoServicosIndicador_CntSrvCod], T1.[ContratoServicosIndicador_Indicador], T1.[ContratoServicosIndicador_Numero], COALESCE( T2.[ContratoServicosIndicador_QtdeFaixas], 0) AS ContratoServicosIndicador_QtdeFaixas FROM ([ContratoServicosIndicador] T1 WITH (NOLOCK) LEFT JOIN (SELECT COUNT(*) AS ContratoServicosIndicador_QtdeFaixas, [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_Codigo] ) T2 ON T2.[ContratoServicosIndicador_Codigo] = T1.[ContratoServicosIndicador_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoServicosIndicador_CntSrvCod] = @AV34ContratoServicosIndicador_CntSrvCod)";
         scmdbuf = scmdbuf + " and ((@AV14TFContratoServicosIndicador_QtdeFaixas = convert(int, 0)) or ( COALESCE( T2.[ContratoServicosIndicador_QtdeFaixas], 0) >= @AV14TFContratoServicosIndicador_QtdeFaixas))";
         scmdbuf = scmdbuf + " and ((@AV15TFContratoServicosIndicador_QtdeFaixas_To = convert(int, 0)) or ( COALESCE( T2.[ContratoServicosIndicador_QtdeFaixas], 0) <= @AV15TFContratoServicosIndicador_QtdeFaixas_To))";
         if ( ! (0==AV10TFContratoServicosIndicador_Numero) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] >= @AV10TFContratoServicosIndicador_Numero)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV11TFContratoServicosIndicador_Numero_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Numero] <= @AV11TFContratoServicosIndicador_Numero_To)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosIndicador_Indicador_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoServicosIndicador_Indicador)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] like @lV12TFContratoServicosIndicador_Indicador)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosIndicador_Indicador_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicador_Indicador] = @AV13TFContratoServicosIndicador_Indicador_Sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosIndicador_CntSrvCod], T1.[ContratoServicosIndicador_Indicador]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00JZ3(context, (short)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (short)dynConstraints[7] , (short)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00JZ3 ;
          prmP00JZ3 = new Object[] {
          new Object[] {"@AV34ContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV14TFContratoServicosIndicador_QtdeFaixas",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV15TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV15TFContratoServicosIndicador_QtdeFaixas_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV10TFContratoServicosIndicador_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV11TFContratoServicosIndicador_Numero_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV12TFContratoServicosIndicador_Indicador",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV13TFContratoServicosIndicador_Indicador_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00JZ3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JZ3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontroservicoindicadoreswcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontroservicoindicadoreswcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontroservicoindicadoreswcfilterdata") )
          {
             return  ;
          }
          getcontroservicoindicadoreswcfilterdata worker = new getcontroservicoindicadoreswcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
