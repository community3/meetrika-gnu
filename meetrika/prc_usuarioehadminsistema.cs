/*
               File: PRC_UsuarioEhAdminSistema
        Description: Eh Administrador do Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:41.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_usuarioehadminsistema : GXProcedure
   {
      public prc_usuarioehadminsistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_usuarioehadminsistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Usuario_Codigo ,
                           out bool aP1_vrEhAdminSistema )
      {
         this.AV8Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV9vrEhAdminSistema = false ;
         initialize();
         executePrivate();
         aP1_vrEhAdminSistema=this.AV9vrEhAdminSistema;
      }

      public bool executeUdp( int aP0_Usuario_Codigo )
      {
         this.AV8Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV9vrEhAdminSistema = false ;
         initialize();
         executePrivate();
         aP1_vrEhAdminSistema=this.AV9vrEhAdminSistema;
         return AV9vrEhAdminSistema ;
      }

      public void executeSubmit( int aP0_Usuario_Codigo ,
                                 out bool aP1_vrEhAdminSistema )
      {
         prc_usuarioehadminsistema objprc_usuarioehadminsistema;
         objprc_usuarioehadminsistema = new prc_usuarioehadminsistema();
         objprc_usuarioehadminsistema.AV8Usuario_Codigo = aP0_Usuario_Codigo;
         objprc_usuarioehadminsistema.AV9vrEhAdminSistema = false ;
         objprc_usuarioehadminsistema.context.SetSubmitInitialConfig(context);
         objprc_usuarioehadminsistema.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_usuarioehadminsistema);
         aP1_vrEhAdminSistema=this.AV9vrEhAdminSistema;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_usuarioehadminsistema)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P001N2 */
         pr_default.execute(0, new Object[] {AV8Usuario_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A3Perfil_Codigo = P001N2_A3Perfil_Codigo[0];
            A275Perfil_Tipo = P001N2_A275Perfil_Tipo[0];
            A276Perfil_Ativo = P001N2_A276Perfil_Ativo[0];
            A1Usuario_Codigo = P001N2_A1Usuario_Codigo[0];
            A275Perfil_Tipo = P001N2_A275Perfil_Tipo[0];
            A276Perfil_Ativo = P001N2_A276Perfil_Ativo[0];
            AV9vrEhAdminSistema = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P001N2_A3Perfil_Codigo = new int[1] ;
         P001N2_A275Perfil_Tipo = new short[1] ;
         P001N2_A276Perfil_Ativo = new bool[] {false} ;
         P001N2_A1Usuario_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_usuarioehadminsistema__default(),
            new Object[][] {
                new Object[] {
               P001N2_A3Perfil_Codigo, P001N2_A275Perfil_Tipo, P001N2_A276Perfil_Ativo, P001N2_A1Usuario_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A275Perfil_Tipo ;
      private int AV8Usuario_Codigo ;
      private int A3Perfil_Codigo ;
      private int A1Usuario_Codigo ;
      private String scmdbuf ;
      private bool AV9vrEhAdminSistema ;
      private bool A276Perfil_Ativo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P001N2_A3Perfil_Codigo ;
      private short[] P001N2_A275Perfil_Tipo ;
      private bool[] P001N2_A276Perfil_Ativo ;
      private int[] P001N2_A1Usuario_Codigo ;
      private bool aP1_vrEhAdminSistema ;
   }

   public class prc_usuarioehadminsistema__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP001N2 ;
          prmP001N2 = new Object[] {
          new Object[] {"@AV8Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P001N2", "SELECT TOP 1 T1.[Perfil_Codigo], T2.[Perfil_Tipo], T2.[Perfil_Ativo], T1.[Usuario_Codigo] FROM ([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Perfil] T2 WITH (NOLOCK) ON T2.[Perfil_Codigo] = T1.[Perfil_Codigo]) WHERE (T1.[Usuario_Codigo] = @AV8Usuario_Codigo) AND (T2.[Perfil_Ativo] = 1) AND (T2.[Perfil_Tipo] = 99) ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001N2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
