/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 0:18:37.20
*/
gx.evt.autoSkip = false;
gx.define('contratada', false, function () {
   this.ServerClass =  "contratada" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("trn");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV7Contratada_Codigo=gx.fn.getIntegerValue("vCONTRATADA_CODIGO",'.') ;
      this.AV14Insert_Contratada_AreaTrabalhoCod=gx.fn.getIntegerValue("vINSERT_CONTRATADA_AREATRABALHOCOD",'.') ;
      this.Gx_BScreen=gx.fn.getIntegerValue("vGXBSCREEN",'.') ;
      this.A52Contratada_AreaTrabalhoCod=gx.fn.getIntegerValue("CONTRATADA_AREATRABALHOCOD",'.') ;
      this.AV11Insert_Contratada_PessoaCod=gx.fn.getIntegerValue("vINSERT_CONTRATADA_PESSOACOD",'.') ;
      this.AV26Insert_Contratada_MunicipioCod=gx.fn.getIntegerValue("vINSERT_CONTRATADA_MUNICIPIOCOD",'.') ;
      this.A40000Contratada_Logo_GXI=gx.fn.getControlValue("CONTRATADA_LOGO_GXI") ;
      this.AV37IsConfiguracoesValidas=gx.fn.getControlValue("vISCONFIGURACOESVALIDAS") ;
      this.A42Contratada_PessoaCNPJ=gx.fn.getControlValue("CONTRATADA_PESSOACNPJ") ;
      this.A41Contratada_PessoaNom=gx.fn.getControlValue("CONTRATADA_PESSOANOM") ;
      this.A342Contratada_BancoNome=gx.fn.getControlValue("CONTRATADA_BANCONOME") ;
      this.A343Contratada_BancoNro=gx.fn.getControlValue("CONTRATADA_BANCONRO") ;
      this.A344Contratada_AgenciaNome=gx.fn.getControlValue("CONTRATADA_AGENCIANOME") ;
      this.A345Contratada_AgenciaNro=gx.fn.getControlValue("CONTRATADA_AGENCIANRO") ;
      this.A51Contratada_ContaCorrente=gx.fn.getControlValue("CONTRATADA_CONTACORRENTE") ;
      this.A350Contratada_UF=gx.fn.getControlValue("CONTRATADA_UF") ;
      this.A518Pessoa_IE=gx.fn.getControlValue("PESSOA_IE") ;
      this.A519Pessoa_Endereco=gx.fn.getControlValue("PESSOA_ENDERECO") ;
      this.A521Pessoa_CEP=gx.fn.getControlValue("PESSOA_CEP") ;
      this.A522Pessoa_Telefone=gx.fn.getControlValue("PESSOA_TELEFONE") ;
      this.A523Pessoa_Fax=gx.fn.getControlValue("PESSOA_FAX") ;
      this.AV28ok=gx.fn.getIntegerValue("vOK",'.') ;
      this.AV35AuditingObject=gx.fn.getControlValue("vAUDITINGOBJECT") ;
      this.A1127Contratada_LogoArquivo=gx.fn.getBlobValue("CONTRATADA_LOGOARQUIVO") ;
      this.A1129Contratada_LogoTipoArq=gx.fn.getControlValue("CONTRATADA_LOGOTIPOARQ") ;
      this.A1128Contratada_LogoNomeArq=gx.fn.getControlValue("CONTRATADA_LOGONOMEARQ") ;
      this.A53Contratada_AreaTrabalhoDes=gx.fn.getControlValue("CONTRATADA_AREATRABALHODES") ;
      this.A1592Contratada_AreaTrbClcPFnl=gx.fn.getControlValue("CONTRATADA_AREATRBCLCPFNL") ;
      this.A1595Contratada_AreaTrbSrvPdr=gx.fn.getIntegerValue("CONTRATADA_AREATRBSRVPDR",'.') ;
      this.AV39Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.Gx_mode=gx.fn.getControlValue("vMODE") ;
      this.AV8WWPContext=gx.fn.getControlValue("vWWPCONTEXT") ;
      this.AV9TrnContext=gx.fn.getControlValue("vTRNCONTEXT") ;
   };
   this.Valid_Contratada_codigo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTRATADA_CODIGO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contratada_tipofabrica=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTRATADA_TIPOFABRICA");
         this.AnyError  = 0;
         if ( ! ( ( this.A516Contratada_TipoFabrica == "S" ) || ( this.A516Contratada_TipoFabrica == "M" ) || ( this.A516Contratada_TipoFabrica == "I" ) || ( this.A516Contratada_TipoFabrica == "O" ) ) )
         {
            try {
               gxballoon.setError("Campo Tipo fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }
         try {
            if ( ! ( this.A516Contratada_TipoFabrica == "I" ) )
            {
               gx.fn.setCtrlProperty("vPESSOA_IE","Visible", true );
            }
         }
         catch(e){}
         try {
            if ( ! ( this.A516Contratada_TipoFabrica == "I" ) )
            {
               gx.fn.setCtrlProperty("TEXTBLOCKPESSOA_IE","Visible", true );
            }
         }
         catch(e){}
         try {
            if ( ! ( this.A516Contratada_TipoFabrica == "I" ) )
            {
               gx.fn.setCtrlProperty("vCONTRATADA_CNPJ","Visible", true );
            }
         }
         catch(e){}
         try {
            if ( ! ( this.A516Contratada_TipoFabrica == "I" ) )
            {
               gx.fn.setCtrlProperty("TEXTBLOCKCONTRATADA_CNPJ","Visible", true );
            }
         }
         catch(e){}

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratada_razaosocial=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATADA_RAZAOSOCIAL");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contratada_logo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTRATADA_LOGO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratada_cnpj=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATADA_CNPJ");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Pessoa_ie=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vPESSOA_IE");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contratada_sigla=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTRATADA_SIGLA");
         this.AnyError  = 0;
         if ( ((''==this.A438Contratada_Sigla)) )
         {
            try {
               gxballoon.setError("Sigla é obrigatório.");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratada_banconome=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATADA_BANCONOME");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratada_banconro=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATADA_BANCONRO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratada_agencianome=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATADA_AGENCIANOME");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratada_agencianro=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATADA_AGENCIANRO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratada_contacorrente=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATADA_CONTACORRENTE");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Pessoa_endereco=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vPESSOA_ENDERECO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Pessoa_cep=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vPESSOA_CEP");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratada_uf=function()
   {
      gx.ajax.validSrvEvt("dyncall","Validv_Contratada_uf",["gx.O.AV25Contratada_UF", "gx.O.A349Contratada_MunicipioCod"],["A349Contratada_MunicipioCod"]);
      return true;
   }
   this.Valid_Contratada_municipiocod=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Contratada_municipiocod",["gx.O.Gx_mode", "gx.O.A349Contratada_MunicipioCod", "gx.O.A350Contratada_UF", "gx.O.AV25Contratada_UF"],["A350Contratada_UF", "AV25Contratada_UF"]);
      return true;
   }
   this.Validv_Pessoa_telefone=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vPESSOA_TELEFONE");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Pessoa_fax=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vPESSOA_FAX");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contratada_ospreferencial=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTRATADA_OSPREFERENCIAL");
         this.AnyError  = 0;
         try {
            gx.fn.setCtrlProperty("CONTRATADA_OSPREFERENCIAL_RIGHTTEXT","Visible", this.A1867Contratada_OSPreferencial );
         }
         catch(e){}

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratada_pessoacod=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATADA_PESSOACOD");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contratada_pessoacod=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Contratada_pessoacod",["gx.O.Gx_mode", "gx.O.A40Contratada_PessoaCod", "gx.O.A41Contratada_PessoaNom", "gx.O.AV24Contratada_RazaoSocial", "gx.O.A42Contratada_PessoaCNPJ", "gx.O.AV21Contratada_CNPJ", "gx.O.A516Contratada_TipoFabrica", "gx.O.A518Pessoa_IE", "gx.O.AV32Pessoa_IE", "gx.O.A519Pessoa_Endereco", "gx.O.AV30Pessoa_Endereco", "gx.O.A521Pessoa_CEP", "gx.O.AV29Pessoa_CEP", "gx.O.A522Pessoa_Telefone", "gx.O.AV33Pessoa_Telefone", "gx.O.A523Pessoa_Fax", "gx.O.AV31Pessoa_Fax", "gx.O.AV23Contratada_PessoaCod"],["A41Contratada_PessoaNom", "A42Contratada_PessoaCNPJ", "A518Pessoa_IE", "A519Pessoa_Endereco", "A521Pessoa_CEP", "A522Pessoa_Telefone", "A523Pessoa_Fax", "AV24Contratada_RazaoSocial", "AV21Contratada_CNPJ", "AV32Pessoa_IE", "AV30Pessoa_Endereco", "AV29Pessoa_CEP", "AV33Pessoa_Telefone", "AV31Pessoa_Fax", "AV23Contratada_PessoaCod"]);
      return true;
   }
   this.s112_client=function()
   {
      gx.fn.setCtrlProperty("CONTRATADA_USAOSISTEMA","Visible", false );
      gx.fn.setCtrlProperty("CONTRATADA_USAOSISTEMA_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("TEXTBLOCKCONTRATADA_USAOSISTEMA_CELL","Class", "Invisible" );
   };
   this.e110c13_client=function()
   {
      this.clearMessages();
      if ( this.A516Contratada_TipoFabrica == "I" )
      {
         this.AV21Contratada_CNPJ =  "Interno"  ;
      }
      this.refreshOutputs([{av:'AV21Contratada_CNPJ',fld:'vCONTRATADA_CNPJ',pic:'',nv:''}]);
   };
   this.e130c2_client=function()
   {
      this.executeServerEvent("AFTER TRN", true, null, false, false);
   };
   this.e140c2_client=function()
   {
      this.executeServerEvent("VCONTRATADA_CNPJ.ISVALID", true, null, false, true);
   };
   this.e150c13_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e160c13_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,14,17,19,22,24,26,28,31,33,35,37,40,42,45,47,49,51,54,56,58,60,62,64,67,69,72,74,76,78,80,82,85,87,89,91,96,98,100,102,104,106,109,111,113,115,118,120,122,124,127,129,133,143,144,145,146,147];
   this.GXLastCtrlId =147;
   this.DVPANEL_TABLEATTRIBUTESContainer = gx.uc.getNew(this, 12, 0, "BootstrapPanel", "DVPANEL_TABLEATTRIBUTESContainer", "Dvpanel_tableattributes");
   var DVPANEL_TABLEATTRIBUTESContainer = this.DVPANEL_TABLEATTRIBUTESContainer;
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Width", "Width", "100%", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Height", "Height", "100", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Title", "Title", "Contratada / Departamento", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsible", "Collapsible", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Visible", "Visible", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Class", "Class", "", "char");
   DVPANEL_TABLEATTRIBUTESContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_TABLEATTRIBUTESContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[5]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[14]={fld:"TABLEATTRIBUTES",grid:0};
   GXValidFnc[17]={fld:"TEXTBLOCKCONTRATADA_TIPOFABRICA", format:0,grid:0};
   GXValidFnc[19]={lvl:0,type:"char",len:1,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contratada_tipofabrica,isvalid:null,rgrid:[],fld:"CONTRATADA_TIPOFABRICA",gxz:"Z516Contratada_TipoFabrica",gxold:"O516Contratada_TipoFabrica",gxvar:"A516Contratada_TipoFabrica",ucs:[],op:[19],ip:[19],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A516Contratada_TipoFabrica=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z516Contratada_TipoFabrica=Value},v2c:function(){gx.fn.setComboBoxValue("CONTRATADA_TIPOFABRICA",gx.O.A516Contratada_TipoFabrica);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A516Contratada_TipoFabrica=this.val()},val:function(){return gx.fn.getControlValue("CONTRATADA_TIPOFABRICA")},nac:gx.falseFn};
   this.declareDomainHdlr( 19 , function() {
   });
   GXValidFnc[22]={fld:"TEXTBLOCKCONTRATADA_RAZAOSOCIAL", format:0,grid:0};
   GXValidFnc[24]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratada_razaosocial,isvalid:null,rgrid:[],fld:"vCONTRATADA_RAZAOSOCIAL",gxz:"ZV24Contratada_RazaoSocial",gxold:"OV24Contratada_RazaoSocial",gxvar:"AV24Contratada_RazaoSocial",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV24Contratada_RazaoSocial=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV24Contratada_RazaoSocial=Value},v2c:function(){gx.fn.setControlValue("vCONTRATADA_RAZAOSOCIAL",gx.O.AV24Contratada_RazaoSocial,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV24Contratada_RazaoSocial=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATADA_RAZAOSOCIAL")},nac:gx.falseFn};
   GXValidFnc[26]={fld:"TEXTBLOCKCONTRATADA_LOGO", format:0,grid:0};
   GXValidFnc[28]={lvl:0,type:"bits",len:1024,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contratada_logo,isvalid:null,rgrid:[],fld:"CONTRATADA_LOGO",gxz:"Z1664Contratada_Logo",gxold:"O1664Contratada_Logo",gxvar:"A1664Contratada_Logo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1664Contratada_Logo=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1664Contratada_Logo=Value},v2c:function(){gx.fn.setMultimediaValue("CONTRATADA_LOGO",gx.O.A1664Contratada_Logo,gx.O.A40000Contratada_Logo_GXI)},c2v:function(){gx.O.A40000Contratada_Logo_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.A1664Contratada_Logo=this.val()},val:function(){return gx.fn.getBlobValue("CONTRATADA_LOGO")},val_GXI:function(){return gx.fn.getControlValue("CONTRATADA_LOGO_GXI")}, gxvar_GXI:'A40000Contratada_Logo_GXI',nac:gx.falseFn};
   GXValidFnc[31]={fld:"TEXTBLOCKCONTRATADA_CNPJ", format:0,grid:0};
   GXValidFnc[33]={lvl:0,type:"svchar",len:15,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratada_cnpj,isvalid:'e140c2_client',rgrid:[],fld:"vCONTRATADA_CNPJ",gxz:"ZV21Contratada_CNPJ",gxold:"OV21Contratada_CNPJ",gxvar:"AV21Contratada_CNPJ",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV21Contratada_CNPJ=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV21Contratada_CNPJ=Value},v2c:function(){gx.fn.setControlValue("vCONTRATADA_CNPJ",gx.O.AV21Contratada_CNPJ,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV21Contratada_CNPJ=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATADA_CNPJ")},nac:gx.falseFn};
   GXValidFnc[35]={fld:"TEXTBLOCKPESSOA_IE", format:0,grid:0};
   GXValidFnc[37]={lvl:0,type:"char",len:15,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Pessoa_ie,isvalid:null,rgrid:[],fld:"vPESSOA_IE",gxz:"ZV32Pessoa_IE",gxold:"OV32Pessoa_IE",gxvar:"AV32Pessoa_IE",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV32Pessoa_IE=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV32Pessoa_IE=Value},v2c:function(){gx.fn.setControlValue("vPESSOA_IE",gx.O.AV32Pessoa_IE,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV32Pessoa_IE=this.val()},val:function(){return gx.fn.getControlValue("vPESSOA_IE")},nac:gx.falseFn};
   GXValidFnc[40]={fld:"TEXTBLOCKCONTRATADA_SIGLA", format:0,grid:0};
   GXValidFnc[42]={lvl:0,type:"char",len:15,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contratada_sigla,isvalid:null,rgrid:[],fld:"CONTRATADA_SIGLA",gxz:"Z438Contratada_Sigla",gxold:"O438Contratada_Sigla",gxvar:"A438Contratada_Sigla",ucs:[],op:[42],ip:[42],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A438Contratada_Sigla=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z438Contratada_Sigla=Value},v2c:function(){gx.fn.setControlValue("CONTRATADA_SIGLA",gx.O.A438Contratada_Sigla,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A438Contratada_Sigla=this.val()},val:function(){return gx.fn.getControlValue("CONTRATADA_SIGLA")},nac:gx.falseFn};
   this.declareDomainHdlr( 42 , function() {
   });
   GXValidFnc[45]={fld:"TEXTBLOCKCONTRATADA_BANCONOME", format:0,grid:0};
   GXValidFnc[47]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratada_banconome,isvalid:null,rgrid:[],fld:"vCONTRATADA_BANCONOME",gxz:"ZV19Contratada_BancoNome",gxold:"OV19Contratada_BancoNome",gxvar:"AV19Contratada_BancoNome",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV19Contratada_BancoNome=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV19Contratada_BancoNome=Value},v2c:function(){gx.fn.setControlValue("vCONTRATADA_BANCONOME",gx.O.AV19Contratada_BancoNome,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV19Contratada_BancoNome=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATADA_BANCONOME")},nac:gx.falseFn};
   GXValidFnc[49]={fld:"TEXTBLOCKCONTRATADA_BANCONRO", format:0,grid:0};
   GXValidFnc[51]={lvl:0,type:"char",len:6,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratada_banconro,isvalid:null,rgrid:[],fld:"vCONTRATADA_BANCONRO",gxz:"ZV20Contratada_BancoNro",gxold:"OV20Contratada_BancoNro",gxvar:"AV20Contratada_BancoNro",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV20Contratada_BancoNro=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV20Contratada_BancoNro=Value},v2c:function(){gx.fn.setControlValue("vCONTRATADA_BANCONRO",gx.O.AV20Contratada_BancoNro,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV20Contratada_BancoNro=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATADA_BANCONRO")},nac:gx.falseFn};
   GXValidFnc[54]={fld:"TEXTBLOCKCONTRATADA_AGENCIANOME", format:0,grid:0};
   GXValidFnc[56]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratada_agencianome,isvalid:null,rgrid:[],fld:"vCONTRATADA_AGENCIANOME",gxz:"ZV17Contratada_AgenciaNome",gxold:"OV17Contratada_AgenciaNome",gxvar:"AV17Contratada_AgenciaNome",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV17Contratada_AgenciaNome=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV17Contratada_AgenciaNome=Value},v2c:function(){gx.fn.setControlValue("vCONTRATADA_AGENCIANOME",gx.O.AV17Contratada_AgenciaNome,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV17Contratada_AgenciaNome=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATADA_AGENCIANOME")},nac:gx.falseFn};
   GXValidFnc[58]={fld:"TEXTBLOCKCONTRATADA_AGENCIANRO", format:0,grid:0};
   GXValidFnc[60]={lvl:0,type:"char",len:10,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratada_agencianro,isvalid:null,rgrid:[],fld:"vCONTRATADA_AGENCIANRO",gxz:"ZV18Contratada_AgenciaNro",gxold:"OV18Contratada_AgenciaNro",gxvar:"AV18Contratada_AgenciaNro",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV18Contratada_AgenciaNro=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV18Contratada_AgenciaNro=Value},v2c:function(){gx.fn.setControlValue("vCONTRATADA_AGENCIANRO",gx.O.AV18Contratada_AgenciaNro,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV18Contratada_AgenciaNro=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATADA_AGENCIANRO")},nac:gx.falseFn};
   GXValidFnc[62]={fld:"TEXTBLOCKCONTRATADA_CONTACORRENTE", format:0,grid:0};
   GXValidFnc[64]={lvl:0,type:"char",len:10,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratada_contacorrente,isvalid:null,rgrid:[],fld:"vCONTRATADA_CONTACORRENTE",gxz:"ZV22Contratada_ContaCorrente",gxold:"OV22Contratada_ContaCorrente",gxvar:"AV22Contratada_ContaCorrente",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV22Contratada_ContaCorrente=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV22Contratada_ContaCorrente=Value},v2c:function(){gx.fn.setControlValue("vCONTRATADA_CONTACORRENTE",gx.O.AV22Contratada_ContaCorrente,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV22Contratada_ContaCorrente=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATADA_CONTACORRENTE")},nac:gx.falseFn};
   GXValidFnc[67]={fld:"TEXTBLOCKPESSOA_ENDERECO", format:0,grid:0};
   GXValidFnc[69]={lvl:0,type:"svchar",len:100,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Pessoa_endereco,isvalid:null,rgrid:[],fld:"vPESSOA_ENDERECO",gxz:"ZV30Pessoa_Endereco",gxold:"OV30Pessoa_Endereco",gxvar:"AV30Pessoa_Endereco",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV30Pessoa_Endereco=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV30Pessoa_Endereco=Value},v2c:function(){gx.fn.setControlValue("vPESSOA_ENDERECO",gx.O.AV30Pessoa_Endereco,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV30Pessoa_Endereco=this.val()},val:function(){return gx.fn.getControlValue("vPESSOA_ENDERECO")},nac:gx.falseFn};
   GXValidFnc[72]={fld:"TEXTBLOCKPESSOA_CEP", format:0,grid:0};
   GXValidFnc[74]={lvl:0,type:"char",len:10,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Pessoa_cep,isvalid:null,rgrid:[],fld:"vPESSOA_CEP",gxz:"ZV29Pessoa_CEP",gxold:"OV29Pessoa_CEP",gxvar:"AV29Pessoa_CEP",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV29Pessoa_CEP=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV29Pessoa_CEP=Value},v2c:function(){gx.fn.setControlValue("vPESSOA_CEP",gx.O.AV29Pessoa_CEP,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV29Pessoa_CEP=this.val()},val:function(){return gx.fn.getControlValue("vPESSOA_CEP")},nac:gx.falseFn};
   GXValidFnc[76]={fld:"TEXTBLOCKCONTRATADA_UF", format:0,grid:0};
   GXValidFnc[78]={lvl:0,type:"char",len:2,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratada_uf,isvalid:null,rgrid:[],fld:"vCONTRATADA_UF",gxz:"ZV25Contratada_UF",gxold:"OV25Contratada_UF",gxvar:"AV25Contratada_UF",ucs:[],op:[82],ip:[82,78],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV25Contratada_UF=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV25Contratada_UF=Value},v2c:function(){gx.fn.setComboBoxValue("vCONTRATADA_UF",gx.O.AV25Contratada_UF)},c2v:function(){if(this.val()!==undefined)gx.O.AV25Contratada_UF=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATADA_UF")},nac:gx.falseFn};
   GXValidFnc[80]={fld:"TEXTBLOCKCONTRATADA_MUNICIPIOCOD", format:0,grid:0};
   GXValidFnc[82]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contratada_municipiocod,isvalid:null,rgrid:[],fld:"CONTRATADA_MUNICIPIOCOD",gxz:"Z349Contratada_MunicipioCod",gxold:"O349Contratada_MunicipioCod",gxvar:"A349Contratada_MunicipioCod",ucs:[],op:[78],ip:[78,82],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.A349Contratada_MunicipioCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z349Contratada_MunicipioCod=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("CONTRATADA_MUNICIPIOCOD",gx.O.A349Contratada_MunicipioCod);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A349Contratada_MunicipioCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTRATADA_MUNICIPIOCOD",'.')},nac:function(){return (this.Gx_mode=="INS")&&!((0==this.AV26Insert_Contratada_MunicipioCod))}};
   this.declareDomainHdlr( 82 , function() {
   });
   GXValidFnc[85]={fld:"TEXTBLOCKPESSOA_TELEFONE", format:0,grid:0};
   GXValidFnc[87]={lvl:0,type:"char",len:15,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Pessoa_telefone,isvalid:null,rgrid:[],fld:"vPESSOA_TELEFONE",gxz:"ZV33Pessoa_Telefone",gxold:"OV33Pessoa_Telefone",gxvar:"AV33Pessoa_Telefone",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV33Pessoa_Telefone=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV33Pessoa_Telefone=Value},v2c:function(){gx.fn.setControlValue("vPESSOA_TELEFONE",gx.O.AV33Pessoa_Telefone,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV33Pessoa_Telefone=this.val()},val:function(){return gx.fn.getControlValue("vPESSOA_TELEFONE")},nac:gx.falseFn};
   GXValidFnc[89]={fld:"TEXTBLOCKPESSOA_FAX", format:0,grid:0};
   GXValidFnc[91]={lvl:0,type:"char",len:15,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Pessoa_fax,isvalid:null,rgrid:[],fld:"vPESSOA_FAX",gxz:"ZV31Pessoa_Fax",gxold:"OV31Pessoa_Fax",gxvar:"AV31Pessoa_Fax",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV31Pessoa_Fax=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV31Pessoa_Fax=Value},v2c:function(){gx.fn.setControlValue("vPESSOA_FAX",gx.O.AV31Pessoa_Fax,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV31Pessoa_Fax=this.val()},val:function(){return gx.fn.getControlValue("vPESSOA_FAX")},nac:gx.falseFn};
   GXValidFnc[96]={fld:"TEXTBLOCKCONTRATADA_SS", format:0,grid:0};
   GXValidFnc[98]={lvl:0,type:"int",len:8,dec:0,sign:false,pic:"ZZZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATADA_SS",gxz:"Z1451Contratada_SS",gxold:"O1451Contratada_SS",gxvar:"A1451Contratada_SS",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1451Contratada_SS=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1451Contratada_SS=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("CONTRATADA_SS",gx.O.A1451Contratada_SS,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1451Contratada_SS=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTRATADA_SS",'.')},nac:gx.falseFn};
   GXValidFnc[100]={fld:"TEXTBLOCKCONTRATADA_OS", format:0,grid:0};
   GXValidFnc[102]={lvl:0,type:"int",len:8,dec:0,sign:false,pic:"ZZZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATADA_OS",gxz:"Z524Contratada_OS",gxold:"O524Contratada_OS",gxvar:"A524Contratada_OS",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A524Contratada_OS=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z524Contratada_OS=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("CONTRATADA_OS",gx.O.A524Contratada_OS,0)},c2v:function(){if(this.val()!==undefined)gx.O.A524Contratada_OS=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTRATADA_OS",'.')},nac:gx.falseFn};
   GXValidFnc[104]={fld:"TEXTBLOCKCONTRATADA_LOTE", format:0,grid:0};
   GXValidFnc[106]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATADA_LOTE",gxz:"Z530Contratada_Lote",gxold:"O530Contratada_Lote",gxvar:"A530Contratada_Lote",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A530Contratada_Lote=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z530Contratada_Lote=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("CONTRATADA_LOTE",gx.O.A530Contratada_Lote,0)},c2v:function(){if(this.val()!==undefined)gx.O.A530Contratada_Lote=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTRATADA_LOTE",'.')},nac:gx.falseFn};
   GXValidFnc[109]={fld:"TEXTBLOCKCONTRATADA_USAOSISTEMA", format:0,grid:0};
   GXValidFnc[111]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATADA_USAOSISTEMA",gxz:"Z1481Contratada_UsaOSistema",gxold:"O1481Contratada_UsaOSistema",gxvar:"A1481Contratada_UsaOSistema",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1481Contratada_UsaOSistema=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1481Contratada_UsaOSistema=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setComboBoxValue("CONTRATADA_USAOSISTEMA",gx.O.A1481Contratada_UsaOSistema);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1481Contratada_UsaOSistema=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("CONTRATADA_USAOSISTEMA")},nac:gx.falseFn};
   this.declareDomainHdlr( 111 , function() {
   });
   GXValidFnc[113]={fld:"TEXTBLOCKCONTRATADA_OSPREFERENCIAL", format:0,grid:0};
   GXValidFnc[115]={fld:"TABLEMERGEDCONTRATADA_OSPREFERENCIAL",grid:0};
   GXValidFnc[118]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contratada_ospreferencial,isvalid:null,rgrid:[],fld:"CONTRATADA_OSPREFERENCIAL",gxz:"Z1867Contratada_OSPreferencial",gxold:"O1867Contratada_OSPreferencial",gxvar:"A1867Contratada_OSPreferencial",ucs:[],op:[],ip:[118],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1867Contratada_OSPreferencial=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1867Contratada_OSPreferencial=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setComboBoxValue("CONTRATADA_OSPREFERENCIAL",gx.O.A1867Contratada_OSPreferencial);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1867Contratada_OSPreferencial=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("CONTRATADA_OSPREFERENCIAL")},nac:gx.falseFn};
   this.declareDomainHdlr( 118 , function() {
   });
   GXValidFnc[120]={fld:"CONTRATADA_OSPREFERENCIAL_RIGHTTEXT", format:0,grid:0};
   GXValidFnc[122]={fld:"TEXTBLOCKCONTRATADA_CNTPADRAO", format:0,grid:0};
   GXValidFnc[124]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATADA_CNTPADRAO",gxz:"Z1953Contratada_CntPadrao",gxold:"O1953Contratada_CntPadrao",gxvar:"A1953Contratada_CntPadrao",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1953Contratada_CntPadrao=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1953Contratada_CntPadrao=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("CONTRATADA_CNTPADRAO",gx.O.A1953Contratada_CntPadrao,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1953Contratada_CntPadrao=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTRATADA_CNTPADRAO",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 124 , function() {
   });
   GXValidFnc[127]={fld:"TEXTBLOCKCONTRATADA_ATIVO", format:0,grid:0};
   GXValidFnc[129]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATADA_ATIVO",gxz:"Z43Contratada_Ativo",gxold:"O43Contratada_Ativo",gxvar:"A43Contratada_Ativo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.A43Contratada_Ativo=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z43Contratada_Ativo=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("CONTRATADA_ATIVO",gx.O.A43Contratada_Ativo,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A43Contratada_Ativo=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("CONTRATADA_ATIVO")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 129 , function() {
   });
   GXValidFnc[133]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[143]={fld:"TBJAVA", format:1,grid:0};
   GXValidFnc[144]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vCONTRATO_CODIGO",gxz:"ZV36Contrato_Codigo",gxold:"OV36Contrato_Codigo",gxvar:"AV36Contrato_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV36Contrato_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV36Contrato_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vCONTRATO_CODIGO",gx.O.AV36Contrato_Codigo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV36Contrato_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTRATO_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[145]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Validv_Contratada_pessoacod,isvalid:null,rgrid:[],fld:"vCONTRATADA_PESSOACOD",gxz:"ZV23Contratada_PessoaCod",gxold:"OV23Contratada_PessoaCod",gxvar:"AV23Contratada_PessoaCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV23Contratada_PessoaCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV23Contratada_PessoaCod=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vCONTRATADA_PESSOACOD",gx.O.AV23Contratada_PessoaCod,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV23Contratada_PessoaCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTRATADA_PESSOACOD",'.')},nac:gx.falseFn};
   GXValidFnc[146]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Contratada_codigo,isvalid:null,rgrid:[],fld:"CONTRATADA_CODIGO",gxz:"Z39Contratada_Codigo",gxold:"O39Contratada_Codigo",gxvar:"A39Contratada_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A39Contratada_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z39Contratada_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("CONTRATADA_CODIGO",gx.O.A39Contratada_Codigo,0)},c2v:function(){if(this.val()!==undefined)gx.O.A39Contratada_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTRATADA_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[147]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contratada_pessoacod,isvalid:null,rgrid:[],fld:"CONTRATADA_PESSOACOD",gxz:"Z40Contratada_PessoaCod",gxold:"O40Contratada_PessoaCod",gxvar:"A40Contratada_PessoaCod",ucs:[],op:[145,91,87,74,69,37,19,33,24],ip:[145,91,87,74,69,37,19,33,24,147],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A40Contratada_PessoaCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z40Contratada_PessoaCod=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("CONTRATADA_PESSOACOD",gx.O.A40Contratada_PessoaCod,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A40Contratada_PessoaCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTRATADA_PESSOACOD",'.')},nac:function(){return (this.Gx_mode=="INS")&&!((0==this.AV11Insert_Contratada_PessoaCod))}};
   this.declareDomainHdlr( 147 , function() {
   });
   this.A516Contratada_TipoFabrica = "" ;
   this.Z516Contratada_TipoFabrica = "" ;
   this.O516Contratada_TipoFabrica = "" ;
   this.AV24Contratada_RazaoSocial = "" ;
   this.ZV24Contratada_RazaoSocial = "" ;
   this.OV24Contratada_RazaoSocial = "" ;
   this.A40000Contratada_Logo_GXI = "" ;
   this.A1664Contratada_Logo = "" ;
   this.Z1664Contratada_Logo = "" ;
   this.O1664Contratada_Logo = "" ;
   this.AV21Contratada_CNPJ = "" ;
   this.ZV21Contratada_CNPJ = "" ;
   this.OV21Contratada_CNPJ = "" ;
   this.AV32Pessoa_IE = "" ;
   this.ZV32Pessoa_IE = "" ;
   this.OV32Pessoa_IE = "" ;
   this.A438Contratada_Sigla = "" ;
   this.Z438Contratada_Sigla = "" ;
   this.O438Contratada_Sigla = "" ;
   this.AV19Contratada_BancoNome = "" ;
   this.ZV19Contratada_BancoNome = "" ;
   this.OV19Contratada_BancoNome = "" ;
   this.AV20Contratada_BancoNro = "" ;
   this.ZV20Contratada_BancoNro = "" ;
   this.OV20Contratada_BancoNro = "" ;
   this.AV17Contratada_AgenciaNome = "" ;
   this.ZV17Contratada_AgenciaNome = "" ;
   this.OV17Contratada_AgenciaNome = "" ;
   this.AV18Contratada_AgenciaNro = "" ;
   this.ZV18Contratada_AgenciaNro = "" ;
   this.OV18Contratada_AgenciaNro = "" ;
   this.AV22Contratada_ContaCorrente = "" ;
   this.ZV22Contratada_ContaCorrente = "" ;
   this.OV22Contratada_ContaCorrente = "" ;
   this.AV30Pessoa_Endereco = "" ;
   this.ZV30Pessoa_Endereco = "" ;
   this.OV30Pessoa_Endereco = "" ;
   this.AV29Pessoa_CEP = "" ;
   this.ZV29Pessoa_CEP = "" ;
   this.OV29Pessoa_CEP = "" ;
   this.AV25Contratada_UF = "" ;
   this.ZV25Contratada_UF = "" ;
   this.OV25Contratada_UF = "" ;
   this.A349Contratada_MunicipioCod = 0 ;
   this.Z349Contratada_MunicipioCod = 0 ;
   this.O349Contratada_MunicipioCod = 0 ;
   this.AV33Pessoa_Telefone = "" ;
   this.ZV33Pessoa_Telefone = "" ;
   this.OV33Pessoa_Telefone = "" ;
   this.AV31Pessoa_Fax = "" ;
   this.ZV31Pessoa_Fax = "" ;
   this.OV31Pessoa_Fax = "" ;
   this.A1451Contratada_SS = 0 ;
   this.Z1451Contratada_SS = 0 ;
   this.O1451Contratada_SS = 0 ;
   this.A524Contratada_OS = 0 ;
   this.Z524Contratada_OS = 0 ;
   this.O524Contratada_OS = 0 ;
   this.A530Contratada_Lote = 0 ;
   this.Z530Contratada_Lote = 0 ;
   this.O530Contratada_Lote = 0 ;
   this.A1481Contratada_UsaOSistema = false ;
   this.Z1481Contratada_UsaOSistema = false ;
   this.O1481Contratada_UsaOSistema = false ;
   this.A1867Contratada_OSPreferencial = false ;
   this.Z1867Contratada_OSPreferencial = false ;
   this.O1867Contratada_OSPreferencial = false ;
   this.A1953Contratada_CntPadrao = 0 ;
   this.Z1953Contratada_CntPadrao = 0 ;
   this.O1953Contratada_CntPadrao = 0 ;
   this.A43Contratada_Ativo = false ;
   this.Z43Contratada_Ativo = false ;
   this.O43Contratada_Ativo = false ;
   this.AV36Contrato_Codigo = 0 ;
   this.ZV36Contrato_Codigo = 0 ;
   this.OV36Contrato_Codigo = 0 ;
   this.AV23Contratada_PessoaCod = 0 ;
   this.ZV23Contratada_PessoaCod = 0 ;
   this.OV23Contratada_PessoaCod = 0 ;
   this.A39Contratada_Codigo = 0 ;
   this.Z39Contratada_Codigo = 0 ;
   this.O39Contratada_Codigo = 0 ;
   this.A40Contratada_PessoaCod = 0 ;
   this.Z40Contratada_PessoaCod = 0 ;
   this.O40Contratada_PessoaCod = 0 ;
   this.A40000Contratada_Logo_GXI = "" ;
   this.AV8WWPContext = {} ;
   this.AV9TrnContext = {} ;
   this.AV40GXV1 = 0 ;
   this.AV14Insert_Contratada_AreaTrabalhoCod = 0 ;
   this.AV11Insert_Contratada_PessoaCod = 0 ;
   this.AV26Insert_Contratada_MunicipioCod = 0 ;
   this.AV23Contratada_PessoaCod = 0 ;
   this.AV13TrnContextAtt = {} ;
   this.AV7Contratada_Codigo = 0 ;
   this.AV10WebSession = {} ;
   this.A39Contratada_Codigo = 0 ;
   this.A52Contratada_AreaTrabalhoCod = 0 ;
   this.A40Contratada_PessoaCod = 0 ;
   this.A349Contratada_MunicipioCod = 0 ;
   this.AV24Contratada_RazaoSocial = "" ;
   this.AV21Contratada_CNPJ = "" ;
   this.AV32Pessoa_IE = "" ;
   this.AV19Contratada_BancoNome = "" ;
   this.AV20Contratada_BancoNro = "" ;
   this.AV17Contratada_AgenciaNome = "" ;
   this.AV18Contratada_AgenciaNro = "" ;
   this.AV22Contratada_ContaCorrente = "" ;
   this.AV30Pessoa_Endereco = "" ;
   this.AV29Pessoa_CEP = "" ;
   this.AV25Contratada_UF = "" ;
   this.AV33Pessoa_Telefone = "" ;
   this.AV31Pessoa_Fax = "" ;
   this.AV35AuditingObject = {} ;
   this.AV37IsConfiguracoesValidas = "" ;
   this.AV39Pgmname = "" ;
   this.Gx_BScreen = 0 ;
   this.A53Contratada_AreaTrabalhoDes = "" ;
   this.A1592Contratada_AreaTrbClcPFnl = "" ;
   this.A1595Contratada_AreaTrbSrvPdr = 0 ;
   this.A41Contratada_PessoaNom = "" ;
   this.A42Contratada_PessoaCNPJ = "" ;
   this.A518Pessoa_IE = "" ;
   this.A519Pessoa_Endereco = "" ;
   this.A521Pessoa_CEP = "" ;
   this.A522Pessoa_Telefone = "" ;
   this.A523Pessoa_Fax = "" ;
   this.A438Contratada_Sigla = "" ;
   this.A516Contratada_TipoFabrica = "" ;
   this.A342Contratada_BancoNome = "" ;
   this.A343Contratada_BancoNro = "" ;
   this.A344Contratada_AgenciaNome = "" ;
   this.A345Contratada_AgenciaNro = "" ;
   this.A51Contratada_ContaCorrente = "" ;
   this.A350Contratada_UF = "" ;
   this.A524Contratada_OS = 0 ;
   this.A1451Contratada_SS = 0 ;
   this.A530Contratada_Lote = 0 ;
   this.A1127Contratada_LogoArquivo = "" ;
   this.A1481Contratada_UsaOSistema = false ;
   this.A1867Contratada_OSPreferencial = false ;
   this.A1953Contratada_CntPadrao = 0 ;
   this.A43Contratada_Ativo = false ;
   this.A1664Contratada_Logo = "" ;
   this.A1129Contratada_LogoTipoArq = "" ;
   this.A1128Contratada_LogoNomeArq = "" ;
   this.AV28ok = 0 ;
   this.Gx_mode = "" ;
   this.Events = {"e130c2_client": ["AFTER TRN", true] ,"e140c2_client": ["VCONTRATADA_CNPJ.ISVALID", true] ,"e150c13_client": ["ENTER", true] ,"e160c13_client": ["CANCEL", true] ,"e110c13_client": ["CONTRATADA_TIPOFABRICA.CLICK", false]};
   this.EvtParms["ENTER"] = [[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[]];
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["AFTER TRN"] = [[{av:'AV35AuditingObject',fld:'vAUDITINGOBJECT',pic:'',nv:null},{av:'AV39Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'A516Contratada_TipoFabrica',fld:'CONTRATADA_TIPOFABRICA',pic:'',nv:''},{av:'AV8WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24Contratada_RazaoSocial',fld:'vCONTRATADA_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],[{av:'AV36Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("TBJAVA","Caption")',ctrl:'TBJAVA',prop:'Caption'}]];
   this.EvtParms["VCONTRATADA_CNPJ.ISVALID"] = [[{av:'AV21Contratada_CNPJ',fld:'vCONTRATADA_CNPJ',pic:'',nv:''},{av:'AV24Contratada_RazaoSocial',fld:'vCONTRATADA_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV19Contratada_BancoNome',fld:'vCONTRATADA_BANCONOME',pic:'@!',nv:''},{av:'AV20Contratada_BancoNro',fld:'vCONTRATADA_BANCONRO',pic:'',nv:''},{av:'AV17Contratada_AgenciaNome',fld:'vCONTRATADA_AGENCIANOME',pic:'@!',nv:''},{av:'AV18Contratada_AgenciaNro',fld:'vCONTRATADA_AGENCIANRO',pic:'',nv:''},{av:'AV22Contratada_ContaCorrente',fld:'vCONTRATADA_CONTACORRENTE',pic:'',nv:''},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV32Pessoa_IE',fld:'vPESSOA_IE',pic:'',nv:''}],[{av:'AV23Contratada_PessoaCod',fld:'vCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["CONTRATADA_TIPOFABRICA.CLICK"] = [[{av:'A516Contratada_TipoFabrica',fld:'CONTRATADA_TIPOFABRICA',pic:'',nv:''}],[{av:'AV21Contratada_CNPJ',fld:'vCONTRATADA_CNPJ',pic:'',nv:''}]];
   this.EnterCtrl = ["BTN_TRN_ENTER"];
   this.setVCMap("AV7Contratada_Codigo", "vCONTRATADA_CODIGO", 0, "int");
   this.setVCMap("AV14Insert_Contratada_AreaTrabalhoCod", "vINSERT_CONTRATADA_AREATRABALHOCOD", 0, "int");
   this.setVCMap("Gx_BScreen", "vGXBSCREEN", 0, "int");
   this.setVCMap("A52Contratada_AreaTrabalhoCod", "CONTRATADA_AREATRABALHOCOD", 0, "int");
   this.setVCMap("AV11Insert_Contratada_PessoaCod", "vINSERT_CONTRATADA_PESSOACOD", 0, "int");
   this.setVCMap("AV26Insert_Contratada_MunicipioCod", "vINSERT_CONTRATADA_MUNICIPIOCOD", 0, "int");
   this.setVCMap("A40000Contratada_Logo_GXI", "CONTRATADA_LOGO_GXI", 0, "svchar");
   this.setVCMap("AV37IsConfiguracoesValidas", "vISCONFIGURACOESVALIDAS", 0, "svchar");
   this.setVCMap("A42Contratada_PessoaCNPJ", "CONTRATADA_PESSOACNPJ", 0, "svchar");
   this.setVCMap("A41Contratada_PessoaNom", "CONTRATADA_PESSOANOM", 0, "char");
   this.setVCMap("A342Contratada_BancoNome", "CONTRATADA_BANCONOME", 0, "char");
   this.setVCMap("A343Contratada_BancoNro", "CONTRATADA_BANCONRO", 0, "char");
   this.setVCMap("A344Contratada_AgenciaNome", "CONTRATADA_AGENCIANOME", 0, "char");
   this.setVCMap("A345Contratada_AgenciaNro", "CONTRATADA_AGENCIANRO", 0, "char");
   this.setVCMap("A51Contratada_ContaCorrente", "CONTRATADA_CONTACORRENTE", 0, "char");
   this.setVCMap("A350Contratada_UF", "CONTRATADA_UF", 0, "char");
   this.setVCMap("A518Pessoa_IE", "PESSOA_IE", 0, "char");
   this.setVCMap("A519Pessoa_Endereco", "PESSOA_ENDERECO", 0, "svchar");
   this.setVCMap("A521Pessoa_CEP", "PESSOA_CEP", 0, "char");
   this.setVCMap("A522Pessoa_Telefone", "PESSOA_TELEFONE", 0, "char");
   this.setVCMap("A523Pessoa_Fax", "PESSOA_FAX", 0, "char");
   this.setVCMap("AV28ok", "vOK", 0, "int");
   this.setVCMap("AV35AuditingObject", "vAUDITINGOBJECT", 0, "WWPBaseObjects\AuditingObject");
   this.setVCMap("A1127Contratada_LogoArquivo", "CONTRATADA_LOGOARQUIVO", 0, "bitstr");
   this.setVCMap("A1129Contratada_LogoTipoArq", "CONTRATADA_LOGOTIPOARQ", 0, "char");
   this.setVCMap("A1128Contratada_LogoNomeArq", "CONTRATADA_LOGONOMEARQ", 0, "char");
   this.setVCMap("A53Contratada_AreaTrabalhoDes", "CONTRATADA_AREATRABALHODES", 0, "svchar");
   this.setVCMap("A1592Contratada_AreaTrbClcPFnl", "CONTRATADA_AREATRBCLCPFNL", 0, "char");
   this.setVCMap("A1595Contratada_AreaTrbSrvPdr", "CONTRATADA_AREATRBSRVPDR", 0, "int");
   this.setVCMap("AV39Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("Gx_mode", "vMODE", 0, "char");
   this.setVCMap("AV8WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("AV9TrnContext", "vTRNCONTEXT", 0, "WWPBaseObjects\WWPTransactionContext");
   this.InitStandaloneVars( );
});
gx.createParentObj(contratada);
