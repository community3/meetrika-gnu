/*
               File: ContagemResultadoArtefato_BC
        Description: Artefatos das OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:53:55.58
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadoartefato_bc : GXHttpHandler, IGxSilentTrn
   {
      public contagemresultadoartefato_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadoartefato_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow4G196( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey4G196( ) ;
         standaloneModal( ) ;
         AddRow4G196( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1769ContagemResultadoArtefato_Codigo = A1769ContagemResultadoArtefato_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_4G0( )
      {
         BeforeValidate4G196( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4G196( ) ;
            }
            else
            {
               CheckExtendedTable4G196( ) ;
               if ( AnyError == 0 )
               {
                  ZM4G196( 2) ;
                  ZM4G196( 3) ;
                  ZM4G196( 4) ;
                  ZM4G196( 5) ;
               }
               CloseExtendedTableCursors4G196( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM4G196( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            Z1772ContagemResultadoArtefato_OSCod = A1772ContagemResultadoArtefato_OSCod;
            Z1771ContagemResultadoArtefato_ArtefatoCod = A1771ContagemResultadoArtefato_ArtefatoCod;
            Z1770ContagemResultadoArtefato_EvdCod = A1770ContagemResultadoArtefato_EvdCod;
            Z1773ContagemResultadoArtefato_AnxCod = A1773ContagemResultadoArtefato_AnxCod;
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -1 )
         {
            Z1769ContagemResultadoArtefato_Codigo = A1769ContagemResultadoArtefato_Codigo;
            Z1772ContagemResultadoArtefato_OSCod = A1772ContagemResultadoArtefato_OSCod;
            Z1771ContagemResultadoArtefato_ArtefatoCod = A1771ContagemResultadoArtefato_ArtefatoCod;
            Z1770ContagemResultadoArtefato_EvdCod = A1770ContagemResultadoArtefato_EvdCod;
            Z1773ContagemResultadoArtefato_AnxCod = A1773ContagemResultadoArtefato_AnxCod;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load4G196( )
      {
         /* Using cursor BC004G8 */
         pr_default.execute(6, new Object[] {A1769ContagemResultadoArtefato_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound196 = 1;
            A1772ContagemResultadoArtefato_OSCod = BC004G8_A1772ContagemResultadoArtefato_OSCod[0];
            A1771ContagemResultadoArtefato_ArtefatoCod = BC004G8_A1771ContagemResultadoArtefato_ArtefatoCod[0];
            A1770ContagemResultadoArtefato_EvdCod = BC004G8_A1770ContagemResultadoArtefato_EvdCod[0];
            n1770ContagemResultadoArtefato_EvdCod = BC004G8_n1770ContagemResultadoArtefato_EvdCod[0];
            A1773ContagemResultadoArtefato_AnxCod = BC004G8_A1773ContagemResultadoArtefato_AnxCod[0];
            n1773ContagemResultadoArtefato_AnxCod = BC004G8_n1773ContagemResultadoArtefato_AnxCod[0];
            ZM4G196( -1) ;
         }
         pr_default.close(6);
         OnLoadActions4G196( ) ;
      }

      protected void OnLoadActions4G196( )
      {
      }

      protected void CheckExtendedTable4G196( )
      {
         standaloneModal( ) ;
         /* Using cursor BC004G4 */
         pr_default.execute(2, new Object[] {A1772ContagemResultadoArtefato_OSCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Artefatos_Contagem Resultado'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOARTEFATO_OSCOD");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor BC004G5 */
         pr_default.execute(3, new Object[] {A1771ContagemResultadoArtefato_ArtefatoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Artefatos_Artefatos'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOARTEFATO_ARTEFATOCOD");
            AnyError = 1;
         }
         pr_default.close(3);
         /* Using cursor BC004G6 */
         pr_default.execute(4, new Object[] {n1770ContagemResultadoArtefato_EvdCod, A1770ContagemResultadoArtefato_EvdCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A1770ContagemResultadoArtefato_EvdCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado Artefatos_contagem Resutlado Evidencias'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOARTEFATO_EVDCOD");
               AnyError = 1;
            }
         }
         pr_default.close(4);
         /* Using cursor BC004G7 */
         pr_default.execute(5, new Object[] {n1773ContagemResultadoArtefato_AnxCod, A1773ContagemResultadoArtefato_AnxCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A1773ContagemResultadoArtefato_AnxCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado Artefatos_Anexos'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOARTEFATO_ANXCOD");
               AnyError = 1;
            }
         }
         pr_default.close(5);
      }

      protected void CloseExtendedTableCursors4G196( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
         pr_default.close(5);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4G196( )
      {
         /* Using cursor BC004G9 */
         pr_default.execute(7, new Object[] {A1769ContagemResultadoArtefato_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound196 = 1;
         }
         else
         {
            RcdFound196 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC004G3 */
         pr_default.execute(1, new Object[] {A1769ContagemResultadoArtefato_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4G196( 1) ;
            RcdFound196 = 1;
            A1769ContagemResultadoArtefato_Codigo = BC004G3_A1769ContagemResultadoArtefato_Codigo[0];
            A1772ContagemResultadoArtefato_OSCod = BC004G3_A1772ContagemResultadoArtefato_OSCod[0];
            A1771ContagemResultadoArtefato_ArtefatoCod = BC004G3_A1771ContagemResultadoArtefato_ArtefatoCod[0];
            A1770ContagemResultadoArtefato_EvdCod = BC004G3_A1770ContagemResultadoArtefato_EvdCod[0];
            n1770ContagemResultadoArtefato_EvdCod = BC004G3_n1770ContagemResultadoArtefato_EvdCod[0];
            A1773ContagemResultadoArtefato_AnxCod = BC004G3_A1773ContagemResultadoArtefato_AnxCod[0];
            n1773ContagemResultadoArtefato_AnxCod = BC004G3_n1773ContagemResultadoArtefato_AnxCod[0];
            Z1769ContagemResultadoArtefato_Codigo = A1769ContagemResultadoArtefato_Codigo;
            sMode196 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load4G196( ) ;
            if ( AnyError == 1 )
            {
               RcdFound196 = 0;
               InitializeNonKey4G196( ) ;
            }
            Gx_mode = sMode196;
         }
         else
         {
            RcdFound196 = 0;
            InitializeNonKey4G196( ) ;
            sMode196 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode196;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4G196( ) ;
         if ( RcdFound196 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_4G0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency4G196( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC004G2 */
            pr_default.execute(0, new Object[] {A1769ContagemResultadoArtefato_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoArtefato"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1772ContagemResultadoArtefato_OSCod != BC004G2_A1772ContagemResultadoArtefato_OSCod[0] ) || ( Z1771ContagemResultadoArtefato_ArtefatoCod != BC004G2_A1771ContagemResultadoArtefato_ArtefatoCod[0] ) || ( Z1770ContagemResultadoArtefato_EvdCod != BC004G2_A1770ContagemResultadoArtefato_EvdCod[0] ) || ( Z1773ContagemResultadoArtefato_AnxCod != BC004G2_A1773ContagemResultadoArtefato_AnxCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoArtefato"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4G196( )
      {
         BeforeValidate4G196( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4G196( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4G196( 0) ;
            CheckOptimisticConcurrency4G196( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4G196( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4G196( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004G10 */
                     pr_default.execute(8, new Object[] {A1772ContagemResultadoArtefato_OSCod, A1771ContagemResultadoArtefato_ArtefatoCod, n1770ContagemResultadoArtefato_EvdCod, A1770ContagemResultadoArtefato_EvdCod, n1773ContagemResultadoArtefato_AnxCod, A1773ContagemResultadoArtefato_AnxCod});
                     A1769ContagemResultadoArtefato_Codigo = BC004G10_A1769ContagemResultadoArtefato_Codigo[0];
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoArtefato") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4G196( ) ;
            }
            EndLevel4G196( ) ;
         }
         CloseExtendedTableCursors4G196( ) ;
      }

      protected void Update4G196( )
      {
         BeforeValidate4G196( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4G196( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4G196( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4G196( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4G196( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004G11 */
                     pr_default.execute(9, new Object[] {A1772ContagemResultadoArtefato_OSCod, A1771ContagemResultadoArtefato_ArtefatoCod, n1770ContagemResultadoArtefato_EvdCod, A1770ContagemResultadoArtefato_EvdCod, n1773ContagemResultadoArtefato_AnxCod, A1773ContagemResultadoArtefato_AnxCod, A1769ContagemResultadoArtefato_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoArtefato") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoArtefato"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4G196( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4G196( ) ;
         }
         CloseExtendedTableCursors4G196( ) ;
      }

      protected void DeferredUpdate4G196( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate4G196( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4G196( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4G196( ) ;
            AfterConfirm4G196( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4G196( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC004G12 */
                  pr_default.execute(10, new Object[] {A1769ContagemResultadoArtefato_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoArtefato") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode196 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel4G196( ) ;
         Gx_mode = sMode196;
      }

      protected void OnDeleteControls4G196( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel4G196( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4G196( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart4G196( )
      {
         /* Using cursor BC004G13 */
         pr_default.execute(11, new Object[] {A1769ContagemResultadoArtefato_Codigo});
         RcdFound196 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound196 = 1;
            A1769ContagemResultadoArtefato_Codigo = BC004G13_A1769ContagemResultadoArtefato_Codigo[0];
            A1772ContagemResultadoArtefato_OSCod = BC004G13_A1772ContagemResultadoArtefato_OSCod[0];
            A1771ContagemResultadoArtefato_ArtefatoCod = BC004G13_A1771ContagemResultadoArtefato_ArtefatoCod[0];
            A1770ContagemResultadoArtefato_EvdCod = BC004G13_A1770ContagemResultadoArtefato_EvdCod[0];
            n1770ContagemResultadoArtefato_EvdCod = BC004G13_n1770ContagemResultadoArtefato_EvdCod[0];
            A1773ContagemResultadoArtefato_AnxCod = BC004G13_A1773ContagemResultadoArtefato_AnxCod[0];
            n1773ContagemResultadoArtefato_AnxCod = BC004G13_n1773ContagemResultadoArtefato_AnxCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext4G196( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound196 = 0;
         ScanKeyLoad4G196( ) ;
      }

      protected void ScanKeyLoad4G196( )
      {
         sMode196 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound196 = 1;
            A1769ContagemResultadoArtefato_Codigo = BC004G13_A1769ContagemResultadoArtefato_Codigo[0];
            A1772ContagemResultadoArtefato_OSCod = BC004G13_A1772ContagemResultadoArtefato_OSCod[0];
            A1771ContagemResultadoArtefato_ArtefatoCod = BC004G13_A1771ContagemResultadoArtefato_ArtefatoCod[0];
            A1770ContagemResultadoArtefato_EvdCod = BC004G13_A1770ContagemResultadoArtefato_EvdCod[0];
            n1770ContagemResultadoArtefato_EvdCod = BC004G13_n1770ContagemResultadoArtefato_EvdCod[0];
            A1773ContagemResultadoArtefato_AnxCod = BC004G13_A1773ContagemResultadoArtefato_AnxCod[0];
            n1773ContagemResultadoArtefato_AnxCod = BC004G13_n1773ContagemResultadoArtefato_AnxCod[0];
         }
         Gx_mode = sMode196;
      }

      protected void ScanKeyEnd4G196( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm4G196( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4G196( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4G196( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4G196( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4G196( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4G196( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4G196( )
      {
      }

      protected void AddRow4G196( )
      {
         VarsToRow196( bcContagemResultadoArtefato) ;
      }

      protected void ReadRow4G196( )
      {
         RowToVars196( bcContagemResultadoArtefato, 1) ;
      }

      protected void InitializeNonKey4G196( )
      {
         A1772ContagemResultadoArtefato_OSCod = 0;
         A1771ContagemResultadoArtefato_ArtefatoCod = 0;
         A1770ContagemResultadoArtefato_EvdCod = 0;
         n1770ContagemResultadoArtefato_EvdCod = false;
         A1773ContagemResultadoArtefato_AnxCod = 0;
         n1773ContagemResultadoArtefato_AnxCod = false;
         Z1772ContagemResultadoArtefato_OSCod = 0;
         Z1771ContagemResultadoArtefato_ArtefatoCod = 0;
         Z1770ContagemResultadoArtefato_EvdCod = 0;
         Z1773ContagemResultadoArtefato_AnxCod = 0;
      }

      protected void InitAll4G196( )
      {
         A1769ContagemResultadoArtefato_Codigo = 0;
         InitializeNonKey4G196( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow196( SdtContagemResultadoArtefato obj196 )
      {
         obj196.gxTpr_Mode = Gx_mode;
         obj196.gxTpr_Contagemresultadoartefato_oscod = A1772ContagemResultadoArtefato_OSCod;
         obj196.gxTpr_Contagemresultadoartefato_artefatocod = A1771ContagemResultadoArtefato_ArtefatoCod;
         obj196.gxTpr_Contagemresultadoartefato_evdcod = A1770ContagemResultadoArtefato_EvdCod;
         obj196.gxTpr_Contagemresultadoartefato_anxcod = A1773ContagemResultadoArtefato_AnxCod;
         obj196.gxTpr_Contagemresultadoartefato_codigo = A1769ContagemResultadoArtefato_Codigo;
         obj196.gxTpr_Contagemresultadoartefato_codigo_Z = Z1769ContagemResultadoArtefato_Codigo;
         obj196.gxTpr_Contagemresultadoartefato_oscod_Z = Z1772ContagemResultadoArtefato_OSCod;
         obj196.gxTpr_Contagemresultadoartefato_artefatocod_Z = Z1771ContagemResultadoArtefato_ArtefatoCod;
         obj196.gxTpr_Contagemresultadoartefato_evdcod_Z = Z1770ContagemResultadoArtefato_EvdCod;
         obj196.gxTpr_Contagemresultadoartefato_anxcod_Z = Z1773ContagemResultadoArtefato_AnxCod;
         obj196.gxTpr_Contagemresultadoartefato_evdcod_N = (short)(Convert.ToInt16(n1770ContagemResultadoArtefato_EvdCod));
         obj196.gxTpr_Contagemresultadoartefato_anxcod_N = (short)(Convert.ToInt16(n1773ContagemResultadoArtefato_AnxCod));
         obj196.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow196( SdtContagemResultadoArtefato obj196 )
      {
         obj196.gxTpr_Contagemresultadoartefato_codigo = A1769ContagemResultadoArtefato_Codigo;
         return  ;
      }

      public void RowToVars196( SdtContagemResultadoArtefato obj196 ,
                                int forceLoad )
      {
         Gx_mode = obj196.gxTpr_Mode;
         A1772ContagemResultadoArtefato_OSCod = obj196.gxTpr_Contagemresultadoartefato_oscod;
         A1771ContagemResultadoArtefato_ArtefatoCod = obj196.gxTpr_Contagemresultadoartefato_artefatocod;
         A1770ContagemResultadoArtefato_EvdCod = obj196.gxTpr_Contagemresultadoartefato_evdcod;
         n1770ContagemResultadoArtefato_EvdCod = false;
         A1773ContagemResultadoArtefato_AnxCod = obj196.gxTpr_Contagemresultadoartefato_anxcod;
         n1773ContagemResultadoArtefato_AnxCod = false;
         A1769ContagemResultadoArtefato_Codigo = obj196.gxTpr_Contagemresultadoartefato_codigo;
         Z1769ContagemResultadoArtefato_Codigo = obj196.gxTpr_Contagemresultadoartefato_codigo_Z;
         Z1772ContagemResultadoArtefato_OSCod = obj196.gxTpr_Contagemresultadoartefato_oscod_Z;
         Z1771ContagemResultadoArtefato_ArtefatoCod = obj196.gxTpr_Contagemresultadoartefato_artefatocod_Z;
         Z1770ContagemResultadoArtefato_EvdCod = obj196.gxTpr_Contagemresultadoartefato_evdcod_Z;
         Z1773ContagemResultadoArtefato_AnxCod = obj196.gxTpr_Contagemresultadoartefato_anxcod_Z;
         n1770ContagemResultadoArtefato_EvdCod = (bool)(Convert.ToBoolean(obj196.gxTpr_Contagemresultadoartefato_evdcod_N));
         n1773ContagemResultadoArtefato_AnxCod = (bool)(Convert.ToBoolean(obj196.gxTpr_Contagemresultadoartefato_anxcod_N));
         Gx_mode = obj196.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1769ContagemResultadoArtefato_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey4G196( ) ;
         ScanKeyStart4G196( ) ;
         if ( RcdFound196 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1769ContagemResultadoArtefato_Codigo = A1769ContagemResultadoArtefato_Codigo;
         }
         ZM4G196( -1) ;
         OnLoadActions4G196( ) ;
         AddRow4G196( ) ;
         ScanKeyEnd4G196( ) ;
         if ( RcdFound196 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars196( bcContagemResultadoArtefato, 0) ;
         ScanKeyStart4G196( ) ;
         if ( RcdFound196 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1769ContagemResultadoArtefato_Codigo = A1769ContagemResultadoArtefato_Codigo;
         }
         ZM4G196( -1) ;
         OnLoadActions4G196( ) ;
         AddRow4G196( ) ;
         ScanKeyEnd4G196( ) ;
         if ( RcdFound196 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars196( bcContagemResultadoArtefato, 0) ;
         nKeyPressed = 1;
         GetKey4G196( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert4G196( ) ;
         }
         else
         {
            if ( RcdFound196 == 1 )
            {
               if ( A1769ContagemResultadoArtefato_Codigo != Z1769ContagemResultadoArtefato_Codigo )
               {
                  A1769ContagemResultadoArtefato_Codigo = Z1769ContagemResultadoArtefato_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update4G196( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1769ContagemResultadoArtefato_Codigo != Z1769ContagemResultadoArtefato_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4G196( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4G196( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow196( bcContagemResultadoArtefato) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars196( bcContagemResultadoArtefato, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey4G196( ) ;
         if ( RcdFound196 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1769ContagemResultadoArtefato_Codigo != Z1769ContagemResultadoArtefato_Codigo )
            {
               A1769ContagemResultadoArtefato_Codigo = Z1769ContagemResultadoArtefato_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1769ContagemResultadoArtefato_Codigo != Z1769ContagemResultadoArtefato_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         context.RollbackDataStores( "ContagemResultadoArtefato_BC");
         VarsToRow196( bcContagemResultadoArtefato) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContagemResultadoArtefato.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContagemResultadoArtefato.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContagemResultadoArtefato )
         {
            bcContagemResultadoArtefato = (SdtContagemResultadoArtefato)(sdt);
            if ( StringUtil.StrCmp(bcContagemResultadoArtefato.gxTpr_Mode, "") == 0 )
            {
               bcContagemResultadoArtefato.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow196( bcContagemResultadoArtefato) ;
            }
            else
            {
               RowToVars196( bcContagemResultadoArtefato, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContagemResultadoArtefato.gxTpr_Mode, "") == 0 )
            {
               bcContagemResultadoArtefato.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars196( bcContagemResultadoArtefato, 1) ;
         return  ;
      }

      public SdtContagemResultadoArtefato ContagemResultadoArtefato_BC
      {
         get {
            return bcContagemResultadoArtefato ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         BC004G8_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         BC004G8_A1772ContagemResultadoArtefato_OSCod = new int[1] ;
         BC004G8_A1771ContagemResultadoArtefato_ArtefatoCod = new int[1] ;
         BC004G8_A1770ContagemResultadoArtefato_EvdCod = new int[1] ;
         BC004G8_n1770ContagemResultadoArtefato_EvdCod = new bool[] {false} ;
         BC004G8_A1773ContagemResultadoArtefato_AnxCod = new int[1] ;
         BC004G8_n1773ContagemResultadoArtefato_AnxCod = new bool[] {false} ;
         BC004G4_A1772ContagemResultadoArtefato_OSCod = new int[1] ;
         BC004G5_A1771ContagemResultadoArtefato_ArtefatoCod = new int[1] ;
         BC004G6_A1770ContagemResultadoArtefato_EvdCod = new int[1] ;
         BC004G6_n1770ContagemResultadoArtefato_EvdCod = new bool[] {false} ;
         BC004G7_A1773ContagemResultadoArtefato_AnxCod = new int[1] ;
         BC004G7_n1773ContagemResultadoArtefato_AnxCod = new bool[] {false} ;
         BC004G9_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         BC004G3_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         BC004G3_A1772ContagemResultadoArtefato_OSCod = new int[1] ;
         BC004G3_A1771ContagemResultadoArtefato_ArtefatoCod = new int[1] ;
         BC004G3_A1770ContagemResultadoArtefato_EvdCod = new int[1] ;
         BC004G3_n1770ContagemResultadoArtefato_EvdCod = new bool[] {false} ;
         BC004G3_A1773ContagemResultadoArtefato_AnxCod = new int[1] ;
         BC004G3_n1773ContagemResultadoArtefato_AnxCod = new bool[] {false} ;
         sMode196 = "";
         BC004G2_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         BC004G2_A1772ContagemResultadoArtefato_OSCod = new int[1] ;
         BC004G2_A1771ContagemResultadoArtefato_ArtefatoCod = new int[1] ;
         BC004G2_A1770ContagemResultadoArtefato_EvdCod = new int[1] ;
         BC004G2_n1770ContagemResultadoArtefato_EvdCod = new bool[] {false} ;
         BC004G2_A1773ContagemResultadoArtefato_AnxCod = new int[1] ;
         BC004G2_n1773ContagemResultadoArtefato_AnxCod = new bool[] {false} ;
         BC004G10_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         BC004G13_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         BC004G13_A1772ContagemResultadoArtefato_OSCod = new int[1] ;
         BC004G13_A1771ContagemResultadoArtefato_ArtefatoCod = new int[1] ;
         BC004G13_A1770ContagemResultadoArtefato_EvdCod = new int[1] ;
         BC004G13_n1770ContagemResultadoArtefato_EvdCod = new bool[] {false} ;
         BC004G13_A1773ContagemResultadoArtefato_AnxCod = new int[1] ;
         BC004G13_n1773ContagemResultadoArtefato_AnxCod = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadoartefato_bc__default(),
            new Object[][] {
                new Object[] {
               BC004G2_A1769ContagemResultadoArtefato_Codigo, BC004G2_A1772ContagemResultadoArtefato_OSCod, BC004G2_A1771ContagemResultadoArtefato_ArtefatoCod, BC004G2_A1770ContagemResultadoArtefato_EvdCod, BC004G2_n1770ContagemResultadoArtefato_EvdCod, BC004G2_A1773ContagemResultadoArtefato_AnxCod, BC004G2_n1773ContagemResultadoArtefato_AnxCod
               }
               , new Object[] {
               BC004G3_A1769ContagemResultadoArtefato_Codigo, BC004G3_A1772ContagemResultadoArtefato_OSCod, BC004G3_A1771ContagemResultadoArtefato_ArtefatoCod, BC004G3_A1770ContagemResultadoArtefato_EvdCod, BC004G3_n1770ContagemResultadoArtefato_EvdCod, BC004G3_A1773ContagemResultadoArtefato_AnxCod, BC004G3_n1773ContagemResultadoArtefato_AnxCod
               }
               , new Object[] {
               BC004G4_A1772ContagemResultadoArtefato_OSCod
               }
               , new Object[] {
               BC004G5_A1771ContagemResultadoArtefato_ArtefatoCod
               }
               , new Object[] {
               BC004G6_A1770ContagemResultadoArtefato_EvdCod
               }
               , new Object[] {
               BC004G7_A1773ContagemResultadoArtefato_AnxCod
               }
               , new Object[] {
               BC004G8_A1769ContagemResultadoArtefato_Codigo, BC004G8_A1772ContagemResultadoArtefato_OSCod, BC004G8_A1771ContagemResultadoArtefato_ArtefatoCod, BC004G8_A1770ContagemResultadoArtefato_EvdCod, BC004G8_n1770ContagemResultadoArtefato_EvdCod, BC004G8_A1773ContagemResultadoArtefato_AnxCod, BC004G8_n1773ContagemResultadoArtefato_AnxCod
               }
               , new Object[] {
               BC004G9_A1769ContagemResultadoArtefato_Codigo
               }
               , new Object[] {
               BC004G10_A1769ContagemResultadoArtefato_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC004G13_A1769ContagemResultadoArtefato_Codigo, BC004G13_A1772ContagemResultadoArtefato_OSCod, BC004G13_A1771ContagemResultadoArtefato_ArtefatoCod, BC004G13_A1770ContagemResultadoArtefato_EvdCod, BC004G13_n1770ContagemResultadoArtefato_EvdCod, BC004G13_A1773ContagemResultadoArtefato_AnxCod, BC004G13_n1773ContagemResultadoArtefato_AnxCod
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound196 ;
      private int trnEnded ;
      private int Z1769ContagemResultadoArtefato_Codigo ;
      private int A1769ContagemResultadoArtefato_Codigo ;
      private int Z1772ContagemResultadoArtefato_OSCod ;
      private int A1772ContagemResultadoArtefato_OSCod ;
      private int Z1771ContagemResultadoArtefato_ArtefatoCod ;
      private int A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int Z1770ContagemResultadoArtefato_EvdCod ;
      private int A1770ContagemResultadoArtefato_EvdCod ;
      private int Z1773ContagemResultadoArtefato_AnxCod ;
      private int A1773ContagemResultadoArtefato_AnxCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode196 ;
      private bool n1770ContagemResultadoArtefato_EvdCod ;
      private bool n1773ContagemResultadoArtefato_AnxCod ;
      private SdtContagemResultadoArtefato bcContagemResultadoArtefato ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC004G8_A1769ContagemResultadoArtefato_Codigo ;
      private int[] BC004G8_A1772ContagemResultadoArtefato_OSCod ;
      private int[] BC004G8_A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int[] BC004G8_A1770ContagemResultadoArtefato_EvdCod ;
      private bool[] BC004G8_n1770ContagemResultadoArtefato_EvdCod ;
      private int[] BC004G8_A1773ContagemResultadoArtefato_AnxCod ;
      private bool[] BC004G8_n1773ContagemResultadoArtefato_AnxCod ;
      private int[] BC004G4_A1772ContagemResultadoArtefato_OSCod ;
      private int[] BC004G5_A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int[] BC004G6_A1770ContagemResultadoArtefato_EvdCod ;
      private bool[] BC004G6_n1770ContagemResultadoArtefato_EvdCod ;
      private int[] BC004G7_A1773ContagemResultadoArtefato_AnxCod ;
      private bool[] BC004G7_n1773ContagemResultadoArtefato_AnxCod ;
      private int[] BC004G9_A1769ContagemResultadoArtefato_Codigo ;
      private int[] BC004G3_A1769ContagemResultadoArtefato_Codigo ;
      private int[] BC004G3_A1772ContagemResultadoArtefato_OSCod ;
      private int[] BC004G3_A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int[] BC004G3_A1770ContagemResultadoArtefato_EvdCod ;
      private bool[] BC004G3_n1770ContagemResultadoArtefato_EvdCod ;
      private int[] BC004G3_A1773ContagemResultadoArtefato_AnxCod ;
      private bool[] BC004G3_n1773ContagemResultadoArtefato_AnxCod ;
      private int[] BC004G2_A1769ContagemResultadoArtefato_Codigo ;
      private int[] BC004G2_A1772ContagemResultadoArtefato_OSCod ;
      private int[] BC004G2_A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int[] BC004G2_A1770ContagemResultadoArtefato_EvdCod ;
      private bool[] BC004G2_n1770ContagemResultadoArtefato_EvdCod ;
      private int[] BC004G2_A1773ContagemResultadoArtefato_AnxCod ;
      private bool[] BC004G2_n1773ContagemResultadoArtefato_AnxCod ;
      private int[] BC004G10_A1769ContagemResultadoArtefato_Codigo ;
      private int[] BC004G13_A1769ContagemResultadoArtefato_Codigo ;
      private int[] BC004G13_A1772ContagemResultadoArtefato_OSCod ;
      private int[] BC004G13_A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int[] BC004G13_A1770ContagemResultadoArtefato_EvdCod ;
      private bool[] BC004G13_n1770ContagemResultadoArtefato_EvdCod ;
      private int[] BC004G13_A1773ContagemResultadoArtefato_AnxCod ;
      private bool[] BC004G13_n1773ContagemResultadoArtefato_AnxCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class contagemresultadoartefato_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC004G8 ;
          prmBC004G8 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004G4 ;
          prmBC004G4 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004G5 ;
          prmBC004G5 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004G6 ;
          prmBC004G6 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_EvdCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004G7 ;
          prmBC004G7 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_AnxCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004G9 ;
          prmBC004G9 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004G3 ;
          prmBC004G3 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004G2 ;
          prmBC004G2 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004G10 ;
          prmBC004G10 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoArtefato_ArtefatoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoArtefato_EvdCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoArtefato_AnxCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004G11 ;
          prmBC004G11 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoArtefato_ArtefatoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoArtefato_EvdCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoArtefato_AnxCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoArtefato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004G12 ;
          prmBC004G12 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004G13 ;
          prmBC004G13 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC004G2", "SELECT [ContagemResultadoArtefato_Codigo], [ContagemResultadoArtefato_OSCod] AS ContagemResultadoArtefato_OSCod, [ContagemResultadoArtefato_ArtefatoCod] AS ContagemResultadoArtefato_ArtefatoCod, [ContagemResultadoArtefato_EvdCod] AS ContagemResultadoArtefato_EvdCod, [ContagemResultadoArtefato_AnxCod] AS ContagemResultadoArtefato_AnxCod FROM [ContagemResultadoArtefato] WITH (UPDLOCK) WHERE [ContagemResultadoArtefato_Codigo] = @ContagemResultadoArtefato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004G2,1,0,true,false )
             ,new CursorDef("BC004G3", "SELECT [ContagemResultadoArtefato_Codigo], [ContagemResultadoArtefato_OSCod] AS ContagemResultadoArtefato_OSCod, [ContagemResultadoArtefato_ArtefatoCod] AS ContagemResultadoArtefato_ArtefatoCod, [ContagemResultadoArtefato_EvdCod] AS ContagemResultadoArtefato_EvdCod, [ContagemResultadoArtefato_AnxCod] AS ContagemResultadoArtefato_AnxCod FROM [ContagemResultadoArtefato] WITH (NOLOCK) WHERE [ContagemResultadoArtefato_Codigo] = @ContagemResultadoArtefato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004G3,1,0,true,false )
             ,new CursorDef("BC004G4", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoArtefato_OSCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoArtefato_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004G4,1,0,true,false )
             ,new CursorDef("BC004G5", "SELECT [Artefatos_Codigo] AS ContagemResultadoArtefato_ArtefatoCod FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @ContagemResultadoArtefato_ArtefatoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004G5,1,0,true,false )
             ,new CursorDef("BC004G6", "SELECT [ContagemResultadoEvidencia_Codigo] AS ContagemResultadoArtefato_EvdCod FROM [ContagemResultadoEvidencia] WITH (NOLOCK) WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoArtefato_EvdCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004G6,1,0,true,false )
             ,new CursorDef("BC004G7", "SELECT [Anexo_Codigo] AS ContagemResultadoArtefato_AnxCod FROM [Anexos] WITH (NOLOCK) WHERE [Anexo_Codigo] = @ContagemResultadoArtefato_AnxCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004G7,1,0,true,false )
             ,new CursorDef("BC004G8", "SELECT TM1.[ContagemResultadoArtefato_Codigo], TM1.[ContagemResultadoArtefato_OSCod] AS ContagemResultadoArtefato_OSCod, TM1.[ContagemResultadoArtefato_ArtefatoCod] AS ContagemResultadoArtefato_ArtefatoCod, TM1.[ContagemResultadoArtefato_EvdCod] AS ContagemResultadoArtefato_EvdCod, TM1.[ContagemResultadoArtefato_AnxCod] AS ContagemResultadoArtefato_AnxCod FROM [ContagemResultadoArtefato] TM1 WITH (NOLOCK) WHERE TM1.[ContagemResultadoArtefato_Codigo] = @ContagemResultadoArtefato_Codigo ORDER BY TM1.[ContagemResultadoArtefato_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004G8,100,0,true,false )
             ,new CursorDef("BC004G9", "SELECT [ContagemResultadoArtefato_Codigo] FROM [ContagemResultadoArtefato] WITH (NOLOCK) WHERE [ContagemResultadoArtefato_Codigo] = @ContagemResultadoArtefato_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004G9,1,0,true,false )
             ,new CursorDef("BC004G10", "INSERT INTO [ContagemResultadoArtefato]([ContagemResultadoArtefato_OSCod], [ContagemResultadoArtefato_ArtefatoCod], [ContagemResultadoArtefato_EvdCod], [ContagemResultadoArtefato_AnxCod]) VALUES(@ContagemResultadoArtefato_OSCod, @ContagemResultadoArtefato_ArtefatoCod, @ContagemResultadoArtefato_EvdCod, @ContagemResultadoArtefato_AnxCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC004G10)
             ,new CursorDef("BC004G11", "UPDATE [ContagemResultadoArtefato] SET [ContagemResultadoArtefato_OSCod]=@ContagemResultadoArtefato_OSCod, [ContagemResultadoArtefato_ArtefatoCod]=@ContagemResultadoArtefato_ArtefatoCod, [ContagemResultadoArtefato_EvdCod]=@ContagemResultadoArtefato_EvdCod, [ContagemResultadoArtefato_AnxCod]=@ContagemResultadoArtefato_AnxCod  WHERE [ContagemResultadoArtefato_Codigo] = @ContagemResultadoArtefato_Codigo", GxErrorMask.GX_NOMASK,prmBC004G11)
             ,new CursorDef("BC004G12", "DELETE FROM [ContagemResultadoArtefato]  WHERE [ContagemResultadoArtefato_Codigo] = @ContagemResultadoArtefato_Codigo", GxErrorMask.GX_NOMASK,prmBC004G12)
             ,new CursorDef("BC004G13", "SELECT TM1.[ContagemResultadoArtefato_Codigo], TM1.[ContagemResultadoArtefato_OSCod] AS ContagemResultadoArtefato_OSCod, TM1.[ContagemResultadoArtefato_ArtefatoCod] AS ContagemResultadoArtefato_ArtefatoCod, TM1.[ContagemResultadoArtefato_EvdCod] AS ContagemResultadoArtefato_EvdCod, TM1.[ContagemResultadoArtefato_AnxCod] AS ContagemResultadoArtefato_AnxCod FROM [ContagemResultadoArtefato] TM1 WITH (NOLOCK) WHERE TM1.[ContagemResultadoArtefato_Codigo] = @ContagemResultadoArtefato_Codigo ORDER BY TM1.[ContagemResultadoArtefato_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004G13,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[5]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[5]);
                }
                stmt.SetParameter(5, (int)parms[6]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
