/*
               File: PRC_CriarPastaDeEvd
        Description: Criar pasta de evidÍncias da OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:31.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_criarpastadeevd : GXProcedure
   {
      public prc_criarpastadeevd( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_criarpastadeevd( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           ref short aP1_Count ,
                           out String aP2_Path )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV17Count = aP1_Count;
         this.AV16Path = "" ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_Count=this.AV17Count;
         aP2_Path=this.AV16Path;
      }

      public String executeUdp( ref int aP0_ContagemResultado_Codigo ,
                                ref short aP1_Count )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV17Count = aP1_Count;
         this.AV16Path = "" ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_Count=this.AV17Count;
         aP2_Path=this.AV16Path;
         return AV16Path ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 ref short aP1_Count ,
                                 out String aP2_Path )
      {
         prc_criarpastadeevd objprc_criarpastadeevd;
         objprc_criarpastadeevd = new prc_criarpastadeevd();
         objprc_criarpastadeevd.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_criarpastadeevd.AV17Count = aP1_Count;
         objprc_criarpastadeevd.AV16Path = "" ;
         objprc_criarpastadeevd.context.SetSubmitInitialConfig(context);
         objprc_criarpastadeevd.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_criarpastadeevd);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_Count=this.AV17Count;
         aP2_Path=this.AV16Path;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_criarpastadeevd)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00AU2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A330ParametrosSistema_Codigo = P00AU2_A330ParametrosSistema_Codigo[0];
            A1499ParametrosSistema_FlsEvd = P00AU2_A1499ParametrosSistema_FlsEvd[0];
            n1499ParametrosSistema_FlsEvd = P00AU2_n1499ParametrosSistema_FlsEvd[0];
            AV14Raiz = StringUtil.Trim( A1499ParametrosSistema_FlsEvd);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV14Raiz)) )
         {
            AV17Count = 0;
            this.cleanup();
            if (true) return;
         }
         /* Using cursor P00AU3 */
         pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00AU3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00AU3_n1553ContagemResultado_CntSrvCod[0];
            A52Contratada_AreaTrabalhoCod = P00AU3_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00AU3_n52Contratada_AreaTrabalhoCod[0];
            A490ContagemResultado_ContratadaCod = P00AU3_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00AU3_n490ContagemResultado_ContratadaCod[0];
            A1603ContagemResultado_CntCod = P00AU3_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00AU3_n1603ContagemResultado_CntCod[0];
            A1603ContagemResultado_CntCod = P00AU3_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00AU3_n1603ContagemResultado_CntCod[0];
            A52Contratada_AreaTrabalhoCod = P00AU3_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00AU3_n52Contratada_AreaTrabalhoCod[0];
            AV11Area = A52Contratada_AreaTrabalhoCod;
            AV9Contratada = A490ContagemResultado_ContratadaCod;
            AV10Contrato = A1603ContagemResultado_CntCod;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         AV13Directory.Source = AV14Raiz+StringUtil.Trim( StringUtil.Str( (decimal)(AV11Area), 6, 0))+"\\"+StringUtil.Trim( StringUtil.Str( (decimal)(AV9Contratada), 6, 0))+"\\"+StringUtil.Trim( StringUtil.Str( (decimal)(AV10Contrato), 6, 0))+"\\"+StringUtil.Trim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0));
         if ( AV13Directory.Exists() )
         {
            AV17Count = (short)(AV13Directory.GetFiles("").ItemCount);
            AV16Path = AV13Directory.GetAbsoluteName();
            this.cleanup();
            if (true) return;
         }
         else
         {
            if ( AV17Count == 1 )
            {
               AV17Count = 0;
               this.cleanup();
               if (true) return;
            }
         }
         AV13Directory.Source = AV14Raiz;
         /* Execute user subroutine: 'CRIAR' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV13Directory.Source = AV14Raiz+StringUtil.Trim( StringUtil.Str( (decimal)(AV11Area), 6, 0));
         /* Execute user subroutine: 'CRIAR' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV13Directory.Source = AV14Raiz+StringUtil.Trim( StringUtil.Str( (decimal)(AV11Area), 6, 0))+"\\"+StringUtil.Trim( StringUtil.Str( (decimal)(AV9Contratada), 6, 0));
         /* Execute user subroutine: 'CRIAR' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV13Directory.Source = AV14Raiz+StringUtil.Trim( StringUtil.Str( (decimal)(AV11Area), 6, 0))+"\\"+StringUtil.Trim( StringUtil.Str( (decimal)(AV9Contratada), 6, 0))+"\\"+StringUtil.Trim( StringUtil.Str( (decimal)(AV10Contrato), 6, 0));
         /* Execute user subroutine: 'CRIAR' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV13Directory.Source = AV14Raiz+StringUtil.Trim( StringUtil.Str( (decimal)(AV11Area), 6, 0))+"\\"+StringUtil.Trim( StringUtil.Str( (decimal)(AV9Contratada), 6, 0))+"\\"+StringUtil.Trim( StringUtil.Str( (decimal)(AV10Contrato), 6, 0))+"\\"+StringUtil.Trim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0));
         /* Execute user subroutine: 'CRIAR' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV16Path = AV13Directory.GetAbsoluteName();
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'CRIAR' Routine */
         if ( ! AV13Directory.Exists() )
         {
            AV13Directory.Create();
         }
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00AU2_A330ParametrosSistema_Codigo = new int[1] ;
         P00AU2_A1499ParametrosSistema_FlsEvd = new String[] {""} ;
         P00AU2_n1499ParametrosSistema_FlsEvd = new bool[] {false} ;
         A1499ParametrosSistema_FlsEvd = "";
         AV14Raiz = "";
         P00AU3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00AU3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00AU3_A456ContagemResultado_Codigo = new int[1] ;
         P00AU3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00AU3_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00AU3_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00AU3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00AU3_A1603ContagemResultado_CntCod = new int[1] ;
         P00AU3_n1603ContagemResultado_CntCod = new bool[] {false} ;
         AV13Directory = new GxDirectory(context.GetPhysicalPath());
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_criarpastadeevd__default(),
            new Object[][] {
                new Object[] {
               P00AU2_A330ParametrosSistema_Codigo, P00AU2_A1499ParametrosSistema_FlsEvd, P00AU2_n1499ParametrosSistema_FlsEvd
               }
               , new Object[] {
               P00AU3_A1553ContagemResultado_CntSrvCod, P00AU3_n1553ContagemResultado_CntSrvCod, P00AU3_A456ContagemResultado_Codigo, P00AU3_A52Contratada_AreaTrabalhoCod, P00AU3_n52Contratada_AreaTrabalhoCod, P00AU3_A490ContagemResultado_ContratadaCod, P00AU3_n490ContagemResultado_ContratadaCod, P00AU3_A1603ContagemResultado_CntCod, P00AU3_n1603ContagemResultado_CntCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV17Count ;
      private int A456ContagemResultado_Codigo ;
      private int A330ParametrosSistema_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1603ContagemResultado_CntCod ;
      private int AV11Area ;
      private int AV9Contratada ;
      private int AV10Contrato ;
      private String scmdbuf ;
      private bool n1499ParametrosSistema_FlsEvd ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool returnInSub ;
      private String AV16Path ;
      private String A1499ParametrosSistema_FlsEvd ;
      private String AV14Raiz ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private short aP1_Count ;
      private IDataStoreProvider pr_default ;
      private int[] P00AU2_A330ParametrosSistema_Codigo ;
      private String[] P00AU2_A1499ParametrosSistema_FlsEvd ;
      private bool[] P00AU2_n1499ParametrosSistema_FlsEvd ;
      private int[] P00AU3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00AU3_n1553ContagemResultado_CntSrvCod ;
      private int[] P00AU3_A456ContagemResultado_Codigo ;
      private int[] P00AU3_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00AU3_n52Contratada_AreaTrabalhoCod ;
      private int[] P00AU3_A490ContagemResultado_ContratadaCod ;
      private bool[] P00AU3_n490ContagemResultado_ContratadaCod ;
      private int[] P00AU3_A1603ContagemResultado_CntCod ;
      private bool[] P00AU3_n1603ContagemResultado_CntCod ;
      private String aP2_Path ;
      private GxDirectory AV13Directory ;
   }

   public class prc_criarpastadeevd__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00AU2 ;
          prmP00AU2 = new Object[] {
          } ;
          Object[] prmP00AU3 ;
          prmP00AU3 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00AU2", "SELECT TOP 1 [ParametrosSistema_Codigo], [ParametrosSistema_FlsEvd] FROM [ParametrosSistema] WITH (NOLOCK) WHERE [ParametrosSistema_Codigo] = 1 ORDER BY [ParametrosSistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AU2,1,0,false,true )
             ,new CursorDef("P00AU3", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T3.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[Contrato_Codigo] AS ContagemResultado_CntCod FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AU3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
