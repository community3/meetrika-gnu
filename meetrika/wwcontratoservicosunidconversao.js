/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 21:25:16.23
*/
gx.evt.autoSkip = false;
gx.define('wwcontratoservicosunidconversao', false, function () {
   this.ServerClass =  "wwcontratoservicosunidconversao" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV6WWPContext=gx.fn.getControlValue("vWWPCONTEXT") ;
      this.AV59Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.AV10GridState=gx.fn.getControlValue("vGRIDSTATE") ;
      this.AV27DynamicFiltersIgnoreFirst=gx.fn.getControlValue("vDYNAMICFILTERSIGNOREFIRST") ;
      this.AV26DynamicFiltersRemoving=gx.fn.getControlValue("vDYNAMICFILTERSREMOVING") ;
      this.AV41Contrato_Codigo=gx.fn.getIntegerValue("vCONTRATO_CODIGO",'.') ;
      this.AV42ContratoServicos_UnidadeContratada=gx.fn.getIntegerValue("vCONTRATOSERVICOS_UNIDADECONTRATADA",'.') ;
   };
   this.Valid_Contratoservicosunidconversao_codigo=function()
   {
      try {
         if(  gx.fn.currentGridRowImpl(88) ===0) {
            return true;
         }
         var gxballoon = gx.util.balloon.getNew("CONTRATOSERVICOSUNIDCONVERSAO_CODIGO", gx.fn.currentGridRowImpl(88));
         this.AnyError  = 0;
         this.StandaloneModal( );
         this.StandaloneNotModal( );

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.s122_client=function()
   {
      gx.fn.setCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME1","Visible", false );
      gx.fn.setCtrlProperty("vDYNAMICFILTERSOPERATOR1","Visible", false );
      if ( this.AV15DynamicFiltersSelector1 == "CONTRATOSERVICOSUNIDCONVERSAO_NOME" )
      {
         gx.fn.setCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME1","Visible", true );
         gx.fn.setCtrlProperty("vDYNAMICFILTERSOPERATOR1","Visible", true );
      }
   };
   this.s132_client=function()
   {
      gx.fn.setCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME2","Visible", false );
      gx.fn.setCtrlProperty("vDYNAMICFILTERSOPERATOR2","Visible", false );
      if ( this.AV19DynamicFiltersSelector2 == "CONTRATOSERVICOSUNIDCONVERSAO_NOME" )
      {
         gx.fn.setCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME2","Visible", true );
         gx.fn.setCtrlProperty("vDYNAMICFILTERSOPERATOR2","Visible", true );
      }
   };
   this.s142_client=function()
   {
      gx.fn.setCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME3","Visible", false );
      gx.fn.setCtrlProperty("vDYNAMICFILTERSOPERATOR3","Visible", false );
      if ( this.AV23DynamicFiltersSelector3 == "CONTRATOSERVICOSUNIDCONVERSAO_NOME" )
      {
         gx.fn.setCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME3","Visible", true );
         gx.fn.setCtrlProperty("vDYNAMICFILTERSOPERATOR3","Visible", true );
      }
   };
   this.s202_client=function()
   {
      this.AV18DynamicFiltersEnabled2 =  false  ;
      this.AV19DynamicFiltersSelector2 =  "CONTRATOSERVICOSUNIDCONVERSAO_NOME"  ;
      this.AV20DynamicFiltersOperator2 = gx.num.trunc( 0 ,0) ;
      this.AV21ContratoServicosUnidConversao_Nome2 =  ''  ;
      this.s132_client();
      this.AV22DynamicFiltersEnabled3 =  false  ;
      this.AV23DynamicFiltersSelector3 =  "CONTRATOSERVICOSUNIDCONVERSAO_NOME"  ;
      this.AV24DynamicFiltersOperator3 = gx.num.trunc( 0 ,0) ;
      this.AV25ContratoServicosUnidConversao_Nome3 =  ''  ;
      this.s142_client();
   };
   this.e12so2_client=function()
   {
      this.executeServerEvent("GRIDPAGINATIONBAR.CHANGEPAGE", false, null, true, true);
   };
   this.e13so2_client=function()
   {
      this.executeServerEvent("VORDEREDBY.CLICK", true, null, false, true);
   };
   this.e14so2_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS1'", true, null, false, false);
   };
   this.e15so2_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS2'", true, null, false, false);
   };
   this.e16so2_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS3'", true, null, false, false);
   };
   this.e11so2_client=function()
   {
      this.executeServerEvent("DDO_MANAGEFILTERS.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e17so2_client=function()
   {
      this.executeServerEvent("'DOINSERT'", true, null, false, false);
   };
   this.e18so2_client=function()
   {
      this.executeServerEvent("'ADDDYNAMICFILTERS1'", true, null, false, false);
   };
   this.e19so2_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR1.CLICK", true, null, false, true);
   };
   this.e20so2_client=function()
   {
      this.executeServerEvent("'ADDDYNAMICFILTERS2'", true, null, false, false);
   };
   this.e21so2_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR2.CLICK", true, null, false, true);
   };
   this.e22so2_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR3.CLICK", true, null, false, true);
   };
   this.e26so2_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e27so2_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,11,14,16,18,20,21,23,28,31,33,35,37,40,42,44,45,48,50,52,54,57,59,61,62,65,67,69,71,74,76,78,79,82,85,89,90,91,92,93,94,95,96,101,102,103];
   this.GXLastCtrlId =103;
   this.GridContainer = new gx.grid.grid(this, 2,"WbpLvl2",88,"Grid","Grid","GridContainer",this.CmpContext,this.IsMasterPage,"wwcontratoservicosunidconversao",[],false,1,false,true,0,false,false,false,"",0,"px","Novo registro",true,false,false,null,null,false,"",false,[1,1,1,1]);
   var GridContainer = this.GridContainer;
   GridContainer.addBitmap("&Update","vUPDATE",89,36,"px",17,"px",null,"","","Image","");
   GridContainer.addBitmap("&Delete","vDELETE",90,36,"px",17,"px",null,"","","Image","");
   GridContainer.addBitmap("&Display","vDISPLAY",91,36,"px",17,"px",null,"","","Image","");
   GridContainer.addSingleLineEdit(160,92,"CONTRATOSERVICOS_CODIGO","Código do Serviço","","ContratoServicos_Codigo","int",0,"px",6,6,"right",null,[],160,"ContratoServicos_Codigo",false,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(2110,93,"CONTRATOSERVICOSUNIDCONVERSAO_CODIGO","Unidade de Contratação","","ContratoServicosUnidConversao_Codigo","int",0,"px",6,6,"right",null,[],2110,"ContratoServicosUnidConversao_Codigo",false,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(2111,94,"CONTRATOSERVICOSUNIDCONVERSAO_NOME","","","ContratoServicosUnidConversao_Nome","char",0,"px",50,50,"left",null,[],2111,"ContratoServicosUnidConversao_Nome",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(2112,95,"CONTRATOSERVICOSUNIDCONVERSAO_SIGLA","","","ContratoServicosUnidConversao_Sigla","char",0,"px",15,15,"left",null,[],2112,"ContratoServicosUnidConversao_Sigla",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(2114,96,"CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO","","","ContratoServicosUnidConversao_Conversao","decimal",0,"px",14,14,"right",null,[],2114,"ContratoServicosUnidConversao_Conversao",true,5,false,false,"BootstrapAttribute",1,"");
   this.setGrid(GridContainer);
   this.WORKWITHPLUSUTILITIES1Container = gx.uc.getNew(this, 100, 20, "DVelop_WorkWithPlusUtilities", "WORKWITHPLUSUTILITIES1Container", "Workwithplusutilities1");
   var WORKWITHPLUSUTILITIES1Container = this.WORKWITHPLUSUTILITIES1Container;
   WORKWITHPLUSUTILITIES1Container.setProp("Width", "Width", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Height", "Height", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Visible", "Visible", true, "bool");
   WORKWITHPLUSUTILITIES1Container.setProp("Enabled", "Enabled", true, "boolean");
   WORKWITHPLUSUTILITIES1Container.setProp("Class", "Class", "", "char");
   WORKWITHPLUSUTILITIES1Container.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(WORKWITHPLUSUTILITIES1Container);
   this.GRIDPAGINATIONBARContainer = gx.uc.getNew(this, 99, 20, "DVelop_DVPaginationBar", "GRIDPAGINATIONBARContainer", "Gridpaginationbar");
   var GRIDPAGINATIONBARContainer = this.GRIDPAGINATIONBARContainer;
   GRIDPAGINATIONBARContainer.setProp("Class", "Class", "PaginationBar", "str");
   GRIDPAGINATIONBARContainer.setProp("ShowFirst", "Showfirst", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowPrevious", "Showprevious", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowNext", "Shownext", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowLast", "Showlast", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("PagesToShow", "Pagestoshow", 5, "num");
   GRIDPAGINATIONBARContainer.setProp("PagingButtonsPosition", "Pagingbuttonsposition", "Right", "str");
   GRIDPAGINATIONBARContainer.setProp("PagingCaptionPosition", "Pagingcaptionposition", "Left", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridClass", "Emptygridclass", "PaginationBarEmptyGrid", "str");
   GRIDPAGINATIONBARContainer.setProp("SelectedPage", "Selectedpage", "", "char");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelector", "Rowsperpageselector", false, "bool");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelectedValue", "Rowsperpageselectedvalue", '', "int");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageOptions", "Rowsperpageoptions", "", "char");
   GRIDPAGINATIONBARContainer.setProp("First", "First", "|«", "str");
   GRIDPAGINATIONBARContainer.setProp("Previous", "Previous", "«", "str");
   GRIDPAGINATIONBARContainer.setProp("Next", "Next", "»", "str");
   GRIDPAGINATIONBARContainer.setProp("Last", "Last", "»|", "str");
   GRIDPAGINATIONBARContainer.setProp("Caption", "Caption", "Página <CURRENT_PAGE> de <TOTAL_PAGES>", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridCaption", "Emptygridcaption", "Não encontraram-se registros", "str");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageCaption", "Rowsperpagecaption", "", "char");
   GRIDPAGINATIONBARContainer.addV2CFunction('AV36GridCurrentPage', "vGRIDCURRENTPAGE", 'SetCurrentPage');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV36GridCurrentPage=UC.GetCurrentPage();gx.fn.setControlValue("vGRIDCURRENTPAGE",UC.ParentObject.AV36GridCurrentPage); });
   GRIDPAGINATIONBARContainer.addV2CFunction('AV37GridPageCount', "vGRIDPAGECOUNT", 'SetPageCount');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV37GridPageCount=UC.GetPageCount();gx.fn.setControlValue("vGRIDPAGECOUNT",UC.ParentObject.AV37GridPageCount); });
   GRIDPAGINATIONBARContainer.setProp("RecordCount", "Recordcount", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Page", "Page", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Visible", "Visible", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("Enabled", "Enabled", true, "boolean");
   GRIDPAGINATIONBARContainer.setC2ShowFunction(function(UC) { UC.show(); });
   GRIDPAGINATIONBARContainer.addEventHandler("ChangePage", this.e12so2_client);
   this.setUserControl(GRIDPAGINATIONBARContainer);
   this.DDO_MANAGEFILTERSContainer = gx.uc.getNew(this, 26, 20, "BootstrapDropDownOptions", "DDO_MANAGEFILTERSContainer", "Ddo_managefilters");
   var DDO_MANAGEFILTERSContainer = this.DDO_MANAGEFILTERSContainer;
   DDO_MANAGEFILTERSContainer.setDynProp("Icon", "Icon", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("Caption", "Caption", "", "str");
   DDO_MANAGEFILTERSContainer.setProp("Tooltip", "Tooltip", "Gerenciar filtros", "str");
   DDO_MANAGEFILTERSContainer.setProp("Cls", "Cls", "ManageFilters", "str");
   DDO_MANAGEFILTERSContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_MANAGEFILTERSContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "Regular", "str");
   DDO_MANAGEFILTERSContainer.setProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_MANAGEFILTERSContainer.setProp("IncludeSortASC", "Includesortasc", false, "boolean");
   DDO_MANAGEFILTERSContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "boolean");
   DDO_MANAGEFILTERSContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("IncludeFilter", "Includefilter", false, "boolean");
   DDO_MANAGEFILTERSContainer.setProp("FilterType", "Filtertype", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("FilterIsRange", "Filterisrange", false, "boolean");
   DDO_MANAGEFILTERSContainer.setProp("IncludeDataList", "Includedatalist", false, "boolean");
   DDO_MANAGEFILTERSContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_MANAGEFILTERSContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_MANAGEFILTERSContainer.setProp("FixedFilters", "Fixedfilters", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("CleanFilter", "Cleanfilter", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("SearchButtonText", "Searchbuttontext", "", "char");
   DDO_MANAGEFILTERSContainer.setProp("UpdateButtonText", "Updatebuttontext", "", "char");
   DDO_MANAGEFILTERSContainer.DropDownOptionsTitleSettingsIcons = '';
   DDO_MANAGEFILTERSContainer.addV2CFunction('AV33ManageFiltersData', "vMANAGEFILTERSDATA", 'SetDropDownOptionsData');
   DDO_MANAGEFILTERSContainer.addC2VFunction(function(UC) { UC.ParentObject.AV33ManageFiltersData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vMANAGEFILTERSDATA",UC.ParentObject.AV33ManageFiltersData); });
   DDO_MANAGEFILTERSContainer.setProp("Visible", "Visible", true, "bool");
   DDO_MANAGEFILTERSContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_MANAGEFILTERSContainer.setProp("Class", "Class", "", "char");
   DDO_MANAGEFILTERSContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_MANAGEFILTERSContainer.addEventHandler("OnOptionClicked", this.e11so2_client);
   this.setUserControl(DDO_MANAGEFILTERSContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[8]={fld:"TABLEHEADER",grid:0};
   GXValidFnc[11]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[14]={fld:"CONTRATOSERVICOSUNIDCONVERSAOTITLE", format:0,grid:0};
   GXValidFnc[16]={fld:"INSERT",grid:0};
   GXValidFnc[18]={fld:"ORDEREDTEXT", format:0,grid:0};
   GXValidFnc[20]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDBY",gxz:"ZV13OrderedBy",gxold:"OV13OrderedBy",gxvar:"AV13OrderedBy",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV13OrderedBy=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV13OrderedBy=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vORDEREDBY",gx.O.AV13OrderedBy)},c2v:function(){if(this.val()!==undefined)gx.O.AV13OrderedBy=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vORDEREDBY",'.')},nac:gx.falseFn};
   GXValidFnc[21]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDDSC",gxz:"ZV14OrderedDsc",gxold:"OV14OrderedDsc",gxvar:"AV14OrderedDsc",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV14OrderedDsc=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV14OrderedDsc=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setControlValue("vORDEREDDSC",gx.O.AV14OrderedDsc,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV14OrderedDsc=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vORDEREDDSC")},nac:gx.falseFn};
   GXValidFnc[23]={fld:"TABLEFILTERS",grid:0};
   GXValidFnc[28]={fld:"TABLEDYNAMICFILTERS",grid:0};
   GXValidFnc[31]={fld:"DYNAMICFILTERSPREFIX1", format:0,grid:0};
   GXValidFnc[33]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR1",gxz:"ZV15DynamicFiltersSelector1",gxold:"OV15DynamicFiltersSelector1",gxvar:"AV15DynamicFiltersSelector1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV15DynamicFiltersSelector1=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV15DynamicFiltersSelector1=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR1",gx.O.AV15DynamicFiltersSelector1)},c2v:function(){if(this.val()!==undefined)gx.O.AV15DynamicFiltersSelector1=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR1")},nac:gx.falseFn};
   GXValidFnc[35]={fld:"DYNAMICFILTERSMIDDLE1", format:0,grid:0};
   GXValidFnc[37]={fld:"TABLEMERGEDDYNAMICFILTERS1",grid:0};
   GXValidFnc[40]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSOPERATOR1",gxz:"ZV16DynamicFiltersOperator1",gxold:"OV16DynamicFiltersOperator1",gxvar:"AV16DynamicFiltersOperator1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV16DynamicFiltersOperator1=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV16DynamicFiltersOperator1=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSOPERATOR1",gx.O.AV16DynamicFiltersOperator1)},c2v:function(){if(this.val()!==undefined)gx.O.AV16DynamicFiltersOperator1=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vDYNAMICFILTERSOPERATOR1",'.')},nac:gx.falseFn};
   GXValidFnc[42]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATOSERVICOSUNIDCONVERSAO_NOME1",gxz:"ZV17ContratoServicosUnidConversao_Nome1",gxold:"OV17ContratoServicosUnidConversao_Nome1",gxvar:"AV17ContratoServicosUnidConversao_Nome1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV17ContratoServicosUnidConversao_Nome1=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV17ContratoServicosUnidConversao_Nome1=Value},v2c:function(){gx.fn.setControlValue("vCONTRATOSERVICOSUNIDCONVERSAO_NOME1",gx.O.AV17ContratoServicosUnidConversao_Nome1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV17ContratoServicosUnidConversao_Nome1=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATOSERVICOSUNIDCONVERSAO_NOME1")},nac:gx.falseFn};
   GXValidFnc[44]={fld:"ADDDYNAMICFILTERS1",grid:0};
   GXValidFnc[45]={fld:"REMOVEDYNAMICFILTERS1",grid:0};
   GXValidFnc[48]={fld:"DYNAMICFILTERSPREFIX2", format:0,grid:0};
   GXValidFnc[50]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR2",gxz:"ZV19DynamicFiltersSelector2",gxold:"OV19DynamicFiltersSelector2",gxvar:"AV19DynamicFiltersSelector2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV19DynamicFiltersSelector2=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV19DynamicFiltersSelector2=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR2",gx.O.AV19DynamicFiltersSelector2)},c2v:function(){if(this.val()!==undefined)gx.O.AV19DynamicFiltersSelector2=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR2")},nac:gx.falseFn};
   GXValidFnc[52]={fld:"DYNAMICFILTERSMIDDLE2", format:0,grid:0};
   GXValidFnc[54]={fld:"TABLEMERGEDDYNAMICFILTERS2",grid:0};
   GXValidFnc[57]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSOPERATOR2",gxz:"ZV20DynamicFiltersOperator2",gxold:"OV20DynamicFiltersOperator2",gxvar:"AV20DynamicFiltersOperator2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV20DynamicFiltersOperator2=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV20DynamicFiltersOperator2=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSOPERATOR2",gx.O.AV20DynamicFiltersOperator2)},c2v:function(){if(this.val()!==undefined)gx.O.AV20DynamicFiltersOperator2=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vDYNAMICFILTERSOPERATOR2",'.')},nac:gx.falseFn};
   GXValidFnc[59]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATOSERVICOSUNIDCONVERSAO_NOME2",gxz:"ZV21ContratoServicosUnidConversao_Nome2",gxold:"OV21ContratoServicosUnidConversao_Nome2",gxvar:"AV21ContratoServicosUnidConversao_Nome2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV21ContratoServicosUnidConversao_Nome2=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV21ContratoServicosUnidConversao_Nome2=Value},v2c:function(){gx.fn.setControlValue("vCONTRATOSERVICOSUNIDCONVERSAO_NOME2",gx.O.AV21ContratoServicosUnidConversao_Nome2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV21ContratoServicosUnidConversao_Nome2=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATOSERVICOSUNIDCONVERSAO_NOME2")},nac:gx.falseFn};
   GXValidFnc[61]={fld:"ADDDYNAMICFILTERS2",grid:0};
   GXValidFnc[62]={fld:"REMOVEDYNAMICFILTERS2",grid:0};
   GXValidFnc[65]={fld:"DYNAMICFILTERSPREFIX3", format:0,grid:0};
   GXValidFnc[67]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR3",gxz:"ZV23DynamicFiltersSelector3",gxold:"OV23DynamicFiltersSelector3",gxvar:"AV23DynamicFiltersSelector3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV23DynamicFiltersSelector3=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV23DynamicFiltersSelector3=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR3",gx.O.AV23DynamicFiltersSelector3)},c2v:function(){if(this.val()!==undefined)gx.O.AV23DynamicFiltersSelector3=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR3")},nac:gx.falseFn};
   GXValidFnc[69]={fld:"DYNAMICFILTERSMIDDLE3", format:0,grid:0};
   GXValidFnc[71]={fld:"TABLEMERGEDDYNAMICFILTERS3",grid:0};
   GXValidFnc[74]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSOPERATOR3",gxz:"ZV24DynamicFiltersOperator3",gxold:"OV24DynamicFiltersOperator3",gxvar:"AV24DynamicFiltersOperator3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV24DynamicFiltersOperator3=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV24DynamicFiltersOperator3=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSOPERATOR3",gx.O.AV24DynamicFiltersOperator3)},c2v:function(){if(this.val()!==undefined)gx.O.AV24DynamicFiltersOperator3=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vDYNAMICFILTERSOPERATOR3",'.')},nac:gx.falseFn};
   GXValidFnc[76]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATOSERVICOSUNIDCONVERSAO_NOME3",gxz:"ZV25ContratoServicosUnidConversao_Nome3",gxold:"OV25ContratoServicosUnidConversao_Nome3",gxvar:"AV25ContratoServicosUnidConversao_Nome3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV25ContratoServicosUnidConversao_Nome3=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV25ContratoServicosUnidConversao_Nome3=Value},v2c:function(){gx.fn.setControlValue("vCONTRATOSERVICOSUNIDCONVERSAO_NOME3",gx.O.AV25ContratoServicosUnidConversao_Nome3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV25ContratoServicosUnidConversao_Nome3=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATOSERVICOSUNIDCONVERSAO_NOME3")},nac:gx.falseFn};
   GXValidFnc[78]={fld:"REMOVEDYNAMICFILTERS3",grid:0};
   GXValidFnc[79]={fld:"JSDYNAMICFILTERS", format:1,grid:0};
   GXValidFnc[82]={fld:"TABLEGRIDHEADER",grid:0};
   GXValidFnc[85]={fld:"GRIDTABLEWITHPAGINATIONBAR",grid:0};
   GXValidFnc[89]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vUPDATE",gxz:"ZV38Update",gxold:"OV38Update",gxvar:"AV38Update",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV38Update=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV38Update=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vUPDATE",row || gx.fn.currentGridRowImpl(88),gx.O.AV38Update,gx.O.AV56Update_GXI)},c2v:function(){gx.O.AV56Update_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV38Update=this.val()},val:function(row){return gx.fn.getGridControlValue("vUPDATE",row || gx.fn.currentGridRowImpl(88))},val_GXI:function(row){return gx.fn.getGridControlValue("vUPDATE_GXI",row || gx.fn.currentGridRowImpl(88))}, gxvar_GXI:'AV56Update_GXI',nac:gx.falseFn};
   GXValidFnc[90]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vDELETE",gxz:"ZV39Delete",gxold:"OV39Delete",gxvar:"AV39Delete",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV39Delete=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV39Delete=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vDELETE",row || gx.fn.currentGridRowImpl(88),gx.O.AV39Delete,gx.O.AV57Delete_GXI)},c2v:function(){gx.O.AV57Delete_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV39Delete=this.val()},val:function(row){return gx.fn.getGridControlValue("vDELETE",row || gx.fn.currentGridRowImpl(88))},val_GXI:function(row){return gx.fn.getGridControlValue("vDELETE_GXI",row || gx.fn.currentGridRowImpl(88))}, gxvar_GXI:'AV57Delete_GXI',nac:gx.falseFn};
   GXValidFnc[91]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vDISPLAY",gxz:"ZV40Display",gxold:"OV40Display",gxvar:"AV40Display",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV40Display=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV40Display=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vDISPLAY",row || gx.fn.currentGridRowImpl(88),gx.O.AV40Display,gx.O.AV58Display_GXI)},c2v:function(){gx.O.AV58Display_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV40Display=this.val()},val:function(row){return gx.fn.getGridControlValue("vDISPLAY",row || gx.fn.currentGridRowImpl(88))},val_GXI:function(row){return gx.fn.getGridControlValue("vDISPLAY_GXI",row || gx.fn.currentGridRowImpl(88))}, gxvar_GXI:'AV58Display_GXI',nac:gx.falseFn};
   GXValidFnc[92]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOS_CODIGO",gxz:"Z160ContratoServicos_Codigo",gxold:"O160ContratoServicos_Codigo",gxvar:"A160ContratoServicos_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A160ContratoServicos_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z160ContratoServicos_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTRATOSERVICOS_CODIGO",row || gx.fn.currentGridRowImpl(88),gx.O.A160ContratoServicos_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A160ContratoServicos_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTRATOSERVICOS_CODIGO",row || gx.fn.currentGridRowImpl(88),'.')},nac:gx.falseFn};
   GXValidFnc[93]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:this.Valid_Contratoservicosunidconversao_codigo,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSUNIDCONVERSAO_CODIGO",gxz:"Z2110ContratoServicosUnidConversao_Codigo",gxold:"O2110ContratoServicosUnidConversao_Codigo",gxvar:"A2110ContratoServicosUnidConversao_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A2110ContratoServicosUnidConversao_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z2110ContratoServicosUnidConversao_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTRATOSERVICOSUNIDCONVERSAO_CODIGO",row || gx.fn.currentGridRowImpl(88),gx.O.A2110ContratoServicosUnidConversao_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A2110ContratoServicosUnidConversao_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTRATOSERVICOSUNIDCONVERSAO_CODIGO",row || gx.fn.currentGridRowImpl(88),'.')},nac:gx.falseFn};
   GXValidFnc[94]={lvl:2,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSUNIDCONVERSAO_NOME",gxz:"Z2111ContratoServicosUnidConversao_Nome",gxold:"O2111ContratoServicosUnidConversao_Nome",gxvar:"A2111ContratoServicosUnidConversao_Nome",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A2111ContratoServicosUnidConversao_Nome=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z2111ContratoServicosUnidConversao_Nome=Value},v2c:function(row){gx.fn.setGridControlValue("CONTRATOSERVICOSUNIDCONVERSAO_NOME",row || gx.fn.currentGridRowImpl(88),gx.O.A2111ContratoServicosUnidConversao_Nome,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A2111ContratoServicosUnidConversao_Nome=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTRATOSERVICOSUNIDCONVERSAO_NOME",row || gx.fn.currentGridRowImpl(88))},nac:gx.falseFn};
   GXValidFnc[95]={lvl:2,type:"char",len:15,dec:0,sign:false,pic:"@!",ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSUNIDCONVERSAO_SIGLA",gxz:"Z2112ContratoServicosUnidConversao_Sigla",gxold:"O2112ContratoServicosUnidConversao_Sigla",gxvar:"A2112ContratoServicosUnidConversao_Sigla",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A2112ContratoServicosUnidConversao_Sigla=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z2112ContratoServicosUnidConversao_Sigla=Value},v2c:function(row){gx.fn.setGridControlValue("CONTRATOSERVICOSUNIDCONVERSAO_SIGLA",row || gx.fn.currentGridRowImpl(88),gx.O.A2112ContratoServicosUnidConversao_Sigla,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A2112ContratoServicosUnidConversao_Sigla=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTRATOSERVICOSUNIDCONVERSAO_SIGLA",row || gx.fn.currentGridRowImpl(88))},nac:gx.falseFn};
   GXValidFnc[96]={lvl:2,type:"decimal",len:14,dec:5,sign:false,pic:"ZZ,ZZZ,ZZ9.999",ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO",gxz:"Z2114ContratoServicosUnidConversao_Conversao",gxold:"O2114ContratoServicosUnidConversao_Conversao",gxvar:"A2114ContratoServicosUnidConversao_Conversao",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A2114ContratoServicosUnidConversao_Conversao=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z2114ContratoServicosUnidConversao_Conversao=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(row){gx.fn.setGridDecimalValue("CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO",row || gx.fn.currentGridRowImpl(88),gx.O.A2114ContratoServicosUnidConversao_Conversao,5,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A2114ContratoServicosUnidConversao_Conversao=this.val()},val:function(row){return gx.fn.getGridDecimalValue("CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO",row || gx.fn.currentGridRowImpl(88),'.',',')},nac:gx.falseFn};
   GXValidFnc[101]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSENABLED2",gxz:"ZV18DynamicFiltersEnabled2",gxold:"OV18DynamicFiltersEnabled2",gxvar:"AV18DynamicFiltersEnabled2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV18DynamicFiltersEnabled2=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV18DynamicFiltersEnabled2=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vDYNAMICFILTERSENABLED2",gx.O.AV18DynamicFiltersEnabled2,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV18DynamicFiltersEnabled2=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSENABLED2")},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[102]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSENABLED3",gxz:"ZV22DynamicFiltersEnabled3",gxold:"OV22DynamicFiltersEnabled3",gxvar:"AV22DynamicFiltersEnabled3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV22DynamicFiltersEnabled3=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV22DynamicFiltersEnabled3=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vDYNAMICFILTERSENABLED3",gx.O.AV22DynamicFiltersEnabled3,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV22DynamicFiltersEnabled3=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSENABLED3")},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[103]={lvl:0,type:"int",len:1,dec:0,sign:false,pic:"9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vMANAGEFILTERSEXECUTIONSTEP",gxz:"ZV29ManageFiltersExecutionStep",gxold:"OV29ManageFiltersExecutionStep",gxvar:"AV29ManageFiltersExecutionStep",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV29ManageFiltersExecutionStep=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV29ManageFiltersExecutionStep=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vMANAGEFILTERSEXECUTIONSTEP",gx.O.AV29ManageFiltersExecutionStep,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV29ManageFiltersExecutionStep=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vMANAGEFILTERSEXECUTIONSTEP",'.')},nac:gx.falseFn};
   this.AV13OrderedBy = 0 ;
   this.ZV13OrderedBy = 0 ;
   this.OV13OrderedBy = 0 ;
   this.AV14OrderedDsc = false ;
   this.ZV14OrderedDsc = false ;
   this.OV14OrderedDsc = false ;
   this.AV15DynamicFiltersSelector1 = "" ;
   this.ZV15DynamicFiltersSelector1 = "" ;
   this.OV15DynamicFiltersSelector1 = "" ;
   this.AV16DynamicFiltersOperator1 = 0 ;
   this.ZV16DynamicFiltersOperator1 = 0 ;
   this.OV16DynamicFiltersOperator1 = 0 ;
   this.AV17ContratoServicosUnidConversao_Nome1 = "" ;
   this.ZV17ContratoServicosUnidConversao_Nome1 = "" ;
   this.OV17ContratoServicosUnidConversao_Nome1 = "" ;
   this.AV19DynamicFiltersSelector2 = "" ;
   this.ZV19DynamicFiltersSelector2 = "" ;
   this.OV19DynamicFiltersSelector2 = "" ;
   this.AV20DynamicFiltersOperator2 = 0 ;
   this.ZV20DynamicFiltersOperator2 = 0 ;
   this.OV20DynamicFiltersOperator2 = 0 ;
   this.AV21ContratoServicosUnidConversao_Nome2 = "" ;
   this.ZV21ContratoServicosUnidConversao_Nome2 = "" ;
   this.OV21ContratoServicosUnidConversao_Nome2 = "" ;
   this.AV23DynamicFiltersSelector3 = "" ;
   this.ZV23DynamicFiltersSelector3 = "" ;
   this.OV23DynamicFiltersSelector3 = "" ;
   this.AV24DynamicFiltersOperator3 = 0 ;
   this.ZV24DynamicFiltersOperator3 = 0 ;
   this.OV24DynamicFiltersOperator3 = 0 ;
   this.AV25ContratoServicosUnidConversao_Nome3 = "" ;
   this.ZV25ContratoServicosUnidConversao_Nome3 = "" ;
   this.OV25ContratoServicosUnidConversao_Nome3 = "" ;
   this.ZV38Update = "" ;
   this.OV38Update = "" ;
   this.ZV39Delete = "" ;
   this.OV39Delete = "" ;
   this.ZV40Display = "" ;
   this.OV40Display = "" ;
   this.Z160ContratoServicos_Codigo = 0 ;
   this.O160ContratoServicos_Codigo = 0 ;
   this.Z2110ContratoServicosUnidConversao_Codigo = 0 ;
   this.O2110ContratoServicosUnidConversao_Codigo = 0 ;
   this.Z2111ContratoServicosUnidConversao_Nome = "" ;
   this.O2111ContratoServicosUnidConversao_Nome = "" ;
   this.Z2112ContratoServicosUnidConversao_Sigla = "" ;
   this.O2112ContratoServicosUnidConversao_Sigla = "" ;
   this.Z2114ContratoServicosUnidConversao_Conversao = 0 ;
   this.O2114ContratoServicosUnidConversao_Conversao = 0 ;
   this.AV18DynamicFiltersEnabled2 = false ;
   this.ZV18DynamicFiltersEnabled2 = false ;
   this.OV18DynamicFiltersEnabled2 = false ;
   this.AV22DynamicFiltersEnabled3 = false ;
   this.ZV22DynamicFiltersEnabled3 = false ;
   this.OV22DynamicFiltersEnabled3 = false ;
   this.AV29ManageFiltersExecutionStep = 0 ;
   this.ZV29ManageFiltersExecutionStep = 0 ;
   this.OV29ManageFiltersExecutionStep = 0 ;
   this.AV13OrderedBy = 0 ;
   this.AV14OrderedDsc = false ;
   this.AV33ManageFiltersData = [ ] ;
   this.AV15DynamicFiltersSelector1 = "" ;
   this.AV16DynamicFiltersOperator1 = 0 ;
   this.AV17ContratoServicosUnidConversao_Nome1 = "" ;
   this.AV19DynamicFiltersSelector2 = "" ;
   this.AV20DynamicFiltersOperator2 = 0 ;
   this.AV21ContratoServicosUnidConversao_Nome2 = "" ;
   this.AV23DynamicFiltersSelector3 = "" ;
   this.AV24DynamicFiltersOperator3 = 0 ;
   this.AV25ContratoServicosUnidConversao_Nome3 = "" ;
   this.AV36GridCurrentPage = 0 ;
   this.AV18DynamicFiltersEnabled2 = false ;
   this.AV22DynamicFiltersEnabled3 = false ;
   this.AV29ManageFiltersExecutionStep = 0 ;
   this.AV38Update = "" ;
   this.AV39Delete = "" ;
   this.AV40Display = "" ;
   this.A160ContratoServicos_Codigo = 0 ;
   this.A2110ContratoServicosUnidConversao_Codigo = 0 ;
   this.A2111ContratoServicosUnidConversao_Nome = "" ;
   this.A2112ContratoServicosUnidConversao_Sigla = "" ;
   this.A2114ContratoServicosUnidConversao_Conversao = 0 ;
   this.AV6WWPContext = {} ;
   this.AV59Pgmname = "" ;
   this.AV10GridState = {} ;
   this.AV27DynamicFiltersIgnoreFirst = false ;
   this.AV26DynamicFiltersRemoving = false ;
   this.AV41Contrato_Codigo = 0 ;
   this.AV42ContratoServicos_UnidadeContratada = 0 ;
   this.Events = {"e12so2_client": ["GRIDPAGINATIONBAR.CHANGEPAGE", true] ,"e13so2_client": ["VORDEREDBY.CLICK", true] ,"e14so2_client": ["'REMOVEDYNAMICFILTERS1'", true] ,"e15so2_client": ["'REMOVEDYNAMICFILTERS2'", true] ,"e16so2_client": ["'REMOVEDYNAMICFILTERS3'", true] ,"e11so2_client": ["DDO_MANAGEFILTERS.ONOPTIONCLICKED", true] ,"e17so2_client": ["'DOINSERT'", true] ,"e18so2_client": ["'ADDDYNAMICFILTERS1'", true] ,"e19so2_client": ["VDYNAMICFILTERSSELECTOR1.CLICK", true] ,"e20so2_client": ["'ADDDYNAMICFILTERS2'", true] ,"e21so2_client": ["VDYNAMICFILTERSSELECTOR2.CLICK", true] ,"e22so2_client": ["VDYNAMICFILTERSSELECTOR3.CLICK", true] ,"e26so2_client": ["ENTER", true] ,"e27so2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{ctrl:'CONTRATOSERVICOSUNIDCONVERSAO_NOME',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("CONTRATOSERVICOSUNIDCONVERSAO_NOME","Title")',ctrl:'CONTRATOSERVICOSUNIDCONVERSAO_NOME',prop:'Title'},{ctrl:'CONTRATOSERVICOSUNIDCONVERSAO_SIGLA',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("CONTRATOSERVICOSUNIDCONVERSAO_SIGLA","Title")',ctrl:'CONTRATOSERVICOSUNIDCONVERSAO_SIGLA',prop:'Title'},{ctrl:'CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO","Title")',ctrl:'CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO',prop:'Title'},{av:'AV36GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV37GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("INSERT","Link")',ctrl:'INSERT',prop:'Link'},{av:'gx.fn.getCtrlProperty("INSERT","Enabled")',ctrl:'INSERT',prop:'Enabled'},{av:'AV33ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]];
   this.EvtParms["GRIDPAGINATIONBAR.CHANGEPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0},{av:'this.GRIDPAGINATIONBARContainer.SelectedPage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],[]];
   this.EvtParms["GRID.LOAD"] = [[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0}],[{av:'gx.fn.getCtrlProperty("vUPDATE","Tooltiptext")',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vUPDATE","Link")',ctrl:'vUPDATE',prop:'Link'},{av:'AV38Update',fld:'vUPDATE',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vUPDATE","Enabled")',ctrl:'vUPDATE',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vDELETE","Tooltiptext")',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vDELETE","Link")',ctrl:'vDELETE',prop:'Link'},{av:'AV39Delete',fld:'vDELETE',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vDELETE","Enabled")',ctrl:'vDELETE',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vDISPLAY","Tooltiptext")',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vDISPLAY","Link")',ctrl:'vDISPLAY',prop:'Link'},{av:'AV40Display',fld:'vDISPLAY',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vDISPLAY","Enabled")',ctrl:'vDISPLAY',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("CONTRATOSERVICOSUNIDCONVERSAO_NOME","Link")',ctrl:'CONTRATOSERVICOSUNIDCONVERSAO_NOME',prop:'Link'}]];
   this.EvtParms["VORDEREDBY.CLICK"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0}],[]];
   this.EvtParms["'ADDDYNAMICFILTERS1'"] = [[],[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS1'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0}],[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'gx.fn.getCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME2","Visible")',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR2'},{av:'gx.fn.getCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME3","Visible")',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR3'},{av:'gx.fn.getCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME1","Visible")',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR1'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR1.CLICK"] = [[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME1","Visible")',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR1'}]];
   this.EvtParms["'ADDDYNAMICFILTERS2'"] = [[],[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS2'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0}],[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'gx.fn.getCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME2","Visible")',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR2'},{av:'gx.fn.getCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME3","Visible")',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR3'},{av:'gx.fn.getCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME1","Visible")',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR1'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR2.CLICK"] = [[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME2","Visible")',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR2'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS3'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0}],[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'gx.fn.getCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME2","Visible")',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR2'},{av:'gx.fn.getCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME3","Visible")',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR3'},{av:'gx.fn.getCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME1","Visible")',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR1'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR3.CLICK"] = [[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME3","Visible")',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR3'}]];
   this.EvtParms["DDO_MANAGEFILTERS.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0},{av:'this.DDO_MANAGEFILTERSContainer.ActiveEventKey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'}],[{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosUnidConversao_Nome1',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosUnidConversao_Nome2',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosUnidConversao_Nome3',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME1","Visible")',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME1',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR1'},{av:'gx.fn.getCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME2","Visible")',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME2',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR2'},{av:'gx.fn.getCtrlProperty("vCONTRATOSERVICOSUNIDCONVERSAO_NOME3","Visible")',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_NOME3',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR3'}]];
   this.EvtParms["'DOINSERT'"] = [[{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0}],[{av:'AV42ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0},{av:'AV41Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.setVCMap("AV6WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("AV59Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV10GridState", "vGRIDSTATE", 0, "WWPBaseObjects\WWPGridState");
   this.setVCMap("AV27DynamicFiltersIgnoreFirst", "vDYNAMICFILTERSIGNOREFIRST", 0, "boolean");
   this.setVCMap("AV26DynamicFiltersRemoving", "vDYNAMICFILTERSREMOVING", 0, "boolean");
   this.setVCMap("AV41Contrato_Codigo", "vCONTRATO_CODIGO", 0, "int");
   this.setVCMap("AV42ContratoServicos_UnidadeContratada", "vCONTRATOSERVICOS_UNIDADECONTRATADA", 0, "int");
   this.setVCMap("AV6WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("AV59Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV10GridState", "vGRIDSTATE", 0, "WWPBaseObjects\WWPGridState");
   this.setVCMap("AV27DynamicFiltersIgnoreFirst", "vDYNAMICFILTERSIGNOREFIRST", 0, "boolean");
   this.setVCMap("AV26DynamicFiltersRemoving", "vDYNAMICFILTERSREMOVING", 0, "boolean");
   this.setVCMap("AV41Contrato_Codigo", "vCONTRATO_CODIGO", 0, "int");
   this.setVCMap("AV42ContratoServicos_UnidadeContratada", "vCONTRATOSERVICOS_UNIDADECONTRATADA", 0, "int");
   GridContainer.addRefreshingVar(this.GXValidFnc[20]);
   GridContainer.addRefreshingVar(this.GXValidFnc[21]);
   GridContainer.addRefreshingVar(this.GXValidFnc[33]);
   GridContainer.addRefreshingVar(this.GXValidFnc[40]);
   GridContainer.addRefreshingVar(this.GXValidFnc[42]);
   GridContainer.addRefreshingVar(this.GXValidFnc[50]);
   GridContainer.addRefreshingVar(this.GXValidFnc[57]);
   GridContainer.addRefreshingVar(this.GXValidFnc[59]);
   GridContainer.addRefreshingVar(this.GXValidFnc[67]);
   GridContainer.addRefreshingVar(this.GXValidFnc[74]);
   GridContainer.addRefreshingVar(this.GXValidFnc[76]);
   GridContainer.addRefreshingVar(this.GXValidFnc[101]);
   GridContainer.addRefreshingVar(this.GXValidFnc[102]);
   GridContainer.addRefreshingVar(this.GXValidFnc[103]);
   GridContainer.addRefreshingVar({rfrVar:"AV6WWPContext"});
   GridContainer.addRefreshingVar({rfrVar:"AV59Pgmname"});
   GridContainer.addRefreshingVar({rfrVar:"AV10GridState"});
   GridContainer.addRefreshingVar({rfrVar:"AV27DynamicFiltersIgnoreFirst"});
   GridContainer.addRefreshingVar({rfrVar:"AV26DynamicFiltersRemoving"});
   GridContainer.addRefreshingVar({rfrVar:"A160ContratoServicos_Codigo", rfrProp:"Value", gxAttId:"160"});
   GridContainer.addRefreshingVar({rfrVar:"A2110ContratoServicosUnidConversao_Codigo", rfrProp:"Value", gxAttId:"2110"});
   GridContainer.addRefreshingVar({rfrVar:"AV41Contrato_Codigo"});
   GridContainer.addRefreshingVar({rfrVar:"AV42ContratoServicos_UnidadeContratada"});
   this.InitStandaloneVars( );
});
gx.createParentObj(wwcontratoservicosunidconversao);
