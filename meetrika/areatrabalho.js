/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 0:18:52.36
*/
gx.evt.autoSkip = false;
gx.define('areatrabalho', false, function () {
   this.ServerClass =  "areatrabalho" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("trn");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV7AreaTrabalho_Codigo=gx.fn.getIntegerValue("vAREATRABALHO_CODIGO",'.') ;
      this.AV29Insert_AreaTrabalho_OrganizacaoCod=gx.fn.getIntegerValue("vINSERT_AREATRABALHO_ORGANIZACAOCOD",'.') ;
      this.A1216AreaTrabalho_OrganizacaoCod=gx.fn.getIntegerValue("AREATRABALHO_ORGANIZACAOCOD",'.') ;
      this.AV11Insert_Contratante_Codigo=gx.fn.getIntegerValue("vINSERT_CONTRATANTE_CODIGO",'.') ;
      this.AV26Contratante_Codigo=gx.fn.getIntegerValue("vCONTRATANTE_CODIGO",'.') ;
      this.AV27Insert_AreaTrabalho_ServicoPadrao=gx.fn.getIntegerValue("vINSERT_AREATRABALHO_SERVICOPADRAO",'.') ;
      this.A12Contratante_CNPJ=gx.fn.getControlValue("CONTRATANTE_CNPJ") ;
      this.A14Contratante_Email=gx.fn.getControlValue("CONTRATANTE_EMAIL") ;
      this.A33Contratante_Fax=gx.fn.getControlValue("CONTRATANTE_FAX") ;
      this.A11Contratante_IE=gx.fn.getControlValue("CONTRATANTE_IE") ;
      this.A10Contratante_NomeFantasia=gx.fn.getControlValue("CONTRATANTE_NOMEFANTASIA") ;
      this.A32Contratante_Ramal=gx.fn.getControlValue("CONTRATANTE_RAMAL") ;
      this.A9Contratante_RazaoSocial=gx.fn.getControlValue("CONTRATANTE_RAZAOSOCIAL") ;
      this.A31Contratante_Telefone=gx.fn.getControlValue("CONTRATANTE_TELEFONE") ;
      this.A13Contratante_WebSite=gx.fn.getControlValue("CONTRATANTE_WEBSITE") ;
      this.A25Municipio_Codigo=gx.fn.getIntegerValue("MUNICIPIO_CODIGO",'.') ;
      this.A23Estado_UF=gx.fn.getControlValue("ESTADO_UF") ;
      this.A547Contratante_EmailSdaHost=gx.fn.getControlValue("CONTRATANTE_EMAILSDAHOST") ;
      this.A548Contratante_EmailSdaUser=gx.fn.getControlValue("CONTRATANTE_EMAILSDAUSER") ;
      this.A549Contratante_EmailSdaPass=gx.fn.getControlValue("CONTRATANTE_EMAILSDAPASS") ;
      this.A550Contratante_EmailSdaKey=gx.fn.getControlValue("CONTRATANTE_EMAILSDAKEY") ;
      this.A552Contratante_EmailSdaPort=gx.fn.getIntegerValue("CONTRATANTE_EMAILSDAPORT",'.') ;
      this.A551Contratante_EmailSdaAut=gx.fn.getControlValue("CONTRATANTE_EMAILSDAAUT") ;
      this.A1048Contratante_EmailSdaSec=gx.fn.getIntegerValue("CONTRATANTE_EMAILSDASEC",'.') ;
      this.Gx_BScreen=gx.fn.getIntegerValue("vGXBSCREEN",'.') ;
      this.AV8WWPContext=gx.fn.getControlValue("vWWPCONTEXT") ;
      this.AV30AuditingObject=gx.fn.getControlValue("vAUDITINGOBJECT") ;
      this.AV39Causa=gx.fn.getControlValue("vCAUSA") ;
      this.A1154AreaTrabalho_TipoPlanilha=gx.fn.getIntegerValue("AREATRABALHO_TIPOPLANILHA",'.') ;
      this.A1588AreaTrabalho_SS_Codigo=gx.fn.getIntegerValue("AREATRABALHO_SS_CODIGO",'.') ;
      this.A2080AreaTrabalho_SelUsrPrestadora=gx.fn.getControlValue("AREATRABALHO_SELUSRPRESTADORA") ;
      this.A335Contratante_PessoaCod=gx.fn.getIntegerValue("CONTRATANTE_PESSOACOD",'.') ;
      this.A26Municipio_Nome=gx.fn.getControlValue("MUNICIPIO_NOME") ;
      this.A24Estado_Nome=gx.fn.getControlValue("ESTADO_NOME") ;
      this.A272AreaTrabalho_ContagensQtdGeral=gx.fn.getIntegerValue("AREATRABALHO_CONTAGENSQTDGERAL",'.') ;
      this.AV40Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.Gx_mode=gx.fn.getControlValue("vMODE") ;
      this.AV9TrnContext=gx.fn.getControlValue("vTRNCONTEXT") ;
   };
   this.Valid_Areatrabalho_codigo=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Areatrabalho_codigo",["gx.O.A5AreaTrabalho_Codigo", "gx.O.A272AreaTrabalho_ContagensQtdGeral"],["A272AreaTrabalho_ContagensQtdGeral"]);
      return true;
   }
   this.Valid_Areatrabalho_descricao=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("AREATRABALHO_DESCRICAO");
         this.AnyError  = 0;
         if ( ((''==this.A6AreaTrabalho_Descricao)) )
         {
            try {
               gxballoon.setError("Área de Trabalho é obrigatório.");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Areatrabalho_servicopadrao=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Areatrabalho_servicopadrao",["gx.O.A830AreaTrabalho_ServicoPadrao"],[]);
      return true;
   }
   this.Valid_Areatrabalho_ativo=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Areatrabalho_ativo",["gx.O.O72AreaTrabalho_Ativo", "gx.O.A72AreaTrabalho_Ativo", "gx.O.A5AreaTrabalho_Codigo", "gx.O.AV39Causa"],[]);
      return true;
   }
   this.Validv_Contratante_cnpj=function()
   {
      gx.ajax.validSrvEvt("dyncall","Validv_Contratante_cnpj",["gx.O.Gx_mode", "gx.O.AV15Contratante_CNPJ"],[["AV15Contratante_CNPJ","Enabled"], ["AV20Contratante_Email","Enabled"], ["AV23Contratante_Fax","Enabled"], ["AV18Contratante_IE","Enabled"], ["AV17Contratante_NomeFantasia","Enabled"], ["AV22Contratante_Ramal","Enabled"], ["AV16Contratante_RazaoSocial","Enabled"], ["AV21Contratante_Telefone","Enabled"], ["AV19Contratante_WebSite","Enabled"], ["AV25Municipio_Codigo","Enabled"], ["AV24Estado_UF","Enabled"], ["AV32Contratante_EmailSdaHost","Enabled"], ["AV33Contratante_EmailSdaUser","Enabled"], ["AV35Contratante_EmailSdaPass","Enabled"], ["AV36Contratante_EmailSdaPort","Enabled"], ["AV37Contratante_EmailSdaAut","Enabled"], ["AV34Contratante_EmailSdaSec","Enabled"]]);
      return true;
   }
   this.Validv_Contratante_ie=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATANTE_IE");
         this.AnyError  = 0;
         if ( ((''==this.AV18Contratante_IE)) )
         {
            try {
               gxballoon.setError("Insc. Estadual é obrigatório.");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratante_razaosocial=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATANTE_RAZAOSOCIAL");
         this.AnyError  = 0;
         if ( ((''==this.AV16Contratante_RazaoSocial)) )
         {
            try {
               gxballoon.setError("Razão Social é obrigatório.");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratante_nomefantasia=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATANTE_NOMEFANTASIA");
         this.AnyError  = 0;
         if ( ((''==this.AV17Contratante_NomeFantasia)) )
         {
            try {
               gxballoon.setError("Nome Fantasia é obrigatório.");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratante_website=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATANTE_WEBSITE");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratante_email=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATANTE_EMAIL");
         this.AnyError  = 0;
         if ( ! ( gx.util.regExp.isMatch(this.AV20Contratante_Email, "^((\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)|(\\s*))$") || ((''==this.AV20Contratante_Email)) ) )
         {
            try {
               gxballoon.setError("O valor de Contratante_Email não coincide com o padrão especificado");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratante_telefone=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATANTE_TELEFONE");
         this.AnyError  = 0;
         if ( ((''==this.AV21Contratante_Telefone)) )
         {
            try {
               gxballoon.setError("Telefone é obrigatório.");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratante_ramal=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATANTE_RAMAL");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratante_fax=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATANTE_FAX");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Estado_uf=function()
   {
      gx.ajax.validSrvEvt("dyncall","Validv_Estado_uf",["gx.O.AV24Estado_UF", "gx.O.AV25Municipio_Codigo"],["AV25Municipio_Codigo"]);
      return true;
   }
   this.Validv_Municipio_codigo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vMUNICIPIO_CODIGO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratante_emailsdahost=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATANTE_EMAILSDAHOST");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratante_emailsdauser=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATANTE_EMAILSDAUSER");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratante_emailsdapass=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATANTE_EMAILSDAPASS");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratante_emailsdaport=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATANTE_EMAILSDAPORT");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratante_emailsdaaut=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATANTE_EMAILSDAAUT");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratante_emailsdasec=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTRATANTE_EMAILSDASEC");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contratante_codigo=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Contratante_codigo",["gx.O.Gx_mode", "gx.O.A72AreaTrabalho_Ativo", "gx.O.A29Contratante_Codigo", "gx.O.A33Contratante_Fax", "gx.O.A32Contratante_Ramal", "gx.O.A31Contratante_Telefone", "gx.O.A14Contratante_Email", "gx.O.A13Contratante_WebSite", "gx.O.A11Contratante_IE", "gx.O.A10Contratante_NomeFantasia", "gx.O.A547Contratante_EmailSdaHost", "gx.O.A548Contratante_EmailSdaUser", "gx.O.A549Contratante_EmailSdaPass", "gx.O.A550Contratante_EmailSdaKey", "gx.O.A551Contratante_EmailSdaAut", "gx.O.A552Contratante_EmailSdaPort", "gx.O.A1048Contratante_EmailSdaSec", "gx.O.A25Municipio_Codigo", "gx.O.A23Estado_UF", "gx.O.A335Contratante_PessoaCod", "gx.O.A12Contratante_CNPJ", "gx.O.A9Contratante_RazaoSocial", "gx.O.AV23Contratante_Fax", "gx.O.AV22Contratante_Ramal", "gx.O.AV21Contratante_Telefone", "gx.O.AV20Contratante_Email", "gx.O.AV19Contratante_WebSite", "gx.O.AV18Contratante_IE", "gx.O.AV17Contratante_NomeFantasia", "gx.O.AV32Contratante_EmailSdaHost", "gx.O.AV33Contratante_EmailSdaUser", "gx.O.AV35Contratante_EmailSdaPass", "gx.O.AV37Contratante_EmailSdaAut", "gx.O.AV36Contratante_EmailSdaPort", "gx.O.AV34Contratante_EmailSdaSec", "gx.O.A26Municipio_Nome", "gx.O.A24Estado_Nome", "gx.O.AV24Estado_UF", "gx.O.AV25Municipio_Codigo", "gx.O.AV15Contratante_CNPJ", "gx.O.AV16Contratante_RazaoSocial", "gx.O.AV26Contratante_Codigo"],["A33Contratante_Fax", "A32Contratante_Ramal", "A31Contratante_Telefone", "A14Contratante_Email", "A13Contratante_WebSite", "A11Contratante_IE", "A10Contratante_NomeFantasia", "A547Contratante_EmailSdaHost", "A548Contratante_EmailSdaUser", "A549Contratante_EmailSdaPass", "A550Contratante_EmailSdaKey", "A551Contratante_EmailSdaAut", "A552Contratante_EmailSdaPort", "A1048Contratante_EmailSdaSec", "A25Municipio_Codigo", "A335Contratante_PessoaCod", "AV23Contratante_Fax", "AV22Contratante_Ramal", "AV21Contratante_Telefone", "AV20Contratante_Email", "AV19Contratante_WebSite", "AV18Contratante_IE", "AV17Contratante_NomeFantasia", "AV32Contratante_EmailSdaHost", "AV33Contratante_EmailSdaUser", "AV35Contratante_EmailSdaPass", "AV37Contratante_EmailSdaAut", "AV36Contratante_EmailSdaPort", "AV34Contratante_EmailSdaSec", "A26Municipio_Nome", "A23Estado_UF", "A24Estado_Nome", "AV24Estado_UF", "AV25Municipio_Codigo", "A12Contratante_CNPJ", "A9Contratante_RazaoSocial", "AV15Contratante_CNPJ", "AV16Contratante_RazaoSocial", "AV26Contratante_Codigo"]);
      return true;
   }
   this.e12042_client=function()
   {
      this.executeServerEvent("AFTER TRN", true, null, false, false);
   };
   this.e13042_client=function()
   {
      this.executeServerEvent("VCONTRATANTE_CNPJ.ISVALID", true, null, false, true);
   };
   this.e1404106_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e1504106_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,11,16,19,22,24,35,37,48,50,52,54,56,58,60,62,67,69,71,73,82,84,97,100,102,104,106,111,113,116,118,121,123,125,127,131,133,135,137,139,141,144,146,148,150,155,163,165,167,169,171,173,176,178,180,182,184,186,189,197,198,199];
   this.GXLastCtrlId =199;
   this.DVPANEL_AREADETRABALHOContainer = gx.uc.getNew(this, 14, 0, "BootstrapPanel", "DVPANEL_AREADETRABALHOContainer", "Dvpanel_areadetrabalho");
   var DVPANEL_AREADETRABALHOContainer = this.DVPANEL_AREADETRABALHOContainer;
   DVPANEL_AREADETRABALHOContainer.setProp("Width", "Width", "100%", "str");
   DVPANEL_AREADETRABALHOContainer.setProp("Height", "Height", "100", "str");
   DVPANEL_AREADETRABALHOContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_AREADETRABALHOContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_AREADETRABALHOContainer.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_AREADETRABALHOContainer.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_AREADETRABALHOContainer.setProp("Title", "Title", "Área de Trabalho", "str");
   DVPANEL_AREADETRABALHOContainer.setProp("Collapsible", "Collapsible", true, "bool");
   DVPANEL_AREADETRABALHOContainer.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_AREADETRABALHOContainer.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_AREADETRABALHOContainer.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_AREADETRABALHOContainer.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_AREADETRABALHOContainer.setProp("Visible", "Visible", true, "bool");
   DVPANEL_AREADETRABALHOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_AREADETRABALHOContainer.setProp("Class", "Class", "", "char");
   DVPANEL_AREADETRABALHOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_AREADETRABALHOContainer);
   this.DVPANEL_CONTRATANTEContainer = gx.uc.getNew(this, 95, 24, "BootstrapPanel", "DVPANEL_CONTRATANTEContainer", "Dvpanel_contratante");
   var DVPANEL_CONTRATANTEContainer = this.DVPANEL_CONTRATANTEContainer;
   DVPANEL_CONTRATANTEContainer.setProp("Width", "Width", "100%", "str");
   DVPANEL_CONTRATANTEContainer.setProp("Height", "Height", "100", "str");
   DVPANEL_CONTRATANTEContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_CONTRATANTEContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_CONTRATANTEContainer.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_CONTRATANTEContainer.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_CONTRATANTEContainer.setProp("Title", "Title", "Contratante", "str");
   DVPANEL_CONTRATANTEContainer.setProp("Collapsible", "Collapsible", false, "bool");
   DVPANEL_CONTRATANTEContainer.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_CONTRATANTEContainer.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_CONTRATANTEContainer.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_CONTRATANTEContainer.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_CONTRATANTEContainer.setProp("Visible", "Visible", true, "bool");
   DVPANEL_CONTRATANTEContainer.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_CONTRATANTEContainer.setProp("Class", "Class", "", "char");
   DVPANEL_CONTRATANTEContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_CONTRATANTEContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[5]={fld:"AREATRABALHOTITLE", format:0,grid:0};
   GXValidFnc[11]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[16]={fld:"AREADETRABALHO",grid:0};
   GXValidFnc[19]={fld:"UNNAMEDTABLE1",grid:0};
   GXValidFnc[22]={fld:"TEXTBLOCKORGANIZACAO_NOME", format:0,grid:0};
   GXValidFnc[24]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"ORGANIZACAO_NOME",gxz:"Z1214Organizacao_Nome",gxold:"O1214Organizacao_Nome",gxvar:"A1214Organizacao_Nome",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1214Organizacao_Nome=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1214Organizacao_Nome=Value},v2c:function(){gx.fn.setControlValue("ORGANIZACAO_NOME",gx.O.A1214Organizacao_Nome,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1214Organizacao_Nome=this.val()},val:function(){return gx.fn.getControlValue("ORGANIZACAO_NOME")},nac:gx.falseFn};
   this.declareDomainHdlr( 24 , function() {
   });
   GXValidFnc[35]={fld:"TEXTBLOCKAREATRABALHO_DESCRICAO", format:0,grid:0};
   GXValidFnc[37]={lvl:0,type:"svchar",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Areatrabalho_descricao,isvalid:null,rgrid:[],fld:"AREATRABALHO_DESCRICAO",gxz:"Z6AreaTrabalho_Descricao",gxold:"O6AreaTrabalho_Descricao",gxvar:"A6AreaTrabalho_Descricao",ucs:[],op:[37],ip:[37],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A6AreaTrabalho_Descricao=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z6AreaTrabalho_Descricao=Value},v2c:function(){gx.fn.setControlValue("AREATRABALHO_DESCRICAO",gx.O.A6AreaTrabalho_Descricao,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A6AreaTrabalho_Descricao=this.val()},val:function(){return gx.fn.getControlValue("AREATRABALHO_DESCRICAO")},nac:gx.falseFn};
   this.declareDomainHdlr( 37 , function() {
   });
   GXValidFnc[48]={fld:"TEXTBLOCKAREATRABALHO_CALCULOPFINAL", format:0,grid:0};
   GXValidFnc[50]={lvl:0,type:"char",len:2,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"AREATRABALHO_CALCULOPFINAL",gxz:"Z642AreaTrabalho_CalculoPFinal",gxold:"O642AreaTrabalho_CalculoPFinal",gxvar:"A642AreaTrabalho_CalculoPFinal",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A642AreaTrabalho_CalculoPFinal=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z642AreaTrabalho_CalculoPFinal=Value},v2c:function(){gx.fn.setComboBoxValue("AREATRABALHO_CALCULOPFINAL",gx.O.A642AreaTrabalho_CalculoPFinal);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A642AreaTrabalho_CalculoPFinal=this.val()},val:function(){return gx.fn.getControlValue("AREATRABALHO_CALCULOPFINAL")},nac:gx.falseFn};
   this.declareDomainHdlr( 50 , function() {
   });
   GXValidFnc[52]={fld:"TEXTBLOCKAREATRABALHO_SERVICOPADRAO", format:0,grid:0};
   GXValidFnc[54]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Areatrabalho_servicopadrao,isvalid:null,rgrid:[],fld:"AREATRABALHO_SERVICOPADRAO",gxz:"Z830AreaTrabalho_ServicoPadrao",gxold:"O830AreaTrabalho_ServicoPadrao",gxvar:"A830AreaTrabalho_ServicoPadrao",ucs:[],op:[],ip:[54],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.A830AreaTrabalho_ServicoPadrao=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z830AreaTrabalho_ServicoPadrao=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("AREATRABALHO_SERVICOPADRAO",gx.O.A830AreaTrabalho_ServicoPadrao);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A830AreaTrabalho_ServicoPadrao=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("AREATRABALHO_SERVICOPADRAO",'.')},nac:function(){return (this.Gx_mode=="INS")&&!((0==this.AV27Insert_AreaTrabalho_ServicoPadrao))}};
   this.declareDomainHdlr( 54 , function() {
   });
   GXValidFnc[56]={fld:"TEXTBLOCKAREATRABALHO_VALIDAOSFM", format:0,grid:0};
   GXValidFnc[58]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"AREATRABALHO_VALIDAOSFM",gxz:"Z834AreaTrabalho_ValidaOSFM",gxold:"O834AreaTrabalho_ValidaOSFM",gxvar:"A834AreaTrabalho_ValidaOSFM",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A834AreaTrabalho_ValidaOSFM=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z834AreaTrabalho_ValidaOSFM=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setComboBoxValue("AREATRABALHO_VALIDAOSFM",gx.O.A834AreaTrabalho_ValidaOSFM)},c2v:function(){if(this.val()!==undefined)gx.O.A834AreaTrabalho_ValidaOSFM=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("AREATRABALHO_VALIDAOSFM")},nac:gx.falseFn};
   GXValidFnc[60]={fld:"TEXTBLOCKAREATRABALHO_DIASPARAPAGAR", format:0,grid:0};
   GXValidFnc[62]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"AREATRABALHO_DIASPARAPAGAR",gxz:"Z855AreaTrabalho_DiasParaPagar",gxold:"O855AreaTrabalho_DiasParaPagar",gxvar:"A855AreaTrabalho_DiasParaPagar",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A855AreaTrabalho_DiasParaPagar=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z855AreaTrabalho_DiasParaPagar=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("AREATRABALHO_DIASPARAPAGAR",gx.O.A855AreaTrabalho_DiasParaPagar,0)},c2v:function(){if(this.val()!==undefined)gx.O.A855AreaTrabalho_DiasParaPagar=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("AREATRABALHO_DIASPARAPAGAR",'.')},nac:gx.falseFn};
   GXValidFnc[67]={fld:"TEXTBLOCKAREATRABALHO_CONTRATADAUPDBSLCOD", format:0,grid:0};
   GXValidFnc[69]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"AREATRABALHO_CONTRATADAUPDBSLCOD",gxz:"Z987AreaTrabalho_ContratadaUpdBslCod",gxold:"O987AreaTrabalho_ContratadaUpdBslCod",gxvar:"A987AreaTrabalho_ContratadaUpdBslCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.A987AreaTrabalho_ContratadaUpdBslCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z987AreaTrabalho_ContratadaUpdBslCod=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("AREATRABALHO_CONTRATADAUPDBSLCOD",gx.O.A987AreaTrabalho_ContratadaUpdBslCod);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A987AreaTrabalho_ContratadaUpdBslCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("AREATRABALHO_CONTRATADAUPDBSLCOD",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 69 , function() {
   });
   GXValidFnc[71]={fld:"TEXTBLOCKAREATRABALHO_VERTA", format:0,grid:0};
   GXValidFnc[73]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"AREATRABALHO_VERTA",gxz:"Z2081AreaTrabalho_VerTA",gxold:"O2081AreaTrabalho_VerTA",gxvar:"A2081AreaTrabalho_VerTA",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A2081AreaTrabalho_VerTA=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z2081AreaTrabalho_VerTA=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("AREATRABALHO_VERTA",gx.O.A2081AreaTrabalho_VerTA)},c2v:function(){if(this.val()!==undefined)gx.O.A2081AreaTrabalho_VerTA=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("AREATRABALHO_VERTA",'.')},nac:gx.falseFn};
   GXValidFnc[82]={fld:"TEXTBLOCKAREATRABALHO_ATIVO", format:0,grid:0};
   GXValidFnc[84]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Valid_Areatrabalho_ativo,isvalid:null,rgrid:[],fld:"AREATRABALHO_ATIVO",gxz:"Z72AreaTrabalho_Ativo",gxold:"O72AreaTrabalho_Ativo",gxvar:"A72AreaTrabalho_Ativo",ucs:[],op:[199,84],ip:[199,84],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.A72AreaTrabalho_Ativo=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z72AreaTrabalho_Ativo=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("AREATRABALHO_ATIVO",gx.O.A72AreaTrabalho_Ativo,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A72AreaTrabalho_Ativo=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("AREATRABALHO_ATIVO")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 84 , function() {
   });
   GXValidFnc[97]={fld:"CONTRATANTE",grid:0};
   GXValidFnc[100]={fld:"TEXTBLOCKCONTRATANTE_CNPJ", format:0,grid:0};
   GXValidFnc[102]={lvl:0,type:"svchar",len:15,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratante_cnpj,isvalid:'e13042_client',rgrid:[],fld:"vCONTRATANTE_CNPJ",gxz:"ZV15Contratante_CNPJ",gxold:"OV15Contratante_CNPJ",gxvar:"AV15Contratante_CNPJ",ucs:[],op:[102],ip:[102],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV15Contratante_CNPJ=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV15Contratante_CNPJ=Value},v2c:function(){gx.fn.setControlValue("vCONTRATANTE_CNPJ",gx.O.AV15Contratante_CNPJ,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV15Contratante_CNPJ=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATANTE_CNPJ")},nac:gx.falseFn};
   GXValidFnc[104]={fld:"TEXTBLOCKCONTRATANTE_IE", format:0,grid:0};
   GXValidFnc[106]={lvl:0,type:"char",len:15,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratante_ie,isvalid:null,rgrid:[],fld:"vCONTRATANTE_IE",gxz:"ZV18Contratante_IE",gxold:"OV18Contratante_IE",gxvar:"AV18Contratante_IE",ucs:[],op:[106],ip:[106],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV18Contratante_IE=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV18Contratante_IE=Value},v2c:function(){gx.fn.setControlValue("vCONTRATANTE_IE",gx.O.AV18Contratante_IE,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV18Contratante_IE=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATANTE_IE")},nac:gx.falseFn};
   GXValidFnc[111]={fld:"TEXTBLOCKCONTRATANTE_RAZAOSOCIAL", format:0,grid:0};
   GXValidFnc[113]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratante_razaosocial,isvalid:null,rgrid:[],fld:"vCONTRATANTE_RAZAOSOCIAL",gxz:"ZV16Contratante_RazaoSocial",gxold:"OV16Contratante_RazaoSocial",gxvar:"AV16Contratante_RazaoSocial",ucs:[],op:[113],ip:[113],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV16Contratante_RazaoSocial=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV16Contratante_RazaoSocial=Value},v2c:function(){gx.fn.setControlValue("vCONTRATANTE_RAZAOSOCIAL",gx.O.AV16Contratante_RazaoSocial,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV16Contratante_RazaoSocial=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATANTE_RAZAOSOCIAL")},nac:gx.falseFn};
   GXValidFnc[116]={fld:"TEXTBLOCKCONTRATANTE_NOMEFANTASIA", format:0,grid:0};
   GXValidFnc[118]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratante_nomefantasia,isvalid:null,rgrid:[],fld:"vCONTRATANTE_NOMEFANTASIA",gxz:"ZV17Contratante_NomeFantasia",gxold:"OV17Contratante_NomeFantasia",gxvar:"AV17Contratante_NomeFantasia",ucs:[],op:[118],ip:[118],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV17Contratante_NomeFantasia=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV17Contratante_NomeFantasia=Value},v2c:function(){gx.fn.setControlValue("vCONTRATANTE_NOMEFANTASIA",gx.O.AV17Contratante_NomeFantasia,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV17Contratante_NomeFantasia=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATANTE_NOMEFANTASIA")},nac:gx.falseFn};
   GXValidFnc[121]={fld:"TEXTBLOCKCONTRATANTE_WEBSITE", format:0,grid:0};
   GXValidFnc[123]={lvl:0,type:"svchar",len:100,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratante_website,isvalid:null,rgrid:[],fld:"vCONTRATANTE_WEBSITE",gxz:"ZV19Contratante_WebSite",gxold:"OV19Contratante_WebSite",gxvar:"AV19Contratante_WebSite",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV19Contratante_WebSite=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV19Contratante_WebSite=Value},v2c:function(){gx.fn.setControlValue("vCONTRATANTE_WEBSITE",gx.O.AV19Contratante_WebSite,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV19Contratante_WebSite=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATANTE_WEBSITE")},nac:gx.falseFn};
   GXValidFnc[125]={fld:"TEXTBLOCKCONTRATANTE_EMAIL", format:0,grid:0};
   GXValidFnc[127]={lvl:0,type:"svchar",len:100,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratante_email,isvalid:null,rgrid:[],fld:"vCONTRATANTE_EMAIL",gxz:"ZV20Contratante_Email",gxold:"OV20Contratante_Email",gxvar:"AV20Contratante_Email",ucs:[],op:[127],ip:[127],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV20Contratante_Email=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV20Contratante_Email=Value},v2c:function(){gx.fn.setControlValue("vCONTRATANTE_EMAIL",gx.O.AV20Contratante_Email,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV20Contratante_Email=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATANTE_EMAIL")},nac:gx.falseFn};
   GXValidFnc[131]={fld:"TEXTBLOCKCONTRATANTE_TELEFONE", format:0,grid:0};
   GXValidFnc[133]={lvl:0,type:"char",len:20,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratante_telefone,isvalid:null,rgrid:[],fld:"vCONTRATANTE_TELEFONE",gxz:"ZV21Contratante_Telefone",gxold:"OV21Contratante_Telefone",gxvar:"AV21Contratante_Telefone",ucs:[],op:[133],ip:[133],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV21Contratante_Telefone=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV21Contratante_Telefone=Value},v2c:function(){gx.fn.setControlValue("vCONTRATANTE_TELEFONE",gx.O.AV21Contratante_Telefone,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV21Contratante_Telefone=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATANTE_TELEFONE")},nac:gx.falseFn};
   GXValidFnc[135]={fld:"TEXTBLOCKCONTRATANTE_RAMAL", format:0,grid:0};
   GXValidFnc[137]={lvl:0,type:"char",len:10,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratante_ramal,isvalid:null,rgrid:[],fld:"vCONTRATANTE_RAMAL",gxz:"ZV22Contratante_Ramal",gxold:"OV22Contratante_Ramal",gxvar:"AV22Contratante_Ramal",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV22Contratante_Ramal=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV22Contratante_Ramal=Value},v2c:function(){gx.fn.setControlValue("vCONTRATANTE_RAMAL",gx.O.AV22Contratante_Ramal,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV22Contratante_Ramal=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATANTE_RAMAL")},nac:gx.falseFn};
   GXValidFnc[139]={fld:"TEXTBLOCKCONTRATANTE_FAX", format:0,grid:0};
   GXValidFnc[141]={lvl:0,type:"char",len:20,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratante_fax,isvalid:null,rgrid:[],fld:"vCONTRATANTE_FAX",gxz:"ZV23Contratante_Fax",gxold:"OV23Contratante_Fax",gxvar:"AV23Contratante_Fax",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV23Contratante_Fax=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV23Contratante_Fax=Value},v2c:function(){gx.fn.setControlValue("vCONTRATANTE_FAX",gx.O.AV23Contratante_Fax,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV23Contratante_Fax=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATANTE_FAX")},nac:gx.falseFn};
   GXValidFnc[144]={fld:"TEXTBLOCKESTADO_UF", format:0,grid:0};
   GXValidFnc[146]={lvl:0,type:"char",len:2,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Estado_uf,isvalid:null,rgrid:[],fld:"vESTADO_UF",gxz:"ZV24Estado_UF",gxold:"OV24Estado_UF",gxvar:"AV24Estado_UF",ucs:[],op:[150],ip:[150,146],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV24Estado_UF=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV24Estado_UF=Value},v2c:function(){gx.fn.setComboBoxValue("vESTADO_UF",gx.O.AV24Estado_UF)},c2v:function(){if(this.val()!==undefined)gx.O.AV24Estado_UF=this.val()},val:function(){return gx.fn.getControlValue("vESTADO_UF")},nac:gx.falseFn};
   GXValidFnc[148]={fld:"TEXTBLOCKMUNICIPIO_CODIGO", format:0,grid:0};
   GXValidFnc[150]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Validv_Municipio_codigo,isvalid:null,rgrid:[],fld:"vMUNICIPIO_CODIGO",gxz:"ZV25Municipio_Codigo",gxold:"OV25Municipio_Codigo",gxvar:"AV25Municipio_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV25Municipio_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV25Municipio_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vMUNICIPIO_CODIGO",gx.O.AV25Municipio_Codigo)},c2v:function(){if(this.val()!==undefined)gx.O.AV25Municipio_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vMUNICIPIO_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[155]={fld:"LBLEMAIL", format:0,grid:0};
   GXValidFnc[163]={fld:"TEXTBLOCKCONTRATANTE_EMAILSDAHOST", format:0,grid:0};
   GXValidFnc[165]={lvl:0,type:"svchar",len:50,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratante_emailsdahost,isvalid:null,rgrid:[],fld:"vCONTRATANTE_EMAILSDAHOST",gxz:"ZV32Contratante_EmailSdaHost",gxold:"OV32Contratante_EmailSdaHost",gxvar:"AV32Contratante_EmailSdaHost",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV32Contratante_EmailSdaHost=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV32Contratante_EmailSdaHost=Value},v2c:function(){gx.fn.setControlValue("vCONTRATANTE_EMAILSDAHOST",gx.O.AV32Contratante_EmailSdaHost,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV32Contratante_EmailSdaHost=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATANTE_EMAILSDAHOST")},nac:gx.falseFn};
   GXValidFnc[167]={fld:"TEXTBLOCKCONTRATANTE_EMAILSDAUSER", format:0,grid:0};
   GXValidFnc[169]={lvl:0,type:"svchar",len:50,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratante_emailsdauser,isvalid:null,rgrid:[],fld:"vCONTRATANTE_EMAILSDAUSER",gxz:"ZV33Contratante_EmailSdaUser",gxold:"OV33Contratante_EmailSdaUser",gxvar:"AV33Contratante_EmailSdaUser",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV33Contratante_EmailSdaUser=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV33Contratante_EmailSdaUser=Value},v2c:function(){gx.fn.setControlValue("vCONTRATANTE_EMAILSDAUSER",gx.O.AV33Contratante_EmailSdaUser,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV33Contratante_EmailSdaUser=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATANTE_EMAILSDAUSER")},nac:gx.falseFn};
   GXValidFnc[171]={fld:"TEXTBLOCKCONTRATANTE_EMAILSDAPASS", format:0,grid:0};
   GXValidFnc[173]={lvl:0,type:"svchar",len:100,dec:0,sign:false,isPwd:true,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratante_emailsdapass,isvalid:null,rgrid:[],fld:"vCONTRATANTE_EMAILSDAPASS",gxz:"ZV35Contratante_EmailSdaPass",gxold:"OV35Contratante_EmailSdaPass",gxvar:"AV35Contratante_EmailSdaPass",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV35Contratante_EmailSdaPass=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV35Contratante_EmailSdaPass=Value},v2c:function(){gx.fn.setControlValue("vCONTRATANTE_EMAILSDAPASS",gx.O.AV35Contratante_EmailSdaPass,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV35Contratante_EmailSdaPass=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATANTE_EMAILSDAPASS")},nac:gx.falseFn};
   GXValidFnc[176]={fld:"TEXTBLOCKCONTRATANTE_EMAILSDAPORT", format:0,grid:0};
   GXValidFnc[178]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratante_emailsdaport,isvalid:null,rgrid:[],fld:"vCONTRATANTE_EMAILSDAPORT",gxz:"ZV36Contratante_EmailSdaPort",gxold:"OV36Contratante_EmailSdaPort",gxvar:"AV36Contratante_EmailSdaPort",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV36Contratante_EmailSdaPort=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV36Contratante_EmailSdaPort=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vCONTRATANTE_EMAILSDAPORT",gx.O.AV36Contratante_EmailSdaPort,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV36Contratante_EmailSdaPort=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTRATANTE_EMAILSDAPORT",'.')},nac:gx.falseFn};
   GXValidFnc[180]={fld:"TEXTBLOCKCONTRATANTE_EMAILSDAAUT", format:0,grid:0};
   GXValidFnc[182]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratante_emailsdaaut,isvalid:null,rgrid:[],fld:"vCONTRATANTE_EMAILSDAAUT",gxz:"ZV37Contratante_EmailSdaAut",gxold:"OV37Contratante_EmailSdaAut",gxvar:"AV37Contratante_EmailSdaAut",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV37Contratante_EmailSdaAut=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV37Contratante_EmailSdaAut=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setComboBoxValue("vCONTRATANTE_EMAILSDAAUT",gx.O.AV37Contratante_EmailSdaAut)},c2v:function(){if(this.val()!==undefined)gx.O.AV37Contratante_EmailSdaAut=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vCONTRATANTE_EMAILSDAAUT")},nac:gx.falseFn};
   GXValidFnc[184]={fld:"TEXTBLOCKCONTRATANTE_EMAILSDASEC", format:0,grid:0};
   GXValidFnc[186]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratante_emailsdasec,isvalid:null,rgrid:[],fld:"vCONTRATANTE_EMAILSDASEC",gxz:"ZV34Contratante_EmailSdaSec",gxold:"OV34Contratante_EmailSdaSec",gxvar:"AV34Contratante_EmailSdaSec",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV34Contratante_EmailSdaSec=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV34Contratante_EmailSdaSec=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vCONTRATANTE_EMAILSDASEC",gx.O.AV34Contratante_EmailSdaSec)},c2v:function(){if(this.val()!==undefined)gx.O.AV34Contratante_EmailSdaSec=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTRATANTE_EMAILSDASEC",'.')},nac:gx.falseFn};
   GXValidFnc[189]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[197]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vPODEDESATIVAR",gxz:"ZV38PodeDesativar",gxold:"OV38PodeDesativar",gxvar:"AV38PodeDesativar",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV38PodeDesativar=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV38PodeDesativar=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vPODEDESATIVAR",gx.O.AV38PodeDesativar,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.AV38PodeDesativar=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vPODEDESATIVAR")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 197 , function() {
   });
   GXValidFnc[198]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contratante_codigo,isvalid:null,rgrid:[],fld:"CONTRATANTE_CODIGO",gxz:"Z29Contratante_Codigo",gxold:"O29Contratante_Codigo",gxvar:"A29Contratante_Codigo",ucs:[],op:[113,102,150,146,186,178,182,173,169,165,118,106,123,127,133,137,141],ip:[113,102,150,146,186,178,182,173,169,165,118,106,123,127,133,137,141,198],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A29Contratante_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z29Contratante_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("CONTRATANTE_CODIGO",gx.O.A29Contratante_Codigo,0)},c2v:function(){if(this.val()!==undefined)gx.O.A29Contratante_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTRATANTE_CODIGO",'.')},nac:function(){return (this.Gx_mode=="INS")&&!((0==this.AV11Insert_Contratante_Codigo))}};
   GXValidFnc[199]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Areatrabalho_codigo,isvalid:null,rgrid:[],fld:"AREATRABALHO_CODIGO",gxz:"Z5AreaTrabalho_Codigo",gxold:"O5AreaTrabalho_Codigo",gxvar:"A5AreaTrabalho_Codigo",ucs:[],op:[],ip:[199],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A5AreaTrabalho_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z5AreaTrabalho_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("AREATRABALHO_CODIGO",gx.O.A5AreaTrabalho_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A5AreaTrabalho_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("AREATRABALHO_CODIGO",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 199 , function() {
   });
   this.A1214Organizacao_Nome = "" ;
   this.Z1214Organizacao_Nome = "" ;
   this.O1214Organizacao_Nome = "" ;
   this.A6AreaTrabalho_Descricao = "" ;
   this.Z6AreaTrabalho_Descricao = "" ;
   this.O6AreaTrabalho_Descricao = "" ;
   this.A642AreaTrabalho_CalculoPFinal = "" ;
   this.Z642AreaTrabalho_CalculoPFinal = "" ;
   this.O642AreaTrabalho_CalculoPFinal = "" ;
   this.A830AreaTrabalho_ServicoPadrao = 0 ;
   this.Z830AreaTrabalho_ServicoPadrao = 0 ;
   this.O830AreaTrabalho_ServicoPadrao = 0 ;
   this.A834AreaTrabalho_ValidaOSFM = false ;
   this.Z834AreaTrabalho_ValidaOSFM = false ;
   this.O834AreaTrabalho_ValidaOSFM = false ;
   this.A855AreaTrabalho_DiasParaPagar = 0 ;
   this.Z855AreaTrabalho_DiasParaPagar = 0 ;
   this.O855AreaTrabalho_DiasParaPagar = 0 ;
   this.A987AreaTrabalho_ContratadaUpdBslCod = 0 ;
   this.Z987AreaTrabalho_ContratadaUpdBslCod = 0 ;
   this.O987AreaTrabalho_ContratadaUpdBslCod = 0 ;
   this.A2081AreaTrabalho_VerTA = 0 ;
   this.Z2081AreaTrabalho_VerTA = 0 ;
   this.O2081AreaTrabalho_VerTA = 0 ;
   this.A72AreaTrabalho_Ativo = false ;
   this.Z72AreaTrabalho_Ativo = false ;
   this.O72AreaTrabalho_Ativo = false ;
   this.AV15Contratante_CNPJ = "" ;
   this.ZV15Contratante_CNPJ = "" ;
   this.OV15Contratante_CNPJ = "" ;
   this.AV18Contratante_IE = "" ;
   this.ZV18Contratante_IE = "" ;
   this.OV18Contratante_IE = "" ;
   this.AV16Contratante_RazaoSocial = "" ;
   this.ZV16Contratante_RazaoSocial = "" ;
   this.OV16Contratante_RazaoSocial = "" ;
   this.AV17Contratante_NomeFantasia = "" ;
   this.ZV17Contratante_NomeFantasia = "" ;
   this.OV17Contratante_NomeFantasia = "" ;
   this.AV19Contratante_WebSite = "" ;
   this.ZV19Contratante_WebSite = "" ;
   this.OV19Contratante_WebSite = "" ;
   this.AV20Contratante_Email = "" ;
   this.ZV20Contratante_Email = "" ;
   this.OV20Contratante_Email = "" ;
   this.AV21Contratante_Telefone = "" ;
   this.ZV21Contratante_Telefone = "" ;
   this.OV21Contratante_Telefone = "" ;
   this.AV22Contratante_Ramal = "" ;
   this.ZV22Contratante_Ramal = "" ;
   this.OV22Contratante_Ramal = "" ;
   this.AV23Contratante_Fax = "" ;
   this.ZV23Contratante_Fax = "" ;
   this.OV23Contratante_Fax = "" ;
   this.AV24Estado_UF = "" ;
   this.ZV24Estado_UF = "" ;
   this.OV24Estado_UF = "" ;
   this.AV25Municipio_Codigo = 0 ;
   this.ZV25Municipio_Codigo = 0 ;
   this.OV25Municipio_Codigo = 0 ;
   this.AV32Contratante_EmailSdaHost = "" ;
   this.ZV32Contratante_EmailSdaHost = "" ;
   this.OV32Contratante_EmailSdaHost = "" ;
   this.AV33Contratante_EmailSdaUser = "" ;
   this.ZV33Contratante_EmailSdaUser = "" ;
   this.OV33Contratante_EmailSdaUser = "" ;
   this.AV35Contratante_EmailSdaPass = "" ;
   this.ZV35Contratante_EmailSdaPass = "" ;
   this.OV35Contratante_EmailSdaPass = "" ;
   this.AV36Contratante_EmailSdaPort = 0 ;
   this.ZV36Contratante_EmailSdaPort = 0 ;
   this.OV36Contratante_EmailSdaPort = 0 ;
   this.AV37Contratante_EmailSdaAut = false ;
   this.ZV37Contratante_EmailSdaAut = false ;
   this.OV37Contratante_EmailSdaAut = false ;
   this.AV34Contratante_EmailSdaSec = 0 ;
   this.ZV34Contratante_EmailSdaSec = 0 ;
   this.OV34Contratante_EmailSdaSec = 0 ;
   this.AV38PodeDesativar = false ;
   this.ZV38PodeDesativar = false ;
   this.OV38PodeDesativar = false ;
   this.A29Contratante_Codigo = 0 ;
   this.Z29Contratante_Codigo = 0 ;
   this.O29Contratante_Codigo = 0 ;
   this.A5AreaTrabalho_Codigo = 0 ;
   this.Z5AreaTrabalho_Codigo = 0 ;
   this.O5AreaTrabalho_Codigo = 0 ;
   this.AV8WWPContext = {} ;
   this.AV9TrnContext = {} ;
   this.AV41GXV1 = 0 ;
   this.AV29Insert_AreaTrabalho_OrganizacaoCod = 0 ;
   this.AV11Insert_Contratante_Codigo = 0 ;
   this.AV27Insert_AreaTrabalho_ServicoPadrao = 0 ;
   this.AV39Causa = "" ;
   this.AV31ParametrosSistema = {} ;
   this.AV32Contratante_EmailSdaHost = "" ;
   this.AV33Contratante_EmailSdaUser = "" ;
   this.AV35Contratante_EmailSdaPass = "" ;
   this.AV36Contratante_EmailSdaPort = 0 ;
   this.AV37Contratante_EmailSdaAut = false ;
   this.AV34Contratante_EmailSdaSec = 0 ;
   this.AV12TrnContextAtt = {} ;
   this.AV7AreaTrabalho_Codigo = 0 ;
   this.AV10WebSession = {} ;
   this.A5AreaTrabalho_Codigo = 0 ;
   this.A1216AreaTrabalho_OrganizacaoCod = 0 ;
   this.A29Contratante_Codigo = 0 ;
   this.A830AreaTrabalho_ServicoPadrao = 0 ;
   this.AV15Contratante_CNPJ = "" ;
   this.AV18Contratante_IE = "" ;
   this.AV16Contratante_RazaoSocial = "" ;
   this.AV17Contratante_NomeFantasia = "" ;
   this.AV19Contratante_WebSite = "" ;
   this.AV20Contratante_Email = "" ;
   this.AV21Contratante_Telefone = "" ;
   this.AV22Contratante_Ramal = "" ;
   this.AV23Contratante_Fax = "" ;
   this.AV24Estado_UF = "" ;
   this.AV30AuditingObject = {} ;
   this.AV26Contratante_Codigo = 0 ;
   this.AV25Municipio_Codigo = 0 ;
   this.Gx_BScreen = 0 ;
   this.AV40Pgmname = "" ;
   this.A6AreaTrabalho_Descricao = "" ;
   this.A1214Organizacao_Nome = "" ;
   this.A335Contratante_PessoaCod = 0 ;
   this.A23Estado_UF = "" ;
   this.A24Estado_Nome = "" ;
   this.A25Municipio_Codigo = 0 ;
   this.A26Municipio_Nome = "" ;
   this.A33Contratante_Fax = "" ;
   this.A32Contratante_Ramal = "" ;
   this.A31Contratante_Telefone = "" ;
   this.A14Contratante_Email = "" ;
   this.A13Contratante_WebSite = "" ;
   this.A12Contratante_CNPJ = "" ;
   this.A11Contratante_IE = "" ;
   this.A10Contratante_NomeFantasia = "" ;
   this.A9Contratante_RazaoSocial = "" ;
   this.A547Contratante_EmailSdaHost = "" ;
   this.A548Contratante_EmailSdaUser = "" ;
   this.A549Contratante_EmailSdaPass = "" ;
   this.A550Contratante_EmailSdaKey = "" ;
   this.A551Contratante_EmailSdaAut = false ;
   this.A552Contratante_EmailSdaPort = 0 ;
   this.A1048Contratante_EmailSdaSec = 0 ;
   this.A272AreaTrabalho_ContagensQtdGeral = 0 ;
   this.A642AreaTrabalho_CalculoPFinal = "" ;
   this.A834AreaTrabalho_ValidaOSFM = false ;
   this.A855AreaTrabalho_DiasParaPagar = 0 ;
   this.A987AreaTrabalho_ContratadaUpdBslCod = 0 ;
   this.A1154AreaTrabalho_TipoPlanilha = 0 ;
   this.A72AreaTrabalho_Ativo = false ;
   this.A1588AreaTrabalho_SS_Codigo = 0 ;
   this.A2081AreaTrabalho_VerTA = 0 ;
   this.A2080AreaTrabalho_SelUsrPrestadora = false ;
   this.Gx_mode = "" ;
   this.Events = {"e12042_client": ["AFTER TRN", true] ,"e13042_client": ["VCONTRATANTE_CNPJ.ISVALID", true] ,"e1404106_client": ["ENTER", true] ,"e1504106_client": ["CANCEL", true]};
   this.EvtParms["ENTER"] = [[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[]];
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["AFTER TRN"] = [[{av:'A830AreaTrabalho_ServicoPadrao',fld:'AREATRABALHO_SERVICOPADRAO',pic:'ZZZZZ9',nv:0},{av:'AV8WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV30AuditingObject',fld:'vAUDITINGOBJECT',pic:'',nv:null},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],[{av:'AV8WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["VCONTRATANTE_CNPJ.ISVALID"] = [[{av:'AV15Contratante_CNPJ',fld:'vCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV20Contratante_Email',fld:'vCONTRATANTE_EMAIL',pic:'',nv:''},{av:'AV23Contratante_Fax',fld:'vCONTRATANTE_FAX',pic:'',nv:''},{av:'AV18Contratante_IE',fld:'vCONTRATANTE_IE',pic:'',nv:''},{av:'AV17Contratante_NomeFantasia',fld:'vCONTRATANTE_NOMEFANTASIA',pic:'@!',nv:''},{av:'AV22Contratante_Ramal',fld:'vCONTRATANTE_RAMAL',pic:'',nv:''},{av:'AV16Contratante_RazaoSocial',fld:'vCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV21Contratante_Telefone',fld:'vCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV19Contratante_WebSite',fld:'vCONTRATANTE_WEBSITE',pic:'',nv:''},{av:'AV25Municipio_Codigo',fld:'vMUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24Estado_UF',fld:'vESTADO_UF',pic:'@!',nv:''}],[{av:'AV26Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EnterCtrl = ["BTN_TRN_ENTER"];
   this.setVCMap("AV7AreaTrabalho_Codigo", "vAREATRABALHO_CODIGO", 0, "int");
   this.setVCMap("AV29Insert_AreaTrabalho_OrganizacaoCod", "vINSERT_AREATRABALHO_ORGANIZACAOCOD", 0, "int");
   this.setVCMap("A1216AreaTrabalho_OrganizacaoCod", "AREATRABALHO_ORGANIZACAOCOD", 0, "int");
   this.setVCMap("AV11Insert_Contratante_Codigo", "vINSERT_CONTRATANTE_CODIGO", 0, "int");
   this.setVCMap("AV26Contratante_Codigo", "vCONTRATANTE_CODIGO", 0, "int");
   this.setVCMap("AV27Insert_AreaTrabalho_ServicoPadrao", "vINSERT_AREATRABALHO_SERVICOPADRAO", 0, "int");
   this.setVCMap("A12Contratante_CNPJ", "CONTRATANTE_CNPJ", 0, "svchar");
   this.setVCMap("A14Contratante_Email", "CONTRATANTE_EMAIL", 0, "svchar");
   this.setVCMap("A33Contratante_Fax", "CONTRATANTE_FAX", 0, "char");
   this.setVCMap("A11Contratante_IE", "CONTRATANTE_IE", 0, "char");
   this.setVCMap("A10Contratante_NomeFantasia", "CONTRATANTE_NOMEFANTASIA", 0, "char");
   this.setVCMap("A32Contratante_Ramal", "CONTRATANTE_RAMAL", 0, "char");
   this.setVCMap("A9Contratante_RazaoSocial", "CONTRATANTE_RAZAOSOCIAL", 0, "char");
   this.setVCMap("A31Contratante_Telefone", "CONTRATANTE_TELEFONE", 0, "char");
   this.setVCMap("A13Contratante_WebSite", "CONTRATANTE_WEBSITE", 0, "svchar");
   this.setVCMap("A25Municipio_Codigo", "MUNICIPIO_CODIGO", 0, "int");
   this.setVCMap("A23Estado_UF", "ESTADO_UF", 0, "char");
   this.setVCMap("A547Contratante_EmailSdaHost", "CONTRATANTE_EMAILSDAHOST", 0, "svchar");
   this.setVCMap("A548Contratante_EmailSdaUser", "CONTRATANTE_EMAILSDAUSER", 0, "svchar");
   this.setVCMap("A549Contratante_EmailSdaPass", "CONTRATANTE_EMAILSDAPASS", 0, "svchar");
   this.setVCMap("A550Contratante_EmailSdaKey", "CONTRATANTE_EMAILSDAKEY", 0, "char");
   this.setVCMap("A552Contratante_EmailSdaPort", "CONTRATANTE_EMAILSDAPORT", 0, "int");
   this.setVCMap("A551Contratante_EmailSdaAut", "CONTRATANTE_EMAILSDAAUT", 0, "boolean");
   this.setVCMap("A1048Contratante_EmailSdaSec", "CONTRATANTE_EMAILSDASEC", 0, "int");
   this.setVCMap("Gx_BScreen", "vGXBSCREEN", 0, "int");
   this.setVCMap("AV8WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("AV30AuditingObject", "vAUDITINGOBJECT", 0, "WWPBaseObjects\AuditingObject");
   this.setVCMap("AV39Causa", "vCAUSA", 0, "char");
   this.setVCMap("A1154AreaTrabalho_TipoPlanilha", "AREATRABALHO_TIPOPLANILHA", 0, "int");
   this.setVCMap("A1588AreaTrabalho_SS_Codigo", "AREATRABALHO_SS_CODIGO", 0, "int");
   this.setVCMap("A2080AreaTrabalho_SelUsrPrestadora", "AREATRABALHO_SELUSRPRESTADORA", 0, "boolean");
   this.setVCMap("A335Contratante_PessoaCod", "CONTRATANTE_PESSOACOD", 0, "int");
   this.setVCMap("A26Municipio_Nome", "MUNICIPIO_NOME", 0, "char");
   this.setVCMap("A24Estado_Nome", "ESTADO_NOME", 0, "char");
   this.setVCMap("A272AreaTrabalho_ContagensQtdGeral", "AREATRABALHO_CONTAGENSQTDGERAL", 0, "int");
   this.setVCMap("AV40Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("Gx_mode", "vMODE", 0, "char");
   this.setVCMap("AV9TrnContext", "vTRNCONTEXT", 0, "WWPBaseObjects\WWPTransactionContext");
   this.InitStandaloneVars( );
   this.LvlOlds[ 106 ]= ["O72AreaTrabalho_Ativo"] ;
});
gx.createParentObj(areatrabalho);
