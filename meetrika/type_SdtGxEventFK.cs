/*
               File: type_SdtGxEventFK
        Description: GxEventFK
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:56.55
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "GxEventFK" )]
   [XmlType(TypeName =  "GxEventFK" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtGxEventFK : GxUserType
   {
      public SdtGxEventFK( )
      {
         /* Constructor for serialization */
      }

      public SdtGxEventFK( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtGxEventFK deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtGxEventFK)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtGxEventFK obj ;
         obj = this;
         obj.gxTpr_Fk = deserialized.gxTpr_Fk;
         obj.gxTpr_Fkmap = deserialized.gxTpr_Fkmap;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "FK") )
               {
                  if ( gxTv_SdtGxEventFK_Fk == null )
                  {
                     gxTv_SdtGxEventFK_Fk = new GxSimpleCollection();
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtGxEventFK_Fk.readxmlcollection(oReader, "FK", "FKItem");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FKMap") )
               {
                  if ( gxTv_SdtGxEventFK_Fkmap == null )
                  {
                     gxTv_SdtGxEventFK_Fkmap = new GxSimpleCollection();
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtGxEventFK_Fkmap.readxmlcollection(oReader, "FKMap", "FKMapItem");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "GxEventFK";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         if ( gxTv_SdtGxEventFK_Fk != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_Meetrika";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_Meetrika";
            }
            gxTv_SdtGxEventFK_Fk.writexmlcollection(oWriter, "FK", sNameSpace1, "FKItem", sNameSpace1);
         }
         if ( gxTv_SdtGxEventFK_Fkmap != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_Meetrika";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_Meetrika";
            }
            gxTv_SdtGxEventFK_Fkmap.writexmlcollection(oWriter, "FKMap", sNameSpace1, "FKMapItem", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         if ( gxTv_SdtGxEventFK_Fk != null )
         {
            AddObjectProperty("FK", gxTv_SdtGxEventFK_Fk, false);
         }
         if ( gxTv_SdtGxEventFK_Fkmap != null )
         {
            AddObjectProperty("FKMap", gxTv_SdtGxEventFK_Fkmap, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "FK" )]
      [  XmlArray( ElementName = "FK"  )]
      [  XmlArrayItemAttribute( Type= typeof( String ), ElementName= "FKItem"  , IsNullable=false)]
      public GxSimpleCollection gxTpr_Fk_GxSimpleCollection
      {
         get {
            if ( gxTv_SdtGxEventFK_Fk == null )
            {
               gxTv_SdtGxEventFK_Fk = new GxSimpleCollection();
            }
            return (GxSimpleCollection)gxTv_SdtGxEventFK_Fk ;
         }

         set {
            if ( gxTv_SdtGxEventFK_Fk == null )
            {
               gxTv_SdtGxEventFK_Fk = new GxSimpleCollection();
            }
            gxTv_SdtGxEventFK_Fk = (GxSimpleCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Fk
      {
         get {
            if ( gxTv_SdtGxEventFK_Fk == null )
            {
               gxTv_SdtGxEventFK_Fk = new GxSimpleCollection();
            }
            return gxTv_SdtGxEventFK_Fk ;
         }

         set {
            gxTv_SdtGxEventFK_Fk = value;
         }

      }

      public void gxTv_SdtGxEventFK_Fk_SetNull( )
      {
         gxTv_SdtGxEventFK_Fk = null;
         return  ;
      }

      public bool gxTv_SdtGxEventFK_Fk_IsNull( )
      {
         if ( gxTv_SdtGxEventFK_Fk == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "FKMap" )]
      [  XmlArray( ElementName = "FKMap"  )]
      [  XmlArrayItemAttribute( Type= typeof( String ), ElementName= "FKMapItem"  , IsNullable=false)]
      public GxSimpleCollection gxTpr_Fkmap_GxSimpleCollection
      {
         get {
            if ( gxTv_SdtGxEventFK_Fkmap == null )
            {
               gxTv_SdtGxEventFK_Fkmap = new GxSimpleCollection();
            }
            return (GxSimpleCollection)gxTv_SdtGxEventFK_Fkmap ;
         }

         set {
            if ( gxTv_SdtGxEventFK_Fkmap == null )
            {
               gxTv_SdtGxEventFK_Fkmap = new GxSimpleCollection();
            }
            gxTv_SdtGxEventFK_Fkmap = (GxSimpleCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Fkmap
      {
         get {
            if ( gxTv_SdtGxEventFK_Fkmap == null )
            {
               gxTv_SdtGxEventFK_Fkmap = new GxSimpleCollection();
            }
            return gxTv_SdtGxEventFK_Fkmap ;
         }

         set {
            gxTv_SdtGxEventFK_Fkmap = value;
         }

      }

      public void gxTv_SdtGxEventFK_Fkmap_SetNull( )
      {
         gxTv_SdtGxEventFK_Fkmap = null;
         return  ;
      }

      public bool gxTv_SdtGxEventFK_Fkmap_IsNull( )
      {
         if ( gxTv_SdtGxEventFK_Fkmap == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( String ))]
      protected IGxCollection gxTv_SdtGxEventFK_Fk=null ;
      [ObjectCollection(ItemType=typeof( String ))]
      protected IGxCollection gxTv_SdtGxEventFK_Fkmap=null ;
   }

   [DataContract(Name = @"GxEventFK", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtGxEventFK_RESTInterface : GxGenericCollectionItem<SdtGxEventFK>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtGxEventFK_RESTInterface( ) : base()
      {
      }

      public SdtGxEventFK_RESTInterface( SdtGxEventFK psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "FK" , Order = 0 )]
      public GxSimpleCollection gxTpr_Fk
      {
         get {
            return (GxSimpleCollection)(sdt.gxTpr_Fk) ;
         }

         set {
            sdt.gxTpr_Fk = value;
         }

      }

      [DataMember( Name = "FKMap" , Order = 1 )]
      public GxSimpleCollection gxTpr_Fkmap
      {
         get {
            return (GxSimpleCollection)(sdt.gxTpr_Fkmap) ;
         }

         set {
            sdt.gxTpr_Fkmap = value;
         }

      }

      public SdtGxEventFK sdt
      {
         get {
            return (SdtGxEventFK)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtGxEventFK() ;
         }
      }

   }

}
