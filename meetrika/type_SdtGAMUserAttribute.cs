/*
               File: type_SdtGAMUserAttribute
        Description: GAMUserAttribute
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 21:58:14.48
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMUserAttribute : GxUserType, IGxExternalObject
   {
      public SdtGAMUserAttribute( )
      {
         initialize();
      }

      public SdtGAMUserAttribute( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMUserAttribute_externalReference == null )
         {
            GAMUserAttribute_externalReference = new Artech.Security.GAMUserAttribute(context);
         }
         returntostring = "";
         returntostring = (String)(GAMUserAttribute_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Id
      {
         get {
            if ( GAMUserAttribute_externalReference == null )
            {
               GAMUserAttribute_externalReference = new Artech.Security.GAMUserAttribute(context);
            }
            return GAMUserAttribute_externalReference.Id ;
         }

         set {
            if ( GAMUserAttribute_externalReference == null )
            {
               GAMUserAttribute_externalReference = new Artech.Security.GAMUserAttribute(context);
            }
            GAMUserAttribute_externalReference.Id = value;
         }

      }

      public bool gxTpr_Ismultivalue
      {
         get {
            if ( GAMUserAttribute_externalReference == null )
            {
               GAMUserAttribute_externalReference = new Artech.Security.GAMUserAttribute(context);
            }
            return GAMUserAttribute_externalReference.IsMultiValue ;
         }

         set {
            if ( GAMUserAttribute_externalReference == null )
            {
               GAMUserAttribute_externalReference = new Artech.Security.GAMUserAttribute(context);
            }
            GAMUserAttribute_externalReference.IsMultiValue = value;
         }

      }

      public String gxTpr_Value
      {
         get {
            if ( GAMUserAttribute_externalReference == null )
            {
               GAMUserAttribute_externalReference = new Artech.Security.GAMUserAttribute(context);
            }
            return GAMUserAttribute_externalReference.Value ;
         }

         set {
            if ( GAMUserAttribute_externalReference == null )
            {
               GAMUserAttribute_externalReference = new Artech.Security.GAMUserAttribute(context);
            }
            GAMUserAttribute_externalReference.Value = value;
         }

      }

      public IGxCollection gxTpr_Multivalues
      {
         get {
            if ( GAMUserAttribute_externalReference == null )
            {
               GAMUserAttribute_externalReference = new Artech.Security.GAMUserAttribute(context);
            }
            IGxCollection intValue ;
            intValue = new GxExternalCollection( context, "SdtGAMUserAttributeMultiValues", "GeneXus.Programs");
            System.Collections.Generic.List<Artech.Security.GAMUserAttributeMultiValues> externalParm0 ;
            externalParm0 = GAMUserAttribute_externalReference.MultiValues;
            intValue.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMUserAttributeMultiValues>), externalParm0);
            return intValue ;
         }

         set {
            if ( GAMUserAttribute_externalReference == null )
            {
               GAMUserAttribute_externalReference = new Artech.Security.GAMUserAttribute(context);
            }
            IGxCollection intValue ;
            System.Collections.Generic.List<Artech.Security.GAMUserAttributeMultiValues> externalParm1 ;
            intValue = value;
            externalParm1 = (System.Collections.Generic.List<Artech.Security.GAMUserAttributeMultiValues>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List<Artech.Security.GAMUserAttributeMultiValues>), intValue.ExternalInstance);
            GAMUserAttribute_externalReference.MultiValues = externalParm1;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMUserAttribute_externalReference == null )
            {
               GAMUserAttribute_externalReference = new Artech.Security.GAMUserAttribute(context);
            }
            return GAMUserAttribute_externalReference ;
         }

         set {
            GAMUserAttribute_externalReference = (Artech.Security.GAMUserAttribute)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMUserAttribute GAMUserAttribute_externalReference=null ;
   }

}
