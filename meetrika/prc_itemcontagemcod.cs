/*
               File: PRC_ItemContagemCod
        Description: Item Contagem Cod
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:28.3
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_itemcontagemcod : GXProcedure
   {
      public prc_itemcontagemcod( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_itemcontagemcod( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_ContagemResultado_Demanda ,
                           ref int aP1_ContagemResultado_Codigo )
      {
         this.AV8ContagemResultado_Demanda = aP0_ContagemResultado_Demanda;
         this.AV9ContagemResultado_Codigo = aP1_ContagemResultado_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Demanda=this.AV8ContagemResultado_Demanda;
         aP1_ContagemResultado_Codigo=this.AV9ContagemResultado_Codigo;
      }

      public int executeUdp( ref String aP0_ContagemResultado_Demanda )
      {
         this.AV8ContagemResultado_Demanda = aP0_ContagemResultado_Demanda;
         this.AV9ContagemResultado_Codigo = aP1_ContagemResultado_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Demanda=this.AV8ContagemResultado_Demanda;
         aP1_ContagemResultado_Codigo=this.AV9ContagemResultado_Codigo;
         return AV9ContagemResultado_Codigo ;
      }

      public void executeSubmit( ref String aP0_ContagemResultado_Demanda ,
                                 ref int aP1_ContagemResultado_Codigo )
      {
         prc_itemcontagemcod objprc_itemcontagemcod;
         objprc_itemcontagemcod = new prc_itemcontagemcod();
         objprc_itemcontagemcod.AV8ContagemResultado_Demanda = aP0_ContagemResultado_Demanda;
         objprc_itemcontagemcod.AV9ContagemResultado_Codigo = aP1_ContagemResultado_Codigo;
         objprc_itemcontagemcod.context.SetSubmitInitialConfig(context);
         objprc_itemcontagemcod.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_itemcontagemcod);
         aP0_ContagemResultado_Demanda=this.AV8ContagemResultado_Demanda;
         aP1_ContagemResultado_Codigo=this.AV9ContagemResultado_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_itemcontagemcod)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P005N2 */
         pr_default.execute(0, new Object[] {AV8ContagemResultado_Demanda});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A793ContagemResultadoItem_Demanda = P005N2_A793ContagemResultadoItem_Demanda[0];
            A773ContagemResultadoItem_ContagemCod = P005N2_A773ContagemResultadoItem_ContagemCod[0];
            n773ContagemResultadoItem_ContagemCod = P005N2_n773ContagemResultadoItem_ContagemCod[0];
            A772ContagemResultadoItem_Codigo = P005N2_A772ContagemResultadoItem_Codigo[0];
            A773ContagemResultadoItem_ContagemCod = AV9ContagemResultado_Codigo;
            n773ContagemResultadoItem_ContagemCod = false;
            BatchSize = 100;
            pr_default.initializeBatch( 1, BatchSize, this, "Executebatchp005n3");
            /* Using cursor P005N3 */
            pr_default.addRecord(1, new Object[] {n773ContagemResultadoItem_ContagemCod, A773ContagemResultadoItem_ContagemCod, A772ContagemResultadoItem_Codigo});
            if ( pr_default.recordCount(1) == pr_default.getBatchSize(1) )
            {
               Executebatchp005n3( ) ;
            }
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoItem") ;
            pr_default.readNext(0);
         }
         if ( pr_default.getBatchSize(1) > 0 )
         {
            Executebatchp005n3( ) ;
         }
         pr_default.close(0);
         this.cleanup();
      }

      protected void Executebatchp005n3( )
      {
         /* Using cursor P005N3 */
         pr_default.executeBatch(1);
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ItemContagemCod");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P005N2_A793ContagemResultadoItem_Demanda = new String[] {""} ;
         P005N2_A773ContagemResultadoItem_ContagemCod = new int[1] ;
         P005N2_n773ContagemResultadoItem_ContagemCod = new bool[] {false} ;
         P005N2_A772ContagemResultadoItem_Codigo = new int[1] ;
         A793ContagemResultadoItem_Demanda = "";
         P005N3_A773ContagemResultadoItem_ContagemCod = new int[1] ;
         P005N3_n773ContagemResultadoItem_ContagemCod = new bool[] {false} ;
         P005N3_A772ContagemResultadoItem_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_itemcontagemcod__default(),
            new Object[][] {
                new Object[] {
               P005N2_A793ContagemResultadoItem_Demanda, P005N2_A773ContagemResultadoItem_ContagemCod, P005N2_n773ContagemResultadoItem_ContagemCod, P005N2_A772ContagemResultadoItem_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV9ContagemResultado_Codigo ;
      private int A773ContagemResultadoItem_ContagemCod ;
      private int A772ContagemResultadoItem_Codigo ;
      private int BatchSize ;
      private String scmdbuf ;
      private bool n773ContagemResultadoItem_ContagemCod ;
      private String AV8ContagemResultado_Demanda ;
      private String A793ContagemResultadoItem_Demanda ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_ContagemResultado_Demanda ;
      private int aP1_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private String[] P005N2_A793ContagemResultadoItem_Demanda ;
      private int[] P005N2_A773ContagemResultadoItem_ContagemCod ;
      private bool[] P005N2_n773ContagemResultadoItem_ContagemCod ;
      private int[] P005N2_A772ContagemResultadoItem_Codigo ;
      private int[] P005N3_A773ContagemResultadoItem_ContagemCod ;
      private bool[] P005N3_n773ContagemResultadoItem_ContagemCod ;
      private int[] P005N3_A772ContagemResultadoItem_Codigo ;
   }

   public class prc_itemcontagemcod__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new BatchUpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP005N2 ;
          prmP005N2 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Demanda",SqlDbType.VarChar,30,0}
          } ;
          Object[] prmP005N3 ;
          prmP005N3 = new Object[] {
          new Object[] {"@ContagemResultadoItem_ContagemCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoItem_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P005N2", "SELECT [ContagemResultadoItem_Demanda], [ContagemResultadoItem_ContagemCod], [ContagemResultadoItem_Codigo] FROM [ContagemResultadoItem] WITH (UPDLOCK) WHERE [ContagemResultadoItem_Demanda] = @AV8ContagemResultado_Demanda ORDER BY [ContagemResultadoItem_Demanda] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005N2,1,0,true,false )
             ,new CursorDef("P005N3", "UPDATE [ContagemResultadoItem] SET [ContagemResultadoItem_ContagemCod]=@ContagemResultadoItem_ContagemCod  WHERE [ContagemResultadoItem_Codigo] = @ContagemResultadoItem_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005N3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
