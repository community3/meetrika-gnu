/*
               File: WP_AssociarContratoServicosArtefatos
        Description: Associar Artefatos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:23:39.68
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_associarcontratoservicosartefatos : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_associarcontratoservicosartefatos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_associarcontratoservicosartefatos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicos_Codigo )
      {
         this.AV11ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         lstavNotassociatedrecords = new GXListbox();
         lstavAssociatedrecords = new GXListbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV11ContratoServicos_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContratoServicos_Codigo), 6, 0)));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAN32( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSN32( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEN32( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Associar Artefatos") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031221233975");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_associarcontratoservicosartefatos.aspx") + "?" + UrlEncode("" +AV11ContratoServicos_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vADDEDKEYLIST", AV7AddedKeyList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vADDEDKEYLIST", AV7AddedKeyList);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vADDEDDSCLIST", AV5AddedDscList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vADDEDDSCLIST", AV5AddedDscList);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTADDEDKEYLIST", AV24NotAddedKeyList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTADDEDKEYLIST", AV24NotAddedKeyList);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTADDEDDSCLIST", AV22NotAddedDscList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTADDEDDSCLIST", AV22NotAddedDscList);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ARTEFATOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1749Artefatos_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSARTEFATOS", AV12ContratoServicosArtefatos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSARTEFATOS", AV12ContratoServicosArtefatos);
         }
         GxWebStd.gx_hidden_field( context, "vARTEFATOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Artefatos_Codigo), 6, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormN32( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_AssociarContratoServicosArtefatos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Associar Artefatos" ;
      }

      protected void WBN30( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_N32( true) ;
         }
         else
         {
            wb_table1_2_N32( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_N32e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavAddedkeylistxml_Internalname, AV8AddedKeyListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", 0, edtavAddedkeylistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarContratoServicosArtefatos.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNotaddedkeylistxml_Internalname, AV25NotAddedKeyListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", 0, edtavNotaddedkeylistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarContratoServicosArtefatos.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavAddeddsclistxml_Internalname, AV6AddedDscListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", 0, edtavAddeddsclistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarContratoServicosArtefatos.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNotaddeddsclistxml_Internalname, AV23NotAddedDscListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", 0, edtavNotaddeddsclistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarContratoServicosArtefatos.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavContratoservicos_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11ContratoServicos_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(AV11ContratoServicos_Codigo), "ZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicos_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavContratoservicos_codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_AssociarContratoServicosArtefatos.htm");
         }
         wbLoad = true;
      }

      protected void STARTN32( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Associar Artefatos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPN30( ) ;
      }

      protected void WSN32( )
      {
         STARTN32( ) ;
         EVTN32( ) ;
      }

      protected void EVTN32( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11N32 */
                           E11N32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12N32 */
                           E12N32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E13N32 */
                                 E13N32 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14N32 */
                           E14N32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15N32 */
                           E15N32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16N32 */
                           E16N32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17N32 */
                           E17N32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18N32 */
                           E18N32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNOTASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19N32 */
                           E19N32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20N32 */
                           E20N32 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18N32 */
                           E18N32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNOTASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19N32 */
                           E19N32 ();
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEN32( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormN32( ) ;
            }
         }
      }

      protected void PAN32( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            lstavNotassociatedrecords.Name = "vNOTASSOCIATEDRECORDS";
            lstavNotassociatedrecords.WebTags = "";
            lstavNotassociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "Selecione", 0);
            if ( lstavNotassociatedrecords.ItemCount > 0 )
            {
               AV26NotAssociatedRecords = (int)(NumberUtil.Val( lstavNotassociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26NotAssociatedRecords), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26NotAssociatedRecords), 6, 0)));
            }
            lstavAssociatedrecords.Name = "vASSOCIATEDRECORDS";
            lstavAssociatedrecords.WebTags = "";
            lstavAssociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "Selecione", 0);
            if ( lstavAssociatedrecords.ItemCount > 0 )
            {
               AV10AssociatedRecords = (int)(NumberUtil.Val( lstavAssociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = lstavNotassociatedrecords_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( lstavNotassociatedrecords.ItemCount > 0 )
         {
            AV26NotAssociatedRecords = (int)(NumberUtil.Val( lstavNotassociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26NotAssociatedRecords), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26NotAssociatedRecords), 6, 0)));
         }
         if ( lstavAssociatedrecords.ItemCount > 0 )
         {
            AV10AssociatedRecords = (int)(NumberUtil.Val( lstavAssociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFN32( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFN32( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E12N32 */
         E12N32 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E20N32 */
            E20N32 ();
            WBN30( ) ;
         }
      }

      protected void STRUPN30( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11N32 */
         E11N32 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            lstavNotassociatedrecords.CurrentValue = cgiGet( lstavNotassociatedrecords_Internalname);
            AV26NotAssociatedRecords = (int)(NumberUtil.Val( cgiGet( lstavNotassociatedrecords_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26NotAssociatedRecords), 6, 0)));
            lstavAssociatedrecords.CurrentValue = cgiGet( lstavAssociatedrecords_Internalname);
            AV10AssociatedRecords = (int)(NumberUtil.Val( cgiGet( lstavAssociatedrecords_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0)));
            AV8AddedKeyListXml = cgiGet( edtavAddedkeylistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AddedKeyListXml", AV8AddedKeyListXml);
            AV25NotAddedKeyListXml = cgiGet( edtavNotaddedkeylistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25NotAddedKeyListXml", AV25NotAddedKeyListXml);
            AV6AddedDscListXml = cgiGet( edtavAddeddsclistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AddedDscListXml", AV6AddedDscListXml);
            AV23NotAddedDscListXml = cgiGet( edtavNotaddeddsclistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23NotAddedDscListXml", AV23NotAddedDscListXml);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11N32 */
         E11N32 ();
         if (returnInSub) return;
      }

      protected void E11N32( )
      {
         /* Start Routine */
         edtavContratoservicos_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicos_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_codigo_Visible), 5, 0)));
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV29WWPContext) ;
         if ( StringUtil.StrCmp(AV17HTTPRequest.Method, "GET") == 0 )
         {
            AV32GXLvl6 = 0;
            /* Using cursor H00N32 */
            pr_default.execute(0, new Object[] {AV11ContratoServicos_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A160ContratoServicos_Codigo = H00N32_A160ContratoServicos_Codigo[0];
               AV32GXLvl6 = 1;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            if ( AV32GXLvl6 == 0 )
            {
               GX_msglist.addItem("Registro n�o encontrado.");
            }
            /* Using cursor H00N33 */
            pr_default.execute(1);
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1749Artefatos_Codigo = H00N33_A1749Artefatos_Codigo[0];
               A1751Artefatos_Descricao = H00N33_A1751Artefatos_Descricao[0];
               AV9Artefatos_Codigo = A1749Artefatos_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)));
               AV16Exist = false;
               /* Using cursor H00N34 */
               pr_default.execute(2, new Object[] {AV11ContratoServicos_Codigo, AV9Artefatos_Codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A1749Artefatos_Codigo = H00N34_A1749Artefatos_Codigo[0];
                  A160ContratoServicos_Codigo = H00N34_A160ContratoServicos_Codigo[0];
                  AV16Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
               AV15Description = A1751Artefatos_Descricao;
               if ( AV16Exist )
               {
                  AV7AddedKeyList.Add(A1749Artefatos_Codigo, 0);
                  AV5AddedDscList.Add(AV15Description, 0);
               }
               else
               {
                  AV24NotAddedKeyList.Add(A1749Artefatos_Codigo, 0);
                  AV22NotAddedDscList.Add(AV15Description, 0);
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if (returnInSub) return;
         }
         edtavAddedkeylistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAddedkeylistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAddedkeylistxml_Visible), 5, 0)));
         edtavNotaddedkeylistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaddedkeylistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaddedkeylistxml_Visible), 5, 0)));
         edtavAddeddsclistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAddeddsclistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAddeddsclistxml_Visible), 5, 0)));
         edtavNotaddeddsclistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaddeddsclistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaddeddsclistxml_Visible), 5, 0)));
      }

      protected void E12N32( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV29WWPContext) ;
         imgImageassociateselected_Visible = (AV29WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateselected_Visible), 5, 0)));
         imgImageassociateall_Visible = (AV29WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateall_Visible), 5, 0)));
         imgImagedisassociateselected_Visible = (AV29WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateselected_Visible), 5, 0)));
         imgImagedisassociateall_Visible = (AV29WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateall_Visible), 5, 0)));
         bttBtn_confirm_Visible = (AV29WWPContext.gxTpr_Insert||AV29WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_confirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_confirm_Visible), 5, 0)));
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if (returnInSub) return;
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24NotAddedKeyList", AV24NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedDscList", AV22NotAddedDscList);
      }

      public void GXEnter( )
      {
         /* Execute user event: E13N32 */
         E13N32 ();
         if (returnInSub) return;
      }

      protected void E13N32( )
      {
         /* Enter Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if (returnInSub) return;
         AV18i = 1;
         AV28Success = true;
         AV35GXV1 = 1;
         while ( AV35GXV1 <= AV7AddedKeyList.Count )
         {
            AV9Artefatos_Codigo = (int)(AV7AddedKeyList.GetNumeric(AV35GXV1));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)));
            GX_msglist.addItem("A: "+StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)+" &AddedKeyList: "+StringUtil.Str( (decimal)(AV7AddedKeyList.Count), 9, 0));
            if ( AV28Success )
            {
               AV16Exist = false;
               /* Using cursor H00N35 */
               pr_default.execute(3, new Object[] {AV11ContratoServicos_Codigo, AV9Artefatos_Codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A1749Artefatos_Codigo = H00N35_A1749Artefatos_Codigo[0];
                  A160ContratoServicos_Codigo = H00N35_A160ContratoServicos_Codigo[0];
                  AV16Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(3);
               if ( ! AV16Exist )
               {
                  AV12ContratoServicosArtefatos = new SdtContratoServicosArtefatos(context);
                  AV12ContratoServicosArtefatos.gxTpr_Contratoservicos_codigo = AV11ContratoServicos_Codigo;
                  AV12ContratoServicosArtefatos.gxTpr_Artefatos_codigo = AV9Artefatos_Codigo;
                  AV12ContratoServicosArtefatos.Save();
                  if ( ! AV12ContratoServicosArtefatos.Success() )
                  {
                     AV28Success = false;
                  }
               }
            }
            AV18i = (int)(AV18i+1);
            AV35GXV1 = (int)(AV35GXV1+1);
         }
         AV18i = 1;
         AV37GXV2 = 1;
         while ( AV37GXV2 <= AV24NotAddedKeyList.Count )
         {
            AV9Artefatos_Codigo = (int)(AV24NotAddedKeyList.GetNumeric(AV37GXV2));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)));
            if ( AV28Success )
            {
               AV16Exist = false;
               /* Using cursor H00N36 */
               pr_default.execute(4, new Object[] {AV11ContratoServicos_Codigo, AV9Artefatos_Codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A1749Artefatos_Codigo = H00N36_A1749Artefatos_Codigo[0];
                  A160ContratoServicos_Codigo = H00N36_A160ContratoServicos_Codigo[0];
                  AV16Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(4);
               if ( AV16Exist )
               {
                  AV12ContratoServicosArtefatos = new SdtContratoServicosArtefatos(context);
                  AV12ContratoServicosArtefatos.Load(AV11ContratoServicos_Codigo, AV9Artefatos_Codigo);
                  if ( AV12ContratoServicosArtefatos.Success() )
                  {
                     AV12ContratoServicosArtefatos.Delete();
                  }
                  if ( ! AV12ContratoServicosArtefatos.Success() )
                  {
                     AV28Success = false;
                  }
               }
            }
            AV18i = (int)(AV18i+1);
            AV37GXV2 = (int)(AV37GXV2+1);
         }
         if ( AV28Success )
         {
            context.CommitDataStores( "WP_AssociarContratoServicosArtefatos");
         }
         else
         {
            /* Execute user subroutine: 'SHOW ERROR MESSAGES' */
            S142 ();
            if (returnInSub) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12ContratoServicosArtefatos", AV12ContratoServicosArtefatos);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24NotAddedKeyList", AV24NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedDscList", AV22NotAddedDscList);
      }

      protected void E14N32( )
      {
         /* 'Disassociate Selected' Routine */
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S152 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24NotAddedKeyList", AV24NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedDscList", AV22NotAddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E15N32( )
      {
         /* 'Associate selected' Routine */
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S162 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24NotAddedKeyList", AV24NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedDscList", AV22NotAddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E16N32( )
      {
         /* 'Associate All' Routine */
         /* Execute user subroutine: 'ASSOCIATEALL' */
         S172 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24NotAddedKeyList", AV24NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedDscList", AV22NotAddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E17N32( )
      {
         /* 'Disassociate All' Routine */
         /* Execute user subroutine: 'ASSOCIATEALL' */
         S172 ();
         if (returnInSub) return;
         AV24NotAddedKeyList = (IGxCollection)(AV7AddedKeyList.Clone());
         AV22NotAddedDscList = (IGxCollection)(AV5AddedDscList.Clone());
         AV5AddedDscList.Clear();
         AV7AddedKeyList.Clear();
         /* Execute user subroutine: 'SAVELISTS' */
         S112 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24NotAddedKeyList", AV24NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedDscList", AV22NotAddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E18N32( )
      {
         /* Associatedrecords_Dblclick Routine */
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S152 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24NotAddedKeyList", AV24NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedDscList", AV22NotAddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E19N32( )
      {
         /* Notassociatedrecords_Dblclick Routine */
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S162 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24NotAddedKeyList", AV24NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedDscList", AV22NotAddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void S122( )
      {
         /* 'UPDATEASSOCIATIONVARIABLES' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if (returnInSub) return;
         lstavAssociatedrecords.removeAllItems();
         lstavNotassociatedrecords.removeAllItems();
         AV18i = 1;
         AV39GXV3 = 1;
         while ( AV39GXV3 <= AV7AddedKeyList.Count )
         {
            AV9Artefatos_Codigo = (int)(AV7AddedKeyList.GetNumeric(AV39GXV3));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)));
            AV15Description = ((String)AV5AddedDscList.Item(AV18i));
            lstavAssociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)), StringUtil.Trim( AV15Description), 0);
            AV18i = (int)(AV18i+1);
            AV39GXV3 = (int)(AV39GXV3+1);
         }
         AV18i = 1;
         AV40GXV4 = 1;
         while ( AV40GXV4 <= AV24NotAddedKeyList.Count )
         {
            AV9Artefatos_Codigo = (int)(AV24NotAddedKeyList.GetNumeric(AV40GXV4));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)));
            AV15Description = ((String)AV22NotAddedDscList.Item(AV18i));
            lstavNotassociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)), StringUtil.Trim( AV15Description), 0);
            AV18i = (int)(AV18i+1);
            AV40GXV4 = (int)(AV40GXV4+1);
         }
      }

      protected void S142( )
      {
         /* 'SHOW ERROR MESSAGES' Routine */
         AV42GXV6 = 1;
         AV41GXV5 = AV12ContratoServicosArtefatos.GetMessages();
         while ( AV42GXV6 <= AV41GXV5.Count )
         {
            AV21Message = ((SdtMessages_Message)AV41GXV5.Item(AV42GXV6));
            if ( AV21Message.gxTpr_Type == 1 )
            {
               GX_msglist.addItem(AV21Message.gxTpr_Description);
            }
            AV42GXV6 = (int)(AV42GXV6+1);
         }
      }

      protected void S132( )
      {
         /* 'LOADLISTS' Routine */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8AddedKeyListXml)) )
         {
            AV5AddedDscList.FromXml(AV6AddedDscListXml, "Collection");
            AV7AddedKeyList.FromXml(AV8AddedKeyListXml, "Collection");
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25NotAddedKeyListXml)) )
         {
            AV24NotAddedKeyList.FromXml(AV25NotAddedKeyListXml, "Collection");
            AV22NotAddedDscList.FromXml(AV23NotAddedDscListXml, "Collection");
         }
      }

      protected void S112( )
      {
         /* 'SAVELISTS' Routine */
         if ( AV7AddedKeyList.Count > 0 )
         {
            AV8AddedKeyListXml = AV7AddedKeyList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AddedKeyListXml", AV8AddedKeyListXml);
            AV6AddedDscListXml = AV5AddedDscList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AddedDscListXml", AV6AddedDscListXml);
         }
         else
         {
            AV8AddedKeyListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AddedKeyListXml", AV8AddedKeyListXml);
            AV6AddedDscListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AddedDscListXml", AV6AddedDscListXml);
         }
         if ( AV24NotAddedKeyList.Count > 0 )
         {
            AV25NotAddedKeyListXml = AV24NotAddedKeyList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25NotAddedKeyListXml", AV25NotAddedKeyListXml);
            AV23NotAddedDscListXml = AV22NotAddedDscList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23NotAddedDscListXml", AV23NotAddedDscListXml);
         }
         else
         {
            AV25NotAddedKeyListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25NotAddedKeyListXml", AV25NotAddedKeyListXml);
            AV23NotAddedDscListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23NotAddedDscListXml", AV23NotAddedDscListXml);
         }
      }

      protected void S172( )
      {
         /* 'ASSOCIATEALL' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if (returnInSub) return;
         AV18i = 1;
         AV19InsertIndex = 1;
         AV43GXV7 = 1;
         while ( AV43GXV7 <= AV24NotAddedKeyList.Count )
         {
            AV9Artefatos_Codigo = (int)(AV24NotAddedKeyList.GetNumeric(AV43GXV7));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)));
            AV15Description = ((String)AV22NotAddedDscList.Item(AV18i));
            while ( ( AV19InsertIndex <= AV5AddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV5AddedDscList.Item(AV19InsertIndex)), AV15Description) < 0 ) )
            {
               AV19InsertIndex = (int)(AV19InsertIndex+1);
            }
            AV7AddedKeyList.Add(AV9Artefatos_Codigo, AV19InsertIndex);
            AV5AddedDscList.Add(AV15Description, AV19InsertIndex);
            AV18i = (int)(AV18i+1);
            AV43GXV7 = (int)(AV43GXV7+1);
         }
         AV24NotAddedKeyList.Clear();
         AV22NotAddedDscList.Clear();
         /* Execute user subroutine: 'SAVELISTS' */
         S112 ();
         if (returnInSub) return;
      }

      protected void S162( )
      {
         /* 'ASSOCIATESELECTED' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if (returnInSub) return;
         AV18i = 1;
         AV44GXV8 = 1;
         while ( AV44GXV8 <= AV24NotAddedKeyList.Count )
         {
            AV9Artefatos_Codigo = (int)(AV24NotAddedKeyList.GetNumeric(AV44GXV8));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)));
            if ( AV9Artefatos_Codigo == AV26NotAssociatedRecords )
            {
               if (true) break;
            }
            AV18i = (int)(AV18i+1);
            AV44GXV8 = (int)(AV44GXV8+1);
         }
         if ( AV18i <= AV24NotAddedKeyList.Count )
         {
            AV15Description = ((String)AV22NotAddedDscList.Item(AV18i));
            AV19InsertIndex = 1;
            while ( ( AV19InsertIndex <= AV5AddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV5AddedDscList.Item(AV19InsertIndex)), AV15Description) < 0 ) )
            {
               AV19InsertIndex = (int)(AV19InsertIndex+1);
            }
            AV7AddedKeyList.Add(AV26NotAssociatedRecords, AV19InsertIndex);
            AV5AddedDscList.Add(AV15Description, AV19InsertIndex);
            AV24NotAddedKeyList.RemoveItem(AV18i);
            AV22NotAddedDscList.RemoveItem(AV18i);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if (returnInSub) return;
         }
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if (returnInSub) return;
      }

      protected void S152( )
      {
         /* 'DISASSOCIATESELECTED' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if (returnInSub) return;
         AV18i = 1;
         AV45GXV9 = 1;
         while ( AV45GXV9 <= AV7AddedKeyList.Count )
         {
            AV9Artefatos_Codigo = (int)(AV7AddedKeyList.GetNumeric(AV45GXV9));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)));
            if ( AV9Artefatos_Codigo == AV10AssociatedRecords )
            {
               if (true) break;
            }
            AV18i = (int)(AV18i+1);
            AV45GXV9 = (int)(AV45GXV9+1);
         }
         if ( AV18i <= AV7AddedKeyList.Count )
         {
            AV15Description = ((String)AV5AddedDscList.Item(AV18i));
            AV19InsertIndex = 1;
            while ( ( AV19InsertIndex <= AV22NotAddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV22NotAddedDscList.Item(AV19InsertIndex)), AV15Description) < 0 ) )
            {
               AV19InsertIndex = (int)(AV19InsertIndex+1);
            }
            AV24NotAddedKeyList.Add(AV10AssociatedRecords, AV19InsertIndex);
            AV22NotAddedDscList.Add(AV15Description, AV19InsertIndex);
            AV7AddedKeyList.RemoveItem(AV18i);
            AV5AddedDscList.RemoveItem(AV18i);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if (returnInSub) return;
         }
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if (returnInSub) return;
      }

      protected void nextLoad( )
      {
      }

      protected void E20N32( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_N32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_8_N32( true) ;
         }
         else
         {
            wb_table2_8_N32( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_N32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_15_N32( true) ;
         }
         else
         {
            wb_table3_15_N32( false) ;
         }
         return  ;
      }

      protected void wb_table3_15_N32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_47_N32( true) ;
         }
         else
         {
            wb_table4_47_N32( false) ;
         }
         return  ;
      }

      protected void wb_table4_47_N32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_N32e( true) ;
         }
         else
         {
            wb_table1_2_N32e( false) ;
         }
      }

      protected void wb_table4_47_N32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_confirm_Internalname, "", "Confirmar", bttBtn_confirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_confirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarContratoServicosArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarContratoServicosArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_47_N32e( true) ;
         }
         else
         {
            wb_table4_47_N32e( false) ;
         }
      }

      protected void wb_table3_15_N32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefullcontent_Internalname, tblTablefullcontent_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableAttributesCell'>") ;
            wb_table5_18_N32( true) ;
         }
         else
         {
            wb_table5_18_N32( false) ;
         }
         return  ;
      }

      protected void wb_table5_18_N32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_15_N32e( true) ;
         }
         else
         {
            wb_table3_15_N32e( false) ;
         }
      }

      protected void wb_table5_18_N32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableAssociation", 0, "", "", 4, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNotassociatedrecordstitle_Internalname, "Artefatos N�o Associados", "", "", lblNotassociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarContratoServicosArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblCenterrecordstitle_Internalname, " ", "", "", lblCenterrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarContratoServicosArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociatedrecordstitle_Internalname, "Artefatos Associados", "", "", lblAssociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarContratoServicosArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavNotassociatedrecords, lstavNotassociatedrecords_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26NotAssociatedRecords), 6, 0)), 2, lstavNotassociatedrecords_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 6, "em", 0, "row", "", "AssociationListAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_WP_AssociarContratoServicosArtefatos.htm");
            lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26NotAssociatedRecords), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", (String)(lstavNotassociatedrecords.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table6_30_N32( true) ;
         }
         else
         {
            wb_table6_30_N32( false) ;
         }
         return  ;
      }

      protected void wb_table6_30_N32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavAssociatedrecords, lstavAssociatedrecords_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0)), 2, lstavAssociatedrecords_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 6, "em", 0, "row", "", "AssociationListAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_WP_AssociarContratoServicosArtefatos.htm");
            lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", (String)(lstavAssociatedrecords.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_18_N32e( true) ;
         }
         else
         {
            wb_table5_18_N32e( false) ;
         }
      }

      protected void wb_table6_30_N32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateall_Internalname, context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContratoServicosArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateselected_Internalname, context.GetImagePath( "56a5f17b-0bc3-48b5-b303-afa6e0585b6d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContratoServicosArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateselected_Internalname, context.GetImagePath( "a3800d0c-bf04-4575-bc01-11fe5d7b3525", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContratoServicosArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateall_Internalname, context.GetImagePath( "c619e28f-4b32-4ff9-baaf-b3063fe4f782", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContratoServicosArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_30_N32e( true) ;
         }
         else
         {
            wb_table6_30_N32e( false) ;
         }
      }

      protected void wb_table2_8_N32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedassociationtitle_Internalname, tblTablemergedassociationtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociationtitle_Internalname, "Associar neste contrato ao Servi�o :: ", "", "", lblAssociationtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_AssociarContratoServicosArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_N32e( true) ;
         }
         else
         {
            wb_table2_8_N32e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV11ContratoServicos_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContratoServicos_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAN32( ) ;
         WSN32( ) ;
         WEN32( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031221234029");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_associarcontratoservicosartefatos.js", "?202031221234030");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblAssociationtitle_Internalname = "ASSOCIATIONTITLE";
         tblTablemergedassociationtitle_Internalname = "TABLEMERGEDASSOCIATIONTITLE";
         lblNotassociatedrecordstitle_Internalname = "NOTASSOCIATEDRECORDSTITLE";
         lblCenterrecordstitle_Internalname = "CENTERRECORDSTITLE";
         lblAssociatedrecordstitle_Internalname = "ASSOCIATEDRECORDSTITLE";
         lstavNotassociatedrecords_Internalname = "vNOTASSOCIATEDRECORDS";
         imgImageassociateall_Internalname = "IMAGEASSOCIATEALL";
         imgImageassociateselected_Internalname = "IMAGEASSOCIATESELECTED";
         imgImagedisassociateselected_Internalname = "IMAGEDISASSOCIATESELECTED";
         imgImagedisassociateall_Internalname = "IMAGEDISASSOCIATEALL";
         tblTable1_Internalname = "TABLE1";
         lstavAssociatedrecords_Internalname = "vASSOCIATEDRECORDS";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablefullcontent_Internalname = "TABLEFULLCONTENT";
         bttBtn_confirm_Internalname = "BTN_CONFIRM";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtavAddedkeylistxml_Internalname = "vADDEDKEYLISTXML";
         edtavNotaddedkeylistxml_Internalname = "vNOTADDEDKEYLISTXML";
         edtavAddeddsclistxml_Internalname = "vADDEDDSCLISTXML";
         edtavNotaddeddsclistxml_Internalname = "vNOTADDEDDSCLISTXML";
         edtavContratoservicos_codigo_Internalname = "vCONTRATOSERVICOS_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         imgImagedisassociateall_Visible = 1;
         imgImagedisassociateselected_Visible = 1;
         imgImageassociateselected_Visible = 1;
         imgImageassociateall_Visible = 1;
         lstavAssociatedrecords_Jsonclick = "";
         lstavNotassociatedrecords_Jsonclick = "";
         bttBtn_confirm_Visible = 1;
         edtavContratoservicos_codigo_Jsonclick = "";
         edtavContratoservicos_codigo_Visible = 1;
         edtavNotaddeddsclistxml_Visible = 1;
         edtavAddeddsclistxml_Visible = 1;
         edtavNotaddedkeylistxml_Visible = 1;
         edtavAddedkeylistxml_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV24NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV22NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV23NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'imgImageassociateselected_Visible',ctrl:'IMAGEASSOCIATESELECTED',prop:'Visible'},{av:'imgImageassociateall_Visible',ctrl:'IMAGEASSOCIATEALL',prop:'Visible'},{av:'imgImagedisassociateselected_Visible',ctrl:'IMAGEDISASSOCIATESELECTED',prop:'Visible'},{av:'imgImagedisassociateall_Visible',ctrl:'IMAGEDISASSOCIATEALL',prop:'Visible'},{ctrl:'BTN_CONFIRM',prop:'Visible'},{av:'AV10AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV26NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV9Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV24NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV22NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null}]}");
         setEventMetadata("ENTER","{handler:'E13N32',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1749Artefatos_Codigo',fld:'ARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV23NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV12ContratoServicosArtefatos',fld:'vCONTRATOSERVICOSARTEFATOS',pic:'',nv:null},{av:'AV9Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV9Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ContratoServicosArtefatos',fld:'vCONTRATOSERVICOSARTEFATOS',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV24NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV22NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null}]}");
         setEventMetadata("'DISASSOCIATE SELECTED'","{handler:'E14N32',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV10AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV22NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV24NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV23NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV9Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV22NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV23NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV10AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV26NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'ASSOCIATE SELECTED'","{handler:'E15N32',iparms:[{av:'AV24NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV26NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV22NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV23NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV9Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV24NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV22NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV23NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV10AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV26NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'ASSOCIATE ALL'","{handler:'E16N32',iparms:[{av:'AV24NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV22NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV23NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV9Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV24NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV22NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV10AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV26NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV23NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}]}");
         setEventMetadata("'DISASSOCIATE ALL'","{handler:'E17N32',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV24NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV22NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV23NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV24NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV22NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV9Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV23NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV10AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV26NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VASSOCIATEDRECORDS.DBLCLICK","{handler:'E18N32',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV10AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV22NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV24NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV23NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV9Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV22NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV23NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV10AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV26NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VNOTASSOCIATEDRECORDS.DBLCLICK","{handler:'E19N32',iparms:[{av:'AV24NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV26NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV22NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV23NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV9Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV24NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV22NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV25NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV23NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV10AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV26NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV7AddedKeyList = new GxSimpleCollection();
         AV5AddedDscList = new GxSimpleCollection();
         AV24NotAddedKeyList = new GxSimpleCollection();
         AV22NotAddedDscList = new GxSimpleCollection();
         AV12ContratoServicosArtefatos = new SdtContratoServicosArtefatos(context);
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV8AddedKeyListXml = "";
         AV25NotAddedKeyListXml = "";
         AV6AddedDscListXml = "";
         AV23NotAddedDscListXml = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV29WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV17HTTPRequest = new GxHttpRequest( context);
         scmdbuf = "";
         H00N32_A160ContratoServicos_Codigo = new int[1] ;
         H00N33_A1749Artefatos_Codigo = new int[1] ;
         H00N33_A1751Artefatos_Descricao = new String[] {""} ;
         A1751Artefatos_Descricao = "";
         H00N34_A1749Artefatos_Codigo = new int[1] ;
         H00N34_A160ContratoServicos_Codigo = new int[1] ;
         AV15Description = "";
         H00N35_A1749Artefatos_Codigo = new int[1] ;
         H00N35_A160ContratoServicos_Codigo = new int[1] ;
         H00N36_A1749Artefatos_Codigo = new int[1] ;
         H00N36_A160ContratoServicos_Codigo = new int[1] ;
         AV41GXV5 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV21Message = new SdtMessages_Message(context);
         sStyleString = "";
         bttBtn_confirm_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblNotassociatedrecordstitle_Jsonclick = "";
         lblCenterrecordstitle_Jsonclick = "";
         lblAssociatedrecordstitle_Jsonclick = "";
         imgImageassociateall_Jsonclick = "";
         imgImageassociateselected_Jsonclick = "";
         imgImagedisassociateselected_Jsonclick = "";
         imgImagedisassociateall_Jsonclick = "";
         lblAssociationtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_associarcontratoservicosartefatos__default(),
            new Object[][] {
                new Object[] {
               H00N32_A160ContratoServicos_Codigo
               }
               , new Object[] {
               H00N33_A1749Artefatos_Codigo, H00N33_A1751Artefatos_Descricao
               }
               , new Object[] {
               H00N34_A1749Artefatos_Codigo, H00N34_A160ContratoServicos_Codigo
               }
               , new Object[] {
               H00N35_A1749Artefatos_Codigo, H00N35_A160ContratoServicos_Codigo
               }
               , new Object[] {
               H00N36_A1749Artefatos_Codigo, H00N36_A160ContratoServicos_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV32GXLvl6 ;
      private short nGXWrapped ;
      private int AV11ContratoServicos_Codigo ;
      private int wcpOAV11ContratoServicos_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int A1749Artefatos_Codigo ;
      private int AV9Artefatos_Codigo ;
      private int edtavAddedkeylistxml_Visible ;
      private int edtavNotaddedkeylistxml_Visible ;
      private int edtavAddeddsclistxml_Visible ;
      private int edtavNotaddeddsclistxml_Visible ;
      private int edtavContratoservicos_codigo_Visible ;
      private int AV26NotAssociatedRecords ;
      private int AV10AssociatedRecords ;
      private int imgImageassociateselected_Visible ;
      private int imgImageassociateall_Visible ;
      private int imgImagedisassociateselected_Visible ;
      private int imgImagedisassociateall_Visible ;
      private int bttBtn_confirm_Visible ;
      private int AV18i ;
      private int AV35GXV1 ;
      private int AV37GXV2 ;
      private int AV39GXV3 ;
      private int AV40GXV4 ;
      private int AV42GXV6 ;
      private int AV19InsertIndex ;
      private int AV43GXV7 ;
      private int AV44GXV8 ;
      private int AV45GXV9 ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String edtavAddedkeylistxml_Internalname ;
      private String edtavNotaddedkeylistxml_Internalname ;
      private String edtavAddeddsclistxml_Internalname ;
      private String edtavNotaddeddsclistxml_Internalname ;
      private String edtavContratoservicos_codigo_Internalname ;
      private String edtavContratoservicos_codigo_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String lstavNotassociatedrecords_Internalname ;
      private String lstavAssociatedrecords_Internalname ;
      private String scmdbuf ;
      private String imgImageassociateselected_Internalname ;
      private String imgImageassociateall_Internalname ;
      private String imgImagedisassociateselected_Internalname ;
      private String imgImagedisassociateall_Internalname ;
      private String bttBtn_confirm_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String bttBtn_confirm_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablefullcontent_Internalname ;
      private String tblTablecontent_Internalname ;
      private String lblNotassociatedrecordstitle_Internalname ;
      private String lblNotassociatedrecordstitle_Jsonclick ;
      private String lblCenterrecordstitle_Internalname ;
      private String lblCenterrecordstitle_Jsonclick ;
      private String lblAssociatedrecordstitle_Internalname ;
      private String lblAssociatedrecordstitle_Jsonclick ;
      private String lstavNotassociatedrecords_Jsonclick ;
      private String lstavAssociatedrecords_Jsonclick ;
      private String tblTable1_Internalname ;
      private String imgImageassociateall_Jsonclick ;
      private String imgImageassociateselected_Jsonclick ;
      private String imgImagedisassociateselected_Jsonclick ;
      private String imgImagedisassociateall_Jsonclick ;
      private String tblTablemergedassociationtitle_Internalname ;
      private String lblAssociationtitle_Internalname ;
      private String lblAssociationtitle_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool AV16Exist ;
      private bool AV28Success ;
      private String AV8AddedKeyListXml ;
      private String AV25NotAddedKeyListXml ;
      private String AV6AddedDscListXml ;
      private String AV23NotAddedDscListXml ;
      private String A1751Artefatos_Descricao ;
      private String AV15Description ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXListbox lstavNotassociatedrecords ;
      private GXListbox lstavAssociatedrecords ;
      private IDataStoreProvider pr_default ;
      private int[] H00N32_A160ContratoServicos_Codigo ;
      private int[] H00N33_A1749Artefatos_Codigo ;
      private String[] H00N33_A1751Artefatos_Descricao ;
      private int[] H00N34_A1749Artefatos_Codigo ;
      private int[] H00N34_A160ContratoServicos_Codigo ;
      private int[] H00N35_A1749Artefatos_Codigo ;
      private int[] H00N35_A160ContratoServicos_Codigo ;
      private int[] H00N36_A1749Artefatos_Codigo ;
      private int[] H00N36_A160ContratoServicos_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV17HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV7AddedKeyList ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV24NotAddedKeyList ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV5AddedDscList ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22NotAddedDscList ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV41GXV5 ;
      private SdtContratoServicosArtefatos AV12ContratoServicosArtefatos ;
      private SdtMessages_Message AV21Message ;
      private wwpbaseobjects.SdtWWPContext AV29WWPContext ;
   }

   public class wp_associarcontratoservicosartefatos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00N32 ;
          prmH00N32 = new Object[] {
          new Object[] {"@AV11ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00N33 ;
          prmH00N33 = new Object[] {
          } ;
          Object[] prmH00N34 ;
          prmH00N34 = new Object[] {
          new Object[] {"@AV11ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00N35 ;
          prmH00N35 = new Object[] {
          new Object[] {"@AV11ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00N36 ;
          prmH00N36 = new Object[] {
          new Object[] {"@AV11ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00N32", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV11ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N32,1,0,false,true )
             ,new CursorDef("H00N33", "SELECT [Artefatos_Codigo], [Artefatos_Descricao] FROM [Artefatos] WITH (NOLOCK) ORDER BY [Artefatos_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N33,100,0,true,false )
             ,new CursorDef("H00N34", "SELECT [Artefatos_Codigo], [ContratoServicos_Codigo] FROM [ContratoServicosArtefatos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV11ContratoServicos_Codigo and [Artefatos_Codigo] = @AV9Artefatos_Codigo ORDER BY [ContratoServicos_Codigo], [Artefatos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N34,1,0,false,true )
             ,new CursorDef("H00N35", "SELECT [Artefatos_Codigo], [ContratoServicos_Codigo] FROM [ContratoServicosArtefatos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV11ContratoServicos_Codigo and [Artefatos_Codigo] = @AV9Artefatos_Codigo ORDER BY [ContratoServicos_Codigo], [Artefatos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N35,1,0,false,true )
             ,new CursorDef("H00N36", "SELECT [Artefatos_Codigo], [ContratoServicos_Codigo] FROM [ContratoServicosArtefatos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV11ContratoServicos_Codigo and [Artefatos_Codigo] = @AV9Artefatos_Codigo ORDER BY [ContratoServicos_Codigo], [Artefatos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N36,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
