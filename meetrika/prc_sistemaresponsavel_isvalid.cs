/*
               File: PRC_SistemaResponsavel_IsValid
        Description: Responsavel do Grupo de Objeto de Controle
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:12.55
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_sistemaresponsavel_isvalid : GXProcedure
   {
      public prc_sistemaresponsavel_isvalid( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_sistemaresponsavel_isvalid( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_GpoObjCtrl_Codigo ,
                           ref int aP1_Responsavel )
      {
         this.A1826GpoObjCtrl_Codigo = aP0_GpoObjCtrl_Codigo;
         this.AV8Responsavel = aP1_Responsavel;
         initialize();
         executePrivate();
         aP0_GpoObjCtrl_Codigo=this.A1826GpoObjCtrl_Codigo;
         aP1_Responsavel=this.AV8Responsavel;
      }

      public int executeUdp( ref int aP0_GpoObjCtrl_Codigo )
      {
         this.A1826GpoObjCtrl_Codigo = aP0_GpoObjCtrl_Codigo;
         this.AV8Responsavel = aP1_Responsavel;
         initialize();
         executePrivate();
         aP0_GpoObjCtrl_Codigo=this.A1826GpoObjCtrl_Codigo;
         aP1_Responsavel=this.AV8Responsavel;
         return AV8Responsavel ;
      }

      public void executeSubmit( ref int aP0_GpoObjCtrl_Codigo ,
                                 ref int aP1_Responsavel )
      {
         prc_sistemaresponsavel_isvalid objprc_sistemaresponsavel_isvalid;
         objprc_sistemaresponsavel_isvalid = new prc_sistemaresponsavel_isvalid();
         objprc_sistemaresponsavel_isvalid.A1826GpoObjCtrl_Codigo = aP0_GpoObjCtrl_Codigo;
         objprc_sistemaresponsavel_isvalid.AV8Responsavel = aP1_Responsavel;
         objprc_sistemaresponsavel_isvalid.context.SetSubmitInitialConfig(context);
         objprc_sistemaresponsavel_isvalid.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_sistemaresponsavel_isvalid);
         aP0_GpoObjCtrl_Codigo=this.A1826GpoObjCtrl_Codigo;
         aP1_Responsavel=this.AV8Responsavel;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_sistemaresponsavel_isvalid)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( A1826GpoObjCtrl_Codigo > 0 )
         {
            /* Using cursor P00SW2 */
            pr_default.execute(0, new Object[] {A1826GpoObjCtrl_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               GXt_int1 = A1828GpoObjCtrl_Responsavel;
               GXt_int2 = (short)(A1826GpoObjCtrl_Codigo);
               new prc_getgpoobjctrl_responsavel(context ).execute( ref  GXt_int2, out  GXt_int1) ;
               A1826GpoObjCtrl_Codigo = GXt_int2;
               A1828GpoObjCtrl_Responsavel = GXt_int1;
               AV8Responsavel = A1828GpoObjCtrl_Responsavel;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
         }
         else
         {
            AV8Responsavel = 0;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00SW2_A1826GpoObjCtrl_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_sistemaresponsavel_isvalid__default(),
            new Object[][] {
                new Object[] {
               P00SW2_A1826GpoObjCtrl_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short GXt_int2 ;
      private int A1826GpoObjCtrl_Codigo ;
      private int AV8Responsavel ;
      private int A1828GpoObjCtrl_Responsavel ;
      private int GXt_int1 ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_GpoObjCtrl_Codigo ;
      private int aP1_Responsavel ;
      private IDataStoreProvider pr_default ;
      private int[] P00SW2_A1826GpoObjCtrl_Codigo ;
   }

   public class prc_sistemaresponsavel_isvalid__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00SW2 ;
          prmP00SW2 = new Object[] {
          new Object[] {"@GpoObjCtrl_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00SW2", "SELECT TOP 1 [GpoObjCtrl_Codigo] FROM [GrupoObjetoControle] WITH (NOLOCK) WHERE [GpoObjCtrl_Codigo] = @GpoObjCtrl_Codigo ORDER BY [GpoObjCtrl_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SW2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
